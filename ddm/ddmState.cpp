
#include "ddmState.h"

#include "ddmAutomaton.h"
#include "ctaErrMsg.h"

bool operator< (ddmState p1, ddmState p2)
{
  CTAERR << "Runtime error: Operator < (ddmState, ddmState) used, but "
	 << "this operator returns always true."
	 << endl;
  CTAPUT;
  return true;
}

bool operator== (ddmState p1, ddmState p2)
{
  CTAERR << "Runtime error: Operator == (ddmState, ddmState) used, but "
	 << "this operator returns always true."
	 << endl;
  CTAPUT;
  return true;
}


ddmAutomatonOstream&
operator<<(ddmAutomatonOstream& s, const ddmState& y)
{
  s << "    STATE " << y.getId() << " {" << endl;
  
  // Invariant.
  s << "      INV " << endl;
  s << "        ";
  s << y.getInvariant() << ";" << endl;
  
  // Derivatives.
  s << "      DERIV " << endl;

  s.enableDerivs();
  s << "        ";
  s << y.getDerivs();
  s.disableDerivs();

  s << ";" << endl;

  // Transitions.
  {
    const vector<ddmTransition*>& lTrans = s.getAutomaton()->getTransitions();
    for(unsigned int i=0; i<lTrans.size(); ++i)
    {
      // Check if transition (i) starts from the current state.
      if(y.getId() == s.getStateIds()[lTrans[i]->getPrecursor()])
      {
	// This transition starts from the current state.
	//   Print it.
	s << *lTrans[i];
      }
    }
  }

  s << "    }" << endl;

  return s;
}

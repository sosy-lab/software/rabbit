#include "ddmAutomaton.h"
#include "ddmConfigPair.h"
#include "ddmConfigPairList.h"
#include "ctaErrMsg.h"

bool operator< (ddmConfigPair p1, ddmConfigPair p2)
{
  CTAERR << "Runtime error: Operator < (ddmConfigPair, ddmConfigPair) "
	 << "used, but "
	 << "this operator returns always true."
	 << endl;
  CTAPUT;
  return true;
}

bool operator== (ddmConfigPair p1, ddmConfigPair p2)
{
  CTAERR << "Runtime error: Operator == (ddmConfigPair, ddmConfigPair) "
	 << "used, but "
	 << "this operator returns always true."
	 << endl;
  CTAPUT;
  return true;
}

// Check if the union of the correspondents is a superset of 
//  the implementation region.
bool
ddmConfigPair::correspondentIsSuperset() const
{
  // Get the implementation region
  //   and set-subtract all spec-regions from it.
  ddmRegion lTmp(this->getImplemConfig().mkRegionUnion());

  for(ddmConfig::const_iterator it = this->getSpecConfig().begin();
      !lTmp.isEmpty() && it != this->getSpecConfig().end();
      ++it)
  {
    // Set-subtract the region of the current configuration
    //   component.
    lTmp.setSubtract(it->second);
  }

  // We found a superset if (lTmp) is now empty.
  return lTmp.isEmpty();
}


void
ddmConfigPair::computeMaximalChangeRoundAndReached(
				   const int pCurrentRound,
				   const int pTracePrefix,
				   const ddmConfigPairList& 
				   pPartition,
				   int& pMaxChangeRound,
				   ctaIntSet& pNowReached) const
{
  // Are we in the first round?
  if(pCurrentRound == 0)
  {
    // In the first round, we do not have to deal with 
    //   (pMaxChangeRound), and we only reach 0.
    pNowReached.insert(0);
  }
  else
  {
    // We are not in the first round.

    // Look if there were any reachable pairs for
    //   the current (pTracePrefix).
    map<int, ctaIntSet>::const_iterator
      it = this->getRecentlyReached().find(pTracePrefix);
    if(it != this->getRecentlyReached().end()
       && it->second.size()>0 )
    {
      // Yes, there were reachable pairs.  They are found
      //   in (it->second).

      // Compute index of 2nd last element of genealogy.
      int lGenealogyIndex = pCurrentRound - 1;

      // Look through all elements of pPartition
      //   whose 2nd last comp in genealogy is
      //   in it->second.  There is always a 2nd last
      //   component, since we are not in the first round.
      for(ddmConfigPairList::const_iterator
	    itPair = pPartition.begin();
	  itPair != pPartition.end();
	  ++itPair)
      {
	// Fetch number of reached pair in 2nd last round.
	int lRecentNumber 
	  = itPair->getGenealogy()[lGenealogyIndex];

	// Does (lRecentNumber) occur in the set?
	if(it->second.find(lRecentNumber) != it->second.end())
	{
	  pNowReached.insert(itPair->getGenealogy().back());
	  
	  if(itPair->getChangeRounds().back() > pMaxChangeRound)
	  {
	    pMaxChangeRound = itPair->getChangeRounds().back();
	  }
	}
      }
    }
  }
}

void
ddmConfigPair::splitWithTimeTransition(const ddmConfigPairList& pPartition,
				       ddmConfigPairList& pResult,
				       const int pCurrentRound) const
{
  // Compute the maximal round in which a recently reached
  //   pair has been touched.
  int lMaxChangeRound = -1;
  // Collect the current numbers of the reached pairs.
  ctaIntSet lNowReached;

  // Fetch maximal change round for reached pairs,
  //  and fetch their current position numbers.
  this->computeMaximalChangeRoundAndReached(pCurrentRound,
					    0,
					    pPartition,
					    lMaxChangeRound,
					    lNowReached);

  // If max change round is too old, there is not much to do.
  if(lMaxChangeRound+1<pCurrentRound)
  {
    // Partition did not change in the last rounds: it will
    //   not split the current pair.
    pResult.push_back(*this);

    // Add information about recently reached pairs.
    pResult.back().setRecentlyReached(map<int, ctaIntSet>());
    if(lNowReached.size() > 0)
    {
      pResult.back().refRecentlyReached()[0] = lNowReached;
    }
  }
  else
  {
    // Compute non-zero-time reachable image of current implem config.
    ddmConfig lImage(* pPartition.getAutoImplem().getDerivedObjDdm()
                            ->mkTimeFollower
		                   (& this->getImplemConfig())->getDerivedObjDdm()
		     );
    
    // Partition the image with (pPartition).  This way, we see which 
    //   non-equivalent configurations can be reached by a nonzero time
    //   transition from the current implementation config,
    //   and we learn about their specification-correspondences.
    ddmConfigPairList lPartition(pPartition.mkPartition(lImage,&lNowReached));
    
    if(lImage.isEmpty())
    {
      // Put current element unchanged into result.
      pResult.push_back(*this);

      // Add information about recently reached pairs.
      pResult.back().setRecentlyReached(map<int, ctaIntSet>());
    }
    else
    {
      // Compute pre-images of partition. This is again
      //   a partition. It can be used to identify the nonequivalent
      //   subregions in the implementation component of the
      //   currently looked at config pair.
      lPartition.nonzeroTimePrecursor(0);
      
      // Partition the current element with (lPartition).
      lPartition.partitionPair(*this, pResult, 0);
    }
  }
}


void
ddmConfigPair::splitWithDiscreteTransition_Sync(const ddmConfigPairList& 
						pPartition,
						const int pSyncLab,
						ddmConfigPairList& pResult,
						const int pCurrentRound,
						const int pTracePrefix) const
{
  // Compute the maximal round in which a recently reached
  //   pair has been touched.
  int lMaxChangeRound = -1;

  // Collect the current numbers of the reached pairs.
  ctaIntSet lNowReached;

  // Fetch maximal change round for reached pairs,
  //  and fetch their current position numbers.
  this->computeMaximalChangeRoundAndReached(pCurrentRound,
					    pTracePrefix,
					    pPartition,
					    lMaxChangeRound,
					    lNowReached);

  // If max change round is too old, there is not much to do.
  if(lMaxChangeRound+1<pCurrentRound)
  {
    // Partition did not change in the last rounds: it will
    //   not split the current pair.
    pResult.push_back(*this);

    // Add information about recently reached pairs.
    pResult.back().setRecentlyReached(map<int, ctaIntSet>());
    if(lNowReached.size() > 0)
    {
      pResult.back().refRecentlyReached()[pTracePrefix] = lNowReached;
    }
  }
  else
  {
    // The reached configuration.
    ddmConfig* lTmpConfig
      = pPartition.getAutoImplem().getDerivedObjDdm()
                            ->mkTransitionFollower(this->getImplemConfig(),
						   pSyncLab,
						   pPartition.getAutoSpec(),
						   false
						   )->getDerivedObjDdm();

    // Why that copy operation????
    ddmConfig lImage(*lTmpConfig);
    delete lTmpConfig;
    
    if(lImage.isEmpty())
    {
      // Put current element unchanged into result.
      pResult.push_back(*this);

      // Add information about recently reached pairs.
      pResult.back().setRecentlyReached(map<int, ctaIntSet>());
    }
    else
    {
      // Partition the image with (pPartition).  This way, we see which
      //   non-equivalent configurations can be reached by the given
      //   transitions from the current implementation configuration,
      //   and we learn about their specification-correspondences.
      ddmConfigPairList 
	lPartition(pPartition.mkPartition(lImage,&lNowReached));
      
      {
	// Compute pre-images of partition. This is no true partition: The 
	//   implem-parts might intersect.
	//   It can be used to identify the nonequivalent
	//   subregions in the implementation component of the
	//   currently looked at config pair.
	lPartition.transitionPrecursor(pSyncLab, pTracePrefix);
	
	// Normally, we would have to call (lPartition.disjoinImplemParts()),
	//   but here this is not necessary because it is done by the caller
	//   for all of (pResult) later.
	
	// Partition the current element with (lPartition).
	lPartition.partitionPair(*this, pResult, pTracePrefix);
      }
      
    } 
  }
}

ostream&
operator<< (ostream& s, const ddmConfigPair& y)
{
  s << "  Genealogy: ";
  for(vector<int>::const_iterator it = y.getGenealogy().begin();
      it != y.getGenealogy().end();
      ++it)
  {
    s << *it << " ";
  }
  s << endl;

  s << "  Changed in rounds:";
  for(vector<int>::const_iterator it1 = y.getChangeRounds().begin();
      it1 != y.getChangeRounds().end();
      ++it1)
  {
    s << *it1 << " ";
  }
  s << endl;

  s << "  Possible traces:" << y.getTraces() << endl;

#ifdef MANAGE_REFUSALS
  s << "  Possible refusals:" << y.getRefusals() << endl;
#endif

  s << "  Implem config:" << y.getImplemConfig() << endl;

  s << "  Spec config:" << y.getSpecConfig() << endl;

  return s;
}




#include "reprNUMBER.h"
#include "ddmVecPlain.h"

ddmVecPlain::ddmVecPlain()
{
  /*
    mSize = 1;
    mContent = new reprNUMBER[mSize];
    mContent[0] = 0;
    */
  mSize = 0;
  mContent = NULL;
}

ddmVecPlain::ddmVecPlain(int pLength)
{
  mSize = pLength;
  mContent = new reprNUMBER[mSize];
  for(int i = 0; i < mSize; ++i)
  {
    mContent[i] = 0;
  }
}

ddmVecPlain::ddmVecPlain(const char *p)
{
  reprNUMBER lNum;
  istrstream lIStr1(p);
  
  // Get dimension of vector.
  mSize = 0;
  while( (void*)(lIStr1 >> lNum) )
  {
    ++mSize;
  }

  mContent = new reprNUMBER[mSize];
  
  istrstream lIStr2(p);    
  
  // Get one number after the other and check success.
  int i = 0;
  while( (void*)(lIStr2 >> lNum) )
  {
    mContent[i] = lNum;
    ++i;
  }
}

ddmVecPlain::ddmVecPlain(const ddmVecPlain& v)
{
  mSize = v.size();
  mContent = new reprNUMBER[mSize];
  for(int i = 0; i < mSize; ++i)
  {
    mContent[i] = v[i];
  }
}
 
// Check if argument is equal to (*this), starting from given index.
bool
ddmVecPlain::equalFromIndex(int pStartIndex,
                            const ddmVecPlain& pVec) const
{
  bool lResult = true;
  
  // Compute upper range for indices to check.
  const int lMaxSize = (int) max(this->size(), pVec.size());
  
  // Check and stop when inequality has been found.
  for(int i = pStartIndex; lResult && (i < lMaxSize); ++i)
  {
    // Check values at current index (i).
    lResult = ((*this)[i] == pVec[i]);
  }

  // Return result.
  return lResult;
}

// Scale all components of a vector by a common factor.
void
ddmVecPlain::scale(const reprNUMBER& pNum)
{
  for(int i = 0; i < this->size(); ++i)
  {
    mContent[i] *= pNum;
  }
}

// Descale all components of a vector by a common factor.
void
ddmVecPlain::descale(const reprNUMBER& pNum)
{
  for(int i = 0; i < this->size(); ++i)
  {
    mContent[i] /= pNum;
  }
}

// Add (pAdd) to current vector.
void
ddmVecPlain::add(const ddmVecPlain& pAdd)
{
  // Compute upper range of indices.
  const int lMaxSize = (int) max(this->size(), pAdd.size());

  // Create new array if dimension of (pAdd) > dimension of (*this) 
  if(lMaxSize > this->size())
  {
    reprNUMBER* lTmpContent = new reprNUMBER[lMaxSize];
    
    // Copy elements to new array.
    for(int i = 0; i < this->size(); ++i)
    {
      lTmpContent[i] = mContent[i];
    }

    // Set new elements to zero.
    for(int i = this->size(); i < lMaxSize; ++i)
    {
      lTmpContent[i] = 0;
    }  
    
    mContent = lTmpContent;
    mSize = lMaxSize;
  }
  
  // Add components to this vector.
  for(int i = 0; i < this->size(); ++i)
  {
    mContent[i] += pAdd[i];
  }
}

// Add (pFactor * pAdd) to current vector.
void
ddmVecPlain::add(const reprNUMBER& pFactor, const ddmVecPlain& pAdd)
{
  // Compute upper range of indices.
  const int lMaxSize = (int) max(this->size(), pAdd.size());

  // Create new array if dimension of (pAdd) > dimension of (*this) 
  if(lMaxSize > this->size())
  {
    // Copy elements to new array.
    reprNUMBER* lTmpContent = new reprNUMBER[lMaxSize];
    for(int i = 0; i < this->size(); ++i)
    {
      lTmpContent[i] = mContent[i];
    }

    // Set new elements to zero.
    for(int i = this->size(); i < lMaxSize; ++i)
    {
      lTmpContent[i] = 0;
    }  
    
    mContent = lTmpContent;
    mSize = lMaxSize;
  }  

  // Add components to this vector.
  for(int i = 0; i < this->size(); ++i)
  {
    mContent[i] += (pFactor * pAdd[i]);
  }
}


// Compute positive greatest common divisor.
//   Only consider components with index starting
//   at pStartPos.
reprNUMBER
ddmVecPlain::gcd() const
{
  // Construct in (v) a copy of (*this) to work on.
  ddmVecPlain v(*this);
  
  reprNUMBER result, minVal, newMinVal;
  int minValIndex = -1, newMinValIndex;

  // Make all components of v positive.
  for(int i = 0; i < v.size(); ++i)
  {
    if(v[i] < 0)
    {
      v[i] = -v[i];
    }
  }

  // Determine minimal nonzero value in (minVal).
  {
    minVal = 0;

    for(int i = 0; i < v.size(); ++i)
    {
      if(   (minVal == 0)
	    || ((v[i] != 0) && (v[i] < minVal))
	    )
      {
	minVal = v[i];
	minValIndex = i;
      }
    }
  }

  // Check if there were any nonzero values.
  if(minVal == 0)
  {
    // No nonzero values!
    result = 0;
  }
  else
  {
    // There is a nonzero value in the vector.

    // May loop finish?
    bool ready = false;
      
    // On entry to while-loop, (newMinVal)
    //   must contain the next divisor.
    newMinVal = minVal;
    newMinValIndex = minValIndex;
      
    // Euclidean algorithm.
    while(!ready)
    {
      // Store current divisor in minVal.
      minVal = newMinVal;
      minValIndex = newMinValIndex;
	
      // Compute next divisor in newMinVal;
      newMinVal = 0;
	
      // Compute modulus of all components by minVal.
      for(int i = 0; i < v.size(); ++i)
      {
	if(i != minValIndex)
	{
	  v[i] %= minVal;
	  
	  // Compute smallest non-zero modulus in (newMinVal).
	  if(   (newMinVal == 0)
		|| ((v[i] != 0) && (v[i] < newMinVal)) 
		)
	  {
	    newMinVal = v[i];
	    newMinValIndex = i;
	  }
	}
      }
	
      // We are ready when there was no non-zero modulus.
      ready = (0 == newMinVal);
    }
      
    // The result is in (minVal).
    result = minVal;
  }

  return result;
}

// Divide all components by positive GCD ??? and strip trailing zeros. ???
void
ddmVecPlain::normalize()
{
  reprNUMBER gcd = this->gcd();

  if(gcd > 1)
  {
    this->descale(gcd);
  }

  /*
  // Strip trailing zeros.
  {
    int i = this->size()-1;
      
    while(i>=0 && this->mContent[i] == 0)
    {
      this->mContent.pop_back();
      i--;
    }
  }
  */
}

// swap (*this) with pVec
void
ddmVecPlain::swap(ddmVecPlain& pVec)
{
  reprNUMBER* lTmpContent = pVec.mContent;
  int lTmpSize =pVec.size();

  pVec.mContent = this->mContent;
  pVec.mSize = this->size();

  this->mContent = lTmpContent;
  this->mSize = lTmpSize;
}

// Add (pNum) dimensions before (pAT).
void
ddmVecPlain::addDimensions(int pAt, int pNum)
{
  reprNUMBER* lTmpContent = new reprNUMBER[this->size() + pNum];
  
  // We only have to do something if the vector
  //   extends longer than the position where the
  //   new dimensions are to be inserted.
  
  if(pAt < this->size())
  { 
    // Copy all elements from index 0 to pAt into lTmpContent.
    for(int i = 0; i < pAt; ++i)
    {
      lTmpContent[i] = mContent[i];
    }
    
    // Append (pNum) zeros at end of lTmpContent.
    for(int i = pAt; i < (pAt + pNum); ++i)
    {
      lTmpContent[i] = 0;
    } 

    // Append all elements from index pAt to end at end of lTmpContent
    for(int i = pAt; i < this->size(); ++i)
    {
      lTmpContent[i + pNum] = mContent[i];
    }
    
    // Set new size of vector.
    mSize += pNum;
    mContent = lTmpContent;
  }
}

void
ddmVecPlain::removeDimensions(int pFrom, int pUpTo)
{
  // Compute the number of elems to erase.
  int lElemsToErase = min((this->size() - pFrom), (pUpTo - pFrom));
  
#ifdef DEBUG  
  cout << "ddmVecPlain::removeDimensions: min("
       << (this->size() - pFrom) << ","
       << (pUpTo - pFrom) << ") = "
       << lElemsToErase << endl;
  cout << "ddmVec::removeDimensions(" << pFrom << "," << pUpTo << "):"
       << "removing " << lElemsToErase << " elems in vector"
       << *this << endl;
#endif
    
  if(lElemsToErase > 0)
  {
    reprNUMBER* lTmpContent = new reprNUMBER[this->size() - lElemsToErase];
  
    // Copy elems from index 0 to (pFrom) into lTmpContent.
    for(int i = 0; i < pFrom; ++i)
    {
      lTmpContent[i] = mContent[i];
    }
    
    // Append residual elems from index (pUpTo) at lTmpContent.
    for(int j = pUpTo; j < this->size(); ++j)
    {
      lTmpContent[j - lElemsToErase] = mContent[j];
    } 
    
    mSize -= lElemsToErase;
    mContent = lTmpContent;
  }
  // Normalize;
  this->normalize();
}

// change sign of vector elems, but ignore first if pIgnoreFirst == true 
void
ddmVecPlain::changeSigns(bool pIgnoreFirst)
{
  int i = 0;
  
  // If we have to ignore the first element,
  //   and there is at least one element in the vector:
  //   move to second.
  if(pIgnoreFirst && (this->size() > 0))
  {
    ++i;
  }
  
  for(/* Initialization already done */; i < this->size(); ++i)
  {
    mContent[i] *= -1;
  }
}

bool
operator < (const ddmVecPlain& v1, const ddmVecPlain& v2)
{
  bool lResult = true;
  
  // Compute upper range for indices to check.
  const int lMaxSize = (int) max(v1.size(), v2.size());
  
  for(int i = 0; lResult && (i < lMaxSize); ++i)
  {
    lResult = (v1[i] < v2[i]);
  }
  
  return lResult;
}

ddmVecPlain
operator + (const ddmVecPlain& v1, const ddmVecPlain& v2)
{
  // Compute upper range of indices
  const int lMaxSize = (int) max(v1.size(), v2.size());
  
  ddmVecPlain lResult(lMaxSize);

  for(int i = 0; i < lMaxSize; ++i)
  {
    lResult[i] = v1[i] + v2[i];
  } 

  return lResult;
}

ddmVecPlain
operator - (const ddmVecPlain& v1, const ddmVecPlain& v2)
{
  // Compute upper range of indices
  const int lMaxSize = (int) max(v1.size(), v2.size());

  ddmVecPlain lResult(lMaxSize);

  for(int i = 0; i < lMaxSize; ++i)
  {
    lResult[i] = v1[i] - v2[i];
  } 

  return lResult;
}

ddmVecPlain
operator - (const ddmVecPlain& v1)
{
  ddmVecPlain lResult(v1);

  for(int i = 0; i < lResult.size(); ++i)
  {
    lResult[i] *= -1;
  } 

  return lResult;
}

ddmVecPlain
operator * (const reprNUMBER& n, const ddmVecPlain& v)
{
  ddmVecPlain lResult(v.size());

  for(int i = 0; i < lResult.size(); ++i)
  {
    lResult[i] = n * v[i];
  } 

  return lResult;
}

ddmVecPlain
operator / (const ddmVecPlain& v, const reprNUMBER& n)
{
  ddmVecPlain lResult(v.size());

  for(int i = 0; i < lResult.size(); ++i)
  {
    lResult[i] = v[i] / n;
  } 

  return lResult;
}

reprNUMBER
operator * (const ddmVecPlain& v1, const ddmVecPlain& v2)
{
  // Compute upper range of indices
  const int lMaxSize = (int) max(v1.size(), v2.size());

  reprNUMBER lResult = 0;

  for(int i = 0; i < lMaxSize; ++i)
  {
    lResult += v1[i] * v2[i];
  } 

  return lResult;
}

bool
operator == (const ddmVecPlain& v1, const ddmVecPlain& v2)
{
  bool lResult = true;

  const int lMaxSize = (int)  max(v1.size(), v2.size());
  
  for(int i = 0; lResult && (i < lMaxSize); ++i)
  {
    lResult = (v1[i] == v2[i]);
  }
    
  return lResult;
}

istream&  
operator >> (istream& s, ddmVecPlain& y)
{
  int lSize;

  // Read vector size.
  s >> lSize;

  // Read vector.
  {
    ddmVecPlain v(lSize);

    for(int i = 0; i < lSize; ++i)
    {
      s >> v[i];
    }

    y = v;

    return s;
  }
}

ostream&  
operator << (ostream& s, const ddmVecPlain& y)
{
  s << y.size() << ":";

  for(int i = 0; i < y.size(); ++i)
  {
    s << " " << y[i];
  }

  return s;
}

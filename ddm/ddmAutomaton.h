#ifndef _ddmAutomaton_h_
#define _ddmAutomaton_h_


#include <algorithm>
#include <set>
#include <vector>

#include "reprAutomaton.h"
#include "ddmAutomatonOstream.h"
#include "ddmConfig.h"
#include "ddmConfigPairList.h"
#include "ctaIntSet.h"
#include "ctaMap.h"
#include "ddmState.h"
#include "ddmTransition.h"

class ddmAutomaton : public reprAutomaton
{
private:
  // It should be not allowed to use the standard operators.
  void operator=(const ddmAutomaton&);
  void operator,(const ddmAutomaton&);

public: // Public static methods.

  // Construct the product automaton for two given automata.
  static ddmAutomaton*
  mkProduct(const ddmAutomaton& pAuto1, const ddmAutomaton& pAuto2);

private: // Attributes.

  // The name of the automaton. Should be to the name of the ctaModule
  //   from which it will be constructed.
  ctaString mName;

  // Initial configuration.
  ddmConfig mInitial;

  // The states.
  vector<ddmState> mStates;
  
  // The transitions are aggregated.
  vector<ddmTransition*> mTransitions;

  // The set of synchronizations.
  ctaIntSet mSyncSet;

  // Ids for the vars, states, and syncs.
  vector<string> mVarIds;
  vector<string> mStateIds;
  vector<string> mSyncIds;

public: // Constructors and destructor.

  ddmAutomaton(ctaString pName,
	       int pNumStates, 
	       const vector<string>& pVarIds, // Without constants.
	       const vector<string>& pSyncIds)
    : mName(pName),
      mInitial(pVarIds.size(), pNumStates),
      mStates(pNumStates),
      mTransitions(),
      mSyncSet(),
      mVarIds(pVarIds),
      mStateIds(pNumStates,string("")),
      mSyncIds(pSyncIds)
  {}
  
  ddmAutomaton(ctaString pName,
	       int pNumStates,
	       const vector<string>& pVarIds,
	       const vector<string>& pSyncIds,
	       const ctaIntSet& pSyncSet)
    : mName(pName),
      mInitial(pVarIds.size(), pNumStates),
      mStates(pNumStates),
      mTransitions(),
      mSyncSet(pSyncSet),
      mVarIds(pVarIds),
      mStateIds(pNumStates, string("")),
      mSyncIds(pSyncIds)
  {}


  ddmAutomaton(const ddmAutomaton& pAuto)
    : mName       (pAuto.mName),
      mInitial    (pAuto.mInitial),
      mStates     (pAuto.mStates),
      // Empty vector, we have to copy the content!
      mTransitions(),
      mSyncSet    (pAuto.mSyncSet),
      mVarIds     (pAuto.mVarIds),
      mStateIds   (pAuto.mStateIds),
      mSyncIds    (pAuto.mSyncIds)
  {
    for(vector<ddmTransition*>::const_iterator
	  it = pAuto.mTransitions.begin();
	it!= pAuto.mTransitions.end();
	++it)
    {
      // We have to copy each transition.
      mTransitions.push_back(new ddmTransition(**it));
    }
  }
  
  ~ddmAutomaton()
  {
    for(vector<ddmTransition*>::iterator
	  it = mTransitions.begin();
	it!= mTransitions.end();
	++it)
    {
      delete *it;
    }
  }
  
public: // Accessors.
  
  void
  setInitial(const ddmConfig& pInitial)
  {
    mInitial = pInitial;
  }
  
  const reprConfig&  
  getInitial() const
  {
    return mInitial;
  }
  
  const vector<ddmState>&  
  getStates() const
  {
    return mStates;
  }
  
  const vector<ddmTransition*>&
  getTransitions() const
  {
    return mTransitions;
  }
  
  void
  addState(int pPos, const ddmState& pState)
  {
    // Put (pState) into the table.
    mStates[pPos] =  pState;
    
    // Put id of (pState) into its table.
    mStateIds[pPos] = pState.getId();
  }
  
  void
  addTransition(ddmTransition* pTransition)
  {
    mTransitions.push_back(pTransition);
  }
  
  // This adds the sync label to the alphabet of this automaton.
  //   To be called from (ctaAutomaton::mkDdmAutomaton).
  void
  addSync(const int pSync)
  {
    // Add sync-signal to sync-set.
    if(pSync > -1)
    {
      mSyncSet.insert(pSync);
    }
  }
  
  const ctaIntSet&
  getSyncSet() const
  {
    return mSyncSet;
  }
  
  const vector<string>&
  getVarIds() const
  {
    return mVarIds;
  }
  
  const vector<string>&
  getStateIds() const
  {
    return mStateIds;
  }
  
  const vector<string>&
  getSyncIds() const
  {
    return mSyncIds;
  }
  
public: // Service methods.
  
  // Check if this automaton allows to sync on the foreign set (pForeign)
  //   if the own transition carries the label set (pOwn).
  // The result is true if the intersection of (pForeign) and the alphabet
  // of (this) is a subset of (pOwn).
  bool
  allowsForeignSync(const int pForeign, const int pOwn) const;
  
  // Check if (*this) must sync on the given label.
  bool
  mustSync(const int pSync) const
  {
    return ( this->getSyncSet().find(pSync) != this->getSyncSet().end() );
  }
  
public:

  //void DeleteLocalSyncs();

  reprConfig*
  mkTimeFollower(const reprConfig* const pConfig) const;

  reprConfig*
  mkTimePrecursor(const reprConfig* const pConfig) const;

  reprConfig*
  mkTransitionFollower(const reprConfig* const pConfig,
		       const bool pInitiated) const;

  reprConfig*
  mkTransitionPrecursor(const reprConfig* const pConfig,
			const bool pInitiated) const;

  // Needed for implementation check.
  reprConfig*
  mkTransitionFollower(const reprConfig& pConfig,
		       const int pSyncLab,
		       const reprAutomaton& pAutoPartner,
		       const bool pInitiated) const;
  
  // Needed for implementation check.
  reprConfig*
  mkTransitionPrecursor(const reprConfig& pConfig,
			const int pSyncLab,
			const reprAutomaton& pAutoPartner,
			const bool pInitiated) const;
  
  //   (pSteps) is the number of maximal steps to use.
  //   It is decremented in each cycle.  If a fixpoint is reached,
  //   the return value is >= 0; if no fixpoint is reached, the 
  //   return value is -1.
  // The caller has to control memory of result.
  reprConfig*
  mkReachable(reprAutomaton_ReachDir pTag,
	      const reprConfig* const pConfig,
	      int& pSteps) const;

  // Compute reachable config from (pNewReachable),
  //   suppose that (lReachable) already has been 
  //   recognized as reachable.
  //   Do at most (pMaxSteps) steps.
  //   (pFixpointReached) is true if fixpoint has been reached.
  void
  computeReachabilityOld(reprConfig* pReachable,
			 reprConfig* pNewReachable,
			 int pMaxSteps,
			 bool &pFixpointReached) const;
  
  void
  computeReachabilityLikeDBM(reprConfig* pReachable,
		             reprConfig* pNewReachable,
		             int pMaxSteps,
		             bool &pFixpointReached) const;

  void
  computeReachability(reprConfig* pReachable,
		      reprConfig* pNewReachable,
		      int pMaxSteps,
		      bool &pFixpointReached) const;
  
  // Computes, if it is possible to reach (pError) from (pInitial).
  bool
  isReachable(const reprConfig* const pInitial, 
	      const reprConfig* const pError) const;

  void
  simulateCtaModel(ctaMap<ctaComponent*>* pComp) const
  {
    cerr << "Runtime error: Simulate a CTA model isn't suported by the " 
	 << "DDM representation." << endl;
  }

  reprAutomatonOstream
  mkAutomatonOstream(ostream& s) const
  {
    ddmAutomatonOstream lAOS(&s, this);
    return lAOS;
  }

  reprConfig*
  mkFullConfig() const
  {
    // Determine dimension.
    int lDim = mInitial.getDim();
    // Create full configuration for all states in automaton (*this).
    ddmConfig* lConfig = new ddmConfig(lDim, mStates.size());
    for(unsigned int j = 0;
	j < mStates.size();
	++j)
    {
      (*lConfig)[j] = ddmRegion(lDim, true);
    }
    return lConfig;
  }

  // aheinig 28.06.2000 
  reprConfig*
  mkEmptyConfig() const
  {
    return new ddmConfig(mInitial.getDim(), mStates.size());
  }

  /*
  bool
  isRefinementOf(const reprAutomaton* pSpecAuto) const
  {
    bool lResult = false;

    cerr << "Refinement checking for ddm representation not implemented yet."
	 << endl;


    // We prepare the parameters for
    //   reprAutomaton::isImplementationProve() from the
    //   representation library.

    ddmConfigPairList* lConfigPairList;
    lConfigPairList = new ddmConfigPairList(*this, *pSpecAuto); 

    int  lMaxRounds = 10;
    int  lReachablePerEquiv = 1;
    bool lInitialError;
	
    //we'll test the relationship of the two automatons
    // Check if (lImplemAuto) implements (lSpecAuto).
    bool result = this->isImplementationProve(*lSpecAuto, 
					 lMaxRounds, 
					 lReachablePerEquiv, 
					 *lConfigPairList, 
					 lInitialError);

    delete lConfigPairList;
    return result;


    return lResult;
  }
  */


  // Check if (*this) implements (pAuto).
  //   If an error has been found, return error examples, otherwise,
  //   return an empty list.
  //   In (pMaxRounds), return a negative value if nothing can
  //   be said, and return a nonnegative value if result is valid.
  //   (pReachablePerEquiv) tells how many reachability-steps
  //   are done per equiv-step.
  //   If result is valid and an error has been found,
  //   (pErrorExamples) contains the error pairs.
  //   (pInitial) is true if the non-implementation is because
  //   of the initial configuration, and it is true if
  //   non-implementation is otherwise.
  bool
  isImplementationProve(const reprAutomaton& pAuto, 
			int& pMaxRounds,
			int pReachablePerEquiv,
			ddmConfigPairList& pErrorExamples,
			bool& pInitial) const;
  
  ddmAutomaton*
  getDerivedObjDdm()
  {
    return this;
  }

  const ddmAutomaton*
  getDerivedObjDdm() const
  {
    return this;
  }

  bddAutomaton*
  getDerivedObjBdd()
  {
    cerr << "Runtime error: Wrong derived object expected." << endl;
    return NULL;
  }

  const bddAutomaton*
  getDerivedObjBdd() const
  {
    cerr << "Runtime error: Wrong derived object expected." << endl;
    return NULL;
  }

public: // IO.
  
  // It prints the automaton itself.
  void print(ostream& s) const;

  void print(ostream& pS, const reprConfig* pConfig) const
  {
    ddmAutomatonOstream s(&pS, this);
    const ddmConfig* lConfig = pConfig->getDerivedObjDdm();

    s << (* lConfig);
  }

};

// Local Variables:
// mode:C++
// End:

#endif // _ddmAutomaton_h_

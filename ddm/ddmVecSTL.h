#ifndef _ddmVecSTL_h_
#define _ddmVecSTL_h_


#include <iostream>
#include <strstream>
#include <vector>

#include "reprNUMBER.h"
#include "ddmObject.h"

// A class for numerical vectors.

class ddmVecSTL : private ddmObject
{
private:
  // It should be not allowed to use the standard operators.
  // Uses the standard operator '='.
  void operator,(const ddmVecSTL&);

public: // Static methods.

  // Construct a vector pointing to the 
  //   positive direction of dimension (pDim).
  static ddmVecSTL
  mkPosDimVec(int pDim)
  {
    ddmVecSTL result(pDim+1);

    result.mContent[pDim] = 1;

    return result;
  }


private: // Attributes.

  // Content of numerical vector.
  vector<reprNUMBER> mContent;

public:

  ddmVecSTL()
    : mContent(0)
  {}

  explicit ddmVecSTL(int pLength)
    : mContent(pLength)
  {}
  
  //ddmVecSTL(int pLength, const reprNUMBER* pContent);

  ddmVecSTL(const ddmVecSTL& v)
    : mContent(v.mContent)
  {}

  explicit 
  ddmVecSTL(const char *p);

public: // Accessors.

  reprNUMBER& 
  operator[](unsigned int i)
  {
    // Lengthen content, if necessary.
    if(i >= mContent.size())
    {
      this->mContent.resize(i+1);
    }

    return mContent[i];
  }

  const reprNUMBER
  operator[](unsigned int i) const
  {
    reprNUMBER result;

    if(i >= mContent.size())
    {
      result = 0;
    }
    else
    {
      result = mContent[i];
    }

    return result;
  }

  int 
  size() const
  {
    return mContent.size();
  }

  bool
  isEmpty() const
  {
    return 0 == mContent.size();
  }
  
public: // Service methods.

  // Counts the nonzero's for sorting.
  int
  countNonZeros() const
  {
    int result=0;
    
    for (vector<reprNUMBER>::const_iterator it = mContent.begin();
         it != mContent.end();
         ++it)
    {
      if ((*it) != 0) 
      {
	++result;
      }
    }
    return result;
  }

  // Check if argument is equal to (*this), starting from given index.
  bool
  equalFromIndex(int pStartIndex, const ddmVecSTL& pVec) const;

  // Scale all components of a vector by a common factor.
  void
  scale(const reprNUMBER& pNum);
  
  // Descale all components of a vector by a common factor.
  void
  descale(const reprNUMBER& pNum);

  // Add (pAdd) to current vector.
  void
  add(const ddmVecSTL& pAdd);
  
  // Add (pFactor * pAdd) to current vector.
  void
  add(const reprNUMBER& pFactor, const ddmVecSTL& pAdd);
  
  // Compute positive greatest common divisor.
  //   Only consider components with index starting
  //   at pStartPos.
  reprNUMBER
  gcd() const;

  // Divide all components by positive GCD and strip trailing zeros.
  void
  normalize();

  void
  swap(ddmVecSTL& pVec)
  {
    this->mContent.swap(pVec.mContent);
  }

  void
  removeDimensions(unsigned int pFrom, unsigned int pUpTo);

  // Add (pNum) dimensions before (pAT).
  void
  addDimensions(unsigned int pAt, unsigned int pNum);

  void
  changeSigns(bool pIgnoreFirst);

public: // Friends.

  friend bool
  operator < (const ddmVecSTL& v1, const ddmVecSTL& v2)
  {
    return v1.mContent < v2.mContent;
  }

  friend ddmVecSTL
  operator + (const ddmVecSTL& v1, const ddmVecSTL& v2);

  friend ddmVecSTL
  operator - (const ddmVecSTL& v1, const ddmVecSTL& v2);

  friend ddmVecSTL
  operator - (const ddmVecSTL& v1);

  friend ddmVecSTL
  operator * (const reprNUMBER& n, const ddmVecSTL& v);

  friend ddmVecSTL
  operator / (const ddmVecSTL& v, const reprNUMBER& n);

  friend reprNUMBER
  operator * (const ddmVecSTL& v1, const ddmVecSTL& v2);

  friend bool
  operator == (const ddmVecSTL& v1, const ddmVecSTL& v2);

  friend bool
  operator != (const ddmVecSTL& v1, const ddmVecSTL& v2)
  {
    return !(v1==v2);
  }

public: // IO.
  
  friend istream&  
  operator >> (istream& s, ddmVecSTL& y);
  
  friend ostream&  
  operator << (ostream& s, const ddmVecSTL& y);

};

// Local Variables:
// mode:C++
// End:

#endif // _ddmVecSTL_h_

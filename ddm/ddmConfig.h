#ifndef _ddmConfig_h_
#define _ddmConfig_h_


#include <map>

class reprAutomaton;

#include "ddmObject.h"
#include "reprConfig.h"

#include "ddmAutomatonOstream.h"
#include "ctaIntSet.h"
#include "ddmRegion.h"
class ddmState;
class ddmTransition;

class ddmConfig : public map<int, ddmRegion>, public reprConfig
{
  // It is an invariant of (ddmConfig) that for an iterator (lIt)
  //   lIt->second.isEmpty() can not be fulfilled!

private:
  // It should be not allowed to use the standard operators.
  // Uses the generated copy constructor. 
  // Uses the standard operator '='.
  void operator,(const ddmConfig&);

  
private: // Attributes.
  
  // The dimension of the polyhedra.
  int mDim;
  // The number of states of the corresponding automaton.
  int mStateNum;
  
  // The empty region.  Used as return result in operator[].
  ddmRegion mEmptyRegion;
  
public: // Constructors and destructor.
  
  // Standard constructor without parameter is forbidden!

  ddmConfig(int pDim, int pStateNum)
    : map<int,ddmRegion>(),
      mDim(pDim),
      mStateNum(pStateNum),
      mEmptyRegion(pDim)
  {}
  
  ddmConfig(const ddmConfig& pConfig)
    : map<int,ddmRegion>(pConfig),
      mDim(pConfig.getDim()),
      mStateNum(pConfig.getStateNum()),
      mEmptyRegion(pConfig.mEmptyRegion)
  {}
  
public: // Accessors.
  
  int
  getDim() const
  {
    return mDim;
  }
  
  int
  getStateNum() const
  {
    return mStateNum;
  }
  
public: // Service methods.

  reprConfig*
  clone() const
  {
    return new ddmConfig(*this);
  }
  
  // Make a union of all regions in the config.
  ddmRegion
  mkRegionUnion() const;
  
  // Unite regions for all states.
  //   Append the ddmXPolys which really have been added
  //   for each state to (pAdded).
  void
  unite(const reprConfig& p, reprConfig* pAdded);
  
  void
  unite(const reprConfig& p)
  {
    unite(p, 0);
  }
  
  bool
  setEqual(const reprConfig& p) const
  {
    return this->setContains(p)
      && p.setContains(*this)
      ;
  }
  
  // Check if (*this) contains (p).
  bool
  setContains(const reprConfig& p) const;
  
  bool
  isEmpty() const;
  
  void
  intersect(const reprConfig& p);
  
  void
  intersectEach(const ddmRegion& p);
  
  void
  setSubtract(const reprConfig& p);

  //Existential quantification of (pIdentifier) which is a
  //  variable or an automaton.
  //It needs (pAutomaton) for accessing the symbol table. 
  virtual void
  exists(const reprAutomaton* pAutomaton, const ctaString* pIdentifier)
  {
    cout << "DDM representation don't support exists operation" << endl; 
  }
  
  // It computes the complement of this configuration set.
  reprConfig*
  mkComplement() const;
  
  // The following operator has to return an empty region of the right
  //   dimension if the region for the state is not represented explicitly.
  const ddmRegion&
  operator[](int pState) const
  {
    const_iterator it = this->find(pState);
    if(it != this->end())
    {
      // Return the region found.
      return it->second;
    }
    else
    {
      // Return an empty region.
      return mEmptyRegion;
    }
  }
  
  // This operator is only for use in left sides of assignments.
  //   If it is used on the right side to read (in a non-const context)
  //   then it returns a empty region with right dimension if
  //   the region for the state is not represented explicitly.
  ddmRegion&
  operator[](int pState)
  {
    return (*((insert( value_type(pState, ddmRegion(mDim)) )).first)).second;
  }

  // Normalize all regions.
  void
  normalize();

  // aheinig.
  virtual void 
  printGraphicalBddVisualization(ostream& pS) const
  {
    cerr << "Runtime error: DDM represantation don't support graphical " 
         << "bdd visualitation." << endl; 
  }
  
  ddmConfig*
  getDerivedObjDdm()
  {
    return this;
  }

  const ddmConfig*
  getDerivedObjDdm() const
  {
    return this;
  }

  bddConfig*
  getDerivedObjBdd()
  {
    cerr << "Runtime error: Wrong derived object expected." << endl;
    return NULL;
  }

  const bddConfig*
  getDerivedObjBdd() const
  {
    cerr << "Runtime error: Wrong derived object expected." << endl;
    return NULL;
  }

public: // Friends.
  
public: // IO.
  
  friend ddmAutomatonOstream&
  operator<<(ddmAutomatonOstream& s, const ddmConfig& y);
  
  friend ostream&
  operator<<(ostream& s, const ddmConfig& y);
};

// Local Variables:
// mode:C++
// End:

#endif // _ddmConfig_h_

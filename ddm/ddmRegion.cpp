
#include "ddmRegion.h"

// Unite (p) with (*this).  If it is nonredundant,
//   also unite it with (*pAdded).
void
ddmRegion::unite(const ddmXPoly& p, ddmRegion* pAdded) // default =0
{
  // Check special case: (*this) is empty.
  //   We know this from (mDim == 0).
  if(this->getDim() == 0)
  {
    mDim = p.getDim();

    this->push_back(p);

    // This was nonredundant.
    if(pAdded)
    {
      pAdded->unite(p);
    }
  }
  else
  {
    // Only a nonempty (p) can contribute.
    if(!p.isEmpty())
    {
      // Iterator of this vector class.
      iterator it; // Used after for-iteration.
      
      // Check if one of the elements of (*this)
      //   contains (p), or if (p) contains
      //   one of the elements of (*this).
      for(it = this->begin();
      it != this->end();
      ++it)
      {
    if( it->contains(p) )
    {
      // (p) is redundant.
      // Leave the for loop.
      break;
    }
    else if( p.contains(*it) )
    {
      // (p) makes (*it) redundant.
      // (*this) is a vector and thus we can use (it) also after erase.
      this->erase(it);
      it--;
    }
      }
      
      // Check if (p) is not redundant.
      if(it == this->end())
      {
    // No inclusion noticed: we have to append (p).
    this->push_back( p );
    
    // This was nonredundant.
    if(pAdded)
    {
      pAdded->unite(p);
    }
      }
    }
  }
}

// Unite the given region (p) with (*this).
//  ddmXPoly's really added to (*this) (which were not in (*this))
//  are also united with (*pAdded).
void
ddmRegion::unite(const ddmRegion& p, ddmRegion* pAdded/* = 0*/)
{
  // To unite with a region, unite with all ddmXPoly's.
  for(const_iterator it = p.begin();
      it != p.end();
      it++)
  {
    this->unite(*it, pAdded);
  }
}

void
ddmRegion::intersect(const ddmXPoly& p)
{
  //cerr << "Region size: " << this->size() << endl;

  // Intersect p with all elements of (*this) and discard
  //   empty results.  Starting from the end makes removals
  //   of empty elements more efficient.
  for(iterator it = this->end();
      it != this->begin();
      )
  {
    // Move to next vector element.
    it--;
      
    // Intersect with p.
    it->intersect(p);

    // Erase if result is empty.
    if(it->isEmpty())
    {
      this->erase(it);
    }
  }
}

void
ddmRegion::intersect(const ddmRegion& p)
{
  //cerr << "Region size: " << this->size() << " " << p.size() << endl;

  // Optimize for cases (p.size() <=1).
  if(p.size() == 0)
  {
    // Empty region.
    // db: Why not this->clear(); to remove all the polys ???
    *this = ddmRegion(mDim, false);
  }
  else if(p.size() == 1)
  {
    this->intersect(p[0]);
  }
  else
  {
    // More than one ddmXPoly in ddmRegion (p).

    // Start with an empty result.
    ddmRegion result(mDim, false);

    for(const_iterator it = p.begin();
    it != p.end();
    ++it)
    {
      // Take a copy of (*this).
      ddmRegion tmp(*this);

    // Compute intersection of current ddmXPoly with copy of (*this).
      tmp.intersect(*it);

      // Append to intermediate result.
      result.unite(tmp);
    }

    *this = result;
  }
}

// Set-subtract a ddmRegion.
void
ddmRegion::setSubtract(const ddmRegion& p)
{
  for(const_iterator it = p.begin();
      !this->isEmpty() && it != p.end();
      ++it)
  {
    this->setSubtract( *it );
  }
}

// Add (pNum) dimensions before (pAT).
void
ddmRegion::addDimensions(int pAt, int pNum)
{
  // Add dimensions to each ddmXPoly.
  for(iterator it = this->begin();
      it != this->end();
      ++it)
  {
    it->addDimensions(pAt, pNum);
  }

  // Update dimension counter.
  mDim += pNum;
}

// Construct a ddmRegion for the changed signs.
void 
ddmRegion::changeSigns()
{
  for(iterator it=this->begin();
      it != this->end();
      ++it)
  {
    it->changeSigns();
  }
}

// Normalize a region by normalizing all elements.
void 
ddmRegion::normalize()
{
  for(iterator it=this->begin();
      it != this->end();
      ++it)
  {
    it->normalize();
  }
}


void
ddmRegion::print(ostream& s, const vector<string>* pVarNames) const
{
  const ddmRegion& y=*this;

  // Print dimension and number of elements.
  s << "ddmRegion: Dim=" << y.mDim 
    << ", Elems=" << y.size() 
    << endl;

  for(const_iterator it=this->begin();
      it != this->end();
      ++it)
  {
    it->print(s, pVarNames);
  }
  
  s << "End ddmRegion" << endl;
}

// Compute projection by existential quantification
//   of variable from number (pFrom) up to but not including
//   (pUpTo).
ddmRegion
ddmRegion::mkProjection(const ddmRegion& p, int pFrom, int pUpTo)
{
  // Start with an empty result of adjusted dimension.
  ddmRegion result(p.getDim()-(pUpTo-pFrom),
           false);
  
  // Project each ddmXPoly.
  for(ddmRegion::const_iterator it = p.begin();
      it != p.end();
      ++it)
  {
    ddmXPoly* ltmp = ddmXPoly::mkProjection(&(*it), pFrom, pUpTo);
    result.unite(* ltmp);
    delete ltmp;
  }
  
  return result;
}

// Compute the complement corresponding to (p).
ddmRegion
ddmRegion::mkComplement(const ddmXPoly& p)
{
  // Start with an empty region.
  ddmRegion result(p.getDim(), false);
  
  // For each constraint, add a ddmXPoly with the inverted constraint.
  const ddmConstraints& lConstraints = p.getConstraints();
  
  ddmConstraints::const_iterator cIt;
  
  for(// The first two constraints are not inverted.
      //   They describe the restrictions for X0 and X1
      //   and express that X0>=1 and X1<=0.
      //   They are used because we are only interested in solutions
      //   with X0<0 and X1=1.
      cIt = lConstraints.begin()+ddmConstraints::mNumStdConstraints;
      cIt != lConstraints.end();
      ++cIt)
  {
    // Compute inverted constraint.
    ddmVec lInvConstraint(cIt->size());
    
    for(int i=0; i<cIt->size(); ++i)
    {
      // Turn all signs. Only at pos. 0: change 0 to 1 and vice versa.
      if(i==0)
      {
    lInvConstraint[i] = ( ((*cIt)[i] == 0) ? 1 : 0 );
      }
      else
      {
    lInvConstraint[i] = -(*cIt)[i];
      }
    }
    
    // Construct a ddmXPoly for the inverted constraint.
    ddmXPoly lXPoly(p.getDim());
    lXPoly.addConstraint(lInvConstraint);
    
    // unite the constructed ddmXPoly with the current result.
    result.unite(lXPoly);
  } // For all constraints starting from the second.
  
  return result;
}

// Yield a union for all ddmXPoly-triples of the arguments to which
//   the function is applied.
ddmRegion*
ddmRegion::mkUnionForAllTriples(ddmXPoly (*func)(const ddmXPoly&,
                         const ddmXPoly&,
                         const ddmXPoly&),
                const ddmRegion& pR1,
                const ddmRegion& pR2,
                const ddmRegion& pR3)
{
  ddmRegion* result = new ddmRegion(pR1.getDim());

  for(ddmRegion::const_iterator it1 = pR1.begin();
      it1 != pR1.end();
      ++it1)
  {
    for(ddmRegion::const_iterator it2 = pR2.begin();
    it2 != pR2.end();
    ++it2)
    {
      for(ddmRegion::const_iterator it3 = pR3.begin();
      it3 != pR3.end();
      ++it3)
      {
    result->unite((*func)(*it1, *it2, *it3));
      }
    }
  }

  return result;
}


/*
#include <map>
#include <string>
#include <algorithm>

extern map<string, ddmXPoly*> pubExpMap;
extern int pubExpCount;
*/

// Yield a union for all ddmXPoly-pairs of the arguments to which
//   the function is applied.
//   Called by ddmRegion::mkTransition*.
ddmRegion*
ddmRegion::mkUnionForAllPairs(ddmXPoly* (*func)(const ddmXPoly&,
                           const ddmXPoly&,
                           const int pSync
                           ),
                  const ddmRegion& pR1,
                  const ddmRegion& pR2,
                  const int pSync
                  )
{
  ddmRegion* result = new ddmRegion(pR1.getDim());

  for(ddmRegion::const_iterator it1=pR1.begin();
      it1 != pR1.end();
      ++it1)
  {
    for(ddmRegion::const_iterator it2=pR2.begin();
    it2 != pR2.end();
    ++it2)
    {
      /*
    ++pubExpCount;
      
    strstream lTmpStream;

    ddmRays lTmpRay1 = it1->getRays();
    ddmRays lTmpRay2 = it2->getRays();

    sort(lTmpRay1.begin(), lTmpRay1.end());
    sort(lTmpRay2.begin(), lTmpRay2.end());
 
    lTmpStream << lTmpRay1 << lTmpRay2
    << '\0';
      
    cout << lTmpStream.str();
    pubExpMap[lTmpStream.str()] = NULL;
    */
      ddmXPoly* ltmp = (*func)(*it1, *it2, pSync); 
      result->unite(* ltmp);
      delete ltmp;
    }
  }
  return result;
}


// Check if (*this) contains all elements of (p).
bool
ddmRegion::elemContains(const ddmRegion& p) const
{
  bool allTriedFound = true;
  
  // Each element of (p) must occur in (*this).
  for(const_iterator itP = p.begin();
      allTriedFound && (itP != p.end());
      ++itP)
  {
    bool found = false;
    
    // Look for (*itP) in (*this).
    for(const_iterator itThis = this->begin();
    !found && itThis != this->end();
    ++itThis)
    {
      found = ((*itP) == (*itThis));
    }
    
    allTriedFound = found;
  }
  
  return allTriedFound;
}

ddmAutomatonOstream&
operator<<(ddmAutomatonOstream& s, const ddmRegion& y)
{
  if(y.isEmpty())
  {
    s << "FALSE";
  }
  else
  {
    // The next ddmXPoly is the first.
    bool lFirst = true;

    for(ddmRegion::const_iterator it = y.begin();
    it != y.end();
    ++it)
    {
      if(!lFirst)
      {
    s << " OR ";
      }
      else
      {
    lFirst = false;
      }

      // Print the current ddmXPoly.
      s << "(";
      s << *it << ")";
    }
  }

  return s;
}

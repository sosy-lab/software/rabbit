#ifndef _ddmConfigPair_h_
#define _ddmConfigPair_h_


#include <map>
#include <vector>

#include "ddmObject.h"
#include "ddmConfig.h"
#include "ctaIntSet.h"
#include "ddmTraces.h"
#include "ctaErrMsg.h"

class ddmConfigPairList;


class ddmConfigPair : private ddmObject
{
private:
  // It should be not allowed to use the standard operators.
  // Uses the generated copy constructor. 
  // Uses the standard operator '='.
  void operator,(const ddmConfigPair&);

private: // Attributes.

  // The config of the implementation automaton.
  ddmConfig mImplemConfig;

  // The associated configuration of the specification automaton.
  ddmConfig mSpecConfig;

  // A genealogy, consisting of the sequence of position numbers
  //   in the different generations of its construction.
  vector<int> mGenealogy;

  // The round numbers when this pair was split.
  vector<int> mChangeRounds;

  // The traces allowed in this configPair.
  ddmTraces mTraces;

#ifdef MANAGE_REFUSALS
  // The traces possibly refused in this configPair.
  ddmTraces mRefusals;
#endif

  // A map which tells for each transition number
  //   which pairs have been reached at the last try.
  //   the pairs are identified via their number during.
  //   the last try.
  map<int, ctaIntSet> mRecentlyReached;

public: // Constructors and destructor.
  explicit ddmConfigPair()
    : mImplemConfig(0, 0),
      mSpecConfig(0, 0)
  {
    CTAERR << "Runtime error: Constructor ddmConfigPair() used, but "
	   << "this Constructor is not implemented."
	   << endl;
    CTAPUT;    
  }

  ddmConfigPair(const ddmConfig& pImplemConfig,
		const ddmConfig& pSpecConfig,
		const vector<int>& pGenealogy,
		const vector<int>& pChangeRounds,
		const ddmTraces& pTraces,
#ifdef MANAGE_REFUSALS
		const ddmTraces& pRefusals,
#endif
		const map<int, ctaIntSet>& pRecentlyReached)
    : mImplemConfig(pImplemConfig),
      mSpecConfig(pSpecConfig),
      mGenealogy(pGenealogy),
      mChangeRounds(pChangeRounds),
      mTraces(pTraces),
#ifdef MANAGE_REFUSALS
      mRefusals(pRefusals),
#endif
      mRecentlyReached(pRecentlyReached)
  {
    this->normalize();
  }
  
  ddmConfigPair(const ddmConfig& pImplemConfig,
		const ddmConfig& pSpecConfig,
		int pLastChangeRound)
    : mImplemConfig(pImplemConfig),
      mSpecConfig(pSpecConfig),
      mGenealogy(),
      mChangeRounds(),
      mTraces(ddmTraces::mkEmptyTrace()),
#ifdef MANAGE_REFUSALS
      mRefusals(ddmTraces::mkEmptySet()),
#endif
      mRecentlyReached()
  {
    mChangeRounds.push_back(pLastChangeRound);
    this->normalize();
    
    // Initialize genealogy to a 0.
    mGenealogy.push_back(0);
  }
  
public: // Accessors.
  
  const ddmConfig&
  getImplemConfig() const
  {
    return mImplemConfig;
  }
  
  ddmConfig&
  refImplemConfig()
  {
    return mImplemConfig;
  }

  const ddmConfig&
  getSpecConfig() const
  {
    return mSpecConfig;
  }
  
  ddmConfig&
  refSpecConfig()
  {
    return mSpecConfig;
  }
  
  void
  setGenealogy(const vector<int>& pVec)
  {
    mGenealogy = pVec;
  }
  
  const vector<int>& 
  getGenealogy() const
  {
    return mGenealogy;
  }
  
  void
  setChangeRounds(const vector<int>& pVec)
  {
    mChangeRounds = pVec;
  }

  const vector<int>& 
  getChangeRounds() const
  {
    return mChangeRounds;
  }
  
  void
  appendLastChangeRound(int pLastChangeRound)
  {
    mChangeRounds.push_back(pLastChangeRound);
  }

  const ddmTraces&
  getTraces() const
  {
    return mTraces;
  }
  
  void
  setTraces(const ddmTraces& pTraces)
  {
    mTraces = pTraces;
  }
  
#ifdef MANAGE_REFUSALS
  const ddmTraces&
  getRefusals() const
  {
    return mRefusals;
  }
  
  void
  setRefusals(const ddmTraces& pRefusals)
  {
    mRefusals = pRefusals;
  }
#endif

  const map<int, ctaIntSet>&
  getRecentlyReached() const
  {
    return mRecentlyReached;
  }
  
  map<int, ctaIntSet>&
  refRecentlyReached()
  {
    return mRecentlyReached;
  }
  
  void
  setRecentlyReached(const map<int, ctaIntSet>& pRecentlyReached)
  {
    mRecentlyReached = pRecentlyReached;
  }
  
public: // Service methods.
  
  // Intersect regions in specification config
  //   with implementation region.
  void
  normalize()
  {
    mImplemConfig.normalize();
    mSpecConfig.intersectEach(mImplemConfig.mkRegionUnion());
    mSpecConfig.normalize();
  }
  
  // Intersect (*this) with argument.
  void
  intersect(const ddmConfigPair& pCP)
  {
    // Compute intersections.
    mImplemConfig.intersect(pCP.getImplemConfig());
    mSpecConfig.intersect(pCP.getSpecConfig());
    
    // Normalize.
    this->normalize();
  }

  // Intersect implem part of (*this) with argument.
  void
  intersect(const ddmConfig& pC)
  {
    // Compute intersection.
    mImplemConfig.intersect(pC);
    
    // Normalize.
    this->normalize();
  }

  // Check if the union of the correspondents is a superset of 
  //  the implementation region.
  bool
  correspondentIsSuperset() const;
  
  bool
  setEqual(const ddmConfigPair& p) const
  {
    return (this->getImplemConfig().setEqual(p.getImplemConfig()))
      && (this->getSpecConfig().setEqual(p.getSpecConfig()))
      ;
  }

  void
  appendToGenealogy(int pI)
  {
    mGenealogy.push_back(pI);
  }
  
  void
  splitWithTimeTransition(const ddmConfigPairList& pPartition,
			  ddmConfigPairList& pResult,
			  const int pCurrentRound) const;
  void
  splitWithDiscreteTransition_Sync(const ddmConfigPairList& pPartition,
				   const int pSyncLab,
				   ddmConfigPairList& pResult,
				   const int pCurrentRound,
				   const int pTracePrefix) const;
  
private: // Private service methods.
  
  // Use (this->mRecentlyReached) to compute for each pair
  //   in (pPartition) reachable from (*this) via
  //   transition of type (pTracePrefix) to compute the number of
  //   the last round in which  an image reachable via (pTracePrefix)
  //   has been changed.  Store this round number in (pMaxChangeRound).
  //   In (this->mRecentlyReached), the indices of the reached 
  //   pairs in the 2nd last round are stored.  The corresponding indices
  //   in the last round are stored in (pNowReached).
  void
  computeMaximalChangeRoundAndReached(const int pCurrentRound,
				      const int pTracePrefix,
				      const ddmConfigPairList& 
				      pPartition,
				      int& pMaxChangeRound,
				      ctaIntSet& pNowReached) const;
  
public: // Friends.
  
public: // IO.
  
  friend ostream&
  operator<< (ostream& s, const ddmConfigPair& y);
};


bool operator< (ddmConfigPair p1, ddmConfigPair p2);
bool operator== (ddmConfigPair p1, ddmConfigPair p2);

// Local Variables:
// mode:C++
// End:

#endif // _ddmConfigPair_h_

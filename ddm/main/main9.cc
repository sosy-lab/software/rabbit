#include <iostream>

#include "cRegion.h"

int 
main(int argc, char *argv[])
{
  // Build vectors of variable names to use.
  string lVarNames_X[] = {"X", "Y",};
  vector<string> lVarNames(&lVarNames_X[0], &lVarNames_X[2]);

  // Build and output parameter values.
  //{
  
    // Super0: 0<X
    cRegion rSuper0("1  0  1  0");

    cout << "\n\n\nSuper0: ";
    rSuper0.print(cout, &lVarNames);


    // Super1: 0<=X
    cRegion rSuper1("0  0  1  0");

    cout << "\n\n\nSuper1: ";
    rSuper1.print(cout, &lVarNames);


    // Sub0: 0<X<1, 0<Y<1
    cRegion rSub0("1  0  1  0");
    rSub0.intersect(cRegion("1  0  0  1"));
    rSub0.intersect(cRegion("1  1 -1  0"));
    rSub0.intersect(cRegion("1  1  0 -1"));

    cout << "\n\n\nSub0: ";
    rSub0.print(cout, &lVarNames);


    // Sub1: 0<=X<=1, 0<=Y<=1
    cRegion rSub1("0  0  1  0");
    rSub1.intersect(cRegion("0  0  0  1"));
    rSub1.intersect(cRegion("0  1 -1  0"));
    rSub1.intersect(cRegion("0  1  0 -1"));

    cout << "\n\n\nSub1: ";
    rSub1.print(cout, &lVarNames);
  //}

  // Check inclusions.
  {
    cout << "\n\n\n";

    cout << "Super0 "
	 << (rSuper0.contains(rSub0) ? "contains" : "does NOT contain") 
	 << " Sub0\n";
    cout << "Super0 "
	 << (rSuper0.contains(rSub1) ? "contains" : "does NOT contain") 
	 << " Sub1\n";
    cout << "Super1 " 
	 << (rSuper1.contains(rSub0) ? "contains" : "does NOT contain") 
	 << " Sub0\n";
    cout << "Super1 " 
	 << (rSuper1.contains(rSub1) ? "contains" : "does NOT contain") 
	 << " Sub1\n";
  }

  return 0;
}

#include <iostream>

#include "cRegion.h"


int 
main(int argc, char *argv[])
{
  cConstraints c;
  int dim;

  int c0[] = {1,-1,1};
  int c1[] = {1,3,-1};
  int c2[] = {1,-1,0,1};
  int c3[] = {1,3,0,-1};

  c.push_back(new cDualVec(cVec(sizeof(c0)/sizeof(c0[0]),
					  &c0[0]) ) );
  c.push_back(new cDualVec(cVec(sizeof(c1)/sizeof(c1[0]),
					  &c1[0]) ) );
  c.push_back(new cDualVec(cVec(sizeof(c2)/sizeof(c2[0]),
					  &c2[0]) ) );
  c.push_back(new cDualVec(cVec(sizeof(c3)/sizeof(c3[0]),
					  &c3[0]) ) );

  dim = 2;

  cXPoly p(dim);
  p.addConstraints( c );

  cRegion r0(dim, false);
  cRegion r1(dim, true);

  r0.unite( p );
  r1.intersect( p );
  
  cout << "r0: " << r0;
  cout << "r1: " << r1;

  if( r0.contains(r1) )
  {
    cout << "r0 contains r1\n";
  }

  if( r1.contains(r0) )
  {
    cout << "r1 contains r0\n";
  }

  return 0;
}

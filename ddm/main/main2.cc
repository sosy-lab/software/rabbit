#include <iostream>

// Interfaces.
#include "cMat.h"
#include "cPoly.h"

// Implementations.
// #include "cConstraintsI.h"
// #include "cRaysI.h"

int 
main(int argc, char *argv[])
{
  cMat mat, mat1, mat2;
  int dim;

  cout << "mat:\n";
  cin >> mat;

  cout << "dimension:\n";
  cin >> dim;

  cPoly pol(dim, mat);

  cout << "pol:\n";
  cout << pol;

  return 0;
}

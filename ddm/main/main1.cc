#include <iostream>

#include "cMat.h"
#include "cPoly.h"

int 
main(int argc, char *argv[])
{
  cMat m;

  int dim;

  cout << "constraints:\n";
  cin >> m;

  cout << "dimension:\n";
  cin >> dim;

  cout << "constraints:" << m;

  // Construct a polyhedron for the given constraints.
  cPoly p1(dim, cConstraints(m));

  // Construct a dual polyhedron.
  cPoly p2(dim, cConstraints(p1.getRays()));

  cout << "dual():" << p1;
  cout << "dual(dual()):" << p2;
  
  return 0;
}

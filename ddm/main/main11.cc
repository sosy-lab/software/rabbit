#include <iostream>
#include <string>

#include "cNUMBER.h"

#include "cAutomaton.h"
#include "cConfig.h"
#include "cRegion.h"
#include "cState.h"
#include "cTransition.h"


// Generate a Fisher-Automaton with i=(pI), a=(pA), b=(pB).
cAutomaton
mkFisher(string pI, string pA, string pB)
{
  cRegion lReg1, lReg2;
  cConfig lInit;

    // Simulate the Fisher mutual exclusion protocol.

    // The variable T says how much time has passed since the Timer
    //   was reset the last time.
    
    // The variable K says which of two processes (1 or 2) may have
    //   access to the critical section.
    // We have six states.
  int lNumStates = 6;

  // Construct skeleton for automaton.
  vector<string> lVarIds(0);
  lVarIds.push_back("T");
  lVarIds.push_back("K");

  vector<string> lSyncIds(0);

  cAutomaton lAuto = cAutomaton(lNumStates, lVarIds, lSyncIds);
  
  // Construct an initial configuration with empty regions
  //   for all states but the first.
  {
    // In the initial configuration, we are in 
    //   state 0 and T=0, K=0.
    lInit = cConfig(2);
    
    lReg1 =         cRegion(" 0  0  1  0");
    lReg1.intersect(cRegion(" 0  0 -1  0"));
    lReg1.intersect(cRegion(" 0  0  0 -1"));
    lReg1.intersect(cRegion(" 0  0  0  1"));
    
    lInit[0] = lReg1;

    lAuto.setInitial(lInit);
  }

  // Construct the states.
  {
    // The derivations are always constant: DER(T)=1, DER(K)=0.
    cRegion lDer = cRegion(" 0  1 -1  0");
    lDer.intersect(cRegion(" 0 -1  1  0"));
    lDer.intersect(cRegion(" 0  0  0 -1"));
    lDer.intersect(cRegion(" 0  0  0  1"));
    
    // State 0: Invar:TRUE
    //   Uncritical code.
    {
      lReg1 =         cRegion(2,true);
      
      lAuto.addState( 0, cState("uncritical", lReg1, lDer) );
    }

    // State 1: Invar:TRUE
    //   Wait for K=0.
    {
      lReg1 = cRegion(2,true);

      lAuto.addState( 1, cState("waitForEntry", lReg1, lDer) );
    }

    // State 2: Invar: T<=pA
    //   The assignment (K:=i) needs at most pA time units.
    {
      lReg1 = cRegion(string(" 0  ") + pA + " -1 0");
      
      lAuto.addState( 2, cState("assign", lReg1, lDer) );
    }

    // State 3: Invar:TRUE
    //   Wait for some delay.
    {
      lReg1 = cRegion(2, true);
      
      lAuto.addState( 3, cState("delay", lReg1, lDer) );
    }

    // State 4: Invar: FALSE
    //   Check if K=i or K!= i
    {
      lReg1 = cRegion(2, false);

      lAuto.addState( 4, cState("check", lReg1, lDer) );
    }

    // State 5: Invar: TRUE
    //   Critical.
    {
      lReg1 = cRegion(2, true);

      lAuto.addState( 5, cState("critical", lReg1, lDer) );
    }
  }

  // Transitions.
  {
    // State 0 -> State 1: Guard TRUE, Assignments T'=T, K'=K.
    {
      lReg1 =         cRegion(2, true);

      lReg2 =         cRegion(" 0  0  1  0 -1  0");
      lReg2.intersect(cRegion(" 0  0 -1  0  1  0"));
      lReg2.intersect(cRegion(" 0  0  0  1  0 -1"));
      lReg2.intersect(cRegion(" 0  0  0 -1  0  1"));

      lAuto.addTransition( cTransition(0,1, lReg1,lReg2) );
    }

    // State 1 -> State 2: Guard K = 0, Assignment T'=0, K'=K.
    {
      lReg1 =         cRegion(" 0  0  0  1");
      lReg1.intersect(cRegion(" 0  0  0 -1"));

      lReg2 =         cRegion("0 0  0  0 -1  0");
      lReg2.intersect(cRegion("0 0  0  0  1  0"));
      lReg2.intersect(cRegion("0 0  0  1  0 -1"));
      lReg2.intersect(cRegion("0 0  0 -1  0  1"));

      lAuto.addTransition( cTransition(1,2, lReg1,lReg2) );
    }

    // State 2 -> State 3: Guard TRUE, Assignment T'=0, K'=i.
    //   Start the counter at zero.
    {
      lReg1 = cRegion(2, true);

      lReg2 =         cRegion("0  0  0  0 -1  0");
      lReg2.intersect(cRegion("0  0  0  0  1  0"));
      lReg2.intersect(cRegion("0  " + pI + " 0  0  0 -1"));
      lReg2.intersect(cRegion("0 -" + pI + " 0  0  0  1"));

      lAuto.addTransition( cTransition(2,3, lReg1,lReg2) );
    }

    // State 3 -> State 4: Guard T>=pB,  Assignment T'=T, K'=K.
    {
      lReg1 =         cRegion(" 0 -" + pB + "  1  0");

      lReg2 =         cRegion(" 0  0  1  0 -1  0");
      lReg2.intersect(cRegion(" 0  0 -1  0  1  0"));
      lReg2.intersect(cRegion(" 0  0  0  1  0 -1"));
      lReg2.intersect(cRegion(" 0  0  0 -1  0  1"));

      lAuto.addTransition( cTransition(3,4, lReg1,lReg2) );
    }

    // State 4 -> State 1: Guard K!=i,  Assignment T'=T, K'=K.
    {
      lReg1 =         cRegion(" 1  "+pI+"  0 -1");
      lReg1.unite(    cRegion(" 1 -"+pI+"  0  1"));

      lReg2 =         cRegion(" 0  0  1  0 -1  0");
      lReg2.intersect(cRegion(" 0  0 -1  0  1  0"));
      lReg2.intersect(cRegion(" 0  0  0  1  0 -1"));
      lReg2.intersect(cRegion(" 0  0  0 -1  0  1"));

      lAuto.addTransition( cTransition(4,1, lReg1,lReg2) );
    }

    // State 4 -> State 5: Guard K=i,  Assignment T'=T, K'=K.
    {
      lReg1 =         cRegion(" 0 -"+pI+"  0  1");
      lReg1.intersect(cRegion(" 0  "+pI+"  0 -1"));

      lReg2 =         cRegion(" 0  0  1  0 -1  0");
      lReg2.intersect(cRegion(" 0  0 -1  0  1  0"));
      lReg2.intersect(cRegion(" 0  0  0  1  0 -1"));
      lReg2.intersect(cRegion(" 0  0  0 -1  0  1"));

      lAuto.addTransition( cTransition(4,5, lReg1,lReg2) );
    }

    // State 5 -> State 0: Guard TRUE,  Assignment T'=T, K'=0.
    {
      lReg1 =         cRegion(2, true);

      lReg2 =         cRegion(" 0  0  1  0 -1  0");
      lReg2.intersect(cRegion(" 0  0 -1  0  1  0"));
      lReg2.intersect(cRegion(" 0  0  0  0  0 -1"));
      lReg2.intersect(cRegion(" 0  0  0  0  0  1"));

      lAuto.addTransition( cTransition(5,0, lReg1,lReg2) );
    }
  }

  return lAuto;
}

int 
main(int argc, char *argv[])
{
  cAutomaton lAuto1 = mkFisher("1","3","4");

  cout << "\n\n\nThe 1st automaton: " << endl << lAuto1;

  cAutomaton lAuto2 = mkFisher("2","3","4");
  cout << "\n\n\nThe 2nd automaton: " << endl << lAuto2;

  cAutomaton lAuto = cAutomaton::mkProduct(lAuto1, lAuto2);
  cout << "\n\n\nThe product automaton: " << endl << lAuto;

    
  cConfig fwdResult;
  int lMax = 20;
  int lSteps = lMax;

  cAutomatonOstream lAOS(&cout, &lAuto);

  fwdResult = cConfig::mkForwardReachable(lAuto, 
					  lAuto.getInitial(), 
					  lSteps);
  if(lSteps >= 0)
  {
    lAOS << "\n\n\nForward fixpoint reached after " << lMax-lSteps
	 << " iterations:\n";
    lAOS << fwdResult;
  }
  else
  {
    lAOS << "\n\n\nNO fixpoint reached after " << lMax
	 << " iterations.  Current:\n";
    lAOS << fwdResult;
  }

  return 0;
}

#include <iostream>

#include "cRegion.h"

int 
main(int argc, char *argv[])
{
  cConstraints c;
  int dim;

  // X>1
  int c0[] = {1,-1,1}; 

  // 3>=X
  int c1[] = {0,3,-1}; 

  // X+X'>=0
  int c2[] = {0,0,1,1}; 

  // 0 >= X+X'
  int c3[] = {0,0,-1,-1}; 

  c.push_back(new cDualVec(cVec(sizeof(c0)/sizeof(c0[0]),
					  &c0[0]) ) );
  c.push_back(new cDualVec(cVec(sizeof(c1)/sizeof(c1[0]),
					  &c1[0]) ) );
  c.push_back(new cDualVec(cVec(sizeof(c2)/sizeof(c2[0]),
					  &c2[0]) ) );
  c.push_back(new cDualVec(cVec(sizeof(c3)/sizeof(c3[0]),
					  &c3[0]) ) );

  dim = 2;

  cXPoly p(dim);
  p.addConstraints(c);

  cRegion r0(dim, false);

  r0.unite( p );

  // Build a vector of variable names to use.
  string lVarNames[] = {"X", "X'"};
  vector<string> lVecNames(&lVarNames[0], &lVarNames[2]);

  r0.print(cout, &lVecNames);

  // Project out dimension 1 (up to but not including 2).
  r0 = cRegion::mkProjection(r0,1,2);
  cout << "projection(r0,1,2):" << r0;

  // Add two new dimensions.
  r0.addDimensions(0,2);
  cout << "Added two dimensions at 0:" << r0;

  // Change signs.
  r0.changeSigns();
  cout << "Changed signs:" << r0;

  return 0;
}

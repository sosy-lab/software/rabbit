%#include <iostream>

#include "cNUMBER.h"
#include "cRegion.h"

int 
main(int argc, char *argv[])
{
  // Build vectors of variable names to use.
  string lVarNames_X[] = {"X", "Y", "X'", "Y'",};
  string lDerNames_X[] = {"DER(X)", "DER(Y)",};
  vector<string> lVarNames(&lVarNames_X[0], &lVarNames_X[4]);
  vector<string> lDerNames(&lDerNames_X[0], &lDerNames_X[2]);

  // A temporary variable.
  cRegion r;

  // Variables for the parameters.
  cRegion rCur, rDer, rInv, rGuard, rAssg;

  // Build and output parameter values.
  {
    // Current: 0<=X<1, 0<=Y<1.
    rCur =         cRegion("0  0  1  0");
    rCur.intersect(cRegion("1  1 -1  0"));
    rCur.intersect(cRegion("0  0  0  1"));
    rCur.intersect(cRegion("1  1  0 -1"));

    cout << "\n\n\ncurrent: ";
    rCur.print(cout, &lVarNames);

    // Derivatives: X=1, Y=1
    rDer =         cRegion("0 -1  1  0");
    rDer.intersect(cRegion("0  1 -1  0"));
    rDer.intersect(cRegion("0 -1  0  1"));
    rDer.intersect(cRegion("0  1  0 -1"));

    cout << "\n\n\nderivs: ";
    rDer.print(cout, lDerNames);


    // Invariant: X > 0.5, X < 2
    rInv =         cRegion("1 -1  2  0");
    rInv.intersect(cRegion("1  2 -1  0"));

    cout << "\n\n\ninvariant: ";
    rInv.print(cout, &lVarNames);


    // Guard: Y<=X+0.5
    rGuard =       cRegion("0  1  2 -2");

    cout << "\n\n\nguard: ";
    rGuard.print(cout, &lVarNames);


    // Assignment: X'=Y+4, Y'=-X
    rAssg =         cRegion("0 -4  0 -1  1  0");
    rAssg.intersect(cRegion("0  4  0  1 -1  0"));
    rAssg.intersect(cRegion("0  0  1  0  0  1"));
    rAssg.intersect(cRegion("0  0 -1  0  0 -1"));

    cout << "\n\n\nassignments: ";
    rAssg.print(cout, &lVarNames);
  }


  // Compute and output results.
  {
    r = cRegion::mkNonZeroTimeFollower(rCur,rDer,rInv);
    cout << "\n\n\ntimeFollower: ";
    r.print(cout, &lVarNames);

    r = cRegion::mkNonZeroTimePrecursor(rCur,rDer,rInv);
    cout << "\n\n\ntimePrecursor: ";
    r.print(cout, &lVarNames);

    r = cRegion::mkTransitionFollower(rCur,rGuard,rAssg);
    cout << "\n\n\ntransitionFollower: ";
    r.print(cout, &lVarNames);

    r = cRegion::mkTransitionPrecursor(rCur,rGuard,rAssg);
    cout << "\n\n\ntransitionPrecursor: ";
    r.print(cout, &lVarNames);
  }

  return 0;
}

#include <iostream>

#include "cXPoly.h"
#include "cMat.h"

int 
main(int argc, char *argv[])
{
  cConstraints c;
  int dim;

  int c0[] = {1,-1,1};
  int c1[] = {1,3,-1};
  int c2[] = {1,-1,0,1};
  int c3[] = {1,3,0,-1};

  c.push_back(new cDualVec(cVec(sizeof(c0)/sizeof(c0[0]),
					  &c0[0]) ) );
  c.push_back(new cDualVec(cVec(sizeof(c1)/sizeof(c1[0]),
					  &c1[0]) ) );
  c.push_back(new cDualVec(cVec(sizeof(c2)/sizeof(c2[0]),
					  &c2[0]) ) );
  c.push_back(new cDualVec(cVec(sizeof(c3)/sizeof(c3[0]),
					  &c3[0]) ) );

  dim = 2;


  cout << "constraints:" << c;

  cXPoly p(dim);
  cout << "Initial cXPoly: " << p;

  p.addConstraints( c );

  cout << "Constrained cXPoly:" << p;
  
  return 0;
}

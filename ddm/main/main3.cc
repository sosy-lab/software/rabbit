#include <iostream>

#include "cXPoly.h"
#include "cMat.h"

int 
main(int argc, char *argv[])
{
  cConstraints c;
  int dim;

  cout << "constraints:\n";
  cin >> c;

  cout << "inhomog. dimension:\n";
  cin >> dim;


  cout << "constraints:" << c;

  cXPoly p(dim);
  cout << "Initial cXPoly: " << p;

  p.addConstraints( c );

  cout << "Constrained cXPoly:" << p;
  
  return 0;
}

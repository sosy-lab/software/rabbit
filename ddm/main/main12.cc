#include <iostream>
#include <string>

#include <stdio.h>

#include "cNUMBER.h"

#include "cAutomaton.h"
#include "cConfig.h"
#include "cConfigPairList.h"
#include "cRegion.h"
#include "cState.h"
#include "cTransition.h"


// A helper function to construct strings.
string
operator+(const string pS, int pI)
{
  static char lBuffer[256];
  sprintf(lBuffer, "%d", pI);

  return pS+lBuffer;
}

// Generate a Fisher-Automaton with i=(pI), total = (pTotal),
//   a=(pA), b=(pB).
// (pStandard) determines if the automaton uses its own
//    labels for entry and exit of critical region (true),
//    or if the same labels are used for all automata (false).
cAutomaton
mkFisher(int pI, int pTotal, string pA, string pB,
	 bool pStandard)
{
  cRegion lReg1, lReg2, lReg3;
  cConfig lInit;

  // Simulate the Fisher mutual exclusion protocol.

  // The variable Ti says how much time has passed since the Timer
  //   was reset the last time in automaton (pI).
    
  // The variable K says which  process may have
  //   access to the critical section.

  // We have six states.
  int lNumStates = 6;

  // Construct skeleton for automaton.

  // Variable IDs.
  vector<string> lVarIds(0);
  for(int i=1; i<=pTotal; i++)
  {
    lVarIds.push_back(string("T")+i);
  }
  lVarIds.push_back("K");

  vector<string> lSyncIds;
  lSyncIds.push_back("enter_critical_region");
  lSyncIds.push_back("exit_critical_region");
  for(int i=1; i<=pTotal; i++)
  {
    lSyncIds.push_back(string("enter_crit")+i);
    lSyncIds.push_back(string("exit_crit")+i);
  }

  cAutomaton lAuto = cAutomaton(lNumStates, lVarIds, lSyncIds);

  // Dimension.
  const int lDim = pTotal+1;

  // Construct three sequences of (pTotal) space-separated numbers.
  //   All but the number at index (pI-1) are zero.
  //   The numbers at index (pI-1) are 1 and -1 and 0.
  string lPlusOne(" ");
  string lMinusOne(" ");
  string lZeros(" ");
  for(int i=0; i<pTotal; i++)
  {
    if(i==pI-1)
    {
      lPlusOne  += "1 ";
      lMinusOne += "-1 ";
      lZeros    += "0 ";
    }
    else
    {
      lPlusOne  += "0 ";
      lMinusOne += "0 ";
      lZeros    += "0 ";
    }
  }
  
  // Construct an initial configuration with empty regions
  //   for all states but the first.
  {
    // In the initial configuration, we are in 
    //   state 0, all Ti=0, K=0.
    lInit = cConfig(lDim);

    // K=0.
    lReg1 =         cRegion(" 0  0 " +lZeros+    "-1");
    lReg1.intersect(cRegion(" 0  0 " +lZeros+    " 1"));

    // Construct equations for Ti=0, for all i.
    for(int i=1; i<=pTotal; ++i)
    {
      string lPlus("0 0 ");
      string lMinus("0 0 ");

      // Append 0-strings with 1 rsp. -1 at pos i to
      //   lPlus and lMinus.
      for(int j=1; j <= pTotal; ++j)
      {
	if(j == i)
	{
	  // Append ones.
	  lPlus +=  "1 ";
	  lMinus += "-1 ";
	}
	else
	{
	  lPlus += "0 ";
	  lMinus+= "0 ";
	}
      }

      // Append k-coefficients.
      lPlus  += "0";
      lMinus += "0";

      // Append rest.
      lReg1.intersect(cRegion(lPlus));
      lReg1.intersect(cRegion(lMinus));
    }

    lInit[0] = lReg1;

    lAuto.setInitial(lInit);
  }

  // Construct the states.
  {
    // The derivations are always constant: DER(Ti)=1, DER(K)=0.
    cRegion lDer = cRegion(" 0  1 " +lMinusOne+ " 0");
    lDer.intersect(cRegion(" 0 -1 " +lPlusOne+  " 0"));
    lDer.intersect(cRegion(" 0  0 " +lZeros+    "-1"));
    lDer.intersect(cRegion(" 0  0 " +lZeros+    " 1"));
    
    // State 0: Invar:TRUE
    //   Uncritical code.
    {
      lReg1 =         cRegion(lDim,true);
      
      lAuto.addState( 0, cState("uncritical", lReg1, lDer) );
    }

    // State 1: Invar:TRUE
    //   Wait for K=0.
    {
      lReg1 = cRegion(lDim,true);

      lAuto.addState( 1, cState("waitForEntry", lReg1, lDer) );
    }

    // State 2: Invar: Ti<=pA
    //   The assignment (K:=i) needs at most pA time units.
    {
      lReg1 = cRegion(string(" 0  ") + pA + lMinusOne + " 0");
      
      lAuto.addState( 2, cState("assign", lReg1, lDer) );
    }

    // State 3: Invar:TRUE
    //   Wait for some delay.
    {
      lReg1 = cRegion(lDim, true);
      
      lAuto.addState( 3, cState("delay", lReg1, lDer) );
    }

    // State 4: Invar: FALSE
    //   Check if K=i or K!= i
    {
      lReg1 = cRegion(lDim, false);

      lAuto.addState( 4, cState("check", lReg1, lDer) );
    }

    // State 5: Invar: TRUE
    //   Critical.
    {
      lReg1 = cRegion(lDim, true);

      lAuto.addState( 5, cState("critical", lReg1, lDer) );
    }
  }

  // Passive transitions: which allow the noncontrolled
  //   variables to change in each state, and the controlled to 
  //   stay the same.
  //   Same structure for all: 
  //     Guard: TRUE, 
  //     Allowed:   (Ti'=Ti).
  //     Initiated: (K'=K).
  {
    lReg1 =         cRegion(lDim, true);

    lReg2 =         cRegion(" 0  0 " +lPlusOne+  " 0 " +lMinusOne+ " 0");
    lReg2.intersect(cRegion(" 0  0 " +lMinusOne+ " 0 " +lPlusOne+  " 0"));

    lReg3 =         cRegion(" 0  0 " +lZeros+    " 1 " +lZeros+    "-1");
    lReg3.intersect(cRegion(" 0  0 " +lZeros+    "-1 " +lZeros+    " 1"));

    lAuto.addTransition( cTransition(0,0, lReg1,lReg2,lReg3) );
    lAuto.addTransition( cTransition(1,1, lReg1,lReg2,lReg3) );
    lAuto.addTransition( cTransition(2,2, lReg1,lReg2,lReg3) );
    lAuto.addTransition( cTransition(3,3, lReg1,lReg2,lReg3) );
    lAuto.addTransition( cTransition(4,4, lReg1,lReg2,lReg3) );
    lAuto.addTransition( cTransition(5,5, lReg1,lReg2,lReg3) );
  }

  // Active transitions: Which move from state to state.
  {
    // State 0 -> State 1: 
    //   Guard: TRUE
    //   Allowed: (Ti'=Ti)
    //   Initiated: (K'=K)
    {
      lReg1 =         cRegion(lDim, true);

      lReg2 =         cRegion(" 0  0 " +lPlusOne+  " 0 " +lMinusOne+ " 0");
      lReg2.intersect(cRegion(" 0  0 " +lMinusOne+ " 0 " +lPlusOne+  " 0"));

      lReg3 =         cRegion(" 0  0 " +lZeros+    " 1 " +lZeros+    "-1");
      lReg3.intersect(cRegion(" 0  0 " +lZeros+    "-1 " +lZeros+    " 1"));

      lAuto.addTransition( cTransition(0,1, lReg1,lReg2,lReg3) );
    }

    // State 1 -> State 2: 
    //   Guard: K = 0
    //   Allowed:   Ti'=0.
    //   Initiated: K'=K.
    {
      lReg1 =         cRegion(" 0  0  "+lZeros+"  1");
      lReg1.intersect(cRegion(" 0  0  "+lZeros+" -1"));

      lReg2 =         cRegion("0 0 " +lZeros+ " 0 " +lMinusOne+ " 0");
      lReg2.intersect(cRegion("0 0 " +lZeros+ " 0 " +lPlusOne+  " 0"));

      lReg3 =         cRegion(" 0  0 " +lZeros+    " 1 " +lZeros+    "-1");
      lReg3.intersect(cRegion(" 0  0 " +lZeros+    "-1 " +lZeros+    " 1"));

      lAuto.addTransition( cTransition(1,2, lReg1,lReg2,lReg3) );
    }

    // State 2 -> State 3:
    //   Guard: TRUE
    //   Allowed:   Ti'=0, K'=i.
    //   Initiated: TRUE.
    //   Start the counter at zero.
    {
      lReg1 = cRegion(lDim, true);

      lReg2 =         cRegion("0  0 "    +lZeros+ " 0 " +lMinusOne+ " 0");
      lReg2.intersect(cRegion("0  0 "    +lZeros+ " 0 " +lPlusOne + " 0"));
      lReg2.intersect(cRegion(string("0  ")+pI +lZeros+ " 0 " +lZeros+"-1"));
      lReg2.intersect(cRegion(string("0 -")+pI +lZeros+ " 0 " +lZeros+" 1"));

      lReg3 = cRegion(2*lDim, true);

      lAuto.addTransition( cTransition(2,3, lReg1,lReg2,lReg3) );
    }

    // State 3 -> State 4:
    //   Guard: Ti>=pB
    //   Allowed: Ti'=Ti
    //   Initiated: K'=K
    {
      lReg1 =         cRegion(" 0 -" + pB +lPlusOne+ " 0");

      lReg2 =         cRegion(" 0  0" +lPlusOne+ " 0 " +lMinusOne+ " 0");
      lReg2.intersect(cRegion(" 0  0" +lMinusOne+" 0 " +lPlusOne+  " 0"));

      lReg3 =         cRegion(" 0  0 " +lZeros+    " 1 " +lZeros+    "-1");
      lReg3.intersect(cRegion(" 0  0 " +lZeros+    "-1 " +lZeros+    " 1"));

      lAuto.addTransition( cTransition(3,4, lReg1,lReg2,lReg3) );
    }

    // State 4 -> State 1:
    //   Guard: K!=i
    //   Allowed:   Ti'=Ti.
    //   Initiated: K'=K.
    {
      lReg1 =         cRegion(string(" 1  ")+pI +lZeros+ "-1");
      lReg1.unite(    cRegion(string(" 1 -")+pI +lZeros+ " 1"));

      lReg2 =         cRegion(" 0  0" +lPlusOne+ " 0 " +lMinusOne+ " 0");
      lReg2.intersect(cRegion(" 0  0" +lMinusOne+" 0 " +lPlusOne+  " 0"));

      lReg3 =         cRegion(" 0  0 " +lZeros+    " 1 " +lZeros+    "-1");
      lReg3.intersect(cRegion(" 0  0 " +lZeros+    "-1 " +lZeros+    " 1"));

      lAuto.addTransition( cTransition(4,1, lReg1,lReg2,lReg3) );
    }

    // State 4 -> State 5:
    //   Guard K=i
    //   Allowed: Ti'=Ti
    //   Initiated: K'=K
    {
      lReg1 =         cRegion(string(" 0 -")+pI +lZeros+ " 1");
      lReg1.intersect(cRegion(string(" 0  ")+pI +lZeros+ "-1"));

      lReg2 =         cRegion(" 0  0" +lPlusOne+ " 0 " +lMinusOne+ " 0");
      lReg2.intersect(cRegion(" 0  0" +lMinusOne+" 0 " +lPlusOne+  " 0"));

      lReg3 =         cRegion(" 0  0 " +lZeros+    " 1 " +lZeros+    "-1");
      lReg3.intersect(cRegion(" 0  0 " +lZeros+    "-1 " +lZeros+    " 1"));

      lAuto.addTransition( cTransition(4,5, lReg1,lReg2,lReg3,
				       (pStandard ? (pI-1)*2+2 : 0)
				       )
			   );
    }

    // State 5 -> State 0: 
    //   Guard: TRUE
    //   Allowed: Ti'=Ti, K'=0.
    //   Initiated: TRUE
    {
      lReg1 =         cRegion(lDim, true);

      lReg2 =         cRegion(" 0  0" +lPlusOne+ " 0 " +lMinusOne+ " 0");
      lReg2.intersect(cRegion(" 0  0" +lMinusOne+" 0 " +lPlusOne+  " 0"));
      lReg2.intersect(cRegion(" 0  0" +lZeros+   " 0 " +lZeros+    "-1"));
      lReg2.intersect(cRegion(" 0  0" +lZeros+   " 0 " +lZeros+    " 1"));

      lReg3 =         cRegion(2*lDim, true);

      lAuto.addTransition( cTransition(5,0, lReg1,lReg2,lReg3,
				       (pStandard ? (pI-1)*2+3 : 1)
				       ) 
			   );
    }
  }

  return lAuto;
}

static void
check_implem(const cAutomaton& pA1, char* pNA1,
	     const cAutomaton& pA2, char* pNA2)
{
  const int lMax = 20;
  int lSteps = lMax;
  cConfigPairList lErrorExamples(pA1, pA2);
  bool lInitial;
  pA1.isImplementationProve(pA2, lSteps, 10, lErrorExamples,lInitial);

  if(lSteps<0)
  {
    cout << "Could not prove anything about "
      "implementation of " << pNA2 << " by " << pNA1 << " in "
	 << lMax << " steps." << endl;
  }
  else
  {
    cout << "After " << lMax-lSteps << " steps: proved that the "
	 << pNA1 << " "
	 << (lErrorExamples.size()==0 
	     ? "implements" : "does not implement") 
	 << " the " << pNA2 << "." << endl;
    if(lErrorExamples.size())
    {
      cout << "Error examples:\n" << lErrorExamples;
    }
  }
}

int 
main(int argc, char *argv[])
{
  cAutomaton lAuto1 = mkFisher(1,2, "3","4", false);
  cout << "\n\n\nThe 1st automaton: " << endl << lAuto1;

  cAutomaton lAuto2 = mkFisher(2,2, "3","4", false);
  cout << "\n\n\nThe 2nd automaton: " << endl << lAuto2;

  cAutomaton lAuto3 = mkFisher(2,2, "3","4", true);
  cout << "\n\n\nThe 3rd automaton: " << endl << lAuto3;

  cAutomaton lAutoProd = cAutomaton::mkProduct(lAuto1, lAuto3);
  cout << "\n\n\nThe product automaton: " << endl << lAutoProd;

  // Check that lAuto1 implements lAuto1.
  check_implem(lAuto1, "1st automaton", lAuto1, "1st automaton");

  // Check that lAuto1 does not implement lAuto2.
  check_implem(lAuto1, "1st automaton", lAuto2, "2nd automaton");

  // Check that lAutoProd implements lAuto3.
  check_implem(lAutoProd, "product automaton",
  	       lAuto3, "3rd automaton");

  // Check that lAutoProd implements itself.
  // check_implem(lAutoProd, "product automaton",
  // lAutoProd, "product automaton");

  return 0;
}

#include <iostream>

#include "cNUMBER.h"

#include "cAutomaton.h"
#include "cConfig.h"
#include "cRegion.h"
#include "cState.h"
#include "cTransition.h"

int 
main(int argc, char *argv[])
{
  cRegion lReg1, lReg2;
  cConfig lInit;

  // Simulate a water-level monitor.

  // Water-level is modelled with "W", a clock with "X".
    
  // Build vectors of variable names to use.

  // We have FOUR states.
  int lNumStates = 4;

  // Construct skeleton for automaton.
  vector<string> lVarIds(0);
  lVarIds.push_back("W");
  lVarIds.push_back("X");

  vector<string> lSyncIds(0);
  lSyncIds.push_back("stopSignal");
  lSyncIds.push_back("pumpGoesOff");
  lSyncIds.push_back("startSignal");
  lSyncIds.push_back("pumpGoesOn");

  cAutomaton lAuto = cAutomaton(lNumStates, lVarIds, lSyncIds);

  // Construct an initial configuration with empty regions
  //   for all states but the first.
  {
    // In the initial configuration, we are in 
    //   state 0 and X=0, W=1, i.e. there is a
    //   bit of water.
    lInit = cConfig(2);
    
    lReg1 =         cRegion(" 0  0  1  0");
    lReg1.intersect(cRegion(" 0  0 -1  0"));
    lReg1.intersect(cRegion(" 0  1  0 -1"));
    lReg1.intersect(cRegion(" 0 -1  0  1"));
    
    lInit[0] = lReg1;

    lAuto.setInitial(lInit);
  }

  // Construct the states.
  {
    // State 0: Invar:W<10, Deriv: DER(X)=1, DER(W)=1.
    //   This means that we can stay in this state as 
    //   long as the water level has not reached 10.
    //   Per time unit, which are counted by X, the water
    //   level rises by one unit.
    {
      lReg1 =         cRegion(" 1 10  0 -1");
      
      lReg2 =         cRegion(" 0  1 -1  0");
      lReg2.intersect(cRegion(" 0 -1  1  0"));
      lReg2.intersect(cRegion(" 0  1  0 -1"));
      lReg2.intersect(cRegion(" 0 -1  0  1"));
      
      lAuto.addState( 0, cState("pumpOn", lReg1, lReg2) );
    }

    // State 1: Invar:x<2, Deriv:  DER(X)=1, DER(W)=1.
    //   In this state, the system has received the signal
    //   to stop the water flow, but the pump needs
    //   2 time units to switch off.  These two seconds
    //   are modelled by this state: The counter has been
    //   reset to zero on entry, and time flows like before.
    {
      lReg1 = cRegion(" 1  2 -1  0");

      // Derivations like previous.
      
      lAuto.addState( 1, cState("waitForPumpStop", lReg1, lReg2) );
    }

    // State 2: Invar: W>5, Deriv: DER(X)=1, DER(W)=-2.
    //  Now, the pump really switched off, which means the 
    //  water just flows out with a rate of two units per 
    //  time unit.  We may stay in this state as long as 
    //  the water level is larger than 5.
    {
      lReg1 = cRegion(" 1 -5  0  1");
      
      lReg2 =         cRegion(" 0  1 -1  0");
      lReg2.intersect(cRegion(" 0 -1  1  0"));
      lReg2.intersect(cRegion(" 0 -2  0 -1"));
      lReg2.intersect(cRegion(" 0  2  0  1"));
      
      lAuto.addState( 2, cState("pumpOff", lReg1, lReg2) );
    }

    // State 3: Invar: X<2, Deriv: DER(X)=1, DER(W)=-2.
    //   The clock has been reset again, since the
    //   pump also need two seconds from the starting
    //   signal to its normal operation.
    {
      lReg1 = cRegion(" 1  2 -1  0");
      
      lReg2 =         cRegion(" 0  1 -1  0");
      lReg2.intersect(cRegion(" 0 -1  1  0"));
      lReg2.intersect(cRegion(" 0 -2  0 -1"));
      lReg2.intersect(cRegion(" 0  2  0  1"));
      
      lAuto.addState( 3, cState("waitForPumpStart", lReg1, lReg2) );
    }
  }

  // Transitions.
  {
    // State 0 -> State 1: Guard W=10, Assignment X'=0, W=W'.
    //   Start the counter at zero.
    {
      lReg1 = cRegion(" 0 -10 0 1");
      lReg1.intersect(cRegion("0 10 0 -1"));

      lReg2 =         cRegion("0 0 0  0  1  0");
      lReg2.intersect(cRegion("0 0 0  0 -1  0"));
      lReg2.intersect(cRegion("0 0 0  1  0 -1"));
      lReg2.intersect(cRegion("0 0 0 -1  0  1"));

      lAuto.addTransition( cTransition(0,1, lReg1,lReg2, 0) );
    }

    // State 1 -> State 2: Guard X = 2, Assignment X=X', W=W'.
    //   Nothing changes with the variables.
    {
      lReg1 =         cRegion(" 0 -2  1  0");
      lReg1.intersect(cRegion(" 0  2 -1  0"));

      lReg2 =         cRegion("0 0  1  0 -1  0");
      lReg2.intersect(cRegion("0 0 -1  0  1  0"));
      lReg2.intersect(cRegion("0 0  0  1  0 -1"));
      lReg2.intersect(cRegion("0 0  0 -1  0  1"));

      lAuto.addTransition( cTransition(1,2, lReg1,lReg2, 1) );
    }

    // State 2 -> State 3: Guard W = 5, Assignment X'=0, W=W'.
    //   Start the counter at zero.
    {
      lReg1 = cRegion(" 0 -5 0 1");
      lReg1.intersect(cRegion("0 5 0 -1"));

      lReg2 =         cRegion("0 0  0  0 -1  0");
      lReg2.intersect(cRegion("0 0  0  0  1  0"));
      lReg2.intersect(cRegion("0 0  0  1  0 -1"));
      lReg2.intersect(cRegion("0 0  0 -1  0  1"));

      lAuto.addTransition( cTransition(2,3, lReg1,lReg2, 2) );
    }

    // State 3 -> State 0: Guard X = 2, Assignment X=X', W=W'.
    //   Nothing changes with the variables.
    {
      lReg1 =         cRegion(" 0 -2  1  0");
      lReg1.intersect(cRegion(" 0  2 -1  0"));

      lReg2 =         cRegion("0 0  1  0 -1  0");
      lReg2.intersect(cRegion("0 0 -1  0  1  0"));
      lReg2.intersect(cRegion("0 0  0  1  0 -1"));
      lReg2.intersect(cRegion("0 0  0 -1  0  1"));

      lAuto.addTransition( cTransition(3,0, lReg1,lReg2, 3) );
    }
  }


  cout << "\n\n\nThe automaton: " << endl << lAuto;

  cout << "\n\n\nInitial configuration:\n" << lAuto.getInitial();

  cConfig fwdResult;

  int lMaxSteps;
  int lSteps;

  lSteps = lMaxSteps = 10;
  fwdResult 
    = cConfig::mkForwardReachable(lAuto, lAuto.getInitial(), lSteps);
  if(lSteps >= 0)
  {
    cout << "\n\n\nForward fixpoint reached after " 
	 << lMaxSteps-lSteps << " iterations:\n" << fwdResult;
  }
  else
  {
    cout << "\n\n\nNO fixpoint reached after " << lMaxSteps 
	 << " iterations.  Aborted.\n";
  }
  

  fwdResult = lAuto.getInitial();

  lSteps = 1; 
  fwdResult = cConfig::mkForwardReachable(lAuto, fwdResult, lSteps);
  cout << "\n\n\nForward reachable after 1 iteration:\n" << fwdResult;

  lSteps = 1; 
  fwdResult = cConfig::mkForwardReachable(lAuto, fwdResult, lSteps);
  cout << "\n\n\nForward reachable after 2 iterations:\n" << fwdResult;

  lSteps = 1; 
  fwdResult = cConfig::mkForwardReachable(lAuto, fwdResult, lSteps);
  cout << "\n\n\nForward reachable after 3 iterations:\n" << fwdResult;

  lSteps = 1; 
  fwdResult = cConfig::mkForwardReachable(lAuto, fwdResult, lSteps);
  cout << "\n\n\nForward reachable after 4 iterations:\n" << fwdResult;

  lSteps = 1; 
  fwdResult = cConfig::mkForwardReachable(lAuto, fwdResult, lSteps);
  cout << "\n\n\nForward reachable after 5 iterations:\n" << fwdResult;

  lSteps = 1; 
  fwdResult = cConfig::mkForwardReachable(lAuto, fwdResult, lSteps);
  cout << "\n\n\nForward reachable after 6 iterations:\n" << fwdResult;

  lSteps = 1; 
  fwdResult = cConfig::mkForwardReachable(lAuto, fwdResult, lSteps);
  cout << "\n\n\nForward reachable after 7 iterations:\n" << fwdResult;

  lSteps = 1; 
  fwdResult = cConfig::mkForwardReachable(lAuto, fwdResult, lSteps);
  cout << "\n\n\nForward reachable after 8 iterations:\n" << fwdResult;

  lSteps = 1; 
  fwdResult = cConfig::mkForwardReachable(lAuto, fwdResult, lSteps);
  cout << "\n\n\nForward reachable after 9 iterations:\n" << fwdResult;

  lSteps = 1; 
  fwdResult = cConfig::mkForwardReachable(lAuto, fwdResult, lSteps);
  cout << "\n\n\nForward reachable after 10 iterations:\n" << fwdResult;


  lSteps = lMaxSteps = 10;
  cConfig bwdResult 
    = cConfig::mkBackwardReachable(lAuto, lAuto.getInitial(), lSteps);
  cout << "\n\n\nBackward reachable after " << lMaxSteps-lSteps 
       << " iterations (of maximal "<< lMaxSteps <<"):\n" << bwdResult;

  return 0;
}

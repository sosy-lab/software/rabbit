
#include "ddmXPoly.h"
#include "ctaErrMsg.h"

const int ddmXPoly::mNumExtraVars = 2;

bool operator< (ddmXPoly p1, ddmXPoly p2)
{
  CTAERR << "Runtime error: Operator < (ddmXPoly, ddmXPoly) used, but "
     << "this operator returns always true."
     << endl;
  CTAPUT;
  return true;
}

// Compute the possibly reachable region from (p1) when
//   the allowed derivations of the variables
//   are given by (pTimeDerivs).  If (nonZeroTime) is true,
//   zero-time reachable regions are not contained in the result.
ddmXPoly
ddmXPoly::mkTimeReach(const ddmXPoly& p1, 
              const ddmXPoly& pTimeDerivs,
              bool pNonZeroTime)
{
  // Construct a ddmPoly (tmp) with (p1)'s rays as 
  //   (possibly strict) constraints.

  // Do NOT use ddmXPoly: The standard constraints of ddmXPoly
  //   are not necessarily true!
  //   Since we use ddmPoly, the dimension has to respect the two 
  //   extra vars for constants and strict inequalities.
  ddmPoly tmp(p1.getDim()+mNumExtraVars);
  
  // We are only interested in solutions with nonnegative first component.
  tmp.addConstraint(ddmVec("1"));

  // Add original rays as constraints.
  //   If (pNonZeroTime), we need special handling.
  if(pNonZeroTime)
  {
    // Get copy of the constraints of (p1).
    ddmConstraints lTmp(p1.getConstraints());

    // We have to add strictness to those non-strict constraints
    //  for which non-zero time will necessarily move away from the 
    //  border.
    
    bool lChangedSomething = false;
    lTmp.addStrictnessDirectedly(pTimeDerivs.getRays(), lChangedSomething);

    // If something changed, compute new rays for these constraints
    //  and add them to (tmp).
    if(lChangedSomething)
    {
      ddmXPoly lNew(p1.getDim());

      // Compute ddmXPoly for changed constraints.
      // (Ignore the standard constraints.)
      lNew.addConstraints(lTmp, ddmConstraints::mNumStdConstraints);

      // Add rays to (tmp).
      tmp.addConstraints(lNew.getRays());
    }
    else
    {
      // We can use the rays of (p1).
      tmp.addConstraints(p1.getRays());
    }
  }
  else
  {
    tmp.addConstraints(p1.getRays());
  }
  
  // Use rays of pTimeDerivs as constraints for (tmp).
  //   Points have to be used as rays.
  int lRayNumber = 0;
  for(ddmRays::const_iterator 
    it = pTimeDerivs.getRays().begin();
      it != pTimeDerivs.getRays().end();
      ++it, ++lRayNumber)
  {
    // Construct a copy of the vector.
    ddmVec lCopy(*it);
    
    // Construct a ray from a point.
    lCopy[1] = 0;
    
    // For non-zero-time reaches, unidirectional
    //   rays are interpreted as strict.
    if(pNonZeroTime
       && lRayNumber >= pTimeDerivs.getRays().getStartOfUniRays())
    {
      // Make ray strict.
      lCopy[0] = -1;
    }
      
    // Normalize the ray.
    lCopy.normalize();

    tmp.addConstraint(lCopy);

    // If ray (it) is bidirectional, 
    //   deal with other direction.
    if(lRayNumber < pTimeDerivs.getRays().getStartOfUniRays())
    {
      // Construct a copy of the vector.
      ddmVec lCopy(*it);
      
      // Construct negation.
      lCopy = -lCopy;

      // Construct a ray from a point.
      lCopy[1] = 0;
      
      // Normalize the ray.
      lCopy.normalize();
      
      tmp.addConstraint(lCopy);
    }
  }

  // By duality, tmp.getRays() are the constraints we have to
  //   use for the result.  We construct it.  Here, we should
  //   use a ddmXPoly, since the standard
  //   restrictions are in effect again.
  ddmXPoly result(p1.getDim());

  result.addConstraints(tmp.getRays());

  return result;
}


ddmXPoly
ddmXPoly::mkTimeFollower(const ddmXPoly& pCurrent, 
             const ddmXPoly& pDerivs, 
             const ddmXPoly& pInvariant)
{
  // Look where passing time can lead us from the current region.
  ddmXPoly result(pCurrent);

  // The invariant must be fulfilled since we only look at 
  //   non-zero-time transitions.
  result.intersect(pInvariant);

  // If this intersection is empty, (mkTimeReach) does not
  //   add anything.

  // Check where time can lead us from here.
  // We allow zero-time followers.
  result = mkTimeReach(result, pDerivs, false);

  result.intersect(pInvariant);

  return result;
}

ddmXPoly
ddmXPoly::mkTimePrecursor(const ddmXPoly& pCurrent,
              const ddmXPoly& pDerivs,
              const ddmXPoly& pInvariant)
{
  // Compute negations of pDerivs.
  ddmXPoly lNegDerivs(pDerivs);
  lNegDerivs.changeSigns();
  
  // Look from where passing time can have led us to the current region.
  ddmXPoly result(pCurrent);
  
  // Non-zero-time-transitions can only lead here if the invariant
  //  has been fulfilled up to the preceding instant.
  result.intersect(pInvariant);
  
  // Compute from where passing non-zero-time can have led us here.
  // We allow zero-time followers.
  result = mkTimeReach(result, lNegDerivs, false);
  
  // The invariant must be fulfilled for nonzero time transitions.
  result.intersect(pInvariant);
  
  return result;
}

ddmXPoly*
ddmXPoly::mkTransitionFollower(const ddmXPoly& pCurrent,
                   const ddmXPoly& pAssignments,
                   const int pSync)
{
  // We start from the current region.
  ddmXPoly* result = new ddmXPoly(pCurrent);

  // Widen system for assignments: append further dimensions
  //   at the end.
  result->addDimensions(pCurrent.getDim(), pCurrent.getDim());

  // Assignments must be fulfilled.
  result->intersect(pAssignments);

  // Project out the old variables.  They are positioned 
  //   at the beginning.
  ddmXPoly* ltmpresult = mkProjection(result, 0, pCurrent.getDim());
  delete result;

  // Construct and set new trace.
  ltmpresult->setTrace(pCurrent.getTrace());
  ltmpresult->addToTrace(pSync);

  return ltmpresult;
}

ddmXPoly*
ddmXPoly::mkTransitionPrecursor(const ddmXPoly& pCurrent, 
                const ddmXPoly& pAssignments,
                const int pSync)
{
  // We start from the current region.
  ddmXPoly* result = new ddmXPoly(pCurrent);
  
  // Widen system for assignments: insert further dimensions
  //   at the beginning.
  result->addDimensions(0, pCurrent.getDim());
  
  // Assignments must be fulfilled.
  result->intersect(pAssignments);
  
  // Project out the old variables.  They are positioned at the end.
  ddmXPoly* ltmpresult = ddmXPoly::mkProjection(result, 
                        pCurrent.getDim(), 
                        2*pCurrent.getDim());
  
  delete result;
  // Construct and set new trace.
  ltmpresult->setTrace(pCurrent.getTrace());
  ltmpresult->addToTrace(pSync);

  return ltmpresult;
}  

bool
ddmXPoly::isEmpty() const
{
  // For the polyhedron to be nonempty,
  //   there must be a ray with ray[0] < 0, and
  //   there must be a ray with ray[1] > 0.
  
  // Only if at least one ray exists with ray[0] < 0
  //   there exists a solution of the given equation system,
  //   which obeys our special interpretation of the first constraint:
  //   Instead of (p <= 0) we interpret (p < 0).

  // Analogously we have a solution for (z = 1) only if
  //   there exists at least one ray with ray[1] > 0.

  bool ray0NegFound = false;
  bool ray1PosFound = false;
  
  for(ddmRays::const_iterator 
    it = this->getRays().begin();
      !(ray0NegFound && ray1PosFound) && it != this->getRays().end();
      ++it)
  {
    if(!ray0NegFound)
    {
      ray0NegFound = (*it)[0] < 0;
    }
    
    if(!ray1PosFound)
    {
      ray1PosFound = (*it)[1] > 0;
    }
  }
  
  return !(ray0NegFound && ray1PosFound);
}

void
ddmXPoly::print(ostream& s, const vector<string>* pVarNames) const
{
  s << "ddmXPoly:" << endl << "  Constraints:" << endl;
  
  // Print constraints.
  for(// Do not print the first two constraints: They are standard.
      ddmConstraints::const_iterator 
    it = this->getConstraints().begin()+ddmConstraints::mNumStdConstraints;
      it != this->getConstraints().end();
      ++it)
  {
    // Print constraint *it.
    for(int i = mNumExtraVars; i<this->getDim()+mNumExtraVars; ++i)
    {
      s << ((const ddmVec)(*it)) [i];
      if(pVarNames)
      {
    s << "*" << (*pVarNames)[i-mNumExtraVars];
      }
      s << " ";
      
      if(i < (this->getDim()+mNumExtraVars) - 1)
      {
    s << "+ ";
      }
    }

    // Print (in)-equality token.
    if( this->getConstraints().isEquality(it) )
    {
      s << "="; 
    }
    else
    {
      s << ( ( ((const ddmVec)(*it)) [0] > 0 ) ? ">" : ">=" ); 
    }

    s << " ";

    s << -((const ddmVec)(*it)) [1];

#ifdef DEBUG
    s << " -- " << (*it);
#endif

    s << endl;
  }

  s << "  Rays:" << endl;

  {
    // Print rays.
    for(ddmRays::const_iterator 
    it = this->getRays().begin();
    it != this->getRays().end();
    ++it)
    {
      // Print ray *it.

      // Prefix with "S" if ray can contribute
      //  to fulfilling strictness.
      if( ((const ddmVec)(*it)) [0] < 0 ) 
      {
    s << "S";
      }

      // Prefix with "B" for bidirectional rays.
      if( it < this->getRays().begin()+this->getRays().getStartOfUniRays() )
      {
    s << "B";
      }

      s << "(";
      for(int i = mNumExtraVars; i<this->getDim()+mNumExtraVars; ++i)
      {
    s << ((const ddmVec)(*it)) [i];

    if(i < (this->getDim()+mNumExtraVars) - 1)
    {
      s << " " ;
    }
      }
      s << ")";

    // Postfix for points.
      if( 0 != ((const ddmVec)(*it)) [1] )
      { 
    s << "/" << ((const ddmVec)(*it)) [1];
      }

      s << endl;
    }
  }

  // Print trace.
  {
    s << "Trace: ";

    {
      s << mTrace.size() << " elements:";
      for(vector<int>::const_iterator 
        it = mTrace.begin();
      it !=  mTrace.end();
      ++it)
      {
    s << *it;
      }
    }

    s << endl;
  }

  s << "End ddmXPoly" << endl;
}

// pDim is the dimension of the vector.
// If pLeft is (-1): output of variables with negative coefficients.
// If pLeft is (+1): output of variables with positive coefficients.
static void
printEquationSide(ddmAutomatonOstream& s, 
          int pDim, int pLeft,
          const ddmVec& pVec)
{
  bool lPrintedTerm = false;
  
  for(int i = 0; i<pDim; ++i)
  {
    reprNUMBER lCoeff = pLeft*pVec[i+ddmXPoly::mNumExtraVars];
    
    if(lCoeff>0)
    {
      if(lPrintedTerm)
      {
    // Connect next term with a plus.
    s << " + ";
      }
      else
      {
    lPrintedTerm = true;
      }
      
      // Print next term.
      {
    if(lCoeff == 1)
    {
      // no special prefix.
    }
    else
    {
      s << lCoeff << "*";
    }
    
    s << s.getVarIds()[i];
      }
    }
  }
  
  // Output of the constant term.
  // Check pVec[1], the absolute term.
  if( pLeft*pVec[1] > 0 )
  {
    if(lPrintedTerm)
    {
      s << " + ";
    }
    else
    {
      lPrintedTerm = true;
    }
    
    s << pLeft*pVec[1];
  }
  
  // Insert 0 if nothing was printed.
  if(!lPrintedTerm)
  {
    s << "0";
  }
}

ddmAutomatonOstream&
operator<<(ddmAutomatonOstream& s, const ddmXPoly& y)
{
  // Nothing has been printed until now.
  bool lNonePrinted = true;

  // Print one condition after the other.
  for(ddmConstraints::const_iterator 
    it = y.getConstraints().begin()+ddmConstraints::mNumStdConstraints;
      it != y.getConstraints().end();
      ++it)
  {
    // Check if current constraint (*it) belongs to an equality
    //   that already has been printed.  This is the case
    //   if BEFORE the current position, the negation 
    //   of the current constraint occurs.
    bool negationFound = false;

    // Construct negation once.
    ddmVec lNeg(-1*(*it));

    // Search for negation.
    for(ddmConstraints::const_iterator
      it1 = y.getConstraints().begin()+ddmConstraints::mNumStdConstraints;
    (!negationFound) && (it1 != it);
    ++it1)
    {
      negationFound = (*it1 == lNeg);
    }

    // Only proceed if negation has not been found.
    //   If a negation has been found, the current 
    //   constraint belongs to an equality which already
    //   has been printed.
    if(!negationFound)
    {
      // Print current (in)equality, if it is non-trivial.
      //  Check if there is at least one non-zero coefficient.
      bool lAllZero = true;
      for(int i=0;
      lAllZero && i<y.getDim();
      ++i)
      {
    lAllZero = ((*it)[i+ddmXPoly::mNumExtraVars] == 0);
      }

      // Only proceed if not all coefficients are zero.
      if(!lAllZero)
      {
    if(lNonePrinted)
    {
      // Now, we will have printed something.
      lNonePrinted = false;
    }
    else
    {
      s << " AND ";
    }

    // Print the left-hand-side: negations of all negative terms.
    printEquationSide(s,y.getDim(), -1,*it);

    // Check what kind of relation we have.
    if((*it)[0])
    {
      // Strict inequality.
      s << " < ";
    }
    else if(y.getConstraints().isEquality(it))
    {
      s << " = ";
    }
    else
    {
      s << " <= ";
    }

    // Print the right-hand-side: all positive terms.
    printEquationSide(s,y.getDim(), 1,*it);
      }
    }
  }

  if(lNonePrinted)
  {
    // No nontrivial constraints found.
    s << "TRUE";
  }

  return s;
}

#ifndef _ddmTransition_h_
#define _ddmTransition_h_


#include "ddmObject.h"
#include "ddmAutomatonOstream.h"
#include "ctaIntSet.h"
#include "ddmRegion.h"

class ddmTransition : private ddmObject
{
private:
  // It should be not allowed to use the standard operators.
  // Uses the generated copy constructor. 
  void operator=(const ddmTransition&);
  void operator,(const ddmTransition&);

private: // Private static methods.

private: // Attributes.

  // The precursor.
  int mPrecursor;

  // The follower.
  int mFollower;

  // The guard and the allowed changes.
  ddmRegion mAllowedGuardedAssignment;

  // We have to remember which variables did not occur primed in the
  // (syntactical) ALLOW clause.
  ctaIntSet mUnmentionedVars;

  // We have to flag if mInitiatedGuardedAssignments has already 
  //   been computed.
  bool mInitiatedGuardedAssignmentComputed;

  // The initiated changes.
  ddmRegion mInitiatedGuardedAssignment;

  // A synchronization signal on which this transition
  //   tries to sync. (-1) is the undefined sync label.
  int mSync;

public: // Constructors and destructor.

  ddmTransition(int pPrecursor,
		int pFollower,
		const ddmRegion& pAllowedGuardedAssignment,
		const ctaIntSet& pUnmentionedVars,
		const int pSync)
    : mPrecursor(pPrecursor),
      mFollower(pFollower),
      mAllowedGuardedAssignment(pAllowedGuardedAssignment),
      mUnmentionedVars(pUnmentionedVars),
      mInitiatedGuardedAssignmentComputed(false),
      mInitiatedGuardedAssignment(pAllowedGuardedAssignment.getDim()),
      mSync(pSync)
  {}
  
public: // Accessors.

  const int
  getPrecursor() const
  {
    return mPrecursor;
  }

  const int
  getFollower() const
  {
    return mFollower;
  }

  const ddmRegion&  
  getAllowedGuardedAssignment() const;

  const ctaIntSet&
  getUnmentionedVars() const;

  const ddmRegion&  
  getInitiatedGuardedAssignment() const;

  const int
  getSync() const
  {
    return mSync;
  }

  void
  setSync(const int pSync)
  {
    mSync = pSync;
  }

public: // Service methods.

public: // Friends.

public: // IO.

  friend ddmAutomatonOstream&
  operator<<(ddmAutomatonOstream& s, const ddmTransition & y);
};

// Local Variables:
// mode:C++
// End:

#endif // _ddmTransition_h_


#include "ddmTransition.h"

const ddmRegion&  
ddmTransition::getAllowedGuardedAssignment() const
{
  return mAllowedGuardedAssignment;
}

const ctaIntSet&
ddmTransition::getUnmentionedVars() const
{
  return mUnmentionedVars;
}

// If the initiated region is already computed then give it back,
//   otherwise compute it and store it in the object.
const ddmRegion&  
ddmTransition::getInitiatedGuardedAssignment() const
{
  if(!mInitiatedGuardedAssignmentComputed)
  {
    // This is a hack!!!
    //   We want to declare the current method as a const method
    //   because its changes in the object are not externally visible.
    //   But we have to change this object for caching purposes.
    //   We only change (mInitiatedGuardedAssignmentComputed) and
    //   (mInitiatedGuardedAssignment).
    //   For this we use the pointer (lThis).
    ddmTransition* lThis = const_cast<ddmTransition*>(this);
    
    // We compute the region for initiated changes.
    lThis->mInitiatedGuardedAssignmentComputed = true;
    
    // Build the initial region for initiated changes.
    // The restrictions from allowed must also be in
    // the initiated section!
    // At first we have to copy the restrictions in mAllowedChanges.
    lThis->mInitiatedGuardedAssignment = mAllowedGuardedAssignment;
    
    // Then we have to take all unmentioned variables. For each
    // such variable (x) we have to insert a restriction of the form (x' = x)
    // conjunctively.
    
    for( ctaIntSet::const_iterator it = this->getUnmentionedVars().begin();
 	 it != this->getUnmentionedVars().end();
 	 ++it)
    {
      // (*it) is an index of a variable (and not a constant)
      // which does not occur ticked in ALLOW.
      // We have to build a region to restrict this variable: it
      // may not change in initiated transition.
      
      // We build the region for (x = x') !!!
      
      // We need the count of variables in this automaton.
      int n = mAllowedGuardedAssignment.getDim() / 2;
      
      ddmVec lTmpVec;
      // We compute the constraint (ddmVec) to build the region
      //   for x >= x' ...
      lTmpVec[ddmXPoly::mNumExtraVars + *it] = 1;
      lTmpVec[ddmXPoly::mNumExtraVars + n + *it] = -1;
      ddmRegion lRegion1(mAllowedGuardedAssignment.getDim(), lTmpVec);
      
      // .. and now for x' >= x.
      lTmpVec[ddmXPoly::mNumExtraVars + *it] = -1;
      lTmpVec[ddmXPoly::mNumExtraVars + n + *it] = 1;
      ddmRegion lRegion2(mAllowedGuardedAssignment.getDim(), lTmpVec);
      
      // Intersection with both regions gives the restriction
      // for x = x'.
      lThis->mInitiatedGuardedAssignment.intersect(lRegion1);
      lThis->mInitiatedGuardedAssignment.intersect(lRegion2);
    }
  }
  
  return mInitiatedGuardedAssignment;
}

ddmAutomatonOstream&
operator << (ddmAutomatonOstream& s, const ddmTransition& y)
{

  s << "      TRANS "     << " {" << endl;

  if( y.getSync() != -1 )
  {
    s << "        SYNC #" << s.getSyncIds()[y.getSync()] << endl;
  }
  s << "        INITIATED " << endl;
  s << "          ";
  s << y.getInitiatedGuardedAssignment() << ";" << endl;

  s << "        ALLOWED " << endl;
  s << "          ";
  s << y.getAllowedGuardedAssignment() << ";" << endl;
  
  s << "      GOTO " << s.getStateIds()[y.getFollower()] << endl;

  s << "      }" << endl;

  
  return s;
}

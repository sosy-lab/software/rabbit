#ifndef _ddmPoly_h_
#define _ddmPoly_h_


#include "ddmConstraints.h"
#include "ddmRays.h"
#include "ddmVec.h"

// Double description representation of a polyhedron.
class ddmPoly : private ddmObject
{
private:
  // It should be not allowed to use the standard operators.
  // Uses the standard operator '='.
  void operator,(const ddmPoly&);

public: // Static methods.

  // Compute projection by existential quantification
  //   of variable from number (pFrom) up to but not including
  //   (pUpTo).
  static ddmPoly*
  mkProjection(const ddmPoly* p, int pFrom, int pUpTo, bool pSpecial = false);

protected: // Attributes.

  // Dimension.
  int mDim;

  // A matrix for the constraints.
  ddmConstraints mConstraints;

  // A matrix for the rays.
  ddmRays mRays;

public: // Constructors and destructor.

  explicit ddmPoly(int pDim)
    : mDim(pDim),
      mConstraints(),
      mRays(pDim)
  {}

  ddmPoly(int pDim, const ddmConstraints& pConstraints)
    : mDim(pDim),
      mConstraints(),
      mRays(pDim)
  {
    this->addConstraints(pConstraints);
  }

  // Use a rays-structure as constraints.
  ddmPoly(int pDim, const ddmRays& pRays)
    : mDim(pDim),
      mConstraints(),
      mRays(pDim)
  {
    this->addConstraints(pRays);
  }

  ddmPoly(const ddmPoly& p)
    : mDim(p.mDim),
      mConstraints(p.mConstraints),
      mRays(p.mRays)
  {}

  ddmPoly()
    : mDim(0),
      mConstraints(),
      mRays()
  {}

public: // Accessors.

  int
  getDim() const
  {
    return mDim;
  }

  const ddmConstraints&
  getConstraints() const
  {
    return mConstraints;
  }

  const ddmRays&
  getRays() const
  {
    return mRays;
  }

public: // Service methods.

  // Check if (*this) contains (pPoly).
  bool
  contains(const ddmPoly& pPoly) const;

  void
  intersect(const ddmPoly& p)
  {
    this->addConstraints( p.getConstraints() );
  }
  
  //Intersect with sorted Constraints
  // First sort all constraints and then build the rays.
  void
  intersect_sort(const ddmPoly& p)
  {
    this->addConstraintsSorted( p.getConstraints());
  }

  void
  addConstraint(const ddmVec& p);

  void
  addConstraints(const ddmConstraints& p, int pStartingAt = 0);
  
  // Insert the constraints sorted and then build the rays.
  void addConstraintsSorted(const ddmConstraints& p, int pStartingAt = 0);

  void
  addConstraints(const ddmRays& p);

  // Remove given dimensions from the polyhedron,
  //   not including (pUpTo).
  void
  removeDimensions(int pFrom, int pUpTo);

  // Add (pNum) dimensions before (pAT).
  void
  addDimensions(int pAt, int pNum)
  {
    // Update (mDim).
    mDim += pNum;

    // Add dimensions to constraints and rays.
    mConstraints.addDimensions(pAt, pNum);
    mRays.addDimensions(pAt, pNum);
  }

  void
  addStdConstraints()
  {
    // We have two extra variables in a ddmXPoly.
    //   First variable is for expressing strictness.
    //   Second variable is for constants.

    // 1st var should be nonpositive. ( p < 0 )
    //   Strictness results from special interpretation
    //   of this constraint.
    this->addConstraint( ddmVec("-1") );

    // 1st var should be larger than or equal to -1. ( p >= -1 )
    //   For optimization purpose.
    this->addConstraint( ddmVec("1 1") );

    // 2nd var should be positive. ( z = 1 )
    //   The virtual variable for constant representation
    //   should be 1 all the time but this is not expressable
    //   with half spaces passing through the origin (Ursprung).
    //   The inequality (z >= 0) needs special interpretation
    //   as (z = 1). For output, input, etc.
    this->addConstraint( ddmVec("0 1") );
  }

public: // Friends.

  friend bool
  operator == (const ddmPoly& p1, const ddmPoly& p2)
  {
    return p1.contains(p2) && p2.contains(p1);
  }

  friend bool
  operator != (const ddmPoly& p1, const ddmPoly& p2)
  {
    return !(p1 == p2);
  }

public: // IO.

  friend ostream&  
  operator << (ostream& s, const ddmPoly& y)
  {
    // Print number of rows.
    s << "Poly: dim " << y.getDim() << endl;

    // Print both constraints and rays.
    s << y.getConstraints();
    s << y.getRays();

    s << "End poly" << endl;

    return s;
  }
};

// Local Variables:
// mode:C++
// End:

#endif // _ddmPoly_h_

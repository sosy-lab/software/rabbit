// A library functions needs this, and it is missing.
//  Hope the arguments and return value are what is expected.
#include <mem.h>

void bzero(void *pAdress, int pCount)
{
  memset(pAdress,0,pCount);
}

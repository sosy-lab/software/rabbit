#ifndef _ddmTraces_h_
#define _ddmTraces_h_


#include <map>
#include <set>

#include "ddmObject.h"
#include "ddmAutomatonOstream.h"

class ddmTraces: public map<int, ddmTraces*>, private ddmObject
{
private:
  // It should be not allowed to use the standard operators.
  // Uses the standard copy constructor.
  // Uses the standard operator '='.
  void operator,(const ddmTraces&);

public: // Public static methods.

  // Construct a ddmTraces object representing one empty trace.
  static ddmTraces
  mkEmptyTrace()
  {
    return ddmTraces();
  }

  // Construct a ddmTraces object representing the empty set.
  static ddmTraces
  mkEmptySet()
  {
    return mkSequence(-1,ddmTraces());
  }

  // Construct a sequence.
  static ddmTraces
  mkSequence(int pPrefix, const ddmTraces& pPostfix);

  static ddmTraces
  mkUnion(const ddmTraces& p1, const ddmTraces& p2);

private: // Attributes.

public: // Constructors and destructor.

  // Construct empty ddmTraces.
  ddmTraces()
    : map<int, ddmTraces*>()
  {}

public: // Accessors.

public: // Service methods.

  bool
  isEmptySet() const
  {
    // (*this) is the empty set if -1 occurs as key.
    return (this->find(-1) != this->end());
  }

public: // Friends.

public: // IO.

  friend ostream&
  operator << (ostream& s, const ddmTraces& y);

  friend ddmAutomatonOstream&
  operator << (ddmAutomatonOstream& s, const ddmTraces& y);
};


// Local Variables:
// mode:C++
// End:

#endif // _ddmTraces_h_

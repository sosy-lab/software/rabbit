#ifndef _ddmRCObject_h_
#define _ddmRCObject_h_

// This is super class for classes with reference counting..


class ddmRCObject
{
private:
  // It should be not allowed to use the standard operators.
  ddmRCObject(const ddmRCObject&);
  void operator=(const ddmRCObject&);
  void operator,(const ddmRCObject&);

public:
  void addReference()        { ++mRefCount; }

  void removeReference()
  {
    if(--mRefCount == 0) delete this;
    if(mRefCount < 0)
    {
      cerr << "Runtime error: memory deallocation of non-existing ddmRCObject."
	   << endl;
    }
  }

  void markUnshareable()
  {
    mSharable = false;
  }

  bool isShareable() const   { return mShareable; }

  bool isShared() const      { return mRefCount > 1; }

protected:
  ddmRCObject()
    : mRefCount(0),
      mShareable(true)
  {}

  ddmRCObject(const ddmRCObject& pRhs)
    : mRefCount(0),
      mShareable(true)
  {}

  ddmRCObject& operator=(const ddmRCObject& pRhs)  { return *this; }

  virtual ~ddmRCObject() {}

private:
  int mRefCount;
  bool mSharable;
};

// Local Variables:
// mode:C++
// End:

#endif // _ddmRCObject_h_

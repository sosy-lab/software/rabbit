#include "reprNUMBER.h"
#include "ddmVecSTL.h"

ddmVecSTL::ddmVecSTL(const char *p)
  : mContent()
{
  int i;
  istrstream istr(p);
    
  // Get one number after the other and check success.
  while( (void*)(istr >> i) )
  {
    mContent.push_back((reprNUMBER) i);
  }
}
 
// Check if argument is equal to (*this), starting from given index.
bool
ddmVecSTL::equalFromIndex(int pStartIndex, const ddmVecSTL& pVec) const
{
  bool result = true;
 
  // Compute lower range for indices to check.
  const int lMinSize = (int) min(this->size(), pVec.size());

  // Check and stop when inequality has been found.
  for(int i = pStartIndex; result && (i < lMinSize); ++i)
  {
    // Check values at current index (i).
    result = (this->mContent[i] == pVec.mContent[i]);
  }
  
  // Compare residual elements of vector pVec with zero.
  for(int j = lMinSize; result && (j < pVec.size()); ++j)
  {
    result = (0 == pVec.mContent[j]);
  }
  
  // Compare residual elements of (this) vector with zero.
  for(int k = lMinSize; result && (k < this->size()); ++k)
  {
    result = (0 == this->mContent[k]);
  }
  
  return result;
}

// Scale all components of a vector by a common factor.
void
ddmVecSTL::scale(const reprNUMBER& pNum)
{
  for(vector<reprNUMBER>::iterator it = this->mContent.begin();
      it != this->mContent.end();
      ++it)
  {
    (*it) *= pNum;
  }
}

// Descale all components of a vector by a common factor.
void
ddmVecSTL::descale(const reprNUMBER& pNum)
{
  for(vector<reprNUMBER>::iterator it = this->mContent.begin();
      it != this->mContent.end();
      ++it)
  {
    (*it) /= pNum;
  }
}

// Add (pAdd) to current vector.
void
ddmVecSTL::add(const ddmVecSTL& pAdd)
{
  // Compute lower range of indices.
  const int lMinSize = (int) min(this->size(), pAdd.size());

  // Add components up to the current length of shortest vector.
  for(int i = 0; i < lMinSize; ++i)
  {
    this->mContent[i] += pAdd.mContent[i];
  }

  // Append residual elements of (pAdd).
  for(int j = this->size(); j < (int) pAdd.size(); ++j)
  {
    this->mContent.push_back(pAdd.mContent[j]);
  }
}

// Add (pFactor * pAdd) to current vector.
void
ddmVecSTL::add(const reprNUMBER& pFactor, const ddmVecSTL& pAdd)
{
  // Compute lower range of indices.
  const int lMinSize = (int) min(this->size(), pAdd.size());

  // Add components up to the current length of shortest vector.
  for(int i = 0; i < lMinSize; ++i)
  {
    this->mContent[i] += pFactor * pAdd.mContent[i];
  }

  // Append residual elements of (pAdd).
  for(int j = this->size(); j < (int) pAdd.size(); ++j)
  {
    this->mContent.push_back(pFactor * pAdd.mContent[j]);
  }
}


// Compute positive greatest common divisor.
//   Only consider components with index starting
//   at pStartPos.
reprNUMBER
ddmVecSTL::gcd() const
{
  // Construct in (v) a copy of (*this) to work on.
  ddmVecSTL v(*this);
  reprNUMBER result;
  reprNUMBER minVal, newMinVal;
  int minValIndex = -1, newMinValIndex;

  // Make all components of v positive.
  for(int i = 0; i < (int) v.size(); ++i)
  {
    if(v.mContent[i] < 0)
    {
      v.mContent[i] *= -1;
    }
  }

  // Determine minimal nonzero value in (minVal).
  {
    minVal = 0;

    for(int i = 0; i < (int) v.size(); ++i)
    {
      if(   (minVal == 0)
	    || ((v.mContent[i] != 0) && (v.mContent[i] < minVal))
	    )
      {
	minVal = v.mContent[i];
	minValIndex = i;
      }
    }
  }

  // Check if there were any nonzero values.
  if(minVal == 0)
  {
    // No nonzero values!
    result = 0;
  }
  else
  {
    // There is a nonzero value in the vector.

    // May loop finish?
    bool ready = false;
      
    // On entry to while-loop, (newMinVal)
    //   must contain the next divisor.
    newMinVal = minVal;
    newMinValIndex = minValIndex;
      
    // Euclidean algorithm.
    while(! ready)
    {
      // Store current divisor in minVal.
      minVal = newMinVal;
      minValIndex = newMinValIndex;
	
      // Compute next divisor in newMinVal;
      newMinVal = 0;
	
      // Compute modulus of all components by minVal.
      for(int i = 0; i < (int) v.size(); ++i)
      {
	if(i != minValIndex)
	{
	  v.mContent[i] %= minVal;
	  
	  // Compute smallest non-zero modulus in (newMinVal).
	  if(   (newMinVal == 0)
		|| ((v.mContent[i] != 0) && (v.mContent[i] < newMinVal)) 
		)
	  {
	    newMinVal = v.mContent[i];
	    newMinValIndex = i;
	  }
	}
      }
	
      // We are ready when there was no non-zero modulus.
      ready = (0 == newMinVal);
    }
      
    // The result is in (minVal).
    result = minVal;
  }

  return result;
}

// Divide all components by positive GCD and strip trailing zeros.
void
ddmVecSTL::normalize()
{
  reprNUMBER lGcd = this->gcd();

  if(lGcd > 1)
  {
    this->descale(lGcd);
  }
}

// Add (pNum) dimensions before (pAT).
void
ddmVecSTL::addDimensions(unsigned int pAt, unsigned int pNum)
{
  // Insert zeros into the vector.

  // We only have to do something if the vector
  //   extends longer than the position where the
  //   new dimensions are to be inserted.
  if(pAt < (unsigned int) this->size())
  {
    // Insert (pNum) zeros at position (pAt).
    mContent.insert(mContent.begin() + pAt, pNum, 0);
  }
}

void
ddmVecSTL::removeDimensions(unsigned int pFrom, unsigned int pUpTo)
{
  // Compute the number of elems to erase.
  int lElemsToErase = min(((int)this->size()) - (int)pFrom, 
			  (int)(pUpTo - pFrom));
  
  if(lElemsToErase > 0)
  {
    // Erase (lElemsToErase) elems starting from (pFrom).
    mContent.erase(mContent.begin() + pFrom, 
		   mContent.begin() + (pFrom + lElemsToErase));
  }

  // Normalize;
  this->normalize();
}

void
ddmVecSTL::changeSigns(bool pIgnoreFirst)
{
  vector<reprNUMBER>::iterator it;

  // Compute iterator to first number to process.
  {
    // Init (it) to the start of the vector.
    it = mContent.begin();

    // If we have to ignore the first element,
    //   and there is at least one element in the vector:
    //   move to second.
    if(pIgnoreFirst && this->size() > 0)
    {
      ++it;
    }
  }

  for(/* Initialization already done */ ;
      it != mContent.end();
      ++it)
  {
    (*it) *= -1;
  }
}

ddmVecSTL
operator + (const ddmVecSTL& v1, const ddmVecSTL& v2)
{
  //  int lSizeV1 = v1.size();
  //  int lSizeV2 = v2.size();
  int lMinSize = min(v1.size(), v2.size());
  int lMaxSize = max(v1.size(), v2.size());

  ddmVecSTL result(lMaxSize);

  for(int i = 0; i < lMinSize; ++i)
  {
    result.mContent[i] = v1.mContent[i] + v2.mContent[i];
  } 

  // Append residual elements of longer vector to result.
  for(int j = lMinSize; j < v1.size(); ++j)
  {
    result.mContent[j] = v1.mContent[j];
  }

  for(int k = lMinSize; k < v2.size(); ++k)
  {
    result.mContent[k] = v2.mContent[k];
  }

  return result;
}

ddmVecSTL
operator - (const ddmVecSTL& v1, const ddmVecSTL& v2)
{
  //  int lSizeV1 = v1.size();
  //  int lSizeV2 = v2.size();
  int lMinSize = min(v1.size(), v2.size());
  int lMaxSize = max(v1.size(), v2.size());

  ddmVecSTL result(lMaxSize);

  for(int i = 0; i < lMinSize; ++i)
  {
    result.mContent[i] = v1.mContent[i] - v2.mContent[i];
  }
  
  // Append residual elements of longer vector v1 to result.
  for(int j = lMinSize; j < v1.size(); ++j)
  {
    result.mContent[j] = v1.mContent[j];
  }
  
  // Append negative value of residual elements of longer vector
  //  v2 to result.
  for(int k = lMinSize; k < v2.size(); ++k)
  {
    result.mContent[k] = -v2.mContent[k];
  }

  return result;
}

ddmVecSTL
operator - (const ddmVecSTL& v1)
{
  ddmVecSTL result((int) v1.size());

  for(int i = 0; i < (int) result.size(); ++i)
  {
    result.mContent[i] = -1 * v1.mContent[i];
  } 

  return result;
}

ddmVecSTL
operator * (const reprNUMBER& n, const ddmVecSTL& v)
{
  ddmVecSTL result((int) v.size());

  for(int i = 0; i < (int) result.size(); ++i)
  {
    result.mContent[i] = n * v.mContent[i];
  } 

  return result;
}

ddmVecSTL
operator / (const ddmVecSTL& v, const reprNUMBER& n)
{
  ddmVecSTL result((int) v.size());

  for(int i = 0; i < (int) result.size(); ++i)
  {
    result.mContent[i] = v.mContent[i] / n;
  } 

  return result;
}

reprNUMBER
operator * (const ddmVecSTL& v1, const ddmVecSTL& v2)
{
  int lMinSize = min(v1.size(), v2.size());

  reprNUMBER result = 0;

  for(int i = 0; i < lMinSize; ++i)
  {
    result += v1.mContent[i] * v2.mContent[i];
  } 

  return result;
}

bool
operator == (const ddmVecSTL& v1, const ddmVecSTL& v2)
{
  bool result = true;
  //  int lSizeV1 = v1.size();
  //  int lSizeV2 = v2.size();

  // Compute lower range for indices to check.
  int lMinSize = min(v1.size(), v2.size());

  // Check and stop when inequality has been found.
  for(int i = 0; result && (i < lMinSize); ++i)
  {
    result = (v1.mContent[i] == v2.mContent[i]);
  }
    
  // Compare residual elements of vector v1 with zero.
  for(int j = lMinSize; result && (j < v1.size()); ++j)
  {
    result = (0 == v1.mContent[j]);
  }

  // Compare residual elements of vector v2 with zero.
  for(int k = lMinSize; result && (k < v2.size()); ++k)
  {
    result = (0 == v2.mContent[k]);
  }

  return result;
}

istream&  
operator >> (istream& s, ddmVecSTL& y)
{
  int lSize;

  // Read vector size.
  s >> lSize;

  // Read vector.
  {
    ddmVecSTL v(lSize);

    for(int i = 0; i < lSize; ++i)
    {
      s >> v.mContent[i];
    }

    y = v;

    return s;
  }
}

ostream&  
operator << (ostream& s, const ddmVecSTL& y)
{
  //  int lSize = y.size();

  s << y.size() << ":";

  for(int i = 0; i < y.size(); ++i)
  {
    s << " " << y.mContent[i];
  }

  return s;
}

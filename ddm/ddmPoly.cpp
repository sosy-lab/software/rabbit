
#include "ddmPoly.h"
// For sorting the constraints we need a list.
#include <list>

// Compute projection by existential quantification
//   of variable from number (pFrom) up to but not including
//   (pUpTo).
ddmPoly*
ddmPoly::mkProjection(const ddmPoly* p, int pFrom, int pUpTo, bool pSpecial)
{
  // Generate a full polyhedron.
  ddmPoly lDual(p->getDim());
  
  // Add bidirectional rays for dimensions (pFrom) up to but 
  //   not including (pUpTo).
  for(int i = pFrom;
      i != pUpTo;
      ++i)
  {
    // Add a positive and a negative ray to the dual.
    lDual.addConstraint(ddmVec::mkPosDimVec(i));
    lDual.addConstraint(-1 * ddmVec::mkPosDimVec(i));
  }
  
  // Add the rays of (p) as constraints.
  lDual.addConstraints(p->getRays());
  
  // Remove dimensions from the dual polyhedron.
  lDual.removeDimensions(pFrom, pUpTo);
  
  // Result is the dual of (lDual).
  
  // Generate a full space of reduced dimension.
  ddmPoly* result = new ddmPoly(lDual.getDim());

  // Do we need the special handling for
  //   strict inequalities and points?
  if(pSpecial)
  {
    result->addStdConstraints();
  }
  
  // Add rays of dual to constraints of result.
  result->addConstraints(lDual.getRays());
  
  return result;
}

// Check if (*this) contains (pPoly).
bool
ddmPoly::contains(const ddmPoly& pPoly) const
{
  bool result = true;

  // (*this) contains (pPoly) if all rays in pPoly
  //   fulfill all the constraints of (*this).

  for(ddmConstraints::const_iterator 
	cIt = this->getConstraints().begin();
      result && (cIt != this->getConstraints().end());
      ++cIt)
  {
    result = pPoly.getRays().fulfill(*cIt);
  }

  return result;
}

void
ddmPoly::addConstraint(const ddmVec& p)
{
  // Only nonempty vectors are added.
  if(p.size() != 0)
  {
    // Check if the new constraint is already in mConstraints,
    //   and also check if it part of an equation.
    bool lIsNew;

    mConstraints.addConstraint(p, lIsNew);

    if(lIsNew)
    {
      mRays.addConstraint(p, this->getDim(), mConstraints.countEqualities());
    }
  }
}

void
ddmPoly::addConstraints(const ddmConstraints& p, int pStartingAt/* = 0*/)
{
  ddmConstraints::const_iterator it;
  
  for(it = p.begin() + pStartingAt;
      it != p.end();
      ++it)
  {
    this->addConstraint(*it);
  }
}

// With this operator the constraints will be sorted.
bool 
operator > (const ddmVec& t1, const ddmVec& t2)
{
  return t1.countNonZeros() > t2.countNonZeros();
}

// Before the rays were build the constraints are sorted with the 
//   operator above.
void
ddmPoly::addConstraintsSorted(const ddmConstraints& p, int pStartingAt/* =0*/)
{
  //ddmConstraints::const_iterator it;
  list<ddmVec> ltmpToSort;
  
  // First add all new constraints into a temporary vector.
  ltmpToSort.insert(ltmpToSort.end(), p.begin() + pStartingAt, p.end());
  /*
  for(it = p.begin() + pStartingAt; it != p.end(); ++it)
  {
    // Only nonempty vectors are added.
    if (it->size() != 0)
    {
      ltmpToSort.push_back(*it);
    }
    }*/

  // Insert the Constraints of the (this) Poly into a temporary vector.
  /*
  for (it = mConstraints.begin() + pStartingAt; it != mConstraints.end(); ++it)
  {
    // Only nonempty vectors are added.
    if (it->size() != 0)
    {
      ltmpToSort.push_back(*it);
    }
  }
*/
  /*  ltmpToSort.insert(ltmpToSort.end(), mConstraints.begin() + pStartingAt,
      mConstraints.end());*/
  // Sort all constraints.
  ltmpToSort.sort();

  // Insert the standardconstraints at front of the sorted array.
  /*  it = mConstraints.begin() + pStartingAt;
  do
  {
    --it;
    ltmpToSort.push_front(*it);
    }while (it != mConstraints.begin());*/

  // Delete the old constraints and Equalities.
  //  mConstraints.clear();
  //mConstraints.mEqualities.clear();

  // Build a full space.
  //mRays = ddmRays(this->getDim());
  
  // Insert the constraints and build the rays.
  list<ddmVec>::const_iterator it1;  
  for(it1 = ltmpToSort.begin(); it1 != ltmpToSort.end(); ++it1)
  {
    this->addConstraint(*it1);
  }
}

void
ddmPoly::addConstraints(const ddmRays& p)
{
  ddmRays::const_iterator it;
  
  for(it = p.begin();
      it != p.end();
      ++it)
  {
    // Add current ray.
    this->addConstraint(*it);
    
    // For bidirectional rays, also add negative version of the ray.
    if(it < p.begin()+p.getStartOfUniRays())
    {
      this->addConstraint(-1 * (*it));
    }
  }
}

  
// Remove given dimensions from the polyhedron,
//   not including (pUpTo).
void
ddmPoly::removeDimensions(int pFrom, int pUpTo)
{
  // Update dimension counter.
  mDim -= pUpTo-pFrom;
  
#ifdef DEBUG
  cout << "ddmPoly::removeDimensions: about to remove dims in mConstraints.\n";
#endif
  
  // Remove dimensions from both constraints and rays.
  mConstraints.removeDimensions(pFrom, pUpTo);
  
#ifdef DEBUG
  cout << "ddmPoly::removeDimensions: removed dims in mConstraints.\n";
#endif
  
  mRays.removeDimensions(pFrom, pUpTo);
  
#ifdef DEBUG
  cout << "ddmPoly::removeDimensions: removed dims in mRays.\n";
#endif
}


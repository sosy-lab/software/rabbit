#ifndef _ddmVecPlain_h_
#define _ddmVecPlain_h_


#include <iostream>
#include <strstream>
// #include <vector>

#include "reprNUMBER.h"
#include "ddmObject.h"

// A class for numerical vectors.

//const int DEFAULT_SIZE = 10;

class ddmVecPlain : private ddmObject
{
private:
  // It should be not allowed to use the standard operators.
  // Uses the standard operator '='.
  void operator,(const ddmVecPlain&);

public: // Static methods.

  // Construct a vector pointing to the 
  // positive direction of dimension (pDim).
  static ddmVecPlain
  mkPosDimVec(int pDim)
  {
    ddmVecPlain result(pDim + 1);

    result[pDim] = 1;

    return result;
  }


private: // Attributes.

  reprNUMBER* mContent; // content of numerical vector
  int mSize;  // dimension of vector 

public:

  ddmVecPlain();            // default_size

  explicit 
  ddmVecPlain(int pLength); // user defined size

  ddmVecPlain(const ddmVecPlain& v);

  explicit 
  ddmVecPlain(const char *p);

public: // Accessors.

  reprNUMBER& 
  operator[](int pIndex)
  {
    
    if (pIndex >= this->size())        
    {
      reprNUMBER* lTmpContent = new reprNUMBER[pIndex + 1];

      // Copy elements to new array.
      for(int i = 0; i < this->size(); ++i)
      {
	lTmpContent[i] = mContent[i];
      }

      // Set new elements to zero.
      for(int j = this->size(); j < pIndex; ++j)
      {
      	lTmpContent[j] = 0;
      }  
      
      mContent = lTmpContent;
      mSize = pIndex + 1;

    }    
    return mContent[pIndex];
  }

  const reprNUMBER
  operator[](int pIndex) const
  {
    reprNUMBER lResult;

    if (pIndex >= this->size())
    {
      lResult = 0;
    }
    else
    {
      lResult = mContent[pIndex];
    }

    return lResult;
  }

  int 
  size() const
  {
    return mSize;
  }

  bool
  isEmpty() const
  {
    return (0 == this->size());
  }
  
public: // Service methods.

  // Check if argument is equal to (*this), starting from given index.
  bool
  equalFromIndex(int pStartIndex, 
                 const ddmVecPlain& pVec) const;

  // Scale all components of a vector by a common factor.
  void
  scale(const reprNUMBER& pNum);
  
  // Descale all components of a vector by a common factor.
  void
  descale(const reprNUMBER& pNum);

  // Add (pAdd) to current vector.
  void
  add(const ddmVecPlain& pAdd);
  
  // Add (pFactor * pAdd) to current vector.
  void
  add(const reprNUMBER& pFactor, const ddmVecPlain& pAdd);
  
  // Compute positive greatest common divisor.
  //   Only consider components with index starting
  //   at pStartPos.
  reprNUMBER
  gcd() const;

  // Divide all components by positive GCD and strip trailing zeros.
  void
  normalize();
  
  // Swap (*this) with pVec.
  void
  swap(ddmVecPlain& pVec);
    
  // Remove elems from index (pFrom) to (pUpTo-1)
  // Elem at position pUpTo will not be removed!!!
  void
  removeDimensions(int pFrom, int pUpTo);

  // Add (pNum) dimensions before (pAT).
  void
  addDimensions(int pAt, int pNum);
  
  // change sign of vector elems, but ignore first if pIgnoreFirst == true
  void
  changeSigns(bool pIgnoreFirst);

public: // Friends.

  friend bool
  operator < (const ddmVecPlain& v1, const ddmVecPlain& v2);
  
  friend ddmVecPlain
  operator + (const ddmVecPlain& v1, const ddmVecPlain& v2);

  friend ddmVecPlain
  operator - (const ddmVecPlain& v1, const ddmVecPlain& v2);

  friend ddmVecPlain
  operator - (const ddmVecPlain& v1);

  friend ddmVecPlain
  operator * (const reprNUMBER& n, const ddmVecPlain& v);

  friend ddmVecPlain
  operator / (const ddmVecPlain& v, const reprNUMBER& n);

  friend reprNUMBER
  operator * (const ddmVecPlain& v1, const ddmVecPlain& v2);

  friend bool
  operator == (const ddmVecPlain& v1, const ddmVecPlain& v2);

  friend bool
  operator != (const ddmVecPlain& v1, const ddmVecPlain& v2)
  {
    return !(v1 == v2);
  }
 
public: // IO.

  friend istream&  
  operator >> (istream& s, ddmVecPlain& y);

  friend ostream&  
  operator << (ostream& s, const ddmVecPlain& y);

};

// Local Variables:
// mode:C++
// End:

#endif // _ddmVecPlain_h_

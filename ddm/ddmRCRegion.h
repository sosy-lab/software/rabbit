#ifndef _ddmRCRegion_h_
#define _ddmRCRegion_h_


Heinrich und Dirk am 23.9.99:
Klasse ddmRCRegion, die von ddmRCIPtr<ddmRegion> erbt
und zunaechst leer bleibt.
Spaeter koennen dann die Operationen
von ddmRegion hier implementiert werden und vor
dem Aufruf der "richtigen" Operation einige
Sonderfaelle abfangen, bei denen ein Kopieren
nicht notwendig wird.

//typedef ddmRCIPtr<ddmRegion> ddmRCRegion;
// Advantage: We do not need to implement the member functions.
// But what is with static procedures?
// We have to implement the operator ->
// with copy on write!

or the following?

// Advantage: We do not need to change application to pointer notation.
// But we have to implement all the members again.
// Advantage: We know, which functions we have
// prepared for RC usage!!!
class ddmRCRegion
{
private:
  // It should be not allowed to use the standard operators.
  ddmRCRegion(const ddmRCRegion&);
  void operator=(const ddmRCRegion&);
  void operator,(const ddmRCRegion&);

public:
  // Constructors (only specials are needed).

  // Destructor not needed.

  // Service methods: (Copy on write handling.)

private:
  ddmRCIPtr<ddmRegion> mValue;
};

// Local Variables:
// mode:C++
// End:

#endif // _ddmRCRegion_h_

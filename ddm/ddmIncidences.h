#ifndef _ddmIncidences_h_
#define _ddmIncidences_h_


#include "ddmObject.h"

#include <vector>

// A class for the representation of incidence vectors.
class ddmIncidences : private ddmObject
{
private:
  // It should be not allowed to use the standard operators.
  // Uses the generated copy constructor. 
  void operator,(const ddmIncidences&);

public: // Static methods.

  static bool
  subset(const ddmIncidences& p1, const ddmIncidences& p2);

private: // Static attributes.

  static const int mBitsPerInt;// = 8*sizeof(unsigned int);

private: // Attributes.

  vector<unsigned int> mVec;

public: // Constructors and destructor.
  ddmIncidences()
    : mVec(0)
  {}

  explicit ddmIncidences(int pSize)
    : mVec((pSize-1)/mBitsPerInt + 1)
  {}

public: // Accessors.

  void
  setBit(int i);

  bool
  operator[](int i) const;

public: // Service methods.

  // Size in ints.
  int
  intSize() const
  {
    return mVec.size();
  }
  
  // Size in bits.
  int
  bitSize() const
  {
    return this->intSize()*mBitsPerInt;
  }

  void
  swap(ddmIncidences& pInc)
  {
    this->mVec.swap(pInc.mVec);
  }

  // Return the number of ones in the incidence vector.
  unsigned int
  countOnes() const;

public: // Friends.

  friend ddmIncidences
  operator &(const ddmIncidences& p1, const ddmIncidences& p2);

  friend ddmIncidences&
  operator &= (ddmIncidences& p1, const ddmIncidences& p2);

  friend ostream&  
  operator << (ostream& s, const ddmIncidences& y);

};

// Local Variables:
// mode:C++
// End:

#endif // _ddmIncidences_h_

//-*-Mode: C++;-*-

/*
 * File:        ddmObject.h
 * Purpose:     Top Level Object for ddm library
 * Author:      Dirk Beyer
 * Created:     1999
 * Copyright:   (c) 1999, University of Brandenburg, Cottbus
 */

#ifndef _ddmObject_h
#define _ddmObject_h

#include "ctaObject.h"

// This is the base class for any other class in the analysis library.
class ddmObject : private ctaObject
{};

#endif

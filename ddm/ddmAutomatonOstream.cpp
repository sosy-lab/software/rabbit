
#include "ddmAutomatonOstream.h"

#include <iostream>
#include <set>

#include "ddmAutomaton.h"
#include "ctaIntSet.h"

ddmAutomatonOstream::ddmAutomatonOstream(ostream* pOs,
					 const ddmAutomaton* pAutomaton)
  : mAutomaton(pAutomaton),
    mStateIds(mAutomaton->getStateIds()),
    mSyncIds(mAutomaton->getSyncIds()),
    mVarIds(mAutomaton->getVarIds()),
    mDerIds(mVarIds),
    mDerivsEnabled(false),
    mOccurringSyncLabels()
{
  mOs = pOs;
  // Construct strings for derivatives.
  for(unsigned int i = 0; i<mDerIds.size(); i++)
  {
    mDerIds[i] = "DER(" + mDerIds[i] + ")";
  }
  
  // Add primed variables.
  unsigned int lNumVars = mVarIds.size();
  for(unsigned int j=0; j<lNumVars; j++)
  {
    mVarIds.push_back(mVarIds[j]+"'");
  }

  // Fetch the set of occuring sync labels.
  for(vector<ddmTransition*>::const_iterator 
	it = pAutomaton->getTransitions().begin();
      it !=  pAutomaton->getTransitions().end();
      ++it)
  {
    mOccurringSyncLabels.insert((*it)->getSync());
  }
}

void
ddmAutomatonOstream::printOccurringSyncLabel(const int pLabel)
{
  if(pLabel == 0)
  {
    // A time transition.
    *mOs << "T";
  }
  else
  {
    // Search element (pLabel) in (this->getOccurringSyncLabels()).
    // (i) is the number of the label in the label set.
    int i = pLabel-1;

    ctaIntSet::const_iterator
      it1 = this->getOccurringSyncLabels().begin();

    // Move to set element with index (i).
    for(;
	i>0;
	--i, ++it1)
    {}
	  
    // Print (*it1): The label name.
    if( *it1 == -1 )
    {
      // There is no sync label.
      *mOs << "<empty>";
    }
    else
    {
      *mOs << this->getAutomaton()->getSyncIds()[*it1];
    }
  }
}


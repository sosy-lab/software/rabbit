
#include "ddmMat.h"

istream&  
operator >> (istream& s, ddmMat& y)
{
  ddmVec row;
  
  int rows;
  
  // Read number of rows.
  s >> rows;
  
  y = ddmMat();
  
  for(int i=0; i<rows; ++i)
  {
    // Fetch and append next row.
    s >> row;
    y.push_back(ddmDualVec(row));
  }
  
  return s;
}


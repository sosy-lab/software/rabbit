
#include <set>

#include "ddmConfigPairList.h"

#include "reprAutomaton.h"
#include "ddmAutomaton.h"
#include "ddmTraces.h"

#ifdef DEBUG
#include "ddmAutomatonOstream.h"
#endif

ddmConfigPairList
ddmConfigPairList::mkFullConfigPairList(const reprAutomaton& pAutoImplem,
					const reprAutomaton& pAutoSpec,
					int pCurrentRound)
{
  ddmConfigPairList result(pAutoImplem, pAutoSpec);

  // There is just one config pair.
  
  // Create full configuration for all states in pAutoImplem.
  ddmConfig* lCImplem = pAutoImplem.getDerivedObjDdm()
                            ->mkFullConfig()->getDerivedObjDdm();
  
  // Create full configuration for all states in pAutoSpec.
  ddmConfig* lCSpec   = pAutoSpec.getDerivedObjDdm()
                            ->mkFullConfig()->getDerivedObjDdm();

  // Append new pair to result.
  result.push_back(ddmConfigPair(*lCImplem, *lCSpec, pCurrentRound));

  delete lCImplem;
  delete lCSpec;

  return result;
}
 

ddmConfigPairList::ddmConfigPairList(const reprAutomaton& pAutoImplem,
				     const reprAutomaton& pAutoSpec)
  : vector<ddmConfigPair>(),
    mAutoImplem(pAutoImplem),
    mAutoSpec(pAutoSpec)
{}

// For the given implementation configuration,
//   yield the sublist of components of (*this) whose
//   implementation part intersects (pImplemConfig).
//   Intersect the implementation parts and the specification
//   parts on the way.
//   If (pNowReached) is given, only elements with indices
//   in (*pNowReached) have to be checked.
ddmConfigPairList
ddmConfigPairList::mkPartition(const ddmConfig& pImplemConfig,
			       const ctaIntSet* pNowReached) const
{
  // Initialize an empty result.
  ddmConfigPairList result(mAutoImplem, mAutoSpec);

  // Initialisation needed for gcc optimizer.
  const_iterator it = this->end();  // The iterator pointing to nothing.
  ctaIntSet::const_iterator itSet;

  // Possibly construct a pair for each element of (*this)
  //   with an index in (pNowReached).

  // Loop flag.
  bool lFinished;

  // Initialization.
  if(pNowReached)
  {
    itSet = pNowReached->begin();
    lFinished = (itSet == pNowReached->end());
  }
  else
  {
    it = this->begin();
    lFinished = (it == this->end());
  } 
    
  // Loop.
  while(!lFinished)
  {
    // Make (it) valid.
    if(pNowReached)
    {
      it = this->begin() + (*itSet);
    }

    // Add a pair to (result) if implementation part of the current pair
    //  intersects (pImplemConfig).

    // Get copy of current pair in partition.
    ddmConfigPair lTmp(*it);

    // Intersect implem part of copy of current pair with 
    //   implementation config.
    lTmp.intersect(pImplemConfig);

    // Only pairs with nonempty implem-intersection are stored.
    if(!lTmp.getImplemConfig().isEmpty())
    {
      // Insert normalized result.
      lTmp.normalize();
      result.push_back(lTmp);
    }

    // Move on and compute next value of loop flag.
    if(pNowReached)
    {
      ++itSet;
      lFinished = (itSet == pNowReached->end());
    }
    else
    {
      ++it;
      lFinished = (it == this->end());
    }
  }

  return result;
}


// Compute the nonzero-time-precursor.
void
ddmConfigPairList::nonzeroTimePrecursor(int pTracePrefix)
{
  // Compute non-zero-time precursor for all pairs.
  for(iterator it = this->begin();
      it != this->end();
      ++it)
  {
    // Implementation component. Find non-zero-time precursor.
    it->refImplemConfig()
      = * mAutoImplem.getDerivedObjDdm()
                            ->mkTimePrecursor(&it->getImplemConfig())->getDerivedObjDdm();

    // Specification component.
    it->refSpecConfig() 
      = * mAutoSpec.getDerivedObjDdm()
                            ->mkTimePrecursor(&it->getSpecConfig())->getDerivedObjDdm();

#if 0
    {
      ddmAutomatonOstream lAOSS(&cout, &mAutoSpec);
      cout << endl << "#####";
      lAOSS << it->getSpecConfig();
      cout << "#####" << endl;
    }
#endif
  }
}

// Compute the transition-precursor of the list for the given label.
void
ddmConfigPairList::transitionPrecursor(const int pSyncLab,
				       int pTracePrefix)
{
  // Compute precursor for all pairs.
  for(iterator it = this->begin();
      it != this->end();
      ++it)
  {
    // Implementation component.
    ddmConfig* lTmpConfigImpl
      = mAutoImplem.getDerivedObjDdm()
                            ->mkTransitionPrecursor(it->getImplemConfig(),
						    pSyncLab,
						    mAutoSpec,
						    false)->getDerivedObjDdm();
    it->refImplemConfig() = *lTmpConfigImpl;
    delete lTmpConfigImpl;

    // Specification component.
    ddmConfig* lTmpConfigSpec
      = mAutoSpec.getDerivedObjDdm()
                            ->mkTransitionPrecursor(it->getSpecConfig(),
						    pSyncLab,
						    mAutoImplem,
						    false)->getDerivedObjDdm();
    ddmConfig lSpecConfig(*lTmpConfigSpec);
    delete lTmpConfigSpec;

    // Null-transitions in the spec might be allowed, but this
    //   is only the case if (mAutoSpec) does not have to sync
    //   on (pSyncLab).
    if(!mAutoSpec.mustSync(pSyncLab))
    {
      // Null-transition is possible.
      it->refSpecConfig().unite(lSpecConfig);
    }
    else
    {
      // Null-transition not possible.
      it->refSpecConfig() = lSpecConfig;
    }
  }
}


void
ddmConfigPairList::normalize()
{
  for(iterator it = this->begin();
      it != this->end();
      ++it)
  {
    it->normalize();
  }
}

// Check that correspondents or reachable regions are supersets.
//  If we have error examples, they are appended to (pErrorExamples).
bool
ddmConfigPairList::correspondentsAreSupersets(const ddmConfig& pImplem,
				      ddmConfigPairList& pErrorExamples)
  const
{
  ddmConfigPairList lTmp(this->mkPartition(pImplem));

  bool result = true;

  // Do not stop loop at first non-superset correspondent,
  //   but collect the rest of the error examples.
  for(const_iterator it = lTmp.begin();
      it != lTmp.end();
      ++it)
  {
    if(!it->correspondentIsSuperset())
    {
      // Set (result) to (false).
      result = false;
      pErrorExamples.push_back(*it);
    }
  }

  return result;
}

// Intersect all elements of (*this) with the given config pair,
//   remove implem-empty elements on the way, and return 
//   non-intersecting rest in (pCP).
void
ddmConfigPairList::intersectAndRest(ddmConfigPair& pCP)
{
  ddmConfigPair lOrig(pCP);

  // Starting from the end makes deletions more efficient.
  for(iterator it = this->end();
      it != this->begin();
      )
  {
    // Go to next element.
    it--;

    // Intersect config-pairs.
    it->intersect(lOrig);

    // Remove elements with empty implementation part from list.
    if(it->getImplemConfig().isEmpty())
    {
      this->erase(it);
    }
    else
    {
      // Non-empty intersections are subtracted from (pCP).
      pCP.refImplemConfig().setSubtract(it->getImplemConfig());
    }
  }

  // Normalize the rest.
  pCP.normalize();
}


// Apply (*this) partition to pair (pPair) and append result to (pResult).
//   Update traces, refusals and set of recently reached pairs on the way.
void
ddmConfigPairList::partitionPair(const ddmConfigPair& pPair,
				 ddmConfigPairList& pResult,
				 int pTracePrefix)
{
  // Split the configuration pair (pPair) with the given pseudo-partition.
  //   This results in a partition of the current implementation config.

  // Get a copy of the pair to split.
  ddmConfigPair lTmp(pPair);

  // Intersect each element of (*this) with
  //  the pair.  In (lTmp) stays what does not 
  //  intersect with any element of (*this).
  this->intersectAndRest(lTmp);

  // Add current traces of pair to each 
  //   element of (*this).  Each element of (*this)
  //   will represent a partition of (pPair).
  for(iterator it = this->begin();
      it != this->end();
      ++it)
  {
    // Normalize the pair.
    it->normalize();

    // Traces and refusals.
    it->setTraces(ddmTraces::mkSequence(pTracePrefix, 
					it->getTraces())
		  );

#ifdef MANAGE_REFUSALS
    it->setRefusals(ddmTraces::mkSequence(pTracePrefix,
					  it->getRefusals())
		    );
#endif

    // Insert information about recently reached state.
    // (*it) will later be interpreted as a part of (pPair).
    it->setRecentlyReached(map<int, ctaIntSet>());
    it->refRecentlyReached()[pTracePrefix].insert(it->getGenealogy().back());
  }

  // Append the rest, if implem-region nonempty.
  if(!lTmp.getImplemConfig().isEmpty())
  {
#ifdef MANAGE_REFUSALS
    // From the rest, there is no transition with (pTracePrefix).
    //   Store this in the refusals.
    lTmp.setRefusals
      (ddmTraces::mkUnion(lTmp.getRefusals(),
			  ddmTraces::mkSequence(pTracePrefix,
						ddmTraces::mkEmptyTrace())));
#endif

    lTmp.normalize();
    this->push_back(lTmp);

    // This transition did not supply a reached 
    //   pair for (lTmp).
    lTmp.setRecentlyReached(map<int, ctaIntSet>());
  }

  // Append the new configurations to the result.
  pResult.insert(pResult.end(), this->begin(), this->end());
}

// Make the implementation sides disjoint.
void
ddmConfigPairList::disjoinImplemParts()
{
  // The implem-parts might be non-disjoint.  We split
  //   this into disjoint components.
    
  // Compute intersections for each element.

  // We use indices, not iterators, since we might append
  //   elements and thus invalidate iterators.
  for(int i = 0;
      i < (int) this->size();
      ++i)
  {
    // Determine upper limit for inner loop.
    //   The elements appended in each loop do not have to be considered,
    //   they are constructed to NOT intersect with element at (i).
    int lUpperLimitForJ = (int) this->size();

    // Process (*this)[i] for all elements behind (i).
    //  Stop loop when (*this)[i] becomes empty.
    for(int j=i+1;
	(!(*this)[i].getImplemConfig().isEmpty())
	  && j < lUpperLimitForJ;
	++j)
    {
      // Look if elements at (i) and (j) intersect.
      ddmConfig lTmp((*this)[i].getImplemConfig());
      lTmp.intersect((*this)[j].getImplemConfig());

      // We only have to do something if the elements do intersect.
      if(!lTmp.isEmpty())
      {
	// Take away the intersection (lTmp) from elements at (i) and (j).
	(*this)[i].refImplemConfig().setSubtract(lTmp);
	(*this)[j].refImplemConfig().setSubtract(lTmp);
	
	// Construct the intersection of the specification configs.
	ddmConfig lSpecIntersection((*this)[i].getSpecConfig());
	lSpecIntersection.intersect((*this)[j].getSpecConfig());
	
	// Construct the union of the recently reached of 
	//  the spec configs.
	map<int, ctaIntSet> 
	  lRecentlyReachedUnion((*this)[i].getRecentlyReached());

	for(map<int, ctaIntSet>::const_iterator
	      it = (*this)[j].getRecentlyReached().begin();
	    it != (*this)[j].getRecentlyReached().end();
	    ++it)
	{
	  ctaIntSet& ltmpset = lRecentlyReachedUnion[it->first];
	  for (ctaIntSet::iterator it10 = it->second.begin(); 
	       it10 != it->second.end(); 
	       it10++)
	  {
	    ltmpset.insert(*it10);
	  }
	}
				     
	// Construct the new pair.  Genealogy and change rounds should
	//  be the same for all elements of (*this), so it does not 
	//  matter from where we take them.
	ddmConfigPair lCPNew(lTmp,
			     lSpecIntersection, 
			     (*this)[i].getGenealogy(),
			     (*this)[i].getChangeRounds(),
			     ddmTraces::mkUnion((*this)[i].getTraces(),
						(*this)[j].getTraces()),
#ifdef MANAGE_REFUSALS
			     ddmTraces::mkUnion((*this)[i].getRefusals(),
						(*this)[j].getRefusals()),
#endif
			     lRecentlyReachedUnion
			     );

	// Append new pair.
	this->push_back(lCPNew);
      }
    }
  }

  // Delete all pairs with empty implem part.
  //   Start at the end: Should be quicker.
  //   We use indices again.
  for(int k = this->size(); k > 0; )
  {
    // Go to next pair.
    --k;

    // Check if implem part is empty.
    if((*this)[k].getImplemConfig().isEmpty())
    {
      // Erase elems with empty implem part.
      this->erase(this->begin()+k);
    }
  }
}


// Split each element in (*this) with each possible transition,
//  collect results in (pResult).
void
ddmConfigPairList::splitWithTransitions(ddmConfigPairList& pResult,
					const int pRound,
					bool& pChangeOccurred,
					const ctaIntSet& pUsedSyncs)
{
  cout << this->size();

  for(const_iterator it = this->begin();
      it != this->end();
      ++it)
  {
    cout << "#"; cout.flush();

    // Construct the result for the current ddmConfigPair.
    // Empty list.
    ddmConfigPairList lTmpResult(pResult.getAutoImplem(),
				 pResult.getAutoSpec());

    cout << "."; cout.flush();

    // First, split with time transition.
    it->splitWithTimeTransition(*this,
				lTmpResult,
				pRound);

    // Then, split with all discrete transitions.
    {
      int lTracePrefix = 1;
      for(ctaIntSet::const_iterator 
	    itSync = pUsedSyncs.begin();
	  itSync != pUsedSyncs.end();
	  ++itSync, ++lTracePrefix)
      {
	cout << "."; cout.flush();

	it->splitWithDiscreteTransition_Sync(*this,
					     *itSync,
					     lTmpResult,
					     pRound,
					     lTracePrefix);
      }
    }

    // Disjoin the overlapping components.
    lTmpResult.disjoinImplemParts();

    // Unite traces and refusals of all result pairs with
    //   traces of (*it).
    {
      for(iterator itTmpResult = lTmpResult.begin();
	  itTmpResult != lTmpResult.end();
	  ++itTmpResult)
      {
	itTmpResult->setTraces
	  (ddmTraces::mkUnion(itTmpResult->getTraces(),
			      it->getTraces())
	   );

#ifdef MANAGE_REFUSALS
	itTmpResult->setRefusals
	  (ddmTraces::mkUnion(itTmpResult->getRefusals(),
			      it->getRefusals())
	   );
#endif
      }
    }

    lTmpResult.normalize();

    // Check if a change occurred.
    if(lTmpResult.size() != 1
       || ! lTmpResult[0].setEqual(*it) )
    {
      pChangeOccurred = true;

      // Replace all change round information with
      //   that of (*it), since in fact, we are 
      //   dealing with a split of (*it) now.
      // Same is true for genealogy.
      for(iterator itTmpResult = lTmpResult.begin();
	  itTmpResult != lTmpResult.end();
	  ++itTmpResult)
      {
	itTmpResult->setChangeRounds(it->getChangeRounds());
	itTmpResult->setGenealogy(it->getGenealogy());
	itTmpResult->appendLastChangeRound(pRound);
      }

      // Output number of pairs constructed.
      cout << lTmpResult.size();

      // Feed temporary result into final result.
      pResult.insert(pResult.end(),
		     lTmpResult.begin(),
		     lTmpResult.end());
    }
    else
    {
      pResult.push_back(*it);
    }
  }

  cout << endl;

  // Append position number to genealogies in result.
  {
    int lCount = 0;
    for(iterator it = pResult.begin();
	it != pResult.end();
	++it, ++lCount)
    {
      it->appendToGenealogy(lCount);
    }
  }
}

ostream&
operator << (ostream& s, const ddmConfigPairList& y)
{
  s << "ddmConfigPairList: " << y.size() << " elems:" << endl;
  
  const ddmAutomaton* lAutoImplTmp = y.getAutoImplem().getDerivedObjDdm();

  const ddmAutomaton* lAutoSpecTmp = y.getAutoSpec().getDerivedObjDdm();

  if ( (lAutoImplTmp == NULL) || (lAutoSpecTmp == NULL) )
  {
    cerr << "Runtime error: operator<< of ddmConfigPairList can only handle"
	 << " ddmAutomaton!";
  }

  ddmAutomatonOstream lAOSI(&s, lAutoImplTmp);
  ddmAutomatonOstream lAOSS(&s, lAutoSpecTmp);

  int lCount = 0;
  for(ddmConfigPairList::const_iterator it = y.begin();
      it != y.end();
      ++it, ++lCount)
  {
    s << "Genealogy: ";
    for(vector<int>::const_iterator it1 = it->getGenealogy().begin();
	it1 != it->getGenealogy().end();
	++it1)
    {
      s << *it1 << " ";
    }
    s << endl;
    
    s << "Changed in rounds:";
    for(vector<int>::const_iterator it11 = it->getChangeRounds().begin();
	it11 != it->getChangeRounds().end();
	++it11)
    {
      s << *it11 << " ";
    }
    s << endl;

    s << "Possible traces:";
    lAOSI << it->getTraces() << endl;

#ifdef MANAGE_REFUSALS
    s << "Possible refusals:";
    lAOSI << it->getRefusals() << endl;
#endif

    s << "Implem config:" << endl;
    lAOSI << it->getImplemConfig() << endl;
    
    s << "Spec config:" << endl;
    lAOSS << it->getSpecConfig() << endl;

    s << "Recently reached:" << endl;
    for(map<int, ctaIntSet>::const_iterator
	  itMap = it->getRecentlyReached().begin();
	itMap != it->getRecentlyReached().end(); 
	++itMap)
    {
      s << " Via ";
      lAOSI.printOccurringSyncLabel(itMap->first);
      s << ":";
      for(ctaIntSet::const_iterator 
	    itSet = itMap->second.begin();
	  itSet != itMap->second.end();
	  ++itSet)
      {
	s << " " << *itSet;
      }
      s << endl;
    }

    s << "*****" << endl;
  }
  s << "End ddmConfigPairList\n";
  
  return s;
}

#ifndef _ddmXPoly_h_
#define _ddmXPoly_h_


#include <iostream>
#include <strstream>
#include <string>

#include "ddmAutomatonOstream.h"
#include "ctaIntSet.h"
#include "ddmPoly.h"

// Extended polyhedra: we are only interested in solutions with x0=1,x1<0.
class ddmXPoly : private ddmPoly
{
private:
  // It should be not allowed to use the standard operators.
  // Uses the standard operator '='.
  void operator,(const ddmXPoly&);

public: // Public constants.

  // We have two extra variables.
  //   First variable is for expressing strictness.
  //   Second variable is for constants.
  static const int mNumExtraVars; //= 2;

private: // Private static methods.

public:  // Public static methods.

  // Compute the possibly reachable region from (p1) when
  //   the allowed derivations of the variables
  //   are given by (pTimeDerivs).
  static ddmXPoly
  mkTimeReach(const ddmXPoly& p1, 
          const ddmXPoly& pTimeDerivs,
          bool pNonZeroTime);

  // Compute closure: remove strictness from inequalities.
  static ddmXPoly
  mkClosure(const ddmXPoly& p);
 
  // Compute projection by existential quantification
  //   of variable from number (pFrom) up to but not including
  //   (pUpTo).
  static ddmXPoly*
  mkProjection(const ddmXPoly* p, int pFrom, int pUpTo)
  {
     ddmPoly* tmpP = ddmPoly::mkProjection((ddmPoly *)p, pFrom+mNumExtraVars, 
                       pUpTo+mNumExtraVars,
                       true);
     // We need this to get a (ddmXPoly) from a (ddmPoly).
     ddmXPoly* result = new ddmXPoly(*tmpP);
     delete tmpP;
     return result;
  }

  static ddmXPoly
  mkTimeFollower(const ddmXPoly& pCurrent, 
         const ddmXPoly& pDerivs, 
         const ddmXPoly& pInvariant);

  static ddmXPoly
  mkTimePrecursor(const ddmXPoly& pCurrent, 
          const ddmXPoly& pDerivs, 
          const ddmXPoly& pInvariant);

  static ddmXPoly*
  mkTransitionFollower(const ddmXPoly& pCurrent, 
               const ddmXPoly& pAssignments,
               const int pSync);

  static ddmXPoly*
  mkTransitionPrecursor(const ddmXPoly& pCurrent, 
            const ddmXPoly& pAssignments,
            const int pSync);

private: // Attributes.

  // Dimension.
  int mDim;

  // A trace leading to this ddmXPoly.
  //   0 if trace should not be used.
  vector<int> mTrace;

public: // Constructors and destructor.

  explicit ddmXPoly(int pDim)
    : ddmPoly(pDim+mNumExtraVars),
      mDim(pDim),
      mTrace(0)
  {
    this->addStdConstraints();
  }

  ddmXPoly(const ddmXPoly& p)
    : ddmPoly(p),
      mDim(p.mDim),
      mTrace(p.mTrace)
  {}

private:
  // Generate a ddmXPoly from a ddmPoly.
  //   This is a bit dangerous: We just assume but we do not check
  //   that the first constraints in the ddmPoly are the standard 
  //   ones for ddmXPoly's.
  ddmXPoly(const ddmPoly& p)
    : ddmPoly(p),
      mDim(p.getDim()-mNumExtraVars),
      mTrace(0)
  {}

public:
  // Generate a ddmXPoly from a ddmPoly.
  //   This is a bit dangerous: We just assume but we do not check
  //   that the first constraints in the ddmPoly are the standard 
  //   ones for ddmXPoly's.
  // We create a ddmXPoly from a ddmPoly and destroy the ddmPoly.
  ddmXPoly(const ddmPoly* p)
    : ddmPoly(*p),
      mDim(p->getDim()-mNumExtraVars),
      mTrace(0)
  {
    delete p;
  }

  ddmXPoly()
    : ddmPoly(),
      mDim(0),
      mTrace(0)
  {
    // Just a placeholder: We do not even add the special constraints.
  }

  ~ddmXPoly()
  {}

public: // Accessors.

  const ddmConstraints&
  getConstraints() const
  {
    return ddmPoly::getConstraints();
  }

  // Added for experiment only!
  const ddmRays&
  getRays() const
  {
    return ddmPoly::getRays();
  }

  int
  getDim() const
  {
    return this->mDim;
  }

  const vector<int>&
  getTrace() const
  {
    return mTrace;
  }

  void
  setTrace(const vector<int>& pTrace)
  {
    mTrace = pTrace;
  }

public: // Service methods.

  void
  addConstraint(const ddmVec& p)
  {
    ddmPoly::addConstraint(p);
  }
  
  void
  intersect(const ddmXPoly& p)
  {
    this->addConstraints(p.getConstraints(), 
             ddmConstraints::mNumStdConstraints);
  }

  // sort the constraints before building the rays.
  void
  intersect_sort(const ddmXPoly& p)
  {
    this->addConstraintsSorted(p.getConstraints(),
             ddmConstraints::mNumStdConstraints);
  }

  bool
  isEmpty() const;

  // Add (pNum) dimensions before (pAT).
  void
  addDimensions(int pAt, int pNum)
  {
    // Call function for parent.
    ((ddmPoly*) this)->addDimensions(pAt+mNumExtraVars, pNum);

    // Update dimension counter.
    mDim += pNum;
  }

  void
  changeSigns()
  {
    // Do not change the sign
    //   of the very first component of the vectors:
    //   Strict inequalities should stay strict, and
    //   nonstrict should stay nonstrict.
    mConstraints.changeSigns(true);
    mRays.changeSigns(true);
  }
  
  // Normalize a ddmXPoly by building twice the dual.
  void
  normalize()
  {
    // Compute dual.  This is no ddmXPoly, just a ddmPoly.
    ddmPoly lTmp(mNumExtraVars+mDim);
    lTmp.addConstraints(this->getRays());

    // Compute dual of (lTmp) as ddmXPoly.
    ddmXPoly result(mDim);
    result.addConstraints(lTmp.getRays());

    // Store result.  Ensure that mDim and mTrace 
    //   do not change.
    (ddmPoly &) (*this) = (ddmPoly) result;
  }

  void
  addToTrace(const int pSync)
  {
    mTrace.push_back(pSync);
  }

  bool
  contains(const ddmXPoly& p) const
  {
    return ddmPoly::contains(p);
  }

public: // Friends.

  friend bool
  operator==(const ddmXPoly& p1, const ddmXPoly& p2)
  {
    return (const ddmPoly&) p1 == (const ddmPoly&) p2;
  }

  friend bool
  operator!=(const ddmXPoly& p1, const ddmXPoly& p2)
  {
    return (const ddmPoly&) p1 != (const ddmPoly&) p2;
  }

public: // IO.

  void
  print(ostream& s, const vector<string>* pVarNames = 0) const;

  friend ostream&  
  operator << (ostream& s, const ddmXPoly& y)
  {
    y.print(s);

    return s;
  }

  friend ddmAutomatonOstream&
  operator<<(ddmAutomatonOstream& s, const ddmXPoly& y);
};

bool operator< (ddmXPoly p1, ddmXPoly p2);

// Local Variables:
// mode:C++
// End:

#endif // _ddmXPoly_h_

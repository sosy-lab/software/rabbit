#ifndef _ddmRCIPtr_h_
#define _ddmRCIPtr_h_

// This is a Smart Pointer Class for reference counting.
// Class T must be derived from ddmRCObject.


template<class T>
class ddmRCIPtr
{
private:
  // It should be not allowed to use the standard operators.
  ddmRCIPtr(const ddmRCIPtr&);
  void operator=(const ddmRCIPtr&);
  void operator,(const ddmRCIPtr&);

public:
  ddmRCIPtr(T* pRealPtr = 0)
    : mCounter(new CountHolder)
  {
    mCounter->mPointee = pRealPtr;
    init();     // init needed or only addReference???
  }

  ddmRCIPtr(const ddmRCIPtr& pRhs)
    : mCounter(pRhs.counter)
  { init(); }

  ~ddmRCIPtr()
  { mCounter->removeReference(); }

  ddmRCIPtr& operator=(const ddmRCIPtr& pRhs)
  {
    if(mCounter != pRhs.counter) // If it is the same object we do nothing.
    {
      mCounter->removeReference();
      counter = pRhs.mCounter;
      init();
    }
    return *this;
  }

  T* operator->()                    // Non-const access. We have to copy.
  {
    // What we would like to do with not-const access?
    // An application can write on our mValue.
    // Would we declare it as private to avoid such access?

    makeCopy();
    return mCounter->mPointee;
  }

  const T* operator->() const        // We have only read access.
  { return mCounter->mPointee; }     // No copy needed.

  T& operator*()                     // Non-const access. We have to copy.
  {
    makeCopy();
    return *(mCounter->mPointee);
  }

  const T& operator*() const         // We have only read access.
  { return *(mCounter->mPointee); }  // No copy needed.

private:
  struct CountHolder: public ddmRCObject
  {
    ~CountHolder() { delete mPointee; }
    T* mPointee;
  };
  Countholder* mCounter;

  void init()
  {
    if(mCounter->isShareble() == false)
    {
      T* lOldValue = mCounter->mPointee;
      mCounter = new CountHolder;
      mCounter->mPointee = new T(*lOldValue);
    }
    mCounter->addReference();
  }

  void makeCopy()   // The copy procedure of copy-on-write.
  {
    if(mCounter->isShared())  // If not only one value is in use.
    {
      T* lOldValue = mCounter->mPointee;
      mCounter->removeReference();
      mCounter = new CountHolder;
      mCounter->mPointee = new T(*lOldValue);
      mCounter->addReference();
    }
  }
};

// Local Variables:
// mode:C++
// End:

#endif // _ddmRCIPtr_h_

#ifndef _ddmMat_h_
#define _ddmMat_h_


#ifdef DEBUG
#  include <iostream>
#endif

// We represent matrices as STL-vectors of ddmVec's.
// We use bitvectors to represent the incidences.
#include <vector>

#include "ddmDualVec.h"
#include "ddmIncidences.h"
#include "ddmVec.h"


// A class for matrices.

// Matrix data: an STL-vector of numerical vectors.
class ddmMat : public vector<ddmDualVec>, private ddmObject
{
private:
  // It should be not allowed to use the standard operators.
  // Uses the standard copy constructor.
  // Uses the standard operator '='.
  void operator,(const ddmMat&);

private: // Attributes.

public: // Constructors and destructor.

  ddmMat()
    : vector<ddmDualVec>(0)
  {}

public: // Service methods.

  // Shorten each vector.
  void
  removeDimensions(unsigned int pFrom, unsigned int pUpTo)
  {
    // Shorten each vector.
    for(iterator it = this->begin();
	it != this->end();
	++it)
    {
      it->removeDimensions(pFrom, pUpTo);
    }
  }

  // Add (pNum) dimensions before (pAT).
  void
  addDimensions(unsigned int pAt, unsigned int pNum)
  {
    // Insert zeroes into the vectors.
    for(iterator it = this->begin();
	it != this->end();
	++it)
    {
      it->addDimensions(pAt, pNum);
    }
  }

  void
  changeSigns(bool bIgnoreFirst)
  {
    for(iterator it = this->begin();
	it != this->end();
	++it)
    {
      it->changeSigns(bIgnoreFirst);
    }
  }

public: // Friends.

public: // IO.

  friend istream&  
  operator >> (istream& s, ddmMat& y);

  friend ostream&  
  operator << (ostream& s, const ddmMat& y)
  {
    // Print number of rows.
    s << y.size() << endl;

    // Print one row after the other.
    for(unsigned int i=0; i<y.size(); ++i)
    {
      s << y[i] << endl;
    }

    return s;
  }

};

// Local Variables:
// mode:C++
// End:

#endif // _ddmMat_h_

#ifndef _ddmConstraints_h_
#define _ddmConstraints_h_


#ifdef DEBUG
#  include <iostream>
#endif

// Sorting.
#include <algorithm>

// We represent constraints as ddmMat's.
#include "ddmMat.h"
#include "ddmVec.h"
class ddmRays;


// A class for matrices.

// Matrix data: an STL-vector of numerical vectors.
class ddmConstraints : public ddmMat
{
private:
  // It should be not allowed to use the standard operators.
  // Uses the generated copy constructor. 
  // Uses the standard operator '='.
  void operator,(const ddmConstraints&);

public: // Static methods.

public: // Attributes.

  static const int mNumStdConstraints;// = 3;
  
  // A vector of indices pointing to the equalities.
  //  For each equality, both corresponding
  //  inequalities are stored in ddmMat,
  //  but only the first is indexed here.
  vector<int> mEqualities;

public: // Constructors and destructor.

  ddmConstraints()
    : ddmMat(),
      mEqualities()
  {}

  ddmConstraints(const ddmMat& p)
    : ddmMat(p),
      mEqualities()
  {}

  ddmConstraints(const ddmConstraints& p)
    : ddmMat(p),
      mEqualities(p.mEqualities)
  {}

public: // Accessors.

  const vector<int>&
  getEqualities() const
  {
    return mEqualities;
  }

public: // Service methods.

  int
  countEqualities() const
  {
    return mEqualities.size();
  }

  bool
  include(const ddmVec& p, bool pBidirectional) const;

  // (pDoProcess) is set to true in this method if
  //   the constraint (p) is new.
  void
  addConstraint(const ddmVec& p, bool& pDoProcess);

  // Sort the constraints for shortest length.
  void Sort(int pStartingAt);

  // Add (pNum) dimensions before (pAT).
  void
  addDimensions(unsigned int pAt, unsigned int pNum)
  {
    // Insert zeroes into the vectors.
    ((ddmMat*)this)->addDimensions(pAt, pNum);
  }

  // Remove strictness from the constraints.
  void
  removeStrictness(bool& pChangedSomething);

  // Remove strictness from the constraints.
  void
  removeStrictnessDirectedly(const ddmRays& pDerivs,
			     bool& pChangedSomething);

  // Add strictness to the constraints.
  void
  addStrictnessDirectedly(const ddmRays& pDerivs, 
			  bool& pChangedSomething);

  bool
  isEquality(int p) const
  {
    return    find(mEqualities.begin(), mEqualities.end(), p) 
      != mEqualities.end();
  }

  bool
  isEquality(const_iterator p) const
  {
    return this->isEquality(p - this->begin());
  }
  
public: // Friends.

public: // IO.

  friend ostream&  
  operator << (ostream& s, const ddmConstraints& y);

};

// Local Variables:
// mode:C++
// End:

#endif // _ddmConstraints_h_

#ifndef _ddmConfigPairList_h_
#define _ddmConfigPairList_h_


#include <vector>

class reprAutomaton;

#include "ddmConfigPair.h"
#include "ctaIntSet.h"
#include "ddmTransition.h"

class ddmConfigPairList: public vector<ddmConfigPair>, private ddmObject
{
private:
  // It should be not allowed to use the standard operators.
  // Uses the generated copy constructor. 
  void operator=(const ddmConfigPairList&);
  void operator,(const ddmConfigPairList&);

public: // Static methods.

  static ddmConfigPairList
  mkFullConfigPairList(const reprAutomaton& pAutoImplem,
		       const reprAutomaton& pAutoSpec,
		       int pLastChangeRound);
 
private: // Attributes.

  // The automata are associated.
  const reprAutomaton& mAutoImplem;
  const reprAutomaton& mAutoSpec;

public: // Constructors and destructor.

  ddmConfigPairList(const reprAutomaton& pAutoImplem,
		    const reprAutomaton& pAutoSpec);
  
public: // Accessors.

  const reprAutomaton&
  getAutoImplem() const
  {
    return mAutoImplem;
  }

  const reprAutomaton&
  getAutoSpec() const
  {
    return mAutoSpec;
  }
  
private: // Private convenience methods.
  
public: // Service methods.
  
  // Split all states via the syncing discrete transitions
  //   with the given sync-lab.
  void
  splitWithDiscreteTransition_Sync(const ctaIntSet& pSyncLab,
				   ddmConfigPairList& pResult,
				   const int pSuperRound,
				   const int pCurrentRound,
				   const int pTracePrefix,
				   int& pLastChangeRound) const;
  
  // Compute the nonzero-time-precursor.
  void
  nonzeroTimePrecursor(int pTracePrefix);
  
  // Compute the transition-precursor of the list for
  //   the given label and the given origin state.
  void
  transitionPrecursor(const int pSyncLab, int pTracePrefix);
  
  // Intersect all elements of (*this) with the given config pair
  //   and remove implem-empty elements on the way.
  //   In (pCP), return the rest.
  void
  intersectAndRest(ddmConfigPair& pCP);
  
  // Apply (*this) partition to (pPair) and store result in 
  //   (pResult).
  void
  partitionPair(const ddmConfigPair& pPair,
		ddmConfigPairList& pResult,
		int pTracePrefix);
  
  // Make the implementation sides disjoint and normalize.
  void
  disjoinImplemParts();
  
  // Split all config pairs into the equivalence
  //   classes induced by a time transition.  Remove the old
  //   pair and add the new classes at the end.
  //   We assume that the implementation side configurations of all
  //   config pairs are a single-state configurations.
  // (pChangedSomething) is a flag in which we signal if the
  //   split changed something in the config pair list.
  void
  splitWithTimeTransition(ddmConfigPairList& pResult,
			  const int pSuperRound,
			  int& pCurrentRound,
			  int& pLastChangeRound) const;
  
  // Split each element in (*this) with each possible transition,
  //  collect results in (pResult).
  void
  splitWithTransitions(ddmConfigPairList& pResult,
		       const int pRound,
		       bool& pChangeOccured,
		       const ctaIntSet& pUsedSyncs);
  
  // Check that correspondents or reachable regions are supersets.
  //  If we have error examples, they are appended to (pErrorExamples).
  bool
  correspondentsAreSupersets(const ddmConfig& pImplem,
			     ddmConfigPairList& pErrorExamples) const;
  
  void
  normalize();
  
  // For the given implementation configuration,
  //   yield the sublist of components of (*this) whose
  //   implementation part intersects (pImplemConfig).
  ddmConfigPairList
  mkPartition(const ddmConfig& pImplemConfig,
	      const ctaIntSet* pNowReached = 0) const;
  
public: // Friends.
  
public: // IO.
  
  friend ostream&
  operator<< (ostream& s, const ddmConfigPairList& y);
};

// Local Variables:
// mode:C++
// End:

#endif // _ddmConfigList_h_

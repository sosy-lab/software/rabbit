#ifndef _ddmAutomatonOstream_h_
#define _ddmAutomatonOstream_h_


#include <iostream>
#include <string>
#include <vector>

#include "reprAutomatonOstream.h"
class ddmAutomaton;
#include "ctaIntSet.h"

class ddmAutomatonOstream : public reprAutomatonOstream
{
private: 
  // It should be not allowed to use the standard operators.
  ddmAutomatonOstream(const ddmAutomatonOstream&);
  void operator=(const ddmAutomatonOstream&);
  void operator,(const ddmAutomatonOstream&);

private: // Attributes.
  // All associated.

  // The automata.
  const ddmAutomaton* mAutomaton;

  // Ids for states.
  const vector<string>& mStateIds;

  // Ids for sync-signals.
  const vector<string>& mSyncIds;

  // Ids for variables.
  vector<string> mVarIds;

  // Ids for the derivatives.
  vector<string> mDerIds;

  // Should we return derivations.
  bool mDerivsEnabled;

  // The occuring sync labels.
  ctaIntSet mOccurringSyncLabels;

public: // Constructors and destructor.

  ddmAutomatonOstream(ostream* pOs, const ddmAutomaton* pAutomaton);

public: // Accessors.

  const ddmAutomaton*
  getAutomaton() const
  {
    return mAutomaton;
  }
  
  const vector<string>&
  getStateIds() const
  {
    return mStateIds;
  }
  
  const vector<string>&
  getSyncIds() const
  {
    return mSyncIds;
  }

  const vector<string>&
  getVarIds() const
  {
    if(mDerivsEnabled)
    {
      return mDerIds;
    }
    else
    {
      return mVarIds;
    }
  }
  
  const ctaIntSet&
  getOccurringSyncLabels() const
  {
    return mOccurringSyncLabels;
  }
  
public: // Service methods.
  
  void
  enableDerivs()
  {
    mDerivsEnabled = true;
  }
  
  void
  disableDerivs()
  {
    mDerivsEnabled = false;
  }
  
  // Print string corresponding to occurring sync label (pLabel).
  void
  printOccurringSyncLabel(const int pLabel);
  
public: // Friends.
  
public: // IO.
};

// Local Variables:
// mode:C++
// End:

#endif // _ddmAutomatonOstream_h_

#ifndef _ddmDualVec_h_
#define _ddmDualVec_h_


// Represent whatever is necessary for rays during the
//  duality computation.

#include <vector>

#include "ddmIncidences.h"
#include "reprNUMBER.h"
#include "ddmVec.h"

class ddmDualVec : public ddmVec
{
private:
  // It should be not allowed to use the standard operators.
  // Uses the standard copy constructor.
  // Uses the standard operator '='.
  void operator,(const ddmDualVec&);

private: // Attributes.

  ddmIncidences mIncidences;
  reprNUMBER mVal;
  int mSaturations;

public: // Constructors and destructor.

  ddmDualVec()
    : ddmVec(0),
      mIncidences(),
      mVal(0),
      mSaturations(0)
  {}

  ddmDualVec(ddmVec pVec,
	     ddmIncidences pIncidences,
	     int pSaturations)
    : ddmVec(pVec),
      mIncidences(pIncidences),
      mVal(0),
      mSaturations(pSaturations)
  {}

  ddmDualVec(ddmVec pVec,
	     ddmIncidences pIncidences)
    : ddmVec(pVec),
      mIncidences(pIncidences),
      mVal(0),
      mSaturations(mIncidences.countOnes())
  {}

  ddmDualVec(ddmVec pVec)
    : ddmVec(pVec),
      mIncidences(),
      mVal(0),
      mSaturations(0)
  {}

public: // Accessors.
  
  const ddmIncidences&
  getIncidences() const
  {
    return mIncidences;
  }

  void
  setIncidences(const ddmIncidences& pIncidences)
  {
    mIncidences = pIncidences;
  }

  const reprNUMBER&
  getVal() const
  {
    return mVal;
  }

  void
  setVal(reprNUMBER pVal, int iConstraint = -1);

  int
  getSaturations() const
  {
    return mSaturations;
  }


public: // Service functions.

  bool
  isEmpty() const
  {
    return 0 == this->size();
  }

  void
  swap(ddmDualVec& pVec)
  {
    this->swap(pVec);
    this->mIncidences.swap(pVec.mIncidences);

    {
      reprNUMBER tmp = this->mVal;
      this->mVal = pVec.mVal;
      pVec.mVal = tmp;
    }
  }

  void
  combineWith(ddmDualVec& pRay, 
	      int pConstraint);

public: // IO.

  friend ostream&  
  operator << (ostream& s, const ddmDualVec& y)
  {
    s << "(" << (ddmVec) y
      << ", " << y.getIncidences()
      << ")";

    return s;
  }
};

// Local Variables:
// mode:C++
// End:

#endif // _ddmDualVec_h_

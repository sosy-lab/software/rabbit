
#include "reprNUMBER.h"
#include "ddmRays.h"
  
void
ddmRays::processBidAndUniRays(iterator nonNullBidRay)
{
  // There was a non-null bidirectional ray: delete it 
  //   from the list of bidirectional rays, add its positive
  //   version to the list of unidirectional rays,
  //   and process all bidirectional and unidirectional
  //   rays with it.
      
#ifdef DEBUG
  cout << "  Found nonzero bidRay " << *nonNullBidRay << endl;
#endif
	
  // Compute positive version.
  if( nonNullBidRay->getVal() < 0 )
  {
#ifdef DEBUG
    cout << "  Turning bidray\n";
#endif
    nonNullBidRay->scale(-1);
    nonNullBidRay->setVal( -1 * nonNullBidRay->getVal() );
  }
	
  // Process all rays.
  for(iterator ray=this->begin();
      ray != this->end();
      ++ray)
  {
    // Do not process the (nonNullBidRay) itself or those which already
    //   are incident with the current constraint.
    if(ray != nonNullBidRay && !(ray->getIncidences()[mNumConstraints]))
    {
      ray->combineWith(*nonNullBidRay, mNumConstraints);
    }
  }
      
  // Move (*nonNullBidRay) from the bidirectional rays to the
  //   unidirectional rays.
  {
    this->mStartOfUniRays--;
    
    if( nonNullBidRay != this->begin() + this->mStartOfUniRays )
    {
      // Swap necessary.
      ddmDualVec tmp = *nonNullBidRay;
      *nonNullBidRay = (*this)[this->mStartOfUniRays];
      (*this)[this->mStartOfUniRays] = tmp;
    }
  }
}
void
ddmRays::processUniRays(int pFormalDim, int pNumEqs)
{
  // There was no non-null bidirectional ray.
  //   This means we only have to process the unidirectional
  //   rays.

#ifdef DEBUG
  cout << "Did not find a nonzero bidRay\n";
#endif
	
  // Collect 'negative' rays here.
  ddmMat negRays;

  // Move 'negative' rays to (negRays).
  for(iterator i = this->begin()+this->mStartOfUniRays, j = this->end();
      i<j;
      )
  {
    // Search with (i) for next negative ray from front.
    while(i<j && i->getVal() >= 0)
    {
      ++i;
    }
	
    // Search with (j) for next nonnegative ray from end.
    //  Move negative rays to (negRays).
    while(i<j && (j-1)->getVal() < 0)
    {
      negRays.push_back( *(j-1) );
#ifdef DEBUG
      cout << "    Negative uniray: " << *(j-1) << endl;
#endif
      this->erase(j-1);
      
      // Move (j-1) to next candidate.
      j--;
    }
    
    // If necessary, exchange rays.
    if(i < (j-1))
    {
      ddmDualVec tmp;
      
      tmp = *i;
      *i = *(j-1);
      *(j-1) = tmp;
    }
  }
  
  // Now, negRays contains the negative uniRays.
  
  // Compute the new uniRays to add.
  
  // The index of the first new ray is fixed once:
  //   New rays are added at the end.
  int lFirstNewRay = this->size();
  
  for(int posRay = this->mStartOfUniRays;
      // Process only the old rays: Stop at the first new ray.
      posRay < lFirstNewRay;
      ++posRay
      )
  {
    // Fetch an iterator to the current position.
    const_iterator posRayIt = this->begin()+posRay;
    
    // Only strictly positive rays must be tried.
    if((*posRayIt).getVal() > 0)
    {
#ifdef DEBUG
      cout << "  Processing positive " << *posRayIt << endl;
#endif
      // Add the nonredundant linear combination of ray (posRayIt) and
      //   each negative ray to (uniRays).
      for(iterator negRayIt = negRays.begin(); 
	  negRayIt != negRays.end(); 
	  ++negRayIt)
      {
#ifdef DEBUG
	cout << "    Processing negative " << *negRayIt << endl;
#endif
	// Compute incidence vector for the linear combination.
	ddmIncidences newBitvec((*posRayIt).getIncidences() 
				& (*negRayIt).getIncidences());
	
	// We will have an incidence in the current constraint.
	newBitvec.setBit(mNumConstraints);
	
	// Check if the new ray is extremal.
	//   If there already is another uniRay which has 
	//   at least all there incidences where the
	//   new ray has them: Then it is
	//   not extremal and thus redundant.
	{
	  bool redundant = false;
	  
	  // First redundancy check: compute the number of incidences.
	  //   It has to be exactly dSuper-dSup-1+2*pNumEqs, where
	  //    - dSuper is the dimension of the least hyperplane
	  //      containing the current polyhedron.  This is the 'formal' 
	  //      dimension (pFormalDim) (number of system parameters) minus
	  //      the number of equations (numEqs).
	  //    - dSup is the dimenson of the least hyperplane contained in
	  //      the current polyhedron.  This is the number of 
	  //      bidirectional rays. 
	  //    - numEqs is the number of equations fulfilled by the system.  
	  //      All of them are always fulfilled, and incidences to the
          //      equations have to be accounted for somehow.  Since an 
          //      equation is represented by to inequalities in our
          //      representation, they have to be counted twice.
	  //         The term can be simplified via 
	  //            (pFormalDim-pNumEqs-dSup-1+2*pNumEqs) == 
	  //            (pFormalDim-dSup-1+pNumEqs)
	  int lNumIncidences = newBitvec.countOnes();
	  
	  // Our special treatment of the first component might give an 
	  //   additional incidence (I do not really understand if this
          //   is really true).  But it can do no harm to check for strict
          //   inequalities instead of for non-equality.
	  if(lNumIncidences < pFormalDim - this->countBidRays() - 1 + pNumEqs)
	  {
	    // Too small number of incidences: New ray is redundant.
	    redundant = true;
#ifdef DEBUG
	    cout << "      New ray redundant: " 
		 << lNumIncidences 
		 << " incidences != " 
		 << pFormalDim - this->countBidRays() - 1 + pNumEqs
		 << endl
	      ;
#endif
	  }

	  // Next redundancy check: Compare bits. 
	  if(!redundant)
	  {
	    for(iterator redIt = this->begin()+this->mStartOfUniRays; 
		!redundant && redIt != this->end();
		++redIt)
	    {
	      redundant = ddmIncidences::subset(newBitvec, 
						(*redIt).getIncidences());
	    }
	  }
		  
	  if(!redundant)
	  {
	    // New ray is not redundant.
	    
	    // Check in the new rays for some which are made
	    //   redundant by the current one.  Remove them.
	    for(iterator redIt = this->begin()+lFirstNewRay; 
		redIt != this->end();
		++redIt)
	    {
	      if(ddmIncidences::subset((*redIt).getIncidences(), newBitvec))
	      {
		// We found a redundant ray: remove it.
#ifdef DEBUG
		cout << "        Found redundant: " <<  (*redIt) << endl;
#endif
		this->erase(redIt);
		
		redIt--;
	      }
	    }
	    
	    // Compute a linear combination of the positive and
	    //   the negative vector which is incident with the 
	    //   current constraint.
	    ddmVec newRay = 
	      (*posRayIt).getVal() * (*negRayIt)
	      - (*negRayIt).getVal() * (*posRayIt);

	    // Normalize the new ray.
	    newRay.normalize();
	    
	    // Append the new ray to the list of unidirectional rays.
	    //   This invalidates iterators!
	    this->push_back(ddmDualVec(newRay,newBitvec,lNumIncidences));
	    
	    // The 'push_back' might have invalidated (posRayIt).
	    //   Reposition the iterator.
	    posRayIt = this->begin()+posRay;
	    
#ifdef DEBUG
	    cout << "      Added " << *(this->back()) << endl;
#endif
	  } // if(!redundant)
	} // Check redundancy
      } // For all negative rays.
    } // If ray is strictly positive.
  } // For all nonnegative rays.
}

ddmRays::ddmRays(int pDim)
  : ddmMat(),
    mStartOfUniRays(pDim),
    mNumConstraints(0)
{
  for(int i=0; i<pDim; ++i)
  {
    this->push_back(ddmDualVec(ddmVec::mkPosDimVec(i)));
  }
}

// Change a rays list so that it respects another constraint (pConstraint).
//  (pFormalDim) is the formal dimension of the polyhedron.  It is used
//  in recognizing redundant rays.
//  (pEqualities) is the number of equalities fulfilled by the polyhedron.
//  it is also used in recognizing redundant rays.
void
ddmRays::addConstraint(const ddmVec& pConstraint, 
		       int pFormalDim, 
		       int pEqualities)
{
  // Save a non-null bidirectional ray in this iterator.
  iterator nonNullRay = this->end();

  // Compute internal product of (pConstraint) with all vectors.
  // Find first non-null bidirectional on the way.
  {
    // Inner products with all rays.
    for(iterator rayIt = this->begin(); 
	rayIt != this->end();
	++rayIt)
    {
      reprNUMBER prod = (pConstraint) * (*rayIt);;

      (*rayIt).setVal(prod, mNumConstraints);
	  
      // Save first non-null ray in nonNullRay.
      if( 0 != prod 
	  && nonNullRay == this->end())
      {
	nonNullRay = rayIt;
      }
    }
  }

  // Did we find a non-null bidirectional ray?
  if(nonNullRay < this->begin()+this->mStartOfUniRays)
  {
    // Process the bidirectional ray with all rays.
    this->processBidAndUniRays(nonNullRay);
  }
  else
  {
    // Process only the unidirectional rays.
    this->processUniRays(pFormalDim, pEqualities);
  }

  // We processed one more constraint.
  ++mNumConstraints;
}

// Check if all rays fulfill the constraint (p).
bool
ddmRays::fulfill(const ddmVec& p) const
{
  bool result = true;
  
  for(const_iterator it = this->begin();
      result && (it != this->end());
      ++it)
  {
    reprNUMBER prod = p * (*it);
    
    if(it < this->begin()+this->mStartOfUniRays)
    {
      // Bidrays must fulfill the constraint both positively and negatively.
      result = (prod == 0);
    }
    else
    {
      // Unirays must fulfill the constraint positively.
      result = (prod >= 0);
    }
  }
  
  return result;
}

// Add (pNum) dimensions before (pAt).
void
ddmRays::addDimensions(int pAt, int pNum)
{
  // Insert zeroes into the vectors.
  ((ddmMat*)this)->addDimensions(pAt, pNum);

  // Add bidirectional rays for dimensions added.
  for(int i = 0; i != pNum; ++i)
  {
    // Insert bidirectional vector for dimension (pAt+i)
    //  as last bidirectional ray.

      // Construct incidence vector:
    ddmIncidences lInc;
      
    // Set incidence bits for all constraints.
    for(int j=0; j<mNumConstraints; ++j)
    {
      lInc.setBit(j);
    }

    // Insert new ddmDualVec.
    this->insert(this->begin()+mStartOfUniRays, 
		 ddmDualVec(ddmVec::mkPosDimVec(pAt+i),
			    lInc)
		 );

    // We have one more bidRay.
    ++mStartOfUniRays;
  }
}

ostream&  
operator << (ostream& s, const ddmRays& y)
{
  // Print number of rows.
  s << "ddmRays: size " << y.size() << endl;
  
  s << "  bidRays:" << endl;
  
  // Print one ray after the other.
  for(ddmRays::const_iterator it=y.begin();
      it != y.end();
      ++it)
  {
    // Are we at the start of the uniRays?
    if(it == y.begin()+y.getStartOfUniRays())
    {
      s << "  uniRays:" << endl;
    }
    
    s << *it << endl;
  }
  
  s << "end ddmRays" << endl;
  
  return s;
}

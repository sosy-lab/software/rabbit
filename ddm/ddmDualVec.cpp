
#include "ddmDualVec.h"

void
ddmDualVec::setVal(reprNUMBER pVal, int iConstraint/* = -1*/)
{
  mVal = pVal;

  if( (iConstraint != -1) && (pVal==0) )
  {
    mIncidences.setBit(iConstraint);

    // Increment the number of saturations.
    ++mSaturations;
  }
}

void
ddmDualVec::combineWith(ddmDualVec& pRay, 
			int pConstraint)
{
#ifdef DEBUG
  cout << "  Deleting " << *this << endl;
#endif

  this->scale( pRay.getVal() );
  this->add( -mVal, pRay );

  this->normalize();

  mIncidences &= pRay.getIncidences();
  mIncidences.setBit(pConstraint);

  mVal = 0;

#ifdef DEBUG
  cout << "  **** and adding " << *this << endl;
#endif
}

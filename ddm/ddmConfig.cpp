
#include <set>

#include "ddmConfig.h"
#include "ddmAutomaton.h"

// Make a union of all regions in the config.
ddmRegion
ddmConfig::mkRegionUnion() const
{
  ddmRegion result;
  
  for(const_iterator it = this->begin();
      it != this->end();
      ++it)
  {
    result.unite(it->second);
  }
  
  return result;
}

// Unite regions for all states.
//   Append the ddmXPolys which really have been added
//   for each state to (pAdded).
void
ddmConfig::unite(const reprConfig& pConfig, reprConfig* pAddedConfig)
{
  // Because of the polymorph call of this method we know that
  //   the arguments must be for type ddmConfig.
  const ddmConfig* p = pConfig.getDerivedObjDdm();
  ddmConfig* lAdded = NULL;
  if(pAddedConfig)
  {
    lAdded = pAddedConfig->getDerivedObjDdm();
  }

  for(const_iterator it = p->begin();
      it != p->end();
      ++it)
  {
    if(pAddedConfig)
    {
      (*this)[it->first].unite(it->second, &(*lAdded)[it->first]);
    }
    else
    {
      (*this)[it->first].unite(it->second);
    }
    
    // Delete empty items.
    {
      if( (*this)[it->first].isEmpty() )
      {
	this->erase(this->find(it->first));
      }
      
      if(pAddedConfig && (*lAdded)[it->first].isEmpty() )
      {
	lAdded->erase(lAdded->find(it->first));
      }
    }
  }
}

// Check if (*this) contains (p).
bool
ddmConfig::setContains(const reprConfig& pConfig) const
{
  // Because of the polymorph call of this method we know that
  //   the arguments must be for type ddmConfig.
  const ddmConfig* p = pConfig.getDerivedObjDdm();

  // Until now it is ok.
  bool result = true;

  // Check containment for the region associated with each state.
  for(const_iterator it = p->begin();
      result && it != p->end();
      ++it)
  {
    // Look for element for current state.
    const_iterator itThis = this->find(it->first);

    if(itThis == this->end())
    {
      // There is no region for the state (it->first) 
      //   in (*this).  Inclusion is only possible if 
      //   (it->second) is empty.
      result = it->second.isEmpty();
    }
    else
    {
      // There is a region.  Check containment.
      result = itThis->second.setContains(it->second);
    }
  }

  return result;
}

bool
ddmConfig::isEmpty() const
{
  bool result = true;

  // Check that each ddmRegion is empty.
  for(const_iterator it = this->begin();
      result && it != this->end();
      ++it)
  {
    result = it->second.isEmpty();
  }

  return result;
}

//an:Kommentar
void
ddmConfig::intersectEach(const ddmRegion& p)
{
  iterator itNext;

  for(iterator it = this->begin();
      it != this->end();
      it = itNext)
  {
    it->second.intersect(p);

    // Fetch a pointer to next elem
    //  since we might invalidate the current one.
    itNext = it;
    ++itNext;

    if(it->second.isEmpty())
    {
      // This invalidates (it).
      this->erase(it);
    }
  }
}

//an:Kommentar
void
ddmConfig::intersect(const reprConfig& pConfig)
{
  // Because of the polymorph call of this method we know that
  //   the arguments must be for type ddmConfig.
  const ddmConfig* p = pConfig.getDerivedObjDdm();

  // (itNext) is for erase.
  iterator itNext;

  for(iterator itThis = this->begin();
      itThis != this->end();
      itThis = itNext)
  {
    // Intersect (itThis->second).
    
    // Determine next position first.
    itNext = itThis;
    ++itNext;

    // (itP) points into (p).
    const_iterator itP = p->find(itThis->first);

    // Check if (itP) must be used for intersection.
    if(itP != p->end())
    {
      // Intersect with (itP).
      itThis->second.intersect(itP->second);
      
      // Delete empty items.
      if( itThis->second.isEmpty() )
      {
	// This invalidates (itThis).
	this->erase(itThis);
      }
    }
    else
    {
      // Delete (*itThis) because the corresponding region 
      //   in (p) is empty, and thus intersection makes this empty.

      // This invalidates (itThis).
      this->erase(itThis);
    }
  }
}

void
ddmConfig::setSubtract(const reprConfig& pConfig)
{
  // Because of the polymorph call of this method we know that
  //   the arguments must be for type ddmConfig.
  const ddmConfig* p = pConfig.getDerivedObjDdm();

  for(const_iterator itP = p->begin();
      itP != p->end();
      ++itP)
  {
    // Subtract (itP->second).
    

    // (itThis) points into (*this).
    iterator itThis = this->find(itP->first);

    // Check if (itThis) must be used for subtraction.
    if(itThis != this->end())
    {
      // Subtract regions.
      itThis->second.setSubtract(itP->second);
      
      // Delete empty items.
      if( itThis->second.isEmpty() )
      {
	// Delete the empty item. This invalidates (itThis).
	this->erase(itThis);
      }
    }
  }
}

// It computes the complement of this configuration set.
reprConfig*
ddmConfig::mkComplement() const
{
  // The number of states of (this) is (this->size())
  //   and the dimension of the region polyhedra is (mDim).
  
  // We construct a new reprConfig of the dimension (lSize),
  // with full regions.
  
  // Construct an empty config. (mDim) is the number of dimensions
  //   of the regions for this configuration. (mStates) is the number
  //   of states of the corresponding automaton.
  ddmConfig* lResult = new ddmConfig(mDim, mStateNum);

  // Full region.
  ddmRegion lReg(mDim, true);
  
  // Construct full config.
  for (int i = 0; i < lResult->getStateNum(); ++i)
  {
    (*lResult)[i] = lReg;
  }

  //cout << *this << endl;
  //cout << "***********************************" << endl;
  //cout << size() << endl;

  // We build the complement.
  lResult->setSubtract(*this);

  //cout << *lResult << endl;

  return lResult;
}


// Normalize all regions.
void
ddmConfig::normalize()
{
  for(iterator it = this->begin();
      it != this->end();
      ++it)
  {
    it->second.normalize();
  }
}

ddmAutomatonOstream&
operator<<(ddmAutomatonOstream& s, const ddmConfig& y)
{
  // Is the next region the first one?    
  bool lFirst = true;
  
  for(ddmConfig::const_iterator
	it = y.begin();
      it != y.end();
      ++it)
  {
    // Only non-empty regions are printed.
    if(! it->second.isEmpty())
    {
      // All but the first region are prefixed with "OR".
      if(lFirst)
      {
	lFirst = false;
      }
      else
      {
	s << endl << " OR ";
      }
      
      // We don't know the name of the automaton here.
      s << "(STATE(_) = " << s.getStateIds()[it->first]
	<< ") AND (";
      s << it->second << ")";
    }
  }
  
  if(lFirst)
  {
    // There was no nonempty region.
    s << "FALSE";
  }

  s << endl;  
  return s;
}

ostream&
operator<<(ostream& s, const ddmConfig& y)
{
  s << "ddmConfig:\n";

  for(ddmConfig::const_iterator it = y.begin();
      it != y.end();
      it++)
  {
    s << "  State " << it->first << ":" << it->second << endl; 
  }

  s << "End ddmConfig\n";

  return s;
}

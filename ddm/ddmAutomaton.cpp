
//#define DEBUG
#include <vector>

#include "ddmAutomaton.h"
#include "ddmConfigPairList.h"
#include "ctaIntSet.h"
#include "ddmTransition.h"


#include "ctaMap.h"
#include "ctaComponent.h"
#include "ctaSystem.h"


#include "ctaParser.h"
#include "utilCmdLineOptions.h"
#include "utilProgress.h"

// For effort information.
int transitionCounter = 0;



// Construct a configuration consisting of all time followers
//   of the regions in (pConfig).
reprConfig*
ddmAutomaton::mkTimeFollower(const reprConfig* const p) const
{
  // Because of the polymorph call of this method we know that
  //   the argument must be for type ddmConfig.
  const ddmConfig* const pConfig = p->getDerivedObjDdm();

  // Initialize an empty result.
  ddmConfig* result = new ddmConfig(pConfig->getDim(),
                    this->mStates.size());
  
  for(map<int, ddmRegion>::const_iterator 
        it = pConfig->begin();
      it != pConfig->end();
      ++it)
  {
    if(!it->second.isEmpty())
    {
      // Fetch follower region.
      ddmRegion* lTmp 
    = ddmRegion::mkTimeFollower(it->second, 
                    mStates[it->first]
                    .getDerivs(), 
                    mStates[it->first]
                    .getInvariant());

      // If region is nonempty, insert into result.
      if(!lTmp->isEmpty())
      {
    (*result)[it->first] = *lTmp;
      }
      delete lTmp;
    }
  }
  
  return result;
}

reprConfig*
ddmAutomaton::mkTimePrecursor(const reprConfig* const p) const
{
  // Because of the polymorph call of this method we know that
  //   the argument must be for type ddmConfig.
  const ddmConfig* const pConfig = p->getDerivedObjDdm();

  // Construct a configuration consisting of all time precursors
  //   of the regions in pConfig.
  
  // Initialize an empty result.
  ddmConfig* result = new ddmConfig(pConfig->getDim(),
                    this->mStates.size());
  
  for(map<int, ddmRegion>::const_iterator 
    it = pConfig->begin();
      it != pConfig->end();
      ++it)
  {
    if(!it->second.isEmpty())
    {
      // Fetch precursor region.
      ddmRegion* lTmp 
    = ddmRegion::mkTimePrecursor(it->second, 
                     mStates[it->first]
                     .getDerivs(), 
                     mStates[it->first]
                     .getInvariant());
      
      // If region is nonempty, insert into result.
      if(!lTmp->isEmpty())
      {
    (*result)[it->first] = *lTmp;
      }
      delete lTmp;
    }
  }
  
  return result;
}


// Construct a configuration consisting of all transition followers
//   of the regions in pConfig.
//   (pInitiated) is true if we analyse a closed system; if we analyse
//   an open system it is false.
reprConfig*
ddmAutomaton::mkTransitionFollower(const reprConfig* const p,
                   const bool pInitiated) const
{
  // Because of the polymorph call of this method we know that
  //   the argument must be for type ddmConfig.
  const ddmConfig* const pConfig = p->getDerivedObjDdm();

  // Initialize an empty result.
  ddmConfig* result = new ddmConfig(pConfig->getDim(),
                    this->mStates.size());
  // Process all transitions.
  for(vector<ddmTransition*>::const_iterator 
      it = mTransitions.begin();
      it != mTransitions.end();
      ++it)
  {
    // Look if there is a nonempty region for the current
    //  transition precursor.
    ddmConfig::const_iterator lIt = pConfig->find((*it)->getPrecursor());

    if(lIt != pConfig->end())
    {
      // It is an invariant of (ddmConfig)
      //   that lIt->second.isEmpty() can not be fulfilled!

      // Process current transition.

      ++ transitionCounter;

      // Fetch follower region.
      ddmRegion* lTmp 
    = ddmRegion::mkTransitionFollower(
              lIt->second,
              (pInitiated 
               ? (*it)->getInitiatedGuardedAssignment()
               : (*it)->getAllowedGuardedAssignment()),
              (*it)->getSync()
              );
      
      // Unite with nonempty regions.
      if(!lTmp->isEmpty())
      {
    (*result)[(*it)->getFollower()].unite(*lTmp);
      }
      delete lTmp;
    }
  }
  
  return result;
}

reprConfig*
ddmAutomaton::mkTransitionPrecursor(const reprConfig* const p,
                    const bool pInitiated) const
{
  // Because of the polymorph call of this method we know that
  //   the argument must be for type ddmConfig.
  const ddmConfig* const pConfig = p->getDerivedObjDdm();

  // Construct a configuration consisting of all transition precursors
  //   of the regions in pConfig.
  
  // Initialize an empty result.
  ddmConfig* result = new ddmConfig(pConfig->getDim(),
                    this->mStates.size());
  
  for(vector<ddmTransition*>::const_iterator 
    it = mTransitions.begin();
      it != mTransitions.end();
      ++it)
  {
    // Look if there is a nonempty region for the current state.
    ddmConfig::const_iterator lIt = pConfig->find((*it)->getFollower());

    if(lIt != pConfig->end()
       && !lIt->second.isEmpty())
    {
      // Process current transition.

      // Fetch precursor region.
      ddmRegion* lTmp 
    = ddmRegion::mkTransitionPrecursor(
               lIt->second,
               (pInitiated 
                ? (*it)->getInitiatedGuardedAssignment()
                : (*it)->getAllowedGuardedAssignment()),
               (*it)->getSync()
               );

      // Unite with nonempty regions.
      if(!lTmp->isEmpty())
      {
    (*result)[(*it)->getPrecursor()].unite(*lTmp);
      }
      delete lTmp;
    }
  }
  
  return result;
}


// For implementation check only.
// Construct a configuration consisting of all transition followers
//   of (pConfig) also being allowed by (pSyncLab).  Also null-transitions
//   are allowed, as far as they are allowed by (pSyncLab).
reprConfig*
ddmAutomaton::mkTransitionFollower(const reprConfig& pConf,
                   const int pSyncLab,
                   const reprAutomaton& pAutoSpec,
                   const bool pInitiated) const
{
  // Because we know that we handle (ddmConfig)s we can use this cast.
  const ddmConfig& pConfig = * pConf.getDerivedObjDdm();

  // Initialize an empty result.
  ddmConfig* result = new ddmConfig(pConfig.getDim(),
                    this->mStates.size());
  
  // Try out all transitions.
  for(vector<ddmTransition*>::const_iterator 
    itTrans = this->getTransitions().begin();
      itTrans != this->getTransitions().end();
      ++itTrans)
  {
    // Check that the sync labels of the transitions fit.
    //   A label of a transition on which the partner automaton must
    //   sync must be equal to the sync label in the partner transition.
    if(pSyncLab == (*itTrans)->getSync() // This is for speed.
       || (this->allowsForeignSync(pSyncLab, (*itTrans)->getSync())
       && pAutoSpec.allowsForeignSync((*itTrans)->getSync(), pSyncLab))
       )
    {
      // Look if there is a nonempty region for the current precursor state.
      ddmConfig::const_iterator lIt = pConfig.find((*itTrans)->getPrecursor());

      if(lIt != pConfig.end()
     && !lIt->second.isEmpty())
      {
    // Process current transition for current region (lIt).

    // Construct region.
    ddmRegion* lTmp 
      = ddmRegion::mkTransitionFollower(
                lIt->second,
                (pInitiated 
                 ? (*itTrans)->getInitiatedGuardedAssignment()
                 : (*itTrans)->getAllowedGuardedAssignment()),
                (*itTrans)->getSync()
                );
    
    // Unite (*result) with nonempty regions.
    if(!lTmp->isEmpty())
    {
      (*result)[(*itTrans)->getFollower()].unite(*lTmp);
    }
    delete lTmp;
      }
    }
  }
  
  return result;
}

reprConfig*
ddmAutomaton::mkTransitionPrecursor(const reprConfig& pConf,
                    const int pSyncLab,
                    const reprAutomaton& pAutoSpec,
                    const bool pInitiated) const
{
  // Only for easy handling and understanding.
  const ddmAutomaton& pAuto = *this;

  // Because we know that we handle (ddmConfig)s we can use this cast.
  const ddmConfig& pConfig = * pConf.getDerivedObjDdm();

  // Construct a configuration consisting of all transition precursors
  //   of (pConfig) also being allowed by (pSyncLab).  Also null-transitions
  //   are allowed, as far as they are allowed by (pSyncLab).
  

  // Initialize an empty result.
  ddmConfig* result = new ddmConfig(pConfig.getDim(),
                    this->mStates.size());

  // Try out all transitions.
  for(vector<ddmTransition*>::const_iterator 
    itTrans = pAuto.getTransitions().begin();
      itTrans != pAuto.getTransitions().end();
      ++itTrans)
  {
    // Check that the sync labels of the transitions fit.
    //   A label of a transition on which the partner automaton must
    //   sync must be equal te the sync label in the partner transition.
    if(pSyncLab == (*itTrans)->getSync() // This is for speed.
       || (pAuto.allowsForeignSync(pSyncLab, (*itTrans)->getSync())
       && pAutoSpec.allowsForeignSync((*itTrans)->getSync(), pSyncLab))
       )
    {
      // Look if there is a nonempty region for the current follower state.
      ddmConfig::const_iterator lIt = pConfig.find((*itTrans)->getFollower());

      if(lIt != pConfig.end()
     && !lIt->second.isEmpty())
      {
    // Process current transition for current region (lIt).

    // Construct region.
    ddmRegion* lTmp 
      = ddmRegion
      ::mkTransitionPrecursor(
              lIt->second,
              (pInitiated 
               ? (*itTrans)->getInitiatedGuardedAssignment()
               : (*itTrans)->getAllowedGuardedAssignment()),
              (*itTrans)->getSync()
              );
    
    // Unite (*result) with nonempty regions.
    if(!lTmp->isEmpty())
    {
      (*result)[(*itTrans)->getPrecursor()].unite(*lTmp);
    }
    delete lTmp;
      }
    }
  }
  
  return result;
}


//   (pSteps) is the number of maximal steps to use.
//   It is decremented in each cycle.  If a fixpoint is reached,
//   the return value is >= 0; if no fixpoint is reached, the 
//   return value is -1.
reprConfig*
ddmAutomaton::mkReachable(reprAutomaton_ReachDir pTag,
              const reprConfig* const pConfig, 
              int& pSteps) const
{
  ddmConfig* result = new ddmConfig(* pConfig->getDerivedObjDdm());

  // We use (lAdded) as starting point for reachability steps.
  ddmConfig* lAdded = new ddmConfig(* pConfig->getDerivedObjDdm());

  // At least one run through the while loop.
  bool lChange = true;
  
  // For progress output. For 1 event a '*' as output.
  utilProgress *lProgress = new utilProgress('*', 1);

  while(pSteps>0 && lChange)
  {
    // For progress output.
    lProgress->GiveEvent();

    // Next round starts.
    --pSteps;

    reprConfig* tmp1 = NULL;
    reprConfig* tmp2 = NULL;
    
    switch(pTag)
    {
    case mFORWARD:
      // Compute time-followers.  Zero-time is not needed,
      //   this would not add anything to the reachable set.
      tmp1 = this->mkTimeFollower(lAdded);

      // Compute transition reach.
      //   Use initiated changes.
      tmp2 = this->mkTransitionFollower(lAdded, true);

      break;
    case mBACKWARD:
      // Compute time-followers.  Zero-time is not needed,
      //   this would not add anything to the reachable set.
      tmp1 = this->mkTimePrecursor(lAdded);

      // Compute transition reach.
      //   Second parameter is to use initiated changes.
      tmp2 = this->mkTransitionPrecursor(lAdded, true);

      break;
    }
    // Unite the (result) which has the result so far 
    //   with the next result.
    //   Compute the "added configuration" on the way.

    // Reset (lAdded) to empty.
    delete lAdded;
    lAdded = new ddmConfig(mVarIds.size(),
               this->mStates.size());

    // Unite (result) with (tmp1) and (tmp2), storing truly added
    //   ddmXPolys in (lAdded).
    result->unite(*tmp1, lAdded);
    result->unite(*tmp2, lAdded);

    delete tmp1;
    delete tmp2;

    lChange = !lAdded->isEmpty();
  }
  delete lAdded;

  // For progress output.
  delete lProgress;

  // Tell caller how many steps were used.
  //   (No fixpoint reached.)
  if(lChange)
  {
    pSteps = -1;
  }
  
  return result;
}


// Compute reachable config from (pNewReachable),
//   putting the result also into (pReachable).
//   Suppose that (pReachable) already has been 
//   recognized as reachable.
//   Do at most (pMaxSteps) steps.
//   (pFixpointReached) is true if fixpoint has been reached.

// At a call we have to set (pNewReachable) to the region
//   from which we start. The states reachable from (pNewReachable)
//   are added to (pReachable). In (pNewReachable) the states which
//   have been newly reached in the last cycle are returned.

void
ddmAutomaton::computeReachabilityOld(reprConfig* pReachable,
                     reprConfig* pNewReachable,
                     int pMaxSteps,
                     bool &pFixpointReached) const
{
  // We need the derived class.
  // (lNewReachable) means the same object as (pNewReachable)!
  ddmConfig* lNewReachable = pNewReachable->getDerivedObjDdm();

  // For progress output. For 1 event a '*' as output.
  utilProgress *lProgress = new utilProgress('*', 1);

  while(!pFixpointReached && pMaxSteps)
  {
    // For progress output.
    lProgress->GiveEvent();

    // We do one more step.
    --pMaxSteps;

    /*
    // For performance study only.
    cerr << lNewReachable->size() << ":" 
     << pReachable->getDerivedObjDdm()->size() << endl;
    */

    // Attention using time followers:
    //   The resulting config set is not a superset of the
    //   current config, because the intersection
    //   with the invariant might throw out some parts
    //   of the current configurations.
    // That's the reason for uniting also (lTmp2) to (pReachable).
    
    reprConfig* lTmp2 = mkTransitionFollower(lNewReachable, true);
    
    reprConfig* lTmp3 = mkTimeFollower(lTmp2);
    
    // Initialize an empty config which receives the newly reached configs.
    lNewReachable->clear();

    // Add newly reached configs from (lTmp2) and (lTmp3)
    // to (pReachable) and to (pNewReachable).
    pReachable->getDerivedObjDdm()->unite(*lTmp2, lNewReachable);
    pReachable->getDerivedObjDdm()->unite(*lTmp3, lNewReachable);
    
    delete lTmp2;
    delete lTmp3;

    // Did the reachable region change?
    pFixpointReached = lNewReachable->isEmpty();
  }

  // For progress output.
  delete lProgress;

  /*
  // For performance test only.
  for(ddmConfig::iterator it = pReachable->getDerivedObjDdm()->begin();
      it!=pReachable->getDerivedObjDdm()->end();
      ++it)
  {
    if(it->second.size() > 100)
    {
      cerr << mStateIds[it->first] << ": " << endl;

      for(ddmRegion::iterator it1 = it->second.begin();
      it1 != it->second.end();
      ++ it1)
      {
    ddmAutomatonOstream s(&cerr, this);
      
    s << *it1 << endl << endl;
      }
    }
  }
  */

  // (pNewReachable) is changed because it is (lNewReachable).
}


void
ddmAutomaton::computeReachabilityLikeDBM(reprConfig* pReachable,
                         reprConfig* pNewReachable,
                         int pMaxSteps,
                         bool &pFixpointReached) const
{
  // Some preparation.
  //   We need a map from state to transition.
  //   Key is state index, value is transition index.
  multimap<int, int>  lStateToTrans;

  // Initialisation of the map.
  //   Process all transitions.
  for(unsigned int i = 0; i < mTransitions.size(); ++i)
  {
    lStateToTrans.insert(pair<int, int>(
             mTransitions[i]->getPrecursor(), i)
                        );
  }

  // We need the derived class.
  // (lNewReachable) means the same object as (pNewReachable)!
  ddmConfig* lNewReachable = pNewReachable->getDerivedObjDdm();
  // (lReachable) means the same object as (pReachable)!
  ddmConfig* lReachable = pReachable->getDerivedObjDdm();

  // A loop invariant: Each config has to contain its time followers.
  reprConfig* lTmpNewReachable = mkTimeFollower(lNewReachable);
  lNewReachable->unite(* lTmpNewReachable);
  delete lTmpNewReachable;

  pReachable->unite(*lNewReachable);

  // For progress output. For 1 event a '*' as output.
  utilProgress *lProgress = new utilProgress('*', 1);

  while(!lNewReachable->isEmpty() )// && pMaxSteps)
  {
    // For progress output.
    lProgress->GiveEvent();

    // We do one more step.
    --pMaxSteps;

    // Setting the current pair.
    ddmConfig::iterator currentPairIt = lNewReachable->begin();
    int currentState                  = currentPairIt->first;
    ddmRegion currentRegion           = currentPairIt->second;
    // And eliminate it from the set of pairs to handle.
    lNewReachable->erase(currentPairIt);

    //cerr << currentRegion.size() << endl;

    // All transitions of the current state.
    typedef multimap<int, int>::const_iterator MapIt;
    pair<MapIt, MapIt> p = lStateToTrans.equal_range( currentState );
    for( MapIt it = p.first;
     it != p.second;
     ++ it)
    {
      ++ transitionCounter;

      // Fetch iterator of current transition.
      ddmTransition* lTrans = mTransitions[it->second];

      // Process current transition.

      // It is an invariant of (ddmConfig) that (currentRegion) is not empty!
      //   (For empty regions we do not store the pair.)

      // Fetch transition follower region.
      ddmRegion* lTmpRegion
    = ddmRegion::mkTransitionFollower(
              currentRegion,
              lTrans->getInitiatedGuardedAssignment(),
              lTrans->getSync()
              );

      // Fetch time follower region.
      ddmRegion* lTmp1Region = ddmRegion::mkTimeFollower(*lTmpRegion, 
                    mStates[lTrans->getFollower()].getDerivs(), 
                    mStates[lTrans->getFollower()].getInvariant());
      lTmpRegion->unite(*lTmp1Region);
      delete lTmp1Region;

      // Unite nonempty regions to the resulting config (lReachable)
      // and unite the new part to (lNewReachable).
      if(!lTmpRegion->isEmpty())
      {
    // We need a buffer because the operator[] may be gives
    //   us the member (mEmptyRegion) as reference.
    // (lNewRegion) ist for the region newly added to (lReachable).
    ddmRegion lNewRegion = (*lNewReachable) [lTrans->getFollower()];
    (*lReachable)[lTrans->getFollower()]
                 .unite(*lTmpRegion,
                &lNewRegion
                );
    if( !lNewRegion.isEmpty() )
    {
      (*lNewReachable)[lTrans->getFollower()] = lNewRegion;
    }
    else
    {
      lNewReachable->erase(lTrans->getFollower());
    }
      }
      delete lTmpRegion;
    }
  }

  // For progress output.
  delete lProgress;

  // (pNewReachable) is changed because it is (lNewReachable).

  pFixpointReached = lNewReachable->isEmpty();
}


void
ddmAutomaton::computeReachability(reprConfig* pReachable,
                  reprConfig* pNewReachable,
                  int pMaxSteps,
                  bool &pFixpointReached) const
{

  // Mixed Version:

  // Some preparation.
  // We need the derived class.
  // (lNewReachable) means the same object as (pNewReachable)!
  ddmConfig* lNewReachable = pNewReachable->getDerivedObjDdm();
  // (lReachable) means the same object as (pReachable)!
  ddmConfig* lReachable = pReachable->getDerivedObjDdm();

  // A loop invariant: Each config has to contain its time followers.
  reprConfig* lTmpNewReachable = mkTimeFollower(lNewReachable);
  lNewReachable->unite(* lTmpNewReachable);
  delete lTmpNewReachable;

  pReachable->unite(*lNewReachable);

  // For progress output. For 1 event a '*' as output.
  utilProgress *lProgress = new utilProgress('*', 1);

  // Here we store the really new things for the next iteration.
  ddmConfig* lAdded = new ddmConfig(mVarIds.size(),
                    this->mStates.size());

  while(!lNewReachable->isEmpty() )// && pMaxSteps)
  {
    // For progress output.
    lProgress->GiveEvent();

    // We do one more step.
    --pMaxSteps;

    // Process all transitions.
    for(vector<ddmTransition*>::const_iterator 
      it = mTransitions.begin();
    it != mTransitions.end();
    ++it)
    {
      // Look if there is a nonempty region for the current
      //  transition precursor.
      ddmConfig::const_iterator lIt 
    = lNewReachable->find((*it)->getPrecursor());

      if(lIt != lNewReachable->end())
      {
    // It is an invariant of (ddmConfig)
    //   that lIt->second is not empty!

    // Process current transition.

    ++ transitionCounter;

    // Fetch transition follower region.
    ddmRegion* lReachedRegion
      = ddmRegion::mkTransitionFollower(
              lIt->second,
              (*it)->getInitiatedGuardedAssignment(),
              (*it)->getSync()
              );

    // Just for shortcut.
    const int lFollowerState = (*it)->getFollower();
      
    // Fetch time follower region.
    ddmRegion* lHelpRegion = ddmRegion::mkTimeFollower(*lReachedRegion, 
                    mStates[lFollowerState].getDerivs(), 
                    mStates[lFollowerState].getInvariant());
    lReachedRegion->unite(*lHelpRegion);
    delete lHelpRegion;

    // Unite nonempty regions to the working config (lNewReachable).
    if(!lReachedRegion->isEmpty())
    {
      // Just for shortcut. Fetch the current 'new added' region.
      ddmRegion* lNew = &( (*lAdded) [lFollowerState] );
      // We have to unite the new regions to the reachable set
      //   und to the 'added' set for next iteration.
      (*lReachable) [lFollowerState] .unite( *lReachedRegion,
                         lNew);
      if( lNew->isEmpty() )
      {
        // Delete from 'added' set if empty.
        lAdded->erase(lAdded->find(lFollowerState));
      }

      // This would dramatically decrease the performance.
      // For remaining transitions of this iteration.
      // (*lNewReachable) [(*it)->getFollower()] .unite(lNew);

    }
    delete lReachedRegion;
      } // if.
    } // for all transitions.

    // Old values of (lNewReachable) we do not need furthermore.
    // Changing the identifier.
    *lNewReachable = *lAdded;

    lAdded->clear();

  } // while fixpoint not reached.
  
  delete lAdded;

  // For progress output.
  delete lProgress;

  pFixpointReached = lNewReachable->isEmpty();

  // (pNewReachable) is changed because it is (lNewReachable).
}


// Computes, if it is possible to reach (pError) from (pInitial).
bool
ddmAutomaton::isReachable(const reprConfig* const pInitial, 
              const reprConfig* const pError) const
{
  bool lResult = false;
  bool lFixedPointReached = false;
  
  // We use the maximum of 1000 steps.
  int lSteps = 1000;

  reprConfig* lReachable    = new ddmConfig(* pInitial->getDerivedObjDdm());
  reprConfig* lNewReachable = new ddmConfig(* pInitial->getDerivedObjDdm());
  this->computeReachability(lReachable,
                lNewReachable, 
                lSteps,
                lFixedPointReached);

  if(pubCmdLineOpt->Verbose())
  {
    cout << transitionCounter 
     << " discrete transitions used." << endl;
  }

  if( ! lFixedPointReached )
  {
    cout << "Fixed point for reachability not reached!" << endl;
  }

  lReachable->intersect(*pError);
  
  if( ! lReachable->isEmpty() )
  {
    lResult = true;
  }
  
  delete lReachable;
  delete lNewReachable;

  return lResult;
}


// Construct product automaton of (pAuto1) and (pAuto2).
//  If given, store in (pCorrespondingStates) for each state
//  of pAuto1 the numbers of the corresponding states of the product.
ddmAutomaton*
ddmAutomaton::mkProduct(const ddmAutomaton& pAuto1, 
            const ddmAutomaton& pAuto2)
{
  // Determine number of states in the two automata.
  int lLen1 = pAuto1.getStates().size();
  int lLen2 = pAuto2.getStates().size();
  
  // First construct a LARGE version of the automaton.
  
  // Construct initial automaton skeleton. 
  //   (pAuto1) and (pAuto2) have the same result from (getVarIds())
  //   and (getSyncIds()) because they come from the same module.
  ddmAutomaton* result 
    = new ddmAutomaton(pAuto1.mName,
               lLen1*lLen2, 
               pAuto1.getVarIds(), 
               pAuto1.getSyncIds(),
               ctaIntSet::mkUnion(pAuto1.getSyncSet(),
                      pAuto2.getSyncSet())
               );
  
  // Construct initial configuration.
  //   We need this only for the reduction step after
  //   we finished construction of the full product automaton.
  {
    // Assumption:
    // Dimensions of both automata should be the same.
    ddmConfig lInitial(pAuto1.getInitial().getDerivedObjDdm()->getDim(),
               pAuto1.getStates().size());
    
    for(int i1=0; i1<lLen1; ++i1)
    {
      for(int i2=0; i2<lLen2; ++i2)
      {
    // Compute number of current state.
    int lCurState = i1*lLen2+i2;
    
    // Compute initial region for current state as intersection
    //   of regions of input states.
    lInitial[lCurState] = (*pAuto1.getInitial().getDerivedObjDdm())[i1];
    lInitial[lCurState].intersect((*pAuto2.getInitial().getDerivedObjDdm())[i2]);
      }
    }
    
    result->setInitial(lInitial);
  }
  
  // Construct all states.
  for(int i1=0; i1<lLen1; ++i1)
  {
    for(int i2=0; i2<lLen2; ++i2)
    {
      ddmRegion lInvariant = pAuto1.getStates()[i1].getInvariant();
      lInvariant.intersect(pAuto2.getStates()[i2].getInvariant());
      
      ddmRegion lDerivs = pAuto1.getStates()[i1].getDerivs();
      lDerivs.intersect(pAuto2.getStates()[i2].getDerivs());
      
      // Warning for empty derivations.
      if( lDerivs.isEmpty() )
      {
        cout << "Runtime warning: Empty derivation in state "
             << pAuto1.getStates()[i1].getId()
                +"$"
                +pAuto2.getStates()[i2].getId()
             << endl;
      }
      
      result->addState(i1*lLen2+i2, 
               ddmState(pAuto1.getStates()[i1].getId()
                +"$"
                +pAuto2.getStates()[i2].getId(),
                lInvariant,
                lDerivs));
    }
  }
  
  // Construct all transitions.
  {
    // Construct transitions in which only pAuto1 takes a transition.
    for(vector<ddmTransition*>::const_iterator
      it1 = pAuto1.getTransitions().begin();
    it1 != pAuto1.getTransitions().end();
    ++it1)
    {
      // Check that the automaton (pAuto2) does not have to sync on the
      //   current label.

      if(!pAuto2.mustSync((*it1)->getSync()))
      {
    // Try all states of pAuto2.
    for(int i2 = 0; i2 < lLen2; ++i2)
    {
      ddmTransition* tmp
        = new ddmTransition(
                (*it1)->getPrecursor()*lLen2+i2, 
                (*it1)->getFollower()*lLen2+i2,
                (*it1)->getAllowedGuardedAssignment(),
                (*it1)->getUnmentionedVars(),
                (*it1)->getSync()
                );
        
      result->addTransition(tmp);
    }
      }
    }

    // Construct transitions in which only (pAuto2) takes a transition.
    for(vector<ddmTransition*>::const_iterator
      it2 = pAuto2.getTransitions().begin();
    it2 != pAuto2.getTransitions().end();
    ++it2)
    {
      // Check that the automaton (pAuto1) does not have to sync on the
      //   current label.
      if(!pAuto1.mustSync((*it2)->getSync())) 
      {
    // Try all states of pAuto1.
    for(int i1 = 0; i1 < lLen1; ++i1)
    {
      ddmTransition* tmp
        = new ddmTransition(
                i1*lLen2+(*it2)->getPrecursor(), 
                i1*lLen2+(*it2)->getFollower(),
                (*it2)->getAllowedGuardedAssignment(),
                (*it2)->getUnmentionedVars(),
                (*it2)->getSync()
                );
        
      result->addTransition(tmp);
    }
      }
    }

    // Construct transitions in which both automata take 
    //  a transition.
    {
      for(vector<ddmTransition*>::const_iterator
      it1 = pAuto1.getTransitions().begin();
      it1 != pAuto1.getTransitions().end();
      ++it1)
      {
    for(vector<ddmTransition*>::const_iterator
        it2 = pAuto2.getTransitions().begin();
        it2 != pAuto2.getTransitions().end();
        ++it2)
    {
      // Construct the transition for the combination
      //   of (*it1) and (*it2).
    

    // There are four cases.
    // 1.) Both sync labels are identical.
    //     Then the transition is ok.
    // 2.) Both have no sync label.
    //     Then the transition is ok.
    // 3.) One has a sync label, the other not.
    // 3.1.) The other has the label in its alphabet.
    //       Then the transition is not ok.
    // 3.2.) The other has the label not in its alphabet.
    //       Then the transition is ok.
    // 4.) Both have label, but they are not identical.
    //     Then the transition is not ok.

    // Compute a sync-label.
    //   If no sync is possible, set (lSyncError).
      int lSyncLab = -1;
      bool lSyncError;

    // Case 1.) and case 2.)
    // Theoretically we can put together cases 1.), 2.) and 3.),
    // but for speed we deal with 1.) and 2.) separately.
    if( (*it1)->getSync() == (*it2)->getSync() )
    {
      // Sync-labs are identical.  No problem.
      //   This special case is handled for speed.
      lSyncLab = (*it1)->getSync();
      lSyncError = false;


      // Experiment: HyTechs semantics!
      if( lSyncLab == -1 )
      {
        lSyncError = true;
      }
      
    }
    // Case 4.)
    // The labels are different because Case 1.)
    // has already been checked.
    else if(    (*it1)->getSync() > -1
            && (*it2)->getSync() > -1)
    {
      lSyncError = true;      
    }
    
    // Case 3.) We only reach this spot in the program if
    // exactly one of the sync labels is defined.
    else if(    pAuto2.allowsForeignSync((*it1)->getSync(), 
                         (*it2)->getSync() )
            && pAuto1.allowsForeignSync((*it2)->getSync(), 
                        (*it1)->getSync() ))
    {
      // Exactly one sync label is (-1).
      lSyncLab = ( (*it1)->getSync() == -1
               ? (*it2)->getSync()
               : (*it1)->getSync()
               );

      // Experiment: HyTechs semantics!
      lSyncError = true;      
      //lSyncError = false;

    }
    else
    {
      lSyncError = true;          
    }

    // We only have to proceed if no error occured in constructing
    //  (lSyncLab).
    if(!lSyncError)
    {
      // Compute initiated and allowed guarded assignments
      //   for new transition.
      // Compute intersection of allowed guarded assignments.
        ddmRegion lAllowedGA((*it1)->getAllowedGuardedAssignment());
        lAllowedGA.intersect((*it2)->getAllowedGuardedAssignment());
      
      // This transition is never taken if nothing is allowed.
        if(!lAllowedGA.isEmpty())
        {
          // Compute ids of variables which are unmentioned in
          //   both transitions.
          ctaIntSet lBothUnmentioned
          = ctaIntSet::mkIntersection((*it1)->getUnmentionedVars(),
                      (*it2)->getUnmentionedVars());
        
          result
          ->addTransition(new ddmTransition(
                        (*it1)->getPrecursor()*lLen2 
                        + (*it2)->getPrecursor(),
                        (*it1)->getFollower()*lLen2
                        + (*it2)->getFollower(),
                        lAllowedGA,
                        lBothUnmentioned,
                        lSyncLab
                            )
                  );
        }
      }
    }
      }
    }
  }
  
  
  // Now reduce the automaton.


  // Construct a mapping from old to new state numbers.
  //   -1 means: not used any more.  We initialize all
  //  entries to "not used".
  int lTmp = -1;
  vector<int> lMap(lLen1*lLen2, lTmp);
  
  // A counter for the number of states used so far.
  //   It is also the identification number for the next state
  //   of the reduced automaton.
  //   New states are numbered from 0.
  int lStatesUsed = 0;
  
  // We look for reachability WITHOUT looking at the 
  //   exact regions of the configurations and the guards and
  //   the assignments.
  
  // Now, we check for every state if it can be occupied in the 
  //   initial configuration.  
  for(int i=0; i<lLen1*lLen2; ++i)
  {
    if( ! (* result->getInitial().getDerivedObjDdm())[i].isEmpty() )
    {
      lMap[i] = lStatesUsed;
      ++lStatesUsed;
    }
  }
  
  // In a loop, we try the 
  //   transitions from the reachable states to other states.
  //   We stop when no new states have been reached.
  bool lFoundNew = true;
  while(lFoundNew)
  {
    // Did not yet find a new reachable state.
    lFoundNew = false;
    
    // Try out all transitions.
    for(vector<ddmTransition*>::const_iterator 
      it = result->getTransitions().begin();
    it != result->getTransitions().end();
    ++it)
    {
      // Does the current transition lead from 
      //  a used state to an used state?
      if(lMap[(*it)->getPrecursor()] >= 0
     && lMap[(*it)->getFollower()] < 0)
      {
    // We found a new state.  
    lFoundNew = true;
    
    // Give the new state a number.
    lMap[(*it)->getFollower()] = lStatesUsed;
    ++lStatesUsed;
      }
    }
  }

  // Construct the result.
  // We have to set the original sync label set to keep
  //   the blocking sync labels.
  ddmAutomaton* lNewResult = new ddmAutomaton(result->mName,
                          lStatesUsed, 
                          result->getVarIds(), 
                          result->getSyncIds(),
                          result->getSyncSet());
  
  // Construct initial configuration with only used states.
  {
    ddmConfig lNewInitial(pAuto1.getInitial().getDerivedObjDdm()->getDim(),
              pAuto1.getStates().size());

    for(int i=0; i<lLen1*lLen2; ++i)
    {
      // Only used states are looked at.
      if(lMap[i] >= 0)
      {
    lNewInitial[lMap[i]] = (*result->getInitial().getDerivedObjDdm())[i];
      }
    }
    
    // Construct automaton with only used states.
    lNewResult->setInitial(lNewInitial);
  }
  
  // Put States into automaton.
  for(int j=0; j<lLen1*lLen2; ++j)
  {
    if(lMap[j] >= 0)
    {
      lNewResult->addState( lMap[j], result->getStates()[j] );
    }
  }
  
  // Add transitions with renumbered states.
  for(vector<ddmTransition*>::const_iterator 
    it = result->getTransitions().begin();
      it != result->getTransitions().end();
      ++it)
  {
    // Check if transition involves an unused state.
    if(lMap[(*it)->getPrecursor()] >= 0
       && lMap[(*it)->getFollower()] >= 0)
    {
      // Both states are used.  Add a transition to
      //   the new automaton with translated states.
      ddmTransition* tmp
    = new ddmTransition(
                lMap[(*it)->getPrecursor()],
                lMap[(*it)->getFollower()],
                (*it)->getAllowedGuardedAssignment(),
                (*it)->getUnmentionedVars(),
                (*it)->getSync()
                );
    
      lNewResult->addTransition(tmp);
    }
  }

  if(pubCmdLineOpt->Verbose())
  {
    cout << "Product automaton, original/reduced: States " 
     << result->getStates().size()
     << "/" << lNewResult->getStates().size() 
     << ", transitions "
     << result->getTransitions().size()
     << "/" << lNewResult->getTransitions().size() 
     << "."
     << endl;
    
    cout << "We have " << pubCmdLineOpt->GetObjCount()
     << " ctaObjects after this step." << endl;
  }

  delete result;

  // lNewResult contains the reduced result.
  return lNewResult;
}

// Check if this automaton allows to sync on the foreign label (pForeign)
//   if the own transition carries the label (pOwn).
bool
ddmAutomaton::allowsForeignSync(const int pForeign,
                const int pOwn) const
{
  bool result;
  if( this->getSyncSet().find(pForeign) != this->getSyncSet().end() )
  {
    result = ( pForeign == pOwn );
  }
  else
  {
    result = true;
  }
  return result;
}


// Check if (*this) implements (pAuto).
//   If an error has been found, return error examples, otherwise,
//   return an empty list.
//   In (pMaxRounds), return a negative value if nothing can
//   be said, and return a nonnegative value if result is valid.
//   (pReachablePerEquiv) tells how many reachability-steps
//   are done per equiv-step.
//   If result is valid and an error has been found,
//   (pErrorExamples) contains the error pairs.
//   (pInitial) is true if the non-implementation is because
//   of the initial configuration, and it is true if
//   non-implementation is otherwise.
bool
ddmAutomaton::isImplementationProve(const reprAutomaton& pAutoSpec,
                    int& pMaxRounds,
                    const int pReachablePerEquiv,
                    ddmConfigPairList& pErrorExamples,
                    bool& pInitial) const
{
  // This is the implementation of refinement check from Heinrich.

  const int lMaxRounds = pMaxRounds;

  bool lReachableFixpointReached = false;
  ddmConfig lReachable(*this->getInitial().getDerivedObjDdm());
  ddmConfig lNewReachable(*this->getInitial().getDerivedObjDdm());  

  // Determine set of syncs for which (*this) has transitions.
  ctaIntSet lUsedSyncs;
  for(vector<ddmTransition*>::const_iterator 
    it = this->getTransitions().begin();
      it != this->getTransitions().end();
      ++it)
  {
    // Add sync of current transition.
    lUsedSyncs.insert((*it)->getSync());
  }

  // Has a fixpoint been reached in computation of equivalences?
  bool lEquivFixpointReached = false;

  // Construct a configuration pair for the initial configurations.
  ddmConfigPair lInitialPair(* this->getInitial().getDerivedObjDdm(),
                 * pAutoSpec.getInitial().getDerivedObjDdm(),
                 -1);

  // Two registers: Initial full simulation relation, 
  //   and placeholder for next result.
  ddmConfigPairList lAccu[2]
    = 
  {
    ddmConfigPairList::mkFullConfigPairList(*this, pAutoSpec, -1),
    ddmConfigPairList(*this, pAutoSpec)
  };
  
  // An index which points to the current result.
  //   (1-index) points to the *next* register to use.
  int lAccuIndex = 0;

  // We did not yet find an error.
  bool lGotError = false;

  // Go on if we did not yet find an error, the rounds have not
  //  yet been used up, and a fixpoint has not been reached.
  while(!lGotError 
    && pMaxRounds
    && (!lEquivFixpointReached || !lReachableFixpointReached))
  {
    // Compute next reachable config, if we did not reach a fixpoint.
    if(!lReachableFixpointReached)
    {
      // ... using the implementation automaton (this).
      this->computeReachability(&lReachable, &lNewReachable,
                pReachablePerEquiv,          // int
                lReachableFixpointReached);  // bool
    }
    
    // Compute next equiv-generation if we did not yet reach a fixpoint.
    if(!lEquivFixpointReached)
    {
      // Do a split.

      // Clear result accu.
      lAccu[1-lAccuIndex].clear();

      // Try a split.
      bool lHasChanged = false;
      lAccu[lAccuIndex].splitWithTransitions(lAccu[1-lAccuIndex],
                         lMaxRounds - pMaxRounds,
                         lHasChanged,
                         lUsedSyncs);

      // Result accu changed.
      lAccuIndex = 1-lAccuIndex;

      if(!lHasChanged)
      {
    lEquivFixpointReached = true;
      }
    }  // end of: if(!lEquivFixpointReached)

    // Check that for each initial configuration of the implementation,
    //   there is a corresponding initial configuration of
    //   the specification.
    {
      // Get copies of result and initial pair.
      ddmConfigPairList lTmpResult(lAccu[lAccuIndex]);
      ddmConfigPair lTmpInitial(lInitialPair);
      
      // Intersect each pair of result with the initial pair.
      lTmpResult.intersectAndRest(lTmpInitial);

      // Check that correspondents are supersets.
      ddmConfigPairList lDummy(*this, pAutoSpec);
      lGotError
    = 
    !lTmpResult.correspondentsAreSupersets
    (lInitialPair.getImplemConfig(), 
     lDummy);
      
      if(lGotError)
      {
    pInitial = true;
      }
    }

    if(!lGotError)
    {
      // Check if for each configuration of the implementation,
      //   there is a correspondig configuration in the specification.
      //   Append error examples to (pErrorExamples).
      lGotError 
    = !lAccu[lAccuIndex].correspondentsAreSupersets(lReachable, 
                            pErrorExamples);
      if(lGotError)
      {
    pInitial = false;
      }
    }

    // We used up one more round.
    --pMaxRounds;
  } // end of: while 

  // Construct result to return.
  bool result;

  // Did we find the true result?  This is the case if we found
  //   an error, or the iteration reached a fixpoint.
  if(lGotError 
     || (lEquivFixpointReached && lReachableFixpointReached))
  {
    // The true result is in (lGotError). 
    //   (lErrorExample) contains error examples, if they exist.
    result = !lGotError;
  }
  else
  {
    // We did not find the true result in the rounds allocated to us.
    //  Signal this with a negative (pMaxRounds).
    pMaxRounds = -1;
    result = false;
  }

  return result;
}

/*
//------------------------sbs-----------------------------------
void
ddmAutomaton::DeleteLocalSyncs()
{
  ctaMap<ctaComponent*>* lctaComponent 
  = pubParsedSystem->GetFlattenedModules()->find(this->mName)
  ->second->GetInterface();

  int nr = 0;
  for(vector<string>::iterator it = mSyncIds.begin();
      it != mSyncIds.end();
      ++it, ++nr)
  {
    // Is "it" a local sync?
    if (lctaComponent->find((ctaString) *it)
    ->second->GetRestrictionType() == ctaComponent::LOCAL)
    {
      // Set new Sync's on transitions.
      for(vector<ddmTransition*>::const_iterator it1 = mTransitions.begin();
      it1 != mTransitions.end();
      ++it1)
      {
    // Have to change this syncId, to delete the syncId "nr"?
    if((*it1)->getSync() > nr)
    {
      // Set new SyncId.
      (*it1)->setSync( (*it1)->getSync() -1 );
    }
    if((*it1)->getSync() == nr)
    {
      // Set no Sync.
      (*it1)->setSync(-1);
    }
      
      }
      cout << *it << " deleted, cause is local!" << endl;
      // Delete local Sync. 
      it = mSyncIds.erase(it);
      it--;

      // Delete SyncId also in SyncSet.
      mSyncSet.erase(mSyncSet.find(nr));

    }
  }
}
*/

void
ddmAutomaton::print(ostream& pS) const
{
  ddmAutomatonOstream s(&pS, this);

  s << "MODULE " << this->mName << endl;
  s << "{" << endl;

  // Local syncs.
  if(this->getSyncIds().size())
  {
    for(ctaIntSet::const_iterator it = this->getSyncSet().begin();
    it != this->getSyncSet().end();
    it++)
    {
      s << "  LOCAL "
    << this->getSyncIds()[*it]
    << ": SYNC;" << endl;
    }
    s << endl;
  }

  // Local variables.
  if(this->getVarIds().size())
  {
    for(int i=0;
    i < (int)this->getVarIds().size();
    ++i)
    {
      s << "  LOCAL "
    << this->getVarIds()[i]
    << ": ANALOG;" << endl;
    }
    s << endl;
  }


  // Initialization.
  s << "  INITIAL" << endl;
  s << "    ";
  s << * this->getInitial().getDerivedObjDdm();
  s << "    ;" << endl;
  
  s << "  AUTOMATON " << this->mName << endl;
  s << "  {" << endl;
  for(unsigned int i = 0;
      i != this->getStates().size();
      ++i)
  {
    s << this->getStates()[i];
  }
  s << "  }" << endl;
  
  s << "}" << endl;
}

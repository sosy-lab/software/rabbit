
#include "ddmTraces.h"

// Construct a sequence.
ddmTraces
ddmTraces::mkSequence(int pPrefix, const ddmTraces& pPostfix)
{
  ddmTraces result;

  if(pPostfix.isEmptySet())
  {
    // Sequence with empty set is an empty set.
    result = pPostfix;
  }
  else
  {
    // Special handling needed for 
    //   time-transitions (pPrefix == 0).
    if(pPrefix != 0)
    {
      result[pPrefix] = new ddmTraces(pPostfix);
    }
    else
    {
      // Does (pPostfix) allow a time transition
      //   as a start?
      ddmTraces::const_iterator it = pPostfix.find(0);
      if(it == pPostfix.end())
      {
	// No time transition in pPostfix: no special handling needed.
	result[pPrefix] = new ddmTraces(pPostfix);
      }
      else
      {
	// Fetch the postfix of the time transition
	//  in (pPostfix).
	ddmTraces lTmp1((*it->second));
	
	// Fetch a copy of pPostfix and delete the time transition.
	ddmTraces lTmp2(pPostfix);
	lTmp2.erase(lTmp2.find(0));
	
	// Allow (lTmp1) parallel to (lTmp2):
	//   the time-transition prefix will be added
	//   later.
	lTmp2 = mkUnion(lTmp2, lTmp1);
	
	// Combine (pPrefix) with (lTmp2).
	result[pPrefix] = new ddmTraces(lTmp2);
      }
    }
  }

  return result;
}

ddmTraces
ddmTraces::mkUnion(const ddmTraces& p1, const ddmTraces& p2)
{
  ddmTraces result;

  // Nothing to be done if (p2) is the empty set.
  if(p2.isEmptySet())
  {
    result = p1;
  }
  else if(p1.isEmptySet())
  {
    result = p2;
  }
  else
  {
    result = p1;

    for(const_iterator it = p2.begin();
	it != p2.end();
	++it)
    {
      result[it->first] 
      = new ddmTraces(mkUnion( *result[it->first], *it->second));
    }
  }

  return result;
}

ostream&
operator << (ostream& s, const ddmTraces& y)
{
  if(y.isEmptySet())
  {
    s << "{}";
  }
  else
  {
    if(y.size() == 0)
    {
      // We have only the empty trace.
      
      s << "E";
    }
    else
    {
      // There is at least one valid prefix.
      
      for(ddmTraces::const_iterator it = y.begin();
	  it != y.end();
	  ++it)
      {
	if(it != y.begin())
	{
	  s << "+";
	}

	if(it->second->size() > 0)
	{
	  s << it->first << ".";

	  // Put parentheses around expressions with at least
	  //  two alternatives.
	  if(it->second->size() > 1)
	  {
	    s << "(";
	  }
	  
	  s << *it->second;
	  
	  if(it->second->size() > 1)
	  {
	    s << ")";
	  }
	}
      }
    }
  }

  return s;
}

ddmAutomatonOstream&
operator << (ddmAutomatonOstream& s, const ddmTraces& y)
{
  if(y.isEmptySet())
  {
    s << "{}";
  }
  else
  {
    if(y.size() == 0)
    {
      // We have only the empty trace.
      
      s << "E";
    }
    else
    {
      // There is at least one valid prefix.
      
      for(ddmTraces::const_iterator it = y.begin();
	  it != y.end();
	  ++it)
      {
	if(it != y.begin())
	{
	  s << "+";
	}
	
	// Print string representing (it->first).
	s.printOccurringSyncLabel(it->first);
	
	if(it->second->size() > 0)
	{
	  s << ".";
	  
	  // Put parentheses around expressions with at least
	  //  two alternatives.
	  if(it->second->size() > 1)
	  {
	    s << "(";
	  }
	  
	  s << *it->second;
	  
	  if(it->second->size() > 1)
	  {
	    s << ")";
	  }
	}
      }
    }
  }

  return s;
}


#include "ddmIncidences.h"

const int ddmIncidences::mBitsPerInt = 8*sizeof(unsigned int);

bool
ddmIncidences::subset(const ddmIncidences& p1,
		      const ddmIncidences& p2)
{
  // Check that each set bit in (p1) is also set in (p2).

  bool result = true;

  // Check it for each word in (p1).
  for(int i=0;
      result && (i < p1.intSize());
      ++i)
  {
    // Check inclusion for word (i).

    // Is there a corresponding element in (p2)?
    if(i < p2.intSize())
    {
      // There is a corresponding element in (p2).
      result = (p1.mVec[i] == (p1.mVec[i]&p2.mVec[i]));
    }
    else
    {
      // There is no corresponding element in (p2).
      result = (0 == p1.mVec[i]);
    }
  }

  return result;
}

void
ddmIncidences::setBit(int i)
{
  // Compute word number.
  int lWordNum = i/mBitsPerInt;
  
  // Do we have to lengthen (mVec)?
  if(lWordNum >= this->intSize())
  {
    // Lengthen mVec.
    int lNull = 0;
    mVec.resize(lWordNum+1, lNull);
  }

  // Set bit.
  mVec[lWordNum] |= (1 << (i%mBitsPerInt));
}

bool
ddmIncidences::operator[](int i) const
{
  // Is bit represented explicitly?
  if(i/mBitsPerInt >= this->intSize())
  {
    return false;
  }
  else
  {
    return (mVec[i/mBitsPerInt] & (1 << (i%mBitsPerInt)));
  }
}

// Return the number of ones in the incidence vector.
unsigned int
ddmIncidences::countOnes() const
{
  int result = 0;

  // Go through all integers.
  for(int i=0;
      i < (int) mVec.size();
      ++i)
  {
    // Go through all bits in current integer.
    unsigned int lPat;
    int j;
    for(j=0, lPat=1;
	j < mBitsPerInt;
	++j, lPat *= 2)
    {
      if(mVec[i] & lPat)
      {
	++result;
      }
    }
  }

  return result;
}


ddmIncidences
operator &(const ddmIncidences& p1, const ddmIncidences& p2)
{
  int minIntSize = min(p1.intSize(), p2.intSize());

  ddmIncidences result(minIntSize*ddmIncidences::mBitsPerInt);

  for(int i=0; i<minIntSize; ++i)
  {
    result.mVec[i] = p1.mVec[i] & p2.mVec[i];
  }

  return result;
}

ddmIncidences&
operator &= (ddmIncidences& p1, const ddmIncidences& p2)
{
  int minIntSize = min(p1.intSize(), p2.intSize());

  // Process start of (p1).
  for(int i=0; i<minIntSize; ++i)
  {
    p1.mVec[i] &= p2.mVec[i];
  }

  // Throw away rest.
  p1.mVec.resize(minIntSize);

  return p1;
}

ostream&  
operator << (ostream& s, const ddmIncidences& y)
{
  for(int i=0; i<y.bitSize(); ++i)
  {
    s << (y[i] ? "1" : "0");
  }

  return s;
}

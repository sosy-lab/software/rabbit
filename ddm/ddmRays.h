#ifndef _ddmRays_h_
#define _ddmRays_h_


#ifdef DEBUG
#  include <iostream>
#endif

// We represent matrices as STL-vectors of ddmVec's.
// We use bitvectors to represent the incidences.
#include <vector>


// #include "ddmConstraints.h"
class ddmConstraints;

#include "ddmMat.h"


// A class for rays.
class ddmRays : public ddmMat
{
private:
  // It should be not allowed to use the standard operators.
  // Uses the standard operator '='.
  void operator,(const ddmRays&);

private: // Attributes.
  
  int mStartOfUniRays;

  int mNumConstraints;

private: // Private methods.
  
  void
  processBidAndUniRays(iterator nonNullBidRay);

  void
  processUniRays(int pFormalDim, int pNumEqs);

public: // Constructors and destructor.

  // Create a full ray space of dimension (pDim).
  explicit ddmRays(int pDim);

  ddmRays(const ddmRays& p)
    : ddmMat(p),
      mStartOfUniRays(p.mStartOfUniRays),
      mNumConstraints(p.mNumConstraints)
  {}

  ddmRays()
    : ddmMat(),
      mStartOfUniRays(0),
      mNumConstraints(0)
  {}

public: // Accessors.

  int
  getStartOfUniRays() const
  {
    return this->mStartOfUniRays;
  }

public: // Service methods.

  int
  countBidRays() const
  {
    return this->mStartOfUniRays;
  }

  // Change a rays list so that it respects another constraint (pConstraint).
  //  (pFormalDim) is the formal dimension of the polyhedron.  It is used
  //  in recognizing redundant rays.
  //  (pEqualities) is the number of equalities fulfilled by the polyhedron.
  //  it is also used in recognizing redundant rays.
  void
  addConstraint(const ddmVec& pConstraint, 
		int pFormalDim, 
		int pEqualities);

  // Check if all rays fulfill the constraint (p).
  bool
  fulfill(const ddmVec& p) const;

  // Add (pNum) dimensions before (pAT).
  void
  addDimensions(int pAt, int pNum);

public: // Friends.

public: // IO.

  friend ostream&  
  operator << (ostream& s, const ddmRays& y);

};

// Local Variables:
// mode:C++
// End:

#endif // _ddmRays_h_

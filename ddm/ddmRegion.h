#ifndef _ddmRegion_h_
#define _ddmRegion_h_


#include <string>
#include <vector>

#include "ddmAutomatonOstream.h"
#include "ctaIntSet.h"
#include "ddmXPoly.h"

#include "ddmObject.h"

// A (not necessarily convex) region, represented
//   as a vector of ddmXPolys.  There are never any ddmXPolys in 
//   the region for which (isEmpty()) is true.

// Optimization remark: We have to check if we need random access:
// If not we can use lists for speed up the erase (f.e. used in
// ddmRegion::intersect(...) ) operation.

class ddmRegion : public vector<ddmXPoly>, private ddmObject
{
private:
  // It should be not allowed to use the standard operators.
  // Uses the generated copy constructor. 
  // Uses the standard operator '='.
  void operator,(const ddmRegion&);

private: // Private static methods.

public: // Public static methods.

  // Compute projection by existential quantification
  //   of variable from number (pFrom) up to but not including
  //   (pUpTo).
  static ddmRegion
  mkProjection(const ddmRegion& p, int pFrom, int pUpTo);

  // Compute the complement corresponding to (p).
  static ddmRegion
  mkComplement(const ddmXPoly& p);

  // Yield a union for all ddmXPoly-triples of the arguments to which
  //   the function is applied.
  static ddmRegion*
  mkUnionForAllTriples(ddmXPoly (*func)(const ddmXPoly&,
                    const ddmXPoly&,
                    const ddmXPoly&
                    ),
               const ddmRegion& pR1,
               const ddmRegion& pR2,
               const ddmRegion& pR3
               );

  // Yield a union for all ddmXPoly-pairs of the arguments to which
  //   the function is applied.
  static ddmRegion*
  mkUnionForAllPairs(ddmXPoly* (*func)(const ddmXPoly&,
                      const ddmXPoly&,
                      const int pSyncLab
                      ),
             const ddmRegion& pR1,
             const ddmRegion& pR2,
             const int pSyncLab
             );
  
  static ddmRegion*
  mkTimeFollower(const ddmRegion& pCurrent, 
         const ddmRegion& pDerivs, 
         const ddmRegion& pInvariant)
  {
    return mkUnionForAllTriples(ddmXPoly::mkTimeFollower, 
                pCurrent, pDerivs, pInvariant);
  }

  static ddmRegion*
  mkTimePrecursor(const ddmRegion& pCurrent, 
          const ddmRegion& pDerivs, 
          const ddmRegion& pInvariant)
  {
    return mkUnionForAllTriples(ddmXPoly::mkTimePrecursor, 
                pCurrent, pDerivs, pInvariant);
  }

  static ddmRegion*
  mkTransitionFollower(const ddmRegion& pCurrent, 
               const ddmRegion& pAssignments,
               const int pSyncLab)
  {
    return mkUnionForAllPairs(ddmXPoly::mkTransitionFollower,
                  pCurrent, 
                  pAssignments,
                  pSyncLab);
  }

  static ddmRegion*
  mkTransitionPrecursor(const ddmRegion& pCurrent, 
            const ddmRegion& pAssignments,
            const int pSyncLab)
  {
    return mkUnionForAllPairs(ddmXPoly::mkTransitionPrecursor,
                  pCurrent, 
                  pAssignments,
                  pSyncLab);
  }

private: // Attributes.

  // The dimension of the space.
  int mDim;

public: // Constructors and destructor.

  // Necessary for ddmState(), and thus for vector initialization.
  ddmRegion()
    : vector<ddmXPoly >(),
      mDim(0)
  {}

  // Construct a full or empty space of dimension (pDim).
  ddmRegion(int pDim, bool pFull = false)
    : vector<ddmXPoly >(),
      mDim(pDim)
  {
    if(pFull)
    {
      this->push_back(ddmXPoly(mDim));
    }
  }

  ddmRegion(const ddmRegion& p)
    : vector<ddmXPoly >(p),
      mDim(p.mDim)
  {}

  ddmRegion(const ddmXPoly& p)
    : vector<ddmXPoly >(),
      mDim(p.getDim())
  {
    this->push_back(p);
  }

  ddmRegion(int pDim, const ddmVec& p)
    : vector<ddmXPoly >(),
      mDim(pDim)
  {
    // Construct a ddmXPoly with the given constraint.
    ddmXPoly l(mDim);
    l.addConstraint(p);

    this->unite(l);
  }

  ddmRegion(const char* p)
    : vector<ddmXPoly >(),
      mDim(0)
  {
    ddmVec lV(p);

    // Compute dimension from length of vector.
    mDim = lV.size()-ddmXPoly::mNumExtraVars;

    // For the rest, we use the vector normalized.
    lV.normalize();

    ddmXPoly lP(mDim);
    lP.addConstraint(lV);

    this->unite(lP);
  }

  ddmRegion(string p)
    : vector<ddmXPoly >(),
      mDim(0)
  {
    ddmVec lV(p.c_str());

    // Compute dimension from length of vector.
    mDim = lV.size()-ddmXPoly::mNumExtraVars;

    // For the rest, we use the vector normalized.
    lV.normalize();

    ddmXPoly lP(mDim);
    lP.addConstraint(lV);

    this->unite(lP);
  }

public: // Accessors.

  int
  getDim() const
  {
    return this->mDim;
  }

public: // Service methods.

  void
  unite(const ddmXPoly& p, ddmRegion* pAdded = 0);

  // Unite the given region (p) with (*this).
  //  ddmXPoly's really added to (*this) are also united with
  //  (*pAdded).
  void
  unite(const ddmRegion& p, ddmRegion* pAdded = 0);

  void
  intersect(const ddmXPoly& p);

  void
  intersect(const ddmRegion& p);

  bool
  isEmpty() const
  {
    // There are never any empty ddmXPoly's in a region.
    //   Because of this, the emptiness check is simple:
    //   The vector size must be 0.
    return (this->size() == 0);
  }

  // Set-subtract a ddmXPoly.
  void
  setSubtract(const ddmXPoly& p)
  {
    // Intersect (*this) with the complement of (p).
    this->intersect( ddmRegion::mkComplement(p) );
  }

  // Set-subtract a ddmRegion.
  void
  setSubtract(const ddmRegion& p);

  // Compute the complement of a region.
  ddmRegion
  mkComplement() const
  {
    // Construct a full space.
    ddmRegion result(this->getDim(), true);

    // Set-subtract (*this) from the full space.
    result.setSubtract(*this);

    return result;
  }

  // Add (pNum) dimensions before (pAT).
  void
  addDimensions(int pAt, int pNum);

  void
  print(ostream& s, const vector<string>* pVarNames = 0) const;

  // Construct a ddmRegion for the changed signs.
  void 
  changeSigns();

  // Normalize a region by normalizing all elements.
  void 
  normalize();

  // Check if (*this) contains (p) as a set of points.
  bool
  setContains(const ddmRegion& p) const
  {
    ddmRegion lTmp(p);

    lTmp.setSubtract(*this);

    return lTmp.isEmpty();
  }

  // Check if (*this) contains all elements of (p).
  bool
  elemContains(const ddmRegion& p) const;

  bool
  setEqual(const ddmRegion& p) const
  {
    return this->setContains(p) && p.setContains(*this);
  }

  bool
  elemEqual(const ddmRegion& p)
  {
    return this->elemContains(p) && p.elemContains(*this);
  }

public: // Friends.

public: // IO.

  friend ostream&  
  operator << (ostream& s, const ddmRegion& y)
  {
    y.print(s);

    return s;
  }

  friend ddmAutomatonOstream&
  operator<<(ddmAutomatonOstream& s, const ddmRegion& y);
};

// Local Variables:
// mode:C++
// End:

#endif // _ddmRegion_h_

#ifndef _ddmState_h_
#define _ddmState_h_


#include <string>
#include <vector>

#include "ddmObject.h"
#include "ddmAutomatonOstream.h"
#include "ddmRegion.h"
#include "ddmTransition.h"

class ddmState : private ddmObject
{
private:
  // It should be not allowed to use the standard operators.
  // Uses the generated copy constructor. 
  // Uses the standard operator '='.
  void operator,(const ddmState&);

private: // Private static methods.

private: // Attributes.

  // The Id.
  string mId;

  // The invariant.
  ddmRegion mInvariant;

  // The derivations.
  ddmRegion mDerivs;

public: // Constructors and destructor.

  ddmState(string pId,
	   const ddmRegion& pInvariant,
	   const ddmRegion& pDerivs)
    : mId(pId),
      mInvariant(pInvariant),
      mDerivs(pDerivs)
  {}    

  // Necessary for vector initialization.
  ddmState()
    : mId(""), 
      mInvariant(),
      mDerivs()
  {}    

public: // Accessors.

  const string&
  getId() const
  {
    return mId;
  }

  const ddmRegion&  
  getInvariant() const
  {
    return mInvariant;
  }

  const ddmRegion&  
  getDerivs() const
  {
    return mDerivs;
  }

public: // Service methods.

public: // Friends.

public: // IO.

  friend ddmAutomatonOstream&
  operator<<(ddmAutomatonOstream& s, const ddmState& y);
};

bool operator< (ddmState p1, ddmState p2);
bool operator== (ddmState p1, ddmState p2);


// Local Variables:
// mode:C++
// End:

#endif // _ddmState_h_

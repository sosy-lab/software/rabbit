
#include "ddmConstraints.h"

#include "ddmRays.h"

// Sort the constraints to length.
//   db 2001-12-18:
//   Diese Methode ist eine Testmethode von Markus Schulz.
void
ddmConstraints::Sort(int pStartingAt)
{
  int length = this->size();
  // Temporary ddmDualVec for the swap while sorting.
  ddmDualVec lSwapElement;

  for (int i=pStartingAt; i < (length - 1); ++i)
  {
    for (int j = i + 1; j < length; ++j)
    {
      // Swap the ddmConstraints if actual ddmConstraint is shorter
      //   then the test ddmConstraint.
      if ((*this)[j].size() < (*this)[i].size())
      {
	// If ddmConstraint[i/j] an equality then change the number.
	vector<int>::iterator litE1 = mEqualities.begin();
	// Search if the i'th element is an equality.
	while( (litE1 != mEqualities.end()) && (*litE1 != i))
	{
	  litE1++;
	}
	vector<int>::iterator litE2 = mEqualities.begin();
	// Search if the j'th element is an equality.
	while( (litE2 != mEqualities.end()) && (*litE2 != j))
	{
	  litE2++;
	}

	if (litE1 != mEqualities.end())
	{
	  if (litE2 != mEqualities.end())
	  {
	    // both are Equalities, nothing has to change.
	  }
	  else
	  {
	    delete litE1;
	    // Insert j because i and j were swapped and i is an equality.
	    mEqualities.push_back(j);
	  }	  
	}
	else
	{
	  if (litE2 != mEqualities.end())
	  {
	    delete litE2;
	    // Insert i because i and j were swapped and j is an equality.
	    mEqualities.push_back(i);
	  }
	}
	lSwapElement = (*this)[i];
	(*this)[i] = (*this)[j];
	(*this)[j] = lSwapElement;
      }
    }
  }
}

const int ddmConstraints::mNumStdConstraints = 3;
// Remove strictness from the constraints.
void
ddmConstraints::removeStrictness(bool& pChangedSomething)
{
  // Each positive first vector component is replaced by zero.
  for(iterator it = this->begin();
      it != this->end();
      ++it)
  {
    if( (*it)[0] > 0 )
    {
      pChangedSomething = true;
      (*it)[0] = 0;
    }
  }
}

bool
ddmConstraints::include(const ddmVec& p, bool pBidirectional) const
{
  const_iterator cIt;

  // Check if (p) fulfills all constraints of (*this).
  for(cIt = this->begin();
      cIt != this->end();
      ++cIt)
  {
    if(pBidirectional)
    {
      // Bidirectional rays fulfill constraints at most with 0.
      if( p * (*cIt) != 0)
      {
	break;
      }
    }
    else
    {
      if( p * (*cIt) < 0 )
      {
	// Break at first unfulfilled constraint.
	break;
      }
    }
  }
    
  return (cIt == this->end());
}

void
ddmConstraints::addConstraint(const ddmVec& p, bool& pDoProcess)
{
  // We assume that the vector is new.
  pDoProcess = true;

  // Look for (p) in the constraints collected
  //   so far.  If it is in there, set (pDoProcess)
  //   to false.  If its negation is in there,
  //   look if it already is an equality.
  //   If it is, do nothing, and set (pDoProcess) to false.  
  //   If the negation does not yet belong to an equality, 
  //   append the index of the negated vector to
  //   (mEqualities).  (pDoProcess) must be true in this 
  //   case, since the newly added constraint is the second
  //   component of the equality.

  // Construct negation once.
  ddmVec lNeg(-1*p);

  // Search for (p) or (lNeg).
  for(const_iterator it = this->begin();
      it != this->end();
      ++it)
  {
    // We do not have to process copies.
    if(p == (*it))
    {
      pDoProcess = false;
      break;
    }  
    else if(lNeg == (*it))
    {
      // Negation found.  Then this vector belongs to an equation.

      //  Is it already known as an equality?
      if(this->isEquality(it))
      {
	// We do not have to further process this constraint.
	//  It has already been recognized as part of an equality.
	pDoProcess = false;
      }
      else
      {
	// Negation of vector found, but it has not yet
	//  been recognized as an equality.

	// Append index of newly recognized equation to 
	//  (mEqualities).
	mEqualities.push_back(it - this->begin());
      }
      // Break from search loop.
      break;
    }
  }
  // Append constraint if we have to process it.
  if(pDoProcess)
  {
    this->push_back(ddmDualVec(p));
  }
}

// Remove strictness from the constraints.
void
ddmConstraints::removeStrictnessDirectedly(const ddmRays& pDerivs, 
					   bool& pChangedSomething)
{
  // A positive first vector component is replaced by zero
  //   if the vector-products of any element of pDerivs with the
  //   vector is negative.
  for(iterator it = this->begin();
      it != this->end();
      ++it)
  {
    // Do we have a strict inequality?
    if( (*it)[0] > 0 )
    {
      // Strict inequality found.  Does it lie in a
      //  direction reachable by a vector in pDerivs?

      bool lFoundNegative = false;

      // Check for a negative ray.
      for(ddmMat::const_iterator it1 = pDerivs.begin();
	  !lFoundNegative && it1 != pDerivs.end();
	  ++it1)
      {
	// When checking for negative inner product,
	//   interprete first two vector components as zeros.
	//   The first just determines strictness,
	//   the second if there is a point.
	reprNUMBER lProduct =  (*it)*(*it1) - ((*it)[0] * (*it1)[0] 
					      + (*it)[1] * (*it1)[1]);
	lFoundNegative = (lProduct < 0);

	// Check other direction for bidirectional rays.
	//   For these rays, also a positive (lProduct)
	//   tells us that a negative product has been found.
	if(!lFoundNegative 
	   && (it < pDerivs.begin() + pDerivs.getStartOfUniRays()))
	{
	  lFoundNegative = (lProduct > 0);
	}
      }

      if(lFoundNegative)
      {
	pChangedSomething = true;
	(*it)[0] = 0;
      }
    }
  }
}

// Add strictness to the constraints.
void
ddmConstraints::addStrictnessDirectedly(const ddmRays& pDerivs, 
					bool& pChangedSomething)
{
  // A zero first vector component is replaced by one
  //   if the vector-products with all elements of pDerivs with the
  //   vector is positive.
  for(iterator it = this->begin();
      it != this->end();
      ++it)
  {
    // Do we have a nonstrict inequality?
    if( (*it)[0] == 0 )
    {
      // Strict inequality found.  Does it lie in a
      //  direction reachable by a vector in pDerivs?

      bool lAllPositive = true;

      // Check for a non-positive ray.
      //   Bidirectional rays can safely be ignored.
      for(ddmMat::const_iterator 
	    it1 = pDerivs.begin()+pDerivs.getStartOfUniRays();
	  lAllPositive && it1 != pDerivs.end();
	  ++it1)
      {
	// When checking for negative inner product,
	//   interprete first two vector components 
	//   of the derivations as zeros.
	//   The first just determines strictness,
	//   the second if there is a point.
	lAllPositive = (0 <
			(*it)*(*it1) - ((*it)[0] * (*it1)[0] 
					+ (*it)[1] * (*it1)[1]));
      }

      if(lAllPositive)
      {
	// Make constraint strict.
	pChangedSomething = true;
	(*it)[0] = 1;
      }
    }
  }
}

ostream&  
operator << (ostream& s, const ddmConstraints& y)
{
  s << "Begin ddmConstraints: " << endl;
  
  s << (ddmMat) y;
  
  // Print equality indices.
  s << "  equalities (" << y.getEqualities().size() << "): ";
  for(unsigned int i=0; i < y.getEqualities().size(); ++i)
  {
    y.getEqualities()[i];
  }
  s << endl;
  
  s << "End ddmConstraints" << endl;
  
  return s;
}

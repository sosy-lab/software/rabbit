//-*-Mode: C++;-*-

/*
 * File:        anaRegInputErrorState.h
 * Purpose:     An object of this class is to apply if the IPUTERRORSTATES
 *              command is used. Then in the evaluate method is consruct a
 *              config with all exists INPUT_ERROR states of all automata.
 * Author:      A. Heinig
 * Created:     2000
 * Copyright:   (c) 2000, University of Brandenburg, Cottbus
 */

#ifndef _anaRegInputErrorState_h
#define _anaRegInputErrorState_h

#include "anaRegion.h"

class anaRegInputErrorState : public anaRegion
{
private:
  // It should be not allowed to use the standard operators.
  anaRegInputErrorState(const anaRegInputErrorState&);
  void operator=(const anaRegInputErrorState&);
  void operator,(const anaRegInputErrorState&);

public:
  anaRegInputErrorState()
  {}
  
  ~anaRegInputErrorState()
  {}
  
  // Nothing to do, because we complete the state automatically.
  void CheckConsistency(ctaMap<reprConfig*>* const pVariables,
			const ctaModule* pModule)
  {}

  // aheinig 09.04.2000
  // Only true.
  bool
  CheckBddConstraint() const
  {
    return true;
  }
  
  reprConfig* Evaluate(ctaMap<reprConfig*>* const pVariables,
		      const ctaModule* pCtaModule,
		      const reprAutomaton* pAuto)
  { 
    reprConfig* lReprConfigResult = pAuto->mkEmptyConfig();

    // For each automaton ...
    for(ctaMap<ctaAutomaton*>::iterator 
	  itAuto = pCtaModule->GetAutomata()->begin();
	itAuto != pCtaModule->GetAutomata()->end();
	itAuto++)
    { 
      ctaString* lStateName = new ctaString(*itAuto->second->GetName());
      
      typedef string::size_type ST;
      // Find the last '.' .
      ST it = lStateName->rfind('.');
      // If we have the character '.' in the string we erase all characters 
      //  from the begining to the last '.'.
      if(it != string::npos )
      {
	// We must erase also the '.'.
	lStateName->replace(0,it+1,"");
	lStateName->append("_INPUTERROR");
      }

      anaRegState* lAnaRegState = 
	new anaRegState(new ctaString(*itAuto->second->GetName()), 
			new ctaString(*lStateName)
			);   
      
      delete lStateName;

      // Search for the anaRegState (INPUTERROR state) in the Automaton.
      if( itAuto->second->GetStates()->find(
             *lAnaRegState->getConfigState()->GetState() ) != 
	  itAuto->second->GetStates()->end() )
      {
	reprConfig* lConfig = lAnaRegState->Evaluate(pVariables, 
						     pCtaModule, 
						     pAuto);	
	lReprConfigResult->unite(*lConfig);
	delete lConfig;
      }

      delete lAnaRegState;
    }

    return lReprConfigResult;
  }
  
};

#endif

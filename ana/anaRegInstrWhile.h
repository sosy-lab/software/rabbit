//-*-Mode: C++;-*-

/*
 * File:        anaRegInstrWhile.h
 * Purpose:     Implementation of an WHILE command
 * Author:      msc+sbs
 * Created:     1999
 * Copyright:   (c) 1999, University of Brandenburg, Cottbus
 */

#ifndef _anaRegInstrWhile_h
#define _anaRegInstrWhile_h


#include "anaRegInstr.h"
#include "anaBool.h"

class anaRegInstrWhile : public anaRegInstr
{
private:
  // Aggregated.
  anaRegion* mCondition;
  anaRegInstr* mInstr;

  // It should be not allowed to use the standard operators.
  anaRegInstrWhile(const anaRegInstrWhile&);
  void operator=(const anaRegInstrWhile&);
  void operator,(const anaRegInstrWhile&);
  
public:
  anaRegInstrWhile(anaRegion* pCondition,
		   anaRegInstr* pInstr)
    :mCondition(pCondition),
     mInstr(pInstr)
  {}
  
  ~anaRegInstrWhile()
  {
    delete(mCondition);
    delete(mInstr);
  }

  void CheckConsistency(ctaMap<reprConfig*>* const pVariables,
			const ctaModule* pModule)
  {
    mCondition->CheckConsistency(pVariables, pModule);
    mInstr->CheckConsistency(pVariables, pModule);
  }

  // aheinig 09.04.2000
  bool CheckBddConstraint() const
  { 
    return mCondition->CheckBddConstraint() && 
           mInstr->CheckBddConstraint();
  }

  void Execute(ctaMap<reprConfig*>* const pVariables,
	       const ctaModule* pModule,
	       const reprAutomaton* pAuto)
  {
    reprConfig* lCondition = mCondition->Evaluate(pVariables, 
						  pModule, 
						  pAuto);
    reprConfig* lConditionComplement = lCondition->mkComplement();

    while( lConditionComplement->isEmpty() )
    {
      // Full set means TRUE.
      mInstr->Execute(pVariables, pModule, pAuto);
      
      // Preparing next iteration.
      delete lCondition;
      delete lConditionComplement;
      lCondition = mCondition->Evaluate(pVariables, 
						  pModule, 
						  pAuto);
      lConditionComplement = lCondition->mkComplement();
    }

    // Warning?
    if( ! lCondition->isEmpty() )
    {
      cerr << "Runtime warning: WHILE expects a boolean value," << endl
	   << "not a nontrivial set. Skipping WHILE statement." << endl;
    }

    delete lCondition;
    delete lConditionComplement;
  }
};

#endif

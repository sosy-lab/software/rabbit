//-*-Mode: C++;-*-

/*
 * File:        anaRegReachBackward.h
 * Purpose:     Contents the execution of a reachbackward command
 * Created:     1999
 * Copyright:   (c) 1999, University of Brandenburg, Cottbus
 */

#ifndef _anaRegReachBackward_h
#define _anaRegReachBackward_h


#include "anaRegion.h"
#include "reprAutomaton.h"

class anaRegReachBackward : public anaRegion
{
private:
  anaRegion* mOperand;
  int mSteps;
  
  // It should be not allowed to use the standard operators.
  anaRegReachBackward(const anaRegReachBackward&);
  void operator=(const anaRegReachBackward&);
  void operator,(const anaRegReachBackward&);

public:
  anaRegReachBackward(anaRegion* lo, int st)
  {
    mOperand = lo;
    mSteps = st;
  }
  
  ~anaRegReachBackward()
  {
    delete mOperand;
  }
  
  void CheckConsistency(ctaMap<reprConfig*>* const pVariables,
			const ctaModule* pModule)
  {
    mOperand->CheckConsistency(pVariables, pModule);
  }

  // aheinig 09.04.2000
  // only false
  bool
  CheckBddConstraint() const
  {
    return false;
  }

  // The method will return the regions from which these region
  //   can be reached after a specified number of steps.
  reprConfig* Evaluate(ctaMap<reprConfig*>* const pVariables,
		      const ctaModule* pModule,
		      const reprAutomaton *pAuto)
  {
    reprConfig* lOperand = mOperand->Evaluate(pVariables, pModule, pAuto);
    
    int lSteps = mSteps;
    reprConfig* result = pAuto->mkReachable(reprAutomaton::mBACKWARD, 
					    lOperand, 
					    lSteps);
    
    if(pubCmdLineOpt->Verbose())
    {
      if(lSteps >= 0)
      {
	cout << "Fixpoint reached after " << mSteps - lSteps 
	     << " steps." << endl;
      }
      else
      {
	cout << "Fixpoint not reached." << endl;
      }
    }

    delete lOperand;

    return result;
  }
};

#endif

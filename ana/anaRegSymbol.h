//-*-Mode: C++;-*-

/*
 * File:        anaRegRegionSymbol.h
 * Purpose:     Contents the name of a region
 * Created:     1999
 * Copyright:   (c) 1999, University of Brandenburg, Cottbus
 */

#ifndef _anaRegRegionSymbol_h
#define _anaRegRegionSymbol_h


#include "anaRegion.h"

class anaRegSymbol : public anaRegion
{
private:
  ctaString* mRegionName;

  // It should be not allowed to use the standard operators.
  anaRegSymbol(const anaRegSymbol&);
  void operator=(const anaRegSymbol&);
  void operator,(const anaRegSymbol&);
  
public:
  anaRegSymbol(ctaString* pRegionName)
    : mRegionName(pRegionName)
  {}
  
  ~anaRegSymbol()
  {
    delete mRegionName;
  }
  
  void CheckConsistency(ctaMap<reprConfig*>* const pVariables,
			const ctaModule* pModule)
  {
    // Is the variable already declared?
    if( pVariables->find(*mRegionName) == pVariables->end() )
    {
      CTAERR << "Region variable " + *mRegionName + " not declared."
	     << endl;
      CTAPUT;
    }
  }

  // aheinig 09.04.2000
  // only true
  bool
  CheckBddConstraint() const
  {
    return true;
  }

  // This method will return the associated value from map.
  reprConfig* Evaluate(ctaMap<reprConfig*>* const pVariables,
		      const ctaModule* pModule,
		      const reprAutomaton* pAuto)
  {
    // Fetch the iterator pointing to the variable and the region.
    ctaMap<reprConfig*>::const_iterator lRegIt;
    lRegIt = pVariables->find(*mRegionName);

    // Is the variable declared?
    if( lRegIt == pVariables->end() )
    {
      cerr << "Runtime Error: Region variable " << *mRegionName 
	   << " not declared!" << endl;
      return NULL;
    }
    
    // Has the variable a valid region from an assignmant?
    if ( lRegIt->second == NULL )
    {
      cerr << "Runtime Error: Variable " << *mRegionName 
	   << " not initialized!"
	   << endl;
      return NULL;
    }
    
    // The memory behind the return pointer is controlled 
    //   by the caller. Thus we have to copy the (reprConfig).
    return lRegIt->second->clone();
  }
};

#endif

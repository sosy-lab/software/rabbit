//-*-Mode: C++;-*-

/*
 * File:        anaRegState.h
 * Purpose:     Contains the definition of a state
 * Created:     1999
 * Copyright:   (c) 1999, University of Brandenburg, Cottbus
 */

#ifndef _anaRegState_h
#define _anaRegState_h

// For type decision.
#include <typeinfo>

#include "anaRegion.h"

class anaRegState : public anaRegion
{
private:
  ctaConfigState* mConfigState;

  // It should be not allowed to use the standard operators.
  anaRegState(const anaRegState&);
  void operator=(const anaRegState&);
  void operator,(const anaRegState&);

public:
  anaRegState(ctaString* pAutomatonName, ctaString* pState)
  {
    // Find the last '.' in the automaton name.
    ctaString lAutomatonName = *pAutomatonName;
    unsigned int lItPoint = 0;
    for(unsigned int it=0; it < lAutomatonName.size(); it++ )
    {
      if( lAutomatonName[it] == '.' )
      {
	lItPoint = it;
      }
    }
    // Erase all chars after the last '.' in the automaton name. 
    lAutomatonName.erase(lItPoint, lAutomatonName.size());
    // If the automaton name is empty after the erasing, we are in the top
    //  module and so we need no '.', else we use '.' as a seperator for the
    //  state name.
    if( lAutomatonName.size() > 0 )
    {
      lAutomatonName.append(".");
    }
    // We build a new state name which the state names extends 
    //  by the automaton and module name.
    ctaString* lStateName = new ctaString( lAutomatonName.append(*pState) );
    delete pState;

    mConfigState = new ctaConfigState(pAutomatonName, lStateName);  
  }

  ~anaRegState()
  {
    delete mConfigState;
  }
  
  //we'll check if this is a valid state
  void CheckConsistency(ctaMap<reprConfig*>* const pVariables,
			const ctaModule* pModule)
  {
    mConfigState->CheckConsistency(pModule);
  }

  // aheinig 09.04.2000
  // only true
  bool
  CheckBddConstraint() const
  {
    return true;
  }

  ctaConfigState*
  getConfigState()
  {
    return mConfigState;
  }
  
  //This method will return a configuration with infinite
  //  regions for all states but the states in (mConfigState).
  reprConfig* Evaluate(ctaMap<reprConfig*>* const pVariables,
		       const ctaModule* pModule,
		       const reprAutomaton* pAuto)
  { 
    // We have to decide which representation we have to handle.
    if( typeid(*pAuto) == typeid(ddmAutomaton) )
    {
      return mConfigState->mkDdmConfig(*pAuto, pModule).clone();
    }
    else
    {
      if( typeid(*pAuto) == typeid(bddAutomaton) )
      {
	// (mkBddConfig), unlike (mkDdmConfig), needs a SymTab as 1st 
	// parameter. So we have to get it out of (pAuto) in a quite dirty way.
	// A redesign of interfaces would possibly help.
	return mConfigState
	  ->mkBddConfig(pAuto->getDerivedObjBdd()->getSymTab(), pModule)
	  .clone();
      }
      else
      {
	cerr << "Runtime error: Wrong representation automaton given " << endl
	     << "to anaRegLinConstraint::Evaluate(...)" << endl;
	return NULL;
      }
    }
  }
  
};

#endif

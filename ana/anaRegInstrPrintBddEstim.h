//-*-Mode: C++;-*-

/*
 * File:        anaRegInstrPrintBddEstim.h
 * Purpose:     Implementation of a PRINT command
 * Author:      Dirk Beyer and Andy Heinig
 * Created:     2001
 * Copyright:   (c) 2001, University of Brandenburg, Cottbus
 */

#ifndef _anaRegInstrPrintBddEstim_h
#define _anaRegInstrPrintBddEstim_h

#include "utilCmdLineOptions.h"

#include "ctaModule.h"

#include "anaRegInstr.h"
#include "anaRegion.h"

class anaRegInstrPrintBddEstim : public anaRegInstr
{
private:
  // It should be not allowed to use the standard operators.
  anaRegInstrPrintBddEstim(const anaRegInstrPrintBddEstim&);
  void operator=(const anaRegInstrPrintBddEstim&);
  void operator,(const anaRegInstrPrintBddEstim&);

public:
  anaRegInstrPrintBddEstim()
  {}
  
  ~anaRegInstrPrintBddEstim()
  {}
  
  void CheckConsistency(ctaMap<reprConfig*>* const pVariables,
			const ctaModule* pModule)
  {}

  bool CheckBddConstraint() const
  {
    return true;
  }

  void Execute(ctaMap<reprConfig*>* const pVariables,
	       const ctaModule* pModule,
	       const reprAutomaton* pAuto)
  {
    ostream& out = *pubCmdLineOpt->GetOutputStream();
    
    const bddAutomaton* lAuto = pAuto->getDerivedObjBdd();
    if( lAuto == NULL )
    {
      cerr << "The PRINTBDDESTIM operation is only for BDD's." << endl;
      exit(1);
    }

    pModule->calculateNewVariableOrdering( lAuto->getSymTab()->getVariableOrder(), 
					   true, 
					   out);
  }
};

#endif

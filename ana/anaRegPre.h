//-*-Mode: C++;-*-

/*
 * File:        anaRegPre.h
 * Purpose:     Contents the execution of a PRE command
 * Created:     1999
 * Copyright:   (c) 1999, University of Brandenburg, Cottbus
 */

#ifndef _anaRegPre_h
#define _anaRegPre_h


#include "anaRegion.h"
#include "reprAutomaton.h"

class anaRegPre : public anaRegion
{
  
private:
  //the parameter of this operator
  anaRegion* mOperand;

  // It should be not allowed to use the standard operators.
  anaRegPre(const anaRegPre&);
  void operator=(const anaRegPre&);
  void operator,(const anaRegPre&);
  
public:
  
  //a PRE-operator needs only one parameter
  anaRegPre(anaRegion* lo)
  {
    mOperand = lo;
  }
  
  ~anaRegPre()
  {
    delete mOperand;
  }
  
  void CheckConsistency(ctaMap<reprConfig*>* const pVariables,
			const ctaModule* pModule)
  {
    mOperand->CheckConsistency(pVariables, pModule);
  }

  // aheinig 09.04.2000
  // only false
  bool
  CheckBddConstraint() const
  {
    return false;
  }

  // The method returns the union of the time-precursor states with 
  //   the transition-precursor states and 
  //   with the transition-precursor states of
  //   the time-precursor states.
  reprConfig* Evaluate(ctaMap<reprConfig*>* const pVariables,
		      const ctaModule* pModule,
		      const reprAutomaton *pAuto)
  {
    reprConfig* lOperand = mOperand->Evaluate(pVariables, pModule, pAuto);

    // We need only one step.
    int lSteps = 1;
    reprConfig* lResult  = pAuto->mkReachable(reprAutomaton::mBACKWARD, 
					      lOperand, 
					      lSteps);

    delete lOperand;

    return lResult;
  }  
};

#endif

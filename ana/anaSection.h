//-*-Mode: C++;-*-

/*
 * File:        anaSection.h
 * Purpose:     Super class for analysis sections.
 * Author:      Dirk Beyer
 * Created:     2001
 * Copyright:   (c) 2001, University of Brandenburg, Cottbus
 */

#ifndef _anaSection_h
#define _anaSection_h


#include "anaBool.h"
#include "anaRegion.h"

class anaSection : public anaObject
{
private:
  // It should be not allowed to use the standard operators.
  anaSection(const anaSection&);
  void operator=(const anaSection&);
  void operator,(const anaSection&);
  
public:
  anaSection()
  {}
  
  ~anaSection()
  {}

  virtual bool
  CheckBddConstraint() const = 0;

  virtual void 
  CheckConsistency() = 0;

  virtual void 
  Execute() = 0;
};

#endif


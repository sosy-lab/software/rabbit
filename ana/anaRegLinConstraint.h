//-*-Mode: C++;-*-

/*
 * File:        anaRegLinConstraint.h
 * Created:     1999
 * Copyright:   (c) 1999, University of Brandenburg, Cottbus
 */

#ifndef _anaRegLinConstraint_h
#define _anaRegLinConstraint_h

// For type decision.
#include <typeinfo>

#include "anaRegion.h"
#include "ctaConfigLinRest.h"

class anaRegLinConstraint : public anaRegion
{
private:

  // This is the instance which contains the convex predicate.
  ctaConfigLinRest* mConfigLinRest;

  // It should be not allowed to use the standard operators.
  anaRegLinConstraint(const anaRegLinConstraint&);
  void operator=(const anaRegLinConstraint&);
  void operator,(const anaRegLinConstraint&);

public:
  
  // Constructor.
  anaRegLinConstraint(ctaConfigLinRest* pConfigLinRest)
  {	
    mConfigLinRest = pConfigLinRest;
  }

  // the destructor (for deleting variables if set)
  ~anaRegLinConstraint()
  {
    delete mConfigLinRest;
  }
  
  
  // This method gives 'true' if and only if the consistency check 
  //   has been successful.
  
  void CheckConsistency(ctaMap<reprConfig*>* const pVariables,
			const ctaModule* pModule)
  {
    mConfigLinRest->CheckConsistency(pModule);
  }
  

  // aheinig 09.04.2000
  // only true
  bool
  CheckBddConstraint() const
  {
    return true;
  }
  
  // This is the evaluation function. 
  // The first parameter contains a map of the current variable marking,
  // the second one contains the (flattened) CTA-module, and the third one
  // contains the REPR-automaton corresponding to this module.
  // Really necessary are two last parameters (in the case of this class only!)
  reprConfig* Evaluate(ctaMap<reprConfig*>* const Variables,
		    const ctaModule* pModule,
		    const reprAutomaton* pAuto)
  {
    // We have to decide which representation we have to handle.
    if( typeid(*pAuto) == typeid(ddmAutomaton) )
    {
      return mConfigLinRest->mkDdmConfig(*pAuto, pModule).clone();
    }
    else
    {
      if( typeid(*pAuto) == typeid(bddAutomaton) )
      {
	// (mkBddConfig), unlike (mkDdmConfig), needs a SymTab as 1st 
	// parameter. So we have to get it out of (pAuto) in a quite dirty way.
	// A redesign of interfaces would possibly help.
	return mConfigLinRest
	  ->mkBddConfig(pAuto->getDerivedObjBdd()->getSymTab(), pModule)
	  .clone();
      }
    }
    cerr << "Runtime error: Wrong representation automaton given " << endl
	 << "to anaRegLinConstraint::Evaluate(...)" << endl;
    return NULL;
  }
};

#endif

//-*-Mode: C++;-*-

/*
 * File:        anaBoolIsReachable.h
 * Purpose:     Check for an reachability check for two regions.
 * Author:      Dirk Beyer
 * Created:     1999
 * Copyright:   (c) 1999, University of Brandenburg, Cottbus
 */

#ifndef _anaBoolIsReachable_h
#define _anaBoolIsReachable_h


#include "anaBool.h"
#include "anaRegion.h"

class anaBoolIsReachable : public anaBool
{
private:
  // Aggregated.
  anaRegion* mRegionInitial;
  anaRegion* mRegionTarget;

  // It should be not allowed to use the standard operators.
  anaBoolIsReachable(const anaBoolIsReachable&);
  void operator=(const anaBoolIsReachable&);
  void operator,(const anaBoolIsReachable&);
  
public:
  anaBoolIsReachable(anaRegion* pRegionInitial, anaRegion* pRegionTarget)
    :mRegionInitial(pRegionInitial),
     mRegionTarget (pRegionTarget)
  {}
  
  ~anaBoolIsReachable()
  {
    delete(mRegionInitial);
    delete(mRegionTarget);
  }

  // aheinig 09.04.2000
  // only true
  bool
  CheckBddConstraint() const
  {
    return true;
  }

  void CheckConsistency(ctaMap<reprConfig*>* const pVariables,
			const ctaModule* pModule)
  {
    mRegionInitial->CheckConsistency(pVariables, pModule);    
    mRegionTarget->CheckConsistency(pVariables, pModule);
  }

  reprConfig* Evaluate(ctaMap<reprConfig*>* const pVariables,
		       const ctaModule* pModule,
		       const reprAutomaton* pAuto)
  {
    reprConfig* lConfInitial = mRegionInitial->Evaluate(pVariables, 
							pModule, 
							pAuto);
    reprConfig* lConfTarget = mRegionTarget->Evaluate(pVariables, 
						      pModule, 
						      pAuto);
    bool lBoolResult = pAuto->isReachable(lConfInitial, lConfTarget);

    delete lConfInitial;
    delete lConfTarget;

    // Regard the meaning of bool as a set.
    reprConfig* lResult;
    if( lBoolResult )
    {
      // Full config means TRUE.
      reprConfig* lTmpResult = pAuto->mkEmptyConfig();
      lResult = lTmpResult->mkComplement();
      delete lTmpResult;
    }
    else
    {
      // Empty config means FALSE.
      lResult = pAuto->mkEmptyConfig();
    }
    return lResult;
  }
};

#endif


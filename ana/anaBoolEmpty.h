//-*-Mode: C++;-*-

/*
 * File:        anaBoolEmpty.h
 * Purpose:     Check for an empty region.
 * Author:      msc+sbs
 * Created:     1999
 * Copyright:   (c) 1999, University of Brandenburg, Cottbus
 */

#ifndef _anaBoolEmpty_h
#define _anaBoolEmpty_h


#include "anaBool.h"
#include "anaRegion.h"

class anaBoolEmpty : public anaBool
{
private:
  // Aggregated.
  anaRegion* mRegion;

  // It should be not allowed to use the standard operators.
  anaBoolEmpty(const anaBoolEmpty&);
  void operator=(const anaBoolEmpty&);
  void operator,(const anaBoolEmpty&);
  
public:
  anaBoolEmpty(anaRegion* pRegion)
    :mRegion(pRegion)
  {}
  
  ~anaBoolEmpty()
  {
    delete(mRegion);
  }

  void CheckConsistency(ctaMap<reprConfig*>* const pVariables,
			const ctaModule* pModule)
  {
    mRegion->CheckConsistency(pVariables, pModule);
  }

  reprConfig* Evaluate(ctaMap<reprConfig*>* const pVariables,
		       const ctaModule* pModule,
		       const reprAutomaton* pAuto)
  {
    reprConfig* result;

    reprConfig* tmpResult = mRegion->Evaluate(pVariables, pModule, pAuto);
    if (tmpResult->isEmpty())
    {
      // (tmpResult) is an empty set.
      result = tmpResult->mkComplement();
      // We return the full config as true.
    }
    else
    {
      result = pAuto->mkEmptyConfig();
      // We return the empty config as false.
    }
    delete tmpResult;
    return result;
  }

  // aheinig 09.04.2000
  // true:  if CheckBddConstraint true for both sides
  // false: else
  bool
  CheckBddConstraint() const
  {
    return true;
  }
};

#endif


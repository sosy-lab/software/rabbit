//-*-Mode: C++;-*-

/*
 * File:        anaRegUnion.h
 * Purpose:     Contents the execution of an Union command
 * Created:     1999
 * Copyright:   (c) 1999, University of Brandenburg, Cottbus
 */

#ifndef _anaRegUnion_h
#define _anaRegUnion_h


#include "anaRegion.h"

class anaRegUnion : public anaRegion
{
private:
  anaRegion* leftOperand;
  anaRegion* rightOperand;

  // It should be not allowed to use the standard operators.
  anaRegUnion(const anaRegUnion&);
  void operator=(const anaRegUnion&);
  void operator,(const anaRegUnion&);
  
public:
  anaRegUnion(anaRegion* lo, anaRegion* ro)
  {
    leftOperand = lo;
    rightOperand = ro;
  }
  
  ~anaRegUnion()
  {
    delete leftOperand;
    delete rightOperand;
  }
  
  void CheckConsistency(ctaMap<reprConfig*>* const pVariables,
			const ctaModule* pModule)
  {
    leftOperand->CheckConsistency(pVariables, pModule);
    rightOperand->CheckConsistency(pVariables, pModule);
  }

  // aheinig 09.04.2000
  // only true
  bool
  CheckBddConstraint() const
  {
    return true;
  }

  reprConfig* Evaluate(ctaMap<reprConfig*>* const pVariables,
		      const ctaModule* pModule,
		      const reprAutomaton* pAuto)
  {
    reprConfig* lResult = leftOperand->Evaluate(pVariables, 
					       pModule, 
					       pAuto);
    reprConfig* rightOperandConfig = rightOperand->Evaluate(pVariables, 
							   pModule, 
							   pAuto);
    lResult->unite(*rightOperandConfig);

    delete rightOperandConfig;

    return lResult;
  }
};

#endif


//-*-Mode: C++;-*-

/*
 * File:        anaRegInstrPrintFlatModule.h
 * Purpose:     Implementation of an PRINT command for flattened modules.
                This is useful for debugging.
 * Author:      Dirk Beyer
 * Created:     2000
 * Copyright:   (c) 2000, University of Brandenburg, Cottbus
 */

#ifndef _anaRegInstrPrintFlatModule_h
#define _anaRegInstrPrintFlatModule_h


#include "anaRegInstr.h"
#include "anaRegion.h"

class anaRegInstrPrintFlatModule : public anaRegInstr
{
private:
  // It should be not allowed to use the standard operators.
  anaRegInstrPrintFlatModule(const anaRegInstrPrintFlatModule&);
  void operator=(const anaRegInstrPrintFlatModule&);
  void operator,(const anaRegInstrPrintFlatModule&);

public:
  anaRegInstrPrintFlatModule()
  {}
  
  ~anaRegInstrPrintFlatModule()
  {}
  
  void CheckConsistency(ctaMap<reprConfig*>* const pVariables,
			const ctaModule* pModule)
  {}

  bool CheckBddConstraint() const
  { 
    return true;
  }

  // This method prints the flattened module (pModule).
  void Execute(ctaMap<reprConfig*>* const pVariables,
	       const ctaModule* pModule,
	       const reprAutomaton* pAuto)
  {
    *pubCmdLineOpt->GetOutputStream() << pModule << endl;
  }
};

#endif

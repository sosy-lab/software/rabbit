//-*-Mode: C++;-*-

/*
 * File:        anaRegIntersect.h
 * Purpose:     Contents the execution of an Intersect command
 * Created:     1999
 * Copyright:   (c) 1999, University of Brandenburg, Cottbus
 */

#ifndef _anaRegIntersect_h
#define _anaRegIntersect_h


#include "anaRegion.h"

class anaRegIntersect : public anaRegion
{
private:
  // This is a binary operator, so it needs two parameters.
  anaRegion* leftOperand;
  anaRegion* rightOperand;

  // It should be not allowed to use the standard operators.
  anaRegIntersect(const anaRegIntersect&);
  void operator=(const anaRegIntersect&);
  void operator,(const anaRegIntersect&);
  
public:
  anaRegIntersect(anaRegion* lo, anaRegion* ro)
  {
    leftOperand = lo;
    rightOperand = ro;
  }
  
  ~anaRegIntersect()
  {
    delete leftOperand;
    delete rightOperand;
  }
  
  void CheckConsistency(ctaMap<reprConfig*>* const pVariables,
			const ctaModule* pModule)
  {
    leftOperand->CheckConsistency(pVariables, pModule);
    rightOperand->CheckConsistency(pVariables, pModule);
  }

  // aheinig 09.04.2000
  // true:  if both sides of the operand are true
  // false: oterwise
  bool
  CheckBddConstraint() const
  {
    return leftOperand->CheckBddConstraint() &&
           rightOperand->CheckBddConstraint();
  }
  
  reprConfig* Evaluate(ctaMap<reprConfig*>* const Variables,
		      const ctaModule* pModule,
		      const reprAutomaton* pAuto)
  {
    reprConfig* lResult = leftOperand->Evaluate(Variables, 
					       pModule, 
					       pAuto);
    reprConfig* lRightOperandConfig = rightOperand->Evaluate(Variables, 
							    pModule, 
							    pAuto);

    lResult->intersect(*lRightOperandConfig);

    delete lRightOperandConfig;

    return lResult;
  }
  
};

#endif

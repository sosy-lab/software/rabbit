//-*-Mode: C++;-*-

/*
 * File:        anaRegInstrPrintCompletedModule.h
 * Purpose:     Implementation of an PRINT command for flattened modules.
                This is useful for debugging.
 * Author:      Dirk Beyer
 * Created:     2000
 * Copyright:   (c) 2000, University of Brandenburg, Cottbus
 */

#ifndef _anaRegInstrPrintCompletedModule_h
#define _anaRegInstrPrintCompletedModule_h


#include "anaRegInstr.h"
#include "anaRegion.h"

class anaRegInstrPrintCompletedModule : public anaRegInstr
{
private:
  // It should be not allowed to use the standard operators.
  anaRegInstrPrintCompletedModule(const anaRegInstrPrintCompletedModule&);
  void operator=(const anaRegInstrPrintCompletedModule&);
  void operator,(const anaRegInstrPrintCompletedModule&);

public:
  anaRegInstrPrintCompletedModule()
  {}
  
  ~anaRegInstrPrintCompletedModule()
  {}
  
  void CheckConsistency(ctaMap<reprConfig*>* const pVariables,
			const ctaModule* pModule)
  {}

  bool CheckBddConstraint() const
  { 
    return true;
  }

  // This method prints the flattened module (pModule).
  void Execute(ctaMap<reprConfig*>* const pVariables,
	       const ctaModule* pModule,
	       const reprAutomaton* pAuto)
  {
    // Before DDM representation contruction we have to make
    //   some completions on the CTA model (completion of 
    //   the derivation restrictions for different data types).
    // Input error state completion is already done, if wanted.
    // This analysis command is to print out this transformed module.

    ctaModule* lModule = (ctaModule*) pModule;

    lModule->mkDdmCompletions();

    *pubCmdLineOpt->GetOutputStream() << lModule << endl;
  }
};

#endif

//-*-Mode: C++;-*-

/*
 * File:        anaSecReachability.h
 * Purpose:     Container for the reachability instructions.
 * Author:      db
 * Created:     1999
 * Copyright:   (c) 1999, University of Brandenburg, Cottbus
 */

#ifndef _anaSecReachability_h
#define _anaSecReachability_h


#include "anaObject.h"

#include "ctaString.h"
#include "ctaSystem.h"

#include "anaRegInstr.h"
#include "anaRegInstrSeq.h"

#include "reprAutomaton.h"

class ctaModule;

// For type decision.
#include <typeinfo>

class anaSecReachability : public anaSection
{
private: // Attributes.

  // Aggregated.
  anaRegInstr* mRegInstr;

  // The name of the module for the reachability analysis.
  // Aggregated.
  const ctaString* mModuleName;

  // Contains the declared variable names.
  // Contains a map defining current variable and
  //   the corresponding region (if evaluated).
  // Aggregated. Built by parser. Names added by parser and 
  //   regions added and used by Execute().
  ctaMap<reprConfig*>* mVariables;
  
  // The representation to use.
  ctaModule::ReprType mRepr;

  // To decide whether we have to construct input-error-states.
  bool mInputErrorStates;

  // It should be not allowed to use the standard operators.
  anaSecReachability(const anaSecReachability&);
  void operator=(const anaSecReachability&);
  void operator,(const anaSecReachability&);

public: // Methods.

  anaSecReachability(anaRegInstr* pRegInstr,
			 ctaString* pModuleName,
			 set<ctaString>* pVarSet,
			 ctaModule::ReprType pRepr,
		         bool pInputErrorStates)
    : mRegInstr(pRegInstr),
      mModuleName(pModuleName),
      mVariables(new ctaMap<reprConfig*>()),
      mRepr(pRepr),
      mInputErrorStates(pInputErrorStates)
  {
    for(set<ctaString>::iterator
	  it = pVarSet->begin();
	it!= pVarSet->end();
	++it)
    {
      mVariables->append(  & ( *it ) , NULL);
    }
    delete pVarSet;
  }
  
  ~anaSecReachability()
  {
    delete mModuleName;

    delete mRegInstr;

    for(ctaMap<reprConfig*>::iterator
	it = mVariables->begin();
	it != mVariables->end();
	++it)
    {
      // May be the variable is declared but not used.
      if(it->second != NULL)
      {
	delete it->second;
	it->second = NULL;
      }
    }
    // The destructor of ctaMap deletes also the elements.
    //   Thus, do not use it.
    mVariables->erase(mVariables->begin(), mVariables->end());
    delete mVariables;
  }

  // Called from the parser.
  void SetModuleName(ctaString* pModuleName)
  {
    mModuleName = pModuleName;
  }
  
  void CheckConsistency()
  {
    // Check whether the module is defined in the model.
    ctaModule* lModule = pubParsedSystem->findModule(*mModuleName);
    if( lModule == NULL)
    {
      CTAERR << "Module " << mModuleName << " does not exist." << endl;
      CTAPUT;
      exit(1);
    }

    // Check if all interface elements of the analyzed module are LOCAL,
    // except for the SYNC signals. It is allowed to use all restriction types
    // for them. (see also todo task 127)
    ctaForAll_Map(lModule->GetInterface(), lInterfIt, ctaComponent*)
    {
      if(lInterfIt->second->GetRestrictionType() != ctaComponent::LOCAL)
      {
	if(lInterfIt->second->GetDataType() != ctaComponent::SYNC)
	{
	  CTAERR << "It is not allowed to use the non LOCAL variable"
		 << endl << " '" << lInterfIt->first << "' in the"
		 << " analyzed module " << mModuleName << "."
		 << endl;
	  CTAPUT;
	  exit(1);
	}
	else
	{
          cout << "Warning: The SYNC signal '" << lInterfIt->first
	       << "' is used non LOCAL in the"
	       << endl << " analyzed module " << mModuleName << "."
	       << endl;
	}
      } // if
    } // for

    // We have to build an extra flattened module for context check
    //   of the analysis section. (To ensure right use of identifiers.)
    const ctaModule* lFlattenedModule = lModule->FlattenModule();

    // Redeclarations of region variables are found by the call
    //   of ctaMap::append() by the parser. 

    // Check the region commands.
    mRegInstr->CheckConsistency(mVariables, lFlattenedModule);

    delete lFlattenedModule;
  }

  // aheinig 09.04.2000
  bool CheckBddConstraint() const
  { 
    return mRegInstr->CheckBddConstraint();
  }
  
  void Execute()
  {
    ctaModule* lModule = pubParsedSystem->findModule(*mModuleName);
    // Context check ensures (lModule != NULL).

    ctaModule* const lFlatModule = lModule->FlattenModule();
    // Now we have the flat module in (lFlatModule).


    // (INPUTERROR) - states completions.
    if( mInputErrorStates )
    {
      lFlatModule->mkInputErrorStateCompletions();
    }


    // The resulting product automaton which is used for analysis.
    const reprAutomaton* lReprAuto = NULL;

    // Check/set representation.
    this->CheckRepr(lFlatModule);

    switch (mRepr)
    {
      case ctaModule::mDDM:
	// Before representation contruction we have to make
	//   some completions on the CTA model (completion of 
	//   the derivation restrictions for different data types).
	lFlatModule->mkDdmCompletions();

	// Construct DDM representation.
	lReprAuto = lFlatModule->mkDdmAutomaton();
	break;
      case ctaModule::mBDD:
	// Construct BDD representation.
	pubCmdLineOpt->initBDD();
	lReprAuto = lFlatModule->mkBddAutomaton();
	break;
      case ctaModule::mNOREPR:
	// Construct no representation.
	lReprAuto = NULL;
	break;
      default: // It is (mUndef).
	cerr << "Runtime error: No representation (BDD or DDM) choosen." 
	     << endl;
	break;
    }
    
    if(pubCmdLineOpt->Verbose())
    {
      cout << "Reachability check started." << endl;
    }

    // Now we can execute the analysis instruction.
    mRegInstr->Execute(mVariables, lFlatModule, lReprAuto);

    delete lFlatModule;
    delete lReprAuto;

    if( mRepr == ctaModule::mBDD )
    {
      // Uninitialize BDD storage.
      bddBdd::done();
    }
  }  

private:
  // Checks whether representation (mRepr) is allowed for module (pModule).
  //   If (mUnDef) then try set to BDD representation.
  void
  CheckRepr(const ctaModule* const pFlatModule)
  {
    // Firstly, we check whether BDD repr is allowed.
    bool lBddCheckSucceed = pFlatModule->CheckBddRestrictions()  
                            && this->CheckBddConstraint();
    if(pubCmdLineOpt->Verbose() && lBddCheckSucceed)
    {
      // BDD representaion possible.
      cerr << "Check of BDD restrictions was successful." << endl;
    }

    switch (mRepr)
    {
      case ctaModule::mNOREPR:
	if(pubCmdLineOpt->Verbose() && lBddCheckSucceed)
	{
	  // The user forces no representation.
	  cerr << "No representation chosen." << endl
	       << "Analysis not possible." << endl;
	}
	break;
      case ctaModule::mDDM:
	if(pubCmdLineOpt->Verbose() && lBddCheckSucceed)
	{
	  // The user forces ddm representation but
	  //   bdd representation is possible.
	  cerr << "We could also use BDD representation." << endl
	       << "But user forced to use DDM." << endl;
	}
	break;
      case ctaModule::mBDD:
	if(!lBddCheckSucceed)
	{
	  cerr << "Runtime warning: BDD representation forced, "
	       << "but restrictions are not fulfilled." << endl;
	}
	break;
      default: // It is (mUndef) which means that we have to choose.
	if(lBddCheckSucceed)
	{
	  mRepr = ctaModule::mBDD;
	}
	else
	{
	  mRepr = ctaModule::mDDM;
	}
	break;
    }
  }
};

#endif


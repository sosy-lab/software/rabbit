//-*-Mode: C++;-*-

/*
 * File:        anaRegNot.h
 * Purpose:     Contents the execution of a NOT command
 * Created:     1999
 * Copyright:   (c) 1999, University of Brandenburg, Cottbus
 */

#ifndef _anaRegNot_h
#define _anaRegNot_h


#include "anaRegion.h"

class anaRegNot : public anaRegion
{
private:
  anaRegion* mOperand;

  // This flag indicates that this NOT refers to a state. This is only
  //  important for BDD representation.
  bool mRefersToState;

  // It should be not allowed to use the standard operators.
  anaRegNot(const anaRegNot&);
  void operator=(const anaRegNot&);
  void operator,(const anaRegNot&);
  
public:
  // (pState) is only used for remember that the operand is a state
  //   restriction.
  anaRegNot(anaRegion* pOp, bool pState = false)
  {
    mOperand = pOp;
    mRefersToState = pState;
  }

  ~anaRegNot()
  {
    delete mOperand;
  }
  
  // These operation is consistent when it's operand is consistent.
  void CheckConsistency(ctaMap<reprConfig*>* const pVariables,
			const ctaModule* pModule)
  {
    mOperand->CheckConsistency(pVariables, pModule);
  }
  
  // The flag (mRefersToState) is true if this (anaRegNot) refers to a state,
  //  false otherwise.
  bool
  CheckBddConstraint() const
  {
    return mRefersToState;
  }

  // The method will return the complement of the parameter.
  reprConfig* Evaluate(ctaMap<reprConfig*>* const pVariables,
		       const ctaModule* pModule,
		       const reprAutomaton* pAuto)
  {
    // Starting with the operand.
    reprConfig* lTmpResult = mOperand->Evaluate(pVariables, pModule, pAuto);
    reprConfig* lResult = lTmpResult->mkComplement();

    if(!mRefersToState && (typeid(*pAuto) == typeid(bddAutomaton)))
    {
      cerr << "Runtime error: NOT is forbidden in expressions " << endl
	   << "with clocks because of discretization." << endl;
    }

    delete lTmpResult;
    return lResult;
  }
  
};

#endif

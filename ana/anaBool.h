//-*-Mode: C++;-*-

/*
 * File:        anaBool.h
 * Purpose:     Baseclass for the implementation of the boolean instructions.
 * Author:      msc+sbs
 * Created:     1999
 * Copyright:   (c) 1999, University of Brandenburg, Cottbus
 */

#ifndef _anaBool_h
#define _anaBool_h


//#include "anaObject.h"
#include "anaRegion.h"

class anaBool : public anaRegion//anaObject
{

  // We use not a real Boolean type (bool),
  //   instead we use a set of configurations as bool with
  //   the following semantics:
  //   reprConfig r means FALSE   iff r.isEmpty() == TRUE,
  //                      TRUE    iff r.mkComplement().isEmpty() == TRUE,
  //                      UNDEF   otherwise.

public:
  anaBool()
  {}
  
  ~anaBool()
  {}

  virtual void CheckConsistency(ctaMap<reprConfig*>* const pVariables,
				const ctaModule* pModule) = 0;

  virtual reprConfig* Evaluate(ctaMap<reprConfig*>* const pVariables,
			       const ctaModule* pModule,
			       const reprAutomaton* pAuto) = 0;  
  
  // aheinig 09.04.2000
  virtual bool CheckBddConstraint() const = 0;
  
};

#endif


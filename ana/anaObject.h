//-*-Mode: C++;-*-

/*
 * File:        anaObject.h
 * Purpose:     Top Level Object for cta analyse library
 * Created:     1999
 * Copyright:   (c) 1999, University of Brandenburg, Cottbus
 */

#ifndef _anaObject_h
#define _anaObject_h


#include <iostream>

#include "ctaObject.h"

// This is the base class for any other class in the analysis library.
class anaObject : ctaObject
{
  
public:
  /*
  // constructor and destructor
  anaObject()
  {

  }
  
  anaObject(const anaObject& pObj)
  {

  }
  
  virtual ~anaObject()
  {

  }
  */
};

#endif

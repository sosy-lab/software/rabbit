//-*-Mode: C++;-*-

/*
 * File:        anaSecRefinement.h
 * Purpose:     Check for a refinement relation between
                modules P and Q of the module to analyse.
 * Author:      Dirk Beyer
 * Created:     2000
 * Copyright:   (c) 2000, University of Brandenburg, Cottbus
 */

#ifndef _anaSecRefinement_h
#define _anaSecRefinement_h


#include "anaBool.h"
#include "anaRegion.h"

class anaSecRefinement : public anaSection
{
private:

  ctaString mModuleName;

  // The representation to use.
  ctaModule::ReprType mRepr;

  // It should be not allowed to use the standard operators.
  anaSecRefinement(const anaSecRefinement&);
  void operator=(const anaSecRefinement&);
  void operator,(const anaSecRefinement&);
  
public:
  anaSecRefinement(ctaString pModuleName,
		       ctaModule::ReprType pRepr)
    : mModuleName(pModuleName),
      mRepr(pRepr)
  {}
  
  ~anaSecRefinement()
  {}

  // aheinig 09.04.2000
  // only true
  bool
  CheckBddConstraint() const
  {
    return true;
  }

  void CheckConsistency()
  {
    // Check whether the module is defined in the model.
    ctaModule* lModule = pubParsedSystem->findModule(mModuleName);
    if( lModule == NULL)
    {
      CTAERR << "Module " << mModuleName << " does not exist." << endl;
      CTAPUT;
      exit(1);
    }

    // Check whether the module containing P and Q is build
    //   as we require.
    bool isCorrect = false;

    // Check names of the instances.
    ctaMap<ctaInstantiation*>::iterator lInstIt 
      = lModule->GetInstantiations()->begin();
    if(lInstIt->first == "P")
    {
      ++lInstIt;
      if(lInstIt->first == "Q")
      {
	isCorrect = true;
      }
    }
    else
    {
      if(lInstIt->first == "Q")
      {
        ++lInstIt;
        if(lInstIt->first == "P")
        {
          isCorrect = true;
        }
      }    
    }

    // Check number of module instances and number of automata.
    if( lModule->GetInstantiations()->size() != 2 &&
	lModule->GetAutomata()->size() != 0 )
    {
      isCorrect = false;
    }
    
    if(! isCorrect)
    {
      CTAERR << "Module" << mModuleName
	     << "has to contain one module instance named \'P\'" << endl
	     << "and one module instance named \'Q\' for refinement checking."
	     << endl;
      CTAPUT;
      exit(1);    
    }

    /* done mvogel2001-04-02 */
    // No variables in module 'Refinement' are allowed!
    // Interface-Map durchgehen und bei "datatype!=SYNC" Alarm schlagen.
    // Check whether the non-local interface signals
    //   of P and Q are the same (range of ident must be the same)!
    // Die Map der connections in ctaInstantiation durchgehen und alle
    //   Namen in eine Hilfs-Map einsammeln. Dann die Interface-Map
    //   durchgehen und jeden Eintrag in der Hilfsmap l"oschen. Bei
    //   Nichtfinden Alarm schlagen und auch, wenn welche "ubrigbleiben.
    /* ---------- */


    // Check if all interface elements are SYNC and declared LOCAL
    ctaForAll_Map(lModule->GetInterface(), lInterfIt, ctaComponent*)
    {
      if(!(lInterfIt->second->GetDataType() == ctaComponent::SYNC
	   || lInterfIt->second->GetDataType() == ctaComponent::CONST))
      {
	CTAERR << "It is only allowed to use SYNC signals and CONST"
               << " in module " << mModuleName << "." << endl;
        CTAPUT;
	exit(1);
      } // if
      else
      {
	if(lInterfIt->second->GetRestrictionType() != ctaComponent::LOCAL)
	{
	  CTAERR << "It is only allowed to use LOCAL signals in module "
		 << mModuleName << "." << endl;
	  CTAPUT;
	  exit(1);
	}
      }
    } // for
       
    unsigned int lInterfSize = lModule->GetInterface()->size();

    // Check if the interface of the module and the connections of
    // instantiations P and Q contain the same number of signals.
    ctaForAll_Map_const(lModule->GetInstantiations(), lInstIt,
			ctaInstantiation*)
    {
      if(lInterfSize != lInstIt->second->GetConnections()->size())
      {
	CTAERR << "There are local signals, which are not connected" << endl
	       << "in the "
	       << "instantiation " << lInstIt->first << "." << endl;
        CTAPUT;
	exit(1);
      }
    }

    // Check for unused signals in module and all instanciated submodules.
    // This check is too extensive. We only want to ensure that the
    // alphabet of the automata of modules P and Q are the same.
    // We have to think of a check which only fulfills this requirement.

    if (!lModule->CheckForUnusedSignals(true))
    {
      CTAERR << "There are unused signals. (see warnings above)"
	     << endl;
      CTAPUT;
      exit(1);
    }

    //cout << "Context Check of anaImplemInstr not completed!" << endl;
  }

  void Execute()
  {
    if(pubCmdLineOpt->Verbose())
    {
      cout << "Refinement check started." << endl;
    }

    ctaModule* lModule = pubParsedSystem->findModule(mModuleName);

    //if( mRepr != ctaModule::mBDD )
    //{
    //  cout << "Runtime error: Refinement check implemented only for BDD"
    //   << endl << "representation." << endl;
    //}

    if( lModule->CheckSimulationBDD() )
    {
      cout << "Simulation relation exists." << endl;
    }
    else
    {
      cout << "Simulation relation does not exist." << endl;
    }
  }
};

#endif


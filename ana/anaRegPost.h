//-*-Mode: C++;-*-

/*
 * File:        anaRegPost.h
 * Purpose:     Contents the execution of a POST command
 * Created:     1999
 * Copyright:   (c) 1999, University of Brandenburg, Cottbus
 */

#ifndef _anaRegPost_h
#define _anaRegPost_h


#include "anaRegion.h"
#include "reprAutomaton.h"

class anaRegPost : public anaRegion
{
private:
  anaRegion* mOperand;

  // It should be not allowed to use the standard operators.
  anaRegPost(const anaRegPost&);
  void operator=(const anaRegPost&);
  void operator,(const anaRegPost&);
  
public:
  
  anaRegPost(anaRegion* lo)
  {
    mOperand = lo;
  }

  ~anaRegPost()
  {
    delete mOperand;
  }
  
  void CheckConsistency(ctaMap<reprConfig*>* const Variables,
			const ctaModule* pModule)
  {
    mOperand->CheckConsistency(Variables, pModule);
  }
  
  // aheinig 09.04.2000
  bool
  CheckBddConstraint() const
  {
    return mOperand->CheckBddConstraint();
  }
  
  // The method returns the union of the time-follower states with 
  //   the transition-follower states and with the transition-follower 
  //   states of the time-follower states.
  reprConfig* Evaluate(ctaMap<reprConfig*>* const pVariables,
		      const ctaModule* pModule,
		      const reprAutomaton* pAuto)
  {
    reprConfig* lOperand = mOperand->Evaluate(pVariables, pModule, pAuto);
    
    // We need only one step.
    int lSteps = 1;
    reprConfig* lResult  = pAuto->mkReachable(reprAutomaton::mFORWARD, 
					      lOperand, 
					      lSteps);

    delete lOperand;

    return lResult;
  }
};

#endif

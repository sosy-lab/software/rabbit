//-*-Mode: C++;-*-

/*
 * File:        anaRegDifference.h
 * Purpose:     Contents the execution of an difference command
 * Created:     1999
 * Copyright:   (c) 1999, University of Brandenburg, Cottbus
 */

#ifndef _anaRegDifference_h
#define _anaRegDifference_h


#include "anaRegion.h"

class anaRegDifference : public anaRegion
{
private:
  // This is a binary operator, so it needs two parameters.
  anaRegion* leftOperand;
  anaRegion* rightOperand;

  // It should be not allowed to use the standard operators.
  anaRegDifference(const anaRegDifference&);
  void operator=(const anaRegDifference&);
  void operator,(const anaRegDifference&);
  
public:
  anaRegDifference(anaRegion* lo, anaRegion* ro)
  {
    leftOperand = lo;
    rightOperand = ro;
  }
  
  ~anaRegDifference()
  {
    delete leftOperand;
    delete rightOperand;
  }
  
  void CheckConsistency(ctaMap<reprConfig*>* const pVariables,
			const ctaModule* pModule)
  {
    leftOperand->CheckConsistency(pVariables, pModule);
    rightOperand->CheckConsistency(pVariables, pModule);
  }
  
  // aheinig 09.04.2000
  // only false
  bool
  CheckBddConstraint() const
  {
    return false;
  }

  reprConfig* Evaluate(ctaMap<reprConfig*>* const Variables,
		    const ctaModule* pModule,
		    const reprAutomaton* pAuto)
  {
    reprConfig* lResult = leftOperand->Evaluate(Variables, 
					       pModule, 
					       pAuto);
    reprConfig* lRightOperandConfig = rightOperand->Evaluate(Variables, 
							    pModule, 
							    pAuto);

    lResult->setSubtract(*lRightOperandConfig);

    delete lRightOperandConfig;

    return lResult;
  }
  
};

#endif

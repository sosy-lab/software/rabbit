//-*-Mode: C++;-*-

/*
 * File:        anaRegInitial.h
 * Purpose:     Contents the execution of an Initial command
 * Created:     1999
 * Copyright:   (c) 1999, University of Brandenburg, Cottbus
 */

#ifndef _anaRegInitial_h
#define _anaRegInitial_h


#include "anaRegion.h"

class anaRegInitial : public anaRegion
{
  
public:
  anaRegInitial()
  {}
  
  ~anaRegInitial()
  {}
  
  void CheckConsistency(ctaMap<reprConfig*>* const pVariables,
			const ctaModule* pModule)
  {}

  // aheinig 09.04.2000
  // only true
  bool
  CheckBddConstraint() const
  {
    return true;
  }
  
  reprConfig* Evaluate(ctaMap<reprConfig*>* const Variables,
		      const ctaModule* pModule,
		      const reprAutomaton* pAuto)
  {
    return (pAuto->getInitial()).clone();
  }
  
};

#endif

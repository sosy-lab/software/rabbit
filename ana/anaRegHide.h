//-*-Mode: C++;-*-

/*
 * File:        anaRegHide.h
 * Purpose:     
 * Author:      A.Heinig
 * Created:     2000
 * Copyright:   (c) 2000, University of Brandenburg, Cottbus
 */

#ifndef _anaRegHide_h
#define _anaRegHide_h

#include "anaRegion.h"

class anaRegHide : public anaRegion
{
private:
  //This set contains the to hiden variables or automaton or the 
  //  keywords ALLAUTOMATA for hide all automaton or ALLVARIABLES
  //  for hide all variables.
  const set<ctaString>* mHide;

  //This contains the to hiden region.
  anaRegion* mHideRegion;
  
  // It should be not allowed to use the standard operators.
  anaRegHide(const anaRegHide&);
  void operator=(const anaRegHide&);
  void operator,(const anaRegHide&);

public:
  anaRegHide(set<ctaString>* pHide, anaRegion* pHideRegion)
    : mHide(pHide),
      mHideRegion(pHideRegion)
  {}
  
  ~anaRegHide()
  {
    delete mHide;
    delete mHideRegion;
  }
  
  //we must check if the name are declared.
  void CheckConsistency(ctaMap<reprConfig*>* const pVariables,
			const ctaModule* pModule)
  {
    for(set<ctaString>::iterator 
	  itHide = mHide->begin();
	itHide != mHide->end();
	++itHide)
    {
      bool lHideNameIsDeclared = false;
      //Check if the to hiden name a variable.
      ctaForAll_Map(pModule->GetInterface(), itInterface, ctaComponent*)
      {
	if(*itInterface->second->GetName() == *itHide)
	{
	  lHideNameIsDeclared = true;
	}
      }
      //If the to hiden name no variable name, we check if it is the 
      //  keyword ALLVARIALES or the keyword ALLAUTOMATA.
      if(*itHide == "ALLVARIABLES" || *itHide == "ALLAUTOMATA" )
      { 
	lHideNameIsDeclared = true;
      }
      //If the to hiden name no variable name or a keyword we check 
      //  if it is a automaton name.
      ctaForAll_Map(pModule->GetAutomata(), itAutomata, ctaAutomaton*)
      {
	if(*itAutomata->second->GetName() == *itHide)
	{
	  lHideNameIsDeclared = true;
	}
      }
      //If it non of all, we generate a error message
      if(lHideNameIsDeclared == false)
      {
	CTAERR << "Runtime error: " << *itHide << " argument " << endl
	       << "for hide operator are not declared."
	       << endl;        
	CTAPUT;
      }
    }
    //End check consistency for all names the are hided.
    //Check consistency for the committed region (anaRegion).
    mHideRegion->CheckConsistency(pVariables, pModule);
  }
  
  // aheinig 09.04.2000
  // only true
  bool
  CheckBddConstraint() const
  {
    return true;
  }

  reprConfig* Evaluate(ctaMap<reprConfig*>* const pVariables,
		      const ctaModule* pModule,
		      const reprAutomaton* pAuto)
  { 
    reprConfig* lPTmpConfig= mHideRegion->Evaluate(pVariables, 
						   pModule, 
						   pAuto);
    //For all arguments of the hide operator we exists them.
    for(set<ctaString>::iterator 
	  itHideName = mHide->begin();
	itHideName != mHide->end();
	++itHideName)
    {
      // Just for testing.
      cout << "Hiding ";

      //If the keyword ALLVARIABLES is detect, we exists all variables.
      if(*itHideName == "ALLVARIABLES")
      {
	ctaForAll_Map(pModule->GetInterface(), itInterface, ctaComponent*)
	{
	  cout << *itInterface->second->GetName() << endl;
	  lPTmpConfig->exists(pAuto, itInterface->second->GetName());
	}
      }
      else
      {
	//If the keyword ALLAutomaton is detect, we exists all automaton.
	if(*itHideName == "ALLAUTOMATA")
	{
	  ctaForAll_Map(pModule->GetAutomata(), itAutomata, ctaAutomaton*)
	  {
	    cout << *itAutomata->second->GetName() << endl;
	    lPTmpConfig->exists(pAuto, itAutomata->second->GetName());
	  }
	}
	//If it a variable or a automaton name, we exitst them.
	else
	{
	  cout << "normal" << endl;
	  lPTmpConfig->exists(pAuto, &(*itHideName));
	}
      }
    }
    return lPTmpConfig;
  }
};

#endif

//-*-Mode: C++;-*-

/*
 * File:        anaRegInstrPrint.h
 * Purpose:     Implementation of an PRINT command
 * Author:      Dirk Beyer
 * Created:     1999
 * Copyright:   (c) 1999, University of Brandenburg, Cottbus
 */

#ifndef _anaRegInstrPrint_h
#define _anaRegInstrPrint_h


#include "anaRegInstr.h"
#include "anaRegion.h"

#include "utilCmdLineOptions.h"
extern utilCmdLineOptions* pubCmdLineOpt;

class anaRegInstrPrint : public anaRegInstr
{
private:
  // Name of the variable.
  // Aggregated.
  // If we have a "t_PRINT String" instruction, mRegion ist NULL
  //   If we have a "t_PRINT String RegionExpr String" instruction mRegion
  //   ist not NULL
  anaRegion* mRegion;

  // Prefix string for output.
  //   If it is a NULL pointer then we do not have a region
  //   (and the print command is only used for string output).
  // Aggregated.
  ctaString* mRegPrefix;

  // Postfix string for output.
  // Aggregated.
  ctaString* mRegPostfix;

  // It should be not allowed to use the standard operators.
  anaRegInstrPrint(const anaRegInstrPrint&);
  void operator=(const anaRegInstrPrint&);
  void operator,(const anaRegInstrPrint&);

public:
  anaRegInstrPrint(ctaString* pRegPrefix, 
		     anaRegion* pRegion, 
		     ctaString* pRegPostfix)
    :mRegion(pRegion),
     mRegPrefix(pRegPrefix),
     mRegPostfix(pRegPostfix)
  {}
  
  ~anaRegInstrPrint()
  {
    // May be there is no region (only string print).
    if(mRegion)
    {
      delete mRegion;
    }
    delete mRegPrefix;
    delete mRegPostfix;
  }
  
  void CheckConsistency(ctaMap<reprConfig*>* const pVariables,
			const ctaModule* pModule)
  {
    //   May be there is no region (only string print).
    if(mRegion)
    {
      mRegion->CheckConsistency(pVariables, pModule);
    }
  }

  // aheinig 09.04.2000
  bool CheckBddConstraint() const
  { 
    if(mRegion)
    {
      return mRegion->CheckBddConstraint();
    } 
    else
    {
      return true;
    }
  }

  //This method prints the region assigned to region (mVarName).
  void Execute(ctaMap<reprConfig*>* const pVariables,
	       const ctaModule* pModule,
	       const reprAutomaton* pAuto)
  {
    ostream& out = *pubCmdLineOpt->GetOutputStream();
    out << mRegPrefix;

    // May be there is no region (only string print).
    if(mRegion)
    {
      reprConfig* lConfig = mRegion->Evaluate(pVariables, pModule, pAuto);

      pAuto->print(out, lConfig);

      // We have to delete it because we are the caller.
      delete lConfig;
    }

    out << mRegPostfix << endl;
  }
};

#endif

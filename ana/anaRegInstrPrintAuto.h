//-*-Mode: C++;-*-

/*
 * File:        anaRegInstrPrintAuto.h
 * Purpose:     Implementation of an PRINT command for automata.
                Prints out the product automaton.
                This is useful for debugging.
 * Author:      Dirk Beyer
 * Created:     2000
 * Copyright:   (c) 2000, University of Brandenburg, Cottbus
 */

#ifndef _anaRegInstrPrintAuto_h
#define _anaRegInstrPrintAuto_h


#include "anaRegInstr.h"
#include "anaRegion.h"

class anaRegInstrPrintAuto : public anaRegInstr
{
private:
  // It should be not allowed to use the standard operators.
  anaRegInstrPrintAuto(const anaRegInstrPrintAuto&);
  void operator=(const anaRegInstrPrintAuto&);
  void operator,(const anaRegInstrPrintAuto&);

public:
  anaRegInstrPrintAuto()
  {}
  
  ~anaRegInstrPrintAuto()
  {}
  
  void CheckConsistency(ctaMap<reprConfig*>* const pVariables,
			const ctaModule* pModule)
  {}

  // aheinig 09.04.2000
  bool CheckBddConstraint() const
  { 
    return true;
  }

  // This method prints the automaton (pAuto).
  void Execute(ctaMap<reprConfig*>* const pVariables,
	       const ctaModule* pModule,
	       const reprAutomaton* pAuto)
  {
    *pubCmdLineOpt->GetOutputStream() << *pAuto << endl;
  }
};

#endif

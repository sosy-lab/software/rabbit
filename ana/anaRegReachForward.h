//-*-Mode: C++;-*-

/*
 * File:        anaRegReachForward.h
 * Purpose:     Contents the execution of a reachforward command
 * Created:     1999
 * Copyright:   (c) 1999, University of Brandenburg, Cottbus
 */

#ifndef _anaRegReachForward_h
#define _anaRegReachForward_h


#include "anaRegion.h"
#include "reprAutomaton.h"

class anaRegReachForward : public anaRegion
{
private:
  anaRegion* mOperand;
  int mSteps;

  // It should be not allowed to use the standard operators.
  anaRegReachForward(const anaRegReachForward&);
  void operator=(const anaRegReachForward&);
  void operator,(const anaRegReachForward&);
  
public:
  anaRegReachForward(anaRegion* lo, int st)
  {
    mOperand = lo;
    mSteps = st;
  }
  
  ~anaRegReachForward()
  {
    delete mOperand;
  }
  
  void CheckConsistency(ctaMap<reprConfig*>* const pVariables,
			const ctaModule* pModule)
  {
    mOperand->CheckConsistency(pVariables, pModule);
  }

  // aheinig 09.04.2000
  bool
  CheckBddConstraint() const
  {
    return mOperand->CheckBddConstraint();
  }

  reprConfig* Evaluate(ctaMap<reprConfig*>* const pVariables,
		      const ctaModule* pModule,
		      const reprAutomaton* pAuto)
  {
    reprConfig* lOperand = mOperand->Evaluate(pVariables, pModule, pAuto);

    int lSteps = mSteps;
    reprConfig* result = pAuto->mkReachable(reprAutomaton::mFORWARD, 
					    lOperand, 
					    lSteps);


    if(pubCmdLineOpt->Verbose())
    {
      if(lSteps >= 0)
      {
	cout << "Fixpoint reached after " << mSteps - lSteps 
	     << " steps." << endl;
      }
      else
      {
	cout << "Fixpoint not reached." << endl;
      }
    }

    delete lOperand;

    return result;
  }
};

#endif

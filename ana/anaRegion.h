//-*-Mode: C++;-*-

/*
 * File:        anaRegion.h
 * Purpose:     Baseclass for the implementation of the operators
 * Created:     1999
 * Copyright:   (c) 1999, University of Brandenburg, Cottbus
 */

#ifndef _anaRegion_h
#define _anaRegion_h


#include "ctaString.h"
#include "anaObject.h"
#include "reprConfig.h"
#include "reprAutomaton.h"
#include "ctaConfigLinRest.h"

class anaRegion : public anaObject
{
private: 
  // It should be not allowed to use the standard operators.
  anaRegion(const anaRegion&);
  void operator=(const anaRegion&);
  void operator,(const anaRegion&);
  
public:
  anaRegion()
  {}
  
  virtual ~anaRegion()
  {}
  
  // (pVariables) is a const pointer. (pModule) and (pAuto) are pointing on 
  //   const objects.
  virtual void CheckConsistency(ctaMap<reprConfig*>* const pVariables,
				const ctaModule* pModule) = 0;

  // These method will be overridden in the derivated classes
  //   it will return the result of the respective operation.
  //   The caller has to control the memory of the return pointer.
  virtual reprConfig* Evaluate(ctaMap<reprConfig*>* const pVariables,
			      const ctaModule* pModule,
			      const reprAutomaton* pAuto) = 0;

  // aheinig 09.04.2000
  virtual bool CheckBddConstraint() const = 0;

  void print()
  {
    cerr << "Print for anaRegion is not yet implemented." << endl;
  }
};

#endif


//-*-Mode: C++;-*-

/*
 * File:        anaRegInstrPrintBddGraph.h
 * Purpose:     Implementation of a PRINT command
 * Author:      Dirk Beyer and Andy Heinig
 * Created:     2001
 * Copyright:   (c) 2001, University of Brandenburg, Cottbus
 */

#ifndef _anaRegInstrPrintBddGraph_h
#define _anaRegInstrPrintBddGraph_h

#include "utilCmdLineOptions.h"

#include "anaRegInstr.h"
#include "anaRegion.h"

class anaRegInstrPrintBddGraph : public anaRegInstr
{
private:
  // Name of the variable.
  // Aggregated.
  anaRegion* mRegion;

  // It should be not allowed to use the standard operators.
  anaRegInstrPrintBddGraph(const anaRegInstrPrintBddGraph&);
  void operator=(const anaRegInstrPrintBddGraph&);
  void operator,(const anaRegInstrPrintBddGraph&);

public:
  anaRegInstrPrintBddGraph(anaRegion* pRegion)
    :mRegion(pRegion)
  {}
  
  ~anaRegInstrPrintBddGraph()
  {
    delete mRegion;
  }
  
  void CheckConsistency(ctaMap<reprConfig*>* const pVariables,
			const ctaModule* pModule)
  {
    mRegion->CheckConsistency(pVariables, pModule);
  }

  // aheinig 09.04.2000
  bool CheckBddConstraint() const
  { 
    return mRegion->CheckBddConstraint();
  }

  void Execute(ctaMap<reprConfig*>* const pVariables,
	       const ctaModule* pModule,
	       const reprAutomaton* pAuto)
  {
    ostream& out = *pubCmdLineOpt->GetOutputStream();

    reprConfig* lConfig = mRegion->Evaluate(pVariables, pModule, pAuto);

    lConfig->printGraphicalBddVisualization(out);

    // We have to delete it because we are the caller.
    delete lConfig;
  }
};

#endif

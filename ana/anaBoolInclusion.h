//-*-Mode: C++;-*-

/*
 * File:        anaBoolInclusion.h
 * Purpose:     Check for an inclusion of two regions.
 * Author:      msc+sbs
 * Created:     1999
 * Copyright:   (c) 1999, University of Brandenburg, Cottbus
 */

#ifndef _anaBoolInclusion_h
#define _anaBoolInclusion_h


#include "anaBool.h"
#include "anaRegion.h"

class anaBoolInclusion : public anaBool
{
private:
  // Aggregated.
  anaRegion* mRegion1;
  anaRegion* mRegion2;

  // It should be not allowed to use the standard operators.
  anaBoolInclusion(const anaBoolInclusion&);
  void operator=(const anaBoolInclusion&);
  void operator,(const anaBoolInclusion&);
  
public:
  anaBoolInclusion(anaRegion* pRegion1,anaRegion* pRegion2)
    :mRegion1(pRegion1),
     mRegion2(pRegion2)
  {}
  
  ~anaBoolInclusion()
  {
    delete(mRegion1);
    delete(mRegion2);
  }

  void CheckConsistency(ctaMap<reprConfig*>* const pVariables,
			const ctaModule* pModule)
  {
    mRegion1->CheckConsistency(pVariables, pModule);    
    mRegion2->CheckConsistency(pVariables, pModule);
  }

  reprConfig* Evaluate(ctaMap<reprConfig*>* const pVariables,
		       const ctaModule* pModule,
		       const reprAutomaton* pAuto)
  {
    reprConfig* lConf1 = mRegion1->Evaluate(pVariables, pModule, pAuto);
    reprConfig* lConf2 = mRegion2->Evaluate(pVariables, pModule, pAuto);
    bool lBoolResult = lConf1->setContains(*lConf2);

    delete lConf1;
    delete lConf2;

    // Regard the meaning of bool as a set.
    reprConfig* lResult;
    if( lBoolResult )
    {
      // Full config means TRUE.
      reprConfig* lTmpResult = pAuto->mkEmptyConfig();
      lResult = lTmpResult->mkComplement();
      delete lTmpResult;
    }
    else
    {
      // Empty config means FALSE.
      lResult = pAuto->mkEmptyConfig();
    }
    return lResult;
  }

  // aheinig 09.04.2000
  // only true
  bool
  CheckBddConstraint() const
  {
    return false;	   
  }
};

#endif


//-*-Mode: C++;-*-

/*
 * File:        anaRegInstrPrintRepr.h
 * Purpose:     Implementation-dependent output of a Config set.
 * Author:      Dirk Beyer
 * Created:     2001
 * Copyright:   (c) 2001, University of Brandenburg, Cottbus
 */

#ifndef _anaRegInstrPrintRepr_h
#define _anaRegInstrPrintRepr_h


#include "anaRegInstr.h"
#include "anaRegion.h"

// For type decision.
#include <typeinfo>

class anaRegInstrPrintRepr : public anaRegInstr
{
private:
  // Name of the variable.
  // Aggregated.
  // If we have a "t_PRINT String" instruction, mRegion ist NULL
  //   If we have a "t_PRINT String RegionExpr String" instruction mRegion
  //   ist not NULL
  anaRegion* mRegion;

  // Prefix string for output.
  //   If it is a NULL pointer then we do not have a region
  //   (and the print command is only used for string output).
  // Aggregated.
  ctaString* mRegPrefix;

  // Postfix string for output.
  // Aggregated.
  ctaString* mRegPostfix;

  // It should be not allowed to use the standard operators.
  anaRegInstrPrintRepr(const anaRegInstrPrintRepr&);
  void operator=(const anaRegInstrPrintRepr&);
  void operator,(const anaRegInstrPrintRepr&);

public:
  anaRegInstrPrintRepr(ctaString* pRegPrefix, 
		       anaRegion* pRegion, 
		       ctaString* pRegPostfix)
    :mRegion(pRegion),
     mRegPrefix(pRegPrefix),
     mRegPostfix(pRegPostfix)
  {}
  
  ~anaRegInstrPrintRepr()
  {
    // May be there is no region (only string print).
    if(mRegion)
    {
      delete mRegion;
    }
    delete mRegPrefix;
    delete mRegPostfix;
  }
  
  void CheckConsistency(ctaMap<reprConfig*>* const pVariables,
			const ctaModule* pModule)
  {
    //   May be there is no region (only string print).
    if(mRegion)
    {
      mRegion->CheckConsistency(pVariables, pModule);
    }
  }

  // aheinig 09.04.2000
  bool CheckBddConstraint() const
  { 
    if(mRegion)
    {
      return mRegion->CheckBddConstraint();
    } 
    else
    {
      return true;
    }
  }

  //This method prints the region assigned to region (mVarName).
  void Execute(ctaMap<reprConfig*>* const pVariables,
	       const ctaModule* pModule,
	       const reprAutomaton* pAuto)
  {
    ostream& out = *pubCmdLineOpt->GetOutputStream();
    out << mRegPrefix;

    // May be there is no region (only string print).
    if(mRegion)
    {
      reprConfig* lConfig = mRegion->Evaluate(pVariables, pModule, pAuto);

      // Representation-dependent output needs concrete object
      //   and the call of specific methods.
      if( typeid(*lConfig) == typeid(bddConfig) )
      {
	bddConfig* lBddConfig = lConfig->getDerivedObjBdd();
	lBddConfig->print(out, false);
      }
      else if ( typeid(*lConfig) == typeid(ddmConfig) )
      {
	ddmConfig* lDdmConfig = lConfig->getDerivedObjDdm();
	out << *lDdmConfig;
      }
      else
      {
	cerr << "Runtime error: Unknown representation type" << endl
	     << " in anaRegInstrPrintRepr!" << endl;
      }
      // We have to delete it because we are the caller.
      delete lConfig;
    }

    out << mRegPostfix << endl;
  }
};

#endif

//-*-Mode: C++;-*-

/*
 * File:        anaRegInstr.h
 * Purpose:     Base class for the implementation of the reachability 
 *              instructions
 * Author:      db
 * Created:     1999
 * Copyright:   (c) 1999, University of Brandenburg, Cottbus
 */

#ifndef _anaRegInstr_h
#define _anaRegInstr_h


#include "anaObject.h"
#include "ctaString.h"
#include "reprAutomaton.h"

class ctaModule;

class anaRegInstr : public anaObject
{
public: // Methods.

  anaRegInstr()
  {}
  
  ~anaRegInstr()
  {}

  virtual void CheckConsistency(ctaMap<reprConfig*>* const pVariables,
				const ctaModule* pModule) = 0;

  virtual void Execute(ctaMap<reprConfig*>* const pVariables,
		       const ctaModule* pModule,
		       const reprAutomaton* pAuto) = 0;
  
  // aheinig 09.04.2000
  virtual bool CheckBddConstraint() const = 0; 

};

#endif


//-*-Mode: C++;-*-

/*
 * File:        anaRegInstrPrintReprSize.h
 * Purpose:     Implementation of a PRINT command
 * Author:      Dirk Beyer
 * Created:     2002
 * Copyright:   (c) 2002, University of Brandenburg, Cottbus
 */

#ifndef _anaRegInstrPrintReprSize_h
#define _anaRegInstrPrintReprSize_h

#include "utilCmdLineOptions.h"

#include "anaRegInstr.h"
#include "anaRegion.h"

class anaRegInstrPrintReprSize : public anaRegInstr
{
private:
  // Name of the variable.
  // Aggregated.
  anaRegion* mRegion;

  // It should be not allowed to use the standard operators.
  anaRegInstrPrintReprSize(const anaRegInstrPrintReprSize&);
  void operator=(const anaRegInstrPrintReprSize&);
  void operator,(const anaRegInstrPrintReprSize&);

public:
  anaRegInstrPrintReprSize(anaRegion* pRegion)
    :mRegion(pRegion)
  {}
  
  ~anaRegInstrPrintReprSize()
  {
    delete mRegion;
  }
  
  void CheckConsistency(ctaMap<reprConfig*>* const pVariables,
			const ctaModule* pModule)
  {
    mRegion->CheckConsistency(pVariables, pModule);
  }

  // aheinig 09.04.2000
  bool CheckBddConstraint() const
  { 
    return mRegion->CheckBddConstraint();
  }

  void Execute(ctaMap<reprConfig*>* const pVariables,
	       const ctaModule* pModule,
	       const reprAutomaton* pAuto)
  {
    ostream& out = *pubCmdLineOpt->GetOutputStream();

    reprConfig* lConfig = mRegion->Evaluate(pVariables, pModule, pAuto);
    
    // Representation-dependent output needs concrete object
    //   and the call of specific methods.
    if( typeid(*lConfig) == typeid(bddConfig) )
    {
      bddConfig* lBddConfig = lConfig->getDerivedObjBdd();
      out << "Number of represented configurations: "
	  << lBddConfig->getConfigNr() << endl;
      
      out << "Number of BDD nodes: "
	  << lBddConfig->getBddSize() << endl;
    }
    else if ( typeid(*lConfig) == typeid(ddmConfig) )
    {
      ddmConfig* lDdmConfig = lConfig->getDerivedObjDdm();
      out << "Number of control locations (states): "
	  << lDdmConfig->size() << endl;

      int lNoPoly = 0;
      for(ddmConfig::const_iterator it = lDdmConfig->begin();
	  it != lDdmConfig->end();
	  ++it)
      {
	lNoPoly += it->second.size();
      }
      out << "Number of polyhedra in representation: "
	  << lNoPoly << endl;
    }
    else
    {
      cerr << "Runtime error: Unknown representation type" << endl
	   << " in anaRegInstrPrintRepr!" << endl;
    }
    
    // We have to delete it because we are the caller.
    delete lConfig;
  }
};

#endif

//-*-Mode: C++;-*-

/*
 * File:        anaRegInstrIf.h
 * Purpose:     Implementation of an IF command
 * Author:      msc+sbs
 * Created:     1999
 * Copyright:   (c) 1999, University of Brandenburg, Cottbus
 */

#ifndef _anaRegInstrIf_h
#define _anaRegInstrIf_h


#include "anaRegInstr.h"
#include "anaBool.h"

class anaRegInstrIf : public anaRegInstr
{
private:
  // Name of the variable.
  // Aggregated.
  anaRegion* mCondition;
  anaRegInstr* mThenInstr;
  anaRegInstr* mElseInstr;

  // It should be not allowed to use the standard operators.
  anaRegInstrIf(const anaRegInstrIf&);
  void operator=(const anaRegInstrIf&);
  void operator,(const anaRegInstrIf&);

public:
  anaRegInstrIf(anaRegion* pCondition,
		anaRegInstr* pThenInstr,
		anaRegInstr* pElseInstr)
    :mCondition(pCondition),
     mThenInstr(pThenInstr),
     mElseInstr(pElseInstr)
  {}
  
  ~anaRegInstrIf()
  {
    delete(mCondition);
    delete(mThenInstr);
    delete(mElseInstr);
  }
  
  void CheckConsistency(ctaMap<reprConfig*>* const pVariables,
			const ctaModule* pModule)
  {
    mCondition->CheckConsistency(pVariables, pModule);
    mThenInstr->CheckConsistency(pVariables, pModule);
    mElseInstr->CheckConsistency(pVariables, pModule);
  }

  // aheinig 09.04.2000
  bool 
  CheckBddConstraint() const
  { 
    return mCondition->CheckBddConstraint() &&
           mThenInstr->CheckBddConstraint() &&
           mElseInstr->CheckBddConstraint();
  }

  void Execute(ctaMap<reprConfig*>* const pVariables,
	       const ctaModule* pModule,
	       const reprAutomaton* pAuto)
  {
    reprConfig* lCondition = mCondition->Evaluate(pVariables, 
						  pModule, 
						  pAuto);
    reprConfig* lConditionComplement = lCondition->mkComplement();

    // Empty set means boolean false.
    if( lConditionComplement->isEmpty() )
    {
      // Full set means TRUE.
      mThenInstr->Execute(pVariables, pModule, pAuto);
    }
    else if( lCondition->isEmpty() )
    {
      // Empty means FALSE.
      mElseInstr->Execute(pVariables, pModule, pAuto);
    }
    else
    {
      cerr << "Runtime warning: IF expects a boolean value," << endl
	   << "not a nontrivial set. Skipping IF statement." << endl;
    }

    delete lCondition;
    delete lConditionComplement;
  }
};

#endif

//-*-Mode: C++;-*-

/*
 * File:        anaRegInstrAssign.h
 * Purpose:     Implementation of an assignment command
 * Created:     1999
 * Copyright:   (c) 1999, University of Brandenburg, Cottbus
 */

#ifndef _anaRegInstrAssign_h
#define _anaRegInstrAssign_h


#include "anaRegInstr.h"
#include "anaRegion.h"

class anaRegInstrAssign : public anaRegInstr
{
private:
  // Name of the variable.
  // Aggregated.
  ctaString* mVarName;

  // Region assigned to the variable.
  // Aggregated.
  anaRegion* mRegion;

  // It should be not allowed to use the standard operators.
  anaRegInstrAssign(const anaRegInstrAssign&);
  void operator=(const anaRegInstrAssign&);
  void operator,(const anaRegInstrAssign&);

public:
  anaRegInstrAssign(ctaString* pVarName, anaRegion* pRegion)
    :mVarName(pVarName),
     mRegion(pRegion)
  {}

  ~anaRegInstrAssign()
  {
    delete mVarName;
    delete mRegion;
  }
  
  //this method proves is the actual command can be executed 
  void CheckConsistency(ctaMap<reprConfig*>* const pVariables,
			const ctaModule* pModule)
  {
    // Is the variable already declared?
    if( pVariables->find(*mVarName) == pVariables->end() )
    {
      CTAERR << "Region variable '" + *mVarName + "' not declared."
	     << endl;
      CTAPUT;
   }

    // Check of existence of the module has been done by
    //   DdmTransformation().
    mRegion->CheckConsistency(pVariables, 
			      pModule);
  }

  // aheinig 09.04.2000
  bool CheckBddConstraint() const
  { 
    return mRegion->CheckBddConstraint();
  }

  void Execute(ctaMap<reprConfig*>* const pVariables,
	       const ctaModule* pModule,
	       const reprAutomaton* pAuto)
  {
    // Fetch the iterator pointing to the variable and the region.
    ctaMap<reprConfig*>::iterator lRegIt;
    lRegIt = pVariables->find(*mVarName);

    // Is the variable declared?
    if( lRegIt == pVariables->end() )
    {
      cerr << "Runtime Error: Region variable '" << *mVarName 
	   << "' not declared!" << endl;
    }

    // After the assignment we have to delete the old reprConfig.
    // May be we refer to the old reprConfig in the region expression
    //   which we have to compute now.
    reprConfig* lPtr = lRegIt->second;

    // Assign the new value.
    lRegIt->second = mRegion->Evaluate(pVariables, 
				       pModule,
				       pAuto);

    // Has the variable been a valid region from an assignment
    //   before the assignment above?
    // If there is an old value, delete it first.
    if ( lPtr != NULL )
    {
      delete lPtr;
    }
  }
};

#endif

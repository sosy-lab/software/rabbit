//-*-Mode: C++;-*-

/*
 * File:        anaRegInstrSeq.h
 * Purpose:     Vector of (anaRegInstr*).
 * Author:      msc+sbs
 * Created:     1999
 * Copyright:   (c) 1999, University of Brandenburg, Cottbus
 */

#ifndef _anaRegInstrSeq_h
#define _anaRegInstrSeq_h


#include "anaRegInstr.h"
#include <vector>

class anaRegInstrSeq : public anaRegInstr
{
private:
  // Name of the variable.
  // Aggregated.
  vector<anaRegInstr*>* mInstrVec;

  // It should be not allowed to use the standard operators.
  anaRegInstrSeq(const anaRegInstrSeq&);
  void operator=(const anaRegInstrSeq&);
  void operator,(const anaRegInstrSeq&);
  
public:
  anaRegInstrSeq()
  {
    mInstrVec = new vector<anaRegInstr*>();
  }
  
  ~anaRegInstrSeq()
  {
    for(vector<anaRegInstr*>::iterator it = mInstrVec->begin();
	it != mInstrVec->end();
	++it)
    {
      // Delete the aggregated anaRegInstr.
      delete(*it);
    }
    delete mInstrVec;
  }
  
  void Append(anaRegInstr* pRegInstr)
  {
    mInstrVec->push_back(pRegInstr);
  }

  void CheckConsistency(ctaMap<reprConfig*>* const pVariables,
			const ctaModule* pModule)
  {
    for(vector<anaRegInstr*>::iterator it = mInstrVec->begin();
	it != mInstrVec->end();
	++it)
    {
      (*it)->CheckConsistency(pVariables, pModule);
    }
  }

  // aheinig 09.04.2000
  bool CheckBddConstraint() const
  { 
    for(vector<anaRegInstr*>::iterator it = mInstrVec->begin();
	it != mInstrVec->end();
	++it)
    {
      if( ! (*it)->CheckBddConstraint())
      {
	return false;
      }
    }
    return true;
  }

  void Execute(ctaMap<reprConfig*>* const pVariables,
	       const ctaModule* pModule,
	       const reprAutomaton* pAuto)
  {
    for(vector<anaRegInstr*>::iterator it = mInstrVec->begin();
	it != mInstrVec->end();
	++it)
    {
      (*it)->Execute(pVariables, pModule, pAuto);
    }
  }
};

#endif

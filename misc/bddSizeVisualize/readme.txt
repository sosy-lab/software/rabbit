Visualisierung des BDD-Graphen.

Tool kompilieren:
javac *.java

Ausfuehren:
java VisualizeSize inputfile1 [inputfile2] ...

Tool laeuft nicht unter Solaris mit jdk1.2. Hier ist jdk1.3 zu benutzen.

Die Zahlen aus den Inputfiles(jeweils eine Zahl je Zeile)
werden grafisch dargestellt. Falls mehr als ein Inputfile angegeben
wurde, werden die Darstellungen, farblich unterschieden, uebereinandergelegt.

Es ist auch moeglich mehrere Darstellungen aus einem inputfile
darzustellen, wenn eine Raute "#" auf einer separaten Zeile die
Wertereihen trennt.


mvogel 2001-03-09

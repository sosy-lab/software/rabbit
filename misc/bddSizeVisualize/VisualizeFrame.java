/**
 * project: BDD width visualization
 * module:  VisualizeFrame.java
 * @version <01.03.2001>
 * @author  <Michael Vogel - (mvogel@informatik.tu-cottbus.de)>
 *
 *
 */

import java.awt.*;          // Import abstract window toolkit.
import java.awt.event.*;    // Import event components for awt.
//import javax.swing.*;       // Import Swing components.
import java.util.*;         // Import java utilities.


public class VisualizeFrame extends Frame {

  private Panel mainPanel;
  private Frame myFrame;                // Pointer to this frame;
  private ScrollPane scrPane;
  private MyCanvas canvas;               // Canvas for graphics.
  private java.util.List visualizeList;  // contains value lists for display
  private int lineDistance = 2;
  private double maxBddWidth;
  private int[] fileSequence;
  private boolean xorMode = false;
  double[][] maxVals;

  /*
   * Class represents the canvas object, the line diagram will be draw on.
   * The class method paint, draws the diagram.
   */

  private class MyCanvas extends Canvas {
    
      /**
     *
     */

    private int maxBDDdepht(java.util.List localList) {
      int maxLength = 0;

      for (int i = 0; i < localList.size(); i++) {
        if (((java.util.List) localList.get(i)).size() > maxLength) {
          maxLength = ((java.util.List) localList.get(i)).size();
        } // if
      }  // for

      return maxLength;
    } // maxBDDdepht

    /*
     * Draws the line diagram
     *
     * @param area the graphics area for drawing.
     */

    public void paint(Graphics area) {
      int xStart, xEnd, // minimal size of window.
          lineWidth,hSize, vSize;
      double tmpVal, tmpVal2;

      java.util.List numberList;

      if (xorMode) {
        area.setXORMode(Color.white);
      } else {
        area.setPaintMode();
      } // if

      for (int i = 0; i < visualizeList.size(); i++) {
         maxVals[i][0] = max((java.util.List) visualizeList.get(i));
         maxVals[i][1] = (double) i;
      } // for

      // BubbleSort (descending sort)
      for (int i = 0; i < maxVals.length - 1; i++) {
        for (int j = i + 1; j < maxVals.length; j++) {
           if (maxVals[i][0] < maxVals[j][0]) {
             tmpVal = maxVals[i][0];
             tmpVal2 = maxVals[i][1];
             maxVals[i][0] = maxVals[j][0];
             maxVals[i][1] = maxVals[j][1];
             maxVals[j][0] = tmpVal;
             maxVals[j][1] = tmpVal2;
           } // if
        } // for
      }  // ofr

      fileSequence = new int[maxVals.length];
      for (int i = 0; i < maxVals.length; i++) {
        fileSequence[i] = (int) maxVals[i][1];
      }  // for

      //maxVal = maxVals[0][0]; //((float) this.max());

      area.drawString("maxBDDWidth: " + maxBddWidth, 0, 12);

/*
      lineDistance = (myFrame.getSize().height - 60)
                     / this.maxBDDdepht(visualizeList);

      if (lineDistance > 3) {
        lineDistance = 3;
      } else if (lineDistance == 0) {
        lineDistance = 1;
      } // if
*/

      hSize = myFrame.getSize().width - 35;  // save window sizes.
      vSize = maxBDDdepht(visualizeList) * lineDistance + 30;
      this.setSize(hSize, vSize);

      //System.out.println("hSize = " + hSize + " | vSize = " + vSize);

      for (int i = 0; (i < visualizeList.size()); i++) {
        area.setColor(ColorAssignment.getColor(i));
        numberList = ((java.util.List) visualizeList.get((int) maxVals[i][1]));

        for (int j = 0; (j < numberList.size()) && (j < hSize); j++) {
          lineWidth =  (int) (( ((double) (hSize - 20)) / maxBddWidth)
               * ((Double) numberList.get(j)).doubleValue());
          xStart = (hSize - lineWidth) / 2;
          xEnd = xStart + lineWidth;

          for (int k = 0; k < lineDistance; k++) {
            area.drawLine(xStart,(lineDistance * j + 20 + k),
                          xEnd, (lineDistance * j + 20 + k));
          } // for
        } // for
      } // for
    } // paint


    public Dimension getPreferedSize() {
      return new Dimension(myFrame.getSize().width - 50,
                           myFrame.getSize().height - 50);
    } // getPreferedSize

  } // class My_Canvas
  

  /**
   * The constructor assigns the owner argument to class variable owner
   * and adds a WindowListener for closing events to this window.
   *
   * @param owner reference to owner frame
   */
  
  public VisualizeFrame(java.util.List visualizeList) {
    double tmpVal, tmpVal2;

    // Initialization of variables.
    this.myFrame = this;
    this.setTitle("Visualization of BDD width"); // Set window title.
    this.setLocation(270, 100);            // Set initial window position.
    this.setSize(600,500);             // Set windows size.

    this.visualizeList = visualizeList;
    this.maxVals = new double[visualizeList.size()][2];

    for (int i = 0; i < visualizeList.size(); i++) {
      this.maxVals[i][0] = max((java.util.List) this.visualizeList.get(i));
      this.maxVals[i][1] = (double) i;
    } // for

    // BubbleSort (descending sort)
    for (int i = 0; i < maxVals.length - 1; i++) {
      for (int j = i + 1; j < maxVals.length; j++) {
        if (maxVals[i][0] < maxVals[j][0]) {
          tmpVal = maxVals[i][0];
          tmpVal2 = maxVals[i][1];
          maxVals[i][0] = maxVals[j][0];
          maxVals[i][1] = maxVals[j][1];
          maxVals[j][0] = tmpVal;
          maxVals[j][1] = tmpVal2;
        } // if
      } // for
    }  // ofr

    this.fileSequence = new int[maxVals.length];
    for (int i = 0; i < maxVals.length; i++) {
      fileSequence[i] = (int) maxVals[i][1];
    }  // for


    // Add WindowListener for closing event.
    addWindowListener(
      new WindowAdapter() {
        public void windowClosing(WindowEvent evt) {
          dispose(); // close
          System.exit(0);
        } // windowClosing(WindowEvent evt)
      } // WindowAdapter
    ); // addWindowListener(new WindowAdapter)

    prepareWindow();  // Add windows components.
    canvas.repaint(); // Initial draw.


  } // constructor
  

  /**
   *
   */

  public double max(java.util.List numberList) {
    double maxVal = 0.0;

    for (int i = 0; i < numberList.size(); i++) {
      if ( ((Double) numberList.get(i)).doubleValue() > maxVal) {
        maxVal = ((Double) numberList.get(i)).doubleValue();
      } // if
    } // for

    return maxVal;
  } // max

/**
 *
 */

  public void setMaxBddWidth(double value) {
    this.maxBddWidth = value;
  }

  public void setDistance(int value) {
    this.lineDistance = value;
  } // if

  public void setXorMode(boolean xorMode) {
    this.xorMode = xorMode;
  } // setXorMode

  public int[] getFileSequence() {
//    System.out.println("getFileSequence length=" + this.fileSequence.length);
    return this.fileSequence;
  }

  /**
   *
   */

  public void repaint() {
    this.canvas.repaint();
  } // repaint;

  /**
   * Defines the appearance of this window and adds the canvas containing
   * the line diagramm.
   */
  
  private void prepareWindow() {
    this.canvas = new MyCanvas(); // initialize canvas.
    this.canvas.setBackground(Color.white);
//    this.mainPanel = new Panel();
    this.scrPane = new ScrollPane();

    this.scrPane.add(this.canvas);

    // Schrittweite der Scrollbars auf 25 Pixel je Mausklick einstellen. 
//    this.scrPane.getVerticalScrollBar().setUnitIncrement(25);

//    this.mainPanel.add(scrPane);

    // Add the graphics canvas to the frames content pane.
    this.add(scrPane);

  } // prepareWindow

  public Dimension getPreferedSize(){
    return new Dimension(600, 400);
  } // fi


} // class

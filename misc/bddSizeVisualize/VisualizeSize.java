/**
 * project: BDD width visualization
 * module:  VisualizeSize.java
 * @version <01.03.2001>
 * @author  <Michael Vogel - (mvogel@informatik.tu-cottbus.de)>
 *
 *
 */

//import javax.swing.*;
//import javax.swing.event.*;
import java.awt.*;
//import java.awt.event.*;
import java.util.*; // Import utilities.
import VisualizeFrame;        // import class for visualization
import FileInput;             // import class for file input
import FilenameDialog;
import ControlDialog;

public class VisualizeSize {

  /**
   *
   */

  public static double maxVal(java.util.List localList) {
    java.util.List numberList;
    double maxVal = 0.0;

    for (int i = 0; i < localList.size(); i++) {
      numberList = (java.util.List) localList.get(i);

      for (int j = 0; j < numberList.size(); j++) {
        if ( ((Double) numberList.get(j)).doubleValue() > maxVal) {
          maxVal = ((Double) numberList.get(j)).doubleValue();
        } // if
      } // for
    } // for

    return maxVal;
  } // max



  /**
   * main opens the main window of this application.
   * 
   * @param args[0] input filename
   */
  
  public static void main(String[] args) {
    VisualizeFrame mainWindow;
    FilenameDialog filenameWindow;
    ControlDialog controlWindow;
    FileInput fileIn;
    java.util.List visualizeList, filenames;

    if (args.length > 0) {
      visualizeList = new Vector();
      filenames = new Vector();
      fileIn = new FileInput();

      // read inputFiles
      for (int i = 0; i < args.length; i++) {
        visualizeList = fileIn.readFile(visualizeList, filenames, args[i]);
      } // for

      // Call visualization frame with visualizeList and filename.
      mainWindow = new VisualizeFrame(visualizeList);
      mainWindow.setMaxBddWidth(maxVal(visualizeList));
      mainWindow.setDistance(2); // initial val equal to control val.

      controlWindow = new ControlDialog(mainWindow);
      controlWindow.setMaxBDDWidth(maxVal(visualizeList));

      filenameWindow = new FilenameDialog(filenames, mainWindow);
      mainWindow.setVisible(true);
      filenameWindow.show();
      controlWindow.show();

    } else {
      System.out.println("BDD width visualizer");
      System.out.println("\nusage: java VisualizeSize inputfilename1 "
                         + "[inputfilename2] ...");
      System.exit(1);
    } // if
  } // main

} // class TimeDisplays

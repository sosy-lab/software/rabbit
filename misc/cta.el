;;; cta-mode.el --- text mode, and its idiosyncratic commands.
;; Copyright (C) 1985, 1992, 1994 Free Software Foundation, Inc.

;; Maintainer: FSF

;; This file is part of GNU Emacs.

;; GNU Emacs is free software; you can redistribute it and/or modify
;; it under the terms of the GNU General Public License as published by
;; the Free Software Foundation; either version 2, or (at your option)
;; any later version.

;; GNU Emacs is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;; GNU General Public License for more details.

;; You should have received a copy of the GNU General Public License
;; along with GNU Emacs; see the file COPYING.  If not, write to the
;; Free Software Foundation, Inc., 59 Temple Place - Suite 330,
;; Boston, MA 02111-1307, USA.

;;; Commentary:

;; This package provides the fundamental text mode documented in the
;; Emacs user's manual.

;;; Code:

(defvar cta-mode-syntax-table nil
  "Syntax table used while in text mode.")

(defvar cta-mode-abbrev-table nil
  "Abbrev table used while in text mode.")
(define-abbrev-table 'cta-mode-abbrev-table ())

(if cta-mode-syntax-table
    ()
  (setq cta-mode-syntax-table (make-syntax-table))
  (modify-syntax-entry ?\" ".   " cta-mode-syntax-table)
  (modify-syntax-entry ?\\ ".   " cta-mode-syntax-table)
  (modify-syntax-entry ?' "w   " cta-mode-syntax-table))

(defvar cta-mode-map nil
  "Keymap for Text mode.
Many other modes, such as Mail mode, Outline mode and Indented Text mode,
inherit all the commands defined in this map.")

(if cta-mode-map
    ()
  (setq cta-mode-map (make-sparse-keymap))
;  (define-key cta-mode-map "\e\t" 'ispell-complete-word)
;  (define-key cta-mode-map "\t" 'tab-to-tab-stop)
  (define-key cta-mode-map "\es" 'center-line)
  (define-key cta-mode-map "\eS" 'center-paragraph))


;(defun non-saved-cta-mode ()
;  "Like cta-mode, but delete auto save file when file is saved for real."
;  (cta-mode)
;  (make-local-variable 'delete-auto-save-files)
;  (setq delete-auto-save-files t))

(defun cta-mode ()
  "Major mode for editing text intended for humans to read.
Special commands:
\\{cta-mode-map}
Turning on Text mode calls the value of the variable `cta-mode-hook',
if that value is non-nil."
  (interactive)
  (kill-all-local-variables)
  (use-local-map cta-mode-map)
  (setq comment-indent-function 'cta-mode-indent-comment)
  (setq mode-name "CTA")
  (setq major-mode 'cta-mode)
  (setq local-abbrev-table cta-mode-abbrev-table)
  (make-local-variable 'tab-width)
  (setq tab-width 2)

  (set-syntax-table cta-mode-syntax-table)
  (run-hooks 'cta-mode-hook))

(defvar indented-cta-mode-map ()
  "Keymap for Indented Text mode.
All the commands defined in Text mode are inherited unless overridden.")

(if indented-cta-mode-map
    ()
  ;; Make different definition for TAB before the one in cta-mode-map, but
  ;; share the rest.
  (let ((newmap (make-sparse-keymap)))
    (define-key newmap "\t" 'indent-relative)
    (setq indented-cta-mode-map (nconc newmap cta-mode-map))))

(defun indented-cta-mode ()
  "Major mode for editing text with indented paragraphs.
In this mode, paragraphs are delimited only by blank lines.
You can thus get the benefit of adaptive filling
 (see the variable `adaptive-fill-mode').
\\{indented-cta-mode-map}
Turning on `indented-cta-mode' calls the value of the variable
`cta-mode-hook', if that value is non-nil."
  (interactive)
  (kill-all-local-variables)
  (use-local-map cta-mode-map)
  (define-abbrev-table 'cta-mode-abbrev-table ())
  (setq local-abbrev-table cta-mode-abbrev-table)
  (set-syntax-table cta-mode-syntax-table)
  (make-local-variable 'indent-line-function)
  (setq indent-line-function 'indent-relative-maybe)
  (make-local-variable 'paragraph-start)
  (setq paragraph-start (concat "$\\|" page-delimiter))
  (make-local-variable 'paragraph-separate)
  (setq paragraph-separate paragraph-start)
  (use-local-map indented-cta-mode-map)
  (setq mode-name "Indented Text")
  (setq major-mode 'indented-cta-mode)
  (run-hooks 'cta-mode-hook 'indented-cta-mode-hook))

(defun center-paragraph ()
  "Center each nonblank line in the paragraph at or after point.
See `center-line' for more info."
  (interactive)
  (save-excursion
    (forward-paragraph)
    (or (bolp) (newline 1))
    (let ((end (point)))
      (backward-paragraph)
      (center-region (point) end))))

(defun center-region (from to)
  "Center each nonblank line starting in the region.
See `center-line' for more info."
  (interactive "r")
  (if (> from to)
      (let ((tem to))
	(setq to from from tem)))
  (save-excursion
    (save-restriction
      (narrow-to-region from to)
      (goto-char from)
      (while (not (eobp))
	(or (save-excursion (skip-chars-forward " \t") (eolp))
	    (center-line))
	(forward-line 1)))))

(defun center-line (&optional nlines)
  "Center the line point is on, within the width specified by `fill-column'.
This means adjusting the indentation so that it equals
the distance between the end of the text and `fill-column'.
The argument NLINES says how many lines to center."
  (interactive "P")
  (if nlines (setq nlines (prefix-numeric-value nlines)))
  (while (not (eq nlines 0))
    (save-excursion
      (let ((lm (current-left-margin))
	    line-length)
	(beginning-of-line)
	(delete-horizontal-space)
	(end-of-line)
	(delete-horizontal-space)
	(setq line-length (current-column))
	(if (> (- fill-column lm line-length) 0)
	    (indent-line-to 
	     (+ lm (/ (- fill-column lm line-length) 2))))))
    (cond ((null nlines)
	   (setq nlines 0))
	  ((> nlines 0)
	   (setq nlines (1- nlines))
	   (forward-line 1))
	  ((< nlines 0)
	   (setq nlines (1+ nlines))
	   (forward-line -1)))))

;;; cta-mode.el ends here

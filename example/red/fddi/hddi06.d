#define SA	20
#define td 	0
#define TRTT 	320

process count = 7; /* 1 is for ring, the others for stations. */ 
local clock x, y;
global synchronizer 
tt1, tt2, tt3, tt4, tt5, tt6, 
rt1, rt2, rt3, rt4, rt5, rt6; 

mode ring_to_1 x <= td { 
	when !tt1 x == td  may goto ring_1; 
}

mode ring_1 true { 
	when ?rt1 true  may x= 0; goto ring_to_2; 
}

mode ring_to_2 x <= td { 
	when !tt2 x == td  may goto ring_2; 
}

mode ring_2 true { 
	when ?rt2 true  may x= 0; goto ring_to_3; 
}

mode ring_to_3 x <= td { 
	when !tt3 x == td  may goto ring_3; 
}

mode ring_3 true { 
	when ?rt3 true  may x= 0; goto ring_to_4; 
}

mode ring_to_4 x <= td { 
	when !tt4 x == td  may goto ring_4; 
}

mode ring_4 true { 
	when ?rt4 true  may x= 0; goto ring_to_5; 
}

mode ring_to_5 x <= td { 
	when !tt5 x == td  may goto ring_5; 
}

mode ring_5 true { 
	when ?rt5 true  may x= 0; goto ring_to_6; 
}

mode ring_to_6 x <= td { 
	when !tt6 x == td  may goto ring_6; 
}

mode ring_6 true { 
	when ?rt6 true  may x= 0; goto ring_to_1; 
}

mode station_1_idle true {
	when ?tt1 true may y= x; x= 0; goto station_1_sync; 
}

mode station_1_sync x <= SA {
	when !rt1 x == SA and y >= TRTT may goto station_1_idle; 
	when x == SA and y < TRTT may goto station_1_async; 
}  

mode station_1_async y <= TRTT { 
	when !rt1 true may goto station_1_idle; 
} 

mode station_2_idle true {
	when ?tt2 true may y= x; x= 0; goto station_2_sync; 
}

mode station_2_sync x <= SA {
	when !rt2 x == SA and y >= TRTT may goto station_2_idle; 
	when x == SA and y < TRTT may goto station_2_async; 
}  

mode station_2_async y <= TRTT { 
	when !rt2 true may goto station_2_idle; 
} 

mode station_3_idle true {
	when ?tt3 true may y= x; x= 0; goto station_3_sync; 
}

mode station_3_sync x <= SA {
	when !rt3 x == SA and y >= TRTT may goto station_3_idle; 
	when x == SA and y < TRTT may goto station_3_async; 
}  

mode station_3_async y <= TRTT { 
	when !rt3 true may goto station_3_idle; 
} 

mode station_4_idle true {
	when ?tt4 true may y= x; x= 0; goto station_4_sync; 
}

mode station_4_sync x <= SA {
	when !rt4 x == SA and y >= TRTT may goto station_4_idle; 
	when x == SA and y < TRTT may goto station_4_async; 
}  

mode station_4_async y <= TRTT { 
	when !rt4 true may goto station_4_idle; 
} 

mode station_5_idle true {
	when ?tt5 true may y= x; x= 0; goto station_5_sync; 
}

mode station_5_sync x <= SA {
	when !rt5 x == SA and y >= TRTT may goto station_5_idle; 
	when x == SA and y < TRTT may goto station_5_async; 
}  

mode station_5_async y <= TRTT { 
	when !rt5 true may goto station_5_idle; 
} 

mode station_6_idle true {
	when ?tt6 true may y= x; x= 0; goto station_6_sync; 
}

mode station_6_sync x <= SA {
	when !rt6 x == SA and y >= TRTT may goto station_6_idle; 
	when x == SA and y < TRTT may goto station_6_async; 
}  

mode station_6_async y <= TRTT { 
	when !rt6 true may goto station_6_idle; 
} 

initially 
    station_1_idle[1] and x[1] == 0 and y[1] >= TRTT 
and station_2_idle[2] and x[2] == 0 and y[2] >= TRTT 
and station_3_idle[3] and x[3] == 0 and y[3] >= TRTT 
and station_4_idle[4] and x[4] == 0 and y[4] >= TRTT 
and station_5_idle[5] and x[5] == 0 and y[5] >= TRTT 
and station_6_idle[6] and x[6] == 0 and y[6] >= TRTT 
and ring_to_1[7] and x[7] == 0 and y[7] >= TRTT; 

risk 
 	(station_1_sync[1] or station_1_async[1])
and 	(station_2_sync[2] or station_2_async[2]);

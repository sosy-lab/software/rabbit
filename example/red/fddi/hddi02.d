#define SA	20
#define td 	0
#define TRTT 	120

process count = 3; /* 1 is for ring, the others for stations. */ 
local clock x, y;
global synchronizer tt1, tt2, rt1, rt2; 

mode ring_to_1 x <= td { 
	when !tt1 x == td  may goto ring_1; 
}

mode ring_1 true { 
	when ?rt1 true  may x= 0; goto ring_to_2; 
}

mode ring_to_2 x <= td { 
	when !tt2 x == td  may goto ring_2; 
}

mode ring_2 true { 
	when ?rt2 true  may x= 0; goto ring_to_1; 
}

mode station_1_idle true {
	when ?tt1 true may y= x; x= 0; goto station_1_sync; 
}

mode station_1_sync x <= SA {
	when !rt1 x == SA and y >= TRTT may goto station_1_idle; 
	when x == SA and y < TRTT may goto station_1_async; 
}  

mode station_1_async y <= TRTT { 
	when !rt1 true may goto station_1_idle; 
} 

mode station_2_idle true {
	when ?tt2 true may y= x; x= 0; goto station_2_sync; 
}

mode station_2_sync x <= SA {
	when !rt2 x == SA and y >= TRTT may goto station_2_idle; 
	when x == SA and y < TRTT may goto station_2_async; 
}  

mode station_2_async y <= TRTT { 
	when !rt2 true may goto station_2_idle; 
} 

initially 
    ring_to_1[1] and x[1] == 0 and y[1] >= TRTT 
and station_1_idle[2] and x[2] == 0 and y[2] >= TRTT 
and station_2_idle[3] and x[3] == 0 and y[3] >= TRTT; 


risk 
 	(station_1_sync[2] or station_1_async[2])
and 	(station_2_sync[3] or station_2_async[3]);

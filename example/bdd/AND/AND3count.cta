/* 
 * project: implementation of a 3-bit AND-Gate in CTA
 * modules: pMOSTransistorHigh, pMOSTransistorLow, nMOSTransistorHigh,
 *          nMOSTransistorLow, NegatorHigh, NANDGateHigh, NORGateLow, System
 * version: 10.05.2000
 * author:  Michael Vogel - (mvogel@informatik.tu-cottbus.de)
 *
 * Based on paper "Some Progress in the Symbolic Verification of Timed 
 *                 Automata" pages 8 - 9 (MOS Circuits).
 *
 * This version of AND3 counts the number of simultaneous switching
 * transistors in variable <count>.
 *
 * This module uses one instance of NAND-gate, one instance of
 * a NOR-gate and one instance of a negator to build an AND-gate with three
 * inputs. (10 transistors)
 * Boolean equation:
 * !( !(A && B) || !C) ) == A && B && C
 */   

// instance of pMOSTransistorHigh is initialized to high
MODULE pMOSTransistorHigh
{
  INPUT
    // gate is value at gate input of pMOS transistor.
    gate         : DISCRETE;
    l            : CONST;
    u            : CONST;

  OUTPUT
    // outerVal = 1 if transistor is... and 0 if transistor is... 
    outerVal     : DISCRETE;
  
  MULTREST
    count        : DISCRETE; // Counts number of simultaneous transitions.
  
  // Initial gate input of pMOS transistor is zero -> outerVal = 1. 
  INITIAL   STATE(pMOS) = stable1
                   AND c = u AND outerVal = 1;  
    
  AUTOMATON pMOS
  {
    STATE stable0   { INV   gate = 1;  
                        TRANS { GUARD   gate = 0;  
                                DO   c' = 0 AND count' = count + 1;  
                                GOTO unstable1;
                              }
                    }
    
    STATE unstable1 { INV   gate = 0 AND c+1 <= u;  
                        TRANS { GUARD   gate = 1;  
                                DO   c' = u AND count' + 1 = count;  
                                GOTO stable0;
                              }
                        TRANS { GUARD   gate = 0 AND l <= c;  
                                DO   outerVal' = 1 AND c' = u 
                                     AND count' + 1 = count;  
                                GOTO stable1;
                              }
                    }

    STATE unstable0 { INV   gate = 1 AND c+1 <= u;  
                        TRANS { GUARD   gate = 1 AND l <= c;  
                                DO   outerVal' = 0 AND c' = u
                                     AND count' + 1 = count;  
                                GOTO stable0;
                              }
                        TRANS { GUARD   gate = 0;  
                                DO   c' = u  AND count' + 1 = count;  
                                GOTO stable1; 
                              }
                    }

    STATE stable1   { INV   gate = 0;  
                        TRANS { GUARD   gate = 1;  
                                DO   c' = 0 AND count' = count + 1;  
                                GOTO unstable0; 
                              }
                    }
  }
 
  LOCAL
    c            : CLOCK(5); // up
}

// instance of pMOSTransistorLow is initialized to low
MODULE pMOSTransistorLow
{
  INPUT
    // gate is value at gate input of pMOS transistor.
    gate         : DISCRETE;
    l            : CONST;
    u            : CONST;

  OUTPUT
    // outerVal = 1 if transistor is... and 0 if transistor is... 
    outerVal     : DISCRETE;
  
  MULTREST
    count        : DISCRETE; // Counts number of simultaneous transitions.
  
  // Initial gate input of pMOS transistor is 1 -> outerVal = 0. 
  INITIAL   STATE(pMOS) = stable0
                   AND c = u AND outerVal = 0;  
    
  AUTOMATON pMOS
  {
    STATE stable0   { INV   gate = 1;  
                        TRANS { GUARD   gate = 0;  
                                DO   c' = 0 AND count' = count + 1;  
                                GOTO unstable1;
                              }
                    }
    
    STATE unstable1 { INV   gate = 0 AND c+1 <= u;  
                        TRANS { GUARD   gate = 1;  
                                DO   c' = u AND count' + 1 = count;  
                                GOTO stable0;
                              }
                        TRANS { GUARD   gate = 0 AND l <= c;  
                                DO   outerVal' = 1 AND c' = u
                                     AND count' + 1 = count;  
                                GOTO stable1;
                              }
                    }

    STATE unstable0 { INV   gate = 1 AND c+1 <= u;  
                        TRANS { GUARD   gate = 1 AND l <= c;  
                                DO   outerVal' = 0 AND c' = u
                                     AND count' + 1 = count;  
                                GOTO stable0;
                              }
                        TRANS { GUARD   gate = 0;  
                                DO   c' = u AND count' + 1 = count;  
                                GOTO stable1; 
                              }
                    }

    STATE stable1   { INV   gate = 0;  
                        TRANS { GUARD   gate = 1;  
                                DO   c' = 0 AND count' = count + 1;  
                                GOTO unstable0; 
                              }
                    }
  }
 
  LOCAL
    c            : CLOCK(5); // up
}

// instance of nMOSTransistorHigh is initialized to high
MODULE nMOSTransistorHigh
{
  INPUT
    // gate is value at gate input of nMOS transistor.
    gate         : DISCRETE;
    l            : CONST;
    u            : CONST;
 
  OUTPUT
    // outerVal = 1 if transistor is... and 0 if transistor is...
    outerVal     : DISCRETE;
  
  MULTREST
    count        : DISCRETE; // Counts number of simultaneous transitions.
  
  // Initial gate input of nMOS transistor is 1 -> outerVal = 1.
  INITIAL   STATE(nMOS) = stable1
                   AND c = u AND outerVal = 1;  
    
  AUTOMATON nMOS
  {
    STATE stable0   { INV   gate = 0;  
                        TRANS { GUARD   gate = 1;  
                                DO   c' = 0 AND count' = count + 1;  
                                GOTO unstable1;
                              }
                    }
    
    STATE unstable1 { INV   gate = 1 AND c+1 <= u;  
                        TRANS { GUARD   gate = 0;  
                                DO   c' = u AND count' + 1 = count;  
                                GOTO stable0;
                              }
                        TRANS { GUARD   gate = 1 AND l <= c;  
                                DO   outerVal' = 1 AND c' = u
                                     AND count' + 1 = count;  
                                GOTO stable1;
                              }
                    }

    STATE unstable0 { INV   gate = 0 AND c+1 <= u;  
                        TRANS { GUARD   gate = 0 AND l <= c;  
                                DO   outerVal' = 0 AND c' = u
                                     AND count' + 1 = count;  
                                GOTO stable0;
                              }
                        TRANS { GUARD   gate = 1;  
                                DO   c' = u AND count' + 1 = count;  
                                GOTO stable1; 
                              }
                    }

    STATE stable1   { INV   gate = 1;  
                        TRANS { GUARD   gate = 0;  
                                DO   c' = 0 AND count' = count + 1;  
                                GOTO unstable0; 
                              }
                    }
  }
 
  LOCAL
    c            : CLOCK(4); // un
}

// instance of nMOSTransistorLow is initialized to low
MODULE nMOSTransistorLow
{
  INPUT
    // gate is value at gate input of nMOS transistor.
    gate         : DISCRETE;
    l            : CONST;
    u            : CONST;
 
  OUTPUT
    // outerVal = 1 if transistor is... and 0 if transistor is...
    outerVal     : DISCRETE;
  
  MULTREST
    count        : DISCRETE; // Counts number of simultaneous transitions.
 
  // Initial gate input of nMOS transistor is 0 -> outerVal = 0.
  INITIAL   STATE(nMOS) = stable0
                   AND c = u AND outerVal = 0;  
    
  AUTOMATON nMOS
  {
    STATE stable0   { INV   gate = 0;  
                        TRANS { GUARD   gate = 1;  
                                DO   c' = 0 AND count' = count + 1;  
                                GOTO unstable1;
                              }
                    }
    
    STATE unstable1 { INV   gate = 1 AND c+1 <= u;  
                        TRANS { GUARD   gate = 0;  
                                DO   c' = u AND count' + 1 = count;  
                                GOTO stable0;
                              }
                        TRANS { GUARD   gate = 1 AND l <= c;  
                                DO   outerVal' = 1 AND c' = u
				     AND count' + 1 = count;  
                                GOTO stable1;
                              }
                    }

    STATE unstable0 { INV   gate = 0 AND c+1 <= u;  
                        TRANS { GUARD   gate = 0 AND l <= c;  
                                DO   outerVal' = 0 AND c' = u
				     AND count' + 1 = count;  
                                GOTO stable0;
                              }
                        TRANS { GUARD   gate = 1;  
                                DO   c' = u AND count' + 1 = count;  
                                GOTO stable1; 
                              }
                    }

    STATE stable1   { INV   gate = 1;  
                        TRANS { GUARD   gate = 0;  
                                DO   c' = 0 AND count' = count + 1;  
                                GOTO unstable0; 
                              }
                    }
  }
 
  LOCAL
    c            : CLOCK(4); // un
}

// This module uses an instances of pMOS transistor and one instance of
// nMOS transistor to build a negator gate.
MODULE NegatorHigh
{
  INPUT
    lp           : CONST;
    ln           : CONST;
    up           : CONST;
    un           : CONST;

    input1       : DISCRETE;
    
  OUTPUT
    output1      : DISCRETE;

  MULTREST
    count        : DISCRETE; // Counts number of simultaneous transitions.

  LOCAL
    outputPMOS1  : DISCRETE(1);
   
  INST pMOS1 FROM pMOSTransistorHigh WITH
  {
    l            AS lp;
    u            AS up;
    gate         AS input1;  
    outerVal     AS outputPMOS1;
    count        AS count;
  }

  LOCAL
    outputNMOS1  : DISCRETE(1);

  INST nMOS1 FROM nMOSTransistorLow WITH
  {
    l            AS ln;
    u            AS un;
    gate         AS input1;  
    outerVal     AS outputNMOS1;
    count        AS count;
  }

  // Initial inputs are 0 so initial output of negator is 1.
  INITIAL   STATE(neg) = high
                   AND output1 = 1;  

  AUTOMATON neg
  {
    STATE high  { INV   NOT (outputPMOS1 = 0 AND outputNMOS1 = 1);
                       
                    TRANS { GUARD   outputPMOS1 = 0 AND outputNMOS1 = 1;  
                            DO   output1' = 0; 
                            GOTO low;
                          }
                }
    STATE low   { INV   NOT (outputPMOS1 = 1 AND outputNMOS1 = 0);
                       
                    TRANS { GUARD   outputPMOS1 = 1 AND outputNMOS1 = 0;  
                            DO   output1' = 1; 
                            GOTO high;
                          }
                }
  }
}

// This module uses two instances of pMOS transistors and two instances of
// nMOS transistors to build a NAND gate with two inputs.
MODULE NANDGateHigh
{
  INPUT
    lp           : CONST;
    ln           : CONST;
    up           : CONST;
    un           : CONST;

    input1       : DISCRETE;
    input2       : DISCRETE;
    
  OUTPUT
    output1      : DISCRETE;

  MULTREST
    count        : DISCRETE; // Counts number of simultaneous transitions.

  LOCAL
    outputPMOS1  : DISCRETE(1);
   
  INST pMOS1 FROM pMOSTransistorHigh WITH
  {
    l            AS lp;
    u            AS up;
    gate         AS input1;  
    outerVal     AS outputPMOS1;
    count        AS count;
  }

  LOCAL
    outputNMOS1  : DISCRETE(1);

  INST nMOS1 FROM nMOSTransistorLow WITH
  {
    l            AS ln;
    u            AS un;
    gate         AS input1;  
    outerVal     AS outputNMOS1;
    count        AS count;
  }

  LOCAL
    outputPMOS2  : DISCRETE(1);

  INST pMOS2 FROM pMOSTransistorHigh WITH
  {
    l            AS lp;
    u            AS up;
    gate         AS input2;  
    outerVal     AS outputPMOS2;
    count        AS count;
  }

  LOCAL
    outputNMOS2  : DISCRETE(1);

  INST nMOS2 FROM nMOSTransistorLow WITH
  {
    l            AS ln;
    u            AS un;
    gate         AS input2;  
    outerVal     AS outputNMOS2;
    count        AS count;
  }

  // Initial inputs are 0 so initial output of NAND-gate is 1.
  INITIAL   STATE(NAND) = high
                   AND output1 = 1;  

  AUTOMATON NAND
  {
    STATE high  { INV   NOT ((outputPMOS1 = 0 AND outputPMOS2 = 0)
                             AND (outputNMOS1 = 1 AND outputNMOS2 = 1));
                       
                    TRANS { GUARD   (outputPMOS1 = 0 AND outputPMOS2 = 0)
                                    AND (outputNMOS1 = 1 AND outputNMOS2 = 1); 
                            DO   output1' = 0; 
                            GOTO low;
                          }
                }
    STATE low   { INV   NOT ((outputPMOS1 = 1 OR outputPMOS2 = 1)
                         AND (outputNMOS1 = 0 OR outputNMOS2 = 0));
                       
                    TRANS { GUARD   (outputPMOS1 = 1 OR outputPMOS2 = 1)
                                    AND (outputNMOS1 = 0 OR outputNMOS2 = 0); 
                            DO   output1' = 1; 
                            GOTO high;
                          }
                }
  }
}

// This module uses two instances of pMOS transistors and two instances of
// nMOS transistors to build a NOR gate with two inputs.
MODULE NORGateLow
{
 
  INPUT
    lp           : CONST;
    ln           : CONST;
    up           : CONST;
    un           : CONST;

    input1       : DISCRETE;
    input2       : DISCRETE;
    
  OUTPUT
    output1      : DISCRETE;

  MULTREST
    count        : DISCRETE; // Counts number of simultaneous transitions.

  LOCAL
    outputPMOS1  : DISCRETE(1);
   
  INST pMOS1 FROM pMOSTransistorLow WITH
  {
    l            AS lp;
    u            AS up;
    gate         AS input1;  
    outerVal     AS outputPMOS1;
    count        AS count;
  }

  LOCAL
    outputNMOS1  : DISCRETE(1);

  INST nMOS1 FROM nMOSTransistorHigh WITH
  {
    l            AS ln;
    u            AS un;
    gate         AS input1;  
    outerVal     AS outputNMOS1;
    count        AS count;
  }

  LOCAL
    outputPMOS2  : DISCRETE(1);

  INST pMOS2 FROM pMOSTransistorLow WITH
  {
    l            AS lp;
    u            AS up;
    gate         AS input2;  
    outerVal     AS outputPMOS2;
    count        AS count;
  }

  LOCAL
    outputNMOS2  : DISCRETE(1);

  INST nMOS2 FROM nMOSTransistorHigh WITH
  {
    l            AS ln;
    u            AS un;
    gate         AS input2;  
    outerVal     AS outputNMOS2;
    count        AS count;
  }

  // The initial inputs of this NOR-gate is !C and the initial output of
  // the NAND gate. Inputs are 1 so initial output of NOR gate is zero.
  INITIAL   STATE(NOR) = low
                   AND output1 = 0;  

  AUTOMATON NOR
  {
    STATE high  { INV   NOT ((outputPMOS1 = 0 OR outputPMOS2 = 0)
                             AND (outputNMOS1 = 1 OR outputNMOS2 = 1));
                       
                    TRANS { GUARD   (outputPMOS1 = 0 OR outputPMOS2 = 0)
                                    AND (outputNMOS1 = 1 OR outputNMOS2 = 1); 
                            DO   output1' = 0;  
                            GOTO low;
                           }
                }
    STATE low   { INV   NOT ((outputPMOS1 = 1 AND outputPMOS2 = 1) 
                             AND (outputNMOS1 = 0 AND outputNMOS2 = 0));
                       
                    TRANS { GUARD   (outputPMOS1 = 1 AND outputPMOS2 = 1)
                                    AND (outputNMOS1 = 0 AND outputNMOS2 = 0); 
                            DO   output1' = 1; 
                            GOTO high; 
                          }
                }
  }
}

// This module switches inputs once to random values if 0 <= clock < lx and
// keeps value if lx <= clock < ux. Initial value is 0.
MODULE inputLow
{
  INPUT
    lx           : CONST;
    ux           : CONST;
    c            : CLOCK;    

  OUTPUT
    outerVal     : DISCRETE;

  INITIAL   STATE(inp) = unstable
                   AND outerVal = 0;   

  AUTOMATON inp
  {
    STATE unstable { TRANS { GUARD   c + 1 <= lx;  
                             DO   outerVal' = 0 OR outerVal' = 1;   
                             GOTO stable;
                           }
                   }

    STATE stable   { INV   c <= lx;  
                       TRANS { GUARD   c = lx;  
                               GOTO unstable;
                             }
                   }
  }
}

// This module switches inputs once to random values if 0 <= clock < lx and
// keeps value if lx <= clock < ux. Initial value is 1.
MODULE inputHigh
{
  INPUT
    lx           : CONST;
    ux           : CONST;
    c            : CLOCK;    

  OUTPUT
    outerVal     : DISCRETE;

  INITIAL   STATE(inp) = unstable
                   AND outerVal = 1;   

  AUTOMATON inp
  {
    STATE unstable { TRANS { GUARD   c + 1 <= lx;  
                             DO   outerVal' = 0 OR outerVal' = 1;   
                             GOTO stable;
                           }
                   }

    STATE stable   { INV   c <= lx;  
                       TRANS { GUARD   c = lx;  
                               GOTO unstable;
                             }
                   }
  }
}

// This module uses one instance of NAND-gate, one instance of a NOR-gate
// and one instance of a negator to build an AND-gate with three inputs.
// Boolean equation: !( !(A && B) || !C) = A && B && C
MODULE System
{
  // All constants have been divided by greatest common 
  // divisor of them.
  // See paper: "Some Progress in the Symbolic Verification 
  // of Timed Automata"
  // pages 8 - 9 (MOS Circuits).
  
  // l? minimal needed time to change output after change of 
  // input.
  // u? maximal needed time... .
  // ?p is time for pMOS transistor,
  // ?n for nMOS transistor.
  
  // Inputs can change if 0 <= clock < lx 
  // and inputs are stable if lx <= clock < ux.  

  LOCAL
    c            : CLOCK(16); // c = ux + 1  

  INITIAL   STATE(resetClock) = reset
                   AND c = 0 AND count = 0;  

  AUTOMATON resetClock
  {
    STATE reset { INV   c <= ux;  
                  TRANS { GUARD   c >= ux;  
                          DO   c' = 0;  
                          GOTO reset;
                        }
                } 
  }
  
  LOCAL
    count        : DISCRETE(10);
    lx = 6       : CONST; // 24 / 40 / 20 / 40
    ux = 15      : CONST; // 56 / 56 / 60 / 60
    
  LOCAL
    lp = 2       : CONST; //  8 / 10
    ln = 2       : CONST; //  8
    up = 5       : CONST; // 20 
    un = 4       : CONST; // 16
    input1       : DISCRETE(1);
  
  INST inp1 FROM inputLow WITH
  {
    lx           AS lx;
    ux           AS ux;
    outerVal     AS input1;
    c            AS c;
  } 

  LOCAL
    input2       : DISCRETE(1);     

  INST inp2 FROM inputLow WITH
  {
    lx           AS lx;
    ux           AS ux;
    outerVal     AS input2;
    c            AS c;
  }  

  LOCAL
    outputNAND1  : DISCRETE(1);
    
  INST NAND1 FROM NANDGateHigh WITH
  {
    lp           AS lp;
    ln           AS ln;
    up           AS up;
    un           AS un;
    input1       AS input1;
    input2       AS input2;
    output1      AS outputNAND1;
    count        AS count;
  }

  LOCAL
    input3       : DISCRETE(1);
  
  INST inp3 FROM inputLow WITH
  {
    lx           AS lx;
    ux           AS ux;
    outerVal     AS input3;
    c            AS c;
  }

  LOCAL
    outputNeg1  : DISCRETE(1);

  INST neg1 FROM NegatorHigh WITH
  {
    lp           AS lp;
    ln           AS ln;
    up           AS up;
    un           AS un;
    input1       AS input3;
    output1      AS outputNeg1;
    count        AS count;
  }

  LOCAL
    outerVal1    : DISCRETE(1);  

  INST NOR1 FROM NORGateLow WITH
  {
    lp           AS lp;
    ln           AS ln;
    up           AS up;
    un           AS un;
    input1       AS outputNAND1;
    input2       AS outputNeg1;
    output1      AS outerVal1;
    count        AS count;
  }
}

REGION CHECK System
{
  VAR 
    initial, exSequence: REGION;

  COMMANDS
    initial := INITIALREGION;
    
    exSequence := count >= 6;
    IF( ISREACHABLE FROM initial TO exSequence FORWARD ) {
      PRINT "6 (or more) simultaneous transitions possible."; }
    ELSE {
      PRINT "NO 6 (or more) simultaneous transitions possible."; } 
}

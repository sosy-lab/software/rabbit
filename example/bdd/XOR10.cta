/* 
 * project: implementation of 10 chained XOR gates in CTA
 * author:  Michael Vogel - (mvogel@informatik.tu-cottbus.de)
 *
 * Based on paper "Some Progress in the Symbolic Verification of Timed 
 *                 Automata" pages 7 - 8.
 */   

MODULE XORGate
{
  INPUT
    // prevOuterVal is value at exit of delay element from previous XOR gate.
    prevOuterVal: DISCRETE;
    l:            CONST;
    u:            CONST;
 
  OUTPUT
    // outerVal is value at exit of delay element from this XOR gate.
    outerVal:     DISCRETE;
  
  LOCAL
    // innerVal is value of this XOR gate.
    innerVal:     DISCRETE(1);

  INITIAL   STATE(XOR) = unstable2
                   AND c = 0 AND outerVal = 1 AND innerVal = 0;  
    
  AUTOMATON XOR
  {
    STATE stable1   { INV   prevOuterVal = 0;  
                        TRANS { GUARD   prevOuterVal = 1;  
                                DO   c' = 0 AND innerVal' = 1;  
			        GOTO unstable1;
                              }
                    }
    
    STATE unstable1 { INV   prevOuterVal = 1 AND c <= u;  
                        TRANS { GUARD   prevOuterVal = 0;  
	                        DO   innerVal' = 0;  
                                GOTO stable1;
                              }
                        TRANS { GUARD   prevOuterVal = 1 AND l <= c;  
                                DO   c' = 0 AND outerVal' = 1 
                                     AND innerVal' = 0;  
                                GOTO unstable2;
                              }
                        TRANS { GUARD   prevOuterVal = 0 AND l <= c;  
			        DO   outerVal' = 1;  
                                GOTO stable2;
                              }
                    }

    STATE unstable2 { INV   prevOuterVal = 1 AND c <= u;  
                        TRANS { GUARD   prevOuterVal = 0 AND l <= c;  
                                DO   outerVal' = 0;  
                                GOTO stable1;
                              }
                            // in paper prevOuterVal = 0.
                        TRANS { GUARD   prevOuterVal = 1 AND l <= c;  
			        DO   c' = 0 AND outerVal' = 0 
                                     AND innerVal' = 1;  
                                GOTO unstable1; 
                              }
                            // In Paper prevOuterVal = 1.
                        TRANS { GUARD   prevOuterVal = 0;  
                                DO   innerVal' = 1;    
				GOTO stable2;
                              }
    
                    }

    STATE stable2   { INV   prevOuterVal = 0;  
                            // In Paper prevOuterVal = 0.
                        TRANS { GUARD   prevOuterVal = 1;  
			        DO   c' = 0 AND innerVal' = 0;  
                                GOTO unstable2; 
                              }
                    }
  }
 
  LOCAL
    c:            CLOCK(7);  // (u + 1)

}

MODULE System
{
  LOCAL
    l = 4        : CONST;
    u = 7        : CONST;
    outerVal1    : DISCRETE(1);
    outerVal10   : DISCRETE(1);

  INST Gate1 FROM XORGate WITH
  {
    l            AS l;
    u            AS u;
    prevOuterVal AS outerVal10;  
    outerVal     AS outerVal1;
  } 
  
  LOCAL
    outerVal2    : DISCRETE(1);

  INST Gate2 FROM XORGate WITH
  {
    l            AS l;
    u            AS u;
    prevOuterVal AS outerVal1;
    outerVal     AS outerVal2;
  }

  LOCAL
    outerVal3    : DISCRETE(1);

  INST Gate3 FROM XORGate WITH
  {
    l            AS l;
    u            AS u;
    prevOuterVal AS outerVal2;
    outerVal     AS outerVal3;
  }

  LOCAL
    outerVal4    : DISCRETE(1);

  INST Gate4 FROM XORGate WITH
  {
    l            AS l;
    u            AS u;
    prevOuterVal AS outerVal3;
    outerVal     AS outerVal4;
  }

  LOCAL
    outerVal5    : DISCRETE(1);

  INST Gate5 FROM XORGate WITH
  {
    l            AS l;
    u            AS u;
    prevOuterVal AS outerVal4;
    outerVal     AS outerVal5;
  }

  LOCAL
    outerVal6    : DISCRETE(1);
  
  INST Gate6 FROM XORGate WITH
  {
    l            AS l;
    u            AS u;
    prevOuterVal AS outerVal5;
    outerVal     AS outerVal6;
  }

 LOCAL
    outerVal7    : DISCRETE(1);

  INST Gate7 FROM XORGate WITH
  {
    l            AS l;
    u            AS u;
    prevOuterVal AS outerVal6;
    outerVal     AS outerVal7;
  }

 LOCAL
    outerVal8    : DISCRETE(1);

  INST Gate8 FROM XORGate WITH
  {
    l            AS l;
    u            AS u;
    prevOuterVal AS outerVal7;
    outerVal     AS outerVal8;
  }

 LOCAL
    outerVal9    : DISCRETE(1);

  INST Gate9 FROM XORGate WITH
  {
    l            AS l;
    u            AS u;
    prevOuterVal AS outerVal8;
    outerVal     AS outerVal9;
  }

  INST Gate10 FROM XORGate WITH
  {
    l            AS l;
    u            AS u;
    prevOuterVal AS outerVal9;
    outerVal     AS outerVal10;
  }
}

REGION CHECK System
{
  VAR 
    initial, stable: REGION;

  COMMANDS
    initial := INITIALREGION;
   
    stable := STATE(Gate1.XOR) = Gate1.stable1
      INTERSECT STATE(Gate2.XOR) = Gate2.stable1
      INTERSECT STATE(Gate3.XOR) = Gate3.stable1;

    IF( ISREACHABLE FROM initial TO FALSE FORWARD ) {
      PRINT "State stable1 reached."; }
    ELSE {
      PRINT "State stable1 not reached."; } 
}

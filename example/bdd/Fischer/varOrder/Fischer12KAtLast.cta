/* 
 * project: CTA-model of Fischer's mutual exclusion protocol.
 * modules: Process, System
 * author:  Dirk Beyer, 2000
 * last change: 2001-03-28 mvogel
 * 
 * Based on: Leslie Lamport, "A fast mutual exclusion algorithm.",
 *           ACM Transactions on Computer Systems, 5(1):1-11, 1987.
 *
 * This model uses a variable ordering with variable k as last element.
 * The BDD grows exponentially when using this bad variable ordering.
 */ 

// This module represents the template for a Fischer process.
MODULE Process
{
  // Constants for time bounds.
  INPUT
    // a is the maximal time the modelled assignment k:=1 needs.
    // b is the minimal time the process waits for assignments
    a:          CONST;
    b:          CONST;
    // Parameter for significant value of k.
    processNo:  CONST;

  MULTREST
    // k is the flag for announcement.
    k: DISCRETE;
    // Counter for number of processes staying in critical section.
    critCnt: DISCRETE;

  // Set initial state of automaton and variable values.
  INITIAL   STATE(Fischer) = uncritical AND x >= 2;  

  AUTOMATON Fischer
  {
    STATE uncritical { TRANS { GUARD   k  = 0;  
                               DO   x' = 0;  
			       GOTO assign;
			     }
		     }

    STATE assign     { INV   x <= a;  
		       TRANS { DO   x' = 0 AND k' = processNo;  
			       GOTO wait;
			     }
                     }

    STATE wait       { TRANS { GUARD   x >= b AND k != processNo;  
			       GOTO uncritical;
			     }
                       TRANS { GUARD   x >= b AND k = processNo;  
                               DO   critCnt' = critCnt + 1;  
			       GOTO critical;
			     }
                     }

    STATE critical   { TRANS { DO   k' = 0 AND critCnt' + 1 = critCnt;  
			       GOTO uncritical;
			     }
    }
  }

  LOCAL
    // A clock to measure the time in a state.
    x:  CLOCK(2);
}

MODULE System
{
  LOCAL
    // a is the maximal time the modelled assignment k:=1 needs.
    // b is the minimal time the process waits for assignments
    a = 1: CONST;
    b = 2: CONST;

    // Process ID's.
    pNo1 = 1: CONST;
    pNo2 = 2: CONST;
    pNo3 = 3: CONST;
    pNo4 = 4: CONST;
    pNo5 = 5: CONST;
    pNo6 = 6: CONST;
    pNo7 = 7: CONST;
    pNo8 = 8: CONST;
    pNo9 = 9: CONST;
    pNo10 = 10: CONST;
    pNo11 = 11: CONST;
    pNo12 = 12: CONST;

  INITIAL   k = 0 AND critCnt = 0;  

  // Instances of process template.
  INST Proc01 FROM Process WITH
  {
    a         AS a;
    b         AS b;
    processNo AS pNo1;
    k         AS k;
    critCnt   AS critCnt;
  }

  INST Proc02 FROM Process WITH
  {
    a         AS a;
    b         AS b;
    processNo AS pNo2;
    k         AS k;
    critCnt   AS critCnt;
  }

  INST Proc03 FROM Process WITH
  {
    a         AS a;
    b         AS b;
    processNo AS pNo3;
    k         AS k;
    critCnt   AS critCnt;
  }

  INST Proc04 FROM Process WITH
  {
    a         AS a;
    b         AS b;
    processNo AS pNo4;
    k         AS k;
    critCnt   AS critCnt;
  }

  INST Proc05 FROM Process WITH
  {
    a         AS a;
    b         AS b;
    processNo AS pNo5;
    k         AS k;
    critCnt   AS critCnt;
  }

  INST Proc06 FROM Process WITH
  {
    a         AS a;
    b         AS b;
    processNo AS pNo6;
    k         AS k;
    critCnt   AS critCnt;
  }

  INST Proc07 FROM Process WITH
  {
    a         AS a;
    b         AS b;
    processNo AS pNo7;
    k         AS k;
    critCnt   AS critCnt;
  }

  INST Proc08 FROM Process WITH
  {
    a         AS a;
    b         AS b;
    processNo AS pNo8;
    k         AS k;
    critCnt   AS critCnt;
  }

  INST Proc09 FROM Process WITH
  {
    a         AS a;
    b         AS b;
    processNo AS pNo9;
    k         AS k;
    critCnt   AS critCnt;
  }

  INST Proc10 FROM Process WITH
  {
    a         AS a;
    b         AS b;
    processNo AS pNo10;
    k         AS k;
    critCnt   AS critCnt;
  }

  INST Proc11 FROM Process WITH
  {
    a         AS a;
    b         AS b;
    processNo AS pNo11;
    k         AS k;
    critCnt   AS critCnt;
  }

  INST Proc12 FROM Process WITH
  {
    a         AS a;
    b         AS b;
    processNo AS pNo12;
    k         AS k;
    critCnt   AS critCnt;
  }

  // Variable k is placed at last element to demonstrate a bad variable
  // ordering.
  LOCAL                // MULTREST for the submodules.
    k: DISCRETE(13);
    critCnt: DISCRETE(3);

}


// Analysis section:
// The module name which should be checked and the type of the internal
// representation for the region of reachable configurations is specified.
REACHABILITY CHECK System
USE BDD
{ 
  // Declaration of region variables. 
  VAR
    reached, error : REGION;

  COMMANDS

    // Define an error region. If this region is reachable from the initial
    // region the mutual exclusion is violated - this means there are two
    // Fischer processes staying in the critical section at the same time.
    // This region (situation) should not be reachable (possible).
    error := critCnt = 2;

    // The ISREACHABLE command executes an 'on the fly-' reachability check
    // to verify if the specified region 'error' is reachable or not.
    IF( ISREACHABLE FROM INITIALREGION TO error FORWARD ) 
    {
      PRINT "Mutual exclusion violated";
    }
    ELSE 
    {
      PRINT "Mutual exclusion satisfied"; 
    } 
    
    // The 'REACH FROM ...' command calculates the full region of all
    // reachable configurations whereas the ISREACHABLE command (see above)
    // only executes an 'on the fly-' reachability check.
    // The intersection of the region of all reachable configurations and
    // the error region (as defined above) should be empty to satisfy the
    // mutual exclusion.
    /* reached := (REACH FROM INITIALREGION FORWARD) INTERSECT error;
       IF ( EMPTY(reached) )
       {
         PRINT "Mutual exclusion satisfied."; 
       }
       ELSE
       {
         PRINT "Mutual exclusion violated."; 
       }
    */

    // The PRINTBDDESTIM command prints the estimated number of
    // BDD nodes for each bit in the BDD to stdout.
    //PRINTBDDESTIM;

    // The PRINTBDDGRAPH command prints the real number of
    // BDD nodes for each bit in the specified region-BDD to stdout.
    //PRINTBDDGRAPH INITIALREGION;
}

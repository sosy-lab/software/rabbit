/* 
 * project: CTA-model of Fischer's mutual exclusion protocol.
 * modules: Process, System
 * author:  Dirk Beyer, 2000
 * last change: 2001-03-28 mvogel
 * 
 * Based on: Leslie Lamport, "A fast mutual exclusion algorithm.",
 *           ACM Transactions on Computer Systems, 5(1):1-11, 1987.
 */ 

// This module represents the template for a Fischer process.
MODULE Process
{
  // Constants for time bounds.
  INPUT
    // a is the maximal time the modelled assignment k:=1 needs.
    // b is the minimal time the process waits for assignments
    a:          CONST;
    b:          CONST;
    // Parameter for significant value of k.
    processNo:  CONST;

  MULTREST
    // k is the flag for announcement.
    k: DISCRETE;
    // Counter for number of processes staying in critical section.
    critCnt: DISCRETE;

  // Set initial state of automaton and variable values.
  INITIAL   STATE(Fischer) = uncritical AND x >= 4;  

  AUTOMATON Fischer
  {
    STATE uncritical { TRANS { GUARD   k  = 0;  
                               DO   x' = 0;  
			       GOTO assign;
			     }
		     }

    STATE assign     { INV   x <= a;  
		       TRANS { DO   x' = 0 AND k' = processNo;  
			       GOTO wait;
			     }
                     }

    STATE wait       { TRANS { GUARD   x >= b AND k != processNo;  
			       GOTO uncritical;
			     }
                       TRANS { GUARD   x >= b AND k = processNo;  
                               DO   critCnt' = critCnt + 1;  
			       GOTO critical;
			     }
                     }

    STATE critical   { TRANS { DO   k' = 0 AND critCnt' + 1 = critCnt;  
			       GOTO uncritical;
			     }
    }
  }

  LOCAL
    // A clock to measure the time in a state.
    x:  CLOCK(4);
}

MODULE System
{
  LOCAL
    // a is the maximal time the modelled assignment k:=1 needs.
    // b is the minimal time the process waits for assignments
    a = 3: CONST;
    b = 4: CONST;

    // Process ID's.
    pNo1 = 1: CONST;
    pNo2 = 2: CONST;
    pNo3 = 3: CONST;
    pNo4 = 4: CONST;
    pNo5 = 5: CONST;
    pNo6 = 6: CONST;
    pNo7 = 7: CONST;
    pNo8 = 8: CONST;
    pNo9 = 9: CONST;
    pNo10 = 10: CONST;
    pNo11 = 11: CONST;
    pNo12 = 12: CONST;
    pNo13 = 13: CONST;
    pNo14 = 14: CONST;
    pNo15 = 15: CONST;
    pNo16 = 16: CONST;
    pNo17 = 17: CONST;
    pNo18 = 18: CONST;
    pNo19 = 19: CONST;
    pNo20 = 20: CONST;
    pNo21 = 21: CONST;
    pNo22 = 22: CONST;
    pNo23 = 23: CONST;
    pNo24 = 24: CONST;
    pNo25 = 25: CONST;
    pNo26 = 26: CONST;
    pNo27 = 27: CONST;
    pNo28 = 28: CONST;
    pNo29 = 29: CONST;
    pNo30 = 30: CONST;
    pNo31 = 31: CONST;
    pNo32 = 32: CONST;

  LOCAL                // MULTREST for the submodules.
    k: DISCRETE(33);
    critCnt: DISCRETE(3);
	
  INITIAL k = 0 AND critCnt = 0;  

  // Instances of process template.
  INST Proc01 FROM Process WITH
  {
    a         AS a;
    b         AS b;
    processNo AS pNo1;
    k         AS k;
    critCnt   AS critCnt;
  }

  INST Proc02 FROM Process WITH
  {
    a         AS a;
    b         AS b;
    processNo AS pNo2;
    k         AS k;
    critCnt   AS critCnt;
  }

  INST Proc03 FROM Process WITH
  {
    a         AS a;
    b         AS b;
    processNo AS pNo3;
    k         AS k;
    critCnt   AS critCnt;
  }

  INST Proc04 FROM Process WITH
  {
    a         AS a;
    b         AS b;
    processNo AS pNo4;
    k         AS k;
    critCnt   AS critCnt;
  }

  INST Proc05 FROM Process WITH
  {
    a         AS a;
    b         AS b;
    processNo AS pNo5;
    k         AS k;
    critCnt   AS critCnt;
  }

  INST Proc06 FROM Process WITH
  {
    a         AS a;
    b         AS b;
    processNo AS pNo6;
    k         AS k;
    critCnt   AS critCnt;
  }

  INST Proc07 FROM Process WITH
  {
    a         AS a;
    b         AS b;
    processNo AS pNo7;
    k         AS k;
    critCnt   AS critCnt;
  }

  INST Proc08 FROM Process WITH
  {
    a         AS a;
    b         AS b;
    processNo AS pNo8;
    k         AS k;
    critCnt   AS critCnt;
  }

  INST Proc09 FROM Process WITH
  {
    a         AS a;
    b         AS b;
    processNo AS pNo9;
    k         AS k;
    critCnt   AS critCnt;
  }

  INST Proc10 FROM Process WITH
  {
    a         AS a;
    b         AS b;
    processNo AS pNo10;
    k         AS k;
    critCnt   AS critCnt;
  }

  INST Proc11 FROM Process WITH
  {
    a         AS a;
    b         AS b;
    processNo AS pNo11;
    k         AS k;
    critCnt   AS critCnt;
  }

  INST Proc12 FROM Process WITH
  {
    a         AS a;
    b         AS b;
    processNo AS pNo12;
    k         AS k;
    critCnt   AS critCnt;
  }

  INST Proc13 FROM Process WITH
  {
    a         AS a;
    b         AS b;
    processNo AS pNo13;
    k         AS k;
    critCnt   AS critCnt;
  }

  INST Proc14 FROM Process WITH
  {
    a         AS a;
    b         AS b;
    processNo AS pNo14;
    k         AS k;
    critCnt   AS critCnt;
  }

  INST Proc15 FROM Process WITH
  {
    a         AS a;
    b         AS b;
    processNo AS pNo15;
    k         AS k;
    critCnt   AS critCnt;
  }

  INST Proc16 FROM Process WITH
  {
    a         AS a;
    b         AS b;
    processNo AS pNo16;
    k         AS k;
    critCnt   AS critCnt;
  }

  INST Proc17 FROM Process WITH
  {
    a         AS a;
    b         AS b;
    processNo AS pNo17;
    k         AS k;
    critCnt   AS critCnt;
  }

  INST Proc18 FROM Process WITH
  {
    a         AS a;
    b         AS b;
    processNo AS pNo18;
    k         AS k;
    critCnt   AS critCnt;
  }

  INST Proc19 FROM Process WITH
  {
    a         AS a;
    b         AS b;
    processNo AS pNo19;
    k         AS k;
    critCnt   AS critCnt;
  }

  INST Proc20 FROM Process WITH
  {
    a         AS a;
    b         AS b;
    processNo AS pNo20;
    k         AS k;
    critCnt   AS critCnt;
  }

  INST Proc21 FROM Process WITH
  {
    a         AS a;
    b         AS b;
    processNo AS pNo21;
    k         AS k;
    critCnt   AS critCnt;
  }

  INST Proc22 FROM Process WITH
  {
    a         AS a;
    b         AS b;
    processNo AS pNo22;
    k         AS k;
    critCnt   AS critCnt;
  }

  INST Proc23 FROM Process WITH
  {
    a         AS a;
    b         AS b;
    processNo AS pNo23;
    k         AS k;
    critCnt   AS critCnt;
  }

  INST Proc24 FROM Process WITH
  {
    a         AS a;
    b         AS b;
    processNo AS pNo24;
    k         AS k;
    critCnt   AS critCnt;
  }

  INST Proc25 FROM Process WITH
  {
    a         AS a;
    b         AS b;
    processNo AS pNo25;
    k         AS k;
    critCnt   AS critCnt;
  }

  INST Proc26 FROM Process WITH
  {
    a         AS a;
    b         AS b;
    processNo AS pNo26;
    k         AS k;
    critCnt   AS critCnt;
  }

  INST Proc27 FROM Process WITH
  {
    a         AS a;
    b         AS b;
    processNo AS pNo27;
    k         AS k;
    critCnt   AS critCnt;
  }

  INST Proc28 FROM Process WITH
  {
    a         AS a;
    b         AS b;
    processNo AS pNo28;
    k         AS k;
    critCnt   AS critCnt;
  }

  INST Proc29 FROM Process WITH
  {
    a         AS a;
    b         AS b;
    processNo AS pNo29;
    k         AS k;
    critCnt   AS critCnt;
  }

  INST Proc30 FROM Process WITH
  {
    a         AS a;
    b         AS b;
    processNo AS pNo30;
    k         AS k;
    critCnt   AS critCnt;
  }

  INST Proc31 FROM Process WITH
  {
    a         AS a;
    b         AS b;
    processNo AS pNo31;
    k         AS k;
    critCnt   AS critCnt;
  }

  INST Proc32 FROM Process WITH
  {
    a         AS a;
    b         AS b;
    processNo AS pNo32;
    k         AS k;
    critCnt   AS critCnt;
  }
}


// Analysis section:
// The module name which should be checked and the type of the internal
// representation for the region of reachable configurations is specified.
REACHABILITY CHECK System
USE BDD
{ 
  // Declaration of region variables. 
  VAR
    reached, error : REGION;

  COMMANDS

    // Define an error region. If this region is reachable from the initial
    // region the mutual exclusion is violated - this means there are two
    // Fischer processes staying in the critical section at the same time.
    // This region (situation) should not be reachable (possible).
    error := critCnt = 2;
  
    // The ISREACHABLE command executes an 'on the fly-' reachability check
    // to verify if the specified region 'error' is reachable or not.
    IF( ISREACHABLE FROM INITIALREGION TO error FORWARD ) 
    {
      PRINT "Mutual exclusion violated";
    }
    ELSE 
    {
      PRINT "Mutual exclusion satisfied"; 
    } 
  
    /*
    // The 'REACH FROM ...' command calculates the full region of all
    // reachable configurations whereas the ISREACHABLE command (see above)
    // only executes an 'on the fly-' reachability check.
    // The intersection of the region of all reachable configurations and
    // the error region (as defined above) should be empty to satisfy the
    // mutual exclusion.
     reached := REACH FROM INITIALREGION FORWARD;
       IF ( EMPTY(reached INTERSECT error))
       {
         PRINT "Mutual exclusion satisfied."; 
       }
       ELSE
       {
         PRINT "Mutual exclusion violated."; 
       }
    */
    // The PRINTBDDESTIM command prints the estimated number of
    // BDD nodes for each bit in the BDD to stdout.
    //PRINTBDDESTIM;

    // The PRINTBDDGRAPH command prints the real number of
    // BDD nodes for each bit in the specified region-BDD to stdout.
    //PRINTBDDGRAPH INITIALREGION;
}

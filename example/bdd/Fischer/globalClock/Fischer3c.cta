MODULE Process
{
  // Constants for time bounds.
  INPUT
    // a is the maximal time the modelled assignment k:=1 needs.
    // b is the minimal time the process waits for assignments
    a:          CONST;
    b:          CONST;
    // Parameter for significant value of k.
    processNo:  CONST;

  MULTREST
    // k is the flag for announcement.
    k: DISCRETE;
    c: CLOCK;
    critCnt: DISCRETE;

  INITIAL   STATE(Fischer) = uncritical AND x >= 2;  

  AUTOMATON Fischer
  {
    STATE uncritical { TRANS { GUARD   k  = 0;  
                               DO   x' = 0;  
			       GOTO assign;
			     }
		     }

    STATE assign     { INV   x <= a;  
		       TRANS { GUARD   k  = 0;  
                               DO   x' = 0 AND k' = processNo AND c' = 0;  
			       GOTO wait;
			     }
		       TRANS { GUARD   k != 0;  
			       DO   x' = 0 AND k' = processNo;  
			       GOTO wait;
			     }
                     }

    STATE wait       { TRANS { GUARD   x >= b AND k != processNo;  
			       GOTO uncritical;
			     }
                       TRANS { GUARD   x >= b AND k = processNo;  
                               DO   critCnt' = critCnt + 1;  
			       GOTO critical;
			     }
                     }

    STATE critical   { TRANS { DO   k' = 0 AND c' = 0
                                    AND critCnt' + 1 = critCnt;  
			       GOTO uncritical;
			     }
    }
  }

  LOCAL
    // A clock to measure the time in a state.
    x:  CLOCK(2);
}

MODULE System
{
  LOCAL
    a = 1: CONST;
    a1 = 2: CONST;
    b = 2: CONST;

    pNo1 = 1: CONST;
    pNo2 = 2: CONST;
    pNo3 = 3: CONST;

  LOCAL                // MULTREST for the submodules.
    k: DISCRETE(4);
    c: CLOCK(2);
    critCnt: DISCRETE(3);
	
  INITIAL   k = 0 AND c = 0 AND critCnt = 0;  

  INST Proc1 FROM Process WITH
  {
    a         AS a;
    b         AS b;
    processNo AS pNo1;
    k         AS k;
    c	      AS c;
    critCnt   AS critCnt;
  }

  INST Proc2 FROM Process WITH
  {
    a         AS a;
    b         AS b;
    processNo AS pNo2;
    k         AS k;
    c         AS c;
    critCnt   AS critCnt;
  }

  INST Proc3 FROM Process WITH
  {
    a         AS a;
    b         AS b;
    processNo AS pNo3;
    k         AS k;
    c         AS c;
    critCnt   AS critCnt;
  }
}


REACHABILITY CHECK System
USE BDD
{
  VAR
    initial, error : REGION;

  COMMANDS
    initial := INITIALREGION;

    error   := critCnt = 2;

    IF( ISREACHABLE FROM initial TO error FORWARD ) 
    {
      PRINT "Mutual exclusion violated";
    }
    ELSE 
    {
      PRINT "Mutual exclusion satisfied"; 
    } 
}

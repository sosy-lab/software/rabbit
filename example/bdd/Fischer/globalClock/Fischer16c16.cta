MODULE Process
{
  // Constants for time bounds.
  INPUT
    // a is the maximal time the modelled assignment k:=1 needs.
    // b is the minimal time the process waits for assignments
    a:          CONST;
    b:          CONST;
    // Parameter for significant value of k.
    processNo:  CONST;

  MULTREST
    // k is the flag for announcement.
    k: DISCRETE;
    c: CLOCK;
    critCnt: DISCRETE;

  INITIAL   STATE(Fischer) = uncritical AND x >= 16;

  AUTOMATON Fischer
  {
    STATE uncritical { TRANS { GUARD   k  = 0;  
                               DO   x' = 0;  
			       GOTO assign;
			     }
		     }

    STATE assign     { INV   x <= a;  
		       TRANS { GUARD   k  = 0;  
                               DO   x' = 0 AND k' = processNo AND c' = 0;  
			       GOTO wait;
			     }
		       TRANS { GUARD   k != 0;  
			       DO   x' = 0 AND k' = processNo;  
			       GOTO wait;
			     }
                     }

    STATE wait       { TRANS { GUARD   x >= b AND k != processNo;  
			       GOTO uncritical;
			     }
                       TRANS { GUARD   x >= b AND k = processNo;  
                               DO   critCnt' = critCnt + 1;  
			       GOTO critical;
			     }
                     }

    STATE critical   { TRANS { DO   k' = 0 AND c' = 0
                                    AND critCnt' + 1 = critCnt;  
			       GOTO uncritical;
			     }
    }
  }

  LOCAL
    // A clock to measure the time in a state.
    x:  CLOCK(16);
}

MODULE System
{
  LOCAL
    a = 15: CONST;
    b = 16: CONST;

    pNo1 = 1: CONST;
    pNo2 = 2: CONST;
    pNo3 = 3: CONST;
    pNo4 = 4: CONST;
    pNo5 = 5: CONST;
    pNo6 = 6: CONST;
    pNo7 = 7: CONST;
    pNo8 = 8: CONST;
    pNo9 = 9: CONST;
    pNo10 = 10: CONST;
    pNo11 = 11: CONST;
    pNo12 = 12: CONST;
    pNo13 = 13: CONST;
    pNo14 = 14: CONST;
    pNo15 = 15: CONST;
    pNo16 = 16: CONST;

  LOCAL                // MULTREST for the submodules.
    k: DISCRETE(17);
    c: CLOCK(16);
    critCnt: DISCRETE(3);
	
  INITIAL k = 0 AND c = 0 AND critCnt = 0;  

  INST Proc01 FROM Process WITH
  {
    a         AS a;
    b         AS b;
    processNo AS pNo1;
    k         AS k;
    c	      AS c;
    critCnt   AS critCnt;
  }

  INST Proc02 FROM Process WITH
  {
    a         AS a;
    b         AS b;
    processNo AS pNo2;
    k         AS k;
    c         AS c;
    critCnt   AS critCnt;
  }

  INST Proc03 FROM Process WITH
  {
    a         AS a;
    b         AS b;
    processNo AS pNo3;
    k         AS k;
    c         AS c;
    critCnt   AS critCnt;
  }

  INST Proc04 FROM Process WITH
  {
    a         AS a;
    b         AS b;
    processNo AS pNo4;
    k         AS k;
    c         AS c;
    critCnt   AS critCnt;
  }

  INST Proc05 FROM Process WITH
  {
    a         AS a;
    b         AS b;
    processNo AS pNo5;
    k         AS k;
    c	      AS c;
    critCnt   AS critCnt;
  }

  INST Proc06 FROM Process WITH
  {
    a         AS a;
    b         AS b;
    processNo AS pNo6;
    k         AS k;
    c         AS c;
    critCnt   AS critCnt;
  }

  INST Proc07 FROM Process WITH
  {
    a         AS a;
    b         AS b;
    processNo AS pNo7;
    k         AS k;
    c         AS c;
    critCnt   AS critCnt;
  }

  INST Proc08 FROM Process WITH
  {
    a         AS a;
    b         AS b;
    processNo AS pNo8;
    k         AS k;
    c         AS c;
    critCnt   AS critCnt;
  }

  INST Proc09 FROM Process WITH
  {
    a         AS a;
    b         AS b;
    processNo AS pNo9;
    k         AS k;
    c	      AS c;
    critCnt   AS critCnt;
  }

  INST Proc10 FROM Process WITH
  {
    a         AS a;
    b         AS b;
    processNo AS pNo10;
    k         AS k;
    c         AS c;
    critCnt   AS critCnt;
  }

  INST Proc11 FROM Process WITH
  {
    a         AS a;
    b         AS b;
    processNo AS pNo11;
    k         AS k;
    c         AS c;
    critCnt   AS critCnt;
  }

  INST Proc12 FROM Process WITH
  {
    a         AS a;
    b         AS b;
    processNo AS pNo12;
    k         AS k;
    c         AS c;
    critCnt   AS critCnt;
  }

  INST Proc13 FROM Process WITH
  {
    a         AS a;
    b         AS b;
    processNo AS pNo13;
    k         AS k;
    c	      AS c;
    critCnt   AS critCnt;
  }

  INST Proc14 FROM Process WITH
  {
    a         AS a;
    b         AS b;
    processNo AS pNo14;
    k         AS k;
    c         AS c;
    critCnt   AS critCnt;
  }

  INST Proc15 FROM Process WITH
  {
    a         AS a;
    b         AS b;
    processNo AS pNo15;
    k         AS k;
    c         AS c;
    critCnt   AS critCnt;
  }

  INST Proc16 FROM Process WITH
  {
    a         AS a;
    b         AS b;
    processNo AS pNo16;
    k         AS k;
    c         AS c;
    critCnt   AS critCnt;
  }
}

REACHABILITY CHECK System
USE BDD
{
  VAR
    initial, error : REGION;

  COMMANDS
    initial := INITIALREGION;

    error   := critCnt = 2;

    IF( ISREACHABLE FROM initial TO error FORWARD ) 
    {
      PRINT "Mutual exclusion violated";
    }
    ELSE 
    {
      PRINT "Mutual exclusion satisfied"; 
    } 
}

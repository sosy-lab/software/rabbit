/**
 * project: CSMA example generator.
 * module:  generator.cpp
 * @version <18.03.2002>
 * @author  <Michael Vogel - (mvogel@Informatik.TU-Cottbus.DE)>
 * 
 * Generates cta example containing the specified number of sender
 * instances.
 */

//#include <stdio.h>
//#include<fstream>
#include<string>
#include<sstream>
//#include<map>
using namespace std;


/*
 * The main method reads specified the filename specified, by cmdln args,
 * creates a new grammar starts the file parsing and modification of
 * the grammar currently read and output the result to the screen.
 *
 * @param argc number of command line arguments
 * @param argv array of command line arguments
 */

int
main(int argc, char* argv[])
{
  if ((argc != 2) || (atoi(argv[1]) < 1))
  {
    cout << "Cta-example generator for CSMA-CD Protocoll."
	 << endl << "2002, M. Vogel, 2002-03-18" << endl << endl;
    cout << "Usage: generator <#Sender>" << endl << endl << " where "
	 << "<#sender> specifies the number of senders contained in the"
	 << "  example." << endl << endl;
    cout << " The minimum number of senders is 1." << endl << endl;
  }
  else
  {
    int n = atoi(argv[1]);

    // Top description.
    cout << "/*" << endl
	 << " * File: CTA-model of Carrier Sense Multiple Access Collision "
	 << "Detect" << endl
	 << " *       Protocol (CSMA/CD)." << endl
	 << " * modules: Sender, Medium" << endl
	 << " * author:  Dirk Beyer, 2002" << endl
	 << " * last change: mvogel 2002-03-18" << endl
	 << " * " << endl
	 << " * Based on: \"A case study: the CSMA/CD protocol\", Sergio"
	 << " Yovine," << endl
	 << " *           VERIMAG-SPECTRE, November 18, 1994" << endl
	 << " */ " << endl << endl;
    
    // Sender Module.
    cout << "// This module represents the template for one sender." << endl
	 << "MODULE Sender" << endl
	 << "{" << endl
	 << "  INPUT" << endl
	 << "    propTime   : CONST; // time for signal propagation."
	 << " (paper: Sigma)" << endl
	 << "    transmTime : CONST; // time for data transmission."
	 << " (paper: Lambda)" << endl << endl
      
	 << "    collDetect : SYNC;  // Collision detection by medium." << endl
	 << endl
	 << "  MULTREST" << endl
	 << "    // Senders syncronisation events." << endl
	 << "    begin      : SYNC; // Begin of data transmitting" << endl
	 << "    end        : SYNC; // End of data transmitting" << endl
	 << "    busy       : SYNC; // Busy signaling by medium." << endl
	 << endl
	 << "  // Set initial state of automaton and clock values." << endl
	 << "  INITIAL STATE(Sender) = init AND x = transmTime + 1;" << endl
	 << endl
	 << "  AUTOMATON Sender" << endl
	 << "  {" << endl
	 << "    STATE init         { TRANS { SYNC ?collDetect;" << endl
	 << "                                 DO x' = x;" << endl
	 << "                                 GOTO init;" << endl
	 << "                               }" << endl
	 << "                         TRANS { DO x' = x;" << endl
	 << "                                 GOTO send;" << endl
	 << "                               }" << endl
	 << "                       }" << endl << endl
	 << "    STATE send         { INV FALSE;" << endl
	 << "                         TRANS { SYNC #begin;" << endl
	 << "                                 DO x' = 0;" << endl
	 << "                                 GOTO transmit;" << endl
	 << "                               }" << endl
	 << "                         TRANS { SYNC #busy;" << endl
	 << "                                 DO x' = 0;" << endl
	 << "                                 GOTO collDetected;" << endl
	 << "                               }" << endl
	 << "                         TRANS { SYNC ?collDetect;" << endl
	 << "                                 DO x' = 0;" << endl
	 << "                                 GOTO collDetected;" << endl
	 << "                               }" << endl
	 << "                       }" << endl << endl
	 << "    STATE collDetected { INV x <= propTime + propTime;" << endl
	 << "                         TRANS { SYNC ?collDetect;" << endl
	 << "                                 DO x' = x;" << endl
	 << "                                 GOTO collDetected;" << endl
	 << "                               }" << endl
	 << "                         TRANS { DO x' = transmTime + 1;" << endl
	 << "                                 GOTO send;" << endl
	 << "                               }" << endl
	 << "                       }" << endl << endl
	 << "    STATE transmit     { INV x <= transmTime;" << endl
	 << "                         TRANS { SYNC ?collDetect;" << endl
	 << "                                 DO x' = 0;" << endl
	 << "                                 GOTO collDetected;" << endl
	 << "                               }" << endl
	 << "                         TRANS { GUARD x = transmTime;" << endl
	 << "                                 SYNC #end;" << endl
	 << "                                 DO x' = transmTime + 1;" << endl
	 << "                                 GOTO init;" << endl
	 << "                               }" << endl
	 << "                       }" << endl
	 << "  }" << endl << endl
	 << "  LOCAL" << endl
	 << "    // A clock to measure the time in a state." << endl
	 << "    x:  CLOCK(5);  // transmTime + 1" << endl
	 << "}" << endl << endl;

    // System Module.
    cout << "// Module of the whole system." << endl
	 << "MODULE System" << endl
	 << "{" << endl
	 << "  LOCAL" << endl
	 << "    // Constants for time bounds." << endl
	 << "    propTime = 1   : CONST; // time for signal propagation."
	 << " (paper: Sigma)" << endl
	 << "    transmTime = 4 : CONST; // time for data transmission."
	 << " (paper: Lambda)" << endl << endl
	 << "    collDetect     : SYNC;  // Collision detect signal."
	 << " (Same signal for all)" << endl << endl
	 << "    // Begin signals for every Sender." << endl;

    // Begin sync signals.
    for(int i = 1; i <= n; ++i)
    {
      cout << "    begin" << i <<"        : SYNC;" << endl;
    } // for

    cout << endl << "    // End signals fro every Sender." << endl;

    // End sync signals.
    for(int i = 1; i <= n; ++i)
    {
      cout << "    end" << i << "          : SYNC;" << endl;
    }

    cout << endl << "    // Busy signals for every Sender." << endl;
    
    // Busy sync signals.
    for(int i = 1; i <= n; ++i)
    {
      cout << "    busy" << i << "         : SYNC;" << endl;
    }

    // Medium automaton.
    cout << endl
	 << "  INITIAL STATE(Medium)= init AND x = propTime + 1;" << endl
	 << endl
	 << "  AUTOMATON Medium" << endl
	 << "  {" << endl
	 << "    STATE init         { INV FALSE;" << endl;

    for(int i = 1; i <= n; ++i)
    {
      cout << "                         TRANS { SYNC #begin" << i << ";"
	   << endl
	   << "                                 DO x' = 0;" << endl
	   << "                                 GOTO transmit;" << endl
	   << "                               }" << endl;
    }

    cout << "                       }" << endl << endl
	 << "    STATE transmit     {" << endl;

    for(int i = 1; i <= n; ++i)
    {
      cout << "                         TRANS { GUARD x >= propTime;" << endl
	   << "                                 SYNC #busy" << i << ";" << endl
	   << "                                 DO x' = propTime + 1;" << endl
	   << "                                 GOTO transmit;" << endl
	   << "                               }" << endl;
    }

    cout << endl;

    for(int i = 1; i <= n; ++i)
    {
      cout << "                         TRANS { SYNC #end" << i << ";" << endl
	   << "                                 DO x' = propTime + 1;" << endl
	   << "                                 GOTO init;" << endl
	   << "                               }" << endl;
    }
    
    cout << endl;

    for(int i = 1; i <= n; ++i)
    {
      cout << "                         TRANS { GUARD x + 1 <= propTime;"
	   << endl
	   << "                                 SYNC #begin" << i << ";"
	   << endl
	   << "                                 DO x' = 0;" << endl
	   << "                                 GOTO collDetected;" << endl
	   << "                               }" << endl;
    }

    cout << "                       }" << endl << endl
	 << "    STATE collDetected { INV x <= propTime;" << endl
	 << "                         TRANS { SYNC !collDetect;" << endl
	 << "                                 DO x' = propTime + 1;" << endl
	 << "                                 GOTO init;" << endl
	 << "                               }" << endl
	 << "                       }" << endl
	 << "  }" << endl << endl
	 << "  LOCAL" << endl
	 << "    x:         CLOCK(2); // propTime + 1" << endl << endl;

    cout << "  // Instances of Sender template." << endl;

    // A Sender instance.
    for(int i = 1; i <= n; ++i)
    {
      cout   << "  INST iSender" << i << " FROM Sender WITH" << endl
	     << "  {" << endl
	     << "    propTime   AS propTime;" << endl
	     << "    transmTime AS transmTime;" << endl
	     << "    collDetect AS collDetect;" << endl
	     << "    begin      AS begin" << i << ";" << endl
	     << "    end        AS end" << i << ";" << endl
	     << "    busy       AS busy" << i << ";" << endl
	     << "  }" << endl << endl;
    }

      cout << "}" << endl << endl
	   << "// Analysis section:" << endl
	   << "// The module name which should be checked and the type"
	   << " of the internal" << endl
	   << "// representation for the region of reachable configurations"
	   << " is specified." << endl
	   << "REACHABILITY CHECK System" << endl
	   << "USE BDD" << endl
	   << "{" << endl
	   << "  // Declaration of region variables." << endl
	   << "  VAR" << endl
	   << "    reached, error : REGION;" << endl << endl
	   << "  COMMANDS" << endl << endl
	   << "    // Define an error region. If this region is reachable"
	   << " from the initial" << endl
	   << "    // region the ... is violated" << endl
	   << "    // This region (situation) should not be reachable"
	   << " (possible)." << endl
	   << "    //error := " << endl
	   << "    /*    " << endl
	   << "    // The ISREACHABLE command executes an 'on the fly-'"
	   << " reachability check" << endl
	   << "    // to verify if the specified region 'error' is reachable"
	   << " or not." << endl
	   << "    IF( ISREACHABLE FROM INITIALREGION TO FALSE FORWARD )"
	   << endl
	   << "    {" << endl
	   << "      PRINT \"Problems in On-the-fly analysis.\";" << endl
	   << "    }" << endl
	   << "    ELSE" << endl
	   << "    {" << endl
	   << "      PRINT \"On-the-fly analysis done.\"; " << endl
	   << "    }" << endl
	   << "    */" << endl
	   << "    // The 'REACH FROM ...' command calculates the full"
	   << " region of all" << endl
	   << "    // reachable configurations whereas the ISREACHABLE"
	   << " command (see above)" << endl
	   << "    // only executes an 'on the fly-' reachability check."
	   << endl
	   <<"    // The intersection of the region of all reachable"
	   << " configurations and" << endl
	   << "    // the error region (as defined above) should be empty"
	   << " to satisfy the" << endl
	   << "    // mutual exclusion." << endl
	   << "       reached := REACH FROM INITIALREGION FORWARD;" << endl
	   << "  /*     IF ( EMPTY(reached INTERSECT error) )" << endl
	   << "       {" << endl
	   << "         PRINT \"Mutual exclusion satisfied.\";" << endl
	   << "       }" << endl
	   << "       ELSE" << endl
	   << "       {" << endl
	   << "         PRINT \"Mutual exclusion violated.\";" << endl
	   << "       }" << endl
	   << "  */" << endl << endl
	   << "    // The PRINTBDDESTIM command prints the estimated number"
	   << " of" << endl
	   << "    // BDD nodes for each bit in the BDD to stdout." << endl
	   << "    //PRINTBDDESTIM;" << endl << endl

	   << "    // The PRINTBDDGRAPH command prints the real number of"
	   << endl
	   << "    // BDD nodes for each bit in the specified region-BDD"
	   << " to stdout." << endl
	   << "    //PRINTBDDGRAPH INITIALREGION;" << endl
	   << "}" << endl;
  }
  
  return 0;
}; // main

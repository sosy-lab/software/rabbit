/* 
 * File: CTA-model of Token Ring FDDI Protocol.
 * modules: Station, System
 * author:  Dirk Beyer, 2002
 * last change: mvogel 2002-03-12
 * 
 */ 

// This module represents the template for one station.
MODULE Station
{
  INPUT
    // sa:   number of time units for high-speed communication.
    // trtt: number of time units for asynchronous token passing.
    sa:          CONST;
    trtt:        CONST;

    // Signal for take token.
    tt:          SYNC;

  OUTPUT
    // Signal for token release.
    rt:          SYNC;

  // Set initial state of automaton and variable values.
  INITIAL STATE(Station) = z_idle AND y = trtt + 1 AND z = trtt + 1;

  AUTOMATON Station
  {
    STATE z_idle  { TRANS { SYNC ?tt;  
                            DO y' = 0;  
			    GOTO z_sync;
			  }
		  }

    STATE z_sync  { INV y <= sa;
                    TRANS { GUARD y >= sa AND z + 1 <= trtt;
                            //DO y' = trtt + 1;
                            GOTO z_async;
                          }
                    TRANS { GUARD y >= sa AND z >= trtt;
                            SYNC !rt;
                            DO z' = trtt + 1;
                            GOTO y_idle;
                          }
                  }

    STATE z_async { INV z <= trtt;
                    TRANS { SYNC !rt;
                            DO z' = trtt + 1;
                            GOTO y_idle;
                          }
                  }

    STATE y_idle  { TRANS { SYNC ?tt;  
                            DO z' = 0;  
			    GOTO y_sync;
			  }
		  }

    STATE y_sync  { INV z <= sa;
                    TRANS { GUARD z >= sa AND y + 1 <= trtt;
		            //DO z' = trtt + 1;
                            GOTO y_async;
                          }
                    TRANS { GUARD z >= sa AND y >= trtt;
                            SYNC !rt;
                            DO y' = trtt + 1;
                            GOTO z_idle;
                          }
                  }

    STATE y_async { INV y <= trtt;
                    TRANS { SYNC !rt;
                            DO y' = trtt + 1; 
                            GOTO z_idle;
                          }
                  }
  }

  // Clocks to measure the time since last take token.
  LOCAL
    y:           CLOCK(66); // trtt + 1
    z:           CLOCK(66); // trtt + 1
}

// Module of the whole system.
MODULE System
{
  LOCAL
    // Constants for time bounds.
    //td = 0:    CONST; // number of time units for the token ring delay.
    sa = 1:    CONST; // number of time units for high-speed communication.
    trtt = 65: CONST; // token round trip time .. 2 * #processes + 1

    // Take token signals.
    tt01:        SYNC;
    tt02:        SYNC;
    tt03:        SYNC;
    tt04:        SYNC;
    tt05:        SYNC;
    tt06:        SYNC;
    tt07:        SYNC;
    tt08:        SYNC;
    tt09:        SYNC;
    tt10:        SYNC;
    tt11:        SYNC;
    tt12:        SYNC;
    tt13:        SYNC;
    tt14:        SYNC;
    tt15:        SYNC;
    tt16:        SYNC;
    tt17:        SYNC;
    tt18:        SYNC;
    tt19:        SYNC;
    tt20:        SYNC;
    tt21:        SYNC;
    tt22:        SYNC;
    tt23:        SYNC;
    tt24:        SYNC;
    tt25:        SYNC;
    tt26:        SYNC;
    tt27:        SYNC;
    tt28:        SYNC;
    tt29:        SYNC;
    tt30:        SYNC;
    tt31:        SYNC;
    tt32:        SYNC;

    // Release token signals.
    rt01:        SYNC;
    rt02:        SYNC;
    rt03:        SYNC;
    rt04:        SYNC;
    rt05:        SYNC;
    rt06:        SYNC;
    rt07:        SYNC;
    rt08:        SYNC;
    rt09:        SYNC;
    rt10:        SYNC;
    rt11:        SYNC;
    rt12:        SYNC;
    rt13:        SYNC;
    rt14:        SYNC;
    rt15:        SYNC;
    rt16:        SYNC;
    rt17:        SYNC;
    rt18:        SYNC;
    rt19:        SYNC;
    rt20:        SYNC;
    rt21:        SYNC;
    rt22:        SYNC;
    rt23:        SYNC;
    rt24:        SYNC;
    rt25:        SYNC;
    rt26:        SYNC;
    rt27:        SYNC;
    rt28:        SYNC;
    rt29:        SYNC;
    rt30:        SYNC;
    rt31:        SYNC;
    rt32:        SYNC;

  // Instances of process template.
  INST iStation01 FROM Station WITH
  {
    sa        AS sa;
    trtt      AS trtt;
    tt        AS tt01;
    rt        AS rt01;
  }

  INST iStation02 FROM Station WITH
  {
    sa        AS sa;
    trtt      AS trtt;
    tt        AS tt02;
    rt        AS rt02;
  }

  INST iStation03 FROM Station WITH
  {
    sa        AS sa;
    trtt      AS trtt;
    tt        AS tt03;
    rt        AS rt03;
  }

  //LOCAL
  //  x:         CLOCK(1); // Clock for token delay.

  INITIAL STATE(Ring)= ring_to_01;// AND x = 1;
 
  AUTOMATON Ring
  {
    STATE ring_to_01 { INV FALSE;
                       TRANS { SYNC !tt01;
                               // DO x' = 1;  
			       GOTO ring_01;
			     }
		     }

    STATE ring_01    { TRANS { SYNC ?rt01;  
			       // DO x' = 0;
                               GOTO ring_to_02;
			     }
		     }

    STATE ring_to_02 { INV FALSE;
                       TRANS { SYNC !tt02; // DO x' = 1;   
			       GOTO ring_02;
			     }
		     }

    STATE ring_02    { TRANS { SYNC ?rt02;  
			       // DO x' = 0;
                               GOTO ring_to_03;
			     }
		     }
 
    STATE ring_to_03 { INV FALSE;
                       TRANS { SYNC !tt03; // DO x' = 1;   
			       GOTO ring_03;
			     }
		     }

    STATE ring_03    { TRANS { SYNC ?rt03;  
			       // DO x' = 0;
                               GOTO ring_to_04;
			     }
		     }

    STATE ring_to_04 { INV FALSE;
                       TRANS { SYNC !tt04; // DO x' = 1;   
			       GOTO ring_04;
			     }
		     }

    STATE ring_04    { TRANS { SYNC ?rt04;  
			       // DO x' = 0;
                               GOTO ring_to_05;
			     }
		     }
   
    STATE ring_to_05 { INV FALSE;
                       TRANS { SYNC !tt05;  // DO x' = 1;  
			       GOTO ring_05;
			     }
		     }

    STATE ring_05    { TRANS { SYNC ?rt05;  
			       // DO x' = 0;
                               GOTO ring_to_06;
			     }
		     }

    STATE ring_to_06 { INV FALSE;
                       TRANS { SYNC !tt06;// DO x' = 1;    
			       GOTO ring_06;
			     }
		     }

    STATE ring_06    { TRANS { SYNC ?rt06;  
			       // DO x' = 0;
                               GOTO ring_to_07;
			     }
		     }
 
    STATE ring_to_07 { INV FALSE;
                       TRANS { SYNC !tt07;  // DO x' = 1;  
			       GOTO ring_07;
			     }
		     }

    STATE ring_07    { TRANS { SYNC ?rt07;  
			       // DO x' = 0;
                               GOTO ring_to_08;
			     }
		     }

    STATE ring_to_08 { INV FALSE;
                       TRANS { SYNC !tt08;  // DO x' = 1;  
			       GOTO ring_08;
			     }
		     }

    STATE ring_08    { TRANS { SYNC ?rt08;  
			       // DO x' = 0;
                               GOTO ring_to_09;
			     }
		     }

    STATE ring_to_09 { INV FALSE;
                       TRANS { SYNC !tt09;  // DO x' = 1;  
			       GOTO ring_09;
			     }
		     }

    STATE ring_09    { TRANS { SYNC ?rt09;  
			       // DO x' = 0;
                               GOTO ring_to_10;
			     }
		     }

    STATE ring_to_10 { INV FALSE;
                       TRANS { SYNC !tt10;  // DO x' = 1;  
			       GOTO ring_10;
			     }
		     }

    STATE ring_10    { TRANS { SYNC ?rt10;  
			       // DO x' = 0;
                               GOTO ring_to_11;
			     }
		     }
 
    STATE ring_to_11 { INV FALSE;
                       TRANS { SYNC !tt11; // DO x' = 1;   
			       GOTO ring_11;
			     }
		     }

    STATE ring_11    { TRANS { SYNC ?rt11;  
			       // DO x' = 0;
                               GOTO ring_to_12;
			     }
		     }

    STATE ring_to_12 { INV FALSE;
                       TRANS { SYNC !tt12; // DO x' = 1;   
			       GOTO ring_12;
			     }
		     }

    STATE ring_12    { TRANS { SYNC ?rt12;  
			       // DO x' = 0;
                               GOTO ring_to_13;
			     }
		     }
   
    STATE ring_to_13 { INV FALSE;
                       TRANS { SYNC !tt13;  // DO x' = 1;  
			       GOTO ring_13;
			     }
		     }

    STATE ring_13    { TRANS { SYNC ?rt13;  
			       // DO x' = 0;
                               GOTO ring_to_14;
			     }
		     }

    STATE ring_to_14 { INV FALSE;
                       TRANS { SYNC !tt14;  // DO x' = 1;  
			       GOTO ring_14;
			     }
		     }

    STATE ring_14    { TRANS { SYNC ?rt14;  
			       // DO x' = 0;
                               GOTO ring_to_15;
			     }
		     }
 
    STATE ring_to_15 { INV FALSE;
                       TRANS { SYNC !tt15;  // DO x' = 1;  
			       GOTO ring_15;
			     }
		     }

    STATE ring_15    { TRANS { SYNC ?rt15;  
			       // DO x' = 0;
                               GOTO ring_to_16;
			     }
		     }

    STATE ring_to_16 { INV FALSE;
                       TRANS { SYNC !tt16;  // DO x' = 1;  
			       GOTO ring_16;
			     }
		     }

    STATE ring_16    { TRANS { SYNC ?rt16;  
			       // DO x' = 0;
                               GOTO ring_to_17;
			     }
		     }

    STATE ring_to_17 { INV FALSE;
                       TRANS { SYNC !tt17;
                               // DO x' = 1;  
			       GOTO ring_17;
			     }
		     }

    STATE ring_17    { TRANS { SYNC ?rt17;  
			       // DO x' = 0;
                               GOTO ring_to_18;
			     }
		     }

    STATE ring_to_18 { INV FALSE;
                       TRANS { SYNC !tt18; // DO x' = 1;   
			       GOTO ring_18;
			     }
		     }

    STATE ring_18    { TRANS { SYNC ?rt18;  
			       // DO x' = 0;
                               GOTO ring_to_19;
			     }
		     }
 
    STATE ring_to_19 { INV FALSE;
                       TRANS { SYNC !tt19; // DO x' = 1;   
			       GOTO ring_19;
			     }
		     }

    STATE ring_19    { TRANS { SYNC ?rt19;  
			       // DO x' = 0;
                               GOTO ring_to_20;
			     }
		     }

    STATE ring_to_20 { INV FALSE;
                       TRANS { SYNC !tt20; // DO x' = 1;   
			       GOTO ring_20;
			     }
		     }

    STATE ring_20    { TRANS { SYNC ?rt20;  
			       // DO x' = 0;
                               GOTO ring_to_21;
			     }
		     }
   
    STATE ring_to_21 { INV FALSE;
                       TRANS { SYNC !tt21;  // DO x' = 1;  
			       GOTO ring_21;
			     }
		     }

    STATE ring_21    { TRANS { SYNC ?rt21;  
			       // DO x' = 0;
                               GOTO ring_to_22;
			     }
		     }

    STATE ring_to_22 { INV FALSE;
                       TRANS { SYNC !tt22;// DO x' = 1;    
			       GOTO ring_22;
			     }
		     }

    STATE ring_22    { TRANS { SYNC ?rt22;  
			       // DO x' = 0;
                               GOTO ring_to_23;
			     }
		     }
 
    STATE ring_to_23 { INV FALSE;
                       TRANS { SYNC !tt23;  // DO x' = 1;  
			       GOTO ring_23;
			     }
		     }

    STATE ring_23    { TRANS { SYNC ?rt23;  
			       // DO x' = 0;
                               GOTO ring_to_24;
			     }
		     }

    STATE ring_to_24 { INV FALSE;
                       TRANS { SYNC !tt24;  // DO x' = 1;  
			       GOTO ring_24;
			     }
		     }

    STATE ring_24    { TRANS { SYNC ?rt24;  
			       // DO x' = 0;
                               GOTO ring_to_25;
			     }
		     }

    STATE ring_to_25 { INV FALSE;
                       TRANS { SYNC !tt25;  // DO x' = 1;  
			       GOTO ring_25;
			     }
		     }

    STATE ring_25    { TRANS { SYNC ?rt25;  
			       // DO x' = 0;
                               GOTO ring_to_26;
			     }
		     }

    STATE ring_to_26 { INV FALSE;
                       TRANS { SYNC !tt26;  // DO x' = 1;  
			       GOTO ring_26;
			     }
		     }

    STATE ring_26    { TRANS { SYNC ?rt26;  
			       // DO x' = 0;
                               GOTO ring_to_27;
			     }
		     }
 
    STATE ring_to_27 { INV FALSE;
                       TRANS { SYNC !tt27; // DO x' = 1;   
			       GOTO ring_27;
			     }
		     }

    STATE ring_27    { TRANS { SYNC ?rt27;  
			       // DO x' = 0;
                               GOTO ring_to_28;
			     }
		     }

    STATE ring_to_28 { INV FALSE;
                       TRANS { SYNC !tt28; // DO x' = 1;   
			       GOTO ring_28;
			     }
		     }

    STATE ring_28    { TRANS { SYNC ?rt28;  
			       // DO x' = 0;
                               GOTO ring_to_29;
			     }
		     }
   
    STATE ring_to_29 { INV FALSE;
                       TRANS { SYNC !tt29;  // DO x' = 1;  
			       GOTO ring_29;
			     }
		     }

    STATE ring_29    { TRANS { SYNC ?rt29;  
			       // DO x' = 0;
                               GOTO ring_to_30;
			     }
		     }

    STATE ring_to_30 { INV FALSE;
                       TRANS { SYNC !tt30;  // DO x' = 1;  
			       GOTO ring_30;
			     }
		     }

    STATE ring_30    { TRANS { SYNC ?rt30;  
			       // DO x' = 0;
                               GOTO ring_to_31;
			     }
		     }
 
    STATE ring_to_31 { INV FALSE;
                       TRANS { SYNC !tt31;  // DO x' = 1;  
			       GOTO ring_31;
			     }
		     }

    STATE ring_31    { TRANS { SYNC ?rt31;  
			       // DO x' = 0;
                               GOTO ring_to_32;
			     }
		     }

    STATE ring_to_32 { INV FALSE;
                       TRANS { SYNC !tt32;  // DO x' = 1;  
			       GOTO ring_32;
			     }
		     }

    STATE ring_32    { TRANS { SYNC ?rt32;  
			       // DO x' = 0;
                               GOTO ring_to_01;
			     }
		     }
  }

  INST iStation04 FROM Station WITH
  {
    sa        AS sa;
    trtt      AS trtt;
    tt        AS tt04;
    rt        AS rt04;
  }

  INST iStation05 FROM Station WITH
  {
    sa        AS sa;
    trtt      AS trtt;
    tt        AS tt05;
    rt        AS rt05;
  }

  INST iStation06 FROM Station WITH
  {
    sa        AS sa;
    trtt      AS trtt;
    tt        AS tt06;
    rt        AS rt06;
  }

  INST iStation07 FROM Station WITH
  {
    sa        AS sa;
    trtt      AS trtt;
    tt        AS tt07;
    rt        AS rt07;
  }

  INST iStation08 FROM Station WITH
  {
    sa        AS sa;
    trtt      AS trtt;
    tt        AS tt08;
    rt        AS rt08;
  }

  INST iStation09 FROM Station WITH
  {
    sa        AS sa;
    trtt      AS trtt;
    tt        AS tt09;
    rt        AS rt09;
  }

  INST iStation10 FROM Station WITH
  {
    sa        AS sa;
    trtt      AS trtt;
    tt        AS tt10;
    rt        AS rt10;
  }

  INST iStation11 FROM Station WITH
  {
    sa        AS sa;
    trtt      AS trtt;
    tt        AS tt11;
    rt        AS rt11;
  }

  INST iStation12 FROM Station WITH
  {
    sa        AS sa;
    trtt      AS trtt;
    tt        AS tt12;
    rt        AS rt12;
  }

  INST iStation13 FROM Station WITH
  {
    sa        AS sa;
    trtt      AS trtt;
    tt        AS tt13;
    rt        AS rt13;
  }

  INST iStation14 FROM Station WITH
  {
    sa        AS sa;
    trtt      AS trtt;
    tt        AS tt14;
    rt        AS rt14;
  }

  INST iStation15 FROM Station WITH
  {
    sa        AS sa;
    trtt      AS trtt;
    tt        AS tt15;
    rt        AS rt15;
  }

  INST iStation16 FROM Station WITH
  {
    sa        AS sa;
    trtt      AS trtt;
    tt        AS tt16;
    rt        AS rt16;
  }

  INST iStation17 FROM Station WITH
  {
    sa        AS sa;
    trtt      AS trtt;
    tt        AS tt17;
    rt        AS rt17;
  }

  INST iStation18 FROM Station WITH
  {
    sa        AS sa;
    trtt      AS trtt;
    tt        AS tt18;
    rt        AS rt18;
  }

  INST iStation19 FROM Station WITH
  {
    sa        AS sa;
    trtt      AS trtt;
    tt        AS tt19;
    rt        AS rt19;
  }

  INST iStation20 FROM Station WITH
  {
    sa        AS sa;
    trtt      AS trtt;
    tt        AS tt20;
    rt        AS rt20;
  }

  INST iStation21 FROM Station WITH
  {
    sa        AS sa;
    trtt      AS trtt;
    tt        AS tt21;
    rt        AS rt21;
  }

  INST iStation22 FROM Station WITH
  {
    sa        AS sa;
    trtt      AS trtt;
    tt        AS tt22;
    rt        AS rt22;
  }

  INST iStation23 FROM Station WITH
  {
    sa        AS sa;
    trtt      AS trtt;
    tt        AS tt23;
    rt        AS rt23;
  }

  INST iStation24 FROM Station WITH
  {
    sa        AS sa;
    trtt      AS trtt;
    tt        AS tt24;
    rt        AS rt24;
  }

  INST iStation25 FROM Station WITH
  {
    sa        AS sa;
    trtt      AS trtt;
    tt        AS tt25;
    rt        AS rt25;
  }

  INST iStation26 FROM Station WITH
  {
    sa        AS sa;
    trtt      AS trtt;
    tt        AS tt26;
    rt        AS rt26;
  }

  INST iStation27 FROM Station WITH
  {
    sa        AS sa;
    trtt      AS trtt;
    tt        AS tt27;
    rt        AS rt27;
  }

  INST iStation28 FROM Station WITH
  {
    sa        AS sa;
    trtt      AS trtt;
    tt        AS tt28;
    rt        AS rt28;
  }

  INST iStation29 FROM Station WITH
  {
    sa        AS sa;
    trtt      AS trtt;
    tt        AS tt29;
    rt        AS rt29;
  }

  INST iStation30 FROM Station WITH
  {
    sa        AS sa;
    trtt      AS trtt;
    tt        AS tt30;
    rt        AS rt30;
  }

  INST iStation31 FROM Station WITH
  {
    sa        AS sa;
    trtt      AS trtt;
    tt        AS tt31;
    rt        AS rt31;
  }

  INST iStation32 FROM Station WITH
  {
    sa        AS sa;
    trtt      AS trtt;
    tt        AS tt32;
    rt        AS rt32;
  }
}

// Analysis section:
// The module name which should be checked and the type of the internal
// representation for the region of reachable configurations is specified.
REACHABILITY CHECK System
USE BDD
{ 
  // Declaration of region variables. 
  VAR
    reached, error : REGION;

  COMMANDS

    // Define an error region. If this region is reachable from the initial
    // region the ... is violated
    // This region (situation) should not be reachable (possible).
    //error := 
    /*    
    // The ISREACHABLE command executes an 'on the fly-' reachability check
    // to verify if the specified region 'error' is reachable or not.
    IF( ISREACHABLE FROM INITIALREGION TO FALSE FORWARD ) 
    {
      PRINT "Problems in On-the-fly analysis.";
    }
    ELSE 
    {
      PRINT "On-the-fly analysis done."; 
    } 
    */
    // The 'REACH FROM ...' command calculates the full region of all
    // reachable configurations whereas the ISREACHABLE command (see above)
    // only executes an 'on the fly-' reachability check.
    // The intersection of the region of all reachable configurations and
    // the error region (as defined above) should be empty to satisfy the
    // mutual exclusion.
       reached := REACH FROM INITIALREGION FORWARD;
  /* IF ( EMPTY(reached INTERSECT error) )
       {
         PRINT "Mutual exclusion satisfied."; 
       }
       ELSE
       {
         PRINT "Mutual exclusion violated."; 
       }
  */

    // The PRINTBDDESTIM command prints the estimated number of
    // BDD nodes for each bit in the BDD to stdout.
    //PRINTBDDESTIM;

    // The PRINTBDDGRAPH command prints the real number of
    // BDD nodes for each bit in the specified region-BDD to stdout.
    //PRINTBDDGRAPH INITIALREGION;
}

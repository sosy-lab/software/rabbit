-- HyTech-code generated from CTA-Module "System".

define(a, 3)
define(b, 3)
define(pNo1, 1)
define(pNo2, 2)
define(pNo3, 3)
define(pNo4, 4)

var
  Process1_x: stopwatch;
  Process2_x: stopwatch;
  Process3_x: stopwatch;
  Process4_x: stopwatch;
  k: discrete;

automaton Process1_Fischer
  synclabs:
;

  initially 
-- Attention: Maybe there are some initial variable restrictions.
Process1_start;

  loc Process1_assign:
    while
      Process1_x <= a
    wait
    {
-- This was a true (ctaRestTrue)!

    }
    when
True      do
      {
Process1_x' = 0 , k' = pNo1
      }
      goto Process1_wait;

  loc Process1_critical:
    while
      True
    wait
    {
-- This was a true (ctaRestTrue)!

    }
    when
True      do
      {
k' = 0
      }
      goto Process1_uncritical;

  loc Process1_start:
    while
      False
    wait
    {
-- This was a true (ctaRestTrue)!

    }
    when
True      do
      {
k' = 0
      }
      goto Process1_uncritical;

  loc Process1_uncritical:
    while
      True
    wait
    {
-- This was a true (ctaRestTrue)!

    }
    when
k = 0      do
      {
Process1_x' = 0
      }
      goto Process1_assign;

  loc Process1_wait:
    while
      True
    wait
    {
-- This was a true (ctaRestTrue)!

    }
    when Process1_x >= b & k > pNo1 do {} goto Process1_uncritical;
    when Process1_x >= b & k < pNo1 do {} goto Process1_uncritical;
    when
Process1_x > b & k = pNo1      do
      {
-- This was a True! 

      }
      goto Process1_critical;

end

automaton Process2_Fischer
  synclabs:
;

  initially 
-- Attention: Maybe there are some initial variable restrictions.
Process2_start;

  loc Process2_assign:
    while
      Process2_x <= a
    wait
    {
-- This was a true (ctaRestTrue)!

    }
    when
True      do
      {
Process2_x' = 0 , k' = pNo2
      }
      goto Process2_wait;

  loc Process2_critical:
    while
      True
    wait
    {
-- This was a true (ctaRestTrue)!

    }
    when
True      do
      {
k' = 0
      }
      goto Process2_uncritical;

  loc Process2_start:
    while
      False
    wait
    {
-- This was a true (ctaRestTrue)!

    }
    when
True      do
      {
k' = 0
      }
      goto Process2_uncritical;

  loc Process2_uncritical:
    while
      True
    wait
    {
-- This was a true (ctaRestTrue)!

    }
    when
k = 0      do
      {
Process2_x' = 0
      }
      goto Process2_assign;

  loc Process2_wait:
    while
      True
    wait
    {
-- This was a true (ctaRestTrue)!

    }
    when Process2_x >= b & k > pNo2 do {} goto Process2_uncritical;
    when Process2_x >= b & k < pNo2 do {} goto Process2_uncritical;

    when
Process2_x > b & k = pNo2      do
      {
-- This was a True! 

      }
      goto Process2_critical;

end

var 
   init_reg, final_reg, reached : region;

init_reg:= loc[Process1_Fischer] = Process1_start 
        & loc[Process2_Fischer] = Process2_start
	& k=0;

final_reg:= (loc[Process1_Fischer] = Process1_critical 
	& loc[Process2_Fischer] = Process2_critical);

reached:= reach backward from final_reg endreach;

prints "Condition for faulty system";

print omit all locations hide non_parameters in reached & init_reg endhide;

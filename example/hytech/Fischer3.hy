-- HyTech-code generated from CTA-Module "System".

define(a, 3)
define(b, 3)
define(pNo1, 1)
define(pNo2, 2)
define(pNo3, 3)

var
  Process1_x: stopwatch;
  Process2_x: stopwatch;
  Process3_x: stopwatch;
  k: discrete;

automaton Process1_Fischer
  synclabs:
;

  initially 
-- Attention: Maybe there are some initial variable restrictions.
Process1_start;

  loc Process1_assign:
    while
      Process1_x <= a
    wait
    {
-- This was a true (ctaRestTrue)!

    }
    when
True      
	do
      {
Process1_x' = 0 , k' = pNo1
      }
      goto Process1_wait;

  loc Process1_critical:
    while
      True
    wait
    {
-- This was a true (ctaRestTrue)!

    }
    when
True      do
      {
k' = 0
      }
      goto Process1_uncritical;

  loc Process1_start:
    while
      False
    wait
    {
-- This was a true (ctaRestTrue)!

    }
    when
True      do
      {
k' = 0
      }
      goto Process1_uncritical;

  loc Process1_uncritical:
    while
      True
    wait
    {
-- This was a true (ctaRestTrue)!

    }
    when
k = 0      do
      {
Process1_x' = 0
      }
      goto Process1_assign;

  loc Process1_wait:
    while
      True
    wait
    {
-- This was a true (ctaRestTrue)!

    }
    when
Process1_x >= b & 
-- HyTech can't handle '<>' operation, replace manually.
k < pNo1      do
      {
-- This was a True! 

      }
      goto Process1_uncritical;

    when
Process1_x > b & k = pNo1      do
      {
-- This was a True! 

      }
      goto Process1_critical;
    when
Process1_x >= b & 
-- HyTech can't handle '<>' operation, replace manually.
k > pNo1       do
      {
-- This was a True! 

      }
      goto Process1_uncritical;

    when
Process1_x > b & k = pNo1      do
      {
-- This was a True! 

      }
      goto Process1_critical;

end

automaton Process2_Fischer
  synclabs:
;

  initially 
-- Attention: Maybe there are some initial variable restrictions.
Process2_start;

  loc Process2_assign:
    while
      Process2_x <= a
    wait
    {
-- This was a true (ctaRestTrue)!

    }
    when
True      do
      {
Process2_x' = 0 , k' = pNo2
      }
      goto Process2_wait;

  loc Process2_critical:
    while
      True
    wait
    {
-- This was a true (ctaRestTrue)!

    }
    when
True      do
      {
k' = 0
      }
      goto Process2_uncritical;

  loc Process2_start:
    while
      False
    wait
    {
-- This was a true (ctaRestTrue)!

    }
    when
True      do
      {
k' = 0
      }
      goto Process2_uncritical;

  loc Process2_uncritical:
    while
      True
    wait
    {
-- This was a true (ctaRestTrue)!

    }
    when
k = 0      do
      {
Process2_x' = 0
      }
      goto Process2_assign;

  loc Process2_wait:
    while
      True
    wait
    {
-- This was a true (ctaRestTrue)!

    }
    when
Process2_x >= b & 
-- HyTech can't handle '<>' operation, replace manually.
k > pNo2   do
      {
-- This was a True! 

      }
      goto Process2_uncritical;
    when
Process2_x >= b & 
-- HyTech can't handle '<>' operation, replace manually.
k < pNo2      do
      {
-- This was a True! 

      }
      goto Process2_uncritical;

    when
Process2_x > b & k = pNo2      do
      {
-- This was a True! 

      }
      goto Process2_critical;

end







automaton Process3_Fischer
  synclabs:
;

  initially 
-- Attention: Maybe there are some initial variable restrictions.
Process3_start;

  loc Process3_assign:
    while
      Process3_x <= a
    wait
    {
-- This was a true (ctaRestTrue)!

    }
    when
True      do
      {
Process3_x' = 0 , k' = pNo3
      }
      goto Process3_wait;

  loc Process3_critical:
    while
      True
    wait
    {
-- This was a true (ctaRestTrue)!

    }
    when
True      do
      {
k' = 0
      }
      goto Process3_uncritical;

  loc Process3_start:
    while
      False
    wait
    {
-- This was a true (ctaRestTrue)!

    }
    when
True      do
      {
k' = 0
      }
      goto Process3_uncritical;

  loc Process3_uncritical:
    while
      True
    wait
    {
-- This was a true (ctaRestTrue)!

    }
    when
k = 0      do
      {
Process3_x' = 0
      }
      goto Process3_assign;

  loc Process3_wait:
    while
      True
    wait
    {
-- This was a true (ctaRestTrue)!

    }
    when
Process3_x >= b & 
-- HyTech can't handle '<>' operation, replace manually.
k < pNo3      do
      {
-- This was a True! 

      }
      goto Process3_uncritical;
    when
Process3_x >= b & 
-- HyTech can't handle '<>' operation, replace manually.
k > pNo3      do
      {
-- This was a True! 

      }
      goto Process3_uncritical;

    when
Process3_x > b & k = pNo3      do
      {
-- This was a True! 

      }
      goto Process3_critical;

end
-- 

var 
   init_reg, final_reg, reached : region;

init_reg:= loc[Process1_Fischer] = Process1_start 
        & loc[Process2_Fischer] = Process2_start
	& loc[Process3_Fischer] = Process3_start
	& k=0;

final_reg:= (loc[Process1_Fischer] = Process1_critical 
	& loc[Process2_Fischer] = Process2_critical) 
	|(loc[Process1_Fischer] = Process1_critical 
	& loc[Process3_Fischer] = Process3_critical) 
	|(loc[Process2_Fischer] = Process2_critical
	& loc[Process3_Fischer] = Process3_critical);

reached:= reach backward from final_reg endreach;

prints "Condition for faulty system";

print omit all locations hide non_parameters in reached & init_reg endhide;

-- ep: 1.18u 0.14s 0:02.45 53.8%
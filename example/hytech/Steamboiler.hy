---------------------------
-- HyTech input file
--
-- Steam boiler
--
define(P_rate,4)
define(W1,0) -- min steam rate
define(W2,6) -- max steam rate
define(MIN,5)
define(param_l_prime,25)
define(param_l,70)
define(N_1,100)
define(N_2,150)
define(param_u,170)
define(param_u_prime,200)
define(MAX,220)
define(delta,5)

var
w, -- water level
p1, -- pump volume from Pump 1 for a time slot
p2, -- pump volume from Pump 2 for a time slot
steam, -- steam volume for a time slot
drain
: analog;
t, -- controller's clock
t1, -- pump controller 1's clock
t2 -- pump controller 2's clock
: clock;
--------------------------------------------------
automaton steam_boiler
synclabs: ;
initially s0;
loc s0: while True wait {dw = dp1 + dp2 - dsteam - ddrain}
end
--------------------------------------------------
automaton pump_cont_1
synclabs: p_1_on, p_1_off;
initially off ;
loc off: while True wait {dp1=0}
when True sync p_1_on do {t1'=0} goto going_on;
loc going_on: while t1<=5 wait {dp1=0}
when t1=5 do {t1'=t1'} goto on;
loc on: while True wait {dp1=P_rate}
when True sync p_1_off goto off;
end
--------------------------------------------------
automaton pump_cont_2
synclabs: p_2_on, p_2_off;
initially off ;
loc off: while True wait {dp2=0}
when True sync p_2_on do {t2'=0} goto going_on;
loc going_on: while t2<=5 wait {dp2=0}
when t2=5 do {t2'=t2'} goto on;
loc on: while True wait {dp2=P_rate}
when True sync p_2_off goto off;
end
--------------------------------------------------
automaton valve
synclabs: open_valve, close_valve;
initially closed;
loc closed: while True wait {ddrain=0}
when True sync open_valve goto open;
loc open: while True wait {ddrain=1}
when True sync close_valve goto closed;
end
--------------------------------------------------
automaton steam
synclabs: start ;
initially idle ;
loc idle: while True wait {dsteam=0}
when True sync start goto running;
loc running: while True wait {dsteam in [W1,W2]}
end
--------------------------------------------------
automaton controller
-- in normal mode, look at water level only, and decide what to do
synclabs:
steam_boiler_waiting,
steam_rate_zero,
start, -- to turn on all systems, esp. the boiler
p_1_on, p_1_off, p_2_on, p_2_off,
open_valve, close_valve;
initially idle & t=0;
-- the initialization mode
loc idle: while True wait {}
when True sync steam_boiler_waiting do {t'=0} goto test;
loc test: while t=0 wait {}
when True sync steam_rate_zero goto init;
loc init: while t=0 wait {}
when w>=N_2 sync open_valve goto wait_till_drained;
when w<=N_1 sync p_1_on goto wait_till_fill;
when N_1<=w & w<=N_2 sync start do {t'=delta} goto off_off;
loc wait_till_drained: while w>=N_2 wait {}
when w=N_2 sync close_valve goto wait_till_drained_b;
loc wait_till_drained_b: while True wait {}
when asap sync start do {t'=delta} goto off_off;
loc wait_till_fill: while w<=N_1 wait {}
when w=N_1 sync start do {t'=delta} goto on_off;
-- the normal operating mode
loc off_off: while t<=delta wait {}
when t=delta & w<=param_l_prime goto emergency_stop;
when t=delta & w>=param_u_prime goto emergency_stop;
when t=delta & param_l_prime<=w & w<=param_l
do {t'=0} sync p_1_on
goto going_on_on;
when t=delta & param_l<=w & w<=N_1 do {t'=0} sync p_1_on
goto on_off;
when t=delta & N_2<=w & w<=param_u do {t'=0} sync p_1_on
goto on_off;
when t=delta & param_u<=w & w<=param_u_prime do {t'=0}
goto off_off;
when t=delta & N_1<=w & w<=N_2 do {t'=0} goto off_off;
loc on_off: while t<=delta wait {}
when t=delta & w<=param_l_prime goto emergency_stop;
when t=delta & w>=param_u_prime goto emergency_stop;
when t=delta & param_l_prime<=w & w<=param_l
do {t'=0} sync p_2_on
goto on_on;
when t=delta & param_l<=w & w<=N_1 do {t'=0}
goto on_off;
when t=delta & N_2<=w & w<=param_u do {t'=0}
goto on_off;
when t=delta & param_u<=w & w<=param_u_prime
do {t'=0} sync p_1_off
goto off_off;
when t=delta & N_1<=w & w<=N_2 do {t'=0} goto on_off;
loc on_on: while t<=delta wait {}
when t=delta & w<=param_l_prime goto emergency_stop;
when t=delta & w>=param_u_prime goto emergency_stop;
when t=delta & param_l_prime<=w & w<=param_l do {t'=0}
goto on_on;
when t=delta & param_l<=w & w<=N_1 do {t'=0} sync p_2_off
goto on_off;
when t=delta & N_2<=w & w<=param_u do {t'=0} sync p_2_off
goto on_off;
when t=delta & param_u<=w & w<=param_u_prime
do {t'=0} sync p_1_off
goto going_off_off;
when t=delta & N_1<=w & w<=N_2 do {t'=0} goto on_on;
loc going_on_on: while True wait {}
when asap sync p_2_on goto on_on;
loc going_off_off: while True wait {}
when asap sync p_2_off goto off_off;
-- emergency stop mode
loc emergency_stop: while t<=delta wait {}
end
--------------------
-- analysis commands
var
init_reg, final_reg, reached, reached_final: region;
init_reg := loc[steam_boiler]=s0
& param_l<=w & w<=param_u
& loc[pump_cont_1]=off
& loc[pump_cont_2]=off
& loc[valve]=closed
& loc[steam]=idle
& loc[controller]=idle
& MIN<=param_l_prime & param_l_prime<=param_l
& param_l<=N_1
& N_2<=param_u
& param_u<=param_u_prime & param_u_prime<=MAX
& delta>=0
;
final_reg := w>=MAX
| w<=MIN
| loc[controller]=emergency_stop;
reached := reach forward from init_reg endreach;
reached_final := reached & final_reg;
if empty(reached_final)
then prints "Water level maintained between bounds MIN and MAX";
else prints "Water level NOT maintained between bounds MIN and MAX";
prints "Violating states";
print reached_final;
prints "End of reached and final";
print trace to final_reg using reached;
endif;

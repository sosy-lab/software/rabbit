-- HyTech-code generated from CTA-Module "System".

define(a, 3)
define(b, 3)
define(pNo1, 1)
define(pNo2, 2)
define(pNo3, 3)
define(pNo4, 4)
define(pNo5, 5)
define(pNo6, 6)

var
  Process1_x: stopwatch;
  Process2_x: stopwatch;
  Process3_x: stopwatch;
  Process4_x: stopwatch;
  Process5_x: stopwatch;
  Process6_x: stopwatch;
  k: discrete;

automaton Process1_Fischer
  synclabs:
;

  initially 
-- Attention: Maybe there are some initial variable restrictions.
Process1_start;

  loc Process1_assign:
    while
      Process1_x <= a
    wait
    {
-- This was a true (ctaRestTrue)!

    }
    when
True      
	do
      {
Process1_x' = 0 , k' = pNo1
      }
      goto Process1_wait;

  loc Process1_critical:
    while
      True
    wait
    {
-- This was a true (ctaRestTrue)!

    }
    when
True      do
      {
k' = 0
      }
      goto Process1_uncritical;

  loc Process1_start:
    while
      False
    wait
    {
-- This was a true (ctaRestTrue)!

    }
    when
True      do
      {
k' = 0
      }
      goto Process1_uncritical;

  loc Process1_uncritical:
    while
      True
    wait
    {
-- This was a true (ctaRestTrue)!

    }
    when
k = 0      do
      {
Process1_x' = 0
      }
      goto Process1_assign;

  loc Process1_wait:
    while
      True
    wait
    {
-- This was a true (ctaRestTrue)!

    }
    when
Process1_x >= b & 
-- HyTech can't handle '<>' operation, replace manually.
k > pNo1     do
      {
-- This was a True! 

      }
      goto Process1_uncritical;
    when
Process1_x >= b & 
-- HyTech can't handle '<>' operation, replace manually.
k < pNo1      do
      {
-- This was a True! 

      }
      goto Process1_uncritical;

    when
Process1_x > b & k = pNo1      do
      {
-- This was a True! 

      }
      goto Process1_critical;

end

automaton Process2_Fischer
  synclabs:
;

  initially 
-- Attention: Maybe there are some initial variable restrictions.
Process2_start;

  loc Process2_assign:
    while
      Process2_x <= a
    wait
    {
-- This was a true (ctaRestTrue)!

    }
    when
True      do
      {
Process2_x' = 0 , k' = pNo2
      }
      goto Process2_wait;

  loc Process2_critical:
    while
      True
    wait
    {
-- This was a true (ctaRestTrue)!

    }
    when
True      do
      {
k' = 0
      }
      goto Process2_uncritical;

  loc Process2_start:
    while
      False
    wait
    {
-- This was a true (ctaRestTrue)!

    }
    when
True      do
      {
k' = 0
      }
      goto Process2_uncritical;

  loc Process2_uncritical:
    while
      True
    wait
    {
-- This was a true (ctaRestTrue)!

    }
    when
k = 0      do
      {
Process2_x' = 0
      }
      goto Process2_assign;

  loc Process2_wait:
    while
      True
    wait
    {
-- This was a true (ctaRestTrue)!

    }
    when
Process2_x >= b & 
-- HyTech can't handle '<>' operation, replace manually.
k < pNo2      do
      {
-- This was a True! 

      }
      goto Process2_uncritical;
    when
Process2_x >= b & 
-- HyTech can't handle '<>' operation, replace manually.
k > pNo2     do
      {
-- This was a True! 

      }
      goto Process2_uncritical;

    when
Process2_x > b & k = pNo2      do
      {
-- This was a True! 

      }
      goto Process2_critical;

end







automaton Process3_Fischer
  synclabs:
;

  initially 
-- Attention: Maybe there are some initial variable restrictions.
Process3_start;

  loc Process3_assign:
    while
      Process3_x <= a
    wait
    {
-- This was a true (ctaRestTrue)!

    }
    when
True      do
      {
Process3_x' = 0 , k' = pNo3
      }
      goto Process3_wait;

  loc Process3_critical:
    while
      True
    wait
    {
-- This was a true (ctaRestTrue)!

    }
    when
True      do
      {
k' = 0
      }
      goto Process3_uncritical;

  loc Process3_start:
    while
      False
    wait
    {
-- This was a true (ctaRestTrue)!

    }
    when
True      do
      {
k' = 0
      }
      goto Process3_uncritical;

  loc Process3_uncritical:
    while
      True
    wait
    {
-- This was a true (ctaRestTrue)!

    }
    when
k = 0      do
      {
Process3_x' = 0
      }
      goto Process3_assign;

  loc Process3_wait:
    while
      True
    wait
    {
-- This was a true (ctaRestTrue)!

    }
    when
Process3_x >= b & 
-- HyTech can't handle '<>' operation, replace manually.
k > pNo3     do
      {
-- This was a True! 

      }
      goto Process3_uncritical;
    when
Process3_x >= b & 
-- HyTech can't handle '<>' operation, replace manually.
k < pNo3      do
      {
-- This was a True! 

      }
      goto Process3_uncritical;

    when
Process3_x > b & k = pNo3      do
      {
-- This was a True! 

      }
      goto Process3_critical;

end





automaton Process4_Fischer
  synclabs:
;

  initially 
-- Attention: Maybe there are some initial variable restrictions.
Process4_start;

  loc Process4_assign:
    while
      Process4_x <= a
    wait
    {
-- This was a true (ctaRestTrue)!

    }
    when
True      do
      {
Process4_x' = 0 , k' = pNo4
      }
      goto Process4_wait;

  loc Process4_critical:
    while
      True
    wait
    {
-- This was a true (ctaRestTrue)!

    }
    when
True      do
      {
k' = 0
      }
      goto Process4_uncritical;

  loc Process4_start:
    while
      False
    wait
    {
-- This was a true (ctaRestTrue)!

    }
    when
True      do
      {
k' = 0
      }
      goto Process4_uncritical;

  loc Process4_uncritical:
    while
      True
    wait
    {
-- This was a true (ctaRestTrue)!

    }
    when
k = 0      do
      {
Process4_x' = 0
      }
      goto Process4_assign;

  loc Process4_wait:
    while
      True
    wait
    {
-- This was a true (ctaRestTrue)!

    }
    when
Process4_x >= b & 
-- HyTech can't handle '<>' operation, replace manually.
k > pNo4     do
      {
-- This was a True! 

      }
      goto Process4_uncritical;
    when
Process4_x >= b & 
-- HyTech can't handle '<>' operation, replace manually.
k < pNo4      do
      {
-- This was a True! 

      }
      goto Process4_uncritical;

    when
Process4_x > b & k = pNo4      do
      {
-- This was a True! 

      }
      goto Process4_critical;

end



automaton Process5_Fischer
  synclabs:
;

  initially 
-- Attention: Maybe there are some initial variable restrictions.
Process5_start;

  loc Process5_assign:
    while
      Process5_x <= a
    wait
    {
-- This was a true (ctaRestTrue)!

    }
    when
True      do
      {
Process5_x' = 0 , k' = pNo5
      }
      goto Process5_wait;

  loc Process5_critical:
    while
      True
    wait
    {
-- This was a true (ctaRestTrue)!

    }
    when
True      do
      {
k' = 0
      }
      goto Process5_uncritical;

  loc Process5_start:
    while
      False
    wait
    {
-- This was a true (ctaRestTrue)!

    }
    when
True      do
      {
k' = 0
      }
      goto Process5_uncritical;

  loc Process5_uncritical:
    while
      True
    wait
    {
-- This was a true (ctaRestTrue)!

    }
    when
k = 0      do
      {
Process5_x' = 0
      }
      goto Process5_assign;

  loc Process5_wait:
    while
      True
    wait
    {
-- This was a true (ctaRestTrue)!

    }
    when
Process5_x >= b & 
-- HyTech can't handle '<>' operation, replace manually.
k > pNo5     do
      {
-- This was a True! 

      }
      goto Process5_uncritical;
    when
Process5_x >= b & 
-- HyTech can't handle '<>' operation, replace manually.
k < pNo5      do
      {
-- This was a True! 

      }
      goto Process5_uncritical;

    when
Process5_x > b & k = pNo5      do
      {
-- This was a True! 

      }
      goto Process5_critical;

end

automaton Process6_Fischer
  synclabs:
;

  initially 
-- Attention: Maybe there are some initial variable restrictions.
Process6_start;

  loc Process6_assign:
    while
      Process6_x <= a
    wait
    {
-- This was a true (ctaRestTrue)!

    }
    when
True      do
      {
Process6_x' = 0 , k' = pNo6
      }
      goto Process6_wait;

  loc Process6_critical:
    while
      True
    wait
    {
-- This was a true (ctaRestTrue)!

    }
    when
True      do
      {
k' = 0
      }
      goto Process6_uncritical;

  loc Process6_start:
    while
      False
    wait
    {
-- This was a true (ctaRestTrue)!

    }
    when
True      do
      {
k' = 0
      }
      goto Process6_uncritical;

  loc Process6_uncritical:
    while
      True
    wait
    {
-- This was a true (ctaRestTrue)!

    }
    when
k = 0      do
      {
Process6_x' = 0
      }
      goto Process6_assign;

  loc Process6_wait:
    while
      True
    wait
    {
-- This was a true (ctaRestTrue)!

    }
    when
Process6_x >= b & 
-- HyTech can't handle '<>' operation, replace manually.
k > pNo6     do
      {
-- This was a True! 

      }
      goto Process6_uncritical;
    when
Process6_x >= b & 
-- HyTech can't handle '<>' operation, replace manually.
k < pNo6      do
      {
-- This was a True! 

      }
      goto Process6_uncritical;

    when
Process6_x > b & k = pNo6      do
      {
-- This was a True! 

      }
      goto Process6_critical;

end

-- 

var 
   init_reg, final_reg, reached : region;

init_reg:= loc[Process1_Fischer] = Process1_start 
        & loc[Process2_Fischer] = Process2_start
	& loc[Process3_Fischer] = Process3_start
	& loc[Process4_Fischer] = Process4_start
	& loc[Process5_Fischer] = Process5_start
	& loc[Process6_Fischer] = Process6_start
	& k=0;

final_reg:= (loc[Process1_Fischer] = Process1_critical 
	& loc[Process2_Fischer] = Process2_critical) 
	|(loc[Process1_Fischer] = Process1_critical 
	& loc[Process3_Fischer] = Process3_critical) 
	|(loc[Process1_Fischer] = Process1_critical 
	& loc[Process4_Fischer] = Process4_critical) 
	|(loc[Process1_Fischer] = Process1_critical 
	& loc[Process5_Fischer] = Process5_critical) 
	|(loc[Process1_Fischer] = Process1_critical 
	& loc[Process6_Fischer] = Process6_critical) 
	|(loc[Process2_Fischer] = Process2_critical 
	& loc[Process3_Fischer] = Process3_critical) 
	|(loc[Process2_Fischer] = Process2_critical 
	& loc[Process4_Fischer] = Process4_critical) 
	|(loc[Process2_Fischer] = Process2_critical 
	& loc[Process5_Fischer] = Process5_critical) 
	|(loc[Process2_Fischer] = Process2_critical 
	& loc[Process6_Fischer] = Process6_critical) 
	|(loc[Process3_Fischer] = Process3_critical 
	& loc[Process4_Fischer] = Process4_critical)
	|(loc[Process3_Fischer] = Process3_critical 
	& loc[Process5_Fischer] = Process5_critical)
	|(loc[Process3_Fischer] = Process3_critical 
	& loc[Process6_Fischer] = Process6_critical)
	|(loc[Process4_Fischer] = Process4_critical 
	& loc[Process5_Fischer] = Process5_critical)
	|(loc[Process4_Fischer] = Process4_critical 
	& loc[Process6_Fischer] = Process6_critical)
	|(loc[Process5_Fischer] = Process5_critical 
	& loc[Process6_Fischer] = Process6_critical);


reached:= reach backward from final_reg endreach;

prints "Condition for faulty system";

print omit all locations hide non_parameters in reached & init_reg endhide;

-- ep (db 2000-07-06): 605.29u 5.11s 10:16.28 99.0%
			(Produktautomat ben"otigt ca. 64 MB)




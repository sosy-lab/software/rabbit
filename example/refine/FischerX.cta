MODULE Refinement
{
  LOCAL

    a = 1: CONST;
    b = 2: CONST;

    pNo1 = 1: CONST;
    pNo2 = 2: CONST;
    pNo=1:CONST;

    k: DISCRETE(3);

    //    enter: SYNC;
    //leave: SYNC;

INITIAL k=0;

  INST Q FROM Fischer2abst WITH
  {
    a             AS a;
    b             AS b;
    lowProcessNo  AS pNo1;
    upProcessNo   AS pNo;
    k             AS k;
    //enter         AS enter;
    //leave         AS leave;
  }

  INST P FROM Process WITH
  {
    a             AS a;
    b             AS b;
    processNo  AS pNo1;
    //processNo   AS pNo2;
    k             AS k;
    //enter         AS enter;
    //leave         AS leave;
  }
}


/*
MODULE Refinement
{
  LOCAL

    a = 1: CONST;
    b = 2: CONST;

    pNo1 = 1: CONST;
    pNo2 = 2: CONST;

    k: DISCRETE(3);

    enter: SYNC;
    leave: SYNC;
	
  INST Q FROM Fischer2abst WITH
  {
    a             AS a;
    b             AS b;
    lowProcessNo  AS pNo1;
    upProcessNo   AS pNo2;
    k             AS k;
    enter         AS enter;
    leave         AS leave;
  }

  INST P FROM Fischer2impl WITH
  {
    a             AS a;
    b             AS b;
    lowProcessNo  AS pNo1;
    upProcessNo   AS pNo2;
    k             AS k;
    enter         AS enter;
    leave         AS leave;
  }
}
*/

MODULE Fischer2abst
{
  // Constants for time bounds.
  INPUT
    // a is the maximal time the modelled assignment k:=1 needs.
    // b is the minimal time the process waits for assignments
    a:          CONST;
    b:          CONST;
    // Parameter for significant value of k.
    lowProcessNo:  CONST;
    upProcessNo:  CONST;

  MULTREST
    // k is the flag for announcement.
    k: DISCRETE;

  MULTREST
  //enter: SYNC;
  //leave: SYNC;

  INITIAL   STATE(Fischer) = uncritical AND 
            x >= 2;  

  AUTOMATON Fischer
  {
    STATE uncritical { TRANS { GUARD   k = 0 OR
			               (k >= lowProcessNo AND
                                        k <= upProcessNo);
                               DO   x' = 0;  
			       GOTO assign;
			     }
		     }

    STATE assign     { //INV   x <= a;  
		       TRANS { DO   x' = 0 AND 
				    k' >= lowProcessNo AND
                                    k' <= upProcessNo;  
			       GOTO wait;
			     }
                     }

    STATE wait       { TRANS { GUARD   x >= b AND 
				       k >= upProcessNo + 1 AND
				   k + 1 <= lowProcessNo;  
			       GOTO uncritical;
			     }
                       TRANS { GUARD   x >= b AND  
				       k >= lowProcessNo AND
                                       k <= upProcessNo;
		       //                               SYNC #enter;
			       GOTO critical;
			     }
                     }

    STATE critical   { TRANS { 
      //SYNC #leave;
                               DO   k' = 0;
			       GOTO uncritical;
			     }
    }
  }

  LOCAL
    // A clock to measure the time in a state.
    x:  CLOCK(2);
}

/*
MODULE Fischer2impl
{
  INPUT
    a:        CONST;
    b:        CONST;

    // Parameter for significant value of k.
    lowProcessNo:  CONST;
    upProcessNo:  CONST;

  MULTREST
    k: DISCRETE;
	
  MULTREST
    enter: SYNC;
    leave: SYNC;

  LOCAL
    enter1: SYNC;
    leave1: SYNC;
    enter2: SYNC;
    leave2: SYNC;

  
  INITIAL   STATE(SyncProxy) = ResourceFree;

  AUTOMATON SyncProxy
  {
    STATE ResourceFree  { TRANS { SYNC ?enter1;
                                  GOTO ThrowSigEnter;
			        }
                          TRANS { SYNC ?enter2;  
                                  GOTO ThrowSigEnter;
			        }
		        }

    STATE ThrowSigEnter { INV FALSE;
                          TRANS { SYNC #enter;  
                                  GOTO ResourceInUse;
			        }
                        }   
                    
    STATE ResourceInUse { TRANS { SYNC ?leave1;
			          GOTO ThrowSigLeave;
			        }
                          TRANS { SYNC ?leave2;   
			          GOTO ThrowSigLeave;
			        }
	                }
    STATE ThrowSigLeave { INV FALSE;
                          TRANS { SYNC #leave;  
                                  GOTO ResourceInUse;
			        }
                        }
  }

  INITIAL   k = 0;

  INST Proc01 FROM Process WITH
  {
    a         AS a;
    b         AS b;
    processNo AS lowProcessNo;
    k         AS k;
    enter     AS enter1;
    leave     AS leave1;
  }

  INST Proc02 FROM Process WITH
  {
    a         AS a;
    b         AS b;
    processNo AS upProcessNo;
    k         AS k;
    enter     AS enter2;
    leave     AS leave2;
  }
}
*/

MODULE Process
{
  // Constants for time bounds.
  INPUT
    // a is the maximal time the modelled assignment k:=1 needs.
    // b is the minimal time the process waits for assignments
    a:          CONST;
    b:          CONST;
    // Parameter for significant value of k.
    processNo:  CONST;

  MULTREST
    // k is the flag for announcement.
    k: DISCRETE;
  //enter: SYNC;
  //leave: SYNC;

  INITIAL   STATE(Fischer) = uncritical AND x >= 2;  

  AUTOMATON Fischer
  {
    STATE uncritical { TRANS { GUARD   k = 0 OR
			               k = processNo;  
                               DO   x' = 0;  
			       GOTO assign;
			     }
		     }

    STATE assign     { INV   x <= a;  
		       TRANS { DO   x' = 0 AND 
                                    k' = processNo;  
			       GOTO wait;
			     }
                     }

    STATE wait       { TRANS { GUARD   x >= b AND 
                                       k != processNo;  
			       GOTO uncritical;
			     }
                       TRANS { GUARD   x >= b AND 
                                       k = processNo;  
//                               SYNC #enter;
			       GOTO critical;
			     }
                     }

    STATE critical   { TRANS {
// SYNC #leave;
                               DO   k' = 0;
			       GOTO uncritical;
			     }
    }
  }

  LOCAL
    // A clock to measure the time in a state.
    x:  CLOCK(2);
}

REGION CHECK Refinement
{
COMMANDS
  IF( ISSIMULATION(Refinement) )
  {
     PRINT " Simulation relation exists.";
  }
  ELSE
  {
    PRINT " Simulation relation does not exist.";
  }
}


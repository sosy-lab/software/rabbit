/* 
 * Test model for refinement checking.
 *
 * The refinement check should result in NOT finding a simulation
 * relation between Q and P.
 *
 * #################################################################
 * The tool rabbit (version 2001-02-23) finds a simulation relation.
 * This is an unknown problem to be found.
 * #################################################################
 *
 * (The module TwoStateA (Q) uses only signal syncOne. Module TwoStateB (P)
 * uses both (syncOne and syncTwo) signals.
 *
 * file:  missingSyncProblem.cta
 * model: two synchronized TwoState automata
 *
 * version: 23.02.2001
 * author:  Michael Vogel - (mvogel@informatik.tu-cottbus.de)
 *
 */   


MODULE System
{
  LOCAL
    syncOne     : SYNC;
    syncTwo     : SYNC;

  INST Q FROM TwoStateA WITH
  {
    syncOne AS syncOne;
    syncTwo AS syncTwo;
  } 

  INST P FROM TwoStateB WITH
  {
    syncOne AS syncOne;
    syncTwo AS syncTwo;
  } 
}

MODULE TwoStateA
{
 
  MULTREST
    syncOne : SYNC;
    syncTwo : SYNC;

  INITIAL  STATE(twoState) = one;
    
  AUTOMATON twoState
  {
    STATE one  { TRANS { // SYNC #syncTwo;
                         GOTO two;
                       }
               }
    
    STATE two  { TRANS { SYNC #syncOne;
                         GOTO one;
		       }
               }
  }
}

MODULE TwoStateB
{
 
  MULTREST
    syncOne : SYNC;
    syncTwo : SYNC;

  INITIAL  STATE(twoState) = one;
    
  AUTOMATON twoState
  {
    STATE one  { TRANS { SYNC #syncTwo;
                         GOTO two;
                       }
               }
    
    STATE two  { TRANS { SYNC #syncOne;
                         GOTO one;
		       }
               }
  }
}

REFINEMENT CHECK System
USE BDD;

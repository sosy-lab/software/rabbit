/* 
 * Test model for refinement checking.
 *
 * The refinement checking should result in NOT finding an simulation
 * relation between Q and P.
 * (The initial state of MODULE UntimedSensor was changed from 'off'
 * to 'on' in difference to file test1.cta)
 *
 * file:  test2.cta
 * model: model of timed and untimed belt sensors (part of 4Belts.cta)
 *
 * version: 22.02.2001
 * author:  Michael Vogel - (mvogel@informatik.tu-cottbus.de)
 *
 */   

MODULE Refinement
{
  LOCAL
    beltStart            : SYNC;
    beltStop             : SYNC;
    pieceStartedToArrive : SYNC;
    sensOn               : SYNC;
    sensOff              : SYNC;

  INST Q FROM UntimedSensor WITH
  {
    beltStart            AS beltStart;
    beltStop             AS beltStop;
    pieceStartedToArrive AS  pieceStartedToArrive;
    sensOn               AS sensOn;
    sensOff              AS sensOff;
  }

  INST P FROM TimedSensor WITH
  {
    beltStart            AS beltStart;
    beltStop             AS beltStop;
    pieceStartedToArrive AS  pieceStartedToArrive;
    sensOn               AS sensOn;
    sensOff              AS sensOff; 
  }

}

MODULE TimedSensor
{
  INPUT
    beltStart            : SYNC;
    beltStop             : SYNC;
    pieceStartedToArrive : SYNC;

  LOCAL
    cMaxVal=4            : CONST;
    minTimeToGoOn = 2    : CONST;
    maxTimeToGoOn = 3    : CONST;
    minTimeToGoOff = 1   : CONST;
    maxTimeToGoOff = 2   : CONST;
    c                    : CLOCK(4); // clock for piece position

  // Is multrest restriction correct for refinement checking??
  // (mvogel-2001-01-25)
  MULTREST
    sensOn:   SYNC;  // Switch-on synchronisation.
    sensOff:  SYNC;  // Switch-off synchronisation.

  INITIAL   STATE(SensorBehavior) = off  AND c = cMaxVal ;  

  AUTOMATON SensorBehavior
  {
    STATE off             { // Switch to on is possible if belt is beeing
                            // switched on and there is a piece (from
                            // which the switch-on command for belt comes)
                            // which can arrive.
                            TRANS { SYNC ?pieceStartedToArrive;
                                    DO   c' = 0;   
                                    GOTO possibleToGoOn;
                                  }

			    // Temporary it is not possible to store the
                            // piece position. Thus beltStart - Stop
                            // is not possible.
		    /*
                            // If the belt was switched off before the
                            // sensor was reached. Then - if the belt
                            // switches to on again - here is the transition
                            // to go back to "possibleToGoOn".
                            TRANS { SYNC ?beltStart;
			            DO   c = 0;  
                                    GOTO possibleToGoOn;
                                  }
                     */
                            // We have to ensure that the first sensor only
			    // reacts on signals concerning it. A beltStart or
                            // -stop signal in this position is for the second
                            // beltSensor.
                            // (maybe implement two different signals)
                            TRANS { SYNC ?beltStart;
                                    GOTO off;
                                  }

                            TRANS { SYNC ?beltStop;
                                    GOTO off;
                                  }
                          }

    STATE possibleToGoOn  { INV  c <= maxTimeToGoOn;
                            TRANS { GUARD c >= minTimeToGoOn;
                                    SYNC #sensOn;
				    DO c' = cMaxVal;
	                            GOTO  on;
                                  }

                           // There is a sychronization problem:
                           // If sensor B of Belt 2 switches to off
                           // a startInTransfer signal is generated
                           // for sensor A of Belt 2 (goes to state
                           //  possibleToGoOn) then the controller
                           // starts the belt, but the sensor cannot
                           // react on the start signal, so we have to
                           // include a loop trnsition here.
                            // Switch to on is only possible if the belt
                            // is on.
                            TRANS { SYNC ?beltStart;
                                     GOTO possibleToGoOn;
                                   }
                        
			// Temporary it is not possible to store
                        // the position information if stopping the
                        // belt.  
                        /*  TRANS { SYNC ?beltStop;
                                    DO   c = maxVal;  
                                    GOTO  off;
                                  } */
                          }

    // Switch to off is possible if belt is beeing
    // switched on.
    STATE on              { TRANS { SYNC ?beltStop;
                                    GOTO on; 
                                  }
                            TRANS { SYNC ?beltStart;
                                    DO   c' = 0;
                                    GOTO  possibleToGoOff;
                                  }

                          }
 
    // Problem without Realtime: After same time
    // if motor is on the sensor have to switch to off.
    STATE possibleToGoOff { INV   c <= maxTimeToGoOff;
                            // After some time the piece leaves the sensor.
                            TRANS { GUARD   c >= minTimeToGoOff;
                                    SYNC #sensOff;
			            DO   c' = cMaxVal;
                                    GOTO  off;
                                  }
                          
                           // Temporary it is not possible to store
                           // the position information if stopping the
                           // belt.  
              //           TRANS { SYNC ?beltStop;
              //                   GOTO  on;
              //                 } 
                          }
 
  }
}



MODULE UntimedSensor
{
  INPUT
    beltStart            : SYNC;
    beltStop             : SYNC;
    pieceStartedToArrive : SYNC;

  MULTREST
    sensOn:   SYNC;  // Switch-on synchronisation.
    sensOff:  SYNC;  // Switch-off synchronisation.

  INITIAL   STATE(SensorBehavior) = on;

  AUTOMATON SensorBehavior
  {
    STATE off             { TRANS { SYNC ?pieceStartedToArrive;
                                    GOTO possibleToGoOn;
                                  }

                            TRANS { SYNC ?beltStart;
                                    GOTO off;
                                  }

                            TRANS { SYNC ?beltStop;
                                    GOTO off;
                                  }
                          }

    STATE possibleToGoOn  { TRANS { SYNC #sensOn;
	                            GOTO  on;
                                  }

                            TRANS { SYNC ?beltStart;
                                     GOTO possibleToGoOn;
                                   }
                          }

    STATE on              { TRANS { SYNC ?beltStop;
                                    GOTO on; 
                                  }
                            TRANS { SYNC ?beltStart;
                                    GOTO  possibleToGoOff;
                                  }

                          }
 
    STATE possibleToGoOff { TRANS { SYNC #sensOff;
                                    GOTO  off;
                                  }
                          }
   }
}


REGION CHECK Refinement
{
COMMANDS
  IF( ISSIMULATION(Refinement) )
  {
     PRINT " Simulation relation exists.";
  }
  ELSE
  {
    PRINT " Simulation relation does not exist.";
  }
}



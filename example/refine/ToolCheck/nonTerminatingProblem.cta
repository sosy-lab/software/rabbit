/* 
 * Test model for refinement checking.
 *
 * The refinement checking should result in finding an simulation
 * relation between Q and P.
 *
 * This model causes the non terminating refinement check problem, because
 * the fixpoint will be reached after three steps compared to one step of
 * other models. The bug was fixed 2001-02-23 by db.
 * 
 * file:  nonTerminatingProblem.cta
 * Model: Fischer's protocol
 *
 * version: 23.02.2001
 * author:  Michael Vogel - (mvogel@informatik.tu-cottbus.de)
 *
 */

MODULE Refinement
{
  LOCAL
    a = 1: CONST;
    b = 2: CONST;

    pNo1 = 1: CONST;
    pNo2 = 2: CONST;

  LOCAL                // MULTREST for the submodules.
    k: DISCRETE(3);

  INITIAL k = 0;
	
  INST Q FROM Process WITH
  {
    a         AS a;
    b         AS b;
    processNo AS pNo1;
    k         AS k;
  }

  INST P FROM Process WITH
  {
    a         AS a;
    b         AS b;
    processNo AS pNo2;
    k         AS k;
  }

}

MODULE Process
{
  // Constants for time bounds.
  INPUT
    // a is the maximal time the modelled assignment k:=1 needs.
    // b is the minimal time the process waits for assignments
    a:          CONST;
    b:          CONST;
    // Parameter for significant value of k.
    processNo:  CONST;

  MULTREST
    // k is the flag for announcement.
    k: DISCRETE;

  INITIAL   STATE(Fischer) = uncritical AND x >= 2;  

  AUTOMATON Fischer
  {
    STATE uncritical { TRANS { GUARD   k  = 0;
                               DO   x' = 0;  
			       GOTO assign;
			     }
		     }

    STATE assign     { INV   x <= a;  
		       TRANS { DO   x' = 0 AND k' = processNo;  
			       GOTO wait;
			     }
                     }

    STATE wait       { TRANS { GUARD   x >= b AND k != processNo;  
			       GOTO uncritical;
			     }
                       TRANS { GUARD   x >= b AND k = processNo;  
			       GOTO critical;
			     }
                     }

    STATE critical   { TRANS { DO   k' = 0 OR k' = processNo;  
			       GOTO uncritical;
			     }
    }
  }

  LOCAL
    // A clock to measure the time in a state.
    x:  CLOCK(2);
}

/*
REGION CHECK Refinement
{
VAR 
    initial, reached: REGION;

COMMANDS
    initial :=  INITIALREGION;

    reached := REACH FROM initial FORWARD;

PRINT reached;
}
*/

REGION CHECK Refinement
{
COMMANDS
  IF( ISSIMULATION(Refinement) )
  {
     PRINT " Simulation relation exists.";
  }
  ELSE
  {
    PRINT " Simulation relation does not exist.";
  }
}



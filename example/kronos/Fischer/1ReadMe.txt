--- Nutzung von Kronos ---

Kronos ist ein sehr bedienfreundliches Tool, deshalb diese kurze Anleitung :)

Jede Fischer?.tg -Datei in diesem Verzeichnis ist ein timed automaton
fuer einen Fischer-Automaten.

Der Automat in den varK*.tg Dateien simuliert eine diskrete Variable k durch
Synchronisation mit den einzelnen Fischerautomaten. Der Wert der Variablen
wird durch den Zustand representiert.

Zur Bildung des Produktautomaten sind die entsprechenden
Fischerautomaten und der passende Automat f"ur die diskrete Variable
zu waehlen. (siehe folgendes Beispiel)

- Erzeugung des Produktautomaten: (Beispiel fuer Fischer5)
kronos -CONTROL 32760 -out Fischer5prod.tg Fischer1.tg Fischer2.tg Fischer3.tg Fischer4.tg Fischer5.tg varK5.tg

- Analyse mit dem Produktautomaten: (Beispiel fuer Fischer5)
kronos -v -FORW 1000000 -reach Fischer.tctl Fischer5prod.tg

- Analyse auf dem Produktautomaten mit Approcimation. (Fischer5)
kronos -v -ac -FORW 10000 -reach Fischer.tctl Fischer5prod.tg

- On-the-fly Analyse ohne expliziten Produktautomaten. (Fischer5)
kronos -CONTROL 10000000 -FORW 1000000 -reach Fischer.tctl Fischer1.tg Fischer2.tg Fischer3.tg Fischer4.tg Fischer5.tg varK5.tg


--- ACHTUNG: ---

Bei der parallelen Nutzung von Kronos auf mehreren Plattformen
(z.B. Linux, Solaris) sollte man zwischendurch mal die
von Kronos generierten Dateien (teilw. Binaerfiles) loeschen.

Solaris kann z.B. mit den .kro-Dateien, die von der Linux-Kronos
Version erzeugt wurden, nichts anfangen.

Die hilfreiche Fehlermeldung: "kronos: not enough memory"

mvogel-2002-03-15

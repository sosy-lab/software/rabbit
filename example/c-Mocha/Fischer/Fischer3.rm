/* 
 * This is an implementation in realtime modules 
 * of the Fischer mutual exclusion protocol.
 * written by Michael Vogel an Andy Heinig
 * 11.01.2001
 */

/*
 * Type declarations.
 */

type pctype:{assign, waiting}
 
/*
 * Atomic modules.
 */

module control
  interface k:(0..2)
    
  lazy atom check controls k
            reads k
    init
      [] true -> k' := 0
    update
      [] true -> k' := 0
  endatom
endmodule

module P1
  interface pc1 : pctype
  external k:(0..2)
  private x1 : clock

  lazy atom A1 controls x1, pc1
               reads x1, pc1, k
    init
      [] true -> pc1' := assign
    update
      [] pc1 = assign & x1 >= 2             -> pc1' := waiting
    wait
      [] pc1 = waiting                      ->
      [] pc1 = assign & x1 <= 3             -> x1' :<= 3
      
  endatom
endmodule

 
/* 
 * Composed modules.
 */
System := P1 || control
System := hide arrive in (P1 || control) endhide




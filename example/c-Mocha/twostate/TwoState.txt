Verifikation TwoState-Modell mit Mocha.

rechner: walnuss
cpu:     AMD Athlon TB 1GHZ 1.2GB

C-Mocha 1.0.1 Linux

|--------------------|------|------|--------|-----------|
| Anzahl Automaten:  |    2 |    4 |      6 |         8 |
|--------------------|------|------|--------|-----------|
| Analysezeit:       | 0.08 | 4.74 | 360.95 | > 7200.00 |
| Speicher (MB):     |    7 |   16 |     26 |     > 139 |
|--------------------|------|------|--------|-----------|

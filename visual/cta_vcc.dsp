# Microsoft Developer Studio Project File - Name="cta_vcc" - Package Owner=<4>
# Microsoft Developer Studio Generated Build File, Format Version 5.00
# ** NICHT BEARBEITEN **

# TARGTYPE "Win32 (x86) Console Application" 0x0103

CFG=cta_vcc - Win32 Debug
!MESSAGE Dies ist kein g�ltiges Makefile. Zum Erstellen dieses Projekts mit\
 NMAKE
!MESSAGE verwenden Sie den Befehl "Makefile exportieren" und f�hren Sie den\
 Befehl
!MESSAGE 
!MESSAGE NMAKE /f "cta_vcc.mak".
!MESSAGE 
!MESSAGE Sie k�nnen beim Ausf�hren von NMAKE eine Konfiguration angeben
!MESSAGE durch Definieren des Makros CFG in der Befehlszeile. Zum Beispiel:
!MESSAGE 
!MESSAGE NMAKE /f "cta_vcc.mak" CFG="cta_vcc - Win32 Debug"
!MESSAGE 
!MESSAGE F�r die Konfiguration stehen zur Auswahl:
!MESSAGE 
!MESSAGE "cta_vcc - Win32 Release" (basierend auf\
  "Win32 (x86) Console Application")
!MESSAGE "cta_vcc - Win32 Debug" (basierend auf\
  "Win32 (x86) Console Application")
!MESSAGE "cta_vcc - Win32 Profile" (basierend auf\
  "Win32 (x86) Console Application")
!MESSAGE 

# Begin Project
# PROP Scc_ProjName ""
# PROP Scc_LocalPath ""
CPP=cl.exe
RSC=rc.exe

!IF  "$(CFG)" == "cta_vcc - Win32 Release"

# PROP BASE Use_MFC 0
# PROP BASE Use_Debug_Libraries 0
# PROP BASE Output_Dir "Release"
# PROP BASE Intermediate_Dir "Release"
# PROP BASE Target_Dir ""
# PROP Use_MFC 0
# PROP Use_Debug_Libraries 0
# PROP Output_Dir "Release"
# PROP Intermediate_Dir "Release"
# PROP Ignore_Export_Lib 0
# PROP Target_Dir ""
# ADD BASE CPP /nologo /W3 /GX /O2 /D "WIN32" /D "NDEBUG" /D "_CONSOLE" /D "_MBCS" /YX /FD /c
# ADD CPP /nologo /W3 /GR /GX /O2 /I ".\ana" /I ".\ddm" /I ".\ana\gnu" /D "WIN32" /D "NDEBUG" /D "_CONSOLE" /D "_MBCS" /YX /FD /c
# ADD BASE RSC /l 0x407 /d "NDEBUG"
# ADD RSC /l 0x407 /d "NDEBUG"
BSC32=bscmake.exe
# ADD BASE BSC32 /nologo
# ADD BSC32 /nologo
LINK32=link.exe
# ADD BASE LINK32 kernel32.lib user32.lib gdi32.lib winspool.lib comdlg32.lib advapi32.lib shell32.lib ole32.lib oleaut32.lib uuid.lib odbc32.lib odbccp32.lib /nologo /subsystem:console /machine:I386
# ADD LINK32 kernel32.lib user32.lib gdi32.lib winspool.lib comdlg32.lib advapi32.lib shell32.lib ole32.lib oleaut32.lib uuid.lib odbc32.lib odbccp32.lib /nologo /subsystem:console /machine:I386

!ELSEIF  "$(CFG)" == "cta_vcc - Win32 Debug"

# PROP BASE Use_MFC 0
# PROP BASE Use_Debug_Libraries 1
# PROP BASE Output_Dir "Debug"
# PROP BASE Intermediate_Dir "Debug"
# PROP BASE Target_Dir ""
# PROP Use_MFC 0
# PROP Use_Debug_Libraries 1
# PROP Output_Dir "Debug"
# PROP Intermediate_Dir "Debug"
# PROP Ignore_Export_Lib 0
# PROP Target_Dir ""
# ADD BASE CPP /nologo /W3 /Gm /GX /Zi /Od /D "WIN32" /D "_DEBUG" /D "_CONSOLE" /D "_MBCS" /YX /FD /c
# ADD CPP /nologo /W3 /Gm /GR /GX /Zi /Od /I ".\ana" /I ".\ddm" /I ".\ana\gnu" /D "WIN32" /D "_DEBUG" /D "_CONSOLE" /D "_MBCS" /YX /FD /c
# ADD BASE RSC /l 0x407 /d "_DEBUG"
# ADD RSC /l 0x407 /d "_DEBUG"
BSC32=bscmake.exe
# ADD BASE BSC32 /nologo
# ADD BSC32 /nologo
LINK32=link.exe
# ADD BASE LINK32 kernel32.lib user32.lib gdi32.lib winspool.lib comdlg32.lib advapi32.lib shell32.lib ole32.lib oleaut32.lib uuid.lib odbc32.lib odbccp32.lib /nologo /subsystem:console /debug /machine:I386 /pdbtype:sept
# ADD LINK32 kernel32.lib user32.lib gdi32.lib winspool.lib comdlg32.lib advapi32.lib shell32.lib ole32.lib oleaut32.lib uuid.lib odbc32.lib odbccp32.lib /nologo /subsystem:console /debug /machine:I386 /pdbtype:sept

!ELSEIF  "$(CFG)" == "cta_vcc - Win32 Profile"

# PROP BASE Use_MFC 0
# PROP BASE Use_Debug_Libraries 0
# PROP BASE Output_Dir "Profile"
# PROP BASE Intermediate_Dir "Profile"
# PROP BASE Target_Dir ""
# PROP Use_MFC 0
# PROP Use_Debug_Libraries 0
# PROP Output_Dir "Profile"
# PROP Intermediate_Dir "Profile"
# PROP Ignore_Export_Lib 0
# PROP Target_Dir ""
# ADD BASE CPP /nologo /W3 /GX /O2 /D "WIN32" /D "NDEBUG" /D "_CONSOLE" /D "_MBCS" /YX /FD /c
# ADD CPP /nologo /W3 /GR /GX /O2 /I ".\ana" /I ".\ddm" /I ".\ana\gnu" /D "WIN32" /D "NDEBUG" /D "_CONSOLE" /D "_MBCS" /YX /FD /c
# ADD BASE RSC /l 0x407 /d "NDEBUG"
# ADD RSC /l 0x407 /d "NDEBUG"
BSC32=bscmake.exe
# ADD BASE BSC32 /nologo
# ADD BSC32 /nologo
LINK32=link.exe
# ADD BASE LINK32 kernel32.lib user32.lib gdi32.lib winspool.lib comdlg32.lib advapi32.lib shell32.lib ole32.lib oleaut32.lib uuid.lib odbc32.lib odbccp32.lib /nologo /subsystem:console /machine:I386
# ADD LINK32 kernel32.lib user32.lib gdi32.lib winspool.lib comdlg32.lib advapi32.lib shell32.lib ole32.lib oleaut32.lib uuid.lib odbc32.lib odbccp32.lib /nologo /subsystem:console /profile /map /machine:I386

!ENDIF 

# Begin Target

# Name "cta_vcc - Win32 Release"
# Name "cta_vcc - Win32 Debug"
# Name "cta_vcc - Win32 Profile"
# Begin Group "ana"

# PROP Default_Filter ""
# Begin Source File

SOURCE=.\ana\ana.cpp
# End Source File
# Begin Source File

SOURCE=.\ana\anaBool.h
# End Source File
# Begin Source File

SOURCE=.\ana\anaBoolAnd.h
# End Source File
# Begin Source File

SOURCE=.\ana\anaBoolEmpty.h
# End Source File
# Begin Source File

SOURCE=.\ana\anaBoolEqual.h
# End Source File
# Begin Source File

SOURCE=.\ana\anaBoolInclusion.h
# End Source File
# Begin Source File

SOURCE=.\ana\anaBoolNot.h
# End Source File
# Begin Source File

SOURCE=.\ana\anaBoolOr.h
# End Source File
# Begin Source File

SOURCE=.\ana\anaCmdLineOptions.h
# End Source File
# Begin Source File

SOURCE=.\ana\anaImplemInstr.h
# End Source File
# Begin Source File

SOURCE=.\ana\anaMain.h
# End Source File
# Begin Source File

SOURCE=.\ana\anaObject.h
# End Source File
# Begin Source File

SOURCE=.\ana\anaProgress.cpp
# End Source File
# Begin Source File

SOURCE=.\ana\anaProgress.h
# End Source File
# Begin Source File

SOURCE=.\ana\anaRegDifference.h
# End Source File
# Begin Source File

SOURCE=.\ana\anaRegInitial.h
# End Source File
# Begin Source File

SOURCE=.\ana\anaRegInstr.h
# End Source File
# Begin Source File

SOURCE=.\ana\anaRegInstrAssign.h
# End Source File
# Begin Source File

SOURCE=.\ana\anaRegInstrIf.h
# End Source File
# Begin Source File

SOURCE=.\ana\anaRegInstrPrint.h
# End Source File
# Begin Source File

SOURCE=.\ana\anaRegInstrSeq.h
# End Source File
# Begin Source File

SOURCE=.\ana\anaRegInstrWhile.h
# End Source File
# Begin Source File

SOURCE=.\ana\anaRegIntersect.h
# End Source File
# Begin Source File

SOURCE=.\ana\anaRegion.h
# End Source File
# Begin Source File

SOURCE=.\ana\anaRegionSection.h
# End Source File
# Begin Source File

SOURCE=.\ana\anaRegLinConstraint.h
# End Source File
# Begin Source File

SOURCE=.\ana\anaRegNot.h
# End Source File
# Begin Source File

SOURCE=.\ana\anaRegPost.h
# End Source File
# Begin Source File

SOURCE=.\ana\anaRegPre.h
# End Source File
# Begin Source File

SOURCE=.\ana\anaRegReachBackward.h
# End Source File
# Begin Source File

SOURCE=.\ana\anaRegReachBackwardFixPnt.h
# End Source File
# Begin Source File

SOURCE=.\ana\anaRegReachForward.h
# End Source File
# Begin Source File

SOURCE=.\ana\anaRegReachForwardFixPnt.h
# End Source File
# Begin Source File

SOURCE=.\ana\anaRegState.h
# End Source File
# Begin Source File

SOURCE=.\ana\anaRegSymbol.h
# End Source File
# Begin Source File

SOURCE=.\ana\anaRegUnion.h
# End Source File
# Begin Source File

SOURCE=.\ana\ctaAnalog.h
# End Source File
# Begin Source File

SOURCE=.\ana\ctaAutomaton.cpp
# End Source File
# Begin Source File

SOURCE=.\ana\ctaAutomaton.h
# End Source File
# Begin Source File

SOURCE=.\ana\ctaClock.h
# End Source File
# Begin Source File

SOURCE=.\ana\ctaComponent.cpp
# End Source File
# Begin Source File

SOURCE=.\ana\ctaComponent.h
# End Source File
# Begin Source File

SOURCE=.\ana\ctaConfigAnd.h
# End Source File
# Begin Source File

SOURCE=.\ana\ctaConfigLinRest.h
# End Source File
# Begin Source File

SOURCE=.\ana\ctaConfigOr.h
# End Source File
# Begin Source File

SOURCE=.\ana\ctaConfigState.cpp
# End Source File
# Begin Source File

SOURCE=.\ana\ctaConfigState.h
# End Source File
# Begin Source File

SOURCE=.\ana\ctaConfiguration.cpp
# End Source File
# Begin Source File

SOURCE=.\ana\ctaConfiguration.h
# End Source File
# Begin Source File

SOURCE=.\ana\ctaConstant.h
# End Source File
# Begin Source File

SOURCE=.\ana\ctaContinuous.h
# End Source File
# Begin Source File

SOURCE=.\ana\ctaDiscrete.h
# End Source File
# Begin Source File

SOURCE=.\ana\ctaErrMsg.h
# End Source File
# Begin Source File

SOURCE=.\ana\ctaExprConst.h
# End Source File
# Begin Source File

SOURCE=.\ana\ctaExpression.h
# End Source File
# Begin Source File

SOURCE=.\ana\ctaExprNegation.h
# End Source File
# Begin Source File

SOURCE=.\ana\ctaExprPlus.h
# End Source File
# Begin Source File

SOURCE=.\ana\ctaExprVar.cpp
# End Source File
# Begin Source File

SOURCE=.\ana\ctaExprVar.h
# End Source File
# Begin Source File

SOURCE=.\ana\ctaHyTechPrinter.cpp
# End Source File
# Begin Source File

SOURCE=.\ana\ctaHyTechPrinter.h
# End Source File
# Begin Source File

SOURCE=.\ana\ctaInstantiation.cpp
# End Source File
# Begin Source File

SOURCE=.\ana\ctaInstantiation.h
# End Source File
# Begin Source File

SOURCE=.\ana\ctaMap.h
# End Source File
# Begin Source File

SOURCE=.\ana\ctaModule.cpp
# End Source File
# Begin Source File

SOURCE=.\ana\ctaModule.h
# End Source File
# Begin Source File

SOURCE=.\ana\ctaObject.cpp
# End Source File
# Begin Source File

SOURCE=.\ana\ctaObject.h
# End Source File
# Begin Source File

SOURCE=.\ana\ctaParser.h
# End Source File
# Begin Source File

SOURCE=.\ana\ctaRelOp.h
# End Source File
# Begin Source File

SOURCE=.\ana\ctaRestAnd.h
# End Source File
# Begin Source File

SOURCE=.\ana\ctaRestFalse.h
# End Source File
# Begin Source File

SOURCE=.\ana\ctaRestIdentity.h
# End Source File
# Begin Source File

SOURCE=.\ana\ctaRestNot.h
# End Source File
# Begin Source File

SOURCE=.\ana\ctaRestOr.h
# End Source File
# Begin Source File

SOURCE=.\ana\ctaRestRel.cpp
# End Source File
# Begin Source File

SOURCE=.\ana\ctaRestRel.h
# End Source File
# Begin Source File

SOURCE=.\ana\ctaRestriction.cpp
# End Source File
# Begin Source File

SOURCE=.\ana\ctaRestriction.h
# End Source File
# Begin Source File

SOURCE=.\ana\ctaRestTrue.h
# End Source File
# Begin Source File

SOURCE=.\ana\ctaScanner.h
# End Source File
# Begin Source File

SOURCE=.\ana\ctaSet.h
# End Source File
# Begin Source File

SOURCE=.\ana\ctaSignal.h
# End Source File
# Begin Source File

SOURCE=.\ana\ctaState.cpp
# End Source File
# Begin Source File

SOURCE=.\ana\ctaState.h
# End Source File
# Begin Source File

SOURCE=.\ana\ctaStopWatch.h
# End Source File
# Begin Source File

SOURCE=.\ana\ctaString.cpp
# End Source File
# Begin Source File

SOURCE=.\ana\ctaString.h
# End Source File
# Begin Source File

SOURCE=.\ana\ctaStringGNU.h
# End Source File
# Begin Source File

SOURCE=.\ana\ctaStringSTL.h
# End Source File
# Begin Source File

SOURCE=.\ana\ctaSynchronisation.cpp
# End Source File
# Begin Source File

SOURCE=.\ana\ctaSynchronisation.h
# End Source File
# Begin Source File

SOURCE=.\ana\ctaSystem.cpp
# End Source File
# Begin Source File

SOURCE=.\ana\ctaSystem.h
# End Source File
# Begin Source File

SOURCE=.\ana\ctaTransition.cpp
# End Source File
# Begin Source File

SOURCE=.\ana\ctaTransition.h
# End Source File
# Begin Source File

SOURCE=.\ana\ctaYacc.tab.cc.h
# End Source File
# End Group
# Begin Group "ddm"

# PROP Default_Filter ""
# Begin Source File

SOURCE=.\ddm\ddmAutomaton.cpp
# End Source File
# Begin Source File

SOURCE=.\ddm\ddmAutomaton.h
# End Source File
# Begin Source File

SOURCE=.\ddm\ddmAutomatonOstream.cpp
# End Source File
# Begin Source File

SOURCE=.\ddm\ddmAutomatonOstream.h
# End Source File
# Begin Source File

SOURCE=.\ddm\ddmConfig.cpp
# End Source File
# Begin Source File

SOURCE=.\ddm\ddmConfig.h
# End Source File
# Begin Source File

SOURCE=.\ddm\ddmConfigPair.cpp
# End Source File
# Begin Source File

SOURCE=.\ddm\ddmConfigPair.h
# End Source File
# Begin Source File

SOURCE=.\ddm\ddmConfigPairList.cpp
# End Source File
# Begin Source File

SOURCE=.\ddm\ddmConfigPairList.h
# End Source File
# Begin Source File

SOURCE=.\ddm\ddmConstraints.cpp
# End Source File
# Begin Source File

SOURCE=.\ddm\ddmConstraints.h
# End Source File
# Begin Source File

SOURCE=.\ddm\ddmDualVec.cpp
# End Source File
# Begin Source File

SOURCE=.\ddm\ddmDualVec.h
# End Source File
# Begin Source File

SOURCE=.\ddm\ddmIncidences.cpp
# End Source File
# Begin Source File

SOURCE=.\ddm\ddmIncidences.h
# End Source File
# Begin Source File

SOURCE=.\ddm\ddmIntSet.cpp
# End Source File
# Begin Source File

SOURCE=.\ddm\ddmIntSet.h
# End Source File
# Begin Source File

SOURCE=.\ddm\ddmMat.cpp
# End Source File
# Begin Source File

SOURCE=.\ddm\ddmMat.h
# End Source File
# Begin Source File

SOURCE=.\ddm\ddmNUMBER.h
# End Source File
# Begin Source File

SOURCE=.\ddm\ddmObject.h
# End Source File
# Begin Source File

SOURCE=.\ddm\ddmPoly.cpp
# End Source File
# Begin Source File

SOURCE=.\ddm\ddmPoly.h
# End Source File
# Begin Source File

SOURCE=.\ddm\ddmRays.cpp
# End Source File
# Begin Source File

SOURCE=.\ddm\ddmRays.h
# End Source File
# Begin Source File

SOURCE=.\ddm\ddmRCIPtr.h
# End Source File
# Begin Source File

SOURCE=.\ddm\ddmRCObject.h
# End Source File
# Begin Source File

SOURCE=.\ddm\ddmRCRegion.h
# End Source File
# Begin Source File

SOURCE=.\ddm\ddmRegion.cpp
# End Source File
# Begin Source File

SOURCE=.\ddm\ddmRegion.h
# End Source File
# Begin Source File

SOURCE=.\ddm\ddmState.cpp
# End Source File
# Begin Source File

SOURCE=.\ddm\ddmState.h
# End Source File
# Begin Source File

SOURCE=.\ddm\ddmTraces.cpp
# End Source File
# Begin Source File

SOURCE=.\ddm\ddmTraces.h
# End Source File
# Begin Source File

SOURCE=.\ddm\ddmTransition.cpp
# End Source File
# Begin Source File

SOURCE=.\ddm\ddmTransition.h
# End Source File
# Begin Source File

SOURCE=.\ddm\ddmVec.cpp
# End Source File
# Begin Source File

SOURCE=.\ddm\ddmVec.h
# End Source File
# Begin Source File

SOURCE=.\ddm\ddmXPoly.cpp
# End Source File
# Begin Source File

SOURCE=.\ddm\ddmXPoly.h
# End Source File
# End Group
# End Target
# End Project

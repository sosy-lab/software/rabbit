/*
 * File:        ctaString.cpp
 * Purpose:     String Class for cta-library
 * Author:      Dirk Beyer
 * Created:     1998
 * Copyright:   (c) 1998, University of Brandenburg, Cottbus
 */

#include "ctaString.h"

const ctaString*
ctaString::mEmptyString = new ctaString("");

/*
 * File:        ctaConfiguration.cpp
 * Purpose:     Configuration Class for cta-library
 * Author:      Dirk Beyer
 * Created:     1998
 * Copyright:   (c) 1998, University of Brandenburg, Cottbus
 */

#include "ctaConfiguration.h"

#include "ctaConfigAnd.h"
#include "ctaConfigOr.h"
#include "ctaConfigLinRest.h"
 
bool
ctaConfiguration::IsTrue() const
{
  const ctaConfigLinRest* tmp = dynamic_cast<const ctaConfigLinRest*>(this);
  
  return tmp && tmp->GetRestriction()->IsTrue();
}

bool
ctaConfiguration::IsFalse() const
{
  const ctaConfigLinRest* tmp = dynamic_cast<const ctaConfigLinRest*>(this);
  
  return tmp && tmp->GetRestriction()->IsFalse();
}

ctaConfiguration*
ctaConfiguration::mkAnd(ctaConfiguration* p1, ctaConfiguration* p2)
{
  ctaConfiguration* result;

  if(p1->IsFalse())
  {
    delete p2;
    result = p1;
  }
  else if(p2->IsFalse())
  {
    delete p1;
    result = p2;
  }
  else if(p1->IsTrue())
  {
    delete p1;
    result = p2;
  }
  else if(p2->IsTrue())
  {
    delete p2;
    result = p1;
  }
  else
  {
    result = new ctaConfigAnd(p1,p2);
  }

  return result;
}

ctaConfiguration*
ctaConfiguration::mkOr(ctaConfiguration* p1, ctaConfiguration* p2)
{
  ctaConfiguration* result;

  if(p1->IsTrue())
  {
    delete p2;
    result = p1;
  }
  else if(p2->IsTrue())
  {
    delete p1;
    result = p2;
  }
  else if(p1->IsFalse())
  {
    delete p1;
    result = p2;
  }
  else if(p2->IsFalse())
  {
    delete p2;
    result = p1;
  }
  else
  {
    result = new ctaConfigOr(p1,p2);
  }

  return result;
}



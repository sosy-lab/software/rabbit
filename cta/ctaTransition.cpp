/*
 * File:        ctaTransition.cpp
 * Purpose:     Transition Class for cta-library
 * Author:      Dirk Beyer
 * Created:     1998
 * Copyright:   (c) 1998, University of Brandenburg, Cottbus
 */

#include "ctaTransition.h"

#include "ctaAutomaton.h"
#include "ctaErrMsg.h"
#include "ctaModule.h"
#include "ctaState.h"

#include "ddmAutomaton.h"
#include "ddmRegion.h"
#include "ddmTransition.h"

#include "bddExpression.h"

ctaTransition::ctaTransition(ctaString* pFollowerState,
                             ctaRestriction* pGuard,
                             ctaSynchronisation* pSync,
                             ctaRestriction* pAllow)
  : mFollowerState(pFollowerState),
    mGuard(pGuard),
    mAllow(pAllow),
    mSync(pSync)
{}

void 
ctaTransition::CheckConsistency(ctaModule* m, ctaAutomaton* a)
{
  // Follower state has to be declared.
  // Try to find idx-th follower state in set of states of automaton (a).
  if(a->GetStates()->find(*mFollowerState) == a->GetStates()->end())
  {
    CTAERR << "State \'"
       << *mFollowerState << "\' undeclared." 
       << endl
       << "in module " << m->GetName() << "."
       << endl;
    CTAPUT;
  }
  
  // guard
  if(GetGuard())
  {
    GetGuard()->CheckConsistency(m);
  }

  // right use of signals
  GetSync()->CheckConsistency(m);

  // checking assignments
  if(GetAllow())
  {
    GetAllow()->CheckConsistency(m); 
  }
}

void 
ctaTransition::collectInterfacesForCommunicationGraph(
                     ctaSet<ctaString>* lUsedInterfaces)
{
  mGuard->collectInterfacesForCommunicationGraph(lUsedInterfaces);
  mAllow->collectInterfacesForCommunicationGraph(lUsedInterfaces);
  mSync->collectInterfacesForCommunicationGraph(lUsedInterfaces);
}

ddmTransition*
ctaTransition::mkDdmTransition(const ddmAutomaton& pAuto,
                   const ctaString& pOriginState,
                   const ctaString& pTargetState,
                   const ctaModule* pCtaModule)
{
  // Compute number of origin state.
  int lOriginNum 
  = 
  find(pAuto.getStateIds().begin(),
       pAuto.getStateIds().end(),
       pOriginState) 
  - pAuto.getStateIds().begin();
  
  // Compute number of destination state.
  int lTargetNum 
  = 
  find(pAuto.getStateIds().begin(),
       pAuto.getStateIds().end(),
       pTargetState)
  - pAuto.getStateIds().begin();

  // Compute number of synchronization label.
  int lTransNum 
  = 
  find(pAuto.getSyncIds().begin(),
       pAuto.getSyncIds().end(),
       *this->GetSync()->GetSignalName())
  - pAuto.getSyncIds().begin();

  // Sync not found -> no sync.
  if(lTransNum == (int) pAuto.getSyncIds().size())
  {
    lTransNum = -1;
  }

  // Build a region for allowed changes.
  ddmRegion lAllowedChanges(this
                ->GetAllow()
                ->mkDdmRegion(pAuto, true, pCtaModule));
  
  // We have to collect the id numbers of the variables
  // which are not mentioned primed in the ALLOW clause.

  ctaIntSet lUnmentionedVars;

  ctaForAll_Map(pCtaModule->GetInterface(), idx, ctaComponent*)
  {
    if( (idx->second->GetDataType() != ctaComponent::SYNC )
    && ( idx->second->GetDataType() != ctaComponent::CONST )
    && ( !( this->GetAllow()
        ->VariableOccursTicked( idx->second->GetName() ) ) ) )
    {
      // (idx->second) is a variable (and not a constant)
      // and does not occur ticked in ALLOW.
      // We have to compute its id number and add it to (lUnmentionedVars).
      vector<string>::const_iterator lIt;
      // We search for the element in the (getVarId) vector ...
      lIt = find(pAuto.getVarIds().begin(), pAuto.getVarIds().end(),
         string(*(idx->second->GetName())));

      // ... and compute its index.
      lUnmentionedVars.insert(lIt - pAuto.getVarIds().begin());
    }
  }

  // Build the region for the guard ...
  ddmRegion lGuard(this
         ->GetGuard()
         ->mkDdmRegion(pAuto,false,pCtaModule));
  // ... and prepare it for intersection with assignment.
  lGuard.addDimensions(lGuard.getDim(), lGuard.getDim());

  // Now we build the resulting guarded assignment.
  lAllowedChanges.intersect(lGuard);

  // Create the transition.
  // lTransNum is the number of the synchronisation label.
  return new ddmTransition(lOriginNum, 
               lTargetNum,
               lAllowedChanges,
               lUnmentionedVars,
               lTransNum);
}

bddTransition*
ctaTransition::mkBddTransition(const ctaString* pOriginState,
                               const ctaString* pTargetState,
                               const bddSymTab* pSymTab,
                               const ctaModule* pCtaModule,
                               const ctaString* pParentAutomatonName)
{
  // Initialize (lAllowed) with the state change.
  bddConfig lAllowed = bddConfig::mkState
    (pSymTab, pParentAutomatonName, pOriginState, false);
  lAllowed.intersect(bddConfig::mkState
    (pSymTab, pParentAutomatonName, pTargetState, true));
  
  // Intersect (lAllowed) with the allowed variable changes and the guard.
  lAllowed.intersect(GetAllow()->mkBddConfig(pSymTab, pCtaModule));
  lAllowed.intersect(GetGuard()->mkBddConfig(pSymTab, pCtaModule));

  // Initialize (lInitiated) as if (this) transition changed nothing.
  bddConfig lInitiated = bddConfig::mkInitiated(pSymTab);
  // Remove state of (mParentAutomaton) from (lInitiated).
  lInitiated.existsAuto(pParentAutomatonName, false);
  lInitiated.existsAuto(pParentAutomatonName, true);

  // Remove from (lInitiated) all variables 
  //   which occur ticked in the ALLOW clause of (this).
  ctaForAll_Map(pCtaModule->GetInterface(), idx, ctaComponent*)
  {
    if((idx->second->GetDataType() != ctaComponent::SYNC)
       && (idx->second->GetDataType() != ctaComponent::CONST)
       && GetAllow()->VariableOccursTicked(idx->second->GetName()))
    {
      // (idx->second) is a variable (and not a constant) 
      //    and occurs ticked in ALLOW.
      lInitiated.existsVar(idx->second->GetName(), false);
      lInitiated.existsVar(idx->second->GetName(), true);
    }
  }

  unsigned lSyncNum = pSymTab->getSyncNum(*this->GetSync()->GetSignalName());

  return new bddTransition(lAllowed, lInitiated, lSyncNum);
}

void 
ctaTransition::print(ostream& s) const
{
  s << "      TRANS";

  s << endl << "      {" << endl;

  // Guard
  if(mGuard)
  {
    s << "        GUARD" << endl;
    s << "          " << mGuard << ";" << endl;
  }

  // Synchronisation
  s << mSync;
  
  // Allowed changes.
  if(mAllow)
  {
    s << "        DO" << endl;
    s << "          " << mAllow << ";" << endl;
  }

  // Follower States.
  s << "        GOTO" << " " << *mFollowerState << ";" << endl;

  s << "      }" << endl;
}

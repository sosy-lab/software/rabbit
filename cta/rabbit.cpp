//-*-Mode: C++;-*-
 
/*
 * File:        rabbit.cpp
 * Purpose:     Main Module for the CTA library.
 * Author:      Dirk Beyer
 * Created:     1999
 * Copyright:   (c) 1999-2001, University of Brandenburg, Cottbus
 */

#include <fstream>
#include <string>

#include "ctaErrMsg.h"
#include "ctaInstantiation.h"
//#include "ctaSet.h"
//#include "ctaMap.h"
#include "ctaString.h"
#include "ctaSystem.h"
#include "ctaParser.h"


#include "utilCmdLineOptions.h"
#include "ctaRestTrue.h"
#include "ctaRestFalse.h"

#include "bddBdd.h"

/*
map<string, cXPoly*> pubExpMap;
int pubExpCount;
*/

// The syntax tree of the whole system.
ctaSystem* pubParsedSystem;

// the parser, coming from the CTA library 
ctaParser* pubParser;

// For nice error output and error counting.
ctaErrMsg* pubErrMsg;

utilCmdLineOptions* pubCmdLineOpt;

// the main program
int main(int argc, char *argv []) 
{
  pubCmdLineOpt = new utilCmdLineOptions(argc,argv);

  // create instances of important classes
  pubParser = new ctaParser(new ctaScanner());
  pubErrMsg = new ctaErrMsg(cerr);

  pubParser->GetScanner()->SetStream(pubCmdLineOpt->InputStream());
  
  // Set no dots and dollars allowed in identifiers.
  pubParser->GetScanner()->SetAllowDotAndDollars(false);

  pubErrMsg->SetPrefix(new ctaString("Parsing: "));

  // Pass 1, both module definitions and analysis section are parsed.
  // What if parsing was not successful?
  if( ! pubParser->Parse() )
  {
    return(1);
  }

  delete pubErrMsg->GetPrefix();
  pubErrMsg->SetPrefix(NULL);

  if (pubCmdLineOpt->Verbose())
  {
    cerr << "Parsing finished." << endl;
  }

  // Context check as well as information about 
  //   top modules and observer modules.
  pubParsedSystem->PrepareCtaModel();

  // The final task.
  if( pubCmdLineOpt->HytechPrinting() )
  {
    pubParsedSystem->PrintHyTech( pubCmdLineOpt->GetModule(), 
    				  pubCmdLineOpt->GetOutputStream());
  }
  else
  {
    pubParsedSystem->ExecuteAnalysis();
  }

  if( pubCmdLineOpt->Verbose() ) 
  {
    cerr << "All tasks finished." << endl;

    cerr << "Error count: " << pubErrMsg->GetErrCount() << endl;

    cerr << "Deallocation of dynamic variables.\n";
  }
  
  // Deallocation of memory for parsed system.
  delete pubParsedSystem;

  delete pubParser;
  delete pubErrMsg;
  
  // Deallocation of static attributes of some classes.
  ctaString::CleanUp();
  
  if( pubCmdLineOpt->GetObjCount() != 3 )
  {
    // For controlling deallocation.
    cerr << "######################################################"
	 << endl
	 << "Warning: We still have " << pubCmdLineOpt->GetObjCount()
         << " ctaObjects after deallocation (should be 3)." << endl;
  }
  
  //cout << pubExpCount << endl;
  //cout << pubExpMap.size() << endl;
  
  delete pubCmdLineOpt;

  return(0);
};

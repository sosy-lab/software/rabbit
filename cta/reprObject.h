//-*-Mode: C++;-*-

/*
 * File:        reprObject.h
 * Purpose:     Top Level Object for DDM and BDD libraries
 * Author:      Dirk Beyer
 * Created:     1999
 * Copyright:   (c) 1999, University of Brandenburg, Cottbus
 */

#ifndef _reprObject_h
#define _reprObject_h

#include "ctaObject.h"

// This is the base class for any other class in the analysis library.
class reprObject : private ctaObject
{};

#endif

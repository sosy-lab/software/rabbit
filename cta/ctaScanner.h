//-*-Mode: C++;-*-

/*
 * File:        ctaScanner.h
 * Purpose:     Scanner Class for cta-library
 * Author:      Dirk Beyer
 * Created:     1998
 * Copyright:   (c) 1998, University of Brandenburg, Cottbus
 */

#ifndef _ctaScanner_h
#define _ctaScanner_h


#include "FlexLexer.h"

class ctaScanner : public yyFlexLexer, private ctaObject
{
private:
  // If mAllowDotAndDollars true, then dots and dollars are allowed in identifiers.
  bool mAllowDotAndDollars;

  // It should be not allowed to use the standard operators.
  ctaScanner(const ctaScanner&);
  void operator=(const ctaScanner&);
  void operator,(const ctaScanner&);

public:
  // Constructor and destructor.
  ctaScanner()
    : yyFlexLexer()
  {}
  
  // Function to set mAllowDotAndDollars.
  void SetAllowDotAndDollars(bool pAllowDotAndDollars)
  {
    mAllowDotAndDollars = pAllowDotAndDollars;
  }

  // Function to get the value of mAllowDotAndDollars.
  bool GetAllowDotAndDollars() const
  {
    return mAllowDotAndDollars;
  }

  void
  SetStream(istream* pInstream)
  {
    yyin = pInstream;
  }
	
  // from yyFlexLexer we use the following methods
  // const char* YYText();
  // virtual int yylex();
};

#endif

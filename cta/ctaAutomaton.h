//-*-Mode: C++;-*-

/*
 * File:        ctaAutomaton.h
 * Purpose:     Automaton Class for cta-library
 * Author:      Dirk Beyer
 * Created:     1998
 * Copyright:   (c) 1998, University of Brandenburg, Cottbus
 */

#ifndef _ctaAutomaton_h
#define _ctaAutomaton_h


#include "ctaString.h"
#include <vector>

class ctaAutomaton;
#include "ctaObject.h"
#include "ctaMap.h"
class ctaModule;
#include "ctaState.h"

#include "ddmAutomaton.h"
#include "bddAutomaton.h"
#include "bddSymTab.h"

class ctaAutomaton : private ctaObject
{
private:
  // Aggregated.
  ctaString* mName;
  ctaMap<ctaState*>* mStates;

  // It should be not allowed to use the standard operators.
  ctaAutomaton(const ctaAutomaton&);
  void operator=(const ctaAutomaton&);
  void operator,(const ctaAutomaton&);

public:
  // constructor and destructor
  ctaAutomaton(ctaString* pName,
	       ctaMap<ctaState*>* pStates)
    : mName(pName),
      mStates(pStates)
  {}

  ~ctaAutomaton()
  {
    if(!mName || !mStates)
    {
      cerr << "Runtime error: Component pointer in ctaAutomaton is NULL!"
	   << endl;
      exit(1);
    }
    delete mName;
    delete mStates;
  }

  unsigned int
  getNumberOfStates()
  {
    return mStates->size();
  }

  ctaString*
  GetName() const
  {
    return mName;
  }

  ctaMap<ctaState*>* 
  GetStates() const
  {
    return(mStates);
  }

  ctaAutomaton*
  cloneSetPrefixAndIdent(const ctaString *const pPrefix,
			 ctaMap<ctaString*>* pIdentNames) const;
  
  // Service methods.
  // This method is for context analysis.
  // We give the parent module for accessing module features.

  bool 
  CheckNonStrictness(const ctaModule* pCtaModule) const;
  
  bool 
  CheckBddConstraint(const ctaModule* pCtaModule) const;

  void 
  CheckConsistency(ctaModule*);

  void 
  CountRestrictions(ctaComponent* pCompo,
		    int& pNrMultrestRestrictions,
		    int& pNrOutputRestrictions);

  void 
  mkDdmCompletions(ctaModule* pModule);

  void 
  mkInputErrorStateCompletions(ctaModule* pModule);

  // Construct a DDM-automaton for (this), using the given
  //   symbol table.  First component has list of sync IDs, 
  //   second of variable IDs.
  ddmAutomaton*
  mkDdmAutomaton(const pair<set<ctaString>, set<ctaString> >& pSymtab,
		 const ctaModule* pCtaModule) const;

  // Construct a BDD-automaton for (this).
  // Insert state names of (this) automaton into pSymTab.
  // pSymTab has to be complete up to the names of the states.
  // pProduct has to be true iff the constructed automaton will be
  //   the basis of the product construction (see bddConfig::mkProduct).
  bddAutomaton*
  mkBddAutomaton(bddSymTab* pBddSymTab,
		 bool pProduct,
		 const ctaModule* pCtaModule) const;

  // aheinig 2000-11-02
  // Collect all used signals and variables for this automaton.
  void 
  collectInterfacesForCommunicationGraph(ctaSet<ctaString>* lUsedInterfaces);

};

ostream& operator << (ostream &, const ctaAutomaton*);

#endif

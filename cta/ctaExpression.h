//-*-Mode: C++;-*-

/*
 * File:        ctaExpression.h
 * Purpose:     Expression Class for cta-library
 * Author:      Dirk Beyer
 * Created:     1998
 * Copyright:   (c) 1998, University of Brandenburg, Cottbus
 */

#ifndef _ctaExpression_h
#define _ctaExpression_h


class ctaComponent;
#include "ctaMap.h"
class ctaModule;
#include "ctaObject.h"
#include "ctaString.h"

class reprAutomaton;
#include "ddmVec.h"

class bddAutomaton;
#include "bddExpression.h"

#include "ctaModule.h"

class ctaExpression : private ctaObject 
{
private:
  // It should be not allowed to use the standard operators.
  ctaExpression(const ctaExpression&);
  void operator=(const ctaExpression&);
  void operator,(const ctaExpression&);
  
public:
  ctaExpression()
  {}

  // accessor methods

  // Service methods.

  // Clone function for all derivated classes
  virtual ctaExpression*
  cloneSetPrefixAndIdent(const ctaString *const pPrefix,
			 ctaMap<ctaString*>* pIdentNames) const = 0;  
  // Clone this.
  ctaExpression*
  clone() const
  {
    ctaString* lDummyPrefix = new ctaString("");
    ctaExpression* theClone = this->cloneSetPrefixAndIdent(lDummyPrefix,
							   NULL);
    delete lDummyPrefix;
    return theClone;
  }
  
  // Check for const expressions.
  // It will be used to check the derivations.
  virtual void 
  CheckDerivation(const ctaModule* const pModule) const = 0;

  // This method is for context analysis.
  // We give the parent module for accessing module features.
  virtual void
  CheckConsistency(const ctaModule* const pModule) = 0;

  virtual bool 
  VariableOccurs(ctaString *pId) = 0;

  virtual bool
  VariableOccursTicked(ctaString *pId) = 0;
 
  virtual bool 
  CheckBddConstraint() const = 0;

  virtual int 
  CountVariables(const ctaModule* pCtaModule) const = 0;

  virtual int
  CountClocks(const ctaModule* pCtaModule) const = 0;

  virtual void
  Print(ostream&) const = 0;

  // This method we overwrite in subclass (ctaExprVar) with
  // code for special handling for the "d"
  // to sign a derivative of a variable in HyTech.
  virtual void 
  PrintHyTech(ostream& pOutStream) const
  {
    this->Print(pOutStream);
  }

  // aheinig 2000-11-02
  // Collect all used signals and variables for this automaton.
  virtual void 
  collectInterfacesForCommunicationGraph(ctaSet<ctaString>* 
					               lUsedInterfaces) = 0;

  // Construct a vector representing (*this) >= 0.
  virtual ddmVec
  mkDdmVec(const reprAutomaton& pAuto,
	   bool pDoubleSize,
	   const ctaModule* pCtaModule) = 0;

  virtual bddExpression*
  mkBddExpression (const ctaModule* pCtaModule) = 0;
};

inline ostream& 
operator << (ostream& out, const ctaExpression* obj)
{
  obj->Print(out);
  return(out);
}

#endif

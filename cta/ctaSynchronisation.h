//-*-Mode: C++;-*-

/*
 * File:        ctaSynchronisation.h
 * Purpose:     Synchronisation Class for cta-library
 * Author:      Dirk Beyer
 * Created:     1998
 * Copyright:   (c) 1998, University of Brandenburg, Cottbus
 */

#ifndef _ctaSynchronisation_h
#define _ctaSynchronisation_h

#include <vector>

#include "ctaComponent.h"
#include "ctaMap.h"
#include "ctaSet.h"
class ctaModule;
#include "ctaString.h"
class ctaSynchronisation;
#include "ctaObject.h"

class ctaSynchronisation : private ctaObject
{
private:
  // All aggregated.
  ctaString* 		signalName;
  ctaString* 		signalType;
  
  // internal service methods
  bool CheckSignalName(const ctaMap<ctaComponent*>*, const ctaString*) const;
  void CheckSignalType(const ctaComponent*, const ctaString*) const;

  // It should be not allowed to use the standard operators.
  ctaSynchronisation(const ctaSynchronisation&);
  void operator=(const ctaSynchronisation&);
  void operator,(const ctaSynchronisation&);

public:
  // constructor and destructor
  ctaSynchronisation();

  ctaSynchronisation(ctaString* pSignalName, ctaString* pSignalType);

  ~ctaSynchronisation();
  
  // accessor methods
  
  void SetSignalName(ctaString* s)
  {
    delete signalName;
    signalName = s;
  }

  ctaString* GetSignalName() const
  {
    return(signalName);
  }
  
  void SetSignalType(ctaString* s)
  {
    delete signalType;
    signalType = s;
  }
  
  ctaString* GetSignalType() const
  {
    return(signalType);
  }
  
  // aheinig 15.03.2000
  // Clone (this).
  ctaSynchronisation* 
  cloneSetPrefixAndIdent(const ctaString *const pPrefix,
			 ctaMap<ctaString*>* pIdentNames) const
  {        
    ctaSynchronisation* lSyncClone = new ctaSynchronisation();
    
    // If the signalName empty we have no signal at the coresponding 
    //  transition, and so the clone must also be empty.  
    if( *signalName != "" )
    {
      lSyncClone->SetSignalName(new ctaString(*pPrefix + *signalName));
      if(pIdentNames != NULL)
      {
	// Replace the variable name by the name defined in the instantiation.
	ctaForAll_Map(pIdentNames, itIdent, ctaString*)
	{
	  if( itIdent->first == *signalName )
	  {
	    lSyncClone->SetSignalName(new ctaString( *itIdent->second));
	  }
	}
      }

      lSyncClone->SetSignalType(new ctaString(*signalType));
    }
    return lSyncClone;
  } 

  // aheinig 2000-11-02
  // Collect all used signals and variables for this automaton.
  void 
  collectInterfacesForCommunicationGraph(ctaSet<ctaString>* lUsedInterfaces)
  {
    if( *signalName != "" )
    {
      lUsedInterfaces->insert(*signalName);
    }
  }
  
  // Service methods.
  // This method is for context analysis.
  // We give the parent module for accessing module features.
  void
  CheckConsistency(ctaModule*);
};

ostream& operator << (ostream&, const ctaSynchronisation*);

#endif

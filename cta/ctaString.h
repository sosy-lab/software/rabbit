//-*-Mode: C++;-*-

/*
 * File:        ctaString.h
 * Purpose:     String Class for cta-library
 * Author:      Dirk Beyer
 * Created:     1998
 * Copyright:   (c) 1998, University of Brandenburg, Cottbus
 */

#ifndef _ctaString_h
#define _ctaString_h


#include <string>
//#include "ctaStringSTL.h"
//#include "ctaStringGNU.h" // Gnu.

#include "ctaObject.h"

// typedef does not work because of use of polymorphism.
// typedef ctaStringSTL ctaString;

class ctaString : public string, ctaObject //ctaStringSTL
{
public: // Static attributes.
  static const ctaString* mEmptyString;
  
public: // Static methods.
  // To be called at the end of main program.
  static void CleanUp()
  {
    delete mEmptyString;
  }

public:
  // Constructor and destructor.
  explicit ctaString()
    : string("")
    {
      cerr << "Runtime error: Constructor ctaString() used, but "
	   << "this Constructor is not implemented."
	   << endl;
    }
  
  explicit ctaString(const string &s)
    : string(s)
    //    : ctaStringSTL(s)
    {
      // cout << "ctaString(\"" << s << "\") called." << endl;
    }

  explicit ctaString(string &s)
    : string(s)
    //    : ctaStringSTL(s)
    {
      // cout << "ctaString(\"" << s << "\") called." << endl;
    }
  
  explicit ctaString(string& pStr, 
		     unsigned int& pStart, 
		     unsigned int pLength)
    : string(pStr, pStart, pLength)
    //    : ctaStringSTL(pStr, pStart, pLength)
    {}
    
  ~ctaString()
  {
    // cout << "ctaString " << *this << " deleted." << endl;
  }

  // Replaces in (ctaString) the char px1 thru px2.
  ctaString ctaString::ReplaceChars(const char px1, const char px2) const
  {
    ctaString lString(*this);
    for( unsigned int li = 0 ; li < lString.length(); ++li)
    {
      if (lString[li]==px1)
      {
	lString[li]=px2;
      }
    }
    return lString;
  }
};


// I have to define it because we have a Pointer to a String.
inline ostream& 
operator << (ostream& out, const ctaString* s)
{
  if(s == NULL)
  {
    cerr << "Runtime error: Null pointer used for a ctaString in " << endl
	 << "ostream& operator << (ostream&, const ctaString*) !!!" << endl;
  }

  // We use the operator of the base class.
  out << *s;

  return(out);
}


#endif

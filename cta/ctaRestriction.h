//-*-Mode: C++;-*-

/*
 * File:        ctaRestriction.h
 * Purpose:     Restriction Class for cta-library
 * Author:      Dirk Beyer
 * Created:     1998
 * Copyright:   (c) 1998, University of Brandenburg, Cottbus
 */

#ifndef _ctaRestriction_h
#define _ctaRestriction_h


class ctaAutomaton;
class ctaComponent;
#include "ctaMap.h"
class ctaModule;
#include "ctaObject.h"
#include "ctaString.h"

#include "reprAutomaton.h"
#include "ddmRegion.h"
#include "bddSymTab.h"
#include "bddConfig.h"
#include "ctaModule.h"

class ctaRestriction : private ctaObject
{
private:
  // It should be not allowed to use the standard operators.
  ctaRestriction(const ctaRestriction&);
  void operator=(const ctaRestriction&);
  void operator,(const ctaRestriction&);

public:
  // constructor and destructor
  ctaRestriction()
  {}

  ~ctaRestriction()
  {}
  
  // Service methods.

  // This is the virtual function for cloning all object
  //   from all derivated classes.
  virtual ctaRestriction*
  cloneSetPrefixAndIdent(const ctaString *const pPrefix,
			 ctaMap<ctaString*>* pIdentNames) const = 0;

  // Clone this.
  ctaRestriction*
  clone() const;

  // Check the derivation for correctness.
  virtual void
  CheckDerivation(const ctaModule* const pModule) const = 0;

  // This method is for context analysis.
  // We give the parent module for accessing module features.
  virtual void 
  CheckConsistency(const ctaModule* const pModule) = 0;

  virtual bool 
  CheckNonStrictness(const ctaModule* pCtaModule) const = 0;

  virtual bool 
  VariableOccurs(ctaString *pId) = 0;

  virtual bool 
  VariableOccursTicked(ctaString *pId) = 0;

  virtual bool
  CheckBddConstraint(const ctaModule* pCtaModule, 
		     const bool logicalOrIsAllowed ) const = 0;

  virtual void 
  Print(ostream&) const = 0;

  // This method is for the HyTech output.
  virtual void 
  PrintHyTech(ostream& pOutStream) const = 0;

  bool
  IsTrue() const;
  
  bool
  IsFalse() const;
  
  static ctaRestriction*
  mkNot(ctaRestriction* p1);

  static ctaRestriction*
  mkAnd(ctaRestriction* p1, ctaRestriction* p2);
  
  static ctaRestriction*
  mkOr(ctaRestriction* p1, ctaRestriction* p2);

  // aheinig 2000-11-02
  // Collect all used signals and variables for this automaton.
  virtual void 
  collectInterfacesForCommunicationGraph(ctaSet<ctaString>* 
					             lUsedInterfaces) = 0; 
 
  // Set pDoubleSize to (true) if ticked and unticked may be used.
  virtual ddmRegion
  mkDdmRegion(const reprAutomaton& pAuto,
	      bool pDoubleSize,
	      const ctaModule* pCtaModule) = 0;

  virtual bddConfig
  mkBddConfig(const bddSymTab* pSymTab,
	      const ctaModule* pCtaModule) = 0;
};

ostream& operator << (ostream& out, const ctaRestriction* obj);

#endif

/*
 * File:        ctaSystem.cpp
 * Purpose:     System Class for cta-library
 * Author:      Dirk Beyer
 * Created:     1998
 * Copyright:   (c) 1998, University of Brandenburg, Cottbus
 */

#include <fstream>

#include "ctaSystem.h"
#include "ctaModule.h"
#include "ctaInstantiation.h"
#include "ctaErrMsg.h"
#include "ctaHyTechPrinter.h"
#include "ctaParser.h"

#include "reprAutomaton.h"
#include "ddmAutomaton.h"
#include "bddAutomaton.h"

#include "utilCmdLineOptions.h"

//==========================================================
ctaSystem::~ctaSystem()
{
  // Delete the modules.
  for(vector<ctaModule*>::iterator
      it = mModules->begin();
      it != mModules->end();
      ++it)
  {
    delete *it;
  }
  delete mModules;
  
  // Delete analysis sections.
  for(vector<anaSection*>::iterator
      it = mAnaSections->begin();
      it != mAnaSections->end();
      ++it)
  {
    delete *it;
  }
  delete mAnaSections;
}

//===========================================================
// After parsing we have to do several checks and transformations.
// This method collect the neccessary calls.
void 
ctaSystem::PrepareCtaModel()
{
  if (pubCmdLineOpt->Verbose()) 
  {
    // For observation of deallocation.
    cout << "We have " << pubCmdLineOpt->GetObjCount();
    cout << " ctaObjects after parsing." << endl;
  }

  // Context checks for CTA-modules.
  pubErrMsg->SetPrefix(new ctaString("Context check: "));
  this->CheckConsistency();
  delete pubErrMsg->GetPrefix();
  pubErrMsg->SetPrefix(NULL);

  if (pubCmdLineOpt->Verbose()) 
  {
    cout << "Context check finished." << endl;
  
    // For observation of deallocation.
    cout << "We have " << pubCmdLineOpt->GetObjCount();
    cout << " ctaObjects after context check." << endl;
  }
    
  // Output is allowed only after context check

  if (pubCmdLineOpt->Verbose()) 
  {
    set<ctaString>* lTopModules = this->GetTopModules();

    // Print the top module names for verbose option.
    cout << "===============================" << endl; 
    cout << "The top modules are the following:" << endl;
    
    ctaForAll_Set(lTopModules, it, ctaString)
    {
      cout << *it << endl;
    }
    delete lTopModules;

    cout << "===============================" << endl; 
    this->PrintObserverModules();
    cout << "===============================" << endl; 
  }
}

//==========================================================
void 
ctaSystem::CheckConsistency()
{
  // mvogel 2001-04-27: Checks for empty input file problem. 
  // If there are no module and no analysis section then exit.
  if( (this->GetModules()->size() == 0)
      && (this->GetAnalysis()->size() == 0))
  {
    CTAERR << "Empty input file. The file has to contain at least one module"
	   << endl << "or an analysis section." << endl;
    CTAPUT;
    exit(1);
  }

  // Here we check for cyclic instantiations.
  {
    map<ctaString,set<ctaString>*>* tmpMap 
      = new map<ctaString,set<ctaString>*>();
  	  
    // Here we generate f1(x) for all modules.
    for(vector<ctaModule*>::iterator
	  it =  this->GetModules()->begin();
	it != this->GetModules()->end();
	it++)
    {
      // Create an entry with empty set for the current module
      // in both maps
      ctaString lModuleName = *( (*it)->GetName() );
      (*tmpMap)[lModuleName] = new set<ctaString>();

      //fill the set
      for(ctaMap<ctaInstantiation*>::iterator
	  it1 = (*it)->GetInstantiations()->begin();
	  it1 != (*it)->GetInstantiations()->end();
	  ++it1)
      {
	(*tmpMap)[lModuleName]
	  ->insert( *( it1->second->GetTemplateName() ) );
      }
    }

    // Now generate f2(x) f4(x) f8(x) ... and check for cyclicity.
    set<ctaString>* tmpSet = new set<ctaString>();

    // At the end of the following loop this variable signals that the fixpoint    //   has been reached.
    bool lFixReached = false;

    do{
      for(map<ctaString,set<ctaString>*>::iterator
	    it = tmpMap->begin();
	  it != tmpMap->end();
	  it++)
      {
	// Clear the entire tmpSet.
	tmpSet->clear();

	// A loop trough the set to evaluate the f(x) for each x in the set.
	for(set<ctaString>::iterator it1 = it->second->begin();
	    it1 != it->second->end();
	    it1++)
	{
	  // Evaluate f(x) - value is a set.
	  if (tmpMap->find(*it1) != tmpMap->end())
	  {
	    set<ctaString>* lSet = (*tmpMap)[*it1];
	    // Insert set (f(x)) to tmpSet.

	    for(set<ctaString>::const_iterator it20 = lSet->begin(); 
		it20 != lSet->end(); 
		++it20)
	    {
	      tmpSet->insert(*it20);
	    }

	    /* To increase performance use set operations (union or merge). 
	       set_union(modules->begin(), modules->end(),
	       subModules->begin(), subModules->end(),
	       inserter(*topModules, topModules->begin()));
	    */
	  }
	  else
	  {
	    CTAERR << "Error in check-cyclic-instantiation";
	    CTAERR << endl << "Instantiated module " << *it1 
		   << " not found." << endl;
	    CTAPUT;
	  }
	}
	// Save the old size.
	unsigned int lOldSetSize = it->second->size();

	// Unite the old set with the new f(old set).
	// Bad performance: Use set operations.
	for(set<ctaString>::const_iterator it21 = tmpSet->begin(); 
	    it21 != tmpSet->end(); 
	    ++it21)
	{
	  it->second->insert(*it21);
	}

	// Test if fixpoint for this module reached.
	if(it->second->size() != lOldSetSize) 
	{
	  lFixReached = false;
	}
	else
	{
	  lFixReached = true;
	}
      }
    }while(!lFixReached);

    tmpSet->clear();
    delete tmpSet;

    for(map<ctaString,set<ctaString>*>::iterator
	  it22 = tmpMap->begin();
	it22 != tmpMap->end();
	it22++)
    {
      // Check the module for cyclicity.
      if (it22->second->find( it22->first ) != it22->second->end())
      {
	CTAERR << "Cyclic instantiation of module \"" << it22->first 
	       << "\" found." << endl;
	CTAPUT;
      }
    }	

    // Deallocation of the sets.
    for(map<ctaString,set<ctaString>*>::iterator
	it23 = tmpMap->begin();
	it23 != tmpMap->end();
	it23++)
    {
      // Deallocate the set.
      delete it23->second;
    }
    
    delete tmpMap;
  }

  // We check if there are two modules with same name:
  // Adding all module names as keys into a temporary ctaMap.
  // The ctaMap is checking for same key elements and
  // produce an error message if needed.

  // A temporary map.
  // We use a cta map because of the error message produced by ctaMap::append if 
   // the key was tried to insert secondly.
  ctaMap<char*>* testMap = new ctaMap<char*>();
  
  // We check all modules.
  for(vector<ctaModule*>::iterator
        it = this->GetModules()->begin();
      it != this->GetModules()->end();
      ++it)
  {
    // Check whether all module names unique.
    testMap->append((*it)->GetName(), NULL);
    
    // Check consistency of current module.
    (*it)->CheckConsistency(this); 
  }

  // We can not delete the inserted null pointer.
  testMap->clear();
  delete testMap;


  // Context checks for analysis commands section.
  for(vector<anaSection*>::iterator
      it = mAnaSections->begin();
      it != mAnaSections->end();
      ++it)
  {
    (*it)->CheckConsistency();
  }

  if(pubErrMsg->GetErrCount() != 0)
  {
    cerr << "Program aborted because of context errors." << endl;
    exit(1);
  }
}

//==========================================================
void 
ctaSystem::ExecuteAnalysis()
{
  for(vector<anaSection*>::iterator
      it = mAnaSections->begin();
      it != mAnaSections->end();
      ++it)
  {
    (*it)->Execute();
  }
}

//===========================================================
// The method below looks for all modules which are
//   not instantiated - and returns these modules.
// The caller owns the memory of the result.
set<ctaString>* 
ctaSystem::GetTopModules() const
{
  // A map for the top modules.
  set<ctaString>* lTopModules = new set<ctaString>();
  
  // Initialization with the whole set of module names.
  //   we need the pointer to the module to get the instantiations.
  for(vector<ctaModule*>::iterator
      it = this->GetModules()->begin();
      it != this->GetModules()->end();
      ++it)
  {
    // Key is the name of the module and the value is the
    //   pointer to the module object.
    lTopModules->insert( *( (*it)->GetName() ) );
  }

  
  // We run through the modules.
  for(vector<ctaModule*>::iterator
      it1 = this->GetModules()->begin();
      it1 != this->GetModules()->end();
      ++it1)
  {
    // Each instantiated template module we erase from the map
    //   which should contain the top modules.
    for(ctaMap<ctaInstantiation*>::iterator
	it2 = (*it1)->GetInstantiations()->begin();
	it2 != (*it1)->GetInstantiations()->end();
	++it2)
    {
      lTopModules->erase( *( it2->second->GetTemplateName() ) );
    }
  }

  // Memory deallocation of (lTopModules).
  // We have to delete (lTopModules) in the caller.

  return lTopModules;
}

//===========================================================
void 
ctaSystem::PrintObserverModules() const
{
  cout << "The pure observer modules are the following:" << endl;
  for(vector<ctaModule*>::iterator
      it = this->GetModules()->begin();
      it != this->GetModules()->end();
      ++it)
  {
    if((*it)->IsObserver())
    {
      cout << (*it)->GetName() << endl;
    }
  }  
}

//===========================================================
ctaModule* 
ctaSystem::findModule(ctaString pModName) const
{
  vector<ctaModule*>::iterator 
    result = GetModules()->begin();
  while(result != GetModules()->end()
	&& (*(*result)->GetName()) != pModName)
  {
    ++result;
  }

  if (result == GetModules()->end())
  {
    return NULL;
  }
  else
  {
    return *result;
  }
}

//===========================================================
void 
ctaSystem::PrintHyTech(const ctaString& pModuleName, 
		       ostream* pOutputStream) const
{
  ctaModule* lModule = this->findModule(pModuleName);
  if( lModule == NULL)
  {
    cerr << "Runtime error: Module " << pModuleName 
	 << " does not exist." << endl;
    exit(1);
  }
  ctaModule* lFlatModule = lModule->FlattenModule();

  cout << "Constructing HyTech representation for module \'";
  cout << lModule->GetName() << "\'." << endl;

  ctaHyTechPrinter lHyTechPrinter(*pOutputStream);
  lHyTechPrinter.PrintModuleToHyTech(lFlatModule);
  
  delete lFlatModule;
}


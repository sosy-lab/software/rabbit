/*
 * File:        ctaComponent.cpp
 * Purpose:     Component Class for cta-library
 * Author:      Dirk Beyer
 * Created:     1998
 * Copyright:   (c) 1998, University of Brandenburg, Cottbus
 */

#include "ctaComponent.h"

// aheinig 12.03.2000
// false: if used STOPWATCH, ANOLOG
// true : if used CONST, CLOCK, SYNC, DISCRETE
bool ctaComponent::CheckOnlyBddVariablesUsed()
{
  if( (dataType == ctaComponent::STOPWATCH) ||
      (dataType == ctaComponent::ANALOG) ) 
  {
    return false;
  }  
  return true; 
}

ostream& operator << (ostream& out, const ctaComponent::DATATYPE obj)
{
  switch(obj)
  {
  case ctaComponent::CONST:
    out << "CONST";
    break;
  case ctaComponent::CLOCK:
    out << "CLOCK";
    break;
  case ctaComponent::STOPWATCH:
    out << "STOPWATCH";
    break;
  case ctaComponent::ANALOG:
    out << "ANALOG";
    break;
  case ctaComponent::SYNC:
    out << "SYNC";
    break;
  case ctaComponent::DISCRETE:
    out << "DISCRETE";
    break;
  }
  return(out);
}

ostream& operator << (ostream& out, const ctaComponent::RESTTYPE obj)
{
  switch(obj)
  {
  case ctaComponent::INPUT:
    out << "INPUT";
    break;
  case ctaComponent::OUTPUT:
    out << "OUTPUT";
    break;
  case ctaComponent::MULTREST:
    out << "MULTREST";
    break;
  case ctaComponent::LOCAL:
    out << "LOCAL";
    break;
  } 
  return(out);
}

ostream& operator << (ostream& out, const ctaComponent* obj)
{
  out << " " << obj->GetRestrictionType() << " ";
  out << obj->GetName();
  if((obj->GetRestrictionType() == ctaComponent::LOCAL) && 
     (obj->GetDataType() == ctaComponent::CONST))
  {
    // Local constant carry a value.
    out << " = " << *( obj->GetValue() );
  }
  out << ": " << obj->GetDataType();	
  if (obj->GetMaxValueOfRange())
  {
    out << "(" << *obj->GetMaxValueOfRange() << ")";
  }
  out << ";" << endl;
  return(out);
};


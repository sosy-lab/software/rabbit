//-*-Mode: C++;-*-

/*
 * File:        ctaComponent.h
 * Purpose:     Component Class for cta-library
 * Author:      Dirk Beyer
 * Created:     1998
 * Copyright:   (c) 1998, University of Brandenburg, Cottbus
 */

#ifndef _ctaComponent_h
#define _ctaComponent_h


#include "ctaObject.h"
#include "ctaString.h"
#include "reprNUMBER.h"

class ctaModule;

class ctaComponent : private ctaObject
{
public:
  typedef enum {INPUT, OUTPUT, MULTREST, LOCAL} RESTTYPE;
  typedef enum {CONST, CLOCK, STOPWATCH, ANALOG, SYNC, DISCRETE} DATATYPE;
private:
  // Aggregated.
  ctaString*    name;

  // Restriction type is INPUT, OUTPUT, MULTREST, or LOCAL.
  RESTTYPE      restrictionType;

  DATATYPE	dataType;

  // Aggregated.
  //   Only used if (dataType) is "CONSTANT"
  //   otherwise it is a NULL-pointer.
  const reprNUMBER* mValue;

  // Aggregated.
  //   Maximal value for the component.
  //   Default is a NULL pointer which means
  //   the greatest value of reprNUMBER.
  const reprNUMBER* mMaxValueOfRange;

private:
  // It should be not allowed to use the standard operators.
  ctaComponent(const ctaComponent&);
  void operator=(const ctaComponent&);
  void operator,(const ctaComponent&);

public:
  // constructor and destructor
  ctaComponent(DATATYPE pDataType,
	       const reprNUMBER* const pValue = NULL)
    :name(NULL),
     dataType(pDataType),
     mValue(pValue),
     mMaxValueOfRange(NULL)
  {
    // (restrictionType) and (dataType) stay uninitialized.
  }

  ~ctaComponent()
  {
    delete name;
    if(mValue)
    {
      // We need the cast because Visual C++
      //   forbids deallocation of const objects.
      delete (reprNUMBER *) mValue;
    }
    if(mMaxValueOfRange)
    {
      // We need the cast because Visual C++
      //   forbids deallocation of const objects.
      delete (reprNUMBER *) mMaxValueOfRange;
    }
  }
  
  // accessor methods

  void 
  SetName(ctaString* s)             { name = s; }

  ctaString* 
  GetName() const             { return(name); }
  
  void 
  SetRestrictionType(RESTTYPE r)  { restrictionType = r; }

  void 
  SetValue(const reprNUMBER* const pValue)
  {
    mValue = pValue;
  }

  RESTTYPE 
  GetRestrictionType() const  { return(restrictionType); }
  
  DATATYPE 
  GetDataType() const         { return(dataType); }
  
  const reprNUMBER*
  GetValue() const
  {
    return mValue;
  }

  void
  SetMaxValueOfRange(const reprNUMBER* const pMaxValueOfRange)
  {
    mMaxValueOfRange = pMaxValueOfRange;
  }

  const reprNUMBER* 
  GetMaxValueOfRange() const
  {
    return mMaxValueOfRange;
  }

  // aheinig 06.09.2000
  // Clone (this)
  ctaComponent* 
  cloneSetPrefix(const ctaString *const pPrefix) const
  {        
    // Create the cloned component.
    ctaComponent* lComponentClone = new ctaComponent(dataType);
  
    // Set the correct assocation for name, restrictionType, value,
    //  maxValueOfRange.

    // Create the cloned signal name.
    ctaString* lSignalName = new ctaString(*pPrefix + *name); 
  
    lComponentClone->SetName(lSignalName);

    // Create the cloned value.
    if( mValue != NULL)
    {
      reprNUMBER* lValueClone = new reprNUMBER(*mValue);
      lComponentClone->SetValue(lValueClone);
    }

    lComponentClone->SetRestrictionType(restrictionType);

    // Create the cloned maxValueOfRange.
    if( mMaxValueOfRange != NULL)
    {
      reprNUMBER* lMaxValueOfRangeClone = new reprNUMBER(*mMaxValueOfRange);
      lComponentClone->SetMaxValueOfRange(lMaxValueOfRangeClone);
    }

    return lComponentClone;
} 

  // Service methods.
  // This method is for context analysis.
  // We supply the parent module for accessing module features.
  void 
  CheckConsistency(ctaModule*)
  {
    // here we have nothing to do
  }

  bool 
  CheckOnlyBddVariablesUsed();
};

ostream& operator << (ostream& out, const ctaComponent::DATATYPE obj);
ostream& operator << (ostream& out, const ctaComponent::RESTTYPE obj);
ostream& operator << (ostream& out, const ctaComponent* obj);

#endif

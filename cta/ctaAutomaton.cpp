
/*
 * File:        ctaAutomaton.cpp
 * Purpose:     Automaton Class for cta-library
 * Author:      Dirk Beyer
 * Created:     1998
 * Copyright:   (c) 1998, University of Brandenburg, Cottbus
 */

#include "ctaAutomaton.h"
#include "ctaRestFalse.h"
#include "ctaRestOr.h"
#include "ctaRestTrue.h"
#include "ctaRestNot.h"

#include "ddmAutomaton.h"
#include "ddmState.h"
#include "ddmTransition.h"

#include "ctaIntSet.h"

#include "bddAutomaton.h"
#include "bddTransition.h"

//===========================================================
// aheinig 06.09.2000
// Clone (this)
ctaAutomaton*
ctaAutomaton::cloneSetPrefixAndIdent(const ctaString *const pPrefix,
                     ctaMap<ctaString*>* pIdentNames) const
{
  // Insert the new automaton name (prefix + old name).
  ctaString* lAutoNameClone = new ctaString(*pPrefix + *mName);  

  // Clone the states.
  ctaMap<ctaState*>* lStateSetClone = new ctaMap<ctaState*>();
  ctaForAll_Map_const(mStates, it, ctaState*)
  {
    ctaState* lStateClone = it->second->cloneSetPrefixAndIdent(pPrefix,
                                   pIdentNames);
  
    lStateSetClone->append(lStateClone->GetName(),lStateClone);
  }

  // Create the automaton clone.
  ctaAutomaton* theAutoClone = new ctaAutomaton(lAutoNameClone, 
                        lStateSetClone);
  
  return theAutoClone;
}

//===========================================================
// aheinig 15.03.2000
// false: if used >, <, != by clocks
// true : else
bool 
ctaAutomaton::CheckNonStrictness(const ctaModule* pCtaModule) const
{
  ctaForAll_Map_const(mStates, it, ctaState*)
  {
    if( ! it->second->CheckNonStrictness(pCtaModule) )
    {
      return false;
    }
  }
  return true;
}

//===========================================================
// aheinig 23.03.2000
// true : if CheckBddConstraint true for all States
// false: else
bool 
ctaAutomaton::CheckBddConstraint(const ctaModule* pCtaModule) const
{
  ctaForAll_Map_const(mStates, it, ctaState*)
  {
    if( ! it->second->CheckBddConstraint(pCtaModule) )
    {
      return false;
    }
  }
  return true;
}

//===========================================================
void 
ctaAutomaton::CheckConsistency(ctaModule* module)
{
  // We have to ensure that an automaton has at least one state.
  if( mStates->size() == 0 )
  {
    CTAERR << "Automaton \'" << *mName << "\' has no state." << endl;
    CTAPUT;
  }


  // We have to check that we havn't two states with the same name.
  // It's fulfilled because of use of map as container.

  // We have to check all states.
  // It works with an empty set.
  ctaForAll_Map(mStates, idx, ctaState*)
  {
    // we take the idx-th element in set
    idx->second->CheckConsistency(module, this); 
  }
}

//===========================================================
void 
ctaAutomaton::CountRestrictions(ctaComponent* pCompo,
                int& pNrMultrestRestrictions,
                int& pNrOutputRestrictions)
{
  // For SYNCs: Visit all transitions and check with
  //   which prefixes (!,#,?) (pCompo) occurs.

  if(pCompo->GetDataType() == ctaComponent::SYNC)
  {
    // Did we find a multrest-restriction?
    bool lMULTREST = false;
  
    // Did we find an output-restriction?
    bool lOUTPUT = false;
    
    // Visit all states.
    ctaForAll_Map(this->GetStates(), state, ctaState*)
    {
      // Visit all transitions in current state.
      ctaForAll_SetPointer(state->second->GetTransitions(), trans, 
               ctaTransition*)
      {
    // Check if the signal name matches.
    if( *( pCompo->GetName() )
        == *( (*trans)->GetSync()->GetSignalName() )
        )
    {
      // Check if this is a MULTREST or an OUTPUT access.
      lMULTREST = lMULTREST || 
                  ( *((*trans)->GetSync()->GetSignalType()) == "#" );
      lOUTPUT = lOUTPUT  || 
                  ( *((*trans)->GetSync()->GetSignalType()) == "!" );
    }
      }
    }

    if(lMULTREST)
    {
      ++pNrMultrestRestrictions;
    }
    
    if(lOUTPUT)
    {
      ++pNrOutputRestrictions;
    }
  }
  else
  {
    // (pCompo) is a variable.
    //   A ticked occurrence in an assignment 
    //   or the occurrence in the derivations
    //   are MULTREST-accesses.  There are no
    //   OUTPUT accesses.
    bool lMULTREST = false;

    // Visit all states.
    ctaForAll_Map(this->GetStates(), state, ctaState*)
    {
      // For constants in derivations we don't check this.
      if( pCompo->GetDataType() != ctaComponent::CONST )
      {
    // Does the variable occur in the derivations?
    lMULTREST = lMULTREST
                  || state->second->GetDerivation()
                       ->VariableOccurs(pCompo->GetName());
      }

      // Visit all transitions.
      ctaForAll_SetPointer(state->second->GetTransitions(), trans, 
               ctaTransition*)
      {
    // Does the variable occur ticked in an assignment?
    if( (*trans)->GetAllow() )
    {
      lMULTREST = lMULTREST
        || (*trans)->GetAllow()
          ->VariableOccursTicked(pCompo->GetName());
    }
      }
    }

    if(lMULTREST)
    {
      ++pNrMultrestRestrictions;
    }
  }
}

//===========================================================
void 
ctaAutomaton::mkDdmCompletions(ctaModule* pModule)
{
  // Completes the semantic restrictions for derivations in states.
  ctaForAll_Map(this->GetStates(), stateIt, ctaState*)
  {
    stateIt->second->mkDdmCompletions(pModule);
  }  
}

//===========================================================
void 
ctaAutomaton::mkInputErrorStateCompletions(ctaModule* pModule)
{
  // Create an empty map to collect all used signals in the actual state.
  ctaSetPointer<ctaString*>* lSignalSet = new ctaSetPointer<ctaString*>();
    
  // Search for all input signals in the actual automaton.
  
  // Go trough all states.
  ctaForAll_Map(this->GetStates(),itState,ctaState*)
  {
    // Go trough all transitions.
    ctaForAll_SetPointer(itState->second->GetTransitions(),itTrans,
             ctaTransition*)
    {
      // Fetch the signal name.
      ctaString* lSigName = (*itTrans)->GetSync()->GetSignalName(); 
      ctaString* lSigType = (*itTrans)->GetSync()->GetSignalType();
 
      // Is there a signal or is it define as empty?
      if( *lSigName != "" )
      {
	// Test if the signal is a input signal.
	if( *lSigType == "?" )
	{
	  // Save the Signal in a vector for later inserts of error 
	  //   transitions for each signal.
	  lSignalSet->insert(lSigName);
	}
      }
    }
  }
  
  // The new name for the error state is INPUTERROR.
  ctaString* lErrorString = new ctaString(*mName);

  lErrorString->append("_INPUTERROR");

  // If no error transitions are created, the error state is unnecessary.
  bool lErrorTransCreated = false;
  
  // Go trough all states.
  ctaForAll_Map(mStates, itState, ctaState*)
  {
    // Go trough the map and insert for each signal a new error transition.  
    ctaForAll_SetPointer(lSignalSet, itSig, ctaString*)
    {
      // Create a new guard restriction.
      ctaRestriction *lRest = new ctaRestFalse();
        
      // Calculate the new guard for the error transition.
      ctaForAll_SetPointer(itState->second->GetTransitions(), itTrans, 
               ctaTransition*)
      {
    // Test if the actual transition uses the actual signal.
    if( *(*itTrans)->GetSync()->GetSignalName() == *(*itSig) )
    {
      // Create the new guard, for this it is needed to 
      //   clone the old guard cause of the aggregation.      
      lRest = ctaRestriction::mkOr(lRest, (*itTrans)->GetGuard()->clone());
    }
      }
      
      // Invert (lRest) and then we have the new guard for the error 
      //   transition.
      lRest = ctaRestriction::mkNot(lRest);
      
      // If the guard is not empty
      //   then create and insert the new error transition.
      if( !lRest->IsFalse() )
      {
    // An error transition is created, the error state necessary.
    lErrorTransCreated = true;
    // Construct a new sync structure.
    ctaSynchronisation* lNewSync = 
      new ctaSynchronisation( new ctaString(*(*itSig)), 
                  new ctaString("?") );
      
    // Store the name of the INPUT_ERROR state in the follower set of 
    //  the new transition.
    ctaString* lFollower = new ctaString(*lErrorString);
    
    ctaTransition* lTrans 
      = new ctaTransition(lFollower, 
                          lRest, 
                          lNewSync, 
                          new ctaRestTrue());
    
    // Insert transition in actual state.
    itState->second->GetTransitions()->insert(lTrans);
      }
      else
      {
    delete lRest;
      }
    }
  }
  
  if( lErrorTransCreated )
  {
    ctaSetPointer<ctaTransition*>* lErrorTransitionSet = 
                                        new ctaSetPointer<ctaTransition*>;
    // Insert an error state here.
    ctaState *lErrorState 
      = new ctaState(lErrorString, 
             new ctaRestTrue(), 
             new ctaRestTrue(), 
             lErrorTransitionSet);
    
    // Go trough all input signals and insert an looping error transition
    //   for each one from and to INPUTERROR state.  
    ctaForAll_SetPointer(lSignalSet, itSig, ctaString*)
    {
      // Name of the error state as follower state.
      ctaString* lFollowerState = new ctaString(*lErrorString);

      // Construct a new sync structure.
      ctaSynchronisation* lNewSync = 
    new ctaSynchronisation(new ctaString(*(*itSig)), 
                   new ctaString("?"));
      
      // Create and insert the new error transition
      ctaTransition* lTrans 
    = new ctaTransition(lFollowerState, 
                new ctaRestTrue(), 
                lNewSync, 
                new ctaRestTrue());
      
      // Save the transition in the transition set of the error state.
      lErrorTransitionSet->append(lTrans);
    }

    // Insert the error state in the actual automaton.
    this->GetStates()->append(lErrorState->GetName(),lErrorState);
  }
  else
  {
    delete lErrorString;
  }     

  // Delete the (lSignalMap).
  lSignalSet->clear();
  delete lSignalSet;
}

//===========================================================
// Construct a DDM-automaton for (this), using the given
//   symbol table.  First component has list of sync IDs, 
//   second of variable IDs.
ddmAutomaton*
ctaAutomaton::mkDdmAutomaton(const pair<set<ctaString>, 
                 set<ctaString> >& pSymtab,
                 const ctaModule* pCtaModule) const
{
  // Make a copy of the symbol tables.
  vector<string> ltmpvar;
  for(set<ctaString>::iterator 
      it1 = pSymtab.second.begin(); 
      it1 != pSymtab.second.end(); 
      ++it1)
  {
    ltmpvar.push_back(*it1);
  }
    
  vector<string> ltmpsync;
  for(set<ctaString>::iterator 
      it2 = pSymtab.first.begin(); 
      it2 != pSymtab.first.end(); 
      ++it2)
  {
    ltmpsync.push_back(*it2);
  }

  // Construct a skeleton automaton (with an empty sync set)
  //   with the name of the ctaModule from which it will
  //   be constructed.
  ddmAutomaton* result = new ddmAutomaton(* pCtaModule->GetName(),
                      this->GetStates()->size(),
                      ltmpvar,
                      ltmpsync);
  // Insert states.
  {
    int lStateNum = 0;
    for(ctaMap<ctaState*>::const_iterator 
      lStateIt = this->GetStates()->begin();
    lStateIt !=  this->GetStates()->end();
    ++lStateIt, ++lStateNum)
    {


      ddmRegion lInvariant  
    = lStateIt->second->GetInvariant()
    ->mkDdmRegion(*result, false, pCtaModule);

      ddmRegion lDerivation 
    = lStateIt->second->GetDerivation()
    ->mkDdmRegion(*result, false, pCtaModule);

      if(lDerivation.isEmpty())
      {
        CTAERR << "Attention: The region for the derivation"     << endl
               << "restriction of state '" 
               << lStateIt->second->GetName()
               << "' is empty!"                                  << endl
               << "(found after construction of the ddm region)" << endl
               << "Please check if you used a wrong restriction" << endl
               << "for a clock, stopwatch or discrete variable!" << endl;
        CTAPUT;
      }
      // Construct a ddm-state.

      ddmState lState(* lStateIt->second->GetName(),
              lInvariant,
              lDerivation);

      // Add state.
      result->addState(lStateNum, lState);
    }
  }

  // Insert transitions.
  {
    // Visit all states.
    for(ctaMap<ctaState*>::const_iterator
      lStateIt = this->GetStates()->begin();
    lStateIt !=  this->GetStates()->end();
    ++lStateIt)
    {
      // Visit all transitions in current state.
      for(ctaSetPointer<ctaTransition*>::const_iterator
          lTransIt = lStateIt->second->GetTransitions()->begin();
          lTransIt != lStateIt->second->GetTransitions()->end();
          ++lTransIt)
      {
        // Construct a ddm-transition.
        ddmTransition* lTrans 
          = (*lTransIt)->mkDdmTransition(*result,
                         *lStateIt->second->GetName(),
                         *( (*lTransIt)->GetState() ),
                         pCtaModule);

        // We have to add the sync label of the transition to the 
        //   alphabet of the automaton (result).
        result->addSync(lTrans->getSync());

        // Add transition to automaton, if the guard is not empty.
        if( ! lTrans->getAllowedGuardedAssignment().isEmpty() )
        {
          result->addTransition(lTrans);
        }
        else
        {
          delete lTrans;
        }
      }
    }
  }
  return result;
}

//===========================================================
// Construct a BDD-automaton for (this).
// Insert state names of (this) automaton into pSymTab.
// pSymTab has to be complete up to the names of the states.
// pProduct has to be true iff the constructed automaton will be
//   the basis of the product construction (see bddConfig::mkProduct).
bddAutomaton*
ctaAutomaton::mkBddAutomaton(bddSymTab* pSymTab,
			     bool pProduct,
			     const ctaModule* pCtaModule) const
{
  // Insert names of states into pSymTab.
  {
    vector<string> lTmpStates;
    for(ctaMap<ctaState*>::const_iterator 
      lStateIt = this->GetStates()->begin();
    lStateIt !=  this->GetStates()->end();
    ++lStateIt)
    {
      lTmpStates.push_back(*lStateIt->second->GetName());
    }
    pSymTab->setStateNames(*this->GetName(), lTmpStates);
  }

  // Compute invariant.
  bddConfig lInvariant(pSymTab, false);
  {
    for(ctaMap<ctaState*>::const_iterator 
      lStateIt = this->GetStates()->begin();
    lStateIt !=  this->GetStates()->end();
    ++lStateIt)
    {
      bddConfig lOneStateInvariant
    = lStateIt->second->GetInvariant()->mkBddConfig(pSymTab,pCtaModule);
      lOneStateInvariant.intersect(bddConfig::mkState(
    pSymTab, mName, lStateIt->second->GetName(), false));

      lInvariant.unite(lOneStateInvariant);
    }
  }

  // Compute transitions and set of sync labels.
  vector<bddTransition> lTransitions;
  ctaIntSet lSyncSet;
  {
    // Visit all states.
    for(ctaMap<ctaState*>::const_iterator
      lStateIt = this->GetStates()->begin();
    lStateIt !=  this->GetStates()->end();
    ++lStateIt)
    {
      // Visit all transitions in current state.
      for(ctaSetPointer<ctaTransition*>::const_iterator
        lTransIt = lStateIt->second->GetTransitions()->begin();
      lTransIt != lStateIt->second->GetTransitions()->end();
      ++lTransIt)
      {
        // Construct the bddTransition.
        bddTransition* lTrans 
          = (*lTransIt)->mkBddTransition(lStateIt->second->GetName(),
                         (*lTransIt)->GetState(),
                         pSymTab,
                         pCtaModule,
                         mName);

        // Add the sync label of the transition to the 
        //   alphabet of the automaton (result).
        if (lTrans->getSync() != -1)
        {
          lSyncSet.insert(lTrans->getSync());
        }

        // Add transition to automaton, if the guard is not empty. 
        if (!lTrans->getAllowedAndInitiated().isEmpty())
        {

          // If the transition is synchronized (i.e. label != -1),
          // unite it with transitions with equal label ands equal set
          // of modified variables. This avoids transition explosion.
          if (lTrans->getSync() != -1)
          {
            vector<bddTransition>::iterator lTransVecIt;
            for (lTransVecIt = lTransitions.begin();
                 lTransVecIt != lTransitions.end() 
                  && !(lTrans->isEqualInitiated(&(*lTransVecIt))
                  && lTrans->getSync() == lTransVecIt->getSync());
                 ++lTransVecIt)
            {}
            if (lTransVecIt == lTransitions.end())
            {
              lTransitions.push_back(*lTrans);
            }
            else
            {
              lTransVecIt->unite(lTrans);
            }       
          } 
          else 
          {
            lTransitions.push_back(*lTrans);
          }
        }

        delete lTrans;
      }
    }
  }

  // mvogel 2001-04-19 This tmpVar is necessary for linux-gnu compiler.
  // Compiler has problems with return statement like
  // 'return new bddAutomaton...'
  bddAutomaton* lAutomaton = 
    new bddAutomaton(lInvariant, lTransitions, lSyncSet, pSymTab, pProduct);

  return lAutomaton;
}

//===========================================================
void
ctaAutomaton::collectInterfacesForCommunicationGraph(ctaSet<ctaString>* 
                                    lUsedInterfaces)
{
  ctaForAll_Map(mStates, itState, ctaState*)
  {
    itState->second->collectInterfacesForCommunicationGraph(lUsedInterfaces);
  }
}

//===========================================================
ostream& operator << (ostream& out, const ctaAutomaton* obj)
{
  out << "  AUTOMATON " 
      << obj->GetName() << endl;
  out << "  {" << endl;

  // it works with an empty set
  out << obj->GetStates(); 
  out << "  }" << endl;
  return(out);
};

#ifndef _reprAutomatonOstream_h_
#define _reprAutomatonOstream_h_


#include <iostream>
#include "reprObject.h"

class reprAutomatonOstream : private reprObject
{
private: 
  // It should be not allowed to use the standard operators.
  void operator=(const reprAutomatonOstream&);
  void operator,(const reprAutomatonOstream&);

  // But we allow the public standard constructor to avoid warnings.

protected: // Attributes.
  // Associated.
  ostream* mOs;

public: // Constructors and destructor.

  reprAutomatonOstream()
    : mOs(NULL)
  {
  }

  reprAutomatonOstream(ostream* pOs)
    : mOs(pOs)
  {
  }

public: // Accessors.

  ostream *getOstream() const
  {
    return mOs;
  }
};

inline reprAutomatonOstream& 
operator << (reprAutomatonOstream& pOs, const char* pS)
{
  *(pOs.getOstream()) << pS;
  return pOs;
}

inline reprAutomatonOstream& 
operator << (reprAutomatonOstream& pOs, ostream& pFunc(ostream&))
{
  *(pOs.getOstream()) << pFunc;
  return pOs;
}

inline reprAutomatonOstream& 
operator << (reprAutomatonOstream& pOs, const int pI)
{
  *(pOs.getOstream()) << pI;
  return pOs;
}

inline reprAutomatonOstream& 
operator << (reprAutomatonOstream& pOs, const string& pS)
{
  *(pOs.getOstream()) << pS;
  return pOs;
}

// Local Variables:
// mode:C++
// End:

#endif // _reprAutomatonOstream_h_

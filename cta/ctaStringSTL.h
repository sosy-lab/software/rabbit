//-*-Mode: C++;-*-

/*
 * File:        ctaStringSTL.h
 * Purpose:     String Class for cta-library, derived from STL library
 * Author:      Dirk Beyer
 * Created:     1998
 * Copyright:   (c) 1998, University of Brandenburg, Cottbus
 */

#ifndef _ctaStringSTL_h
#define _ctaStringSTL_h


#include "string"
#include <iostream>

#include "ctaObject.h"

class ctaStringSTL : public string, private ctaObject
{
private:
  // It should be not allowed to use the standard operators.
  ctaStringSTL(const ctaStringSTL&);
  void operator=(const ctaStringSTL&);
  void operator,(const ctaStringSTL&);

public:
  // Constructor and destructor.
  explicit ctaStringSTL(const string &s)
    : string(s)
    {
      // cout << "ctaString(\"" << s << "\") called." << endl;
    }

  explicit ctaStringSTL(string &s)
    : string(s)
    {
      // cout << "ctaString(\"" << s << "\") called." << endl;
    }
  
  explicit ctaStringSTL(string& pStr, 
			unsigned int& pStart, 
			unsigned int pLength)
    : string(pStr, pStart, pLength)
    {}
    
  ~ctaStringSTL()
    {
      // cout << "ctaString " << *this << " deleted." << endl;
    }
  

  // We use the following methods of class string.
  
  //                    string();
  //                    string(const String& x);
  //                    string(const char* t);
  //                    ~string();
  //  string&           operator =  (const string&     y);
  //  string&           operator =  (const char* y);
  //  int operator==(const string& x, const string& y);
  //  int operator!=(const string& x, const string& y);
};

// I have to define it because we have a Pointer to a String.
inline ostream& 
operator << (ostream& out, const ctaStringSTL* s)
{
  // We use the operator of the base class.
  out << *s;

  return(out);
}


#endif

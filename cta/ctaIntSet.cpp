
#include "ctaIntSet.h"

#include <algorithm>

ctaIntSet
ctaIntSet::mkIntersection(const ctaIntSet& p1, const ctaIntSet& p2)
{
  ctaIntSet lResult;
  set_intersection(p1.begin(), 
		   p1.end(),
		   p2.begin(), 
		   p2.end(),
		   inserter(lResult, lResult.begin()));

  return lResult;
}

ctaIntSet
ctaIntSet::mkDifference(const ctaIntSet& p1, const ctaIntSet& p2)
{
  ctaIntSet lResult;
  set_difference(p1.begin(), 
		 p1.end(),
		 p2.begin(), 
		 p2.end(),
		 inserter(lResult, lResult.begin()));
  
  return lResult;
}

ctaIntSet
ctaIntSet::mkUnion(const ctaIntSet& p1, const ctaIntSet& p2)
{
  ctaIntSet lResult;
  set_union(p1.begin(), 
 	    p1.end(),
 	    p2.begin(), 
 	    p2.end(),
 	    inserter(lResult, lResult.begin()));
  
  return lResult;
}


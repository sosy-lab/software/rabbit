/*
 * File:        ctaModule.cpp
 * Purpose:     Module class for cta-library
 * Author:      Dirk Beyer
 * Created:     1998
 * Copyright:   (c) 1998, University of Brandenburg, Cottbus
 */

#include "ctaModule.h"

#include "utilCmdLineOptions.h"
#include "utilTime.h"

#include "ctaSystem.h"
#include "ctaAutomaton.h"
#include "ctaInstantiation.h"
#include "ctaConfigState.h"

#include "bddCommunicationGraph.h"
#include "bddSymTab.h"

// For insert of an integer to ctaString. 
#include <strstream>

// For find.
#include <algorithm>

ctaModule::ctaModule()
  : mName(NULL),
    mAutomata(new ctaMap<ctaAutomaton*>()),
    mInitial(new ctaConfigLinRest(new ctaRestTrue())),
    mInterface(new ctaMap<ctaComponent*>()),
    mInstantiations(new ctaMap<ctaInstantiation*>()),
    mParsingOrder(new vector<pair<ParsingTag, ctaString*> >)
{}

ctaModule::~ctaModule()
{
  delete mName;

  if(mParsingOrder)
  {
    for(vector<pair<ParsingTag,ctaString*> >::iterator 
	  it = mParsingOrder->begin();
	it != mParsingOrder->end();
	++it)
    {
      delete it->second;
    }
    
    delete mParsingOrder;
  }

  if(mAutomata)
  {
    delete mAutomata;
  }

  if(mInitial)
  {
    delete mInitial;
  }

  if(mInterface)
  {
    delete mInterface;
  }

  if(mInstantiations)
  {
    delete mInstantiations;
  }
}

void 
ctaModule::addInitial(ctaConfiguration* pInitial)
{
  mInitial = ctaConfiguration::mkAnd(mInitial, pInitial);
}

void 
ctaModule::CheckConsistency(ctaSystem* pSystem)
{
  // We have to check the consistency of all components of module "this"
  
  // There must be an interface.
  if(GetInterface()->size() == 0)
  {
    CTAERR << "There is no interface."
	   << endl;        
    CTAPUT;
  }

  // if we have at least one automaton then we need an initialization
  if( (GetAutomata()->size() != 0) &&
      (GetInitial() == NULL) )
  {
    CTAERR << "We have an automaton but no initialization." 
	   << endl;
    CTAPUT;
  }

  {
    // A map to check disjunctness of state sets.
    ctaMap<char*>* testMap = new ctaMap<char*>();
      
    // Check the automata.
    ctaForAll_Map(GetAutomata(), idx, ctaAutomaton*)
    {
      // Check the current automaton.
      idx->second->CheckConsistency(this);

      // We check whether the state sets of all automata are disjunct.
      // Append from ctaMap forces the error message.
      ctaForAll_Map(idx->second->GetStates(), state, ctaState*)
      {
	testMap->append(&(state->first), NULL);
      }
    }

    /* mvogel-2001-05-02 Check for disjunctness of state and interface
       identifiers. See Task 140 ind todo.txt. */
    
    // We also have to check the disjunctness for the union of all state
    // identifiers and all interface element identifiers.
    // Append from ctaMap also forces error message.
    ctaForAll_Map(GetInterface(), it, ctaComponent*)
    {
      testMap->append(&(it->first), NULL);
    }

    /* End mvogel 2001-05-02 */

    testMap->clear();
    delete testMap;
  }

  // check of initial configuration
  GetInitial()->CheckConsistency(this);
  
  // check of instantiations
  ctaForAll_Map(GetInstantiations(), idx, ctaInstantiation*)
  {
    // Check current instantiation.
    idx->second->CheckConsistency(pSystem, this);
  }

  {
    // We have to check that at most one automaton or instance
    //   is allowed to use a component for output.
    ctaForAll_Map(GetInterface(), idx, ctaComponent*)
    {
      // Check the current interface component (*idx).

      // We count the number of components in which it is 
      //   restricted.
      int lNrMultrestRestrictions = 0;
      int lNrOutputRestrictions = 0;

      // We have to check a synchronization signal or a var.

      // Restrictions are for SYNCs:
      //   1. Instantiations in which the signal is identified
      //      with an OUTPUT or MULTREST signal, and
      //   2. Automata in which the signal occurs with prefix "#"
      //      or "!".
      
      // Restrictions are for variables:
      //   1. Instantiations in which the variable is identified
      //      with an OUTPUT or MULTREST variable, and
      //   2. Automata in which the variable occurs in an 
      //      assignment as ticked variable, or in a derivation.
    
      // Check all instantiations.
      ctaForAll_Map(GetInstantiations(), inst, ctaInstantiation*)
      {	
	// Check instantiation (inst).
	inst->second->CountRestrictions(idx->second, 
					lNrMultrestRestrictions,
					lNrOutputRestrictions);
      }
    
      // Check all automata.
      ctaForAll_Map(GetAutomata(), a, ctaAutomaton*)
      {
	ctaString tmpstr = *(idx->second->GetName());
	// Check automaton (a).
	a->second->CountRestrictions(idx->second, 
				     lNrMultrestRestrictions,
				     lNrOutputRestrictions);
      }
    
      // Now check if the number of restrictions is ok:
      //  1. INPUTs may never be restricted.
      //  2. Each component which is restricted as OUTPUT may 
      //     be restricted only once this way, and may not be
      //     restricted as MULTREST.
      if( idx->second->GetRestrictionType() == ctaComponent::INPUT )
      {
	if(lNrMultrestRestrictions + lNrOutputRestrictions > 0)
	{
	  CTAERR << "INPUT \'" << idx->first 
		 << "\' of module " << mName << " is restricted."
		 << endl;
	  CTAPUT;
	}
      }
      else
      {
	if(  (lNrOutputRestrictions > 0)
	     && (lNrMultrestRestrictions + lNrOutputRestrictions > 1) )
	{
	  CTAERR << "Component \'" << idx->first 
		 << "\' of module " << mName
		 << " is used as OUTPUT, but is restricted multiply."
		 << endl;
	  CTAPUT;
	}
      } // End of result check.

    } // End of for all interface compos.
  }

  // Check only this module for unused signals.
  CheckForUnusedSignals(false);

}

// BEGIN mvogel 2001-04-03
// This method is used by CheckConsistency() with argument false to check
// only this module for unused signals and by
// anaSecRefinement:CheckConsistency() with argument true to check
// recursive all instanciated submodules of this module for unused
// signals.
bool 
ctaModule::CheckForUnusedSignals(bool recursive)
{
  bool result = true; // Return value.

  // Check for unused signals.
  {
    // Collect used signals in one set.
    set<ctaString> lUsedSignals;
    
    // Collect the signals from all automata.
    ctaForAll_Map(this->GetAutomata(), it,  ctaAutomaton*)
    {
      ctaForAll_Map(it->second->GetStates(), it1, ctaState*)
      {
	ctaForAll_SetPointer(it1->second->GetTransitions(), it2, 
			     ctaTransition*)
	{
	  // Test if a synchronisation exists.
	  if((*it2)->GetSync())
	  {
	    if((*it2)->GetSync()->GetSignalName())
	    {
	       lUsedSignals.insert(*((*it2)->GetSync()->GetSignalName()));
	    }
	  }
        }
      }
    }
    
    {
      // Collect the signals of all instantiations.
      ctaForAll_Map(this->GetInstantiations(), it, ctaInstantiation*)
      {
	ctaForAll_Map(it->second->GetConnections(), it1, ctaString*)
	{
	  ctaMap<ctaComponent*>::const_iterator it2 
	  = this->GetInterface()->find(*it1->second);
	  // Test, if the name exists in the interface.
	  if (it2 != this->GetInterface()->end())
	  {
	    // Test if the component is a signal.
	    if (it2->second->GetDataType() == ctaComponent::SYNC)
	    {
	      lUsedSignals.insert(*it1->second);
	    }
	  }
	}
      }
    }

    {
      // Test if any signal from the interface is a part of a instantiation
      //   or an automata.
      ctaForAll_Map(this->GetInterface(), it, ctaComponent*)
      {
	// Test if the component is a signal.
	if (it->second->GetDataType() == ctaComponent::SYNC)
	{
	  // Test if the signal is used or not.
	  if (lUsedSignals.find(it->first) == lUsedSignals.end())
	  {
	    // Omit the warning printout if called by
	    // anaSecRefinement::CheckConsistency();
	    if(!recursive)
	    {
	      // Do not use CTAERR and CTAPUT because it is only a warning.
	      cout << "Warning: Interface signal \'" << it->first 
		   << "\' of" << endl << " module \'" << *this->GetName()
		   << "\' is not used." << endl;
	      // Or should we add an additional automaton having one
	      //   transition labeled with the unused signal and 
	      //                      with a false guard?
	    }
	    result = false;
	  }
	}
      }
    }
  }

  // If method was called by anaSecRefinement::CheckConsistency().
  // we have to check recursively all instanciated submodules of
  // this module for unused signals.
  if(recursive)
  {
    // Get the template module of all instantiations and check for
    // unused signals.
    ctaForAll_Map(this->GetInstantiations(), it, ctaInstantiation*)
    {
      result &= it->second->GetTemplateModule()->CheckForUnusedSignals(true);
    }
  }

  return result;

}
// END mvogel 2001-04-03

bool ctaModule::IsObserver()
{
  ctaForAll_Map(GetInterface(), idx, ctaComponent*)
  {
    if( ( idx->second->GetRestrictionType() != ctaComponent::INPUT ) &&
	( idx->second->GetRestrictionType() != ctaComponent::LOCAL )   )
    {
      return false;
    }
  }
  return true;
}

// aheinig 12.03.2000
// false: if used STOPWATCH, ANALOG
// true : if used CONST, CLOCK, SYNC, DISCRETE
bool ctaModule::CheckOnlyBddVariablesUsed() const
{
  ctaForAll_Map(mInterface, it, ctaComponent*)
  {
    if( ! it->second->CheckOnlyBddVariablesUsed() )
    {
      return false;
    }
  }
  return true;
}

// aheinig 15.03.2000
// false: if used >, <, != (strict) by clocks
// true : else
bool ctaModule::CheckNonStrictness() const
{
  ctaForAll_Map_const(mAutomata, it, ctaAutomaton*)
  {
    if( ! it->second->CheckNonStrictness(this) )
    {
      return false;
    }
  }
  ctaForAll_Map_const(mInstantiations, it1, ctaInstantiation*)
  {
    if( ! it1->second->CheckNonStrictness(this) )
    {
      return false;
    } 
  }
  return mInitial->CheckNonStrictness(this);
}

// aheinig 23.03.2000
// true : if CheckBddConstraint true for all automation and for all initials
// false: else
bool ctaModule::CheckBddConstraint() const
{
  ctaForAll_Map_const(mAutomata, it, ctaAutomaton*)
  {
    if( ! it->second->CheckBddConstraint(this) )
    {
      return false;
    }
  }
  ctaForAll_Map_const(mInstantiations, it1, ctaInstantiation*)
  {
    if( ! it1->second->CheckBddConstraint(this) )
    {
      return false;
    } 
  }
  return mInitial->CheckBddConstraint(this);
}

// aheinig 24.03.2000
// This call checks for bdd restrictions above.
bool ctaModule::CheckBddRestrictions() const
{
  return (this->CheckOnlyBddVariablesUsed() &&
	  this->CheckBddConstraint() &&
  	  this->CheckNonStrictness());
}

//==========================================================
// The following method constructs a flattened CTA module 
//  for (this) and stores it in the result.
// It initiates the whole flattening process.
ctaModule*
ctaModule::FlattenModule() const
{
  ctaModule* lResult = new ctaModule();

  // Construct a prefix to use.
  ctaString* lNewPrefix = new ctaString( "" );
    
  // Construct an identification function.
  ctaMap<ctaString*>* lIdentNames = new ctaMap<ctaString*>();

  // Flatten this module. Writes the flattened version to (lResult).
  this->Flatten(lNewPrefix, lIdentNames, lResult, pubParsedSystem);
      
  delete lIdentNames;    
  delete lNewPrefix;

  return lResult;
}

// aheinig 03.09.2000
// This method flattens a module (module instantiation) and
//   is called by (FlattenModule) at top level at first.
// The whole flattening process is initiated by the method
//   (FlattenModule)!
// (ident) is the multi-level indentification function
//   for components.
void 
ctaModule::Flatten(const ctaString *const pPrefix,
		   ctaMap<ctaString*>* pIdentNames,
		   ctaModule* pFlatModule,
		   const ctaSystem *const pSystem) const
{
  // If we are in the top module we have no prefix, and so we can set
  //  the name of the new created flat module.
  if(*pPrefix == "")
  {
    ctaString* lName = new ctaString(*mName);
    pFlatModule->SetName(lName);   

    ctaForAll_Map(mInterface, interfaceIt, ctaComponent*)
    {
      if( interfaceIt->second->GetRestrictionType() != ctaComponent::LOCAL )
      {
	ctaComponent* lInterfaceClone = interfaceIt->second
	                                           ->cloneSetPrefix(pPrefix);
      
	pFlatModule->addInterface(lInterfaceClone->GetName(), 
				  lInterfaceClone);
      }
    }  
  }

  // Then we have automata, we clone this automata, and insert this automata
  //  in pFlatModule for collecting all automata.
  ctaForAll_Map(mAutomata, autoIt, ctaAutomaton*)
  {
    ctaAutomaton* lAutoClone = autoIt->second->cloneSetPrefixAndIdent(pPrefix,
								  pIdentNames);
    pFlatModule->addAutomaton(lAutoClone->GetName(), lAutoClone);
  }
  
  // If it is a LOCAL variable, we clone this variable and insert it in 
  //  pModule for collecting all variables.
  ctaForAll_Map(mInterface, interfaceIt, ctaComponent*)
  {
    if( interfaceIt->second->GetRestrictionType() == ctaComponent::LOCAL )
    {
      ctaComponent* lInterfaceClone = interfaceIt->second
	                                             ->cloneSetPrefix(pPrefix);
      
      pFlatModule->addInterface(lInterfaceClone->GetName(), lInterfaceClone);
    }
  }

  // We intersect (mkAnd) the existing config of (lFlatModule) 
  //   with the config of this module. 
  ctaConfiguration* lInitial = mInitial->cloneSetPrefixAndIdent(pPrefix, 
								pIdentNames);
  pFlatModule->addInitial( lInitial );

  // For every parsingTag of the local parsingOrder we insert this tag in 
  //  the parsingOrder of the flat module. If we have a instantation we call
  //  flatten for this module.
  for(vector<pair<ParsingTag, ctaString*> >::const_iterator 
	lParsingOrderIt = mParsingOrder->begin();
      lParsingOrderIt != mParsingOrder->end();
      ++lParsingOrderIt) 
  {
  
    if( lParsingOrderIt->first == ctaModule::AUTOMATON ) 
    { 
      pair<ctaModule::ParsingTag,ctaString*> lParsingOrderPair;
      lParsingOrderPair.first = ctaModule::AUTOMATON;
      lParsingOrderPair.second = new ctaString(*pPrefix + 
					       *lParsingOrderIt->second);
      pFlatModule->GetParsingOrder()->push_back(lParsingOrderPair);
    } 
    else if( lParsingOrderIt->first == ctaModule::INTERFACE ) 
    {
      pair<ctaModule::ParsingTag,ctaString*> lParsingOrderPair;
      lParsingOrderPair.first = ctaModule::INTERFACE;
      lParsingOrderPair.second = new ctaString(*pPrefix + 
					       *lParsingOrderIt->second);
      pFlatModule->GetParsingOrder()->push_back(lParsingOrderPair);
    }
    // If we have a instantation we must bild a new prefix, a new identList
    //  and call with this flatten.
    else  // INSTANTIATION.
    {  
      // For all instantiaton we insert the automaton and initialization 
      //  of this module into pFlatModule, and give back pFlatModule.
      ctaMap<ctaInstantiation*>::iterator ItInst = mInstantiations
	                                    ->find(*lParsingOrderIt->second);
      {
	// We build a new prefix, the old prefix plus the 
	//  instanceName plus ".". 
	ctaString* lTmpPrefix = new ctaString(*pPrefix + 
				   (*ItInst->second->GetInstanceName()) + ".");

	ctaMap<ctaString*>* lIdentNames = new ctaMap<ctaString*>();
	// We go across the instantiation and copy the instantiation in the 
	//  lIdentNames map. In following seqenze: string1 a (AS) string2 a1.  
	ctaForAll_Map(ItInst->second->GetConnections(), ItString, ctaString*)
	{
	  ctaString* firstString = new ctaString(ItString->first);
	  ctaString* secondString = new ctaString("");
	  ctaComponent* lVar = mInterface->find(*ItString->second)->second;
	  if ( lVar->GetRestrictionType() == ctaComponent::LOCAL )
	  {
	    secondString->append(*pPrefix);
	    secondString->append(*ItString->second);
	  }
	  else
	  {
	    secondString->append(*ItString->second);
	  }
	  lIdentNames->append(firstString, secondString);
	  delete firstString;
	}     

	// If a signal is defined in a higher instantiation we must verifying 
	//  if we must connect a local signal by it.
	ctaForAll_Map(lIdentNames, ItLocalIdentNames, ctaString*)
	{
	  ctaForAll_Map(pIdentNames, ItPublicIdentNames, ctaString*) 
	  {
	    if(*ItLocalIdentNames->second == ItPublicIdentNames->first)
	    {
	      *ItLocalIdentNames->second = *ItPublicIdentNames->second;
	    }
	  }
	}

	// Search the module with the template name. 
	for(vector<ctaModule*>::iterator 
	      itModule = pSystem->GetModules()->begin();
	    itModule != pSystem->GetModules()->end();
	    itModule++)
	{
	  if(*(*itModule)->GetName() == *ItInst->second->GetTemplateName())
	  {
	    // If we have find the module, we are flatting this module.
	    (*itModule)->Flatten(lTmpPrefix, 
				 lIdentNames, 
				 pFlatModule, 
				 pSystem);
	  }
	}

	{
	  // Adding initial configuration into (pFlatModule).
	  //  The follow instructions are for the INITIAL (initalisation) 
	  //  in the INST (instantiation).
	  ctaConfiguration* 
	    lConfig = ItInst->second->GetInitialConfig()
	                ->cloneSetPrefixAndIdent(lTmpPrefix,
						   lIdentNames);
	  pFlatModule->addInitial( lConfig );
	}

	delete lTmpPrefix; 
	delete lIdentNames;
      }   
    }
  }
}

void 
ctaModule::mkDdmCompletions()
{
  ctaForAll_Map(GetAutomata(), itAuto, ctaAutomaton*)
  {
    // Check the current automaton.
    itAuto->second->mkDdmCompletions(this);
  }  
}

void 
ctaModule::mkInputErrorStateCompletions()
{
  ctaForAll_Map(GetAutomata(), itAuto, ctaAutomaton*)
  {
    itAuto->second->mkInputErrorStateCompletions(this);
  }  
}

// Collect IDs of syncs and non-CONST variables.
void
ctaModule::mkSymtab(pair<set<ctaString>, set<ctaString> >& pSymtab) const
{
  // Visit all interface components and append.
  ctaForAll_Map(GetInterface(), compo, ctaComponent*)
  {
    // To which vector does (compo) belong?
    if(compo->second->GetDataType() == ctaComponent::SYNC)
    {
      // Compo belongs to 1st vector.

      pSymtab.first.insert(compo->first);
    }
    else if(compo->second->GetDataType() != ctaComponent::CONST)
    {
      // Compo belongs to 2nd vector.

      pSymtab.second.insert(compo->first);
    }
    // And what we do with symbols for constants?
    // They are replaced by their values in the semantics
    // and thus they do not need to occur in the symbol table.
  }
}

// Construct a DDM-automaton for (this).
ddmAutomaton*
ctaModule::mkDdmAutomaton() const
{
  if(pubCmdLineOpt->Verbose())
  {
    cout << "Constructing DDM automaton for module \'";
    cout << this->GetName() << "\'." << endl;
  }

  pair<set<ctaString>, set<ctaString> > lSymTab;
  this->mkSymtab(lSymTab);

  // Make a copy of the symbol tables.
  vector<string> ltmpvar;
  for(set<ctaString>::iterator 
      it1 = lSymTab.second.begin(); 
      it1 != lSymTab.second.end(); 
      ++it1)
  {
    ltmpvar.push_back(*it1);
  }
	
  vector<string> ltmpsync;
  for(set<ctaString>::iterator 
      it2 = lSymTab.first.begin(); 
      it2 != lSymTab.first.end(); 
      ++it2)
  {
    ltmpsync.push_back(*it2);
  }

  // We start without any automaton.
  ddmAutomaton* result = NULL;

  for(ctaMap<ctaAutomaton*>::const_iterator 
	lAutoIt = this->GetAutomata()->begin();
      lAutoIt != this->GetAutomata()->end();
      ++lAutoIt)
  {
    // Construct a ddm-automaton for the 
    //   cta-automaton (*lAutoIt->second).

    ddmAutomaton* lAuto = lAutoIt->second->mkDdmAutomaton(lSymTab, this);

    // Determine an initial configuration (regarding only states).
    //   It is only needed by the reduction step in mkProduct.
    //   The reduction removes states which are not reachable in
    //   discrete fashion.
    {
      // Determine set of initial states.
      // Collect all initial state conditions as disjunction
      //   in (lInitialStates) 
      //   which occur for (lAuto) somewhere in (this->GetInitial()).
      ctaConfiguration* lInitialStates = this->GetInitial()
          ->collectStatesForAutomaton(this, lAutoIt->second->GetName());

      lAuto->setInitial(lInitialStates->mkDdmConfig(*lAuto, this));

      // Free mem again.
      delete lInitialStates;
    }
    
    // The first automaton requires special handling.
    //   It is simply assigned to the result.
    if(result == NULL)
    {
      result = lAuto;
    }
    else
    {
      ddmAutomaton* lPtrToDelete = result;

      // Construct product of old result and new automaton.
      result = ddmAutomaton::mkProduct(*result, *lAuto);

      delete lAuto;

      delete lPtrToDelete;
    }

  }
	
  if(result == NULL)
  {
    cerr << "Runtime error: The module \'"
	 << * this->GetName()
	 << "\' contains no automaton.";

    // Construct an empty automaton with name from module.
    result = new ddmAutomaton(ctaString("_"),
			      0,
			      ltmpvar,
			      ltmpsync);
  }  

  // Construct and insert initial configuration for the ddm representation.
  //   It overwrites the initial configurations set above,
  //   because these setting were only used in ddmAutomaton::mkProduct(...)
  //   for reducing the states by discrete reachability.

  result->setInitial(this->GetInitial()->mkDdmConfig(*result, this));

  return result;
}
/*
void
ctaModule::mkRandomVarOrd()
{
  srand(1);

  cerr << "alte Ordnung";
  for (unsigned lIt = 0;
       lIt < mParsingOrder->size();
       ++lIt)
  {  
    cerr << ' ' << (*mParsingOrder)[lIt].second;
  }
  cerr << endl;

  for (unsigned lIt = 20; 
       lIt < mParsingOrder->size();
       ++lIt)
  {
    unsigned lRandomVar;
    pair<ParsingTag, ctaString*> lSwap;

    lRandomVar = rand() % (mParsingOrder->size()-lIt);
    cerr << lIt << ' ' << lIt+lRandomVar << ' ';
    lSwap = (*mParsingOrder)[lIt];
    (*mParsingOrder)[lIt] = (*mParsingOrder)[lIt+lRandomVar];
    (*mParsingOrder)[lIt+lRandomVar] = lSwap;
  }
  cerr << endl;

  cerr << "neue Ordnung";
  for (unsigned lIt = 0; 
       lIt < mParsingOrder->size();
       ++lIt)
  {  
    cerr << ' ' << (*mParsingOrder)[lIt].second;
  }
  cerr << endl;
}
*/

// Construct symbol table. 
// If (pSymTab) then
//   1) (pSymTab) must contain (at least) all variables and automata of (this),
//   2) the (bddVarInfo-)positions of the automata and variables
//      in the returned bddSymTab are the same as in (pSymTab).
// (pSymTab) is needed only for construction of automaton P
//    within the simulation check.
bddSymTab*
ctaModule::mkBddSymTab(const bddSymTab* pSymTab, 
		   vector<pair<ParsingTag, ctaString*> >* pParsingOrder) const
{
  bddSymTab* lSymTab = new bddSymTab(pParsingOrder);

  lSymTab->setModulName(*this->GetName());

  // Add automata and variables to (lSymTab).
  {
    if (pSymTab)
    {
      for (vector<pair<ParsingTag, ctaString*> >::const_iterator 
	     lIt = pParsingOrder->begin();
	   lIt != pParsingOrder->end();
	   ++lIt) 
      {

	if (lIt->first == ctaModule::AUTOMATON)
	{
	  lSymTab->addAutomaton(*lIt->second, 
	    mAutomata->find(*lIt->second)->second->GetStates()->size(),
	    pSymTab->getAutoInfo(*lIt->second).position);
	}
	else
	{
	  if (lIt->first == ctaModule::INTERFACE)
	  {
	    ctaComponent* lVar = mInterface->find(*lIt->second)->second;
	    if (lVar->GetDataType() == ctaComponent::CLOCK 
		|| lVar->GetDataType() == ctaComponent::DISCRETE)
	    {
	      if (lVar->GetMaxValueOfRange())
	      {
		lSymTab->addVariable(*lIt->second, 
		  *lVar->GetMaxValueOfRange(),
		  pSymTab->getVarInfo(*lIt->second).position);
	      }
	      else
	      {
		cerr << "Runtime error: No range given for variable '"
		     << *lIt->second << "'!" << endl;
		lSymTab->addVariable(*lIt->second, 
	          15,
		  pSymTab->getVarInfo(*lIt->second).position);
	      }
	    }
	  }
	}
      }
    }
    else
    {
      for (vector<pair<ParsingTag, ctaString*> >::const_iterator 
	     lIt = pParsingOrder->begin();
	   lIt != pParsingOrder->end();
	   ++lIt) 
      {

	if (lIt->first == ctaModule::AUTOMATON)
	{
	  lSymTab->addAutomaton(*lIt->second, 
	    mAutomata->find(*lIt->second)->second->GetStates()->size());
	}
	else
	{
	  if (lIt->first == ctaModule::INTERFACE)
	  {
	    ctaComponent* lVar = mInterface->find(*lIt->second)->second;
	    if (lVar->GetDataType() == ctaComponent::CLOCK 
		|| lVar->GetDataType() == ctaComponent::DISCRETE)
	    {
	      if (lVar->GetMaxValueOfRange())
	      {
		lSymTab->addVariable(*lIt->second, *lVar->GetMaxValueOfRange());
	      }
	      else
	      {
		cerr << "Runtime error: No range given for variable '"
		     << *lIt->second << "'!" << endl;
		lSymTab->addVariable(*lIt->second, 15);
	      }
	    }
	  }
	}
      }
    }
  }

  // db 2001-02-21: Changed for using pSymTab (for simulation check)!
  {
    vector<string> lTmpSync;
    
    if (pSymTab)
    {
      // This path is only used within the creation of 
      //   the bddAutomaton for P in a simulation check.
      //   We use the SymTab (pSymTab), which has created within
      //   the construction of the bddAutomaton for P||Q.
      lSymTab->setSyncNames(pSymTab->getSyncNames());
    }
    else
    {
      ctaForAll_Map(this->GetInterface(), lSyncIt, ctaComponent*)
      {
	if (lSyncIt->second->GetDataType() == ctaComponent::SYNC)
	{
	  lTmpSync.push_back(lSyncIt->first);
	}
      }

      lSymTab->setSyncNames(lTmpSync);
    }
  }

  return lSymTab;
}

//=======================================================================
// Construct a bddAutomaton for (this).
// For (pSymTab) see (mkBddSymTab).
bddAutomaton*
ctaModule::mkBddAutomaton(const bddSymTab* pSymTab = NULL) const
{
  if(pubCmdLineOpt->Verbose())
  {
    cout << "Constructing BDD automaton for module \'";
    cout << this->GetName() << "\'." << endl;
  }

  vector<pair<ParsingTag, ctaString*> >* lParsingOrder = 
                               new vector<pair<ParsingTag, ctaString*> >;

  // Construct a local copy of the parsing order.
  for(vector<pair<ParsingTag, ctaString*> >::const_iterator 
	lParsingOrderIt = mParsingOrder->begin();
      lParsingOrderIt != mParsingOrder->end();
      ++lParsingOrderIt) 
  {
    ctaString* lString = new ctaString(*lParsingOrderIt->second);
    lParsingOrder->push_back(
	  pair<ParsingTag, ctaString*>(lParsingOrderIt->first, lString));
  }

  // If we used the -b0 command line options then we order the variables. 
  if( pubCmdLineOpt->getUseVariableOrdering() )
  {
    // Creates the communication graph and variable ordering for bdd
    //  representation.
    this->calculateVariableOrdering(lParsingOrder);
  }

  utilTime* lTime = new utilTime();

  // Construct symtab and aggregate (lParsingOrder) as part of (lSymTab).
  bddSymTab* lSymTab = this->mkBddSymTab(pSymTab, lParsingOrder);

  // Construct product automaton.
  bddAutomaton* result = NULL;
  { 
    // Check if (this) contains an automaton.
    if (this->GetAutomata()->begin() == this->GetAutomata()->end())
    {
      cerr << "Runtime error: The module \'"
	   << * this->GetName()
	   << "\' contains no automaton." << endl;
      return NULL;
    }

    bool firstAutomaton = true;
    bool firstProduct = false;

    // We must insert the automata regarding the parsing order, 
    //   because the discrete transition become inserted in the same order.
    for(vector<pair<ParsingTag, ctaString*> >::const_iterator 
	  lParsingOrderIt = lParsingOrder->begin();
	lParsingOrderIt != lParsingOrder->end();
	++lParsingOrderIt) 
    {
      if( lParsingOrderIt->first == ctaModule::AUTOMATON )
      {
	ctaMap<ctaAutomaton*>::const_iterator itAutomaton
	  = mAutomata->find( *lParsingOrderIt->second );

	if( firstAutomaton ) 
	{
	  // Construct first automaton.
	  result = itAutomaton->second->mkBddAutomaton(lSymTab, true, this);
	  firstAutomaton = false;
	  firstProduct = true;
	}
	else
	{
	  // Add other automata.
	  bddAutomaton* lAuto
	    = itAutomaton->second->mkBddAutomaton(lSymTab, false, this);
	  result->product(lAuto, firstProduct);
	  firstProduct = false;
	  delete lAuto;
	}
      }
    }
  }

  // Do not delete lParsingOrder because it is aggregated by (lSymTab).

  // Construct and insert initial configuration.
  {
    bddConfig lInitial = this->GetInitial()->mkBddConfig(lSymTab, this);
    result->setInitial(lInitial);
  }

  // Construct and insert time step transition.
  { 
    // First construct a transition that leaves all states and vars unchanged.
    bddConfig lTimeStep = bddConfig::mkInitiated(lSymTab);

    // Modify lTimeStep so that it increments all clocks.
    ctaForAll_Map (this->GetInterface(), lVarIt, ctaComponent*)
    {
      if (lVarIt->second->GetDataType() == ctaComponent::CLOCK) 
      { 
	// Quantify out clock and clock' and intersect with (clock+1 = clock')
	lTimeStep.existsVar(&lVarIt->first, false);
	lTimeStep.existsVar(&lVarIt->first, true);
	bddExpression lE1(lVarIt->first, false, 1);
	bddExpression lE2(lVarIt->first, true, 0);

	lTimeStep.intersect(bddConfig::mkEqual(lSymTab, &lE1, &lE2));
      }
    } 
    result->setTimeStep(lTimeStep);
  }

  // aheinig
  // Unite all transitions.
  if( pubCmdLineOpt->getTransitionProductType() == 
                               utilCmdLineOptions::mUniteAllTransitions )
  {
    result->mkUnionOfAllTransitions();
  }

  // aheinig
  // If we used in the command line the options transitive closure of the 
  //  diskrete syncs, this method is called.
  if( pubCmdLineOpt->getTransitiveClosureOfDiskreteTransition() ) 
  {  
    if(pubCmdLineOpt->Verbose())
    {
      cout << "Computing transitive closure of transition relation." << endl;
    }
    result->mkTransitiveClosureOfEveryTransition();
  }

  // aheinig
  // If we used the command line options for random order of diskrete 
  //  transition, then this method is called.
  if ( pubCmdLineOpt->getRandomDiskreteTransitionOrder() ) 
  {
    if(pubCmdLineOpt->Verbose())
    {
      cout << "We use random diskrete transition order." << endl;
    }
    result->randomDiskreteTransitionOrder();
  }

  if (pubCmdLineOpt->Verbose())
  {  
    cout << "Time to construct product automaton: ";
    lTime->printPastTime();
    cout << "." << endl;
  }

  delete lTime;

  return result;
}

//=======================================================================
// This method decide if new variable ordering is calculating or the 
//  old one is using.
// We calculate a new variable ordering if the .vrd file is not exists
//  or the variable name in the .vrd file are not correct. 
void
ctaModule::calculateVariableOrdering(vector<pair<ParsingTag, 
				     ctaString*> >* pParsingOrder) const
{
  ctaString* lCtaInputFileName = 
    pubCmdLineOpt->getCtaFileNameWithoutExtension();
  lCtaInputFileName->append("_");
  lCtaInputFileName->append(*mName);
  lCtaInputFileName->append(".vrd");

  istream *lInputStream = new ifstream(lCtaInputFileName->data(), ios::in);

  // Decide if the file is exits or not.
  if (!(*lInputStream))
  {
    ostream& out = *pubCmdLineOpt->GetOutputStream();
    // File not found, and so we calculate a new variable ordering.
    this->calculateNewVariableOrdering(pParsingOrder, false, out);
  }
  else
  {
    // File found but the correctness is not checked yet.
    // Clears the old parsing order vector but the doesn't need it.
    for(vector<pair<ParsingTag, ctaString*> >::const_iterator 
	  lParsingOrderIt = pParsingOrder->begin();
	lParsingOrderIt != pParsingOrder->end();
	++lParsingOrderIt) 
    {
      delete lParsingOrderIt->second;
    }
    pParsingOrder->clear();

    // Read the parsing order into the vector.
    while( !lInputStream->eof() )
    {
      pair<ctaModule::ParsingTag, ctaString*> tmpPair;
     
      ctaString* lString = new ctaString("");
      *lInputStream >> *lString;
  
      if( !lInputStream->eof() )
      {
	if( *lString == "AUTOMATON" )
	{
	  tmpPair.first = ctaModule::AUTOMATON;
	} 
	else if( *lString == "INSTANTIATION" )
	{
	  tmpPair.first = ctaModule::INSTANTIATION;
	}
	else
	{
	  tmpPair.first = ctaModule::INTERFACE;
	}

	ctaString* lString1 = new ctaString("");
	*lInputStream >> *lString1;
	tmpPair.second = lString1;
         
	pParsingOrder->push_back(tmpPair);
      }

      delete lString;
    }
   
    delete lInputStream;
 
    bool IsCorrectOrder = true;
    if( mParsingOrder->size() != pParsingOrder->size() ) 
    {
      // We have new variables in the parsing order, so we must recall
      //  the variable ordering algorithm.
      IsCorrectOrder = false; 
    }
    else
    {
      // We show if all name in the actual parsing order are also in the
      //  loaded one. 
      for(vector<pair<ParsingTag, ctaString*> >::const_iterator 
	    lParsingOrderIt = mParsingOrder->begin();
	  ((lParsingOrderIt != mParsingOrder->end()) && IsCorrectOrder);
	  ++lParsingOrderIt) 
      {
	IsCorrectOrder = false;
	for(vector<pair<ParsingTag, ctaString*> >::const_iterator 
	      lParsingOrderIt1 = pParsingOrder->begin();
	    ( (lParsingOrderIt1 != pParsingOrder->end()) && !IsCorrectOrder );
	    ++lParsingOrderIt1) 
	{
	  if( (*lParsingOrderIt1->second) == (*lParsingOrderIt->second) )
	  {
	    IsCorrectOrder = true;
	  }
	}
      }
    }
    // The loaded (from file) parsing order is not to use and so we 
    //  calculate a new one.
    if( !IsCorrectOrder )
    {
      // Clears the vector.
      for(vector<pair<ParsingTag, ctaString*> >::const_iterator 
	    lParsingOrderIt = pParsingOrder->begin();
	  lParsingOrderIt != pParsingOrder->end();
	  ++lParsingOrderIt) 
      {
	delete lParsingOrderIt->second;
      }
      pParsingOrder->clear();
          
      // Construct a local copy of the parsing order.
      for(vector<pair<ParsingTag, ctaString*> >::const_iterator 
	    lParsingOrderIt = mParsingOrder->begin();
	  lParsingOrderIt != mParsingOrder->end();
	  ++lParsingOrderIt) 
      {
	ctaString* lString = new ctaString(*lParsingOrderIt->second);
	pParsingOrder->push_back(
	      pair<ParsingTag, ctaString*>(lParsingOrderIt->first, lString));
      }
      ostream& out = *pubCmdLineOpt->GetOutputStream();
      this->calculateNewVariableOrdering(pParsingOrder, false, out);
    }
  }

  delete lCtaInputFileName;
}

//=======================================================================
// This method calculate the variable ordering. The (pIsPrintBddEstim) 
//  variable is true if the PRINTBDDESTIM command is used.
void 
ctaModule::calculateNewVariableOrdering(vector<pair<ParsingTag, 
					ctaString*> >* pParsingOrder,
					bool pIsPrintBddEstim,
					ostream& pS) const
{
  if( !pIsPrintBddEstim && pubCmdLineOpt->Verbose() )
  {
    cout << "Calculate variable ordering." << endl;
  }

  // Construct a new communication graph.
  bddCommunicationGraph* lGraph = new bddCommunicationGraph();  
  
  // Insert in this communication graph the nodes for automaton and
  //  interfaces.
  for(vector<pair<ParsingTag, ctaString*> >::const_iterator 
	lParsingOrderIt = pParsingOrder->begin();
      lParsingOrderIt != pParsingOrder->end();
      ++lParsingOrderIt) 
  {
    // Add all automaton and interfaces into the communication graph.
    if( lParsingOrderIt->first != ctaModule::INSTANTIATION )
    {
      lGraph->insertNode(lParsingOrderIt->second, lParsingOrderIt->first);

      ctaMap<ctaComponent*>::const_iterator itComponent
	= mInterface->find( *lParsingOrderIt->second );
      // We have a interface and so we get the range trough the component.
      if( itComponent != mInterface->end() )
      {
	// We insert only disrete variables or clocks, not constants or syncs.
	if( itComponent->second->GetDataType() == ctaComponent::DISCRETE ||
	    itComponent->second->GetDataType() == ctaComponent::CLOCK)
	{
	  reprNUMBER* lMaxValueOfRange = 
	    new reprNUMBER(*itComponent->second->GetMaxValueOfRange());
	  // It's for the zero value.
	  (*lMaxValueOfRange)++; 
	  lGraph->insertMaxValueOfRangeAndBitWide( lParsingOrderIt->second, 
						   lMaxValueOfRange);
	  delete lMaxValueOfRange;
	}
      }
      // We have a automaton and so the range is the numer of states.
      else
      {
	ctaMap<ctaAutomaton*>::const_iterator itAutomaton
	  = mAutomata->find( *lParsingOrderIt->second );

	reprNUMBER* lMaxValueOfRange = 
	  new reprNUMBER(itAutomaton->second->getNumberOfStates() );
	
	lGraph->insertMaxValueOfRangeAndBitWide( lParsingOrderIt->second, 
						 lMaxValueOfRange);
	delete lMaxValueOfRange;
      }
    }
  }

  // Init the matrix with number of Node * number of Node. 
  lGraph->initInzidenzMatrix();

  multimap<ctaString, ctaString>* lUsedSyncs = new multimap<ctaString, 
                                                            ctaString>();
  
  ctaForAll_Map_const(mAutomata, itAuto, ctaAutomaton*)
  {
    // Collect all used interfaces(signals) for this automata in this map. 
    ctaSet<ctaString>* lUsedInterfaces = new ctaSet<ctaString>();
    // Go through the automaton and collect the interfaces(signals, variables);
    itAuto->second->collectInterfacesForCommunicationGraph(lUsedInterfaces);
    // Add for every interface a edge in the inzidenzmatrix.
    ctaForAll_Set(lUsedInterfaces, itUsedInterfaces, ctaString)
    {
      ctaMap<ctaComponent*>::const_iterator itComponent
	= mInterface->find( *itUsedInterfaces );
      // CONST signals don't interest us. 
      if( itComponent->second->GetDataType() != ctaComponent::CONST )
      {
	if( itComponent->second->GetDataType() != ctaComponent::SYNC ) 
	{
	  // We insert a edge between a automaton and a variable.
	  //cout << "insertedEdges: " << *itUsedInterfaces << " " 
	  //     << *itAuto->second->GetName() << endl;
	  lGraph->insertEdge(&(*itUsedInterfaces), 
			     itAuto->second->GetName());
	}
      }
    }
    ctaForAll_Map(mInterface, itComponent, ctaComponent*)
    {
      if( itComponent->second->GetDataType() == ctaComponent::SYNC )
      {
	ctaSet<ctaString>::const_iterator itUsedSyncs
	  = lUsedInterfaces->find( *itComponent->second->GetName() );
	
	if( itUsedSyncs != lUsedInterfaces->end() )
	{
	  lUsedSyncs->insert( make_pair(*itComponent->second->GetName(), 
					*itAuto->second->GetName()) );
	}
      }     
    }
    delete lUsedInterfaces;
  }  
  ctaForAll_Map(mInterface, itComponent, ctaComponent*)
  {
    if( itComponent->second->GetDataType() == ctaComponent::SYNC )
    {
      typedef multimap<ctaString, ctaString>::iterator itCtaStringMultimap;
      pair<itCtaStringMultimap, itCtaStringMultimap> itSyncs = 
	lUsedSyncs->equal_range( *itComponent->second->GetName() );
	
      for( itCtaStringMultimap p = itSyncs.first; p != itSyncs.second; p++ )
      {
	for( itCtaStringMultimap p1 = itSyncs.first; 
	     p1 != itSyncs.second; 
	     p1++ )
	{
	  // keine kante zwischen sich selbst (automaten).
	  if( p1 != p )
	  {
	    lGraph->insertEdge(&p->second, &p1->second);
	  }
	}
      }
    }
  }
  
  delete lUsedSyncs;

  if( !pIsPrintBddEstim )
  {
    lGraph->calculateVariableOrder();

    lGraph->giveOrderedNodes(pParsingOrder);

    ctaString* lCtaOutputFileName = 
      pubCmdLineOpt->getCtaFileNameWithoutExtension();
    lCtaOutputFileName->append("_");
    lCtaOutputFileName->append(*mName);
    lCtaOutputFileName->append(".vrd");

    // Output of the variable order in a file.
    ostream *lOutputStream = 
      new ofstream(lCtaOutputFileName->data(), ios::out);
  
    if (!(*lOutputStream))
    {
      cerr << "Runtime error: can't open input file" << endl;
      exit(1);
    }
    else
    {
      for(vector<pair<ParsingTag, ctaString*> >::const_iterator 
	    lParsingOrderIt = pParsingOrder->begin();
	  lParsingOrderIt != pParsingOrder->end();
	  ++lParsingOrderIt) 
      {
	if( lParsingOrderIt->first == ctaModule::AUTOMATON ) 
	{
	  *lOutputStream << "AUTOMATON" << endl;
	}
	else if( lParsingOrderIt->first == ctaModule::INSTANTIATION ) 
	{
	  *lOutputStream << "INSTANTIATION" << endl;
	}
	else
	{
	  *lOutputStream << "INTERFACE" << endl;
	}
	*lOutputStream << lParsingOrderIt->second << endl;
      }
    }

    delete lCtaOutputFileName;
    delete lOutputStream;
  }
  else
  {
    lGraph->printGraphicalBddEstim(pS);
  }

  delete lGraph;
}

//=======================================================================
// Checks whether module instance P of module (this)
//   is a refinement (implementation) of module instance Q of module (this).
bool
ctaModule::CheckSimulationBDD()
{
  // Handling error messages.
  // Save the old error prefix.
  const ctaString *lOldErrPrefix = pubErrMsg->GetPrefix();
  // Set the new error prefix.
  pubErrMsg->SetPrefix(new ctaString("CheckSimulationBDD: "));

  if(pubCmdLineOpt->Verbose())
  {
    cout << "Refinement check." << endl;
  }

  // We have to initialize the BDD package.
  pubCmdLineOpt->initBDD();

  // All the checks for consistency are done by
  //   anaImplemInstr::CheckConsistency().
  // Here we assume that all things are right.

  // Firstly we construct the product automaton for P||Q from module (this).
  ctaModule* lFlatModule = this->FlattenModule();
  bddAutomaton* lPQSimuAuto = lFlatModule->mkBddAutomaton();
  delete lFlatModule;


  // Secondly we construct the automaton for P from module (this).

  // We have to eliminate Q from this module containing P and Q.
  ctaMap<ctaInstantiation*>::iterator lInstIt2 
    = mInstantiations->find(ctaString("Q"));
  ctaInstantiation* lQInstance = lInstIt2->second;

  mInstantiations->erase(lInstIt2);

  // Search for the position to store the parsing order pair.
  vector<pair<ctaModule::ParsingTag,ctaString*> >::iterator lOrderIt 
    = mParsingOrder->begin();
  while(lOrderIt->first != INSTANTIATION ||
	*(lOrderIt->second) != "Q")
  {
    ++lOrderIt;
  }
  pair<ctaModule::ParsingTag,ctaString*> lInstPair = * lOrderIt;
  mParsingOrder->erase(lOrderIt);


  // Construct the automaton for P from module (this).
  lFlatModule = this->FlattenModule();
  bddAutomaton* lPSimuAuto = lFlatModule->mkBddAutomaton(
						lPQSimuAuto->getSymTab());
  delete lFlatModule;


  // Restore Q within this module.
  mInstantiations->append(lQInstance->GetInstanceName(),
			  lQInstance);
  mParsingOrder->insert(lOrderIt, lInstPair);




  // Now we collect the synchronization labels for synchronization
  //   between (lPQSimuAuto) and (lPSimuAuto).
  ctaIntSet lVisibleSyncs;
  // Construct lVisibleSyncs.
  {
    // To consider: (this) is the original module (not the flattened one).

    // Visit all interface components and insert sync labs.
    ctaForAll_Map(mInterface, compo, ctaComponent*)
    {
      if(compo->second->GetDataType() == ctaComponent::SYNC)
      {
	lVisibleSyncs.insert(
	  lPQSimuAuto->getSymTab()->getSyncNum(compo->first));
      }
    }
  }

  // Regard the definition of the alphabet of an automaton as
  //   "all the sync labels which are used by at least one transition."
  // For the purpose of simulation all visible sync labels
  //   have to be in the alphabet of both automata.
  // This condition is only implemented by the context check.

  // Refinement checking.
  bool lResult = lPQSimuAuto->isSimulation(lPSimuAuto, lVisibleSyncs);

  // Free memory.
  delete lPQSimuAuto;
  delete lPSimuAuto;

  // Uninitialize BDD storage.
  bddBdd::done();

  // Handling error messages.
  // Delete the error prefix.
  delete pubErrMsg->GetPrefix();
  // Set the old error prefix.
  pubErrMsg->SetPrefix(lOldErrPrefix);

  return lResult;
}

//=======================================================================
ostream&
operator << (ostream& out, const ctaModule* obj)
{
  out << "MODULE ";
  if(obj->GetName() != NULL)
  {
    out << obj->GetName() << endl;
  }
  out << "{" << endl;

  // Output of the interface components which are not in the parsing order.
  ctaForAll_Map(obj->GetInterface(), itInterface, ctaComponent*)
  {
    if( itInterface->second->GetRestrictionType() == ctaComponent::INPUT || 
	itInterface->second->GetRestrictionType() == ctaComponent::OUTPUT ||
	itInterface->second->GetRestrictionType() == ctaComponent::MULTREST )
    {
      out << itInterface->second;
    }
    
    // Non-LOCAL ANALOG, STOPWATCH, CONST and SYNC 
    //   not contained in the parsing order
    //   because it contains only things relevant for BDD representation.
    if( itInterface->second->GetRestrictionType() == ctaComponent::LOCAL )
    { 
      if( itInterface->second->GetDataType() == ctaComponent::ANALOG    || 
	  itInterface->second->GetDataType() == ctaComponent::STOPWATCH ||
	  itInterface->second->GetDataType() == ctaComponent::CONST     ||
	  itInterface->second->GetDataType() == ctaComponent::SYNC )
      {
	out << itInterface->second;
      }
    }
  }
  
  out << endl;
  
  for(vector<pair<ctaModule::ParsingTag,ctaString*> >::iterator 
      it = obj->GetParsingOrder()->begin();
      it != obj->GetParsingOrder()->end();
      ++it)
  {
    if(it->first == ctaModule::INTERFACE)
    {
      // it works with an empty set
      // output of interface
      // always we have at least an empty set
      out << obj->GetInterface()->find(*it->second)->second;
      out << endl;				
    }	
    if(it->first == ctaModule::INSTANTIATION)
    {
      // output of Instantiations
      out << obj->GetInstantiations()->find(*it->second)->second;
      out << endl;				
    }	
    if(it->first == ctaModule::AUTOMATON)
    {
      // output of automaton
      out << obj->GetAutomata()->find(*it->second)->second;
    }
  }

  // output of initial configuration
  {
    out << "  INITIAL" << endl;
    out << "    ";
    out << obj->GetInitial();
    out << ";";
    out << endl;
  }


  out << "}" << endl << endl;
  return(out);
};

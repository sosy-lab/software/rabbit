//-*-Mode: C++;-*-

/*
 * File:        ctaSetPointer.h
 * Purpose:     Set Class for cta-library
 * Author:      Dirk Beyer
 * Created:     1998
 * Copyright:   (c) 1998, University of Brandenburg, Cottbus
 */

#ifndef _ctaSetPointer_h
#define _ctaSetPointer_h


// for new sdt-lib
#include <iterator>
#include <set>

#include "ctaObject.h"

//---- ctaSetPointer---------------------------------------------------------

// This is a set for references.

template<class E> class ctaSetPointer : public set<E>, private ctaObject
{
private:
  // It should be not allowed to use the standard operators.
  ctaSetPointer(const ctaSetPointer&);
  void operator=(const ctaSetPointer&);
  void operator,(const ctaSetPointer&);
	
public:
  // constructor
  ctaSetPointer()
  {}

  // traversion for elements idx of list S
#define ctaForAll_SetPointer(S, idx, TYPE) for(ctaSetPointer<TYPE>::iterator idx = (S)->begin();  idx != (S)->end() ; ++idx )
  
  ~ctaSetPointer()
    {
      // it works with an empty set
      ctaForAll_SetPointer(this, idx, E)
      {
	// we take the idx-th element in set.
	delete *idx; 
      }    
    }
  
  void append(const E item)
    {
      insert(item);
    }

  void join(ctaSetPointer<E>* l)
    {
      // set l inserting as a range
      ctaForAll_SetPointer(l, it, E)
      {
	this->insert(*it);
      }
    }
  
  // we use the following methods of set:
  
};

template<class E> ostream& operator << (ostream& out, ctaSetPointer<E>* obj)
{
				// it works with an empty set
  ctaForAll_SetPointer(obj, idx, E)
  {
    // we take the idx-th element in set parsedModules
    out << *idx; 
  }
  return(out);
}

#endif

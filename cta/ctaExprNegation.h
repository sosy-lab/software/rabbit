//-*-Mode: C++;-*-

/*
 * File:        ctaExprNegation.h
 * Purpose:     Negation-Expression Class for cta-library
 * Author:      H. Rust
 * Created:     1998
 * Copyright:   (c) 1998, University of Brandenburg, Cottbus
 */

#ifndef _ctaExprNegation_h
#define _ctaExprNegation_h


#include "ctaExpression.h"
class ctaModule;

class ddmAutomaton;
#include "ddmVec.h"
class bddAutomaton;
#include "bddExpression.h"


class ctaExprNegation : public ctaExpression
{
private:

  ctaExpression *mE;

  // It should be not allowed to use the standard operators.
  ctaExprNegation(const ctaExprNegation&);
  void operator=(const ctaExprNegation&);
  void operator,(const ctaExprNegation&);

public:
  // constructor and destructor
  ctaExprNegation(ctaExpression *pE)
    : mE(pE)
  {}

  ~ctaExprNegation()
  {
    if(mE) 
    {
      delete mE;
    }
  }
  
  // accessor methods
  // Service methods.

  // Clone (this).
  ctaExpression*
  cloneSetPrefixAndIdent(const ctaString *const pPrefix,
			 ctaMap<ctaString*>* pIdentNames) const
  {
    // Create the cloned operand.
    ctaExpression* cloneOperand = mE->cloneSetPrefixAndIdent(pPrefix, 
							     pIdentNames);
    // Create the clone.
    ctaExpression* theClone = new ctaExprNegation(cloneOperand);
    
    return theClone;
  }

  // Check the derivation for correctness.
  void 
  CheckDerivation(const ctaModule* const pModule) const
  {
    this->mE->CheckDerivation(pModule);
  }

  // This method is for context analysis.
  // We give the parent module for accessing module features.
  void
  CheckConsistency(const ctaModule* const pModule)
  {
    mE->CheckConsistency(pModule);
  }

  bool
  VariableOccurs(ctaString *pId)
  {
    return mE->VariableOccurs(pId);
  }

  bool
  VariableOccursTicked(ctaString *pId)
  {
    return mE->VariableOccursTicked(pId);
  }
  
  // aheinig 22.03.2000
  // only false, in bdd minus are not allowed
  bool 
  CheckBddConstraint() const
  {
    return false;
  }

  // aheinig 22.03.2000
  // count all used variable
  int 
  CountVariables(const ctaModule* pCtaModule) const
  { 
    return mE->CountVariables(pCtaModule);
  }

  // aheinig 22.03.2000
  // count all used clocks
  int 
  CountClocks(const ctaModule* pCtaModule) const
  { 
    return mE->CountClocks(pCtaModule);
  }

  void
  Print(ostream& s) const
  {
    s << "-(" << mE << ")";
  }

  // aheinig 2000-11-02
  // Collect all used signals and variables for this automaton.
  void 
  collectInterfacesForCommunicationGraph(ctaSet<ctaString>* lUsedInterfaces)
  {
    mE->collectInterfacesForCommunicationGraph(lUsedInterfaces);
  }

  // Construct a vector representing (*this) >= 0.
  ddmVec
  mkDdmVec(const reprAutomaton& pAuto,
	   bool pDoubleSize,
	   const ctaModule* pCtaModule)
  {
    return - ( mE->mkDdmVec(pAuto, pDoubleSize, pCtaModule) );
  }

  bddExpression*
  mkBddExpression (const ctaModule* pCtaModule) 
  {
    cerr << "Runtime error: Negation in Expression " 
	 << this << endl
	 << "in module '" << pCtaModule->GetName() << "'!" << endl;

    return new bddExpression("", false, 0);
  }
};
#endif

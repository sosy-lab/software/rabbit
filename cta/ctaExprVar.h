//-*-Mode: C++;-*-

/*
 * File:        ctaExpVar.h
 * Purpose:     Variable Class for cta-library
 * Author:      H. Rust
 * Created:     1998
 * Copyright:   (c) 1998, University of Brandenburg, Cottbus
 */

#ifndef _ctaExprVar_h
#define _ctaExprVar_h


// class ctaModule;
#include "ctaComponent.h"
#include "ctaExpression.h"
#include "ctaString.h"
#include "ctaSet.h"

#include "ddmVec.h"
#include "ddmXPoly.h"
#include "bddExpression.h"

class ctaExprVar : public ctaExpression
{
private:
  // Variable tags.
  typedef enum {mSTANDARD, mDER, mTICKED} ctaExprVarTag;

  // Aggregated,
  ctaExprVarTag mTag;
  reprNUMBER mFactor;
  ctaString* mIdentifier;

  void PrintCtaOrHyTech(ostream& pOutStream, bool pHyTechPrinting) const;

  // It should be not allowed to use the standard operators.
  ctaExprVar(const ctaExprVar&);
  void operator=(const ctaExprVar&);
  void operator,(const ctaExprVar&);

public:
  // constructor and destructor
  ctaExprVar(ctaString* pIdentifier,
	     ctaExprVarTag pTag)
    : mTag(pTag),
      mFactor(1),
      mIdentifier(pIdentifier)
  {}

  ~ctaExprVar()
  {
    delete mIdentifier;
  }
  
  // accessor methods
  reprNUMBER 
  GetFactor() const
  {
    return mFactor;
  }

  void 
  SetFactor(reprNUMBER pFactor)
  {
    mFactor = pFactor;
  }

  // Service methods.

  // Clone (this).
  ctaExpression* 
  cloneSetPrefixAndIdent(const ctaString *const pPrefix,
			 ctaMap<ctaString*>* pIdentNames) const
  { 
    // Clone the identifier string.
    ctaString*  lIdentifier = new ctaString( *pPrefix + *mIdentifier );
    if(pIdentNames != NULL)
    {
      // Replace the variable name by the name defined in the instantiation.
      ctaForAll_Map(pIdentNames, itIdent, ctaString*)
      {
	if(itIdent->first == *mIdentifier)
	{
	  *lIdentifier = *itIdent->second;
	}
      }
    }
  
    // Create the cloned object.
    ctaExprVar* theClone = new ctaExprVar(lIdentifier, this->mTag);
 
    // Give the clone the aggregated factor.
    theClone->SetFactor(mFactor);

    return theClone;
  } 

  // Check for correct derivation.
  void 
  CheckDerivation(const ctaModule* const pModule) const;

  // This method is for context analysis.
  // We give the parent module for accessing module features.
  void 
  CheckConsistency(const ctaModule* const pModule);

  bool
  VariableOccurs(ctaString *pId)
  {
    return (*pId == *mIdentifier);
  }

  bool
  VariableOccursTicked(ctaString *pId)
  {
    return 
      (*pId == *mIdentifier)
      && (mTag == mTICKED);
  }

  // aheinig 22.03.2000
  // true: if the factor equal 1
  // false:else 
  bool 
  CheckBddConstraint() const
  {
    if( mTag != mDER &&  mFactor == 1 )
    {
      return true;
    }
    return false;
  }

  // aheinig 22.03.2000
  // count all used variable, CONST variable are none variable
  int 
  CountVariables(const ctaModule* pCtaModule) const
  { 
    const ctaComponent* lTmpComp = pCtaModule->GetInterface()
                                             ->find(*(this->mIdentifier))
                                             ->second;
    // Check if this is a clock.
    if( lTmpComp->GetDataType() == ctaComponent::CONST ||
	lTmpComp->GetDataType() == ctaComponent::CLOCK )
    {
      return 0;
    }
    return 1;    
  }

  // aheinig 22.03.2000
  // count all used clocks
  int 
  CountClocks(const ctaModule* pCtaModule) const
  { 
    const ctaComponent* lTmpComp = pCtaModule->GetInterface()
                                             ->find(*(this->mIdentifier))
                                             ->second;

    // Check if this is a clock.
    if( lTmpComp->GetDataType() == ctaComponent::CLOCK )
    {
      return 1;
    }
    return 0;
  }

  void 
  Print(ostream& pOutStream) const
  {
    // Printing as CTA.
    this->PrintCtaOrHyTech(pOutStream, false);
  }

  void 
  PrintHyTech(ostream& pOutStream) const
  {
    // Printing as HyTech.
    this->PrintCtaOrHyTech(pOutStream, true);
  }

  // Class methods.
  static ctaExprVar* 
  mkDer(ctaString* pId)
  {
    return new ctaExprVar(pId, mDER);
  }

  static ctaExprVar* 
  mkStandard(ctaString* pId)
  {
    return new ctaExprVar(pId, mSTANDARD);
  }
  
  static ctaExprVar* 
  mkTicked(ctaString* pId)
  {
    return new ctaExprVar(pId, mTICKED);
  }

  // aheinig 2000-11-02
  // Collect all used signals and variables for this automaton.
  void 
  collectInterfacesForCommunicationGraph(ctaSet<ctaString>* lUsedInterfaces)
  {
    lUsedInterfaces->insert(*mIdentifier);
  }

  // Construct a vector representing (*this) >= 0.
  ddmVec
  mkDdmVec(const reprAutomaton& pAuto, 
	   bool pDoubleSize,
	   const ctaModule* pCtaModule);

  bddExpression*
  mkBddExpression (const ctaModule* pCtaModule);
};

#endif

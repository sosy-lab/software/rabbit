//-*-Mode: C++;-*-

/*
 * File:        ctaRestFalse.h
 * Purpose:     False-Restriction class for cta-library
 * Author:      H. Rust
 * Created:     1998
 * Copyright:   (c) 1998, University of Brandenburg, Cottbus
 */

#ifndef _ctaRestFalse_h
#define _ctaRestFalse_h


class ctaComponent;
class ctaModule;
#include "ctaRestriction.h"

#include "ddmAutomaton.h"
#include "ddmRegion.h"

#include "bddConfig.h"

class ctaRestFalse : public ctaRestriction
{
private:
  // It should be not allowed to use the standard operators.
  ctaRestFalse(const ctaRestFalse&);
  void operator=(const ctaRestFalse&);
  void operator,(const ctaRestFalse&);

public:
  // constructor
  ctaRestFalse()
  {}

  ~ctaRestFalse()
  {}

  // accessor methods

  // Service methods.

  // aheinig 15.03.2000
  // false: if used >, <, != by clocks
  // true : else 
  bool CheckNonStrictness(const ctaModule* pCtaModule) const
  {
    return true;
  }

  // aheinig 22.03.2000
  // only true: no variables appropriated
  bool CheckBddConstraint(const ctaModule* pCtaModule, 
			  const bool logicalOrIsAllowed) const
  {
    return true;
  }

  // Clone (this).
  ctaRestriction* 
  cloneSetPrefixAndIdent(const ctaString *const pPrefix,
			 ctaMap<ctaString*>* pIdentNames) const 
  {
    // Create the clone.
    ctaRestriction* theClone = new ctaRestFalse();
    return theClone;
  }

  // Check the derivation for correctness.
  void CheckDerivation(const ctaModule* const pModule) const 
  {
    // Nothing to do because (this) has not member.
  }
  
  // This method is for context analysis.
  // We give the parent module for accessing module features.
  virtual void 
  CheckConsistency(const ctaModule* const pModule)
  {}

  virtual bool
  VariableOccurs(ctaString *pId)
  {
    return false;
  }

  virtual bool
  VariableOccursTicked(ctaString *pId)
  {
    return false;
  }

  // aheinig 2000-11-02
  // Collect all used signals and variables for this automaton.
  void 
  collectInterfacesForCommunicationGraph(ctaSet<ctaString>* lUsedInterfaces)
  {}

  virtual void 
  Print(ostream& s) const
  {
    s << "FALSE";
  }

  virtual void 
  PrintHyTech(ostream& pOutStream) const
  {
    pOutStream << "False";
  }

  virtual ddmRegion
  mkDdmRegion(const reprAutomaton& pAuto,
	      bool pDoubleSize,
	      const ctaModule* pCtaModule)
  {
    const ddmAutomaton* lAuto = pAuto.getDerivedObjDdm();

    return ddmRegion((pDoubleSize ? 2 : 1)*lAuto->getVarIds().size(), 
		     false);
  }

  virtual bddConfig
  mkBddConfig(const bddSymTab* pSymTab,
	      const ctaModule* pCtaModule)
  {
    return bddConfig(pSymTab, false);
  }  
};
#endif

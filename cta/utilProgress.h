//-*-Mode: C++;-*-

/*
 * File:        utilProgress.h
 * Purpose:     Progress Indicator Class for cta-library
 * Author:      Sebastian Schmerl
 * Created:     1999
 * Copyright:   (c) 1999, University of Brandenburg, Cottbus
 */

#ifndef _utilProgress_h
#define _utilProgress_h


#include <iostream>

#include "anaObject.h"
#include "utilCmdLineOptions.h"

class utilProgress : private anaObject
{
 private:
  long mProgressEventCount;
  // How often we have to call GiveEvent
  //   to the ProgressEvent to get one output.
  long mProgressMaxEventCount;

  // The character we have to write on screen.
  char mChar;

  // It should be not allowed to use the standard operators.
  utilProgress(const utilProgress&);
  void operator=(const utilProgress&);
  void operator,(const utilProgress&);

 public:
  // Constructor and destructor.
  utilProgress(char pChar, int pProgressMaxEventCount)
    : mProgressMaxEventCount(pProgressMaxEventCount),
      mChar(pChar)
  {
    mProgressEventCount = 0;
  }

  ~utilProgress()
  {
    if(pubCmdLineOpt->Verbose())
    {
      cout << endl;
    }    
  }

  // Set value, how much have to call GiveEvent to the ProgressEvent.
  void SetMaxEventCount(long t)
  {
    if (t>=1) 
    {
      mProgressMaxEventCount = t;
    }
  }

  // Get value, how much have to call GiveEvent to the ProgressEvent.
  long GetMaxEventValue() const
  {
    return mProgressMaxEventCount;
  }

  // The ProgressEvent, print a '*'.
  void ProgressEvent()
  {
    if (pubCmdLineOpt->Verbose())
    {
      cout << mChar; 
      cout.flush();
    }
  }

  // Count the events, until it reached the value of MaxEventCount 
  // then create a Progressevent.
  void GiveEvent()
  {
    ++mProgressEventCount;
    if (mProgressEventCount >= mProgressMaxEventCount) 
    {
      mProgressEventCount = 0;
      this->ProgressEvent();
    }
  }
};

#endif

//-*-Mode: C++;-*-

/*
 * File:        ctaSet.h
 * Purpose:     Set Class for cta-library
 * Author:      Andy Heinig
 * Created:     2000
 * Copyright:   (c) 2000, University of Brandenburg, Cottbus
 */

#ifndef _ctaSet_h
#define _ctaSet_h

// for new sdt-lib
#include <iterator>
#include <set>

#include "ctaObject.h"

template<class E> class ctaSet : public set<E>, private ctaObject
{
private:
  // It should be not allowed to use the standard operators.
  ctaSet(const ctaSet&);
  void operator=(const ctaSet&);
  void operator,(const ctaSet&);
	
public:
  // constructor
  ctaSet()
  {}

  ~ctaSet()
  {}
  
  // traversion for elements idx of list S
#define ctaForAll_Set(S, idx, TYPE) for(ctaSet<TYPE>::iterator idx = (S)->begin();  idx != (S)->end() ; ++idx )
  
  void append(const E item)
  {
    insert(item);
  }

  void join(ctaSet<E>* l)
  {
    // set l inserting as a range
    ctaForAll_Set(l, it, E)
    {
      this->insert(*it);
    }
  }
  
};

template<class E> ostream& operator << (ostream& out, ctaSet<E>* obj)
{
  // it works with an empty set
  ctaForAll_Set(obj, idx, E)
  {
    // we take the idx-th element in set parsedModules
    out << *idx; 
  }
  return out;
}

#endif

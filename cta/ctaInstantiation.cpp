/*
 * File:        ctaInstantiation.cpp
 * Purpose:     Instantiation Class for cta-library
 * Author:      Dirk Beyer
 * Created:     1998
 * Copyright:   (c) 1998, University of Brandenburg, Cottbus
 */

#include "ctaInstantiation.h"

#include "ctaAutomaton.h"
#include "ctaComponent.h"
#include "ctaConfiguration.h"
#include "ctaErrMsg.h"
#include "ctaModule.h"
#include "ctaSystem.h"

ctaInstantiation::~ctaInstantiation()
{
  if(mInstanceName)
  {
    delete mInstanceName;
  }

  if(mTemplateName)
  {
    delete mTemplateName;
  }

  // templateModule is an association

  if(mIdentNames)
  {
    delete mIdentNames;
  }

  if(mIdentComps)
  {
    // we have to delete all the entries because the default
    // destructor of ctaMap deletes also the value objects
    //mIdentComps->erase(mIdentComps->begin(), mIdentComps->end());
    mIdentComps->clear();

    // then we delete the ctaMap
    delete mIdentComps;
  }

  delete mInitialConfig;
}

// aheinig 15.03.2000
// false: if used >, <, !=
// true : if used =, <=, >= 
bool 
ctaInstantiation::CheckNonStrictness(const ctaModule* pModule) const
{
  ctaModule* lTmpModule =
                 pubParsedSystem->findModule(*mTemplateName);
  if( lTmpModule == NULL)
  {
    CTAERR << "Runtime error: Modul"
           << *mTemplateName 
           << "not found."
	   << endl;	  
    CTAPUT;
    return false;
  }
  else
  {
    return ( lTmpModule->CheckNonStrictness()
	     &&  mInitialConfig->CheckNonStrictness(pModule) );
  }
}

bool 
ctaInstantiation::CheckBddConstraint(const ctaModule* pModule) const
{
  return ( mInitialConfig->CheckBddConstraint(pModule) );
}

void 
ctaInstantiation::CheckConsistency(ctaSystem* pSystem,
				   ctaModule* pModule)
{
  // Check for cyclic instantiations is done by
  // ctaSystem::CheckConsistency.

  // We have to check that the templateModule is declared 
  // in (pSystem).
  // We try to find the declaration of module used as template in
  // the system.
  // Assumption: No cycles in instantiations.

  // Set the reference to the templateModule. If not found then NULL.
  this->SetTemplateModule(pSystem->findModule(*this->GetTemplateName()));
  if( this->GetTemplateModule() == NULL)
  {
    CTAERR << "Module \'"
	   << this->GetTemplateName() 
	   << "\', used as template, is not declared."
	   << endl;
    CTAPUT;
  }

  // We have to check that one identifier only one time is connected,
  //  i. e. 'ident' must be a function.
  // It is fulfilled because the connections are collected in a map.

  {
    // We have to check whether 'ident' is injectiv.
    set<ctaString>* lTmpSet = new set<ctaString>;
    ctaForAll_Map(this->GetConnections(), idx, ctaString*)
    {
      if (!lTmpSet->insert(*(idx->second)).second)
      {
	// false, is not injective
	CTAERR << "Identification function "
	       << "in instantiation is not injective " 
	       << "in module " << pModule->GetName()
	       << " INST " << this->GetInstanceName() << "."
	       << endl;	  
	CTAPUT;
	break;
      }
    }
    lTmpSet->clear();
    delete lTmpSet;
  }

  // mvogel 2002-06-10 Added context check for unconnected interface
  // components, resulting in problems with variable ordering.
  
  ctaForAll_Map(this->GetTemplateModule()->GetInterface(), idx,
		ctaComponent*)
  {
    // We have to check if all interface components of template module are
    // connected in this instantiation except LOCAL interface components.
    if( (this->GetConnections()->find(idx->first)
	 == this->GetConnections()->end())
	&& (idx->second->GetRestrictionType() != ctaComponent::LOCAL) )
    {
      CTAERR << "Interface component \""
	     << idx->first << "\"" << endl << "of template module " 
	     << this->GetTemplateModule()->GetName() <<  " not connected"
	     << endl
	     << "in module " << pModule->GetName()
	     << " at instance " << GetInstanceName() << "."
	     << endl;      
      CTAPUT;
    }
  }


  // We want to build a map pointing to the component objects.
  mIdentComps = new ctaMap<ctaComponent*>();

  ctaForAll_Map(this->GetConnections(), idx, ctaString*)
  {
    // We have to check whether used components are declared.
    // Check of domain.
    if(this->GetTemplateModule()->GetInterface()->find(idx->first)
       == this->GetTemplateModule()->GetInterface()->end() )
    {
      CTAERR << "Used identifier \""
	     << idx->first << "\" undeclared (in domain of ident)"
	     << endl
             << "in module " << pModule->GetName()
	     << " INST " << GetInstanceName() << "."
	     << endl;      
      CTAPUT;
    }
    // Check range.
    if( pModule->GetInterface()->find(*(idx->second))
	== pModule->GetInterface()->end() )
    {
      CTAERR << "Used identifier \""
	     << *(idx->second) << "\" undeclared (in range of ident)"
	     << endl
             << "in module " << pModule->GetName()
	     << " INST " << this->GetInstanceName() << "."
	     << endl;     
      CTAPUT;
    }

    // We have to set the map for components.
    mIdentComps->append(&(idx->first),
			pModule->GetInterface()
			  ->find(*(idx->second))->second);
  }

  {
    // Data types must match.
    ctaForAll_Map(mIdentComps, idx, ctaComponent*)
    {
      ctaComponent* lhs;     // left-hand-side of connection
      lhs = ( *(mTemplateModule->GetInterface()) ) [idx->first];
      ctaComponent* rhs;     // right-hand-side of connection
      rhs = idx->second;
      // the data types must be the same
 
      if( lhs->GetDataType() != rhs->GetDataType() )
      {
	CTAERR << "data type mismatch "
	       << "at connection of: \'" << idx->first << "\'" 
	       << endl
	       << "in module \'" << pModule->GetName()
	       << "\' INST \'" << GetInstanceName() << "\'."
	       << endl;      
	CTAPUT;
      }
      // we have to check the right use of restriction types
    
      // we have to check whether we have LOCAL in domain
      if( lhs->GetRestrictionType() == ctaComponent::LOCAL)
      {
	CTAERR << "restriction type mismatch "
	       << "at connection of: \'" << idx->first << "\'" 
	       << endl
	       << "in module \'" << pModule->GetName()
	       << "\' INST \'" << GetInstanceName() << "\'."
	       << endl;
	CTAPUT;
      }
      // we have to check whether we have OUTPUT in domain
      if( (lhs->GetRestrictionType() == ctaComponent::OUTPUT) &&
	  (rhs->GetRestrictionType() != ctaComponent::OUTPUT) &&
	  (rhs->GetRestrictionType() != ctaComponent::LOCAL)
	  )
      {
	CTAERR << "restriction type mismatch "
	       << "at connection of: \'" << idx->first << "\'"
	       << endl
	       << "in module \'" << pModule->GetName()
	       << "\' INST \'" << GetInstanceName() << "\'."
	       << endl;      
	CTAPUT;
      }
      // we have to check whether we have MULTREST in domain
      if( ( lhs->GetRestrictionType() == ctaComponent::MULTREST )&&
	  ( rhs->GetRestrictionType() != ctaComponent::MULTREST ) &&
	  ( rhs->GetRestrictionType() != ctaComponent::OUTPUT ) &&
	  ( rhs->GetRestrictionType() != ctaComponent::LOCAL )
	  )
      {
	CTAERR << "restriction type mismatch "
	       << "at connection of: \'" << idx->first << "\'" 
	       << endl
	       << "in module \'" << pModule->GetName()
	       << "\' INST \'" << GetInstanceName() << "\'."
	       << endl;      
	CTAPUT;
      }
      // if we have INPUT then it can be connected with all restriction types
      // nothing to check
    }  
  }  

 

  // Check of the initial configuration.
  mInitialConfig->CheckConsistency(this->GetTemplateModule());

}

void 
ctaInstantiation::CountRestrictions(ctaComponent* pCompo,
				    int& pNrMultrestRestrictions,
				    int& pNrOutputRestrictions)
{
  // Check if (pCompo) is identified in the current instantiation
  //   with something.  If yes, check if it is identified
  //   with a MULTREST or an OUTPUT.
  ctaForAll_Map(this->GetIdentComps(), connection, ctaComponent*)
  {
    // Compare (pCompo) with (connection->second).
    if(pCompo == connection->second)
    {
      // We found an occurence of the current component in the
      //   interface.
      
      // Is this an OUTPUT or a MULTREST-component?
      if( (* (this->GetTemplateModule()->GetInterface()) )
	     [connection->first]
	     ->GetRestrictionType()
	  == ctaComponent::OUTPUT )
      {
	++pNrOutputRestrictions;
      }

      if( (* (this->GetTemplateModule()->GetInterface()) )
	     [connection->first]
	     ->GetRestrictionType()
	  == ctaComponent::MULTREST )
      {
	++pNrMultrestRestrictions;
      }
    }
  }
}

ostream& operator << (ostream& out, const ctaInstantiation* obj)
{
  out << "  INST " << obj->GetInstanceName() << endl;
  out << "  FROM " << obj->GetTemplateName() << endl;
  out << "  WITH" << endl;
  out << "  {" << endl;
  
  // it works with an empty map
  ctaForAll_Map(obj->GetConnections(), idx, ctaString*)
  {
    out << "    " << idx->first << " AS " << idx->second << ";" << endl;
  }
  
  out << "  }" << endl;
  return(out);
}


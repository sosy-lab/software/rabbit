/*
 * File:        utilCmdLineOptions.cpp
 * Purpose:     Class to handle and set the Cmd. line options for cta-library
 * Author:      Sebastian Schmerl
 * Created:     2000
 * Copyright:   (c) 2000, University of Brandenburg, Cottbus
 */

#include "utilCmdLineOptions.h"
#include "utilGetOpt.h"

#include "ctaModule.h"
#include "bddBdd.h"

// This methode analyze the cmd line options and set the flags.
void utilCmdLineOptions::AnalyzeOptions(int pargc, char ** pargv)
{
  int lc;
  char lc1;
  ctaString lOutFileName("");
  ctaString lInFileName("");
 
  // We have no option.
  if (pargc==1) 
  {
    printShortHelp();
    exit(1);  
  }
  else
  {
    while (1)
    {
      lc = getopt (pargc, pargv, "ab:c:hm:o:p:v");
      if (lc == -1) break;

      switch( lc ) 
      {
      case 'b':
	lc1 = *optarg;
	switch( lc1 )
	{
	case '0':
	  mUseVariableOrdering = true;
	  mUseVariableOrderingAfterRandom = true;
	  break;
	case '1':
	  mTransitionProductType = mUniteOnlySyncs;
	  break;
	case '2':
	  mTransitionProductType = mUniteAllTransitionsOfAnAutomaton;
	  break;
	case '3':
	  mTransitionProductType = mUniteAllTransitions;
	  break;
	case '7':
	  mTransitiveClosureOfDiskreteTransition = true;
	  break;
	case '8':
	  mRandomDiskreteTransitionOrder = true;
	  break;
	case '9': 
	  // At first call the method for the variable ordering, this includes
	  // the method for random variable ordering.
	  mUseVariableOrdering = true;
	  // Then chake the variables...
	  mRandomVariableOrder = true;
	  // And now we will not order the variables.
	  mUseVariableOrderingAfterRandom = false;
	  break;
	default:
	  cerr << "Please select an option for -b. " << endl;
	  exit(1);
	}
	break;
      case 'c':
	lc1 = *optarg;
	switch( lc1 )
	{
	case '1':
	  mCountMaxNumbersOfBddNodes = true;
	  break;
	case '2':
	  mPrintBddSizeAfterComputeFixPnt = true;
	  break;  
	default:
	  cerr << "Please select an option for -c. " << endl;
	  exit(1);
	}
	break;
      case 'h':
	printHelp();
	exit(1);
	break;
      case 'm':
	lc1 = *optarg;
	switch( lc1 )
	{
	case '0' :               // Only for experiments.
	  mMemorySizeForBdd = 0;
	  break;
	case '1':
	  mMemorySizeForBdd = 1; // Only for experiments.
	  break;
	case '2':
	  mMemorySizeForBdd = 2;
	  break;
	case '3':
	  mMemorySizeForBdd = 3;
	  break;
	case '4':
	  mMemorySizeForBdd = 4;
	  break;
	case '5':
	  mMemorySizeForBdd = 5;
	  break;  
	case '6':
	  mMemorySizeForBdd = 6;
	  break;
	case '7' :
	  mMemorySizeForBdd = 7;
	  break;
	case '8' :
	  mMemorySizeForBdd = 8;
	  break;
	case '9' :
	  mMemorySizeForBdd = 9;
	  break;
	default:
	  cerr << "Please select an option for -m. " << endl;
	  exit(1);
	}
	break;
      case 'o':
	// filename of the output file.
	lOutFileName = ctaString(optarg);
	break;
      case 'p':
	mOptFlgHytechPrinting = true;
	mModuleToHandle = ctaString(optarg);
	break;
      case 'v':
	mOptFlgVerbose = true;
	break;
      default:
	printShortHelp();
	exit(1);
	break;
      }
    }
  }
  
  if( optind < pargc )
  {
    lInFileName = ctaString(pargv[optind]);
    mCtaFileName = lInFileName;
    optind++;
    for ( ; optind < pargc; optind++)
    {
      cout << "argument <" << ctaString(pargv[optind]) 
	   << "> ignored." << endl; 
    }
  }
  else
  {
    cout << "Runtime error: can't open input file" << endl;
    exit(1);
  }
  
  if(this->mOptFlgVerbose)
  {
    cout << "Verbose option is set." << endl;
    cout << "Input file is <" << lInFileName << ">." << endl; 
    if (mOptFlgHytechPrinting)
    {
      cout << "Printing HyTech in file <" << lOutFileName 
	   << ">." << endl;
    }	
  }
  
  // open the input file stream
  // The contruct (.data()) have to used, because ifstream need a (*char) 
  // as argument.
  mInputStream = new ifstream(lInFileName.data(),ios::in);
  
  // We use the (operator !()) from  class (ios), which
  //   returns true if ios::fail is true.
  if (!(*mInputStream))
  {
    cerr << "Runtime error: can't open input file" << endl;
    exit(2);
  }
  
  // OutputFile given?
  if(lOutFileName!="")
  {
    mOutputStream = new ofstream(lOutFileName.data(),ios::out);
    // We use the (operator !()) from  class (ios), which
    //   returns true if ios::fail is true.
    if (!(*mOutputStream))
    {
      cerr << "Runtime error: Can't open HyTech output file <" 
	   << lOutFileName <<">." << endl;
      exit(2);
    }
    
    // Achtung: Speicherfreigabe fuer mOutputStream ist nicht implementiert.
  }
}

//=======================================================================
void
utilCmdLineOptions::printIntro()
{
  cout << "Rabbit 2.1 - The verification tool for Cottbus Timed " 
       << "Automata (CTA)." << endl
       << "It supports reachability analysis and refinement checking." << endl
       << "For closed timed automata it uses a BDD representation." << endl
       << "For hybrid automata it uses a DDM representation" << endl
       << "(only reachability analysis supported in this version)." << endl
       << "(c) 1998-2002 BTU Cottbus, Germany," << endl 
       << "Software Systems Engineering Research Group, 2002-08-30." << endl
       << "http://www-sst.informatik.tu-cottbus.de/~db/Rabbit" << endl
       << endl;
}

//=======================================================================
void
utilCmdLineOptions::printHelp()
{
  printIntro();
  cout << "Usage:  rabbit [options] ctafile." << endl
       << endl
       << "where options include:" << endl
       << "    -v                 set the verbose option (for a lot of comments)." << endl
       << "    -h                 this help text" << endl
    //       << "    -p <module>        prints hytech-like output of the model" << endl
       << "    -o <filename>      specifies output filename " 
       << "(default is stdout)" << endl
       << endl
       << "Verification options: (only for BDD representation)" << endl
       << "    -b0     estimate-based variable ordering " << endl
       << "    -b1     partial transition relation (default)" << endl
  // Partial united is forbidden.
  //       << "    -b2     partial united transition relation" << endl
       << "    -b3     united transition relation" << endl
    
       << "    -b7     transitive closure of every transition (regarding b1-b3)" << endl
       << "    -b8     random transition ordering " << endl
       << endl
       << "Counting options: (only for BDD representation, "
       << "for performance analysis)" 
       << endl
       << "  Attention! The following options need a lot of computation time." 
       << endl
       << "    -c1     prints the size (in number of bdd-nodes) of "
       <<                "the largest BDD" 
       << endl
       << "              representing the reachable set of configurations" 
       << endl
       << "              which occurs within the "
       <<                "discrete fixed point iteration" 
       << endl
       << "              between two time steps" 
       << endl  
       << "    -c2     prints the size (in number of bdd-nodes) of the BDD" 
       << endl
       << "              representing the reachable set of configurations" 
       << endl
       << "              after each discrete fixed point iteration" 
       << endl  
       << endl 
       << "Memory options: (only for BDD representation)" << endl
       << "  Amount of memory for BDD package" << endl
       << "    -m2     use  12 MB memory" << endl
       << "    -m3     use  25 MB memory" << endl
       << "    -m4     use  50 MB memory (default)" << endl
       << "    -m5     use 100 MB memory" << endl
       << "    -m6     use 200 MB memory" << endl
       << "    -m7     use 400 MB memory" << endl
       << "    -m8     use 800 MB memory" << endl
       << "    -m9     use 800 MB memory (with minimum cache)" << endl 
       << endl << endl;
  
  cout.flush();
}

//=======================================================================
void
utilCmdLineOptions::printShortHelp()
{
  printIntro();
  cout << "For an usage summary type rabbit -h" << endl
       << endl;
}

//=======================================================================
void
utilCmdLineOptions::initBDD() const
{
  // Initialize BDD package (i.e. static members of class bddBdd).
  // Parameters: max. number of nodes, 
  //            elements of node-hash,
  //            elements of operation-cache, 
  //            elements of cache for computing the number of configurations.
  // 2nd, 3rd and 4th parameter are binary logarithms!
  // Recommendation: Do not change the relation of the sizes.
  
  switch( mMemorySizeForBdd )
  {
    case 2:
      if(pubCmdLineOpt->Verbose())
      {
	cerr << "Using 12.5 MB memory for BDD package." << endl;
      }
      bddBdd::init(500000, 18, 18, 14);
      break;
    case 3:
      if(pubCmdLineOpt->Verbose())
      {
	cerr << "Using 25 MB memory for BDD package." << endl;
      }
      bddBdd::init(1000000, 19, 19, 15);
      break;
    case 4:
      // Original settings. (uses 55 MB RAM)
      if(pubCmdLineOpt->Verbose())
      {
	cerr << "Using 50 MB memory for BDD package." << endl;
      }
      bddBdd::init(2000000, 20, 20, 16);
      break;
    case 5:
      if(pubCmdLineOpt->Verbose())
      {
	cerr << "Using 100 MB memory for BDD package." << endl;
      }
      bddBdd::init(4000000, 21, 21, 17);
      break;
    case 6:
      if(pubCmdLineOpt->Verbose())
      {
	cerr << "Using 200 MB memory for BDD package." << endl;
      }
      bddBdd::init(8000000, 22, 22, 18);
      break;
    case 7:
      if(pubCmdLineOpt->Verbose())
      {
	cerr << "Using 400 MB memory for BDD package." << endl;
      }
      bddBdd::init(16000000, 23, 23, 19);
      break;
    case 8:
      if(pubCmdLineOpt->Verbose())
      {
	cerr << "Using 800 MB memory for BDD package." << endl;
      }
      bddBdd::init(32000000, 24, 24, 20);
      break;
    case 9:
      if(pubCmdLineOpt->Verbose())
      {
	cerr << "Using 800 MB memory for BDD package " 
	     << "(with minimal  cache)" << endl;
      }
      bddBdd::init(52000000, 19, 19, 15);
      break;

      //////////////////////////////////////////////////////////////
    case 1:               // Only for experiments.
      if(pubCmdLineOpt->Verbose())
      {
	cerr << "Using 3.2 MB memory for BDD package." << endl;
      }
      bddBdd::init(125000, 16, 16, 12);
      break;
    case 0:               // Only for experiments.
      if(pubCmdLineOpt->Verbose())
      {
	cerr << "Using 0.8 MB memory for BDD package." << endl;
      }
      bddBdd::init(31250, 14, 14, 10);
      break;
    
  }
}    


//-*-Mode: C++;-*-

/*
 * File:        ctaRestOr.h
 * Purpose:     Or-Restriction class for cta-library
 * Author:      H. Rust
 * Created:     1998
 * Copyright:   (c) 1998, University of Brandenburg, Cottbus
 */

#ifndef _ctaRestOr_h
#define _ctaRestOr_h


class ctaComponent;
#include "ctaMap.h"
class ctaModule;
#include "ctaRestriction.h"

class ctaRestOr : public ctaRestriction
{
private:
  ctaRestriction *mR1;
  ctaRestriction *mR2;

  friend ctaRestriction*
  ctaRestriction::mkOr(ctaRestriction* p1, ctaRestriction* p2);

  // constructor and destructor
private:
  // Only (ctaRestriction::mkOr) should call this constructor.
  ctaRestOr(ctaRestriction *pR1,
	    ctaRestriction *pR2)
    : mR1(pR1),
      mR2(pR2)
  {}

  // It should be not allowed to use the standard operators.
  ctaRestOr(const ctaRestOr&);
  void operator=(const ctaRestOr&);
  void operator,(const ctaRestOr&);

public:
  ~ctaRestOr()
  {
    if(mR1) 
    {
      delete mR1;
    }

    if(mR2) 
    {
      delete mR2;
    }
  }
  
  // accessor methods
  // Service methods.

  // aheinig 15.03.2000
  // false: if used >, <, !=
  // true : if used =, <=, >= 
  bool CheckNonStrictness(const ctaModule* pCtaModule) const
  {
    return ( mR1->CheckNonStrictness(pCtaModule) && 
	     mR2->CheckNonStrictness(pCtaModule) );
  }

  // aheinig 22.03.2000
  // true:  if CheckBddConstraint true for both sides
  // false: else
  bool
  CheckBddConstraint(const ctaModule* pCtaModule, 
		     const bool logicalOrIsAllowed) const
  {
    if( logicalOrIsAllowed )
    {
      return ( mR1->CheckBddConstraint(pCtaModule, true) && 
	       mR2->CheckBddConstraint(pCtaModule, true) );
    } 
    return false;
  }

  // Clone (this).
  ctaRestriction*
  cloneSetPrefixAndIdent(const ctaString *const pPrefix,
			 ctaMap<ctaString*>* pIdentNames) const
  {
    // Clone the left and right operand of (this).
    ctaRestriction* cloneLeft  = mR1->cloneSetPrefixAndIdent(pPrefix, 
							     pIdentNames);
    ctaRestriction* cloneRight = mR2->cloneSetPrefixAndIdent(pPrefix, 
							     pIdentNames);

    // Create a new RestOr, this is the clone.
    ctaRestriction* theClone = new ctaRestOr(cloneLeft, cloneRight);

    return theClone;
  }

  // Check the derivation for correctness.
  void
  CheckDerivation(const ctaModule* const pModule) const 
  {
    this->mR1->CheckDerivation(pModule);
    this->mR2->CheckDerivation(pModule);
  }

  // This method is for context analysis.
  // We give the parent module for accessing module features.
  virtual void 
  CheckConsistency(const ctaModule* const pModule)
  {
    mR1->CheckConsistency(pModule);
    mR2->CheckConsistency(pModule);
  }

  virtual bool
  VariableOccurs(ctaString *pId)
  {
    return 
      mR1->VariableOccurs(pId) 
      || mR2->VariableOccurs(pId);
  }

  virtual bool
  VariableOccursTicked(ctaString *pId)
  {
    return 
      mR1->VariableOccursTicked(pId) 
      || mR2->VariableOccursTicked(pId);
  }

  // aheinig 2000-11-02
  // Collect all used signals and variables for this automaton.
  void 
  collectInterfacesForCommunicationGraph(ctaSet<ctaString>* lUsedInterfaces)
  {
    mR1->collectInterfacesForCommunicationGraph(lUsedInterfaces);
    mR2->collectInterfacesForCommunicationGraph(lUsedInterfaces);
  }

  virtual void 
  Print(ostream& s) const
  {
    s << mR1 << " OR " << mR2;
  }

  virtual void 
  PrintHyTech(ostream& pOutStream) const
  {
    //Save the old error prefix und set the new one.
    const ctaString *lOldPrefix = pubErrMsg->GetPrefix();
    pubErrMsg->SetPrefix( new ctaString("PrintHyTech error: (in ctaRestOr: ") );

    pOutStream << endl << "-- HyTech can't handle 'OR' operation, replace manualy." << endl;
    CTAERR << "HyTech can't handle 'OR' operation." << endl;
    CTAPUT;

    mR1->PrintHyTech(pOutStream);
    pOutStream << " OR ";
    mR2->PrintHyTech(pOutStream);

    //Set the old error msg prefix.
    delete pubErrMsg->GetPrefix();
    pubErrMsg->SetPrefix(lOldPrefix);
  }

  virtual ddmRegion mkDdmRegion(const reprAutomaton& pAuto,
			      bool pDoubleSize,
			      const ctaModule* pCtaModule)
  {
    ddmRegion result(mR1->mkDdmRegion(pAuto, pDoubleSize, pCtaModule));
    result.unite(mR2->mkDdmRegion(pAuto, pDoubleSize, pCtaModule));
    result.normalize();
    
    return result;
  }

  virtual bddConfig mkBddConfig(const bddSymTab* pSymTab,
				const ctaModule* pCtaModule)
  {
    bddConfig result(mR1->mkBddConfig(pSymTab, pCtaModule));
    result.unite(mR2->mkBddConfig(pSymTab, pCtaModule));
    
    return result;
  }
};
#endif

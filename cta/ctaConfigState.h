//-*-Mode: C++;-*-

/*
 * File:        ctaConfigState.h
 * Purpose:     State-Configuration class for cta-library
 * Author:      H. Rust
 * Created:     1998
 * Copyright:   (c) 1998, University of Brandenburg, Cottbus
 */

#ifndef _ctaConfigState_h
#define _ctaConfigState_h


class ctaAutomaton;
#include "ctaComponent.h"
#include "ctaConfigLinRest.h"
#include "ctaMap.h"
class ctaModule;
#include "ctaState.h"

class ddmAutomaton;
class ddmConfig;

class ctaConfigState : public ctaConfiguration
{
private:

  // Aggregated.
  const ctaString* mAutomatonName;
  const ctaString* mState;

  // It should be not allowed to use the standard operators.
  ctaConfigState(const ctaConfigState&);
  void operator=(const ctaConfigState&);
  void operator,(const ctaConfigState&);

public:
  // Constructor and destructor.
  ctaConfigState(const ctaString* pAutomatonName,
		 const ctaString* pState)
    : mAutomatonName(pAutomatonName),
      mState(pState)
  {}

  ~ctaConfigState()
  {
    delete mAutomatonName;
    delete mState;
  }
  
  // accessor methods

  const ctaString*
  GetState() const
  {
    return mState;
  }

  const ctaString*
  getAutomatonName() const
  {
    return mAutomatonName;
  }

  // aheinig 26.09.2000
  // Clone (this). 
  ctaConfiguration* 
  cloneSetPrefixAndIdent(const ctaString *const pPrefix,
			 ctaMap<ctaString*>* pIdentNames) const
  {
    // Create the cloned strings.
    ctaString* lAutoNameClone = new ctaString(*pPrefix + *mAutomatonName);

    ctaString* lStateClone = new ctaString(*pPrefix + *mState);
    if(pIdentNames != NULL)
    {
      // Replace the variable name by the name defined in the instantiation.
      ctaForAll_Map(pIdentNames, itIdent, ctaString*)
      {
	if(itIdent->first == *mState)
	{
	  *lStateClone = *itIdent->second;
	}
      }
    }

    // Create the cloned configState.
    ctaConfiguration* lConfigStateClone = new ctaConfigState(lAutoNameClone,
							     lStateClone);
				     
    return lConfigStateClone;
  } 

  // Service methods.

  // aheinig 15.03.2000 
  // false: if used >, <, != by clocks
  // true : else
  virtual bool CheckNonStrictness(const ctaModule* pCtaModule) const
  {
    return true;   
  }

  // aheinig 23.03.2000
  // only true
  virtual bool CheckBddConstraint(const ctaModule* pCtaModule) const
  {
    return true;
  }

  // This method is for context analysis.
  // We give the parent module for accessing module features.
  virtual void CheckConsistency(const ctaModule* pModule);

  virtual void Print(ostream& s) const;

  virtual ddmConfig
  mkDdmConfig(const reprAutomaton& pAuto,
	      const ctaModule* pCtaModule) const;

  virtual bddConfig
  mkBddConfig(const bddSymTab* pSymTab,
	      const ctaModule* pCtaModule) const;

  virtual ctaConfiguration*
  collectStatesForAutomaton(const ctaModule* pCtaModule, 
			    const ctaString* pAutoName) const
  {
    ctaConfiguration* result = new ctaConfigLinRest(new ctaRestFalse());

    // Collect state names of the current automaton.
    if(*mAutomatonName == *pAutoName)
    {
      result = ctaConfiguration::mkOr(
	          result,
	          new ctaConfigState(new ctaString(*pAutoName),
				     new ctaString(*mState)
				     )
		  );
    }

    return result;
  }
};
#endif

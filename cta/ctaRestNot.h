//-*-Mode: C++;-*-

/*
 * File:        ctaRestNot.h
 * Purpose:     Not-Restriction class for cta-library
 * Author:      H. Rust
 * Created:     1998
 * Copyright:   (c) 1998, University of Brandenburg, Cottbus
 */

#ifndef _ctaRestNot_h
#define _ctaRestNot_h


class ctaAutomaton;
class ctaComponent;
#include "ctaMap.h"
class ctaModule;
#include "ctaRestriction.h"

class ctaRestNot : public ctaRestriction
{
private:
  ctaRestriction *mR1;

  friend ctaRestriction*
  ctaRestriction::mkNot(ctaRestriction* p1);

  // constructor and destructor
private:
  // Only (ctaRestriction::mkNot) should call this constructor.
  ctaRestNot(ctaRestriction *pR1)
    : mR1(pR1)
  {}

  // It should be not allowed to use the standard operators.
  ctaRestNot(const ctaRestNot&);
  void operator=(const ctaRestNot&);
  void operator,(const ctaRestNot&);

public:
  ~ctaRestNot()
  {
    if(mR1) 
    {
      delete mR1;
    }
  }
  
  // accessor methods
  // Service methods.

  // aheinig 15.03.2000
  // false: if used >, <, != by clocks
  // true : else 
  bool CheckNonStrictness(const ctaModule* pCtaModule) const
  {
    return mR1->CheckNonStrictness(pCtaModule);
  }

  // aheinig 22.03.2000
  // only false: we forbid NOT operation in bdd
  bool CheckBddConstraint(const ctaModule* pCtaModule, 
              const bool logicalOrIsAllowed) const
  {
    return false;
  }

  // Clone (this).
  ctaRestriction*
  cloneSetPrefixAndIdent(const ctaString *const pPrefix,
             ctaMap<ctaString*>* pIdentNames) const
  {
    // Clone the operand from (this).
    ctaRestriction* cloneOperand = mR1->cloneSetPrefixAndIdent(pPrefix, 
                                   pIdentNames);

    // Create a simplified clone of (this).
    ctaRestriction* theClone = new ctaRestNot(cloneOperand);

    return theClone;
  }

  // Check the derivation for correctness.
  void
  CheckDerivation(const ctaModule* const pModule) const 
  {
    this->mR1->CheckDerivation(pModule);
  }

  // This method is for context analysis.
  // We give the parent module for accessing module features.
  virtual void 
  CheckConsistency(const ctaModule* const pModule)
  {
    mR1->CheckConsistency(pModule);
  }

  virtual bool
  VariableOccurs(ctaString *pId)
  {
    return mR1->VariableOccurs(pId);
  }

  virtual bool
  VariableOccursTicked(ctaString *pId)
  {
    return mR1->VariableOccursTicked(pId);
  }

  // aheinig 2000-11-02
  // Collect all used signals and variables for this automaton.
  void 
  collectInterfacesForCommunicationGraph(ctaSet<ctaString>* lUsedInterfaces)
  {
    mR1->collectInterfacesForCommunicationGraph(lUsedInterfaces);
  }

  virtual void 
  Print(ostream& s) const
  {
    s << "NOT(" << mR1 << ")";
  }

  virtual void 
  PrintHyTech(ostream& pOutStream) const
  {
    //Save the old error prefix und set the new one.
    const ctaString *lOldPrefix = pubErrMsg->GetPrefix();
    pubErrMsg->SetPrefix( new ctaString("PrintHyTech error: (in ctaRestNot: ") );

    pOutStream << endl << "-- HyTech can't handle 'NOT' operation, replace manualy." << endl;
    CTAERR << "HyTech can't handle 'NOT' operation." << endl;
    CTAPUT;

    pOutStream << "NOT( ";
    mR1->PrintHyTech(pOutStream);
    pOutStream << ")";

    //Set the old error msg prefix.
    delete pubErrMsg->GetPrefix();
    pubErrMsg->SetPrefix(lOldPrefix);
  }

  virtual ddmRegion
  mkDdmRegion(const reprAutomaton& pAuto,
          bool pDoubleSize,
          const ctaModule* pCtaModule)
  {
    ddmRegion result(mR1
           ->mkDdmRegion(pAuto, pDoubleSize, pCtaModule)
           .mkComplement());
    result.normalize();

    return result;
  }

  // NOT is forbidden when using the bdd representation.
  // Cf. for CheckBddConstraint().
  virtual bddConfig
  mkBddConfig(const bddSymTab* pSymTab,
          const ctaModule* pCtaModule)
  {
    bddConfig result(mR1->mkBddConfig(pSymTab, pCtaModule));
    result.complement();
    return result;
  }
};
#endif

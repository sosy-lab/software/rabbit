/*
 * File:        ctaConfigState.cpp
 * Purpose:     ConfigState Class for cta-library
 * Author:      H. Rust
 * Created:     1998
 * Copyright:   (c) 1998, University of Brandenburg, Cottbus
 */


#include "ctaConfigState.h"

#include "ctaAutomaton.h"
#include "ctaErrMsg.h"
#include "ctaModule.h"

#include "ddmConfig.h"
#include "bddConfig.h"

// This method is for context analysis.
// We give the parent module for accessing module features.
void ctaConfigState::CheckConsistency(const ctaModule* pModule)
{
  // Look for automaton.
  ctaMap<ctaAutomaton*>::iterator 
    itAuto = pModule->GetAutomata()->find(*mAutomatonName);
  
  if(itAuto == pModule->GetAutomata()->end())
  {
    // Automaton not found.
    CTAERR << "Did not find automaton \'" << mAutomatonName 
	   << "\' in module \'" << pModule->GetName()
	   << "\'." << endl;
    CTAPUT;
  }
  else
  {
    // Check that the state occurs in the automaton.
    // Look for (*s) in the state map.
    ctaMap<ctaState*>::const_iterator it 
    = itAuto->second->GetStates()->find(*mState);
    
    if(it == itAuto->second->GetStates()->end())
    {
      // State not found.
      CTAERR << "Did not find state \'" << mState
	     << "\' in automaton \'" << itAuto->first
	     << "\' of module \'" << pModule->GetName() 
	     << "\'." << endl;
      CTAPUT;
    }
  }
}

// (pProduct) should be true if we have a product automaton to analyse.
ddmConfig ctaConfigState::mkDdmConfig(const reprAutomaton& pAuto,
				      const ctaModule* pCtaModule) const
{
  const ddmAutomaton* lAuto = pAuto.getDerivedObjDdm();

  // Start with an empty configuration.
  ddmConfig result(lAuto->getVarIds().size(),
		   lAuto->getStates().size());
  
  // Construct a full region.
  ddmRegion lRegion(lAuto->getVarIds().size(), true);

  // Compute the number of (mAutomatonName) in 
  //   the automata of the parent.
  int lAutoNum = 0;
  
  //   In simple automata, (lAutoNum==0) holds.
  // We have to set (lAutoNum) if we deal with
  //   a product automaton.
  // Run through automata of (pCtaModule) until 
  //   one is found which has (mAutomatonName) as name.
  for(ctaMap<ctaAutomaton*>::const_iterator
	it = pCtaModule->GetAutomata()->begin();
      (*mAutomatonName != *it->second->GetName())
        && (it != pCtaModule->GetAutomata()->end());
      ++it)
  {
    ++lAutoNum;
  }
  
  // For each state, insert the full region into (result)
  //   if the name-component number (lAutoNum) belongs to 
  //   (mState).
  for(int i=0; i< (int) lAuto->getStates().size(); ++i)
  {
    // Fetch the complete state name.
    ctaString lStateName = ctaString(lAuto->getStates()[i].getId());

    // Construct two pointers to start and end of substring 
    ctaString::size_type lStrStartPos = 0;
    int lCount = lAutoNum;
    while(lCount)
    {
      --lCount;
      lStrStartPos = 1+lStateName.find('$', lStrStartPos);
    }
    ctaString::size_type lStrEndPos = lStateName.find('$', lStrStartPos);

    // Copy substring.
    ctaString lSubString(lStateName, lStrStartPos, lStrEndPos-lStrStartPos);

    // Check if the substring equals (mState).
    // If yes, insert full region for current state.
    // Is necessary because the state can occur in another
    //   automaton of the module.
    if( *mState == lSubString )
    {
      result[i] = lRegion;
    }
  }

  // Now the state with name (mState) is associated with the
  //   full region and the others with the empty region.

  return result;
}

bddConfig ctaConfigState::mkBddConfig(const bddSymTab* pSymTab,
				      const ctaModule* pCtaModule) const
{
  return bddConfig::mkState(pSymTab, 
			    mAutomatonName, 
			    mState,
			    false);
}


void ctaConfigState::Print(ostream& s) const
{
  s << "STATE("
    << mAutomatonName
    << ") = "
    << mState;
}


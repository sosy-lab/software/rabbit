//-*-Mode: C++;-*-

/*
 * File:        utilTime.cpp
 * Purpose:     
 * Author:      Andy Heinig
 * Created:     2001
 * Copyright:   (c) 2001, Brandenburg Technical University of Cottbus
 */ 

#ifndef _utilTime_hh_
#define _utilTime_hh_

#include "ctaObject.h"

#include <sys/time.h>
#include <time.h>

#include <iostream>

class utilTime : ctaObject 
{

private: // Attributes.

  long mStartTime;

public:

  utilTime()
  {
    time(&mStartTime);
  }

  ~utilTime()
  {}

  long 
  getStartTime()
  {
    return mStartTime;
  }

  void
  printPastTime();

};

#endif 

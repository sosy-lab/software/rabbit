//-*-Mode: C++;-*-

/*
 * File:        ctaStringGNU.h
 * Purpose:     String Class for cta-library derived from old g++ lib
 * Author:      Dirk Beyer
 * Created:     1998
 * Copyright:   (c) 1998, University of Brandenburg, Cottbus
 */

#ifndef _ctaStringGNU_h
#define _ctaStringGNU_h


#include "String.h" // Gnu.

#include <iostream>

#include "ctaObject.h"

class ctaStringGNU : public string, private ctaObject
{
public:
  static const ctaStringGNU *mEmptyString;

private:
	
public:
  // It should be not allowed to use the standard operators.
  ctaStringGNU(const ctaStringGNU&);
  void operator=(const ctaStringGNU&);
  void operator,(const ctaStringGNU&);

  // Constructor and destructor.
  // Only for explicite casts!
  explicit ctaStringGNU(const string &s)
    : string(s)
    {
      // cout << "ctaString(\"" << s << "\") called." << endl;
    }

  explicit ctaStringGNU(string &s)
    : string(s)
    {
      // cout << "ctaString(\"" << s << "\") called." << endl;
    }
  
  explicit ctaString(ctaString& pStr, unsigned int& pStart, unsigned int pLength)
    : string(pStr, pStart, pLength)
    {}
    
  ~ctaString()
    {
      // cout << "ctaString " << *this << " deleted." << endl;
    }
  

  // We use the following methods of class string.
  
  //                    string();
  //                    string(const String& x);
  //                    string(const char* t);
  //                    ~string();
  //  string&           operator =  (const string&     y);
  //  string&           operator =  (const char* y);
  //  int operator==(const string& x, const string& y);
  //  int operator!=(const string& x, const string& y);
};

// I have to define it because we have a Pointer to a String.
ostream& operator << (ostream& out, const ctaStringGNU* s);

#endif

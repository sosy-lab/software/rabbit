%{
/*************** Includes and Defines *****************************/
#ifdef WIN32
#define alloca _alloca
#include <malloc.h>
#endif

#include <vector>

#include "ctaObject.h"

#include "ctaString.h"
#include "ctaSystem.h"
#include "ctaModule.h"
#include "ctaAutomaton.h"

#include "ctaConfiguration.h"
#include "ctaConfigAnd.h"
#include "ctaConfigOr.h"
#include "ctaConfigState.h"
#include "ctaConfigLinRest.h"

#include "ctaSetPointer.h"
#include "ctaMap.h"
#include "ctaComponent.h"
#include "ctaTransition.h"
#include "ctaState.h"

#include "ctaRestriction.h"
#include "ctaRestAnd.h"
#include "ctaRestFalse.h"
#include "ctaRestNot.h"
#include "ctaRestOr.h"
#include "ctaRestRel.h"
#include "ctaRestTrue.h"

#include "ctaExpression.h"
#include "ctaExprPlus.h"
#include "ctaExprNegation.h"
#include "ctaExprConst.h"

#include "ctaExprVar.h"

#include "ctaInstantiation.h"
#include "ctaRelOp.h"
#include "ctaSynchronisation.h"

#include "ctaParser.h"

// Things from the ANA library:

#include "anaBool.h"
#include "anaBoolEmpty.h"
#include "anaBoolEqual.h"
#include "anaBoolInclusion.h"
#include "anaBoolIsReachable.h"
#include "anaRegInstr.h"
#include "anaRegInstrAssign.h"
#include "anaRegInstrIf.h"
#include "anaRegInstrPrint.h"
#include "anaRegInstrPrintRepr.h"
#include "anaRegInstrPrintReprSize.h"
#include "anaRegInstrPrintAuto.h"
#include "anaRegInstrPrintBddGraph.h"
#include "anaRegInstrPrintBddEstim.h"
#include "anaRegInstrPrintFlatModule.h"
#include "anaRegInstrPrintCompletedModule.h"
#include "anaRegInstrSeq.h"
#include "anaRegInstrWhile.h"
#include "anaRegion.h"
#include "anaSection.h"
#include "anaSecReachability.h"
#include "anaSecRefinement.h"
#include "anaSecSimulation.h"
#include "anaRegInitial.h"
#include "anaRegSymbol.h"
#include "anaRegLinConstraint.h"
#include "anaRegState.h"
#include "anaRegInputErrorState.h"
#include "anaRegReachBackward.h"
#include "anaRegReachForward.h"
#include "anaRegNot.h"
#include "anaRegHide.h"
#include "anaRegUnion.h"
#include "anaRegIntersect.h"
#include "anaRegDifference.h"
#include "anaRegPost.h"
#include "anaRegPre.h"

// It is only for communication between parser
//   and the caller of the parser (rabbit.cpp).
extern ctaSystem* pubParsedSystem;

extern int line;

void yyerror(char* string)
{
  cout << "parser error: " << string << " at/before line: " << line;
  cout << ": " << pubParser->GetScanner()->YYText() << endl;
}

int yylex()
{
  return pubParser->GetScanner()->yylex();
}

%}

// Operators with precedences.
%left t_DIFFERENCE
%left t_UNION
%left t_OR
%left t_INTERSECT
%left t_AND
%left t_COMPLEMENT
%left t_NOT
%left t_CONTAINS
%left t_EQUALS
%left '='            // This is the 'equal' for sets!
%left '+' '-'

// keywords of CTA
%token t_ANALOG
%token t_AS
%token t_AUTOMATON
%token t_CLOCK
%token t_CONST
%token t_DER
%token t_DERIV
%token t_DISCRETE
%token t_DO
%token t_FROM
%token t_FALSE
%token t_GOTO
%token t_GUARD
%token t_INITIAL
%token t_INPUT
%token t_INST
%token t_INV
%token t_LOCAL
%token t_MODULE
%token t_MULTREST
%token t_OUTPUT
%token t_STATE
%token t_STOPWATCH
%token t_SYNC
%token t_TRANS
%token t_TRUE
%token t_WITH

// keywords of ANA

%token t_BACKWARD
%token t_BDD
%token t_COMMANDS
%token t_COMPLEMENT
%token t_CONTAINS
%token t_CHECK
%token t_DIFFERENCE
%token t_DDM
%token t_ELSE
%token t_EMPTY
%token t_EQUALS
%token t_FORWARD
%token t_HIDE
%token t_IF
%token t_IN
%token t_INITIALREGION
%token t_INPUTERRORSTATES
%token t_INTERSECT
%token t_ISREACHABLE
%token t_NOT
%token t_NOREPR
%token t_POST
%token t_PRE
%token t_PRINT
%token t_PRINTREPR
%token t_PRINTREPRSIZE
%token t_PRINTAUTO
%token t_PRINTCOMPLETEDMODULE
%token t_PRINTFLATMODULE
%token t_PRINTBDDGRAPH
%token t_PRINTBDDESTIM
%token t_REACH
%token t_REACHABILITY
%token t_REFINEMENT
%token t_SIMULATION
%token t_RUN
%token t_REGION
%token t_REGIONVARIABLE
%token t_STATE
%token t_STEPS
%token t_TO
%token t_UNION
%token t_USE
%token t_VAR
%token t_WHILE

// pseudo literals
%token t_IDENTIFIER
%token t_INTEGERCONSTANT
%token t_STRING

// define the types of rules
%union {
  reprNUMBER          cta_NUMBER;
  reprNUMBER*         cta_NUMBER_Ptr;
  ctaString*          cta_String;
  set<ctaString>*     cta_IdentList;
  ctaSystem*          cta_System;
  vector<ctaModule*>* cta_Modules;
  ctaModule*          cta_Module;
  ctaAutomaton*       cta_Automaton;
  ctaConfiguration*   cta_Configuration;
  vector<pair<ctaString*, ctaComponent*> >* cta_Comps;
  ctaRestriction*     cta_Restriction;
  ctaSetPointer<ctaTransition*>* cta_Transitions;
  ctaComponent*       cta_Component;
  ctaMap<ctaState*>*  cta_States;
  ctaExpression*      cta_Expression;
  ctaExprVar*         cta_ExprVar;
  ctaRelOp*           cta_RelOp;
  ctaMap<ctaString*>* cta_Connections;
  ctaInstantiation*   cta_Instantiation;
  ctaSynchronisation* cta_Sync;

  // added for ANA:
  vector<anaSection*>* ana_Analysis;
  anaSection*         ana_Section;
  set<ctaString>*     ana_RegDeclList;
  anaRegInstrSeq*     ana_RegInstrSeq;
  anaRegInstr*        ana_RegInstr;
  anaRegion*          ana_RegionExpr;
  anaBool*            ana_RegBool;
  ctaModule::ReprType ana_ReprType;
  pair<ctaModule::ReprType, bool>* ana_ReachOptions;
}

// declare a component (as type) in yylval for each rule
%type <cta_String>      t_IDENTIFIER SignalType String t_STRING
%type <cta_System>      System
%type <cta_Modules>     Modules
%type <cta_Module>      Module ModuleContent
%type <cta_Automaton>   Automaton
%type <cta_States>      States
%type <cta_Transitions> Transitions 
%type <cta_IdentList>   Hide
%type <cta_Configuration> Configuration Initial
%type <cta_Component>   Type TypeWithoutConst
%type <cta_Comps>       DeclList LocalDeclList Interface
%type <cta_NUMBER>      Constant t_INTEGERCONSTANT
%type <cta_NUMBER_Ptr>  Range
%type <cta_Connections> Connections
%type <cta_Instantiation> Instantiation
%type <cta_Expression>  DerExpr LinExpr DoExpr
%type <cta_ExprVar>     Variable DerVar TickedVar DoVar
%type <cta_RelOp>       RelOp
%type <cta_Restriction> LinConstraint LinRestriction
%type <cta_Restriction> DerRestriction DoRestriction Do
%type <cta_Restriction> Guard Invariant Derivation 
%type <cta_Sync>        Sync

// added for ANA:

%type <cta_String>         t_REGIONVARIABLE
%type <ana_Analysis>       Analysis
%type <ana_Section>        Reachability Refinement Simulation
%type <ana_RegDeclList>    VarList RegDeclList
%type <ana_RegInstrSeq>    Commands
%type <ana_RegInstr>       Command
%type <ana_RegionExpr>     RegionExpr SetOperation BoolOperation
%type <ana_ReprType>       ReprType
%type <ana_ReachOptions>   ReachOptions

// start symbol declaration
%start System

/*************************************************************************/
%%

/* Terminals written in capitals and nonterminals with
   one capital at beginning. */

/* We have to use a left recursiv gramar because we produce a
   bottom up parser. If we use a right recursiv gramar we
   risk a stack overflow because of the large size of stack we need. */

System:
      Modules Analysis
      {
    $$ = new ctaSystem($1, $2);
    pubParsedSystem = $$;
      }
    ;

Analysis:
      // nothing   // For using context checks and transformations only.
      {
    $$ = new vector<anaSection*>();
    // Set dots and dollars are allowed in identifiers.
    //  Needed to specify flattened names.
    pubParser->GetScanner()->SetAllowDotAndDollars(true);
      }
    | Analysis Reachability
      {
    $$ = $1;
    $$->push_back($2);
      }
    | Analysis Refinement
      {
    $$ = $1;
    $$->push_back($2);
      } 
    | Analysis Simulation
      {
    $$ = $1;
    $$->push_back($2);
      }
    ;

ReprType:
      t_BDD
      {  
    $$ = ctaModule::mBDD;
      }
    | t_DDM
      {
    $$ = ctaModule::mDDM;
      }
      // Just for printing out flat module or completed module.
    | t_NOREPR
      {
    $$ = ctaModule::mNOREPR;
      }
      ;

ReachOptions:
      // nothing
      {
    $$ = new pair<ctaModule::ReprType, bool>;
    $$->first  = ctaModule::mUndef;
    $$->second = false;
      }
    | t_USE ReprType
      {
    $$ = new pair<ctaModule::ReprType, bool>;
    $$->first  = $2;
    $$->second = false;
      }
    | t_USE t_INPUTERRORSTATES
      {
    $$ = new pair<ctaModule::ReprType, bool>;
    $$->first  = ctaModule::mUndef;
    $$->second = true;
      }
    | t_USE ReprType t_USE t_INPUTERRORSTATES
      {
    $$ = new pair<ctaModule::ReprType, bool>;
    $$->first  = $2;
    $$->second = true;
      }
      ;

Reachability:
      t_REACHABILITY t_CHECK t_IDENTIFIER ReachOptions
               '{' Opt_t_VAR RegDeclList t_COMMANDS Commands '}'
      {
    // After parsing of the commands-region we do not
    //   need the set-pointer and have to leave it intact!
    pubParser->mRegionVariables = NULL;

    $$ = new anaSecReachability($9, $3, $7, $4->first, $4->second);
    delete $4;
      }
    ;

Refinement:
      t_REFINEMENT t_CHECK t_IDENTIFIER t_USE ReprType ';' 
      {
    $$ = new anaSecRefinement(*$3, $5);
    delete $3;
      }
    ;

Simulation:
      t_RUN t_SIMULATION t_IDENTIFIER ReachOptions ';' 
      { 
    $$ = new anaSecSimulation($3, $4->first, $4->second); 
    delete $4;
      }
    ;

Opt_t_VAR:
      /* nothing */
    | t_VAR
    ;

RegDeclList:
      /* nothing */
      {
    $$ = new set<ctaString>();

    // Save the pointer from the stack pointing
    //   to the set of region variables for usage
    //   in the scanner.
    pubParser->mRegionVariables = $$;
      }
    | RegDeclList VarList ':' t_REGION';'
      {
    $$ = $1;
    for( set<ctaString>::iterator
         it = $2->begin();
         it!= $2->end();
         ++it)
    {
      $$->insert(*it);
    }
    
    { delete $2; /* Braces for nice ctaGrammar.txt output. */ }
      }
    ;

VarList:
      t_IDENTIFIER
      {
    $$ = new set<ctaString>();
    $$->insert(*$1);
    delete $1;
      }
    | VarList ',' t_IDENTIFIER
      {
    $$ = $1;
    $$->insert(*$3);
    delete $3;
      }
    ;

Commands:
      // nothing 
      {
        $$ = new anaRegInstrSeq();
      }
    | Commands Command 
      {
        $$ = $1;
        $$->Append($2);
      }
    ;

Command:
      // Assignment
      // (t_REGIONVARIABLE) is not a real terminal (see scanner)!
      t_REGIONVARIABLE ':' '=' RegionExpr ';'
      {
      $$ = new anaRegInstrAssign($1,$4);
      }
      // Output of results.
    | t_PRINT String RegionExpr String ';'
      {
    $$ = new anaRegInstrPrint($2, $3, $4);
      }
      // String output.
      // Output of results.
    | t_PRINTREPR String RegionExpr String ';'
      {
    $$ = new anaRegInstrPrintRepr($2, $3, $4);
      }
      // String output.
    | t_PRINT String ';'
      {
    $$ = new anaRegInstrPrint($2, NULL, new ctaString(""));
      }
      // Output of product automaton.
    | t_PRINTAUTO ';'
      {
    $$ = new anaRegInstrPrintAuto();
      }
      // Output of flattened module.
    | t_PRINTFLATMODULE ';'
      {
    $$ = new anaRegInstrPrintFlatModule();
      }
      // Output of completed module (for DDM representation).
    | t_PRINTCOMPLETEDMODULE ';'
      {
    $$ = new anaRegInstrPrintCompletedModule();
      }
      // Compute the number of configurations represented in a BDD.
    | t_PRINTREPRSIZE RegionExpr ';'
      {
    $$ = new anaRegInstrPrintReprSize($2);
      }
      // Create graphical BDD visualization.
    | t_PRINTBDDGRAPH RegionExpr ';'
      {
    $$ = new anaRegInstrPrintBddGraph($2);
      }
      // Create graphical BDD visualization.
    | t_PRINTBDDESTIM ';'
      {
    $$ = new anaRegInstrPrintBddEstim();
      }
      // If statement with else instruction. 
    | t_IF '(' RegionExpr ')' '{' Commands '}' 
      t_ELSE '{' Commands '}'
      {
    $$ = new anaRegInstrIf($3, $6, $10); 
      }       
      // If statement without else instruction. 
    | t_IF '(' RegionExpr ')' '{' Commands '}' 
      {
    $$ = new anaRegInstrIf($3, $6, new anaRegInstrSeq() ); 
      }       
      // While Statment.
    | t_WHILE '(' RegionExpr ')' '{' Commands '}'
      {
    $$ = new anaRegInstrWhile($3, $6);
      }
    ;

String:
      /* nothing */
      {
    $$ = new ctaString("");
      }
    | t_STRING
      {
    $$ = $1;
      }

RegionExpr:
      // Rules for constants and variables.
      t_REGIONVARIABLE        // This is not a real terminal (see scanner)!
      {
	$$ = new anaRegSymbol($1);
      }
    | t_INITIALREGION
      {
	$$ = new anaRegInitial();
      }
    | t_INPUTERRORSTATES
      {
	$$ = new anaRegInputErrorState();
      }
      // Rules for generation of regions.
    | LinConstraint
      {
	$$ = new anaRegLinConstraint(new ctaConfigLinRest($1));
      }
    | t_STATE '(' t_IDENTIFIER ')' '=' t_IDENTIFIER
      {
	$$ = new anaRegState($3, $6);
      }
    | t_STATE '(' t_IDENTIFIER ')' '!' '=' t_IDENTIFIER 
      {
	anaRegState* lAnaRegState = new anaRegState($3, $7);
	$$ = new anaRegNot(lAnaRegState, true);
      }
      // Computing regions from existing regions.
    | t_POST '(' RegionExpr ')'
      {
	$$ = new anaRegPost($3);
      }
    | t_PRE '(' RegionExpr ')'
      {
	$$ = new anaRegPre($3);
      }
    | t_REACH t_FROM RegionExpr t_FORWARD
              t_IN t_INTEGERCONSTANT t_STEPS
      {
	$$ = new anaRegReachForward($3, $6);
      }
    | t_REACH t_FROM RegionExpr t_BACKWARD 
              t_IN t_INTEGERCONSTANT t_STEPS
      {
	$$ = new anaRegReachBackward($3, $6);
      }
    | t_REACH t_FROM RegionExpr t_FORWARD
      {
	$$ = new anaRegReachForward($3, 1000);
      }
    | t_REACH t_FROM RegionExpr t_BACKWARD 
      {
	$$ = new anaRegReachBackward($3, 1000);
      }
    | SetOperation
      {
	$$ = $1;
      }
    | BoolOperation
      {
	$$ = $1;
      }
    | '(' RegionExpr ')'
      {
	$$ = $2;
      }
    | t_HIDE '(' t_IDENTIFIER Hide t_IN RegionExpr ')' 
      {
	$4->insert(*$3);
	delete $3;
	
	$$ = new anaRegHide($4, $6);
      }
    ;

SetOperation:
      RegionExpr t_UNION RegionExpr
      {  
	$$ = new anaRegUnion($1, $3);
      }
    | RegionExpr t_OR RegionExpr
      {  
	$$ = new anaRegUnion($1, $3);
      }
    | RegionExpr t_INTERSECT RegionExpr
      {
	$$ = new anaRegIntersect($1, $3);
      }
    | RegionExpr t_AND RegionExpr
      {
	$$ = new anaRegIntersect($1, $3);
      }
    | RegionExpr t_DIFFERENCE RegionExpr
      {
	$$ = new anaRegDifference($1, $3);
      }
    | t_COMPLEMENT '(' RegionExpr ')'
      {
	$$ = new anaRegNot($3);
      }
    | t_NOT '(' RegionExpr ')'
      {
	$$ = new anaRegNot($3);
      }
    ;

// The following is for operations with a Boolean result (anaBool).
BoolOperation:
      t_EMPTY '(' RegionExpr ')'
      {
    $$ = new anaBoolEmpty($3);
      }
    | RegionExpr t_EQUALS RegionExpr
      {
    $$ = new anaBoolEqual($1, $3);
      }
    | RegionExpr '=' RegionExpr
      {
    $$ = new anaBoolEqual($1, $3);
      }
    | RegionExpr t_CONTAINS RegionExpr
      {
    $$ = new anaBoolInclusion($1, $3);
      }
    | t_ISREACHABLE t_FROM RegionExpr t_TO RegionExpr t_FORWARD
      {
    $$ = new anaBoolIsReachable($3, $5);
      }
    ;

Hide:
      /* nothing */               
      {
        $$ = new set<ctaString>();
      }
    | Hide ',' t_IDENTIFIER
      {
        $1->insert(*$3);
    delete $3;

        $$ = $1;
      }
    ;



// The section for the modules. 

Modules:
      /* nothing */               // For using only region operations.
      {
    $$ = new vector<ctaModule*>();
      }
    | Modules Module
      {
        $1->push_back($2);
        $$ = $1;
      }
    ;

Module: 
      t_MODULE t_IDENTIFIER '{' ModuleContent '}'
      {
        $4->SetName($2);
        $$ = $4;
      }
    ;

ModuleContent:  
      /* nothing */
      {
        $$ = new ctaModule();
      }
    | ModuleContent Interface
      {
        for(vector<pair<ctaString*, ctaComponent*> >::iterator 
        idx = $2->begin(); 
        idx != $2->end();
        ++idx)
        {
      // Append the ctaComponents to the interface-map.
      $1->GetInterface()->append(idx->first, idx->second);
      
      if( idx->second->GetRestrictionType() == ctaComponent::LOCAL )
      {
        if( idx->second->GetDataType() == ctaComponent::CLOCK ||
        idx->second->GetDataType() == ctaComponent::DISCRETE )
        {
          pair<ctaModule::ParsingTag,ctaString*> tmp;
          tmp.first = ctaModule::INTERFACE;
          tmp.second = new ctaString(*idx->second->GetName());

          // Append the entry for parsing order.
          $1->GetParsingOrder()->push_back(tmp);
        }
      }
        }
    // Only remove the container and not the associated objects.
    $2->clear();
    delete $2;
        $$ = $1;
      }
    | ModuleContent Instantiation
      {
        $1->GetInstantiations()->append($2->GetInstanceName(), $2);

    pair<ctaModule::ParsingTag,ctaString*> tmp;
    tmp.first = ctaModule::INSTANTIATION;
    tmp.second = new ctaString(*$2->GetInstanceName());
    
    $1->GetParsingOrder()->push_back(tmp); 

        $$ = $1;
      }
    | ModuleContent Initial
      {
        $1->addInitial($2);
        $$ = $1;
      }
    | ModuleContent Automaton
      {
        $1->GetAutomata()->append($2->GetName(), $2);

    pair<ctaModule::ParsingTag,ctaString*> tmp;
    tmp.first = ctaModule::AUTOMATON;
    tmp.second = new ctaString(*$2->GetName());

    $1->GetParsingOrder()->push_back(tmp); 

        $$ = $1;
      }
    ;

Interface:
      t_INPUT DeclList
      {
    for(vector<pair<ctaString*, ctaComponent*> >
        ::iterator idx = $2->begin(); 
        idx != $2->end();
        ++idx)
        {
          idx->second->SetRestrictionType(ctaComponent::INPUT);
        }

    {    // braces for nice grammar output.
      $$ = $2;
    }
      }
    | t_OUTPUT DeclList
      {
    for(vector<pair<ctaString*, ctaComponent*> >
        ::iterator idx = $2->begin(); 
        idx != $2->end();
        ++idx)
        {
          idx->second->SetRestrictionType(ctaComponent::OUTPUT);
        }

    {
      $$ = $2;
    }
      }
    | t_MULTREST DeclList
      {
    for(vector<pair<ctaString*, ctaComponent*> >
        ::iterator idx = $2->begin(); 
        idx != $2->end();
        ++idx)
        {
          idx->second->SetRestrictionType(ctaComponent::MULTREST);
        }

    {
      $$ = $2;
    }
      }
    | t_LOCAL LocalDeclList
      {
    for(vector<pair<ctaString*, ctaComponent*> >
        ::iterator idx = $2->begin(); 
        idx != $2->end();
        ++idx)
        {
          idx->second->SetRestrictionType(ctaComponent::LOCAL);
        }

    {
      $$ = $2;
    }
      }
    ;

DeclList:
      /* nothing */
      {
        $$ = new vector<pair<ctaString*, ctaComponent*> >();
      }
    | DeclList 
      t_IDENTIFIER ':' Type ';'
      {
        $4->SetName($2);
    $1->push_back(pair<ctaString*, ctaComponent*>($4->GetName(), $4));
        $$ = $1;
      }
    | DeclList 
      error ';'     // error recovery
      {
        $$ = $1;
        cout << "parser: error recovery: declaration skipped." << endl;
      }
    ;

LocalDeclList:        // DeclList but with initialisation of constants
      /* nothing */
      {
        $$ = new vector<pair<ctaString*, ctaComponent*> >();
      }
    // Local constants must have a value initialization.
    //   This is forbidden: " LOCAL identname : CONST;".
    // Local variables can have a range definition.
    | LocalDeclList 
      t_IDENTIFIER ':' TypeWithoutConst Range ';'
      {
        $4->SetName($2);
    $4->SetMaxValueOfRange($5);
    $1->push_back(pair<ctaString*, ctaComponent*>($4->GetName(), $4));
        $$ = $1;
      }
    | LocalDeclList 
      t_IDENTIFIER '=' Constant ':' t_CONST ';'
      {
        ctaComponent* c = new ctaComponent(ctaComponent::CONST,
                       new reprNUMBER($4));
        c->SetName($2);

    $1->push_back(pair<ctaString*, ctaComponent*>(c->GetName(), c));
        $$ = $1;        
      }
    ;

Type:
      t_CONST
      {
        $$ = new ctaComponent(ctaComponent::CONST);
      }
    | TypeWithoutConst
      {
        $$ = $1;
    $$->SetMaxValueOfRange(NULL);
      }
    ;

Range:
      /* nothing */
      {
    $$ = NULL;
      }
    | '(' Constant ')'
      {
    $$ = new reprNUMBER($2);
      }
    ;

TypeWithoutConst:
      t_DISCRETE
      {
        $$ = new ctaComponent(ctaComponent::DISCRETE);
      }
    | t_CLOCK
      {
        $$ = new ctaComponent(ctaComponent::CLOCK);
      }
    | t_STOPWATCH
      {
        $$ = new ctaComponent(ctaComponent::STOPWATCH);
      }
    | t_ANALOG
      {
        $$ = new ctaComponent(ctaComponent::ANALOG);
      }
    | t_SYNC
      {
        $$ = new ctaComponent(ctaComponent::SYNC);
      }
    ;

Instantiation:
      t_INST t_IDENTIFIER t_FROM t_IDENTIFIER
      t_WITH '{' Connections '}'
      {
        // Empty initialization clause means no restriction.
        ctaConfiguration* lConfig = new ctaConfigLinRest(new ctaRestTrue());
        $$ = new ctaInstantiation($2, $4, $7, lConfig);
      }
    | t_INST t_IDENTIFIER t_FROM t_IDENTIFIER
      t_WITH '{' Connections Initial '}'
      {
        $$ = new ctaInstantiation($2, $4, $7, $8);
      }
    ;

Connections:
      /* nothing */
      {
        $$ = new ctaMap<ctaString*>();
      }
    | Connections 
      t_IDENTIFIER t_AS t_IDENTIFIER ';'
      {
        $1->append($2, $4);
          delete $2;
        $$ = $1;
      }
    ;

Initial:
      t_INITIAL Configuration ';' 
      {
        $$ = $2;
      }
    ;

Automaton:
      t_AUTOMATON t_IDENTIFIER '{' States '}'
      {
        $$ = new ctaAutomaton($2, $4);
      }
    ;

States:
      /* nothing */
      {
        $$ = new ctaMap<ctaState*>();
      }
    | States 
      t_STATE t_IDENTIFIER 
      '{' Invariant Derivation Transitions '}'
      {
        ctaState* state;
        state = new ctaState($3,$5,$6,$7);
        $1->append(state->GetName(), state);
        $$ = $1;
      }
    ;

Invariant:
      /* nothing */
      {
        $$ = new ctaRestTrue();
      }
    | t_INV LinRestriction ';' 
      {
        $$ = $2;
      }
    ;

Derivation:
      /* nothing */
      {
        $$ = new ctaRestTrue();
      }
    | t_DERIV DerRestriction ';' 
      {
        $$ = $2;
      }
    ;

Transitions:
      /* nothing */
      {
        $$ = new ctaSetPointer<ctaTransition*>();
      }
    | Transitions 
      t_TRANS '{' Guard Sync Do t_GOTO t_IDENTIFIER ';' '}'
      {
        ctaTransition* t = new ctaTransition($8,$4,$5,$6);
        $1->append(t);        // append t to the others
        $$ = $1;
      }
    ;

Guard:
      /* nothing */
      {
        $$ = new ctaRestTrue();
      }
    | t_GUARD LinRestriction ';'
      {
        $$ = $2;
      }
    ;

Sync:
      /* nothing */
      {
    // We have to initialize it for further checking.
        $$ = new ctaSynchronisation();
      }
    | t_SYNC SignalType t_IDENTIFIER ';'
      {
        $$ = new ctaSynchronisation();
        $$->SetSignalName($3);
        $$->SetSignalType($2);
      }
    ;

SignalType:
      '?'
      {
        $$ = new ctaString("?");
      }
    | '!'
      {
        $$ = new ctaString("!");
      }
    | '#'
      {
        $$ = new ctaString("#");
      }
    ;

Do:
      /* nothing */
      {
    // If the allowed assignments are not given,
    //   we assume that everything is allowed.
        $$ = new ctaRestTrue();
      }
    | t_DO DoRestriction ';' 
      {
        $$ = $2;
      }
    ;

// Nonterminals for configurations and restrictions.

Configuration:
      LinConstraint
      {
        $$ = new ctaConfigLinRest($1);
      }
    | Configuration t_AND Configuration
      {
        $$ = ctaConfiguration::mkAnd($1, $3);
      }
    | Configuration t_OR Configuration
      {
        $$ = ctaConfiguration::mkOr($1, $3);
      }
    | t_STATE '(' t_IDENTIFIER ')' '=' t_IDENTIFIER 
      {
        $$ = new ctaConfigState($3, $6);
      }
    | '(' Configuration ')'
      {
        $$ = $2;
      }
    ;

LinRestriction:
      LinConstraint
      {
    $$ = $1;
      }
    | t_NOT LinRestriction
      {
        $$ = ctaRestriction::mkNot($2);
      }
    | LinRestriction t_AND LinRestriction
      {
        $$ = ctaRestriction::mkAnd($1, $3);
      }
    | LinRestriction t_OR  LinRestriction
      {
        $$ = ctaRestriction::mkOr($1, $3);
      }
    | '(' LinRestriction ')'
      {
        $$ = $2;
      }
    ;

LinConstraint:
      t_TRUE
      {
        $$ = new ctaRestTrue();
      }
    | t_FALSE
      {
        $$ = new ctaRestFalse();
      }
    | LinExpr RelOp LinExpr
      {
        $$ = new ctaRestRel($1, $2, $3);
      }
    ;

LinExpr:    
      LinExpr '+' LinExpr
      {
        $$ = new ctaExprPlus($1,$3);
      }
    | LinExpr '-' LinExpr
      {
        $$ = new ctaExprPlus($1, new ctaExprNegation($3));
      }
    | '(' LinExpr ')'
      {
        $$ = $2;
      }
    | '-' LinExpr
      {
        $$ = new ctaExprNegation($2);
      }
    | Variable
      {
        $$ = $1;
      }
    | Constant '*' Variable
      {
    $3->SetFactor($1);
        $$ = $3;
      }
    | Constant
      {
        $$ = new ctaExprConst($1);
      }
    ;

DerRestriction:
      t_TRUE
      {
        $$ = new ctaRestTrue();
      }
    | t_FALSE
      {
        $$ = new ctaRestFalse();
      }
    | DerExpr RelOp DerExpr
      {
    $$ = new ctaRestRel($1, $2, $3);
      }
    | t_NOT DerRestriction
      {
        $$ = ctaRestriction::mkNot($2);
      }
    | DerRestriction t_AND DerRestriction
      {
    $$ = ctaRestriction::mkAnd($1,$3);
      }
    | DerRestriction t_OR  DerRestriction
      {
    $$ = ctaRestriction::mkOr($1,$3);
      }
    | '(' DerRestriction ')'
      {
        $$ = $2;
      }
    ;

DerExpr:    
      DerExpr '+' DerExpr
      {
        $$ = new ctaExprPlus($1,$3);
      }
    | DerExpr '-' DerExpr
      {
        $$ = new ctaExprPlus($1, new ctaExprNegation($3));
      }
    | '(' DerExpr ')'
      {
        $$ = $2;
      }
    | '-' DerExpr
      {
        $$ = new ctaExprNegation($2);
      }
    | DerVar
      {
        $$ = $1;
      }
    | Constant '*' DerVar
      {
    $3->SetFactor($1);
        $$ = $3;
      }
    | Constant   // Explicit constant e.g. "51".
      {
        $$ = new ctaExprConst($1);
      }
    | Variable  // Symbolic constants. It means identifiers declared as CONST.
      {         // Or 'implicite constants'.
    $$ = $1;
      }
    ;

DoRestriction:
      t_TRUE
      {
        $$ = new ctaRestTrue();
      }
    | t_FALSE
      {
        $$ = new ctaRestFalse();
      }
    | DoExpr RelOp DoExpr
      {
        $$ = new ctaRestRel($1, $2, $3);
      }
    | t_NOT DoRestriction
      {
        $$ = ctaRestriction::mkNot($2);
      }
    | DoRestriction t_AND DoRestriction
      {
        $$ = ctaRestriction::mkAnd($1,$3);
      }
    | DoRestriction t_OR DoRestriction
      {
        $$ = ctaRestriction::mkOr($1,$3);
      }
    | '(' DoRestriction ')'
      {
        $$ = $2;
      }
    ;

DoExpr:    
      DoExpr '+' DoExpr
      {
        $$ = new ctaExprPlus($1,$3);
      }
    | DoExpr '-' DoExpr
      {
        $$ = new ctaExprPlus($1, new ctaExprNegation($3));
      }
    | '(' DoExpr ')'
      {
        $$ = $2;
      }
    | '-' DoExpr
      {
        $$ = new ctaExprNegation($2);
      }
    | DoVar
      {
        $$ = $1;
      }
    | Constant '*' DoVar
      {
    $3->SetFactor($1);
        $$ = $3;
      }
    | Constant
      {
        $$ = new ctaExprConst($1);
      }
    ;

// The different forms of occurances of variables.

Variable:
      t_IDENTIFIER               // A standard variable.
      {
    $$ = ctaExprVar::mkStandard($1);
      }
    ;

DerVar:
      t_DER '(' t_IDENTIFIER ')' // A derivative of a variable.
      {
        $$ = ctaExprVar::mkDer($3);
      }
    ;

TickedVar:
      t_IDENTIFIER '\''          // A ticked variable.
      {
        $$ = ctaExprVar::mkTicked($1);
      }
    ;

DoVar:
      Variable                // A standard variable.
      {
        $$ = $1;
      }
    | TickedVar               // A ticked variable.
      {
        $$ = $1;
      }
    ;

Constant:
     t_INTEGERCONSTANT
      {
        $$ = $1;
      }
    ;

// Relation operations.

RelOp:
      '='
      {
        $$ = new ctaRelOp( ctaRelOp::EQUAL );
      }
    | '<'
      {
        $$ = new ctaRelOp( ctaRelOp::LESS );
      }
    | '>'
      {
        $$ = new ctaRelOp( ctaRelOp::GREATER );
      }
    | '!' '='
      {
        $$ = new ctaRelOp( ctaRelOp::NOTEQUAL );
      }
    | '<' '='
      {
        $$ = new ctaRelOp( ctaRelOp::LESSTHAN );
      }
    | '>' '='
      {
        $$ = new ctaRelOp( ctaRelOp::GREATERTHAN );
      }
    ;
%%

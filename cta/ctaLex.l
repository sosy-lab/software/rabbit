/*
 * File:        ctaLex.l
 * Purpose:     Lex Module for cta-library
 * Author:      Dirk Beyer
 * Created:     1998
 * Copyright:   (c) 1998, University of Brandenburg, Cottbus
 */

%{

/* Included code before lex code */
/*************** Includes and Defines *****************************/

#include "ctaParser.h"

// following includes we need for yylval components

#include "ctaAutomaton.h"
#include "ctaComponent.h"
#include "ctaConfiguration.h"
#include "ctaExpression.h"
#include "ctaInstantiation.h"
#include "ctaModule.h"
#include "ctaObject.h"
#include "ctaRelOp.h"
#include "ctaRestriction.h"    
#include "ctaSetPointer.h"
#include "ctaState.h"
#include "ctaString.h"
#include "ctaTransition.h"
#include "ctaExprVar.h"

#include <string.h>
#include "ctaScanner.h"

// Things from the ANA library:

#include "anaBool.h"
#include "anaRegInstr.h"
#include "anaSection.h"
#include "anaRegInstrSeq.h"
#include "anaRegion.h"


/* YACC generated definitions based on CTA parser input*/
#include "ctaYacc.tab.cpp.h"

int line=1;

%}

/* We use '.' and '$' internally -- they should not occur
   in user programs. */
identifier	 [a-zA-Z_][0-9a-zA-Z_$.]*

decimal_constant [0-9]+

h_tab	         [\011]
form_feed	 [\014]
v_tab	         [\013]
c_return	 [\015]

horizontal_white [ ]|{h_tab}

string_literal   [^"]+

%x comment

%%


{horizontal_white}+	{}

({v_tab}|{c_return}|{form_feed})+ {}

({horizontal_white}|{v_tab}|{c_return}|{form_feed})*"\n"	{
			line++;
		       }

ANALOG	   {return(t_ANALOG);}
AND        {return(t_AND);}
AS	   {return(t_AS);}
AUTOMATON  {return(t_AUTOMATON);}
CLOCK	   {return(t_CLOCK);}
CONST	   {return(t_CONST);}
DER	   {return(t_DER);}
DERIV	   {return(t_DERIV);}
DO         {return(t_DO);}
FALSE      {return(t_FALSE);}
FROM	   {return(t_FROM);}
GOTO       {return(t_GOTO);}
GUARD	   {return(t_GUARD);}
INITIAL    {return(t_INITIAL);}
INPUT	   {return(t_INPUT);}
INST	   {return(t_INST);}
INV	   {return(t_INV);}
LOCAL	   {return(t_LOCAL);}
MODULE	   {return(t_MODULE);}
MULTREST   {return(t_MULTREST);}
NOT        {return(t_NOT);}
OR         {return(t_OR);}
OUTPUT	   {return(t_OUTPUT);}
STATE	   {return(t_STATE);}
SYNC	   {return(t_SYNC);}
TRANS	   {return(t_TRANS);}
TRUE       {return(t_TRUE);}
WITH	   {return(t_WITH);}
DISCRETE   {return(t_DISCRETE);}
STOPWATCH  {return(t_STOPWATCH);}


BACKWARD        {return(t_BACKWARD);}
BDD             {return(t_BDD);}
COMMANDS        {return(t_COMMANDS);}
COMPLEMENT      {return(t_COMPLEMENT);}
CONTAINS        {return(t_CONTAINS);}
CHECK           {return(t_CHECK);}
ELSE            {return(t_ELSE);}
DIFFERENCE      {return(t_DIFFERENCE);}
DDM             {return(t_DDM);}
EMPTY           {return(t_EMPTY);}
EQUALS          {return(t_EQUALS);}
HIDE            {return(t_HIDE);}
IF              {return(t_IF);}
IN              {return(t_IN);}
INITIALREGION   {return(t_INITIALREGION);}
INPUTERRORSTATES {return(t_INPUTERRORSTATES);}
INTERSECT       {return(t_INTERSECT);}
ISREACHABLE     {return(t_ISREACHABLE);}
NOREPR          {return(t_NOREPR);}
REACHABILITY    {return(t_REACHABILITY);}
REFINEMENT      {return(t_REFINEMENT);}
SIMULATION      {return(t_SIMULATION);}
RUN             {return(t_RUN);}
FORWARD         {return(t_FORWARD);}
POST            {return(t_POST);}
PRE             {return(t_PRE);}
PRINT           {return(t_PRINT);}
PRINTREPR       {return(t_PRINTREPR);}
PRINTREPRSIZE   {return(t_PRINTREPRSIZE);}
PRINTBDDGRAPH   {return(t_PRINTBDDGRAPH);}
PRINTBDDESTIM   {return(t_PRINTBDDESTIM);}
PRINTAUTO       {return(t_PRINTAUTO);}
PRINTCOMPLETEDMODULE {return(t_PRINTCOMPLETEDMODULE);}
PRINTFLATMODULE {return(t_PRINTFLATMODULE);}
REACH           {return(t_REACH);}
REGION          {return(t_REGION);}
TO              {return(t_TO);}  
STEPS           {return(t_STEPS);}
UNION           {return(t_UNION);}
USE             {return(t_USE);}
VAR             {return(t_VAR);}
WHILE           {return(t_WHILE);}  

{identifier} {
                // Are dots and dollars allowed?
                if (!pubParser->GetScanner()->GetAllowDotAndDollars())
                {
                   // Dots and dollars are not allowed!
                   if (strchr(yytext,'.') || strchr(yytext,'$'))
		   {
                     yyerror("There are dots or dollars in identifier.\n");
		   }
                }
	        yylval.cta_String = new ctaString(yytext);

                if(pubParser->mRegionVariables != NULL)
                {
                  if( pubParser->mRegionVariables->find( *(yylval.cta_String) )
                      != pubParser->mRegionVariables->end() )
                  {
                    return(t_REGIONVARIABLE);
                  }
                }
		return(t_IDENTIFIER);
	     }

{decimal_constant}  {
      	        yylval.cta_NUMBER = (reprNUMBER)atol((char *)yytext);
		return(t_INTEGERCONSTANT);
	     }


\"{string_literal}\"       {
                ctaString lTmpStr(yytext);

                // Without the first character which is a (").
                ctaString::size_type lStart = 1;

                // Without the last character which is a (").
                ctaString::size_type lLength = lTmpStr.length() - 2;

                yylval.cta_String = new ctaString(lTmpStr, lStart, lLength);
		return(t_STRING);
	     }

.            {
		return(yytext[0]);
	     }

(--).*$     {
		/* ignore comments to end of line. */
	    };

(\/\/).*$     {
		/* ignore comments to end of line. */
	    };

"/*"     BEGIN(comment);
         <comment>[^*\n]*        /* eat anything that's not a '*' */
         <comment>"*"+[^*/\n]*   /* eat up '*'s not followed by '/'s */
         <comment>\n             ++line;
         <comment>"*"+"/"        BEGIN(INITIAL);

%%

/*******************************************************************/
int yywrap()		// if we need some other files
{ 
	return(1);
}



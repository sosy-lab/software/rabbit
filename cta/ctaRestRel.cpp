//-*-Mode: C++;-*-

/*
 * File:        ctaRestRel.cpp
 * Purpose:     Relation-Restriction class for cta-library
 * Author:      H. Rust
 * Created:     1998
 * Copyright:   (c) 1998, University of Brandenburg, Cottbus
 */

#include "ctaRestRel.h"

class ctaModule;

#include "reprAutomaton.h"
#include "ddmAutomaton.h"
#include "ddmRegion.h"
#include "ddmVec.h"

#include "bddAutomaton.h"
#include "bddConfig.h"

ddmRegion ctaRestRel::mkDdmRegion(const reprAutomaton& pAuto,
				  bool pDoubleSize,
				  const ctaModule* pCtaModule)
{
  const ddmAutomaton* lAuto = pAuto.getDerivedObjDdm();

  // Compute dimension to use.
  int lDim = (pDoubleSize ? 2 : 1)*lAuto->getVarIds().size();
  
  // Compute vector representing E1>=E2.
  //   Negation represents E1<=E2.
  ddmVec lVec(mE1->mkDdmVec(pAuto, pDoubleSize, pCtaModule)
	      - mE2->mkDdmVec(pAuto, pDoubleSize, pCtaModule));

  // Start with TRUE (means full space of right dimension).
  ddmRegion result(ddmRegion(lDim, true));

  if(mRelOp->GetOp() == ctaRelOp::EQUAL)
  {
    // Must fulfill both E1>=E2 and E1<=E2.
    result.intersect(ddmRegion(lDim, lVec));
    result.intersect(ddmRegion(lDim, -lVec));
  }
  else if(mRelOp->GetOp() == ctaRelOp::LESS)
  {
    // Make inequality strict.
    //   (lVec[0]) has to be 1.
    //   Since it will be inverted by the second line
    //   we have to set it to (-1).
    lVec[0] = -1;
    result.intersect(ddmRegion(lDim, -lVec));
  }
  else if(mRelOp->GetOp() == ctaRelOp::GREATER)
  {
    // Make inequality strict.
    lVec[0] = 1;
    result.intersect(ddmRegion(lDim, lVec));
  }
  else if(mRelOp->GetOp() == ctaRelOp::NOTEQUAL)
  {
    // Make inequality strict.
    lVec[0] = 1;

    // Either E1>E2, or E1<E2.
    result.intersect(ddmRegion(lDim, lVec));

    // Make inequality strict.
    //   (lVec[0]) has to be 1.
    //   Since it will be inverted by the second line
    //   we have to set it to (-1).
    lVec[0] = -1;
    result.unite(ddmRegion(lDim, -lVec));
  }
  else if(mRelOp->GetOp() == ctaRelOp::LESSTHAN)
  {
    result.intersect(ddmRegion(lDim, -lVec));
  }
  else // (mRelOp->GetOp() == ctaRelOp::GREATERTHAN)
  {
    result.intersect(ddmRegion(lDim, lVec));
  }

  result.normalize();

  return result;
}

bddConfig
ctaRestRel::mkBddConfig(const bddSymTab* pSymTab,
			const ctaModule* pCtaModule)
{

  bddExpression* lE1 = mE1->mkBddExpression(pCtaModule);
  bddExpression* lE2 = mE2->mkBddExpression(pCtaModule);

  bddConfig result(pSymTab, false);

  if(mRelOp->GetOp() == ctaRelOp::EQUAL)
  {
    result = bddConfig::mkEqual(pSymTab, lE1, lE2);
  }
  else if(mRelOp->GetOp() == ctaRelOp::LESS)
  {
    result = bddConfig::mkGreater(pSymTab, lE2, lE1);
  }
  else if(mRelOp->GetOp() == ctaRelOp::GREATER)
  {
    result = bddConfig::mkGreater(pSymTab, lE1, lE2);
  }
  else if(mRelOp->GetOp() == ctaRelOp::NOTEQUAL)
  {
    result = bddConfig::mkGreater(pSymTab, lE1, lE2);
    result.unite(bddConfig::mkGreater(pSymTab, lE2, lE1));
  }
  else if(mRelOp->GetOp() == ctaRelOp::LESSTHAN)
  {
    result = bddConfig::mkGreater(pSymTab, lE2, lE1);
    result.unite(bddConfig::mkEqual(pSymTab, lE1, lE2));
  }
  else // (mRelOp->GetOp() == ctaRelOp::GREATERTHAN)
  {
    result = bddConfig::mkGreater(pSymTab, lE1, lE2);
    result.unite(bddConfig::mkEqual(pSymTab, lE1, lE2));
  }


  delete lE1;
  delete lE2;
  
  return result;
}

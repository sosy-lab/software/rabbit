#ifndef _reprNUMBER_h_
#define _reprNUMBER_h_


// This file defines the basic number-type to use.

#ifdef WIN32
//typedef long reprNUMBER;
typedef int reprNUMBER;
#endif

#ifndef WIN32
//typedef long long reprNUMBER;
typedef int reprNUMBER;
#endif

// Local Variables:
// mode:C++
// End:

#endif // _reprNUMBER_h_

//-*-Mode: C++;-*-

/*
 * File:        ctaExprConst.h
 * Purpose:     Const-Expression Class for cta-library
 * Author:      H. Rust
 * Created:     1998
 * Copyright:   (c) 1998, University of Brandenburg, Cottbus
 */

#ifndef _ctaExprConst_h
#define _ctaExprConst_h


#include "ctaExpression.h"
// class ctaModule;

#include "ddmVec.h"
#include "bddExpression.h"

class ctaExprConst : public ctaExpression
{
private:

  reprNUMBER mConst;

  // It should be not allowed to use the standard operators.
  ctaExprConst(const ctaExprConst&);
  void operator=(const ctaExprConst&);
  void operator,(const ctaExprConst&);

public:
  // constructor and destructor
  ctaExprConst(int pConst)
    : mConst(pConst)
  {}

  // accessor methods

  // Service methods.

  // Clone (this).
  ctaExpression*
  cloneSetPrefixAndIdent(const ctaString *const pPrefix,
			 ctaMap<ctaString*>* pIdentNames) const
  {
    // Create the clone.
    ctaExpression* theClone = new ctaExprConst(mConst);
    
    return theClone;
  }

  // Check the derivation for correctness.
  void
  CheckDerivation(const ctaModule* const pModule) const
  {
    // Nothing to do because (this) has no members.
  }

  // This method is for context analysis.
  // We give the parent module for accessing module features.
  void
  CheckConsistency(const ctaModule* const pModule)
  {
  }

  bool
  VariableOccurs(ctaString *pId)
  {
    return false;
  }

  bool
  VariableOccursTicked(ctaString *pId)
  {
    return false;
  }

  // aheinig 22.03.2000
  // any number of constants are allowed
  bool 
  CheckBddConstraint() const
  {
    return true;
  }

  // aheinig 22.03.2000
  // count all used variable, constants are no variables
  int 
  CountVariables(const ctaModule* pCtaModule) const
  { 
    return 0;
  }

  // aheinig 22.03.2000
  // count all used clocks, constants are no variables(clocks)
  int 
  CountClocks(const ctaModule* pCtaModule) const
  { 
    return 0;
  }

  void 
  Print(ostream& s) const
  {
    s << mConst;
  }

  // aheinig 2000-11-02
  // Collect all used signals and variables for this automaton.
  void 
  collectInterfacesForCommunicationGraph(ctaSet<ctaString>* lUsedInterfaces)
  {}

  // Construct a vector representing (*this) >= 0.
  ddmVec
  mkDdmVec(const reprAutomaton& pAuto,
	   bool pDoubleSize,
	   const ctaModule* pCtaModule)
  {
    // Compute dimension to use.
    ddmVec result;
    result[1] = mConst;

    return result;
  }

  bddExpression*
  mkBddExpression (const ctaModule* pCtaModule) {
    return new bddExpression("", false, mConst);
  }
  
};
#endif

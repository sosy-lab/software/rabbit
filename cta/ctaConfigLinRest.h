//-*-Mode: C++;-*-

/*
 * File:        ctaConfigLinRest.h
 * Purpose:     Linear-Restriction-Configuration class for cta-library
 * Author:      H. Rust
 * Created:     1998
 * Copyright:   (c) 1998, University of Brandenburg, Cottbus
 */

#ifndef _ctaConfigLinRest_h
#define _ctaConfigLinRest_h


class ctaComponent;
#include "ctaMap.h"
class ctaModule;
#include "ctaConfiguration.h"
#include "ctaRestFalse.h"

#include "ddmConfig.h"
#include "ddmAutomaton.h"

#include "bddConfig.h"

class ctaConfigLinRest : public ctaConfiguration
{
private:

  ctaRestriction* mRestriction;

  // It should be not allowed to use the standard operators.
  ctaConfigLinRest(const ctaConfigLinRest&);
  void operator=(const ctaConfigLinRest&);
  void operator,(const ctaConfigLinRest&);

public:
  // constructor and destructor
  ctaConfigLinRest(ctaRestriction* pRestriction)
    : mRestriction(pRestriction)
  {}

  ~ctaConfigLinRest()
  {
    if(mRestriction)
    {
      delete mRestriction;
    }
  }
  
  // accessor methods

  ctaRestriction*
  GetRestriction() const
  {
    return mRestriction;
  }

  // aheinig 26.09.2000
  // Clone (this). 
  ctaConfiguration* 
  cloneSetPrefixAndIdent(const ctaString *const pPrefix,
			 ctaMap<ctaString*>* pIdentNames) const
  {        

    // Create the cloned restriction.
    ctaRestriction* lRestClone = mRestriction->cloneSetPrefixAndIdent(pPrefix,
								  pIdentNames);

    ctaConfiguration* lConfigLinRestClone = new ctaConfigLinRest(lRestClone);
				     
    return lConfigLinRestClone;
  } 

  // Service methods.

  // false if used >, <, != by clocks
  virtual bool
  CheckNonStrictness(const ctaModule* pCtaModule) const
  {
    return mRestriction->CheckNonStrictness(pCtaModule);
  }

  virtual bool
  CheckBddConstraint(const ctaModule* pCtaModule) const
  {
    return mRestriction->CheckBddConstraint(pCtaModule, true);
  }


  // This method is for context analysis.
  // We give the parent module for accessing module features.
  virtual void 
  CheckConsistency(const ctaModule* pModule)
  {
    mRestriction->CheckConsistency(pModule);
  }

  virtual void 
  Print(ostream& s) const
  {
    mRestriction->Print(s);
  }

  ddmConfig
  mkDdmConfig(const reprAutomaton& pAuto,
	      const ctaModule* pCtaModule) const
  {
    const ddmAutomaton* p = pAuto.getDerivedObjDdm();

    ddmConfig result(p->getVarIds().size(),
		     p->getStates().size());

    // Construct a region for the given restriction.
    ddmRegion lRegion(mRestriction->mkDdmRegion(pAuto, false,
						pCtaModule));

    const ddmAutomaton* lAuto = pAuto.getDerivedObjDdm();

    // For each state, insert the same region into (result).
    for(int i=0; i < (int)lAuto->getStates().size(); ++i)
    {
      result[i] = lRegion;
    }

    return result;
  }

  bddConfig
  mkBddConfig(const bddSymTab* pSymTab,
	      const ctaModule* pCtaModule) const
  {
    return mRestriction->mkBddConfig(pSymTab, pCtaModule);
  }

  virtual ctaConfiguration*
  collectStatesForAutomaton(const ctaModule* pCtaModule, 
			    const ctaString* pAutoName) const
  {
    return new ctaConfigLinRest(new ctaRestFalse());
  }
};
#endif

//-*-Mode: C++;-*-

/*
 * File:        ctaObject.h
 * Purpose:     Top Level Object for cta-library
 * Author:      Dirk Beyer
 * Created:     1998
 * Copyright:   (c) 1998, University of Brandenburg, Cottbus
 */

// The purpose of this class is only to count the number
//   of objects instantiated within the Rabbit library.
// This is useful for checking memory deallocation.


#ifndef _ctaObject_h
#define _ctaObject_h

#include <iostream>

#ifdef WIN32
/*
  Using this #pragma for acoid the compiler level 1 warning c4786 under VC++.
  Warning in VC++:
    Die Zeichenfolge f��r ein title- oder subtitle-Pragma ��berschritt die 
    maximal erlaubte L��nge und wurde verk��rzt. Der Debugger kann keine 
    Fehler in Code beheben, der Symbole enth��lt, die l��nger als 255 Zeichen 
    sind. Im Debugger k��nnen Sie die verk��rzten Symbole nicht anzeigen, 
    auswerten, aktualisieren oder ��berwachen.

    Durch die Verk��rzung von Bezeichnernamen k��nnen Sie diese Beschr��nkung 
    umgehen. 
*/
#pragma warning (disable : 4786)
using namespace std;
#endif

#ifndef WIN32
#endif

#define min(a, b)  (((a) < (b)) ? (a) : (b))
#define max(a, b)  (((a) > (b)) ? (a) : (b))

class ctaObject
{
private:
  static long count;  // This counts the number of instatiated objects.

  // It should be not allowed to use the standard operators.
  // Uses the standard operator '='.
  void operator,(const ctaObject&);

public:
  // constructor and destructor
  ctaObject()
  {
    xyz();     // This empty function is to avoid compiler warnings.
               //   May be it results from a compiler bug.

    ++count;   // We count the number of ctaObjects.
  }
  
  // We define a copy constructor for the case that
  //   an implicitly defined copy constructor of
  //   a subclass is used.
  ctaObject(const ctaObject& pObj)
  {
    ++count;			// We count the number of ctaObjects.
  }
  
  // this virtual destructor forces that all derivated components
  // also have virtual destructors without virtual declaration
  virtual ~ctaObject()
  {
    --count;
  }
  
  void xyz()
  {
  }

  // accessor methods
  int GetCount() const
  {
    return(count);
  }
};

ostream& operator << (ostream& out, const ctaObject* obj);

#endif

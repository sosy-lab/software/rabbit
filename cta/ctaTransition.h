//-*-Mode: C++;-*-

/*
 * File:        ctaTransition.h
 * Purpose:     Transition Class for cta-library
 * Author:      Dirk Beyer
 * Created:     1998
 * Copyright:   (c) 1998, University of Brandenburg, Cottbus
 */

#ifndef _ctaTransition_h
#define _ctaTransition_h

class ctaAutomaton;
class ctaModule;
#include "ctaObject.h"
#include "ctaRestriction.h"
#include "ctaSetPointer.h"
#include "ctaString.h"
#include "ctaSynchronisation.h"
#include "ctaRestriction.h"

class ddmAutomaton;
class ddmTransition;

#include "bddSymTab.h"
#include "bddAutomaton.h"
#include "bddTransition.h"

class ctaTransition : private ctaObject
{
private:
  // Aggregated.
  ctaString*            mFollowerState;   
  ctaRestriction*       mGuard;
  ctaRestriction*       mAllow;
  ctaSynchronisation*   mSync;
  

  // It should be not allowed to use the standard operators.
  ctaTransition(const ctaTransition&);
  void operator=(const ctaTransition&);
  void operator,(const ctaTransition&);

public:
  // constructor and destructor
  ctaTransition(ctaString* pFollowerState,
                ctaRestriction* pGuard,
                ctaSynchronisation* pSync,
                ctaRestriction* pAllow);

  ~ctaTransition()
  {
    if(!mFollowerState || !mGuard || !mAllow || !mSync)
    {
      cerr << "Runtime error: Component pointer in ctaTransition is NULL!"
       << endl;
      exit(1);
    }
    delete mFollowerState;
    delete mGuard;
    delete mAllow;
    delete mSync;
  }

  // accessor methods

  ctaString* 
  GetState() const
  {
    return(mFollowerState);
  }

  ctaRestriction* 
  GetGuard() const
  {
    return(mGuard);
  }

  ctaRestriction* 
  GetAllow() const
  {
    return(mAllow);
  }

  ctaSynchronisation* 
  GetSync() const
  {
    return(mSync);
  }

  // aheinig 06.09.2000
  // Clone (this)
  ctaTransition* 
  cloneSetPrefixAndIdent(const ctaString *const pPrefix,
             ctaMap<ctaString*>* pIdentNames) const
  {        
    // Create the cloned guard.
    ctaRestriction* lGuardClone = mGuard->cloneSetPrefixAndIdent(pPrefix, 
                                 pIdentNames);
    // Create the cloned allow.
    ctaRestriction* lAllowClone = mAllow->cloneSetPrefixAndIdent(pPrefix, 
                                 pIdentNames);
    // Create the cloned sync.
    ctaSynchronisation* lSyncClone = mSync->cloneSetPrefixAndIdent(pPrefix,
                                  pIdentNames);

    ctaString* lFollowerStateClone = new ctaString(*pPrefix + *mFollowerState);

    ctaTransition* lTransClone = new ctaTransition(lFollowerStateClone,
                                                   lGuardClone,
                                                   lSyncClone,
                                                   lAllowClone);

    return lTransClone;
  } 

  // Service methods.
  // This method is for context analysis.
  // We give the parent module for accessing module features.

  // aheinig 15.03.2000
  // false: if used >, <, != by clocks
  // true : else
  bool 
  CheckNonStrictness(const ctaModule* pCtaModule) const
  {
    return ( mGuard->CheckNonStrictness(pCtaModule) && 
         mAllow->CheckNonStrictness(pCtaModule) );
  }

  // aheinig 22.03.2000
  // true:  if CheckBddConstraint true for mGuard, mAllow
  // false: else
  bool
  CheckBddConstraint(const ctaModule* pCtaModule) const
  {
    return ( mGuard->CheckBddConstraint(pCtaModule, true) && 
         mAllow->CheckBddConstraint(pCtaModule, true) );
  }

  void 
  CheckConsistency(ctaModule*, ctaAutomaton*);

  // aheinig 2000-11-02
  // Collect all used signals and variables for this automaton.
  void 
  collectInterfacesForCommunicationGraph(ctaSet<ctaString>* lUsedInterfaces);

  void 
  print(ostream& s) const;

  ddmTransition*
  mkDdmTransition(const ddmAutomaton& pAuto,
          const ctaString& pOriginState,
          const ctaString& pTargetState,
          const ctaModule* pCtaModule);

  bddTransition*
  mkBddTransition(const ctaString* pOriginState,
          const ctaString* pTargetState,
          const bddSymTab* pBddSymTab,           
          const ctaModule* pCtaModule,
          const ctaString* pParentAutomatonName);
};

ostream& operator << (ostream&, const ctaTransition*);

#endif

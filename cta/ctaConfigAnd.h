//-*-Mode: C++;-*-

/*
 * File:        ctaConfigAnd.h
 * Purpose:     And-Configuration class for cta-library
 * Author:      H. Rust
 * Created:     1998
 * Copyright:   (c) 1998, University of Brandenburg, Cottbus
 */

#ifndef _ctaConfigAnd_h
#define _ctaConfigAnd_h


class ctaComponent;
#include "ctaMap.h"
class ctaModule;
#include "ctaConfiguration.h"

class ddmAutomaton;
#include "ddmConfig.h"

#include "bddConfig.h"

class ctaConfigAnd : public ctaConfiguration
{
private:
  ctaConfiguration *mC1;
  ctaConfiguration *mC2;

  friend ctaConfiguration*
  ctaConfiguration::mkAnd(ctaConfiguration* p1, ctaConfiguration* p2);

  // constructor and destructor
private:
  // Only (ctaConfiguration::mkAnd) should call this constructor.
  ctaConfigAnd(ctaConfiguration *pC1,
	       ctaConfiguration *pC2)
    : mC1(pC1),
      mC2(pC2)
  {}

  // It should be not allowed to use the standard operators.
  ctaConfigAnd(const ctaConfigAnd&);
  void operator=(const ctaConfigAnd&);
  void operator,(const ctaConfigAnd&);

public:
  ~ctaConfigAnd()
  {
    if(!mC1 || !mC2) 
    {
      cerr << "Runtime error: Null pointer deletion in ctaConfigAnd"
	   << endl;
      abort();
    }
    delete mC1;
    delete mC2;
  }
  
  // accessor methods

  // Service methods.

  // aheinig 06.09.2000
  // Clone (this). 
  ctaConfiguration* 
  cloneSetPrefixAndIdent(const ctaString *const pPrefix,
			 ctaMap<ctaString*>* pIdentNames) const
  {        
    // Create the cloned config1.
    ctaConfiguration* lConfigClone1 = mC1->cloneSetPrefixAndIdent(pPrefix, 
								  pIdentNames);
    // Create the cloned config2.
    ctaConfiguration* lConfigClone2 = mC2->cloneSetPrefixAndIdent(pPrefix, 
								  pIdentNames);
    // Create the cloned configAnd.
    ctaConfiguration* lConfigAndClone = new ctaConfigAnd(lConfigClone1,
						     lConfigClone2);
				     
    return lConfigAndClone;
  } 

  // aheinig 15.03.2000 
  // false: if used >, <, != by clocks
  // true : else
  virtual bool CheckNonStrictness(const ctaModule* pCtaModule) const
  {
    return ( mC1->CheckNonStrictness(pCtaModule) && 
	     mC2->CheckNonStrictness(pCtaModule) );
  }
  
  // aheinig 23.03.2000
  // true : if CheckBddConstraint true right and left side of the AND
  // false: else
  virtual bool
  CheckBddConstraint(const ctaModule* pCtaModule) const
  {
    return ( mC1->CheckBddConstraint(pCtaModule) && 
	     mC2->CheckBddConstraint(pCtaModule) );
  }

  // This method is for context analysis.
  // We give the parent module for accessing module features.
  virtual void CheckConsistency(const ctaModule* pModule)
  {
    mC1->CheckConsistency(pModule);
    mC2->CheckConsistency(pModule);
  }

  virtual void 
  Print(ostream& s) const
  {
    s << "(" << mC1 << ")" 
      << " AND " 
      << "(" << mC2 << ")";
  }

  ddmConfig
  mkDdmConfig(const reprAutomaton& pAuto,
	      const ctaModule* pCtaModule) const
  {
    ddmConfig result(mC1->mkDdmConfig(pAuto, pCtaModule));
    result.intersect(mC2->mkDdmConfig(pAuto, pCtaModule));
    return result;
  }

  bddConfig
  mkBddConfig(const bddSymTab* pSymTab,
	      const ctaModule* pCtaModule) const
  {
    bddConfig result(mC1->mkBddConfig(pSymTab, pCtaModule));
    result.intersect(mC2->mkBddConfig(pSymTab, pCtaModule));
    return result;
  }

  virtual ctaConfiguration*
  collectStatesForAutomaton(const ctaModule* pCtaModule, 
			    const ctaString* pAutoName) const
  {
    return ctaConfiguration::mkOr(
	     mC1->collectStatesForAutomaton(pCtaModule, pAutoName),
	     mC2->collectStatesForAutomaton(pCtaModule, pAutoName)
	     );
  }
};
#endif

//-*-Mode: C++;-*-

/*
 * File:        ctaInstantiation.h
 * Purpose:     Instantiation Class for cta-library
 * Author:      Dirk Beyer
 * Created:     1998
 * Copyright:   (c) 1998, University of Brandenburg, Cottbus
 */

#ifndef _ctaInstantiation_h
#define _ctaInstantiation_h


#include <vector>


#include "ctaObject.h"
#include "ctaString.h"
class ctaSystem;
class ctaModule;
#include "ctaMap.h"
#include "ctaComponent.h"
#include "ctaConfiguration.h"

class ctaInstantiation : private ctaObject
{
private:
  // Aggregated.
  ctaString*	mInstanceName;
  ctaString*	mTemplateName;

  // Associated. Set by context check.
  ctaModule*	mTemplateModule;

  // Ident function for identifier mapping.
  // Aggregated. Built by parser.
  ctaMap<ctaString*>* mIdentNames;

  // Ident function for identifier mapping.
  // Aggregated. Built by context check.
  ctaMap<ctaComponent*>* mIdentComps;   

  // To allow a parametric initialization of the module instance.
  // Aggregated.
  ctaConfiguration* mInitialConfig;
  
  // It should be not allowed to use the standard operators.
  ctaInstantiation(const ctaInstantiation&);
  void operator=(const ctaInstantiation&);
  void operator,(const ctaInstantiation&);

public:
  // constructor and destructor
  ctaInstantiation(ctaString* pInstanceName,
		   ctaString* pTemplateName,
		   ctaMap<ctaString*>*  pIdentNames,
		   ctaConfiguration* pInitialConfig)
    : mInstanceName(pInstanceName),
      mTemplateName(pTemplateName),
      mTemplateModule(NULL),
      mIdentNames(pIdentNames),
      mIdentComps(NULL),
      mInitialConfig(pInitialConfig)
  {}

  ~ctaInstantiation();
  
  // accessor methods

  ctaString* 
  GetInstanceName() const
  {
    return(mInstanceName);
  }
  
  ctaString* 
  GetTemplateName() const
  {
    return(mTemplateName);
  }
  
  void 
  SetTemplateModule(ctaModule* m)
  {
    if(mTemplateModule)
    {
      CTAERR << "Runtime error in Method ctaInstantiation::"
	     << "SetTemplateModule: mTemplateModule already"
	     << " associated!" << endl;
      CTAPUT;
    }

    mTemplateModule = m;
  }

  ctaModule* 
  GetTemplateModule() const
  {
    return(mTemplateModule);
  }
  
  ctaMap<ctaString*>* 
  GetConnections() const
  {
    return(mIdentNames);
  }
  
  ctaMap<ctaComponent*>* 
  GetIdentComps() const
  {
    return(mIdentComps);
  }
  
  ctaConfiguration* 
  GetInitialConfig() const
  {
    return(mInitialConfig);
  }
  
  // Service methods.
  // This method is for context analysis.
  // We give the parent module for accessing module features.
  // Also it has to know all declared modules in system.

  bool 
  CheckNonStrictness(const ctaModule* pCtaModule) const;  

  bool 
  CheckBddConstraint(const ctaModule* pCtaModule) const;

  void 
  CheckConsistency(ctaSystem* pSystem, ctaModule* pModule);
  
  void 
  CountRestrictions(ctaComponent* pCompo,
		    int& pNrMultrestRestrictions,
		    int& pNrOutputRestrictions);
};

ostream& operator << (ostream &, const ctaInstantiation*);

#endif

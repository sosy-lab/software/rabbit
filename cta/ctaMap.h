//-*-Mode: C++;-*-

/*
 * File:        ctaMap.h
 * Purpose:     Map Class for cta-library
 * Author:      Dirk Beyer
 * Created:     1998
 * Copyright:   (c) 1998, University of Brandenburg, Cottbus
 */

#ifndef _ctaMap_h
#define _ctaMap_h


// for new sdt-lib
#include <map>

#include "ctaErrMsg.h"
#include "ctaString.h"

// This is a map for references.
// Destructor deletes the referenced objects.
template<class E>
class ctaMap : public map<const ctaString, E>, private ctaObject
{
private:

  // It should be not allowed to use the standard operators.
  void operator=(const ctaMap&);
  void operator,(const ctaMap&);
  
public:
  // traversion for elements idx of list S
#define ctaForAll_Map(S, idx, TYPE) \
  for(ctaMap<TYPE>::iterator idx = (S)->begin();\
      idx != (S)->end();\
      ++idx )
  
#define ctaForAll_Map_const(S, idx, TYPE) \
  for(ctaMap<TYPE>::const_iterator idx = (S)->begin();\
      idx != (S)->end();\
      ++idx )

  explicit ctaMap()
    : map<const ctaString, E>(),
      ctaObject()
  {}
  
  explicit ctaMap(const ctaMap<E>& pMap)
    : map<const ctaString, E>(pMap),
      ctaObject()
  {}
  
  ~ctaMap()
  {
    // it works with an empty set
    ctaForAll_Map(this, idx, E)
    {
      if(idx->second == NULL)
      {
	cerr << "Runtime error: Deletion of NULL pointer!"
	     << endl;
      }
      else
      {
	delete idx->second; 
      }
    }    
  }
  
  void append(const ctaString* str, const E item)
  {  
    if(! insert(pair<ctaString, E>(*str, item)).second)
    {
      CTAERR << "\'" << str << "\' previously declared/used." << endl;
      CTAPUT;
    }
  }
  

  void join(ctaMap<E>* l)
  {
    // inserting map l with append respecting to redeclaration 
    ctaForAll_Map(l, idx, E)
    {
      // we append the idx-th element
      append(&(idx->first), idx->second); 
    }    
  }

  // We use a special operator for const-[]-access.
  E 
  operator[](const ctaString p) const
  {
    const_iterator it = this->find(p);

    if(it != this->end())
    {
      return it->second;
    }
    else
    {
      return 0;
    }
  }

  E&
  operator[](const ctaString p)
  {
    return ((map<const ctaString, E>*)this)->operator[](p);
  }
};

template<class E> ostream& operator << (ostream& out, const ctaMap<E>* obj)
{
  // it works with an empty set
  ctaForAll_Map_const(obj, idx, E)
  {
    // we take the idx-th element in set parsedModules
    out << idx->second; 
  }
  return(out);
}

#endif

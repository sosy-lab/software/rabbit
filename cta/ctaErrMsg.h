//-*-Mode: C++;-*-

/*
 * File:        ctaErrMsg.h
 * Purpose:     Error message class for cta-library
 * Author:      H. Rust
 * Created:     1998
 * Copyright:   (c) 1998, University of Brandenburg, Cottbus
 */

#ifndef _ctaErrMsg_h
#define _ctaErrMsg_h


#include <iostream>
#include <strstream>

#include "ctaObject.h"
#include "ctaString.h"

// Error message class: Contains:
//   1. A counter for the errors which occured.
//   2. An output stream.
//   3. A current ctaString prefix.
//   4. A strstream to collect the next error message.


// Goals of this class:
//   1. It is possible to use prefix ctaString to explain the kind
//      of check (f. e. context check) which found the error.
//   2. Usage of mErrCount: - After a count of errors the parser can exit.
//                          - After parsing the count of errors occured
//                            can be printed out.

class ctaErrMsg : private ctaObject
{
private:
  int mErrCount;

  // Associated.
  ostream& mOstream;

  // Aggregated.
  const ctaString* mPrefix;
  ostrstream* mMsg;

  // It should be not allowed to use the standard operators.
  ctaErrMsg(const ctaErrMsg&);
  void operator=(const ctaErrMsg&);
  void operator,(const ctaErrMsg&);

public:

  // Constructor and Destructor.

  ctaErrMsg(ostream& pOstream)
    : mErrCount(0),
      mOstream(pOstream),
      mPrefix(NULL),
      mMsg(new ostrstream())
  {}

  ~ctaErrMsg()
  {
    if(mPrefix)
    {
      delete mPrefix;
    }

    if(mMsg)
    {
      delete mMsg;
    }
  }

  // Accessors.

  int
  GetErrCount() const
  {
    return mErrCount;
  }

  void
  SetPrefix(const ctaString* pPrefix)
  {
    mPrefix = pPrefix;
  }

  const ctaString* GetPrefix()
  {
    return mPrefix;
  }

  ostrstream*
  GetMsg()
  {
    return mMsg;
  }

  // Service.

  void
  Out()
  {
    ++mErrCount;

    if(mPrefix)
    {
      mOstream << mPrefix;
    }
    else
    {
      mOstream << "NULL errMsg! :";
    }

    // String termination.
    mMsg->put(0);

    mOstream << mMsg->str()
	     << flush;

    delete mMsg;
    mMsg = new ostrstream();
  }
};

extern ctaErrMsg* pubErrMsg;

#define CTAERR (*pubErrMsg->GetMsg())
#define CTAPUT (pubErrMsg->Out())

#endif

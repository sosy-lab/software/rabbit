//-*-Mode: C++;-*-

/*
 * File:        ctaSystem.h
 * Purpose:     System Class for cta-library
 * Author:      Dirk Beyer
 * Created:     1998
 * Copyright:   (c) 1998, University of Brandenburg, Cottbus
 */

#ifndef _ctaSystem_h
#define _ctaSystem_h

#include <vector>
class ctaString;

//#include "ctaSetPointer.h"
#include "ctaMap.h"
#include "ctaModule.h"
#include "ddmAutomaton.h"
#include "anaSection.h"

class anaMain;

class ctaSystem : private ctaObject
{
  // all attributes are pointers !!!
private:
  // This is a vector instead of map to preserve the order
  // of the modules in the file. This is necessary for context check:
  // Cyclic instantiation is forbidden.
  // Aggregated.
  vector<ctaModule*>* mModules;

  // The syntax tree of the analysis section.
  //   To be executed (as interpreter) by using the recursive
  //   method (Execute).
  // Contains a list of verification sections.
  // Aggregated. Built by parser.
  vector<anaSection*>* mAnaSections;
  

  // It should be not allowed to use the standard operators.
  ctaSystem(const ctaSystem&);
  void operator=(const ctaSystem&);
  void operator,(const ctaSystem&);

public:
  // constructor and destructor
  ctaSystem(vector<ctaModule*>* pModules,
	    vector<anaSection*>* pAnaSections)
    : mModules(pModules), // Only the pointer constructor.
      mAnaSections(pAnaSections)
  {}

  ~ctaSystem();
  
  vector<ctaModule*>* 
  GetModules() const
  {
    return mModules;
  }

  vector<anaSection*>* 
  GetAnalysis() const
  {
    return mAnaSections;
  }

  // Service methods.
  void PrepareCtaModel();
  void CheckConsistency();

  void ExecuteAnalysis();
  
  // The caller owns the memory of the result.
  set<ctaString>* GetTopModules() const;

  void PrintObserverModules() const;

  void PrintHyTech(const ctaString& pModuleName, 
		   ostream* pOutputStream) const;

  ctaModule* findModule(ctaString pModName) const;
};

ostream& operator << (ostream &, const ctaSystem*);

extern ctaSystem* pubParsedSystem;

#endif

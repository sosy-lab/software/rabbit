/*
 * File:        ctaRestriction.cpp
 * Purpose:     Restriction Class for cta-library
 * Author:      Dirk Beyer
 * Created:     1998
 * Copyright:   (c) 1998, University of Brandenburg, Cottbus
 */

#include "ctaRestriction.h"
#include "ctaRestAnd.h"
#include "ctaRestOr.h"
#include "ctaRestNot.h"
#include "ctaRestTrue.h"
#include "ctaRestFalse.h"

ctaRestriction*
ctaRestriction::clone() const
{
  ctaString* lString = new ctaString("");
  ctaRestriction* theClone = this->cloneSetPrefixAndIdent(lString,
							  NULL);
  delete lString;
  return theClone;
}

bool
ctaRestriction::IsTrue() const
{
  return 0 != dynamic_cast<const ctaRestTrue*>(this);
}

bool
ctaRestriction::IsFalse() const
{
  return 0 != dynamic_cast<const ctaRestFalse*>(this);
}

ctaRestriction*
ctaRestriction::mkNot(ctaRestriction* p1)
{
  ctaRestriction* result;
  if(p1->IsFalse())
  {
    delete p1;
    result = new ctaRestTrue();
  }
  else if(p1->IsTrue())
  {
    delete p1;
    result = new ctaRestFalse();
  }
  else
  {
    result = new ctaRestNot(p1);
  }

  return result;
}

ctaRestriction*
ctaRestriction::mkAnd(ctaRestriction* p1, ctaRestriction* p2)
{
  ctaRestriction* result;

  if(p1->IsFalse())
  {
    delete p2;
    result = p1;
  }
  else if(p2->IsFalse())
  {
    delete p1;
    result = p2;
  }
  else if(p1->IsTrue())
  {
    delete p1;
    result = p2;
  }
  else if(p2->IsTrue())
  {
    delete p2;
    result = p1;
  }
  else
  {
    result = new ctaRestAnd(p1, p2);
  }

  return result;
}
 
ctaRestriction*
ctaRestriction::mkOr(ctaRestriction* p1, ctaRestriction* p2)
{
  ctaRestriction* result;

  if(p1->IsTrue())
  {
    delete p2;
    result = p1;
  }
  else if(p2->IsTrue())
  {
    delete p1;
    result = p2;
  }
  else if(p1->IsFalse())
  {
    delete p1;
    result = p2;
  }
  else if(p2->IsFalse())
  {
    delete p2;
    result = p1;
  }
  else
  {
    result = new ctaRestOr(p1, p2);
  }

  return result;
}
 
ostream& operator << (ostream& out, const ctaRestriction* obj)
{
  obj->Print(out);
  return(out);
};


/*
 * File:        ctaHyTechPrinter.h
 * Purpose:     HyTech printer for syntax trees in cta-library
 * Author:      Dirk Beyer & Sebastian Schmerl
 * Created:     1999
 * Copyright:   (c) 1999, University of Brandenburg, Cottbus
 */

#include "ctaHyTechPrinter.h"

#include "ctaAutomaton.h"
#include "ctaConfigState.h"
#include "ctaConfiguration.h"
#include "ctaErrMsg.h"
#include "ctaInstantiation.h"
#include "ctaModule.h"
#include "ctaState.h"
#include "ctaTransition.h"
#include "ctaRestriction.h"
#include "ctaRestTrue.h"
#include "ctaRestFalse.h"


void ctaHyTechPrinter::PrintModuleToHyTech(const ctaModule* pModule) const
{
  //Save the old error prefix und set the new one.
  const ctaString* lOldPrefix = pubErrMsg->GetPrefix();
  pubErrMsg->SetPrefix( new ctaString("PrintHyTech error: (in ctaHyTechPrinter-ctaModule: ") );
  // Output of header.
  mOutStream << "-- HyTech-code generated from CTA-Module \""
         << pModule->GetName()
         << "\"."
         << endl
         << endl;

  // output of constants
  ctaForAll_Map(pModule->GetInterface(), idx, ctaComponent*)
  {
    ctaComponent* lComp = idx->second;

    if( lComp->GetDataType() == ctaComponent::CONST )
    {
      if(lComp->GetValue() == NULL)
      {
    CTAERR << "Runtime error: Value of constant '" << lComp->GetName()
           << "' undefined in module '" << pModule->GetName() 
           << "'!" << endl;
    CTAPUT;
      }
      else
      {
    mOutStream << "define("
           << lComp->GetName()
           << ", "
           << *( lComp->GetValue() )
           << ")"
           << endl;
      }
    }
  }
  
  // output of variables
  mOutStream << endl
         << "var"
         << endl;

  {
    ctaForAll_Map(pModule->GetInterface(), idx, ctaComponent*)
    {
      ctaComponent* lComp = idx->second;
      if( lComp->GetDataType() == ctaComponent::DISCRETE )
      {
    // Replace dots with underscares in variable names.
    mOutStream << "  "
           << lComp->GetName()->ReplaceChars('.','_')
           << ": "
           << "discrete"
           << ";"
           << endl;
      }
      if( lComp->GetDataType() == ctaComponent::CLOCK )
      {
    mOutStream << "  "
           << lComp->GetName()->ReplaceChars('.','_')
           << ": "
    // We print the restriction (x = 1) in the derivation.
    // So we must declare it as stopwatch.
    // << "clock"
           << "stopwatch"
           << ";"
           << endl;
      }
      if( lComp->GetDataType() == ctaComponent::STOPWATCH )
      {
    mOutStream << "  "
           << lComp->GetName()->ReplaceChars('.','_')
           << ": "
           << "stopwatch"
           << ";"
           << endl;
      }
      if( lComp->GetDataType() == ctaComponent::ANALOG )
      {
    mOutStream << "  "
           << lComp->GetName()->ReplaceChars('.','_')
           << ": "
           << "analog"
           << ";"
           << endl;
      }
    }
  }

  // Output of automata.
  ctaForAll_Map(pModule->GetAutomata(), a, ctaAutomaton*)
  {
    // Replace dots with underscores in automaton names.
    mOutStream << endl
           << "automaton "
           << a->second->GetName()->ReplaceChars('.','_')
           << endl;

    // Find out what the alphabet of synclabs is.
    ctaMap<char*>* lSignals = new  ctaMap<char*>();
    ctaForAll_Map(a->second->GetStates(), s, ctaState*)
    {
      ctaForAll_SetPointer(s->second->GetTransitions(), t, ctaTransition*)
      {
    if( *( (*t)->GetSync()->GetSignalName() ) != "")
    {
      lSignals->insert( pair<ctaString, char*>
                ( *((*t)->GetSync()->GetSignalName()), NULL ) );
    }
      }
    }

    mOutStream << "  synclabs:"
           << endl;
    bool lFirst = true; // To count the synclabs for setting kommata.
    ctaForAll_Map(lSignals, idx, char*)
    {
      // Replace dots with underscores in signal names.
      if(!lFirst)
      {
    mOutStream << ","
           << endl;
      }
      mOutStream << "    "
         << idx->first.ReplaceChars('.','_');
      lFirst = false;
    }
    mOutStream << ";"
           << endl
           << endl;

    lSignals->clear();
    delete lSignals;
    
    {
      // Output of initialization.
      mOutStream << "  initially ";
      // At first the initial states.

      cerr << "Runtime error: HyTech output (for initial states) " << endl
       << "not implemented in this version of Rabbit." << endl;

      //ctaConfigState* lConfigState
      //           = new ctaConfigState( new ctaString( *a->second->GetName() ), 
      //                   new ctaString("") );
      //pModule->GetInitial()->collectStatesForAutomaton(pModule, lConfigState);
      // Now all the initial states of the current automaton (a->second)
      //   are inserted in the state set in (lConfigState).
      
      // db collect
      //if( lConfigState->GetStates()->size() != 1 )
      //{
      //CTAERR << "We must have exactly one initial state for HyTech." << endl;
      //CTAPUT;
      //}

      
      // We have to output the initial restrictions but it's 
      //   unclear how to translate.
      //mOutStream << endl << "-- Attention: Maybe there are "
      //           << "some initial variable restrictions." << endl;
      
      // Output with replaced dots with underscores in state names.
      //mOutStream << (*lConfigState->GetState()).ReplaceChars('.','_');
      //mOutStream << ";"
      //     << endl
      //     << endl;
      //delete lConfigState;
    }

    
    // output of states/locations
    ctaForAll_Map(a->second->GetStates(), lStateIt, ctaState*)
    {
      this->PrintStateToHyTech(lStateIt->second);
    }
    mOutStream << "end"
           << endl;
  } // End of automata.

  //Set the old error msg prefix.
  delete pubErrMsg->GetPrefix();
  pubErrMsg->SetPrefix(lOldPrefix);
}

void ctaHyTechPrinter::PrintStateToHyTech(const ctaState* pState) const
{
  // Output of location with replacing dots with underscores.
  mOutStream << "  loc "
         << pState->GetName()->ReplaceChars('.','_')
         << ":"
         << endl;

  // Invariant.
  if(pState->GetInvariant() != NULL)
  {
    this->PrintInvariantToHyTech(pState->GetInvariant());
  }
  else
  {
    mOutStream << "    while"
           << endl
           << "      True"
           << endl;
  }
  // Derivation.
  if(pState->GetDerivation() != NULL)
  {
    this->PrintDerivationToHyTech(pState->GetDerivation());
  }
  else
  {
    mOutStream << "    wait {}"
           << endl;
  }
  // Transitions.
  ctaForAll_SetPointer(pState->GetTransitions(), lTransIt, ctaTransition*)
  {
    this->PrintTransitionToHyTech(*lTransIt);
  }
}

void ctaHyTechPrinter::PrintDerivationToHyTech(const ctaRestriction* pDeriv) const
{
  mOutStream << "    wait"
         << endl
         << "    {"
         << endl;
  // Print only the (ctaRestriction) if not equal to 
  //   (ctaRestTrue) or (ctaRestFalse).
  const ctaRestTrue*  ltest1 = dynamic_cast<const ctaRestTrue *> (pDeriv);
  const ctaRestFalse* ltest2 = dynamic_cast<const ctaRestFalse*> (pDeriv);
  if (ltest1)
  {
    mOutStream << "-- This was a true (ctaRestTrue)!" << endl;      
  }
  if (ltest2)
  {
    mOutStream << "-- This was a False (ctaRestFalse)!" << endl;
  }
  if ( (!ltest1) && (!ltest2) )
  {
    // Use the special method for output because of the "d"
    // to sign a derivative of a variable in HyTech.
    pDeriv->PrintHyTech(mOutStream);
  }
  mOutStream << endl
         << "    }"
         << endl;
}

void ctaHyTechPrinter::PrintInvariantToHyTech(const ctaRestriction* pInvariant) const
{
  mOutStream << "    while"
         << endl
         << "      ";
  pInvariant->PrintHyTech(mOutStream);
  mOutStream << endl;
}

void ctaHyTechPrinter::PrintTransitionToHyTech(const ctaTransition* pTrans) const
{
  //Save the old error prefix und set the new one.
  const ctaString *lOldPrefix = pubErrMsg->GetPrefix();
  pubErrMsg->SetPrefix( new ctaString("PrintHyTech error: (in ctaHyTechPrinter-ctaTransition: ") );

  mOutStream << "    when"
         << endl;
  
  // Guard.
  // If the guard is (syntactical) empty then it is true.
  if(pTrans->GetGuard() != NULL)
  {
    //Is a operation '!=' in the guard? Write a Message to replace in the HyTech output,
    //because operation '!=' is not allowd in the hytech guards.
    pTrans->GetGuard()->PrintHyTech(mOutStream);
  }
  else
  {
    mOutStream << "      True"
           << endl;
  }
  
  // Synchronisation.
  if( *(pTrans->GetSync()->GetSignalName()) != "")
  {
    mOutStream << "      sync "
           << pTrans->GetSync()->GetSignalName()->ReplaceChars('.','_')
           << endl;
  }
  
  // Allow.
  if(pTrans->GetAllow() != NULL)
  {
    mOutStream << "      do"
           << endl
           << "      {"
           << endl;
    // No special handling because of same syntax in HyTech.
    // Print only the (ctaRestriction) if not equal to (ctaRestTrue) or (ctaRestFalse).
    const ctaRestTrue *ltest1 = dynamic_cast<const ctaRestTrue*> (pTrans->GetAllow());
    const ctaRestFalse *ltest2 = dynamic_cast<const ctaRestFalse*> (pTrans->GetAllow());
    if (ltest1)
    {
      mOutStream << "-- This was a True! " << endl;      
    }
    if(ltest2)
    {
      mOutStream << "-- This was a False! " << endl;
    }
    if ((!ltest1)&&(!ltest2))
    {
      // open new inoutput stream
      iostream* lIoStreamTmp = new strstream();
      char lTest;

      // Replace the '&' from a (ctaRestAnd) through ',' for a do-clausel.
      pTrans->GetAllow()->PrintHyTech((ostream &)*lIoStreamTmp);
      lIoStreamTmp->flush();
      while((lTest=lIoStreamTmp->get()) != EOF)
      {
    if (lTest == '&')
    {
      mOutStream << ',';
    }
    else
    {
      mOutStream << lTest;    
    }
      }
      delete lIoStreamTmp;
    }
    mOutStream << endl 
           <<"      }"
           << endl;
  }
  // Following State.
  mOutStream << "      goto "
         << (*pTrans->GetState()).ReplaceChars('.','_')
         << ";"
         << endl
         << endl;

  //Set the old error msg prefix.
  delete pubErrMsg->GetPrefix();
  pubErrMsg->SetPrefix(lOldPrefix);
}

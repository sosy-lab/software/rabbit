//-*-Mode: C++;-*-

/*
 * File:        ctaRestRel.h
 * Purpose:     Relation-Restriction class for cta-library
 * Author:      H. Rust
 * Created:     1998
 * Copyright:   (c) 1998, University of Brandenburg, Cottbus
 */

#ifndef _ctaRestRel_h
#define _ctaRestRel_h


#include "ctaExpression.h"
class ctaModule;
#include "ctaRelOp.h"
#include "ctaRestriction.h"

#include "ddmRegion.h"
#include "ddmVec.h"
#include "bddConfig.h"

class ctaRestRel : public ctaRestriction
{
private:
  ctaExpression *mE1;
  ctaExpression *mE2;
  ctaRelOp *mRelOp;

  // It should be not allowed to use the standard operators.
  ctaRestRel(const ctaRestRel&);
  void operator=(const ctaRestRel&);
  void operator,(const ctaRestRel&);

public:
  // constructor and destructor
  ctaRestRel(ctaExpression *pE1,
	     ctaRelOp *pRel,
	     ctaExpression *pE2)
    : mE1(pE1),
      mE2(pE2),
      mRelOp(pRel)
  {}

  ~ctaRestRel()
  {
    delete mE1;
    delete mRelOp;
    delete mE2;
  }
  
  // accessor methods
  // Service methods.
  // aheinig 15.03.2000
  // false: if used >, <, != by clocks
  // true : otherwise
  virtual bool
  CheckNonStrictness(const ctaModule* pCtaModule) const
  { 
    if( (mE1->CountClocks(pCtaModule)) + 
	(mE2->CountClocks(pCtaModule)) == 0)
    {
      return true;
    }
    else
    {
      return ( (mRelOp->GetOp() == ctaRelOp::EQUAL)  || 
	       (mRelOp->GetOp() == ctaRelOp::LESSTHAN) || 
	       (mRelOp->GetOp() == ctaRelOp::GREATERTHAN) );
    }
  }
  
  
 
  // aheinig 22.03.2000
  // true:  Only 1 Clock, only 1 Clock on every side of ctaRelOp, 
  //        CheckBddConstraint true on every side of ctaRelOp. 
  //        If we have a clock, we don't have any other variable.
  // false: otherwise 
  virtual bool
  CheckBddConstraint(const ctaModule* pCtaModule, 
		     const bool logicalOrIsAllowed) const
  {
    if( mE1->CheckBddConstraint() && 
	mE2->CheckBddConstraint() )
    {
      if( (mE1->CountVariables(pCtaModule) <= 1) && 
          (mE2->CountVariables(pCtaModule) <= 1) )
      {
	if( mE1->CountClocks(pCtaModule) + 
	    mE2->CountClocks(pCtaModule) == 0)
	{
	  return true;
	} 
	else
	{  
	  if( (mE1->CountClocks(pCtaModule) + 
	       mE2->CountClocks(pCtaModule) <= 1) &&
	      (mE1->CountVariables(pCtaModule) + 
	       mE2->CountVariables(pCtaModule) == 0) )
	  {
	    return true;
	  }
	}
      }
    }
    return false;
  }

  // Clone (this).
  ctaRestriction*
  cloneSetPrefixAndIdent(const ctaString *const pPrefix,
			 ctaMap<ctaString*>* pIdentNames) const
  {
    // Clone the left operand.
    ctaExpression* leftOperand = mE1->cloneSetPrefixAndIdent(pPrefix, 
							     pIdentNames);
    // Clone the right operand.
    ctaExpression* rightOperand = mE2->cloneSetPrefixAndIdent(pPrefix, 
							      pIdentNames);

    // Clone the operation.
    ctaRelOp* clonedOp = mRelOp->clone();

    // Create the clone.
    ctaRestriction* theClone = 
      new ctaRestRel(leftOperand, clonedOp, rightOperand);

    return theClone;
  }

  // Check the derivation for correctness.
  void
  CheckDerivation(const ctaModule* const pModule) const 
  {
    this->mE1->CheckDerivation(pModule);
    this->mE2->CheckDerivation(pModule);
  }

  // This method is for context analysis.
  // We give the parent module for accessing module features.
  virtual void 
  CheckConsistency(const ctaModule* const pModule)
  {
    mE1->CheckConsistency(pModule);
    mE2->CheckConsistency(pModule);
  }

  virtual bool
  VariableOccurs(ctaString *pId)
  {
    return 
      mE1->VariableOccurs(pId)
      || mE2->VariableOccurs(pId);
  }

  virtual bool
  VariableOccursTicked(ctaString *pId)
  {
    return 
      mE1->VariableOccursTicked(pId)
      || mE2->VariableOccursTicked(pId);
  }

  // aheinig 2000-11-02
  // Collect all used signals and variables for this automaton.
  void 
  collectInterfacesForCommunicationGraph(ctaSet<ctaString>* lUsedInterfaces)
  {
    mE1->collectInterfacesForCommunicationGraph(lUsedInterfaces);
    mE2->collectInterfacesForCommunicationGraph(lUsedInterfaces);
  }

  virtual void 
  Print(ostream& s) const
  {
    s << mE1 << " " << mRelOp << " " << mE2;
  }

  virtual void 
  PrintHyTech(ostream& pOutStream) const
  {
    //Save the old error prefix und set the new one.
    const ctaString *lOldPrefix = pubErrMsg->GetPrefix();
    pubErrMsg->SetPrefix( new ctaString("PrintHyTech error: (in ctaRestRel: ") );

    if ( mRelOp->GetOp()== ctaRelOp::NOTEQUAL )
    {
      pOutStream << endl << "-- HyTech can't handle '!=' operation,"
		 << " replace manually." << endl;
      CTAERR << "HyTech can't handle '!=' operation." << endl;
      CTAPUT;
    }
    mE1->PrintHyTech(pOutStream);
    pOutStream << " " << mRelOp << " ";
    mE2->PrintHyTech(pOutStream);

    //Set the old error msg prefix.
    delete pubErrMsg->GetPrefix();
    pubErrMsg->SetPrefix(lOldPrefix);
  }

  virtual ddmRegion
  mkDdmRegion(const reprAutomaton& pAuto,
	      bool pDoubleSize,
	      const ctaModule* pCtaModule);

  virtual bddConfig
  mkBddConfig(const bddSymTab* pSymTab,
	      const ctaModule* pCtaModule);
};
#endif

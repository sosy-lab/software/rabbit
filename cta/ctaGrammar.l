/*
 * File:        ctaGrammar.l
 * Purpose:     Grammar Extracter for cta-library
 * Author:      Dirk Beyer
 * Created:     1998
 * Copyright:   (c) 1998, University of Brandenburg, Cottbus
 */

%{

/* Included code before lex code */
/*************** Includes and Defines *****************************/

#include <fstream.h>

int start = 0;
int trash = 0;
%}

form_feed                       [\014]
v_tab                           [\013]
c_return                        [\015]

%%


\%\%        {
				start = 1;
			};

\'\{\'		{
				if(start)
				{
					cout << yytext;
				}
			}

\'\}\'		{
				if(start)
				{
					cout << yytext;
				}
			}

\{        	{
				trash = 1;
			};

\}        	{
				trash = 0;
			};

(\n|\010|{v_tab}|{c_return}|{form_feed})+                       {
                                if(start && !trash)
                                {
                                        cout << endl;
                                }
                        }

.			{
				if(start && !trash)
				{
					cout << yytext;
				}
			}

%%

/*******************************************************************/
int yywrap()		// if we need some other files
{ 
	return(1);
}

main()
{
	yylex();
}

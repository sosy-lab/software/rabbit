//-*-Mode: C++;-*-

/*
 * File:        utilTime.cpp
 * Purpose:     
 * Author:      Andy Heinig
 * Created:     2001
 * Copyright:   (c) 2001, Brandenburg Technical University of Cottbus
 */ 

#include "utilTime.h"

void
utilTime::printPastTime()
{
  long lEndTime;
  time(&lEndTime);
  if( ((((lEndTime - mStartTime) / 60) / 60)) <= 9 )
  {
    cout << "0";
  }
  cout << ((((lEndTime - mStartTime) / 60) / 60)) << ":";
  if( (((lEndTime - mStartTime) / 60) % 60) <= 9 )
  {
    cout << "0";
  }
  cout << (((lEndTime - mStartTime) / 60) % 60) << ":";
  if( ((lEndTime - mStartTime) % 60) <= 9 )
  {
    cout << "0";
  }
  cout << ((lEndTime - mStartTime) % 60) << " (hrs:min:sec)";        
}




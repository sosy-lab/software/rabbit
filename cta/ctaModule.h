//-*-Mode: C++;-*-

/*
 * File:        ctaModule.h
 * Purpose:     Module Class for cta-library
 * Author:      Dirk Beyer
 * Created:     1998
 * Copyright:   (c) 1998, University of Brandenburg, Cottbus
 */

#ifndef _ctaModule_h
#define _ctaModule_h

// Avoid template instantiations.

#include "ctaString.h"
#include <vector>
#include <set>

class ctaAutomaton;
class ctaComponent;
class ctaConfiguration;
class ctaInstantiation;
#include "ctaMap.h"
class ctaModule;
#include "ctaObject.h"
#include "ctaSet.h"
#include "ctaString.h"

class ctaSystem;

class ddmAutomaton;
class bddAutomaton;
class reprAutomaton;
class bddSymTab;
class bddCommunicationGraph;

class ctaModule : private ctaObject
{
public:
  typedef enum {INSTANTIATION, AUTOMATON, INTERFACE} ParsingTag;
  typedef enum {mDDM, mBDD, mUndef, mNOREPR} ReprType;

private:
  // All aggregated.
  ctaString* 		      	mName;
  ctaMap<ctaAutomaton*>*       	mAutomata;
  ctaConfiguration* 	        mInitial;
  ctaMap<ctaComponent*>* 	mInterface;
  ctaMap<ctaInstantiation*>*	mInstantiations;

  // Aggregated. Build by parser.
  // The original order of the parts of the module.
  //   A pair (x, y) means: x is the type of the component,
  //   i. e. INSTANTIATION for instantiation,
  //         AUTOMATON for an automaton,
  //         INTERFACE for variables, constants and signals,
  //   and y is the string for the name of the component.
  vector<pair<ParsingTag, ctaString*> >* mParsingOrder;

  // It should be not allowed to use the standard operators.
  ctaModule(const ctaModule&);
  void operator=(const ctaModule&);
  void operator,(const ctaModule&);


public:
  // constructor and destructor
  ctaModule();
  ~ctaModule();
  
  // Accessor methods.
  // Only called directly after construction of this module.
  void SetName(ctaString* s)
  {
    if(mName)
    {
      delete mName;
    }
    mName = s;
  }

  ctaString* 
  GetName() const
  {
    return mName;
  }

  vector<pair< ParsingTag, ctaString*> >*
  GetParsingOrder() const
  {
    return mParsingOrder;
  }

  ctaMap<ctaComponent*>* 
  GetInterface() const
  {
    return mInterface;
  }

  ctaConfiguration*
  GetInitial() const
  {
    return mInitial;
  }

  ctaMap<ctaAutomaton*>* 
  GetAutomata() const
  {
    return mAutomata;
  }

  ctaMap<ctaInstantiation*>* 
  GetInstantiations() const
  {
    return mInstantiations;
  }

  void 
  addInitial(ctaConfiguration* pInitial);

  void 
  addAutomaton(ctaString* pString, ctaAutomaton* pAutomaton)
  {
    mAutomata->append(pString, pAutomaton);
  }

  void 
  addInterface(ctaString* pString, ctaComponent* pInterface)
  {
    mInterface->append(pString, pInterface);
  }


  // Service methods.

  // To call after flatten, but before mkDdm.
  void 
  mkDdmCompletions();

  void 
  mkInputErrorStateCompletions();

  // This method is for context analysis.
  // We give the parent module for accessing module features.
  void 
  CheckConsistency(ctaSystem* pSystem);

  // This method is used by CheckConsistency() with argument false to check
  // only this module for unused signals and by
  // anaSecRefinement:CheckConsistency() with argument true to check
  // recursive all instanciated submodules of this module for unused
  // signals.
  bool 
  CheckForUnusedSignals(bool recursive);

  bool 
  IsObserver();

  bool 
  CheckOnlyBddVariablesUsed() const;
  
  bool 
  CheckNonStrictness() const;
  
  bool 
  CheckBddConstraint() const;
  // This call the checks for bdd restrictions above.
  bool 
  CheckBddRestrictions() const;

  ctaModule*
  FlattenModule() const;

  void 
  Flatten(const ctaString *const pPrefix,
	  ctaMap<ctaString*>* pIdentNames,
	  ctaModule* pModule,
	  const ctaSystem *const pSystem) const;

  // Collect IDs of variables and syncs.
  void 
  mkSymtab(pair<set<ctaString>, set<ctaString> >& pSymtab) const;

  // Construct a DDM-automaton for (this), using the given
  //   symbol table.  First component has list of sync IDs, 
  //   second of variable IDs.
  ddmAutomaton*
  mkDdmAutomaton() const;

  // Construct a symbol table. 
  // If (pSymTab) then
  //   1) (pSymTab) must contain (at least) all variables and automata of (this),
  //   2) the (bddVarInfo-)positions of the automata and variables
  //      in the returned bddSymTab are the same as in (pSymTab).
  bddSymTab*
  mkBddSymTab(const bddSymTab* pSymTab, 
	      vector<pair<ParsingTag, ctaString*> >* lParsingOrder) const;

  // Construct a bddAutomaton for (this).
  // For (pSymTab) see (mkBddSymTab).
  bddAutomaton*
  mkBddAutomaton(const bddSymTab* pSymTab = NULL) const;

  void 
  calculateVariableOrdering(vector<pair<ParsingTag, ctaString*> >* 
			    pParsingOrder) const;

  
  // Checks whether modules within this module
  //   fulfil a refinement relation.
  //   We use weak simulation after [Weise&Lenzkes1997].
  bool
  CheckSimulationBDD();

  void
  initBDD() const;

  void 
  calculateNewVariableOrdering(vector<pair<ParsingTag, ctaString*> >* 
			       pParsingOrder,
			       bool pIsPrintBddEstim,
			       ostream& pS) const;

};

ostream& operator << (ostream &, const ctaModule*);

#endif

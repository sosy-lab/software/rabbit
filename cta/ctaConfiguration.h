//-*-Mode: C++;-*-

/*
 * File:        ctaConfiguration.h
 * Purpose:     Configuration Class for cta-library
 * Author:      Dirk Beyer
 * Created:     1998
 * Copyright:   (c) 1998, University of Brandenburg, Cottbus
 */

#ifndef _ctaConfiguration_h
#define _ctaConfiguration_h

class ctaConfigState;
class ctaModule;
#include "ctaObject.h"
#include "ctaRestriction.h"
#include "ctaString.h"

class reprAutomaton;
class ddmConfig;

class bddConfig;

class ctaConfiguration : private ctaObject
{
private:
  // It should be not allowed to use the standard operators.
  ctaConfiguration(const ctaConfiguration&);
  void operator=(const ctaConfiguration&);
  void operator,(const ctaConfiguration&);
	
public:
  //constructor
  ctaConfiguration()
  {}

  // Service methods.
  // This method is for context analysis.
  // We give the parent module for accessing module features.

  virtual ctaConfiguration* 
  cloneSetPrefixAndIdent(const ctaString *const pPrefix,
			 ctaMap<ctaString*>* pIdentNames) const = 0;

  virtual bool CheckNonStrictness(const ctaModule* pCtaModule) const = 0;

  virtual bool CheckBddConstraint(const ctaModule* pCtaModule) const = 0;

  virtual void CheckConsistency(const ctaModule*) = 0;

  virtual void Print(ostream& s) const = 0;

  virtual ddmConfig
  mkDdmConfig(const reprAutomaton& pAuto,
	      const ctaModule* pCtaModule) const = 0;

  virtual bddConfig
  mkBddConfig(const bddSymTab* pSymTab,
	      const ctaModule* pCtaModule) const = 0;

  virtual ctaConfiguration*
  collectStatesForAutomaton(const ctaModule* pCtaModule, 
			    const ctaString* pAutoName) const = 0;

  bool
  IsTrue() const;
  
  bool
  IsFalse() const;
  
  static ctaConfiguration*
  mkAnd(ctaConfiguration* p1, ctaConfiguration* p2);

  static ctaConfiguration*
  mkOr(ctaConfiguration* p1, ctaConfiguration* p2);
};

inline ostream&
operator << (ostream& s, const ctaConfiguration* y)
{
  y->Print(s);

  return s;
}

#endif

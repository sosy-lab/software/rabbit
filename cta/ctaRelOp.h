//-*-Mode: C++;-*-

/*
 * File:        ctaRelOp.h
 * Purpose:     Relation Operator Class for cta-library
 * Author:      Dirk Beyer
 * Created:     1998
 * Copyright:   (c) 1998, University of Brandenburg, Cottbus
 */

#ifndef _ctaRelOp_h
#define _ctaRelOp_h


//---- ctaRelOp -----------------------------------------------------------

#include "ctaObject.h"
#include "ctaString.h"

class ctaRelOp : public ctaObject
{
public:
  typedef enum {EQUAL, LESS, GREATER, NOTEQUAL, 
                LESSTHAN, GREATERTHAN} OPERATIONTYPE;

private:
  // Contains "=", "<", ">", "!=", "<=", ">=".
  OPERATIONTYPE mOp;

  // It should be not allowed to use the standard operators.
  ctaRelOp();
  ctaRelOp(const ctaRelOp&);
  void operator=(const ctaRelOp&);
  void operator,(const ctaRelOp&);
	
public:
  // constructor and destructor

  ctaRelOp(OPERATIONTYPE lOp)
    : mOp(lOp)
  {}

  ~ctaRelOp()
  {}
  
  OPERATIONTYPE 
  GetOp() const
  {
    return mOp;
  }

  // Clone (this).  
  ctaRelOp*
  clone() const
  {
    // Create the clone.
    ctaRelOp* theClone = new ctaRelOp(mOp);

    return theClone;
  }

  void 
  Print(ostream& s) const
  {
    switch(mOp)
    {
    case ctaRelOp::EQUAL:
      s << "=";
      break;
    case ctaRelOp::LESS:
      s << "<";
      break;
    case ctaRelOp::GREATER:
      s << ">";
      break;
    case ctaRelOp::NOTEQUAL:
      s << "!=";
      break;
    case ctaRelOp::LESSTHAN:
      s << "<=";
      break;
    case ctaRelOp::GREATERTHAN:
      s << ">=";
      break;
    }
  }
};

inline ostream&
operator << (ostream& out, const ctaRelOp* obj)
{
  obj->Print(out);
  return(out);
}

#endif

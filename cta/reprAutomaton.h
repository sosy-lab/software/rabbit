#ifndef _reprAutomaton_h_
#define _reprAutomaton_h_

#include "reprConfig.h"
#include "reprAutomatonOstream.h"

#include "ctaMap.h"
#include "ctaComponent.h"

// We need this for the down casts.
class ddmAutomaton;
class bddAutomaton;

class reprAutomaton : private reprObject
{
private:
  // It should be not allowed to use the standard operators.
  void operator=(const reprAutomaton&);
  void operator,(const reprAutomaton&);

public: // Public static methods.

  // Reachabletype tags.
  typedef enum {mFORWARD, mBACKWARD} reprAutomaton_ReachDir;

  // Used by analysis interpreter.
  //   (pSteps) is the number of maximal steps to use.
  //   It is decremented in each cycle.  If a fixpoint is reached,
  //   the return value is >= 0; if no fixpoint is reached, the 
  //   return value is -1.
  // The caller has to control memory of result.
  virtual reprConfig*
  mkReachable(reprAutomaton_ReachDir pTag,
	      const reprConfig* const pConfig,
	      int& pSteps) const = 0;

  // Computes, if it is possible to reach (pError) from (pInitial).
  virtual bool
  isReachable(const reprConfig* const pInitial, 
	      const reprConfig* const pError) const = 0;
  
  virtual void
  simulateCtaModel(ctaMap<ctaComponent*>* pComp) const = 0;

  virtual reprAutomatonOstream
  mkAutomatonOstream(ostream& s) const = 0;

public: // Service methods.
  
  // Check if this automaton allows to sync on the foreign set (pForeign)
  //   if the own transition carries the label set (pOwn).
  // The result is true if the intersection of (pForeign) and the alphabet
  // of (this) is a subset of (pOwn).
  virtual bool
  allowsForeignSync(const int pForeign, const int pOwn) const = 0;
  
  // Check if (*this) must sync on the given label.
  virtual bool
  mustSync(const int pSync) const = 0;
  
public: // Accessors.

  virtual const reprConfig&  
  getInitial() const = 0;
  
  virtual reprConfig*
  mkEmptyConfig() const = 0; 

  // To get direct references to the subclass object.

  virtual ddmAutomaton*
  getDerivedObjDdm() = 0;

  virtual const ddmAutomaton*
  getDerivedObjDdm() const = 0;

  virtual bddAutomaton*
  getDerivedObjBdd() = 0;

  virtual const bddAutomaton*
  getDerivedObjBdd() const = 0;

public: // IO.
  
  virtual void print(ostream& s) const = 0;
  virtual void print(ostream& s, const reprConfig* const pConfig) const = 0;

  friend ostream&
  operator<<(ostream& s, const reprAutomaton& y);
};

inline ostream&
operator<<(ostream& s, const reprAutomaton& y)
{
  y.print(s);
  return s;
}


// Local Variables:
// mode:C++
// End:

#endif // _reprAutomaton_h_

//-*-Mode: C++;-*-

/*
 * File:        ctaVectorOfPointer.h
 * Purpose:     Set Class for cta-library
 * Author:      Andy Heinig
 * Created:     2000
 * Copyright:   (c) 2000, University of Brandenburg, Cottbus
 */

#ifndef _ctaVectorOfPointer_h
#define _ctaVectorOfPointer_h


// for new sdt-lib
#include <iterator>
#include <vector>

#include "ctaObject.h"

//---- ctaVectorOfPointer----------------------------------------------------

// This is a vector for references.

template<class E> class ctaVectorOfPointer : public vector<E>, 
					      private ctaObject
{
private:
  // It should be not allowed to use the standard operators.
  ctaVectorOfPointer(const ctaVectorOfPointer&);
  void operator=(const ctaVectorOfPointer&);
  void operator,(const ctaVectorOfPointer&);
	
public:
  // constructor
  ctaVectorOfPointer()
  {}

  // Traversion for elements idx of vector S
#define ctaForAll_VectorOfPointer(S, idx, TYPE) for(ctaVectorOfPointer<TYPE>::iterator idx = (S)->begin(); idx != (S)->end(); ++idx )
  
  ~ctaVectorOfPointer()
  {
    // It works with an empty vector.
    ctaForAll_VectorOfPointer(this, idx, E)
    {
      // We delete the idx-th element in set.
      delete *idx; 
    }    
  }
  
  void append(const E item)
  {
    this->push_back(item);
  }

  void join(ctaVectorOfPointer<E>* l)
  {
    // Set l inserting as a range.
    ctaForAll_VectorOfPointer(l, it, E)
    {
      this->push_back(*it);
    }
  }
};

template<class E> ostream& operator << (ostream& out, 
					ctaVectorOfPointer<E>* obj)
{
  // It works with an empty vector.
  ctaForAll_VectorOfPointer(obj, idx, E)
  {
    // We print the idx-th element in vector.
    out << *idx; 
  }
  return out;
}

#endif

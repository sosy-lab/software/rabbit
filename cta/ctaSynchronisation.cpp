/*
 * File:        ctaSynchronisation.cpp
 * Purpose:     Synchronisation Class for cta-library
 * Author:      Dirk Beyer
 * Created:     1998
 * Copyright:   (c) 1998, University of Brandenburg, Cottbus
 */

#include "ctaSynchronisation.h"

#include "ctaErrMsg.h"
#include "ctaModule.h"
	

ctaSynchronisation::ctaSynchronisation()
  : signalName(new ctaString("")),
    signalType(new ctaString(""))
{}

ctaSynchronisation::ctaSynchronisation(ctaString* pSignalName, 
				       ctaString* pSignalType)
  : signalName(pSignalName),
    signalType(pSignalType)
{}

ctaSynchronisation::~ctaSynchronisation()
{
  delete signalName;
  delete signalType;
}

bool
ctaSynchronisation::CheckSignalName(const ctaMap<ctaComponent*>* interface,
				    const ctaString* identName) const
{
  // here we have a compromise: we use the find operation
  // two times because we don't like to used an temporary iterator

  // we have to check whether used signals are declared
  if( interface -> find(*identName)      // try to find declaration
      == interface->end() )          // it's the result if it isn't found
  {
    CTAERR << "signal \'"
	   << identName << "\' undeclared. "
	   << endl;
    CTAPUT;

    return false;
  }
  else
  {
    // we have to check whether used components are signals
    ctaComponent* comp;
    comp = interface -> find(*identName) -> second;  // find declaration
    // find out the datatype
    if( comp -> GetDataType() != ctaComponent::SYNC)
    {
      CTAERR << "Wrong identifier \'"
	     << identName << "\' used as signal." 
	     << endl;
      CTAPUT;

      return false;
    }
  }

  return true;
}

void ctaSynchronisation::CheckSignalType(const ctaComponent* comp,
					 const ctaString* identType) const
{
  // we have to check whether the restriction type is fulfilled

  // If we have LOCAL or OUTPUT then we have not any restriction.

  // If we have INPUT then we need use with "?".
  if( (comp->GetRestrictionType() == ctaComponent::INPUT)
      && ( *identType != "?" ) )
  {
    CTAERR << "Wrong use of input signal \'"
	   << comp->GetName() << "\'."
	   << endl;     
    CTAPUT;
  }

  // If we have MULTREST then we need use as "#" or "?".
  if( (comp->GetRestrictionType() == ctaComponent::MULTREST)
      && ( *identType != "#" )
      && ( *identType != "?" ) )
  {
    CTAERR << "wrong use of multrest signal \'"

	   << comp->GetName() << "\'."
	   << endl;
    CTAPUT;
  }
}

void 
ctaSynchronisation::CheckConsistency(ctaModule* m)
{
  if(*signalName != "")
  {
    // for signalName: 
    if(CheckSignalName( m->GetInterface(),          // get components
			GetSignalName()))             // identifier to check
    {
      ctaComponent* comp;
      // find declaration
      comp = m -> GetInterface() -> find(*GetSignalName()) -> second;
      CheckSignalType(comp, GetSignalType());         // check type conditions
    }
  }
}

ostream& operator << (ostream& out, const ctaSynchronisation* obj)
{

  if( *( obj->GetSignalName() ) != "" )
  {
    out << "        SYNC ";
    out << obj->GetSignalType();
    out << obj->GetSignalName();

    out << ";" << endl;
  }
  return(out);  
};


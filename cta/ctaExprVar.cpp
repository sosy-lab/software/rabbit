//-*-Mode: C++;-*-

/*
 * File:        ctaExprVar.cpp
 * Purpose:     Variable Class for cta-library
 * Author:      H. Rust
 * Created:     1998
 * Copyright:   (c) 1998, University of Brandenburg, Cottbus
 */

#include "ctaExprVar.h"

#include "ctaComponent.h"
#include "ctaErrMsg.h"
#include "ctaMap.h"
#include "ctaModule.h"
#include "ctaString.h"

#include "ddmAutomaton.h"

// Check for correct derivation.
void
ctaExprVar::CheckDerivation(const ctaModule* const pModule) const
{
  if(this->mTag == mDER)
  {
    // No check if the identifier really exists in the interface
    //   of the module. This is done by CheckConsistency().
    const ctaComponent::DATATYPE ldatatype = pModule
    ->GetInterface()
    ->find(*(this->mIdentifier))
    ->second
    ->GetDataType();
    
    // Test the string.
    if ((ldatatype != ctaComponent::CONST) && (ldatatype != ctaComponent::DISCRETE) &&
	(ldatatype != ctaComponent::CLOCK))
    {
      // It is a correct derivation.
    }
    else
    {
      // It is an incorrect derivation.
      CTAERR << "Variable: " << mIdentifier << " is used in a derivation"
	     << endl
	     << " and is declared as CONST/DISCRETE/CLOCK in Module: " 
	     << pModule->GetName() << endl;
      CTAPUT;
    }
  }
  else if (this->mTag == mSTANDARD) 
  {
    // Test for const derivation.
    if(pModule
       ->GetInterface()
       ->find(*(this->mIdentifier))
       ->second
       ->GetDataType() == ctaComponent::CONST)
    {
      // All right, derivation is const.
    }
    else
    {
      // Error, derivation is not const.
      CTAERR << "Variable: " << mIdentifier << " is used in a derivation"
	     << endl
	     << " and not declared as CONST in Module: " 
	     << pModule->GetName() << endl;
      CTAPUT;
    }
  }
  else
  {
    // mTag is mTICKED and this is not allowed in a derivation.
    CTAERR << "Variable: " << mIdentifier << " is used in a derivation"
	   << endl
	   << " and is mTICKED in Module: " << pModule->GetName() << endl;
    CTAPUT;
  }
}

void 
ctaExprVar::CheckConsistency(const ctaModule* const pModule)
{
  // Check that (mIdentifier) is a variable in (pModule).
  bool lFound = false;

  ctaMap<ctaComponent*>::iterator 
    it = pModule->GetInterface()->find(*mIdentifier);

  lFound = (it != pModule->GetInterface()->end());

  if(!lFound)
  {
    CTAERR << "Variable \'" << mIdentifier << "\'"
	   << " not found in module " << pModule->GetName() << "\'"
	   << endl;
    CTAPUT;
  }
  else 
  {
    // Non-standard variables may not be constant.
    if(mTag != mSTANDARD 
       && ( it->second->GetDataType() == ctaComponent::CONST )
       )
    {
      CTAERR << "Ticked or DER'ed variable \'" << mIdentifier << "\'"
	     << " is declared CONST in module \'" << pModule->GetName()
	     << "\'"
	     << endl;
      CTAPUT;
    }
  }
}

// Construct a vector representing (*this) >= 0.
ddmVec
ctaExprVar::mkDdmVec(const reprAutomaton& pAuto,
		     bool pDoubleSize,
		     const ctaModule* pCtaModule)
{
  ddmVec result;

  const ddmAutomaton* lAuto = pAuto.getDerivedObjDdm();

  // Look up this variable in (pCtaModule).
  const ctaComponent* lTmpComp = pCtaModule->GetInterface()
                                            ->find(*(this->mIdentifier))
                                            ->second;

  // Check if this is a constant.
  if( lTmpComp->GetDataType() == ctaComponent::CONST )
  {
    if(lTmpComp->GetValue() == NULL)
    {
      cerr << "Runtime error: Value of constant '" << lTmpComp->GetName()
	   << "'" << endl
	   << " is undefined in module '" << pCtaModule->GetName() 
	   << "'!" << endl;
    }
    else
    {
      // Insert value as constant into result.
      result[1] = *lTmpComp->GetValue();
    }
  }
  else
  {
    // Look for index of variable name in (lAuto.getVarIds()).
    // We assume that the name is in (lAuto.getVarIds).
    int lIndex 
      = find(lAuto->getVarIds().begin(),
	     lAuto->getVarIds().end(),
	     *(this->mIdentifier))
      - lAuto->getVarIds().begin();
    
    // Add number of variables for ticked vars.
    // Remember that the vector may have double length.
    if(mTag == mTICKED)
    {
      lIndex += lAuto->getVarIds().size();
    }
    
    // Add number of extra vars needed by representation library
    // f. e. strictness (position 0) and constatnts (position 1).
    lIndex += ddmXPoly::mNumExtraVars;
    
    // Insert factor into Vector.
    result[lIndex] = this->GetFactor();
  }

  return result;
}

bddExpression*
ctaExprVar::mkBddExpression (const ctaModule* pCtaModule) 
{
  bddExpression* result;
  
  // Look up this variable in (pCtaModule).
  const ctaComponent* lTmpComp = pCtaModule->GetInterface()
                                           ->find(*(this->mIdentifier))
                                           ->second;

  // Check if this is a constant.
  if(lTmpComp->GetDataType() == ctaComponent::CONST)
  {
    if(lTmpComp->GetValue() == NULL)
    {
      cerr << "Runtime error: Value of constant '" << lTmpComp->GetName()
	   << "'" << endl
	   << " is undefined in module '" << pCtaModule->GetName() 
	   << "'!" << endl;
    }

    result = new bddExpression("", false, *lTmpComp->GetValue());
  }
  else
  {
    result = new bddExpression(*mIdentifier, mTag == mTICKED, 0);
  }

  return result;
}

// If (pHyTechPrinting) true then we print "d" instead of "DER(".
void 
ctaExprVar::PrintCtaOrHyTech(ostream& pOutStream, bool pHyTechPrinting) const
{
  if(mFactor == 0)
  {
    pOutStream << 0;
  }
  else
  {
    // Print sign or factor.
    if(mFactor == -1)
    {
      pOutStream << "-";
    }
    else if(mFactor != 1)
    {
      pOutStream << mFactor << "*";
    }

    // Output before identifier.
    switch(mTag)
    {
    case mSTANDARD:
      break;

    case mDER:
      if(pHyTechPrinting)
      {
	pOutStream << "d";
      }
      else
      {
	pOutStream << "DER(";
      }
      break;

    case mTICKED:
      break;
    }

    if (pHyTechPrinting)
    {
      pOutStream << mIdentifier->ReplaceChars('.','_');
    }
    else
    {
      pOutStream << mIdentifier;
    }
    
    // Output after identifier.
    switch(mTag)
    {
    case mSTANDARD:
      break;

    case mDER:
      if(pHyTechPrinting)
      {
	// Do nothing.
      }
      else
      {
	pOutStream << ")";
      }
      break;

    case mTICKED:
      pOutStream << "'";
      break;
    }
  }
}


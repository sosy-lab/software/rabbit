#ifndef _ctaIntSet_h_
#define _ctaIntSet_h_


#include "reprObject.h"
#include <set>

class ctaIntSet : public set<int>, private reprObject
{
private:
  // It should be not allowed to use the standard operators.
  // Uses the generated copy constructor. 
  // Uses the standard operator '='.
  void operator,(const ctaIntSet&);

public: // Public static methods.

  static ctaIntSet
  mkIntersection(const ctaIntSet& p1, const ctaIntSet& p2);

  static ctaIntSet
  mkDifference(const ctaIntSet& p1, const ctaIntSet& p2);

  static ctaIntSet
  mkUnion(const ctaIntSet& p1, const ctaIntSet& p2);

private: // Attributes.

public: // Constructors and destructor.
  ctaIntSet()
  {}

  ~ctaIntSet()
  {
    // This should not be necessary, but the destructor
    //   for nonempty sets seems to loop endlessly.
    this->erase(this->begin(), this->end());
  }    

public: // Accessors.


public: // Service methods.

public: // Friends.

public: // IO.
};

// Local Variables:
// mode:C++
// End:

#endif // _ctaIntSet_h_

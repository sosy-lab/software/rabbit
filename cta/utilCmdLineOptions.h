//-*-Mode: C++;-*-

/*
 * File:        utilCmdLineOptions.h
 * Purpose:     Class to handle and set the Cmd. line options for cta-library
 * Author:      Sebastian Schmerl
 * Created:     1999
 * Copyright:   (c) 1999, University of Brandenburg, Cottbus
 */

#ifndef _utilCmdLineOptions_h
#define _utilCmdLineOptions_h

#include <vector>

#include "anaObject.h"
#include "ctaString.h"
#include <fstream>

class utilCmdLineOptions : private ctaObject
{
 public:
  typedef enum {mUniteOnlySyncs, 
		mUniteAllTransitionsOfAnAutomaton,
		mUniteAllTransitions} TransitionProductType;

 private:
  // It should be not allowed to use the standard operators.
  utilCmdLineOptions(const utilCmdLineOptions&);
  void operator=(const utilCmdLineOptions&);
  void operator,(const utilCmdLineOptions&);

  // This ctaString includes the name of the cta file. 
  ctaString mCtaFileName;
  // In these (ctaString) is the modulename from the cmd. line. 
  ctaString mModuleToHandle;
  // The option below is responsible for printing out ALL
  //  the debugging information. The dafault values is 'false'.
  bool mOptFlgVerbose;
  // This variable is set, if printout in Hytech is ordered.
  bool mOptFlgHytechPrinting;
  // This (istream) indecates the CTA input file. 
  istream *mInputStream;
  // This (ostream) indecates the output file for HyTech printing. 
  ostream *mOutputStream;
  // This variable include the TransitionProductType.
  TransitionProductType mTransitionProductType;
  // This variable is set if the use random order of diskrete transition.
  bool mRandomDiskreteTransitionOrder;
  // This variable is set if we use transitive closure of diskrete transition.
  bool mTransitiveClosureOfDiskreteTransition;
  // This variable is set if we would count the max numbers of bddNodes. 
  bool mCountMaxNumbersOfBddNodes;

  bool mPrintBddSizeAfterComputeFixPnt;

  // This bool is true if we would order the variables for the bdd 
  //  representation. 
  bool mUseVariableOrdering;

  // This int is for the size of memory which is allocated for the BDDs.
  int  mMemorySizeForBdd;
  // 
  bool mRandomVariableOrder;
  //
  bool mUseVariableOrderingAfterRandom;

  // This methode analyze the cmd line options and set the flags.
  void AnalyzeOptions(int pargc, char ** pargv);

  // Prints the intro.
  void printIntro();

  // Prints the help text.
  void printHelp();

  // Prints the help text.
  void printShortHelp();

public:
  utilCmdLineOptions(int pargc, char **pargv)
    : mCtaFileName(""),
      mModuleToHandle(""),
      mOptFlgVerbose(false),
      mOptFlgHytechPrinting(false),
      mInputStream(NULL),
      mOutputStream(&cout),
      mTransitionProductType(mUniteOnlySyncs),
      mRandomDiskreteTransitionOrder(false),
      mTransitiveClosureOfDiskreteTransition(false),
      mCountMaxNumbersOfBddNodes(false),
      mPrintBddSizeAfterComputeFixPnt(false),
      mUseVariableOrdering(false),
      mMemorySizeForBdd(4),
      mRandomVariableOrder(false),
      mUseVariableOrderingAfterRandom(false)
  {
    AnalyzeOptions(pargc,pargv);
  }

  ~utilCmdLineOptions()
  {
    // If we were not been able to open the file we has left the method
    //   AnalyseOptions with exit.
    delete mInputStream;
  }

  bool Verbose()
  {
    return mOptFlgVerbose;
  } 

  ctaString GetModule()
  {
    return mModuleToHandle; 
  }

  ctaString* getCtaFileNameWithoutExtension()
  {
    // Search the point between the name and the extension.
    ctaString* lCtaFileName = new ctaString(mCtaFileName);
    unsigned int lItPoint = lCtaFileName->size();
    for(unsigned int it=0; it < lCtaFileName->size(); it++ )
    {
      if( (*lCtaFileName)[it] == '.' )
      {
	lItPoint = it;
      }
    }
    // Erase all chars after the last '.' in the file name. 
    lCtaFileName->erase(lItPoint, lCtaFileName->size());
    return lCtaFileName;
  }

  istream* InputStream()
  {
    return mInputStream;
  }

  ostream* GetOutputStream()
  {
    return mOutputStream;
  }

  bool HytechPrinting()
  {
    return mOptFlgHytechPrinting;
  }

  TransitionProductType getTransitionProductType()
  {
    return mTransitionProductType;
  }

  bool getRandomDiskreteTransitionOrder() 
  {
    return mRandomDiskreteTransitionOrder;
  }

  bool getTransitiveClosureOfDiskreteTransition()
  {
    return mTransitiveClosureOfDiskreteTransition;
  }

  bool getCountMaxNumbersOfBddNodes()
  {
    return mCountMaxNumbersOfBddNodes;
  }

  bool getPrintBddSizeAfterComputeFixPnt()
  {
    return mPrintBddSizeAfterComputeFixPnt;
  }

  bool getUseVariableOrdering()
  {
    return mUseVariableOrdering;
  }

  bool useRandomVariableOrder()
  {
    return mRandomVariableOrder;
  }

  bool useVariableOrderingAfterRandom()
  {
    return mUseVariableOrderingAfterRandom;
  }
  
  void
  initBDD() const;

  int
  GetObjCount()
  {
    return this->ctaObject::GetCount();
  }

};

extern utilCmdLineOptions* pubCmdLineOpt;

#endif

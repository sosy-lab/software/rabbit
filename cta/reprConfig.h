#ifndef _reprConfig_h_
#define _reprConfig_h_


#include <map>

#include "reprObject.h"
#include "ctaString.h"

// Needed for the casts.
class ddmConfig;
class bddConfig;
class reprAutomaton;

class reprConfig : private reprObject
{
private:
  // It should be not allowed to use the standard operators.
  // Uses the generated copy constructor. 
  // Uses the standard operator '='.
  void operator,(const reprConfig&);

public: // Service methods.

  // Because we are not able to use the copy constructor
  //   in a polymorph way, we need a clone method.
  virtual reprConfig*
  clone() const = 0;

  virtual void
  unite(const reprConfig& p) = 0;
  
  virtual bool
  setEqual(const reprConfig& p) const = 0;
  
  // Check if (*this) contains (p).
  virtual bool
  setContains(const reprConfig& p) const = 0;
  
  virtual bool
  isEmpty() const = 0;
  
  virtual void
  intersect(const reprConfig& p) = 0;

  virtual void
  setSubtract(const reprConfig& p) = 0;
  
  //Existential quantification of (pIdentifier) which is a
  //  variable or an automaton.
  //(pAutomaton) is for accessing the symbol table. 
  virtual void
  exists(const reprAutomaton* pAutomaton, const ctaString* pIdentifier) = 0;

  // It computes the complement of this configuration set.
  virtual reprConfig*
  mkComplement() const = 0;

  // aheinig.
  virtual void 
  printGraphicalBddVisualization(ostream& pS) const = 0;

  // To get direct references to the subclass object.
  virtual ddmConfig*
  getDerivedObjDdm() = 0;

  virtual const ddmConfig*
  getDerivedObjDdm() const = 0;

  virtual bddConfig*
  getDerivedObjBdd() = 0;

  virtual const bddConfig*
  getDerivedObjBdd() const = 0;
};

// Local Variables:
// mode:C++
// End:

#endif // _reprConfig_h_

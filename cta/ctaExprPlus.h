//-*-Mode: C++;-*-

/*
 * File:        ctaExprPlus.h
 * Purpose:     Plus-Expression Class for cta-library
 * Author:      H. Rust
 * Created:     1998
 * Copyright:   (c) 1998, University of Brandenburg, Cottbus
 */

#ifndef _ctaExprPlus_h
#define _ctaExprPlus_h


class ctaComponent;
#include "ctaExpression.h"
#include "ctaMap.h"
class ctaModule;

class ddmAutomaton;
#include "ddmVec.h"
class bddAutomaton;
#include "bddExpression.h"

class ctaExprPlus : public ctaExpression
{
private:

  ctaExpression *mE1;
  ctaExpression *mE2;

  // It should be not allowed to use the standard operators.
  ctaExprPlus(const ctaExprPlus&);
  void operator=(const ctaExprPlus&);
  void operator,(const ctaExprPlus&);

public:
  // constructor and destructor
  ctaExprPlus(ctaExpression *pE1,
	      ctaExpression *pE2)
    : mE1(pE1),
      mE2(pE2)
  {}

  ~ctaExprPlus()
  {
    if(mE1) 
    {
      delete mE1;
    }

    if(mE2) 
    {
      delete mE2;
    }
  }
  
  // accessor methods
  // Service methods.

  // Clone (this).
  ctaExpression*
  cloneSetPrefixAndIdent(const ctaString *const pPrefix,
			 ctaMap<ctaString*>* pIdentNames) const
  {
    // Clone left operand of "this".
    ctaExpression* cloneOp1 = mE1->cloneSetPrefixAndIdent(pPrefix,
							  pIdentNames);
    // Clone right operand of "this".
    ctaExpression* cloneOp2 = mE2->cloneSetPrefixAndIdent(pPrefix, 
							  pIdentNames);
    // Create the clone.
    ctaExpression* theClone = new ctaExprPlus(cloneOp1, cloneOp2);

    return theClone;
  }

  // Check the derivation for correctness. 
  void
  CheckDerivation(const ctaModule* const pModule) const
  {
    mE1->CheckDerivation(pModule);
    mE2->CheckDerivation(pModule);
  }

  // This method is for context analysis.
  // We give the parent module for accessing module features.
  void 
  CheckConsistency(const ctaModule* const pModule)
  {
    mE1->CheckConsistency(pModule);
    mE2->CheckConsistency(pModule);
  }

  bool
  VariableOccurs(ctaString *pId)
  {
    return 
      mE1->VariableOccurs(pId)
      || mE2->VariableOccurs(pId);
  }

  bool
  VariableOccursTicked(ctaString *pId)
  {
    return 
      mE1->VariableOccursTicked(pId)
      || mE2->VariableOccursTicked(pId);
  }

  // aheinig 22.03.2000
  // true: if mE1 and mE2 true
  // false:else 
  bool 
  CheckBddConstraint() const
  {
    if(mE1->CheckBddConstraint())
    {
      if(mE2->CheckBddConstraint())
      {
        return true;
      }
    }
    return false;
  }

  // aheinig 22.03.2000
  // count all used variable
  int 
  CountVariables(const ctaModule* pCtaModule) const
  { 
    return (mE1->CountVariables(pCtaModule) + mE2->CountVariables(pCtaModule));
  }

  // aheinig 22.03.2000
  // count all used clocks
  int 
  CountClocks(const ctaModule* pCtaModule) const
  { 
    return (mE1->CountClocks(pCtaModule) + mE2->CountClocks(pCtaModule));
  }

  void 
  Print(ostream& s) const
  {
    s << mE1 << "+" << mE2;
  }

  // aheinig 2000-11-02
  // Collect all used signals and variables for this automaton.
  void 
  collectInterfacesForCommunicationGraph(ctaSet<ctaString>* lUsedInterfaces)
  {
    mE1->collectInterfacesForCommunicationGraph(lUsedInterfaces);
    mE2->collectInterfacesForCommunicationGraph(lUsedInterfaces);
  }

  // Construct a vector representing (*this) >= 0.
  ddmVec
  mkDdmVec(const reprAutomaton& pAuto,
	   bool pDoubleSize,
	   const ctaModule* pCtaModule)
  {
    return mE1->mkDdmVec(pAuto, pDoubleSize, pCtaModule)
      + mE2->mkDdmVec(pAuto, pDoubleSize, pCtaModule);
  }

  bddExpression*
  mkBddExpression (const ctaModule* pCtaModule) 
  {
    bddExpression* lE1 = mE1->mkBddExpression(pCtaModule);
    bddExpression* lE2 = mE2->mkBddExpression(pCtaModule);
    bddExpression* result = new bddExpression(*lE1 + *lE2);
    
    if (lE1->getVar() != "" && lE2->getVar() != "") 
    {
      cerr << "Runtime error: Two Variables in Expression '(" 
	   << this << endl << ")'"
	   << "in module '" << pCtaModule->GetName() << "'!" << endl;
    } 

    delete lE1;
    delete lE2;

    return result;
  }
};
#endif

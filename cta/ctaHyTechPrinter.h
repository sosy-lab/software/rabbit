//-*-Mode: C++;-*-

/*
 * File:        ctaHyTechPrinter.h
 * Purpose:     HyTech printer for syntax trees in cta-library
 * Author:      Dirk Beyer & Sebastian Schmerl
 * Created:     1999
 * Copyright:   (c) 1999, University of Brandenburg, Cottbus
 */

#ifndef _ctaHyTechPrinter_h
#define _ctaHyTechPrinter_h


#include <iostream>

#include "ctaObject.h"
class ctaModule;
class ctaState;
class ctaRestriction;
class ctaTransition;

class ctaHyTechPrinter : public ctaObject
{
private:
  // Associated.
  ostream& mOutStream;

  // It should be not allowed to use the standard operators.
  ctaHyTechPrinter(const ctaHyTechPrinter&);
  void operator=(const ctaHyTechPrinter&);
  void operator,(const ctaHyTechPrinter&);

public:
  // Constructor.
  ctaHyTechPrinter(ostream& pOutStream)
    :mOutStream(pOutStream)
  {}
  
  // Destructor: Standard.
  void PrintModuleToHyTech(const ctaModule* pModule) const;
  void PrintStateToHyTech(const ctaState* pState) const;
  void PrintDerivationToHyTech(const ctaRestriction* pDeriv) const;
  void PrintInvariantToHyTech(const ctaRestriction* pInvariant) const;
  void PrintTransitionToHyTech(const ctaTransition* pTrans) const;
};

#endif

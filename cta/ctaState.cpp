/*
 * File:        ctaState.cpp
 * Purpose:     State Class for cta-library
 * Author:      Dirk Beyer
 * Created:     1998
 * Copyright:   (c) 1998, University of Brandenburg, Cottbus
 */

#include "ctaState.h"

#include "ctaExprVar.h"
#include "ctaRelOp.h"
#include "ctaExprConst.h"
#include "ctaRestRel.h"
#include "ctaRestAnd.h"
#include "ctaRestOr.h"
#include "ctaModule.h"

// aheinig 06.09.2000
// Clone (this). 
ctaState* 
ctaState::cloneSetPrefixAndIdent(const ctaString *const pPrefix,
                 ctaMap<ctaString*>* pIdentNames) const
{        
  // Create the cloned state name.
  ctaString* lStateName = new ctaString(*pPrefix + *mName);

  // Create the cloned invarinat.
  ctaRestriction* lInvClone = mInvariant->cloneSetPrefixAndIdent(pPrefix,
                                 pIdentNames);
  // Create the cloned sync.
  ctaRestriction* lDerClone = mDerivation->cloneSetPrefixAndIdent(pPrefix, 
                                  pIdentNames);

  ctaSetPointer<ctaTransition*>* lTransSetClone = 
                                           new ctaSetPointer<ctaTransition*>();
  
  ctaForAll_SetPointer(mTransitions, idx, ctaTransition*)
  { 
    ctaTransition* lTransClone = (*idx)->cloneSetPrefixAndIdent(pPrefix, 
                                                                pIdentNames);  
    lTransSetClone->insert(lTransClone);
  }

  ctaState* lStateClone = new ctaState(lStateName,
                                       lInvClone,
                                       lDerClone,
                                       lTransSetClone);
  
  return lStateClone;
} 

// aheinig 15.03.2000
// false: if used >, <, != by clocks
// true : else
bool 
ctaState::CheckNonStrictness(const ctaModule* pCtaModule) const
{
  if( mInvariant->CheckNonStrictness(pCtaModule) )
  {
    ctaForAll_SetPointer(mTransitions, idx, ctaTransition*)
    { 
      if( ! ((*idx)->CheckNonStrictness(pCtaModule)) )
      {
    return false;
      }
    }
    return true;
  }
  return false;
}

// aheinig 22.03.2000
// true : if CheckBddConstraint true for mGuard, mAllow and for all transition
// false: else
bool 
ctaState::CheckBddConstraint(const ctaModule* pCtaModule) const
{
  // If we use BDDs, we must not have derivations, and so the derivation 
  //   has to be true.
  if( !mDerivation->IsTrue() )
  {
    return false;
  }

  if( mInvariant->CheckBddConstraint(pCtaModule, false) )
  {
    ctaForAll_SetPointer(mTransitions, idx, ctaTransition*)
    { 
      if( ! ((*idx)->CheckBddConstraint(pCtaModule)) )
      {
	return false;
      }
    }
    return true;
  }
  return false;
}

void 
ctaState::CheckConsistency(ctaModule* m, ctaAutomaton* a)
{
  /* mvogel 2001-05-07 Check for state name INPUTERROR (task 146 todo.txt) */
  // We have to ensure that the state name is not INPUTERROR.
  // This name is internally used for the generated input error states.

  if(mName->find("INPUTERROR") != string::npos)
  {
    CTAERR << "It is not allowed to use the reserved state name INPUTERROR."
	   << endl;
    CTAPUT;
  }
  /* mvogel-2001-05-07 */

  // We have to check the invariant.
  if(mInvariant)
  {
    // Perform the context check for the ctaRestriction of the invariant.
    mInvariant->CheckConsistency(m);
  }

  // We have to check each derivation.
  if(this->mDerivation)
  {
    // Perform the context check for the ctaRestriction of the derivation.
    this->mDerivation->CheckConsistency(m);

    // Check of right use of derivation restriction
    //   of different data types.
    //   Only for ANALOG and STOPWATCH we allow
    //   restricted derivations, because it is very
    //   difficult to determine exactly if a restriction contains
    //   a type mismatch. For example, look at the following restriction,
    //   where x is declared as ANALOG and y is declared as STOPWATCH:
    //   DER(x) = 5 AND DER(y) = DER(x).
    //   Thus, we introduce the following restriction to our notation:
    //   CLOCK, DISCRETE and CONST identifiers are forbidden in a
    //   derivation.

    // Because the stuff above we have to check the emptiness
    //   of the region build by ctaRestriction::mkDdmRegion()
    //   for the derivation of the state. If the derivation
    //   contains a contradiction, than we output a runtime error.

    // In addition, we introduce the following restriction to our notation:
    //   CLOCK, DISCRETE and CONST identifiers are forbidden in a
    //   derivation (it means in an expression like DER(x) = c:
    //   c is allowed to be a constant but x must not be a constant).
    // This restriction is neccessary to forbid expressions like
    //   DER(x) = 0 OR DER(x) = 1 (if x is declared as CLOCK) ! 

    this->mDerivation->CheckDerivation(m);
  }

  // We have to check each transition.
  ctaForAll_SetPointer(GetTransitions(), idx, ctaTransition*)
  { // we take the idx-th element in set
    (*idx)->CheckConsistency(m, a); 
  }
}

// Make some completions for the ddm construction.
//   Precondition: To call after context check!
void 
ctaState::mkDdmCompletions(ctaModule* pModule)
{
  // Completion of the derivation restrictions
  //   for different data types.
  //   For example: DER(x) must be 0 if x is DISCRETE.
  // ANALOG:     no restriction necessary.
  // STOPWATCH:  DER(x) = 1 OR DER(x) = 0.
  // CLOCK:      DER(x) = 1.
  // DISCRETE:   DER(x) = 0.
  ctaForAll_Map(pModule->GetInterface(), it, ctaComponent*)
  {
    // Prepare some structure for further use:
    //   The variable we have to deal with.
    ctaExprVar* lVar = ctaExprVar::mkDer( new ctaString(it->first) );
    ctaRelOp* lRelOp = new ctaRelOp(ctaRelOp::EQUAL);
    
    // Fetch the datatype of the component.
    ctaComponent::DATATYPE lDataType = it->second->GetDataType();
    
    if( lDataType == ctaComponent::STOPWATCH )
    {
      // Build the restriction we have to add.
      ctaRestriction* lLeftRest = new ctaRestRel(lVar,
                         lRelOp,
                         new ctaExprConst(0));
      ctaRestriction* lRightRest = new ctaRestRel(lVar->clone(),
                          lRelOp->clone(),
                          new ctaExprConst(1));
      ctaRestriction* lOrRest = ctaRestriction::mkOr(lLeftRest, lRightRest);
      
      // Combine it with the origin restriction
      //   to build the new one.
      mDerivation = ctaRestriction::mkAnd(mDerivation, lOrRest);
    }
    else if( lDataType == ctaComponent::CLOCK)
    {
      // Build the restriction we have to add.
      ctaRestriction* lRest = new ctaRestRel(lVar,
                         lRelOp,
                         new ctaExprConst(1));
      
      // Combine it with the origin restriction
      //   to build the new one.
      mDerivation = ctaRestriction::mkAnd(mDerivation, lRest);
    }
    else if( lDataType == ctaComponent::DISCRETE)
    {
      // Build the restriction we have to add.
      ctaRestriction* lRest = new ctaRestRel(lVar,
                         lRelOp,
                         new ctaExprConst(0));
      
      // Combine it with the origin restriction
      //   to build the new one.
      mDerivation = ctaRestriction::mkAnd(mDerivation, lRest);
    }
    else
    {
      delete lVar;
      delete lRelOp;
    }
  }
}

void 
ctaState::collectInterfacesForCommunicationGraph(ctaSet<ctaString>* 
                                        lUsedSignals)
{
  mInvariant->collectInterfacesForCommunicationGraph(lUsedSignals);
  mDerivation->collectInterfacesForCommunicationGraph(lUsedSignals);
  ctaForAll_SetPointer(mTransitions, itTransitions, ctaTransition*)
  { 
    (*itTransitions)->collectInterfacesForCommunicationGraph(lUsedSignals);
  }
}

ostream& operator << (ostream& out, const ctaState* obj)
{
  out << "    STATE " << obj->GetName() << endl;
  out << "    {" << endl;

  if(obj->GetInvariant())
  {
    out << "      INV" << endl;
    out << "        " << obj->GetInvariant() << ";" << endl;
  }

  if(obj->GetDerivation())
  {
    out << "      DERIV" << endl;
    out << "        " << obj->GetDerivation() << ";" << endl;
  }

  ctaForAll_SetPointer(obj->GetTransitions(), idx, ctaTransition*)
  { 
    (*idx)->print(out);
  }

  out << "    }" << endl;
  return out;
};

//-*-Mode: C++;-*-

/*
 * File:        ctaState.h
 * Purpose:     State Class for cta-library
 * Author:      Dirk Beyer
 * Created:     1998
 * Copyright:   (c) 1998, University of Brandenburg, Cottbus
 */

#ifndef _ctaState_h
#define _ctaState_h


#include "ctaObject.h"
#include "ctaRestTrue.h"
#include "ctaString.h"
#include "ctaSetPointer.h"
#include "ctaTransition.h"
class ctaModule;

class ctaState : private ctaObject
{
private:
  ctaString* mName;

  ctaRestriction* mInvariant;
  ctaRestriction* mDerivation;
  ctaSetPointer<ctaTransition*>* mTransitions;
  
  // It should be not allowed to use the standard operators.
  ctaState(const ctaState&);
  void operator=(const ctaState&);
  void operator,(const ctaState&);

public:
  // constructor and destructor
  ctaState()
  {}
 
  ctaState(ctaString* pName,
	   ctaRestriction* pInvariant,
	   ctaRestriction* pDerivation,
	   ctaSetPointer<ctaTransition*>* pTransitions)
    : mName(pName),
      mInvariant(pInvariant),
      mDerivation(pDerivation),
      mTransitions(pTransitions)
  {
    if(!mInvariant || !mDerivation)
    {
      cerr << "Runtime error: Component pointer in ctaState is NULL!"
	   << endl;
      exit(1);
    }
  }
  
  ~ctaState()
  {
    if(!mName || !mInvariant || !mDerivation || !mTransitions)
    {
      cerr << "Runtime error: Component pointer in ~ctaState() is NULL!"
	   << endl;
      exit(1);
    }
    delete mName;
    delete mInvariant;
    delete mDerivation;
    // This works with an empty set.
    delete mTransitions;
  }

  // accessor methods

  ctaString* 
  GetName() const
  {
    return mName;
  }

  ctaRestriction*
  GetInvariant() const
  {
    return mInvariant;
  }

  ctaRestriction* 
  GetDerivation() const
  {
    return mDerivation;
  }

  ctaSetPointer<ctaTransition*>* 
  GetTransitions() const
  {
    return mTransitions;
  }

  ctaState* 
  cloneSetPrefixAndIdent(const ctaString *const pPrefix,
			 ctaMap<ctaString*>* pIdentNames) const;

  // Service methods.
  // This method is for context analysis.
  // We give the parent module for accessing module features.

  bool 
  CheckNonStrictness(const ctaModule* pCtaModule) const;

  bool 
  CheckBddConstraint(const ctaModule* pCtaModule) const;
 
  void 
  CheckConsistency(ctaModule*, ctaAutomaton*);

  void 
  mkDdmCompletions(ctaModule* pModule);

  // aheinig 2000-11-02
  // Collect all used signals and variables for this automaton.
  void 
  collectInterfacesForCommunicationGraph(ctaSet<ctaString>* lUsedInterfaces);

};

ostream& operator << (ostream &, const ctaState*);

#endif

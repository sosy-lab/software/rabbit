//-*-Mode: C++;-*-

/*
 * File:        ctaParser.h
 * Purpose:     Parser Class for cta-library
 * Author:      Dirk Beyer
 * Created:     1998
 * Copyright:   (c) 1998, University of Brandenburg, Cottbus
 */

#ifndef _ctaParser_h
#define _ctaParser_h


#include <set>

#include "ctaObject.h"
#include "ctaScanner.h"
#include "ctaString.h"
//#include "ctaSet.h"

extern int line;
extern int yyparse();

class ctaParser : private ctaObject
{
private: // Attributes.
  // Aggregated.
  ctaScanner* mScanner;

public: // Attributes.
  // Used only by scanner to determine if an
  //   identifier is a region variable or something else.
  // Associated.
  set<ctaString>* mRegionVariables;

  // It should be not allowed to use the standard operators.
  ctaParser(const ctaParser&);
  ctaParser& operator=(const ctaParser&);
  void operator,(const ctaParser&);

public: // Methods.
  // constructor and destructor
  ctaParser(ctaScanner* pScanner)
    : mScanner(pScanner),
      mRegionVariables(NULL)
  {}
 
  ~ctaParser()
  {
    delete mScanner;
  }
	
  ctaScanner* 
  GetScanner() const
  {
    return mScanner;
  }

  // Service methods.
  bool 
  Parse()
  {
    line = 1;
    return(!yyparse());
  }
};

extern ctaParser* pubParser;
extern void yyerror(char* ctaString);
#endif

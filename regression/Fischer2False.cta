MODULE Process
{
  // Constants for time bounds.
  INPUT
    // a is the maximal time the modelled assignment k:=1 needs.
    // b is the minimal time the process waits for assignments
    // initiated from other processes.
    //a:          DISCRETE;
    //b:          DISCRETE;
    a:          CONST;
    b:          CONST;
    // Parameter for significant value of k.
    processNo:  CONST;

  MULTREST
    // k is the flag for announcement.
    k:  DISCRETE;

  LOCAL
    // A clock to measure the time in a state.
    x:  CLOCK;

  INITIALIZATION { STATE(Fischer) = start; }

  AUTOMATON Fischer
  {
    STATE start      { INV { FALSE; }
	                 TRANS { DO { k' = 0; }
			         GOTO uncritical
                             }
		       }

    STATE uncritical { TRANS { GUARD { k  = 0; }
                               DO { x' = 0; }
				 GOTO assign
			       }
		       }

    STATE assign     { INV { x <= a; }
			 TRANS { DO { x' = 0 AND k' = processNo; }
			         GOTO wait
			       }
                     }

    STATE wait       { TRANS { GUARD { x >= b AND k <> processNo; }
			         GOTO uncritical			 
			       }
                       TRANS { GUARD { x > b AND k = processNo; }
				 GOTO critical
			       }
                     }

    STATE critical   { TRANS { DO { k' = 0; }
			         GOTO uncritical
			 }
    }
  }
}

MODULE System
{
  LOCAL
//  a : DISCRETE;
//  b : DISCRETE;
    a = 3 : CONST;
    b = 2 : CONST;

    pNo1 = 1: CONST;
    pNo2 = 2: CONST;

  LOCAL                // MULTREST for the submodules.
    k:     DISCRETE;

  INST Proc1 FROM Process WITH
  {
    a         AS a;
    b         AS b;
    processNo AS pNo1;
    k         AS k;
  }

  INST Proc2 FROM Process WITH
  {
    a         AS a;
    b         AS b;
    processNo AS pNo2;
    k         AS k;
  }
}

REGION CHECK System
{
  VAR
    initial, error, reached : REGION;

  COMMANDS
    initial :=  STATE(Proc1.Fischer) = Proc1.start
      INTERSECT STATE(Proc2.Fischer) = Proc2.start;

    error   :=  STATE(Proc1.Fischer) = Proc1.critical
      INTERSECT STATE(Proc2.Fischer) = Proc2.critical;

    reached := REACH FROM initial FORWARD;

    IF( EMPTY( error INTERSECT reached ) ) {
      PRINT "Mutual exclusion satisfied"; }
    ELSE {
      PRINT "Mutual exclusion violated";
      PRINT "The resulting region:" reached " !"; };
}

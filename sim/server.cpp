#include <fstream>
#include <stdio.h>
#include <map>
#include <string>

#include "WinSock2.h" 

#include "fertanl.h"

#define Win32_Winsock

void
processInput(int pTheirSockfd,
	     map<string, bool (*)()>& pInputFuncMap) 
{   
  char inputBuffer[255];
  int lSendLenght;

  do_set_signals();  

  for(map<string, bool (*)()>::const_iterator 
	lIt = pInputFuncMap.begin();
      lIt != pInputFuncMap.end();
      ++lIt)
  {
    if( (*lIt->second)() )
    {
      //cout << "Following signal pushed: " << lIt->first << endl;
      //lSendLenght = send(pTheirSockfd, (char *)&inputBuffer, 255, 0);

      const char* buffer = lIt->first.c_str();    

      lSendLenght = send(pTheirSockfd, buffer, 255, 0);
      //pQueue.push(mSymTab->getSyncNum(lIt->first)); 
    }   
  }
}

// Initialize function map for input.
//   (pInputFuncMap) is in/out parameter.
void 
initInputFuncMap(map<string, bool (*)()>& pInputFuncMap) 
{
  pInputFuncMap["b_sensX_on"]        = &b_sensX_on;
  pInputFuncMap["b_sensLdrX_on"]     = &b_sensLdrX_on;
  pInputFuncMap["b_sensLdrX_off"]    = &b_sensLdrX_off;
  pInputFuncMap["b1_sens1_on"]       = &b1_sens1_on;
  pInputFuncMap["b1_sens2_on"]       = &b1_sens2_on;
  pInputFuncMap["b2_sens_on"]        = &b2_sens_on;
  pInputFuncMap["b3_sens1_on"]       = &b3_sens1_on;
  pInputFuncMap["b3_sens2_on"]       = &b3_sens2_on;
  pInputFuncMap["b5_sens1_on"]       = &b5_sens1_on;
  pInputFuncMap["b5_sens2_on"]       = &b5_sens2_on;
  pInputFuncMap["b6_sens1_on"]       = &b6_sens1_on;
  pInputFuncMap["b6_sens2_on"]       = &b6_sens2_on;
  pInputFuncMap["b7_sens_on"]        = &b7_sens_on; 
  pInputFuncMap["b8_sens1_on"]       = &b8_sens1_on;
  pInputFuncMap["b8_sens2_on"]       = &b8_sens2_on;
  pInputFuncMap["b10_sens1_on"]      = &b10_sens1_on;
  pInputFuncMap["b10_sens2_on"]      = &b10_sens2_on;
  pInputFuncMap["m1_sensTop_on"]     = &m1_sensTop_on;
  pInputFuncMap["m1_sensBottom_on"]  = &m1_sensBottom_on; 
  pInputFuncMap["m1_sensBack_on"]    = &m1_sensBack_on;
  pInputFuncMap["m1_sensFront_on"]   = &m1_sensFront_on; 
  pInputFuncMap["m2_sensTop_on"]     = &m2_sensTop_on;
  pInputFuncMap["m2_sensBottom_on"]  = &m2_sensBottom_on; 
  pInputFuncMap["m2_sensBack_on"]    = &m2_sensBack_on;
  pInputFuncMap["m2_sensFront_on"]   = &m2_sensFront_on; 
  pInputFuncMap["m2_sensRevolv_on"]  = &m2_sensRevolv_on; 
  pInputFuncMap["m3_sensTop_on"]     = &m3_sensTop_on;
  pInputFuncMap["m3_sensBottom_on"]  = &m3_sensBottom_on; 
  pInputFuncMap["m4_sensTop_on"]     = &m4_sensTop_on;
  pInputFuncMap["m4_sensBottom_on"]  = &m4_sensBottom_on;
  pInputFuncMap["p1_sensBack_on"]    = &p1_sensBack_on;
  pInputFuncMap["p1_sensFront_on"]   = &p1_sensFront_on; 
  pInputFuncMap["p2_sensBack_on"]    = &p2_sensBack_on;
  pInputFuncMap["p2_sensFront_on"]   = &p2_sensFront_on; 
  pInputFuncMap["t_sensLeft_on"]     = &t_sensLeft_on;
  pInputFuncMap["t_sensRight_on"]    = &t_sensRight_on;
  pInputFuncMap["tt1_sensX_on"]      = &tt1_sensX_on;
  pInputFuncMap["tt1_sensLeft_on"]   = &tt1_sensLeft_on;
  pInputFuncMap["tt1_sensRight_on"]  = &tt1_sensRight_on;
  pInputFuncMap["tt2_sensX_on"]      = &tt2_sensX_on;
  pInputFuncMap["tt2_sensLeft_on"]   = &tt2_sensLeft_on;
  pInputFuncMap["tt2_sensRight_on"]  = &tt2_sensRight_on; 
}

void 
initOutputFuncMap(map<string, void (*)()>& pOutputFuncMap) 
{
  pOutputFuncMap["b_forward"]        = &b_forward;
  pOutputFuncMap["b_stop"]           = &b_stop;
  pOutputFuncMap["b1_forward"]       = &b1_forward;
  pOutputFuncMap["b1_backward"]      = &b1_backward;
  pOutputFuncMap["b1_stop"]          = &b1_stop;
  pOutputFuncMap["b2_forward"]       = &b2_forward;
  pOutputFuncMap["b2_backward"]      = &b2_backward;
  pOutputFuncMap["b2_stop"]          = &b2_stop;
  pOutputFuncMap["b3_forward"]       = &b3_forward;
  pOutputFuncMap["b3_backward"]      = &b3_backward;
  pOutputFuncMap["b3_stop"]          = &b3_stop;
  pOutputFuncMap["b4_forward"]       = &b4_forward;
  pOutputFuncMap["b4_backward"]      = &b4_backward;
  pOutputFuncMap["b4_stop"]          = &b4_stop;  
  pOutputFuncMap["b5_forward"]       = &b5_forward;
  pOutputFuncMap["b5_backward"]      = &b5_backward;
  pOutputFuncMap["b5_stop"]          = &b5_stop;  
  pOutputFuncMap["b6_forward"]       = &b6_forward;
  pOutputFuncMap["b6_backward"]      = &b6_backward;
  pOutputFuncMap["b6_stop"]          = &b6_stop;  
  pOutputFuncMap["b7_forward"]       = &b7_forward;
  pOutputFuncMap["b7_backward"]      = &b7_backward;
  pOutputFuncMap["b7_stop"]          = &b7_stop;  
  pOutputFuncMap["b8_forward"]       = &b8_forward;
  pOutputFuncMap["b8_backward"]      = &b8_backward;
  pOutputFuncMap["b8_stop"]          = &b8_stop;  
  pOutputFuncMap["b9_forward"]       = &b9_forward;
  pOutputFuncMap["b9_backward"]      = &b9_backward;
  pOutputFuncMap["b9_stop"]          = &b9_stop;  
  pOutputFuncMap["b10_forward"]      = &b10_forward;
  pOutputFuncMap["b10_backward"]     = &b10_backward;
  pOutputFuncMap["b10_stop"]         = &b10_stop;
  pOutputFuncMap["m1_up"]            = &m1_up;
  pOutputFuncMap["m1_down"]          = &m1_down;
  pOutputFuncMap["m1_stop"]          = &m1_stop;
  pOutputFuncMap["m1_t_forward"]     = &m1_t_forward;
  pOutputFuncMap["m1_t_backward"]    = &m1_t_backward;
  pOutputFuncMap["m1_t_stop"]        = &m1_t_stop;
  pOutputFuncMap["m1_d_right"]       = &m1_d_right;
  pOutputFuncMap["m1_d_stop"]        = &m1_d_stop;
  pOutputFuncMap["m2_up"]            = &m2_up;
  pOutputFuncMap["m2_down"]          = &m2_down;
  pOutputFuncMap["m2_stop"]          = &m2_stop;
  pOutputFuncMap["m2_t_forward"]     = &m2_t_forward;
  pOutputFuncMap["m2_t_backward"]    = &m2_t_backward;
  pOutputFuncMap["m2_t_stop"]        = &m2_t_stop;
  pOutputFuncMap["m2_d_right"]       = &m2_d_right;
  pOutputFuncMap["m2_d_stop"]        = &m2_d_stop;
  pOutputFuncMap["m2_revolv_right"]  = &m2_revolv_right;
  pOutputFuncMap["m2_revolv_stop"]   = &m2_revolv_stop;
  pOutputFuncMap["m3_up"]            = &m3_up;
  pOutputFuncMap["m3_down"]          = &m3_down;
  pOutputFuncMap["m3_stop"]          = &m3_stop;
  pOutputFuncMap["m4_up"]            = &m4_up;
  pOutputFuncMap["m4_down"]          = &m4_down;
  pOutputFuncMap["m4_stop"]          = &m4_stop;
  pOutputFuncMap["m4_d_right"]       = &m4_d_right;
  pOutputFuncMap["m4_d_stop"]        = &m4_d_stop;
  pOutputFuncMap["p1_forward"]       = &p1_forward;
  pOutputFuncMap["p1_backward"]      = &p1_backward;
  pOutputFuncMap["p1_stop"]          = &p1_stop;
  pOutputFuncMap["p2_forward"]       = &p2_forward;
  pOutputFuncMap["p2_backward"]      = &p2_backward;
  pOutputFuncMap["p2_stop"]          = &p2_stop;
  pOutputFuncMap["t_left"]           = &t_left;
  pOutputFuncMap["t_right"]          = &t_right;
  pOutputFuncMap["t_stop"]           = &t_stop;
  pOutputFuncMap["tt1_b_forward"]    = &tt1_b_forward;
  pOutputFuncMap["tt1_b_stop"]       = &tt1_b_stop;
  pOutputFuncMap["tt1_left"]         = &tt1_left;
  pOutputFuncMap["tt1_right"]        = &tt1_right;
  pOutputFuncMap["tt1_stop"]         = &tt1_stop;
  pOutputFuncMap["tt2_b_forward"]    = &tt2_b_forward;
  pOutputFuncMap["tt2_b_stop"]       = &tt2_b_stop;
  pOutputFuncMap["tt2_left"]         = &tt2_left;
  pOutputFuncMap["tt2_right"]        = &tt2_right;
  pOutputFuncMap["tt2_stop"]         = &tt2_stop;
}

// the main program
int main(int argc, char *argv []) 
{
  WSADATA wsaData;
  int lSockfd, lTheirSockfd;
  struct sockaddr_in lMySockAdr;

  cout << "Sockets aufbauen." << endl;

  if( WSAStartup(MAKEWORD(2, 2), &wsaData ) != 0 )
  {
    cout << "Error in startup the sockets." << endl;
  }

  lSockfd = socket(AF_INET, SOCK_STREAM, 0);
  if(lSockfd == (-1))
  { 
    cout << "Construction of socket incorrect." << endl;
  }  

  lMySockAdr.sin_family = AF_INET;
  lMySockAdr.sin_port=htons(4444);
  lMySockAdr.sin_addr.s_addr = inet_addr("141.43.23.134"); //erle
  //lMySockAdr.sin_addr.s_addr = inet_addr("141.43.23.159"); //linde
  memset(&(lMySockAdr.sin_zero),'\0', 8);

  if( bind(lSockfd, (struct sockaddr*) &lMySockAdr, sizeof(sockaddr)) < 0 )
  {
    cout << "Method bind() generate error: " << WSAGetLastError() <<endl;
  }

  if( listen(lSockfd, 3) == -1 )
  {
    cout << "Error bei listen(): " << WSAGetLastError() << endl;
  }
  
  lTheirSockfd = accept(lSockfd, NULL, NULL);
  if( lTheirSockfd == -1 )
  {
    cout << "Error bei accept(): " << WSAGetLastError() << endl;
  }

  cout << "accepted" << endl;
  
  string outputBuffer;
  string cancelSequenze = "";
  int sendLenght = 0;

  char buffer[255];

  // Mapping signal names to functions for I/O.
  map<string, bool (*)()> lInputFuncMap;
  initInputFuncMap(lInputFuncMap);

  // Mapping signal names to functions for I/O.
  map<string, void (*)()> lOutputFuncMap;
  initOutputFuncMap(lOutputFuncMap);

  init();

  do
  {
    bool cancelReceive = false;
    do
    {
      //cout << "vor recv" << endl;

      sendLenght = recv(lTheirSockfd, buffer, 255, 0);
 
      //printf("%s\n", buffer);

      outputBuffer = buffer;

      //cout << "recv: " << outputBuffer << endl;

      if( outputBuffer == "" )
      {
	// Cancel output to enviroment.
	cancelReceive = true;
      }
      else
      {
	//umgebung schalten
	(* lOutputFuncMap[outputBuffer] )();
	cout << "method called: " << outputBuffer << endl;
      }
    }
    while( !cancelReceive );

    //cout << "nach while: " << endl;

    //inputseinlesen
    processInput(lTheirSockfd, lInputFuncMap);
  
    const char* buffer = cancelSequenze.c_str();   
    //abschlusssequenz senden-blockieren aufheben
    sendLenght = send(lTheirSockfd, buffer, 255, 0);
  }
  while( true );

  cout << "Sockets aufbauen beendet." << endl;

  return(0);
}

//  Compile with '-O'.

// 'stdlib.h' is used for 'abort'.
#include <stdlib.h>
// 'signal.h' is used for 'signal' and 'SIGINT'.
#include <signal.h>

#include <stdio.h>

#include <string.h>


// 'io' is used for port io: 'inb' and 'outb'.
#include "io.h"

//for GetTickCount();
//#include <windows.h>


// Port layout:
//   We have six identically built PIOs 8255,
//   each with 4 byte-wide registers.  These regs are mapped to
//   byte adresses at 512..535.
//   The registers for a PIO are mapped to four contiguous bytes.
//   The first three bytes (offsets 0,1,2 from PIO start adress) are
//   data registers, the fourth byte (offset 3) is a control register.
//   The data registers are counted A,B,C.  They can be programmed 
//   as inputs or outputs.  The bits of register must either all be
//   programmed as output or all as input.  The same is true for 
//   register B.  Register C may be split: The lower four bits
//   may be programmed one way, the upper four bits the other way.
//   The byte value written to the control register determines how
//   register A, register B, the lower half and the upper half of 
//   register C will be used:
//
//   Bits: 7  6  5  4  3  2  1  0
//   Val:  1  0  0  X  X  0  X  X
//
//   Where:
//     X = 1 means: the corresponding bits are programmed as inputs.
//     X = 0 means: the corresponding bits are programmed as outputs.
//
//     The value of bit 4 determines the use of all bits of register A.
//     The value of bit 3 determines the use of register C, bits 4..7.
//     The value of bit 1 determines the use of all bits of register B.
//     The value of bit 0 determines the use of register C, bits 0..3.
//
//   All the outputs are low-active, i.e. an active signal
//   is encoded by a 0, an inactive signal is encoded by a 1.
//

// An array for the data transfer with the outer world.
static char portVal[24];


// Declaration of a high-active input signal at port 'port', bit 'bit'.
#define DECL_GET_INPUT_SIGNAL(port, bit) \
  int M_Pio_Connections_I_pio##port##_##bit() \
  { \
    if ((portVal[port] & (1<<(bit))) != 0 ) \
        return 1; \
    else \
        return 0; \
  }


//Declare all input signals
DECL_GET_INPUT_SIGNAL(0,0);
DECL_GET_INPUT_SIGNAL(0,1);
DECL_GET_INPUT_SIGNAL(0,2);
DECL_GET_INPUT_SIGNAL(0,3);
DECL_GET_INPUT_SIGNAL(0,4);
DECL_GET_INPUT_SIGNAL(0,5);
DECL_GET_INPUT_SIGNAL(0,6);
DECL_GET_INPUT_SIGNAL(0,7);

DECL_GET_INPUT_SIGNAL(1,0);
DECL_GET_INPUT_SIGNAL(1,1);
DECL_GET_INPUT_SIGNAL(1,2);
DECL_GET_INPUT_SIGNAL(1,3);
DECL_GET_INPUT_SIGNAL(1,5);

DECL_GET_INPUT_SIGNAL(2,0);
DECL_GET_INPUT_SIGNAL(2,1);
DECL_GET_INPUT_SIGNAL(2,2);
DECL_GET_INPUT_SIGNAL(2,4);
DECL_GET_INPUT_SIGNAL(2,5);
DECL_GET_INPUT_SIGNAL(2,6);
DECL_GET_INPUT_SIGNAL(2,7);

DECL_GET_INPUT_SIGNAL(4,0);
DECL_GET_INPUT_SIGNAL(4,1);
DECL_GET_INPUT_SIGNAL(4,2);
DECL_GET_INPUT_SIGNAL(4,3);
DECL_GET_INPUT_SIGNAL(4,4);
DECL_GET_INPUT_SIGNAL(4,5);
DECL_GET_INPUT_SIGNAL(4,6);
DECL_GET_INPUT_SIGNAL(4,7);

DECL_GET_INPUT_SIGNAL(5,0);
DECL_GET_INPUT_SIGNAL(5,1);
DECL_GET_INPUT_SIGNAL(5,2);
DECL_GET_INPUT_SIGNAL(5,3);
DECL_GET_INPUT_SIGNAL(5,4);
DECL_GET_INPUT_SIGNAL(5,5);
DECL_GET_INPUT_SIGNAL(5,6);
DECL_GET_INPUT_SIGNAL(5,7);

DECL_GET_INPUT_SIGNAL(6,0);
DECL_GET_INPUT_SIGNAL(6,1);
DECL_GET_INPUT_SIGNAL(6,2);
DECL_GET_INPUT_SIGNAL(6,3);
DECL_GET_INPUT_SIGNAL(6,4);
DECL_GET_INPUT_SIGNAL(6,5);
DECL_GET_INPUT_SIGNAL(6,6);
DECL_GET_INPUT_SIGNAL(6,7);

DECL_GET_INPUT_SIGNAL(16,6);
DECL_GET_INPUT_SIGNAL(16,7);

DECL_GET_INPUT_SIGNAL(17,0);
DECL_GET_INPUT_SIGNAL(17,1);
DECL_GET_INPUT_SIGNAL(17,2);
DECL_GET_INPUT_SIGNAL(17,3);
DECL_GET_INPUT_SIGNAL(17,4);
DECL_GET_INPUT_SIGNAL(17,5);
DECL_GET_INPUT_SIGNAL(17,6);
DECL_GET_INPUT_SIGNAL(17,7);



// Declaration of a low-active output signal at port 'port', bit 'bit'.
#define DECL_SET_LOWOUTPUT_SIGNAL(port, bit) \
  void M_Pio_Connections_O_setpiolow##port##_##bit()\
  {\
    portVal[port] &= ~(1<<(bit));\
  }\
  void M_Pio_Connections_O_setpiohigh##port##_##bit()\
  {\
    portVal[port] |= (1<<(bit));\
  }


// Declare all output signals as low-active.
DECL_SET_LOWOUTPUT_SIGNAL(8,0);
DECL_SET_LOWOUTPUT_SIGNAL(8,1);
DECL_SET_LOWOUTPUT_SIGNAL(8,2);
DECL_SET_LOWOUTPUT_SIGNAL(8,3);
DECL_SET_LOWOUTPUT_SIGNAL(8,4);
DECL_SET_LOWOUTPUT_SIGNAL(8,5);
DECL_SET_LOWOUTPUT_SIGNAL(8,6);
DECL_SET_LOWOUTPUT_SIGNAL(8,7);

DECL_SET_LOWOUTPUT_SIGNAL(9,0);
DECL_SET_LOWOUTPUT_SIGNAL(9,1);
DECL_SET_LOWOUTPUT_SIGNAL(9,2);
DECL_SET_LOWOUTPUT_SIGNAL(9,3);
DECL_SET_LOWOUTPUT_SIGNAL(9,4);
DECL_SET_LOWOUTPUT_SIGNAL(9,5);
DECL_SET_LOWOUTPUT_SIGNAL(9,6);
DECL_SET_LOWOUTPUT_SIGNAL(9,7);

DECL_SET_LOWOUTPUT_SIGNAL(10,0);
DECL_SET_LOWOUTPUT_SIGNAL(10,1);
DECL_SET_LOWOUTPUT_SIGNAL(10,2);
DECL_SET_LOWOUTPUT_SIGNAL(10,3);
DECL_SET_LOWOUTPUT_SIGNAL(10,4);
DECL_SET_LOWOUTPUT_SIGNAL(10,5);
DECL_SET_LOWOUTPUT_SIGNAL(10,6);
DECL_SET_LOWOUTPUT_SIGNAL(10,7);


DECL_SET_LOWOUTPUT_SIGNAL(12,0);
DECL_SET_LOWOUTPUT_SIGNAL(12,1);
DECL_SET_LOWOUTPUT_SIGNAL(12,2);
DECL_SET_LOWOUTPUT_SIGNAL(12,3);
DECL_SET_LOWOUTPUT_SIGNAL(12,4);
DECL_SET_LOWOUTPUT_SIGNAL(12,5);
DECL_SET_LOWOUTPUT_SIGNAL(12,6);
DECL_SET_LOWOUTPUT_SIGNAL(12,7);

DECL_SET_LOWOUTPUT_SIGNAL(13,0);
DECL_SET_LOWOUTPUT_SIGNAL(13,1);
DECL_SET_LOWOUTPUT_SIGNAL(13,2);
DECL_SET_LOWOUTPUT_SIGNAL(13,3);
DECL_SET_LOWOUTPUT_SIGNAL(13,4);
DECL_SET_LOWOUTPUT_SIGNAL(13,5);
DECL_SET_LOWOUTPUT_SIGNAL(13,6);
DECL_SET_LOWOUTPUT_SIGNAL(13,7);

DECL_SET_LOWOUTPUT_SIGNAL(14,0);
DECL_SET_LOWOUTPUT_SIGNAL(14,1);
DECL_SET_LOWOUTPUT_SIGNAL(14,2);
DECL_SET_LOWOUTPUT_SIGNAL(14,3);
DECL_SET_LOWOUTPUT_SIGNAL(14,4);
DECL_SET_LOWOUTPUT_SIGNAL(14,5);
DECL_SET_LOWOUTPUT_SIGNAL(14,6);
DECL_SET_LOWOUTPUT_SIGNAL(14,7);


DECL_SET_LOWOUTPUT_SIGNAL(18,0);
DECL_SET_LOWOUTPUT_SIGNAL(18,1);
DECL_SET_LOWOUTPUT_SIGNAL(18,2);
DECL_SET_LOWOUTPUT_SIGNAL(18,3);
DECL_SET_LOWOUTPUT_SIGNAL(18,4);
DECL_SET_LOWOUTPUT_SIGNAL(18,5);
DECL_SET_LOWOUTPUT_SIGNAL(18,6);
DECL_SET_LOWOUTPUT_SIGNAL(18,7);


DECL_SET_LOWOUTPUT_SIGNAL(20,0);
DECL_SET_LOWOUTPUT_SIGNAL(20,1);
DECL_SET_LOWOUTPUT_SIGNAL(20,2);
DECL_SET_LOWOUTPUT_SIGNAL(20,3);
DECL_SET_LOWOUTPUT_SIGNAL(20,4);
DECL_SET_LOWOUTPUT_SIGNAL(20,5);
DECL_SET_LOWOUTPUT_SIGNAL(20,6);
DECL_SET_LOWOUTPUT_SIGNAL(20,7);

DECL_SET_LOWOUTPUT_SIGNAL(21,0);
DECL_SET_LOWOUTPUT_SIGNAL(21,1);
DECL_SET_LOWOUTPUT_SIGNAL(21,2);
DECL_SET_LOWOUTPUT_SIGNAL(21,3);
DECL_SET_LOWOUTPUT_SIGNAL(21,4);
DECL_SET_LOWOUTPUT_SIGNAL(21,5);
DECL_SET_LOWOUTPUT_SIGNAL(21,6);
DECL_SET_LOWOUTPUT_SIGNAL(21,7);

DECL_SET_LOWOUTPUT_SIGNAL(22,0);
DECL_SET_LOWOUTPUT_SIGNAL(22,1);
DECL_SET_LOWOUTPUT_SIGNAL(22,2);
DECL_SET_LOWOUTPUT_SIGNAL(22,3);
DECL_SET_LOWOUTPUT_SIGNAL(22,4);
DECL_SET_LOWOUTPUT_SIGNAL(22,5);
DECL_SET_LOWOUTPUT_SIGNAL(22,6);
DECL_SET_LOWOUTPUT_SIGNAL(22,7);

// Declaration of a unset low-active output signal at port 'port', bit 'bit'.
#define DECL_UNSET_LOWOUTPUT_SIGNAL(port, bit) \
  void M_Pio_Connections_O_unsetpiolow##port##_##bit()\
  {\
    portVal[port] |= (1<<(bit));\
  }


// Declare all output signals as low-active.
DECL_UNSET_LOWOUTPUT_SIGNAL(8,0);
DECL_UNSET_LOWOUTPUT_SIGNAL(8,1);
DECL_UNSET_LOWOUTPUT_SIGNAL(8,2);
DECL_UNSET_LOWOUTPUT_SIGNAL(8,3);
DECL_UNSET_LOWOUTPUT_SIGNAL(8,4);
DECL_UNSET_LOWOUTPUT_SIGNAL(8,5);
DECL_UNSET_LOWOUTPUT_SIGNAL(8,6);
DECL_UNSET_LOWOUTPUT_SIGNAL(8,7);

DECL_UNSET_LOWOUTPUT_SIGNAL(9,0);
DECL_UNSET_LOWOUTPUT_SIGNAL(9,1);
DECL_UNSET_LOWOUTPUT_SIGNAL(9,2);
DECL_UNSET_LOWOUTPUT_SIGNAL(9,3);
DECL_UNSET_LOWOUTPUT_SIGNAL(9,4);
DECL_UNSET_LOWOUTPUT_SIGNAL(9,5);
DECL_UNSET_LOWOUTPUT_SIGNAL(9,6);
DECL_UNSET_LOWOUTPUT_SIGNAL(9,7);

DECL_UNSET_LOWOUTPUT_SIGNAL(10,0);
DECL_UNSET_LOWOUTPUT_SIGNAL(10,1);
DECL_UNSET_LOWOUTPUT_SIGNAL(10,2);
DECL_UNSET_LOWOUTPUT_SIGNAL(10,3);
DECL_UNSET_LOWOUTPUT_SIGNAL(10,4);
DECL_UNSET_LOWOUTPUT_SIGNAL(10,5);
DECL_UNSET_LOWOUTPUT_SIGNAL(10,6);
DECL_UNSET_LOWOUTPUT_SIGNAL(10,7);

DECL_UNSET_LOWOUTPUT_SIGNAL(12,0);
DECL_UNSET_LOWOUTPUT_SIGNAL(12,1);
DECL_UNSET_LOWOUTPUT_SIGNAL(12,2);
DECL_UNSET_LOWOUTPUT_SIGNAL(12,3);
DECL_UNSET_LOWOUTPUT_SIGNAL(12,4);
DECL_UNSET_LOWOUTPUT_SIGNAL(12,5);
DECL_UNSET_LOWOUTPUT_SIGNAL(12,6);
DECL_UNSET_LOWOUTPUT_SIGNAL(12,7);

DECL_UNSET_LOWOUTPUT_SIGNAL(13,0);
DECL_UNSET_LOWOUTPUT_SIGNAL(13,1);
DECL_UNSET_LOWOUTPUT_SIGNAL(13,2);
DECL_UNSET_LOWOUTPUT_SIGNAL(13,3);
DECL_UNSET_LOWOUTPUT_SIGNAL(13,4);
DECL_UNSET_LOWOUTPUT_SIGNAL(13,5);
DECL_UNSET_LOWOUTPUT_SIGNAL(13,6);
DECL_UNSET_LOWOUTPUT_SIGNAL(13,7);

DECL_UNSET_LOWOUTPUT_SIGNAL(14,0);
DECL_UNSET_LOWOUTPUT_SIGNAL(14,1);
DECL_UNSET_LOWOUTPUT_SIGNAL(14,2);
DECL_UNSET_LOWOUTPUT_SIGNAL(14,3);
DECL_UNSET_LOWOUTPUT_SIGNAL(14,4);
DECL_UNSET_LOWOUTPUT_SIGNAL(14,5);
DECL_UNSET_LOWOUTPUT_SIGNAL(14,6);
DECL_UNSET_LOWOUTPUT_SIGNAL(14,7);

DECL_UNSET_LOWOUTPUT_SIGNAL(18,0);
DECL_UNSET_LOWOUTPUT_SIGNAL(18,1);
DECL_UNSET_LOWOUTPUT_SIGNAL(18,2);
DECL_UNSET_LOWOUTPUT_SIGNAL(18,3);
DECL_UNSET_LOWOUTPUT_SIGNAL(18,4);
DECL_UNSET_LOWOUTPUT_SIGNAL(18,5);
DECL_UNSET_LOWOUTPUT_SIGNAL(18,6);
DECL_UNSET_LOWOUTPUT_SIGNAL(18,7);

DECL_UNSET_LOWOUTPUT_SIGNAL(20,0);
DECL_UNSET_LOWOUTPUT_SIGNAL(20,1);
DECL_UNSET_LOWOUTPUT_SIGNAL(20,2);
DECL_UNSET_LOWOUTPUT_SIGNAL(20,3);
DECL_UNSET_LOWOUTPUT_SIGNAL(20,4);
DECL_UNSET_LOWOUTPUT_SIGNAL(20,5);
DECL_UNSET_LOWOUTPUT_SIGNAL(20,6);
DECL_UNSET_LOWOUTPUT_SIGNAL(20,7);

DECL_UNSET_LOWOUTPUT_SIGNAL(21,0);
DECL_UNSET_LOWOUTPUT_SIGNAL(21,1);
DECL_UNSET_LOWOUTPUT_SIGNAL(21,2);
DECL_UNSET_LOWOUTPUT_SIGNAL(21,3);
DECL_UNSET_LOWOUTPUT_SIGNAL(21,4);
DECL_UNSET_LOWOUTPUT_SIGNAL(21,5);
DECL_UNSET_LOWOUTPUT_SIGNAL(21,6);
DECL_UNSET_LOWOUTPUT_SIGNAL(21,7);

DECL_UNSET_LOWOUTPUT_SIGNAL(22,0);
DECL_UNSET_LOWOUTPUT_SIGNAL(22,1);
DECL_UNSET_LOWOUTPUT_SIGNAL(22,2);
DECL_UNSET_LOWOUTPUT_SIGNAL(22,3);
DECL_UNSET_LOWOUTPUT_SIGNAL(22,4);
DECL_UNSET_LOWOUTPUT_SIGNAL(22,5);
DECL_UNSET_LOWOUTPUT_SIGNAL(22,6);
DECL_UNSET_LOWOUTPUT_SIGNAL(22,7);

// Initialize outport values.
static void reset_outports()
{
  // Inactive means bit=1.

  // The pio number for the current output pio.
  int pio;

  // Reset regs for all output pios: 2,3,4,5.
  for(pio=2; pio<6; pio++)
  {
    // The register number.
    int reg;

    // Pio 4 is special: only reg c (offset 2) 
    //   is used for output.
    if (pio==4)
    { 
      outb(0xff, 512+pio*4+2);
    }
    else
    {
      // All data regs. are used for output.

      // Data registers are at offsets 0,1,2
      for(reg=0; reg<3; reg++)
      {
        // Put byte to port. 
        outb(0xff, 512+pio*4+reg);
      }
    }
  }
}

// Initialize outport values in portVal-array.
static void init_portVal()
{
  // Inactive means bit=1.

  // The pio number for the current output pio.
  int pio;

  // Reset regs for all output pios: 2,3,4,5.
  for(pio=2; pio<6; pio++)
  {
    // The register number.
    int reg;

    // Pio 4 is special.
    if (pio==4)
    { 
      portVal[pio*4+2] = 0xff;
    }
    else
    {
      // Each PIO occupies 4 contiguous bytes in the adress space.
      // Data registers are at offsets 0,1,2, the control register
      // is at offset 3.
      for(reg=0; reg<3; reg++)
      {
        // Put byte to portVal-array. 
        //  0xff is right since all outputs are low-active.
        portVal[pio*4+reg] = 0xff;
      }
    }
  }
}

// Initialize ports.
static void init_()
{
  // Initialize input port control regs.
  outb(0x9b, 512+0*4+3);
  outb(0x9b, 512+1*4+3);
  
  // Initialize output port control regs.
  outb(0x80, 512+2*4+3);
  outb(0x80, 512+3*4+3);

  // Initialize pio4 regs. a,b for input and c for output.
  outb(0x92, 512+4*4+3);	

  outb(0x80, 512+5*4+3);

  // Initialize output port values.
  reset_outports();
  init_portVal();  
}  

static void do_set_signals()
{
  // Transfer inputs.
  {
    portVal[0] = inb(512+0);
    portVal[1] = inb(512+1);
    portVal[2] = inb(512+2);
    
    portVal[4] = inb(512+4);
    portVal[5] = inb(512+5);
    portVal[6] = inb(512+6);

    portVal[16] = inb(512+16);
    portVal[17] = inb(512+17);
  }
}

// Put the output values to the output port.
void do_process_outputs()
{
  int pio;

  // PIOs 2,3,4,5 are used for output.
  for(pio = 2; pio < 6; pio++)
  {
    int reg;
    

    if (pio==4) 
    {
      outb(portVal[pio*4+2], 512+pio*4+2); 
    }
    else 
    {
      // Offsets 0,1,2 are data registers, 3 is the control register.
      for(reg=0; reg<3; reg++)
      {
        outb(portVal[pio*4+reg], 512+pio*4+reg); 
      }
    }
  }
}

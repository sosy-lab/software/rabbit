//-*-Mode: C++;-*-

/*
 * File:        fertanl.h
 * Purpose:     Interface functions for abstract access to actuators and
 *              sensors of the production cell.
 * Author:      Andy Heinig, Michael Vogel
 * Created:     2002
 * modified:    2002-08-06 mvogel
 * Copyright:   (c) 2000, Brandenburg Technical University of Cottbus
 */

#include "pcdriver.h"
#include <conio.h>

/* Flags used for generation of signal edges. */

static bool b_sensX_on_isHigh;
static bool b_sensX_off_isHigh;
static bool b_sensLdrX_on_isHigh;
static bool b_sensLdrX_off_isHigh;
static bool b1_sens1_on_isHigh;
static bool b1_sens1_off_isHigh;
static bool b1_sens2_on_isHigh;
static bool b1_sens2_off_isHigh;
static bool b2_sens_on_isHigh; 
static bool b2_sens_off_isHigh;
static bool b3_sens1_on_isHigh;
static bool b3_sens1_off_isHigh;
static bool b3_sens2_on_isHigh;
static bool b3_sens2_off_isHigh;
static bool b5_sens1_on_isHigh;
static bool b5_sens1_off_isHigh;
static bool b5_sens2_on_isHigh;
static bool b5_sens2_off_isHigh;
static bool b6_sens1_on_isHigh;
static bool b6_sens1_off_isHigh;
static bool b6_sens2_on_isHigh;
static bool b6_sens2_off_isHigh;
static bool b7_sens_on_isHigh;
static bool b7_sens_off_isHigh;
static bool b8_sens1_on_isHigh;
static bool b8_sens1_off_isHigh;
static bool b8_sens2_on_isHigh;
static bool b8_sens2_off_isHigh;
static bool b10_sens1_on_isHigh;
static bool b10_sens1_off_isHigh;
static bool b10_sens2_on_isHigh;
static bool b10_sens2_off_isHigh;
static bool m1_sensBack_on_isHigh;
static bool m1_sensBack_off_isHigh;
static bool m1_sensFront_on_isHigh;
static bool m1_sensFront_off_isHigh;
static bool m1_sensTop_on_isHigh;
static bool m1_sensTop_off_isHigh;
static bool m1_sensBottom_on_isHigh;
static bool m1_sensBottom_off_isHigh;
static bool m2_sensBack_on_isHigh;
static bool m2_sensBack_off_isHigh;
static bool m2_sensFront_on_isHigh;
static bool m2_sensFront_off_isHigh;
static bool m2_sensTop_on_isHigh;
static bool m2_sensTop_off_isHigh;
static bool m2_sensBottom_on_isHigh;
static bool m2_sensBottom_off_isHigh;
static bool m2_sensRevolv_on_isHigh;
static bool m2_sensRevolv_off_isHigh;
static bool m3_sensTop_on_isHigh;
static bool m3_sensTop_off_isHigh;
static bool m3_sensBottom_on_isHigh;
static bool m3_sensBottom_off_isHigh;
static bool m4_sensTop_on_isHigh;
static bool m4_sensTop_off_isHigh;
static bool m4_sensBottom_on_isHigh;
static bool m4_sensBottom_off_isHigh;
static bool p1_sensBack_on_isHigh;
static bool p1_sensBack_off_isHigh;
static bool p1_sensFront_on_isHigh;
static bool p1_sensFront_off_isHigh;
static bool p2_sensBack_on_isHigh;
static bool p2_sensBack_off_isHigh;
static bool p2_sensFront_on_isHigh;
static bool p2_sensFront_off_isHigh;
static bool t_sensRight_on_isHigh;
static bool t_sensRight_off_isHigh;
static bool t_sensLeft_on_isHigh;
static bool t_sensLeft_off_isHigh;
static bool tt1_sensX_on_isHigh;
static bool tt1_sensX_off_isHigh;
static bool tt1_sensLeft_on_isHigh;
static bool tt1_sensLeft_off_isHigh;
static bool tt1_sensRight_on_isHigh;
static bool tt1_sensRight_off_isHigh;
static bool tt2_sensX_on_isHigh;
static bool tt2_sensX_off_isHigh;
static bool tt2_sensLeft_on_isHigh;
static bool tt2_sensLeft_off_isHigh;
static bool tt2_sensRight_on_isHigh;
static bool tt2_sensRight_off_isHigh;

/* Initialize signal flags from hardware. */
void init()
{
  init_();

  do_set_signals();  
  b_sensX_on_isHigh        = M_Pio_Connections_I_pio2_2();  
  b_sensX_off_isHigh       = M_Pio_Connections_I_pio2_2(); 
  b_sensLdrX_on_isHigh     = M_Pio_Connections_I_pio2_5(); 
  b_sensLdrX_off_isHigh    = M_Pio_Connections_I_pio2_5();
  b1_sens1_on_isHigh       = M_Pio_Connections_I_pio2_4();
  b1_sens1_off_isHigh      = M_Pio_Connections_I_pio2_4();
  b1_sens2_on_isHigh       = M_Pio_Connections_I_pio2_7();
  b1_sens2_off_isHigh      = M_Pio_Connections_I_pio2_7();
  b2_sens_on_isHigh        = M_Pio_Connections_I_pio2_6(); 
  b2_sens_off_isHigh       = M_Pio_Connections_I_pio2_6();
  b3_sens1_on_isHigh       = M_Pio_Connections_I_pio0_1(); 
  b3_sens1_off_isHigh      = M_Pio_Connections_I_pio0_1();
  b3_sens2_on_isHigh       = M_Pio_Connections_I_pio0_0();
  b3_sens2_off_isHigh      = M_Pio_Connections_I_pio0_0();
  b5_sens1_on_isHigh       = M_Pio_Connections_I_pio0_2();
  b5_sens1_off_isHigh      = M_Pio_Connections_I_pio0_2(); 
  b5_sens2_on_isHigh       = M_Pio_Connections_I_pio0_5();
  b5_sens2_off_isHigh      = M_Pio_Connections_I_pio0_5();
  b6_sens1_on_isHigh       = M_Pio_Connections_I_pio0_4();
  b6_sens1_off_isHigh      = M_Pio_Connections_I_pio0_4(); 
  b6_sens2_on_isHigh       = M_Pio_Connections_I_pio0_7();
  b6_sens2_off_isHigh      = M_Pio_Connections_I_pio0_7();
  b7_sens_on_isHigh        = M_Pio_Connections_I_pio4_6(); 
  b7_sens_off_isHigh       = M_Pio_Connections_I_pio4_6();
  b8_sens1_on_isHigh       = M_Pio_Connections_I_pio1_1(); 
  b8_sens1_off_isHigh      = M_Pio_Connections_I_pio1_1();
  b8_sens2_on_isHigh       = M_Pio_Connections_I_pio1_0();
  b8_sens2_off_isHigh      = M_Pio_Connections_I_pio1_0();
  b10_sens1_on_isHigh      = M_Pio_Connections_I_pio1_2();
  b10_sens1_off_isHigh     = M_Pio_Connections_I_pio1_2();
  b10_sens2_on_isHigh      = M_Pio_Connections_I_pio1_5();
  b10_sens2_off_isHigh     = M_Pio_Connections_I_pio1_5();
  m1_sensTop_on_isHigh     = M_Pio_Connections_I_pio6_3();
  m1_sensTop_off_isHigh    = M_Pio_Connections_I_pio6_3();
  m1_sensBottom_on_isHigh  = M_Pio_Connections_I_pio6_2();
  m1_sensBottom_off_isHigh = M_Pio_Connections_I_pio6_2();
  m1_sensBack_on_isHigh    = M_Pio_Connections_I_pio6_1();
  m1_sensBack_off_isHigh   = M_Pio_Connections_I_pio6_1();
  m1_sensFront_on_isHigh   = M_Pio_Connections_I_pio6_0();
  m1_sensFront_off_isHigh  = M_Pio_Connections_I_pio6_0();
  m2_sensTop_off_isHigh    = M_Pio_Connections_I_pio6_7();
  m2_sensBottom_on_isHigh  = M_Pio_Connections_I_pio6_6();
  m2_sensBottom_off_isHigh = M_Pio_Connections_I_pio6_6();
  m2_sensBack_on_isHigh    = M_Pio_Connections_I_pio6_5();
  m2_sensBack_off_isHigh   = M_Pio_Connections_I_pio6_5();
  m2_sensFront_on_isHigh   = M_Pio_Connections_I_pio6_4();
  m2_sensFront_off_isHigh  = M_Pio_Connections_I_pio6_4();
  m2_sensRevolv_on_isHigh  = M_Pio_Connections_I_pio4_1();
  m2_sensRevolv_off_isHigh = M_Pio_Connections_I_pio4_1();
  m3_sensTop_on_isHigh     = M_Pio_Connections_I_pio4_0();
  m3_sensTop_off_isHigh    = M_Pio_Connections_I_pio4_0();
  m3_sensBottom_on_isHigh  = M_Pio_Connections_I_pio4_3();
  m3_sensBottom_off_isHigh = M_Pio_Connections_I_pio4_3();
  m4_sensTop_on_isHigh     = M_Pio_Connections_I_pio4_2();
  m4_sensTop_off_isHigh    = M_Pio_Connections_I_pio4_2();  
  m4_sensBottom_on_isHigh  = M_Pio_Connections_I_pio4_5();
  m4_sensBottom_off_isHigh = M_Pio_Connections_I_pio4_5();
  p1_sensBack_on_isHigh    = M_Pio_Connections_I_pio4_7();
  p1_sensBack_off_isHigh   = M_Pio_Connections_I_pio4_7();
  p1_sensFront_on_isHigh   = M_Pio_Connections_I_pio4_4();
  p1_sensFront_off_isHigh  = M_Pio_Connections_I_pio4_4();
  p2_sensBack_on_isHigh    = M_Pio_Connections_I_pio2_0();
  p2_sensBack_off_isHigh   = M_Pio_Connections_I_pio2_0();
  p2_sensFront_on_isHigh   = M_Pio_Connections_I_pio2_1();
  p2_sensFront_off_isHigh  = M_Pio_Connections_I_pio2_1();
  t_sensRight_on_isHigh    = M_Pio_Connections_I_pio5_7();
  t_sensRight_off_isHigh   = M_Pio_Connections_I_pio5_7();
  t_sensLeft_on_isHigh     = M_Pio_Connections_I_pio5_6();
  t_sensLeft_off_isHigh    = M_Pio_Connections_I_pio5_6();
  tt1_sensRight_on_isHigh  = M_Pio_Connections_I_pio5_0();
  tt1_sensRight_off_isHigh = M_Pio_Connections_I_pio5_0();
  tt1_sensLeft_on_isHigh   = M_Pio_Connections_I_pio5_1(); 
  tt1_sensLeft_off_isHigh  = M_Pio_Connections_I_pio5_1();
  tt1_sensX_on_isHigh      = M_Pio_Connections_I_pio5_3(); 
  tt1_sensX_off_isHigh     = M_Pio_Connections_I_pio5_3();
  tt2_sensRight_on_isHigh  = M_Pio_Connections_I_pio5_5();
  tt2_sensRight_off_isHigh = M_Pio_Connections_I_pio5_5();
  tt2_sensLeft_on_isHigh   = M_Pio_Connections_I_pio5_2();
  tt2_sensLeft_off_isHigh  = M_Pio_Connections_I_pio5_2();
  tt2_sensX_on_isHigh      = M_Pio_Connections_I_pio5_4(); 
  tt2_sensX_off_isHigh     = M_Pio_Connections_I_pio5_4();
}


/* Functions for actuator access. */

void 
b_forward()
{
  M_Pio_Connections_O_setpiolow10_5();
  M_Pio_Connections_O_setpiohigh10_4();
  do_process_outputs();
}

void 
b_stop()
{
  M_Pio_Connections_O_setpiohigh10_5();
  M_Pio_Connections_O_setpiohigh10_4();
  do_process_outputs();
}

void 
b1_forward()
{
  M_Pio_Connections_O_setpiolow12_3();
  M_Pio_Connections_O_setpiohigh12_2();
  do_process_outputs();
}

void 
b1_backward()
{
  M_Pio_Connections_O_setpiolow12_3();
  M_Pio_Connections_O_setpiolow12_2();
  do_process_outputs();
}

void 
b1_stop()
{
  M_Pio_Connections_O_setpiohigh12_3();
  M_Pio_Connections_O_setpiohigh12_2();
  do_process_outputs();
}

void 
b2_forward()
{
  M_Pio_Connections_O_setpiolow14_1();
  M_Pio_Connections_O_setpiohigh14_0();
  do_process_outputs();
}

void 
b2_backward()
{
  M_Pio_Connections_O_setpiolow14_1();
  M_Pio_Connections_O_setpiolow14_0();
  do_process_outputs();
}

void 
b2_stop()
{
  M_Pio_Connections_O_setpiohigh14_1();
  M_Pio_Connections_O_setpiohigh14_0();
  do_process_outputs();
}

void 
b3_forward()
{
  M_Pio_Connections_O_setpiolow13_3();
  M_Pio_Connections_O_setpiohigh13_2();
  do_process_outputs();
}

void 
b3_backward()
{
  M_Pio_Connections_O_setpiolow13_3();
  M_Pio_Connections_O_setpiolow13_2();
  do_process_outputs();
}

void 
b3_stop()
{
  M_Pio_Connections_O_setpiohigh13_3();
  M_Pio_Connections_O_setpiohigh13_2();
  do_process_outputs();
}

void 
b4_forward()
{
  M_Pio_Connections_O_setpiolow13_1();
  M_Pio_Connections_O_setpiohigh13_0();
  do_process_outputs();
}

void 
b4_backward()
{
  M_Pio_Connections_O_setpiolow13_1();
  M_Pio_Connections_O_setpiolow13_0();
  do_process_outputs();
}

void 
b4_stop()
{
  M_Pio_Connections_O_setpiohigh13_1();
  M_Pio_Connections_O_setpiohigh13_0();
  do_process_outputs();
}

void 
b5_forward()
{
  M_Pio_Connections_O_setpiolow13_5();
  M_Pio_Connections_O_setpiohigh13_4();
  do_process_outputs();
}

void 
b5_backward()
{
  M_Pio_Connections_O_setpiolow13_5();
  M_Pio_Connections_O_setpiolow13_4();
  do_process_outputs();
}

void 
b5_stop()
{
  M_Pio_Connections_O_setpiohigh13_5();
  M_Pio_Connections_O_setpiohigh13_4();
  do_process_outputs();
}

void 
b6_forward()
{
  M_Pio_Connections_O_setpiolow13_7();
  M_Pio_Connections_O_setpiohigh13_6();
  do_process_outputs();
}

void 
b6_backward()
{
  M_Pio_Connections_O_setpiolow13_7();
  M_Pio_Connections_O_setpiolow13_6();
  do_process_outputs();
}

void 
b6_stop()
{
  M_Pio_Connections_O_setpiohigh13_7();
  M_Pio_Connections_O_setpiohigh13_6();
  do_process_outputs();
}

void 
b7_forward()
{
  M_Pio_Connections_O_setpiolow14_3();
  M_Pio_Connections_O_setpiohigh14_2();
  do_process_outputs();
}

void 
b7_backward()
{
  M_Pio_Connections_O_setpiolow14_3();
  M_Pio_Connections_O_setpiolow14_2();
  do_process_outputs();
}

void 
b7_stop()
{
  M_Pio_Connections_O_setpiohigh14_3();
  M_Pio_Connections_O_setpiohigh14_2();
  do_process_outputs();
}

void 
b8_forward()
{
  M_Pio_Connections_O_setpiolow14_7();
  M_Pio_Connections_O_setpiohigh14_6();
  do_process_outputs();
}

void 
b8_backward()
{
  M_Pio_Connections_O_setpiolow14_7();
  M_Pio_Connections_O_setpiolow14_6();
  do_process_outputs();
}

void 
b8_stop()
{
  M_Pio_Connections_O_setpiohigh14_7();
  M_Pio_Connections_O_setpiohigh14_6();
  do_process_outputs();
}

void 
b9_forward()
{
  M_Pio_Connections_O_setpiolow14_5();
  M_Pio_Connections_O_setpiohigh14_4();
  do_process_outputs();
}

void 
b9_backward()
{
  M_Pio_Connections_O_setpiolow14_5();
  M_Pio_Connections_O_setpiolow14_4();
  do_process_outputs();
}

void 
b9_stop()
{
  M_Pio_Connections_O_setpiohigh14_5();
  M_Pio_Connections_O_setpiohigh14_4();
  do_process_outputs();
}

void 
b10_forward()
{
  M_Pio_Connections_O_setpiolow12_1();
  M_Pio_Connections_O_setpiohigh12_0();
  do_process_outputs();
}

void 
b10_backward()
{
  M_Pio_Connections_O_setpiolow12_1();
  M_Pio_Connections_O_setpiolow12_0();
  do_process_outputs();
}

void 
b10_stop()
{
  M_Pio_Connections_O_setpiohigh12_1();
  M_Pio_Connections_O_setpiohigh12_0();
  do_process_outputs();
}

void 
m1_up()
{
  M_Pio_Connections_O_setpiolow8_7();
  M_Pio_Connections_O_setpiohigh8_6();
  do_process_outputs();
}

void 
m1_down()
{
  M_Pio_Connections_O_setpiolow8_7();
  M_Pio_Connections_O_setpiolow8_6();
  do_process_outputs();
}

void 
m1_stop()
{
  M_Pio_Connections_O_setpiohigh8_7();
  M_Pio_Connections_O_setpiohigh8_6();
  do_process_outputs();
}

void 
m1_t_forward()
{
  M_Pio_Connections_O_setpiolow8_1();
  M_Pio_Connections_O_setpiohigh8_0();
  do_process_outputs();
}

void 
m1_t_backward()
{
  M_Pio_Connections_O_setpiolow8_1();
  M_Pio_Connections_O_setpiolow8_0();
  do_process_outputs();
}

void 
m1_t_stop()
{
  M_Pio_Connections_O_setpiohigh8_1();
  M_Pio_Connections_O_setpiohigh8_0();
  do_process_outputs();
}

void 
m1_d_left()
{
  M_Pio_Connections_O_setpiolow8_5();
  M_Pio_Connections_O_setpiohigh8_4();
  do_process_outputs();
}

void 
m1_d_right()
{
  M_Pio_Connections_O_setpiolow8_5();
  M_Pio_Connections_O_setpiolow8_4();
  do_process_outputs();
}

void 
m1_d_stop()
{
  M_Pio_Connections_O_setpiohigh8_5();
  M_Pio_Connections_O_setpiohigh8_4();
  do_process_outputs();
}

void 
m2_up()
{
  M_Pio_Connections_O_setpiolow22_7();
  M_Pio_Connections_O_setpiohigh22_6();
  do_process_outputs();
}

void 
m2_down()
{
  M_Pio_Connections_O_setpiolow22_7();
  M_Pio_Connections_O_setpiolow22_6();
  do_process_outputs();
}

void 
m2_stop()
{
  M_Pio_Connections_O_setpiohigh22_7();
  M_Pio_Connections_O_setpiohigh22_6();
  do_process_outputs();
}

void 
m2_t_forward()
{
  M_Pio_Connections_O_setpiolow22_1();
  M_Pio_Connections_O_setpiohigh22_0();
  do_process_outputs();
}

void 
m2_t_backward()
{
  M_Pio_Connections_O_setpiolow22_1();
  M_Pio_Connections_O_setpiolow22_0();
  do_process_outputs();
}

void 
m2_t_stop()
{
  M_Pio_Connections_O_setpiohigh22_1();
  M_Pio_Connections_O_setpiohigh22_0();
  do_process_outputs();
}

void 
m2_d_left()
{
  M_Pio_Connections_O_setpiolow22_5();
  M_Pio_Connections_O_setpiohigh22_4();
  do_process_outputs();
}

void 
m2_d_right()
{
  M_Pio_Connections_O_setpiolow22_5();
  M_Pio_Connections_O_setpiolow22_4();
  do_process_outputs();
}

void 
m2_d_stop()
{
  M_Pio_Connections_O_setpiohigh22_5();
  M_Pio_Connections_O_setpiohigh22_4();
  do_process_outputs();
}

void 
m2_revolv_right()
{
  M_Pio_Connections_O_setpiolow22_3();
  M_Pio_Connections_O_setpiohigh22_2();
  do_process_outputs();
}

void 
m2_revolv_left()
{
  M_Pio_Connections_O_setpiolow22_3();
  M_Pio_Connections_O_setpiolow22_2();
  do_process_outputs();
}

void 
m2_revolv_stop()
{
  M_Pio_Connections_O_setpiohigh22_3();
  M_Pio_Connections_O_setpiohigh22_2();
  do_process_outputs();
}

void 
m3_up()
{
  M_Pio_Connections_O_setpiolow8_3();
  M_Pio_Connections_O_setpiohigh8_2();
  do_process_outputs();
}

void 
m3_down()
{
  M_Pio_Connections_O_setpiolow8_3();
  M_Pio_Connections_O_setpiolow8_2();
  do_process_outputs();
}

void 
m3_stop()
{
  M_Pio_Connections_O_setpiohigh8_3();
  M_Pio_Connections_O_setpiohigh8_2();
  do_process_outputs();
}

void 
m4_up()
{
  M_Pio_Connections_O_setpiolow20_3();
  M_Pio_Connections_O_setpiohigh20_2();
  do_process_outputs();
}

void 
m4_down()
{
  M_Pio_Connections_O_setpiolow20_3();
  M_Pio_Connections_O_setpiolow20_2();
  do_process_outputs();
}

void 
m4_stop()
{
  M_Pio_Connections_O_setpiohigh20_3();
  M_Pio_Connections_O_setpiohigh20_2();
  do_process_outputs();
}

void 
m4_d_left()
{
  M_Pio_Connections_O_setpiolow20_1();
  M_Pio_Connections_O_setpiohigh20_0();
  do_process_outputs();
}

void 
m4_d_right()
{
  M_Pio_Connections_O_setpiolow20_1();
  M_Pio_Connections_O_setpiolow20_0();
  do_process_outputs();
}

void 
m4_d_stop()
{
  M_Pio_Connections_O_setpiohigh20_1();
  M_Pio_Connections_O_setpiohigh20_0();
  do_process_outputs();
}

void 
p1_forward()
{
  M_Pio_Connections_O_setpiolow20_5();
  M_Pio_Connections_O_setpiolow20_4();
  do_process_outputs();
}

void 
p1_backward()
{
  M_Pio_Connections_O_setpiolow20_5();
  M_Pio_Connections_O_setpiohigh20_4();
  do_process_outputs();
}

void 
p1_stop()
{
  M_Pio_Connections_O_setpiohigh20_5();
  M_Pio_Connections_O_setpiohigh20_4();
  do_process_outputs();
}

void 
p2_forward()
{
  M_Pio_Connections_O_setpiolow20_7();
  M_Pio_Connections_O_setpiohigh20_6();
  do_process_outputs();
}

void 
p2_backward()
{
  M_Pio_Connections_O_setpiolow20_7();
  M_Pio_Connections_O_setpiolow20_6();
  do_process_outputs();
}

void 
p2_stop()
{
  M_Pio_Connections_O_setpiohigh20_7();
  M_Pio_Connections_O_setpiohigh20_6();
  do_process_outputs();
}

void 
t_left()
{
  M_Pio_Connections_O_setpiolow10_7();
  M_Pio_Connections_O_setpiolow10_6();  
  do_process_outputs();
}

void 
t_right()
{
  M_Pio_Connections_O_setpiolow10_7();
  M_Pio_Connections_O_setpiohigh10_6();  
  do_process_outputs();
}

void 
t_stop()
{
  M_Pio_Connections_O_setpiohigh10_7();
  M_Pio_Connections_O_setpiohigh10_6();  
  do_process_outputs();
}

void 
tt1_b_forward()
{
  M_Pio_Connections_O_setpiolow12_7();
  M_Pio_Connections_O_setpiohigh12_6();
  do_process_outputs();
}

void 
tt1_b_backward()
{
  M_Pio_Connections_O_setpiolow12_7();
  M_Pio_Connections_O_setpiolow12_6();
  do_process_outputs();
}

void 
tt1_b_stop()
{
  M_Pio_Connections_O_setpiohigh12_7();
  M_Pio_Connections_O_setpiohigh12_6();
  do_process_outputs();
}

void 
tt1_left()
{
  M_Pio_Connections_O_setpiolow12_5();
  M_Pio_Connections_O_setpiohigh12_4();
  do_process_outputs();
}

void 
tt1_right()
{
  M_Pio_Connections_O_setpiolow12_5();
  M_Pio_Connections_O_setpiolow12_4();
  do_process_outputs();
}

void 
tt1_stop()
{
  M_Pio_Connections_O_setpiohigh12_5();
  M_Pio_Connections_O_setpiohigh12_4();
  do_process_outputs();
}

void 
tt2_b_forward()
{
  M_Pio_Connections_O_setpiolow10_3();
  M_Pio_Connections_O_setpiohigh10_2();
  do_process_outputs();
}

void 
tt2_b_backward()
{
  M_Pio_Connections_O_setpiolow10_3();
  M_Pio_Connections_O_setpiolow10_2();
  do_process_outputs();
}

void 
tt2_b_stop()
{ 
  M_Pio_Connections_O_setpiohigh10_3();
  M_Pio_Connections_O_setpiohigh10_2();
  do_process_outputs();
}

void 
tt2_left()
{ 
  M_Pio_Connections_O_setpiolow10_1();
  M_Pio_Connections_O_setpiohigh10_0();
  do_process_outputs();
}

void 
tt2_right()
{
  M_Pio_Connections_O_setpiolow10_1();
  M_Pio_Connections_O_setpiolow10_0();
  do_process_outputs();
}

void 
tt2_stop()
{
  M_Pio_Connections_O_setpiohigh10_1();
  M_Pio_Connections_O_setpiohigh10_0();
  do_process_outputs();
}



/* Functions for sensor access. */

// All functions having postfix '_on' return TRUE, if there is a
// rising signal edge (change from 'low' to 'high' state) and FALSE
// otherwise.
//
// All functions having postfix '_off' return TRUE if there is a
// falling signal edge (change from 'high' to 'low' state) and FALSE
// otherwise.

// Sensor for the piecestore transport chain control.
bool
b_sensX_on()
{
  bool result = M_Pio_Connections_I_pio2_2()  &&  ! b_sensX_on_isHigh; 

  b_sensX_on_isHigh = M_Pio_Connections_I_pio2_2();

  return result;
}

bool
b_sensX_off()
{
  bool result = ! M_Pio_Connections_I_pio2_2()  &&  b_sensX_off_isHigh; 
  
  b_sensX_off_isHigh = M_Pio_Connections_I_pio2_2(); 
  
  return result;
}

// Light barrier for piece detection in the piecestore.
bool
b_sensLdrX_on()
{
  bool result = M_Pio_Connections_I_pio2_5()  &&  ! b_sensLdrX_on_isHigh;

  b_sensLdrX_on_isHigh = M_Pio_Connections_I_pio2_5();

  return result;
}

bool
b_sensLdrX_off()
{
  bool result = ! M_Pio_Connections_I_pio2_5()  &&  b_sensLdrX_off_isHigh; 

  b_sensLdrX_off_isHigh = M_Pio_Connections_I_pio2_5();
  
  return result;
}

// Sensor1 for piece detection at Belt1.
bool
b1_sens1_on()
{
  bool result = M_Pio_Connections_I_pio2_4()  &&  ! b1_sens1_on_isHigh; 

  b1_sens1_on_isHigh = M_Pio_Connections_I_pio2_4();

  return result;
}

bool
b1_sens1_off()
{
  bool result = ! M_Pio_Connections_I_pio2_4()  &&  b1_sens1_off_isHigh; 
  
  b1_sens1_off_isHigh = M_Pio_Connections_I_pio2_4();
  
  return result;
}

// Sensor2 for piece detection at Belt1.
bool
b1_sens2_on()
{
  bool result = M_Pio_Connections_I_pio2_7()  &&  ! b1_sens2_on_isHigh; 

  b1_sens2_on_isHigh = M_Pio_Connections_I_pio2_7();

  return result;
}

bool
b1_sens2_off()
{
  bool result = ! M_Pio_Connections_I_pio2_7()  &&  b1_sens2_off_isHigh;

  b1_sens2_off_isHigh = M_Pio_Connections_I_pio2_7();
  
  return result;
}

// Sensor for piece detection at Belt2.
bool
b2_sens_on()
{
  bool result = M_Pio_Connections_I_pio2_6()  &&  ! b2_sens_on_isHigh;

  b2_sens_on_isHigh = M_Pio_Connections_I_pio2_6();

  return result;
}

bool
b2_sens_off()
{
  bool result = ! M_Pio_Connections_I_pio2_6()  &&  b2_sens_off_isHigh;

  b2_sens_off_isHigh = M_Pio_Connections_I_pio2_6();

  return result;
}

// Sensor1 for piece detection at Belt3.
bool
b3_sens1_on()
{
  bool result = M_Pio_Connections_I_pio0_1()  &&  ! b3_sens1_on_isHigh; 
  
  b3_sens1_on_isHigh = M_Pio_Connections_I_pio0_1();

  return result;
}

bool
b3_sens1_off()
{
  bool result = ! M_Pio_Connections_I_pio0_1()  &&  b3_sens1_off_isHigh;  
  
  b3_sens1_off_isHigh = M_Pio_Connections_I_pio0_1(); 

  return result;
}

// Sensor2 for piece detection at Belt3.
bool
b3_sens2_on()
{
  bool result = M_Pio_Connections_I_pio0_0()  &&  ! b3_sens2_on_isHigh; 

  b3_sens2_on_isHigh = M_Pio_Connections_I_pio0_0(); 

  return result;
}

bool
b3_sens2_off()
{
  bool result = ! M_Pio_Connections_I_pio0_0()  &&  b3_sens2_off_isHigh; 

  b3_sens2_off_isHigh = M_Pio_Connections_I_pio0_0();

  return result;
}

// Sensor1 for piece detection at Belt5.
bool
b5_sens1_on()
{
  bool result = M_Pio_Connections_I_pio0_2()  &&  ! b5_sens1_on_isHigh; 

  b5_sens1_on_isHigh = M_Pio_Connections_I_pio0_2();

  return result;
}

bool
b5_sens1_off()
{
  bool result = ! M_Pio_Connections_I_pio0_2()  &&  b5_sens1_off_isHigh;  

  b5_sens1_off_isHigh = M_Pio_Connections_I_pio0_2();
  
  return result;
}

// Sensor2 for piece detection at Belt5.
bool
b5_sens2_on()
{
  bool result = M_Pio_Connections_I_pio0_5()  &&  ! b5_sens2_on_isHigh; 

  b5_sens2_on_isHigh = M_Pio_Connections_I_pio0_5();  
    
  return result;
}

bool
b5_sens2_off()
{
  bool result = ! M_Pio_Connections_I_pio0_5()  &&  b5_sens2_off_isHigh; 

  b5_sens2_off_isHigh = M_Pio_Connections_I_pio0_5(); 

  return result;
}

// Sensor1 for piece detection at Belt6.
bool
b6_sens1_on()
{
  bool result = M_Pio_Connections_I_pio0_4()  &&  ! b6_sens1_on_isHigh; 

  b6_sens1_on_isHigh = M_Pio_Connections_I_pio0_4(); 

  return result;
}

bool
b6_sens1_off()
{
  bool result = ! M_Pio_Connections_I_pio0_4()  &&  b6_sens1_off_isHigh; 

  b6_sens1_off_isHigh = M_Pio_Connections_I_pio0_4();

  return result;
}

// Sensor2 for piece detection at Belt6.
bool
b6_sens2_on()
{
  bool result = M_Pio_Connections_I_pio0_7()  &&  ! b6_sens2_on_isHigh; 

  b6_sens2_on_isHigh = M_Pio_Connections_I_pio0_7();

  return result;
}

bool
b6_sens2_off()
{
  bool result = ! M_Pio_Connections_I_pio0_7()  &&  b6_sens2_off_isHigh;

  b6_sens2_off_isHigh = M_Pio_Connections_I_pio0_7();

  return result;
}

// Sensor for piece detection at Belt7 resp. Pusher1.
bool
b7_sens_on()
{
  bool result = M_Pio_Connections_I_pio4_6()  &&  ! b7_sens_on_isHigh; 

  b7_sens_on_isHigh = M_Pio_Connections_I_pio4_6(); 

  return result;
}

bool
b7_sens_off()
{
  bool result = ! M_Pio_Connections_I_pio4_6()  &&  b7_sens_off_isHigh; 

  b7_sens_off_isHigh = M_Pio_Connections_I_pio4_6(); 

  return result;
}

// Sensor1 for piece detection at Belt8.
bool
b8_sens1_on()
{
  bool result = M_Pio_Connections_I_pio1_1()  &&  ! b8_sens1_on_isHigh;  

  b8_sens1_on_isHigh = M_Pio_Connections_I_pio1_1(); 

  return result;
}

bool
b8_sens1_off()
{
  bool result = ! M_Pio_Connections_I_pio1_1()  &&  b8_sens1_off_isHigh;
 
  b8_sens1_off_isHigh = M_Pio_Connections_I_pio1_1(); 

  return result;
}

// Sensor2 for piece detection at Belt8.
bool
b8_sens2_on()
{
  bool result = M_Pio_Connections_I_pio1_0()  &&  ! b8_sens2_on_isHigh;

  b8_sens2_on_isHigh = M_Pio_Connections_I_pio1_0(); 

  return result;
}

bool
b8_sens2_off()
{
  bool result = ! M_Pio_Connections_I_pio1_0() && b8_sens2_off_isHigh;

  b8_sens2_off_isHigh = M_Pio_Connections_I_pio1_0(); 

  return result;
}

// Sensor1 for piece detection at Belt10.
bool
b10_sens1_on()
{
  bool result = M_Pio_Connections_I_pio1_2()  &&  ! b10_sens1_on_isHigh; 

  b10_sens1_on_isHigh = M_Pio_Connections_I_pio1_2(); 

  return result;
}

bool
b10_sens1_off()
{
  bool result = ! M_Pio_Connections_I_pio1_2() && b10_sens1_off_isHigh; 

  b10_sens1_off_isHigh = M_Pio_Connections_I_pio1_2(); 

  return result;
}

// Sensor2 for piece detection at Belt10.
bool
b10_sens2_on()
{
  bool result = M_Pio_Connections_I_pio1_5()  &&  ! b10_sens2_on_isHigh; 

  b10_sens2_on_isHigh = M_Pio_Connections_I_pio1_5(); 

  return result;
}

bool
b10_sens2_off()
{
  bool result = ! M_Pio_Connections_I_pio1_5()  &&  b10_sens2_off_isHigh; 

  b10_sens2_off_isHigh = M_Pio_Connections_I_pio1_5();

  return result;
}

// Sensor for top vertical move position of Machine1 control.
bool
m1_sensTop_on()
{
  bool result = M_Pio_Connections_I_pio6_3()  &&  ! m1_sensTop_on_isHigh; 

  m1_sensTop_on_isHigh = M_Pio_Connections_I_pio6_3();

  return result;
}

bool
m1_sensTop_off()
{
  bool result = ! M_Pio_Connections_I_pio6_3()  &&  m1_sensTop_off_isHigh; 

  m1_sensTop_off_isHigh = M_Pio_Connections_I_pio6_3();  

  return result;
}

// Sensor for bottom vertical move position of Machine1 control.
bool
m1_sensBottom_on()
{
  bool result = M_Pio_Connections_I_pio6_2()  &&  ! m1_sensBottom_on_isHigh; 

  m1_sensBottom_on_isHigh = M_Pio_Connections_I_pio6_2(); 

  return result;
}

bool
m1_sensBottom_off()
{
  bool result = ! M_Pio_Connections_I_pio6_2()  &&  m1_sensBottom_off_isHigh;
  
  m1_sensBottom_off_isHigh = M_Pio_Connections_I_pio6_2();
  
  return result;
}

// Sensor for back horizontal move position of Machine1 control.
bool
m1_sensBack_on()
{
  bool result = M_Pio_Connections_I_pio6_1()  &&  ! m1_sensBack_on_isHigh; 

  m1_sensBack_on_isHigh = M_Pio_Connections_I_pio6_1(); 

  return result;
}

bool
m1_sensBack_off()
{
  bool result = ! M_Pio_Connections_I_pio6_1()  &&  m1_sensBack_off_isHigh;
  
  m1_sensBack_off_isHigh = M_Pio_Connections_I_pio6_1();
  
  return result;
}

// Sensor for front horizontal move position of Machine1 control.
bool
m1_sensFront_on()
{
  bool result = M_Pio_Connections_I_pio6_0()  &&  ! m1_sensFront_on_isHigh; 

  m1_sensFront_on_isHigh = M_Pio_Connections_I_pio6_0(); 

  return result;
}

bool
m1_sensFront_off()
{
  bool result = ! M_Pio_Connections_I_pio6_0()  &&  m1_sensFront_off_isHigh;
  
  m1_sensFront_off_isHigh = M_Pio_Connections_I_pio6_0();
  
  return result;
}

// Sensor for top vertical move position of Machine2 control.
bool
m2_sensTop_on()
{
  bool result = M_Pio_Connections_I_pio6_7()  &&  ! m2_sensTop_on_isHigh; 

  m2_sensTop_on_isHigh = M_Pio_Connections_I_pio6_7();

  return result;
}

bool
m2_sensTop_off()
{
  bool result = ! M_Pio_Connections_I_pio6_7()  &&  m2_sensTop_off_isHigh; 

  m2_sensTop_off_isHigh = M_Pio_Connections_I_pio6_7();  

  return result;
}

// Sensor for bottom vertical move position of Machine2 control.
bool
m2_sensBottom_on()
{
  bool result = M_Pio_Connections_I_pio6_6()  &&  ! m2_sensBottom_on_isHigh; 

  m2_sensBottom_on_isHigh = M_Pio_Connections_I_pio6_6(); 

  return result;
}

bool
m2_sensBottom_off()
{
  bool result = ! M_Pio_Connections_I_pio6_6()  &&  m2_sensBottom_off_isHigh;
  
  m2_sensBottom_off_isHigh = M_Pio_Connections_I_pio6_6();
  
  return result;
}

// Sensor for back horizontal move position of Machine2 control.
bool
m2_sensBack_on()
{
  bool result = M_Pio_Connections_I_pio6_5()  &&  ! m2_sensBack_on_isHigh; 

  m2_sensBack_on_isHigh = M_Pio_Connections_I_pio6_5(); 

  return result;
}

bool
m2_sensBack_off()
{
  bool result = ! M_Pio_Connections_I_pio6_5()  &&  m2_sensBack_off_isHigh;
  
  m2_sensBack_off_isHigh = M_Pio_Connections_I_pio6_5();
  
  return result;
}

// Sensor for front horizontal move position of Machine2 control.
bool
m2_sensFront_on()
{
  bool result = M_Pio_Connections_I_pio6_4()  &&  ! m2_sensFront_on_isHigh; 

  m2_sensFront_on_isHigh = M_Pio_Connections_I_pio6_4(); 

  return result;
}

bool
m2_sensFront_off()
{
  bool result = ! M_Pio_Connections_I_pio6_4()  &&  m2_sensFront_off_isHigh;
  
  m2_sensFront_off_isHigh = M_Pio_Connections_I_pio6_4();
  
  return result;
}

// Sensor for drilltool revolver movement of Machine2 control.
bool
m2_sensRevolv_on()
{
  bool result = M_Pio_Connections_I_pio4_1()  &&  ! m2_sensRevolv_on_isHigh; 

  m2_sensRevolv_on_isHigh = M_Pio_Connections_I_pio4_1(); 

  return result;
}

bool
m2_sensRevolv_off()
{
  bool result = ! M_Pio_Connections_I_pio4_1()  &&  m2_sensRevolv_off_isHigh;
  
  m2_sensRevolv_off_isHigh = M_Pio_Connections_I_pio4_1();
  
  return result;
}

// Sensor for top vertical move position of Machine3 control.
bool
m3_sensTop_on()
{
  bool result = M_Pio_Connections_I_pio4_0()  &&  ! m3_sensTop_on_isHigh; 

  m3_sensTop_on_isHigh = M_Pio_Connections_I_pio4_0();

  return result;
}

bool
m3_sensTop_off()
{
  bool result = ! M_Pio_Connections_I_pio4_0()  &&  m3_sensTop_off_isHigh; 

  m3_sensTop_off_isHigh = M_Pio_Connections_I_pio4_0();  

  return result;
}

// Sensor for bottom vertical move position of Machine3 control.
bool
m3_sensBottom_on()
{
  bool result = M_Pio_Connections_I_pio4_3()  &&  ! m3_sensBottom_on_isHigh; 

  m3_sensBottom_on_isHigh = M_Pio_Connections_I_pio4_3(); 

  return result;
}

bool
m3_sensBottom_off()
{
  bool result = ! M_Pio_Connections_I_pio4_3()  &&  m3_sensBottom_off_isHigh;
  
  m3_sensBottom_off_isHigh = M_Pio_Connections_I_pio4_3();
  
  return result;
}

// Sensor for top vertical move position of Machine4 control.
bool
m4_sensTop_on()
{
  bool result = M_Pio_Connections_I_pio4_2()  &&  ! m4_sensTop_on_isHigh; 

  m4_sensTop_on_isHigh = M_Pio_Connections_I_pio4_2();  

  return result;
}

bool
m4_sensTop_off()
{
  bool result = ! M_Pio_Connections_I_pio4_2()  &&  m4_sensTop_off_isHigh; 
  
  m4_sensTop_off_isHigh = M_Pio_Connections_I_pio4_2(); 
  
  return result;
}

// Sensor for bottom vertical move position of Machine4 control.
bool
m4_sensBottom_on()
{
  bool result = M_Pio_Connections_I_pio4_5()  &&  ! m4_sensBottom_on_isHigh; 

  m4_sensBottom_on_isHigh = M_Pio_Connections_I_pio4_5(); 

  return result;
}

bool
m4_sensBottom_off()
{
  bool result = ! M_Pio_Connections_I_pio4_5()  &&  m4_sensBottom_off_isHigh;
  
  m4_sensBottom_off_isHigh = M_Pio_Connections_I_pio4_5();

  return result;
}

// Sensor for back move postion of Pusher1 control.
bool
p1_sensBack_on()
{
  bool result = M_Pio_Connections_I_pio4_7()  &&  ! p1_sensBack_on_isHigh;

  p1_sensBack_on_isHigh = M_Pio_Connections_I_pio4_7();

  return result;
}

bool
p1_sensBack_off()
{
  bool result = ! M_Pio_Connections_I_pio4_7()  &&  p1_sensBack_off_isHigh;

  p1_sensBack_off_isHigh = M_Pio_Connections_I_pio4_7();

  return result;
}

// Sensor for front move postion of Pusher1 control.
bool
p1_sensFront_on()
{
  bool result = M_Pio_Connections_I_pio4_4()  &&  ! p1_sensFront_on_isHigh;

  p1_sensFront_on_isHigh = M_Pio_Connections_I_pio4_4();

  return result;
}

bool
p1_sensFront_off()
{
  bool result = ! M_Pio_Connections_I_pio4_4()  &&  p1_sensFront_off_isHigh;

  p1_sensFront_off_isHigh = M_Pio_Connections_I_pio4_4();

  return result;
}

// Sensor for back move postion of Pusher2 control.
bool
p2_sensBack_on()
{
  bool result = M_Pio_Connections_I_pio2_0()  &&  ! p2_sensBack_on_isHigh;

  p2_sensBack_on_isHigh = M_Pio_Connections_I_pio2_0();

  return result;
}

bool
p2_sensBack_off()
{
  bool result = ! M_Pio_Connections_I_pio2_0()  &&  p2_sensBack_off_isHigh;

  p2_sensBack_off_isHigh = M_Pio_Connections_I_pio2_0();

  return result;
}

// Sensor for front move postion of Pusher2 control.
bool
p2_sensFront_on()
{
  bool result = M_Pio_Connections_I_pio2_1()  &&  ! p2_sensFront_on_isHigh;

  p2_sensFront_on_isHigh = M_Pio_Connections_I_pio2_1();

  return result;
}

bool
p2_sensFront_off()
{
  bool result = ! M_Pio_Connections_I_pio2_1()  &&  p2_sensFront_off_isHigh;

  p2_sensFront_off_isHigh = M_Pio_Connections_I_pio2_1();

  return result;
}

// Sensor for left move postion of PusherTable control.
bool
t_sensLeft_on()
{
  bool result = M_Pio_Connections_I_pio5_6()  &&  ! t_sensLeft_on_isHigh;

  t_sensLeft_on_isHigh = M_Pio_Connections_I_pio5_6();

  return result;
}

bool
t_sensLeft_off()
{
  bool result = ! M_Pio_Connections_I_pio5_6()  &&  t_sensLeft_off_isHigh;

  t_sensLeft_off_isHigh = M_Pio_Connections_I_pio5_6();

  return result;
}

// Sensor for right move postion of PusherTable control.
bool
t_sensRight_on()
{
  bool result = M_Pio_Connections_I_pio5_7()  &&  ! t_sensRight_on_isHigh;

  t_sensRight_on_isHigh = M_Pio_Connections_I_pio5_7();

  return result;
}

bool
t_sensRight_off()
{
  bool result = ! M_Pio_Connections_I_pio5_7()  &&  t_sensRight_off_isHigh;

  t_sensRight_off_isHigh = M_Pio_Connections_I_pio5_7();

  return result;
}

// Sensor for piece detection at belt of TurnTable1.
bool
tt1_sensX_on()
{
  bool result = M_Pio_Connections_I_pio5_3()  &&  ! tt1_sensX_on_isHigh; 

  tt1_sensX_on_isHigh = M_Pio_Connections_I_pio5_3(); 

  return result;
}

bool
tt1_sensX_off()
{
  bool result = ! M_Pio_Connections_I_pio5_3()  &&  tt1_sensX_off_isHigh; 
  
  tt1_sensX_off_isHigh = M_Pio_Connections_I_pio5_3();

  return result;
}

// Sensor for left turn position of TurnTable1 control.
bool
tt1_sensLeft_on()
{
  bool result = M_Pio_Connections_I_pio5_1()  &&  ! tt1_sensLeft_on_isHigh; 

  tt1_sensLeft_on_isHigh = M_Pio_Connections_I_pio5_1(); 
  
  return result;
}

bool
tt1_sensLeft_off()
{
  bool result = ! M_Pio_Connections_I_pio5_1()  &&  tt1_sensLeft_off_isHigh; 

  tt1_sensLeft_off_isHigh = M_Pio_Connections_I_pio5_1();

  return result;
}

// Sensor for right turn position of TurnTable1 control.
bool
tt1_sensRight_on()
{
  bool result = M_Pio_Connections_I_pio5_0()  &&  ! tt1_sensRight_on_isHigh; 

  tt1_sensRight_on_isHigh = M_Pio_Connections_I_pio5_0();

  return result;
}

bool
tt1_sensRight_off()
{
  bool result = ! M_Pio_Connections_I_pio5_0()  &&  tt1_sensRight_off_isHigh;

  tt1_sensRight_off_isHigh = M_Pio_Connections_I_pio5_0();

  return result;
}


bool
tt2_sensX_on()
{
  bool result = M_Pio_Connections_I_pio5_4()  &&  ! tt2_sensX_on_isHigh; 

  tt2_sensX_on_isHigh = M_Pio_Connections_I_pio5_4();
  
  return result;
}

bool
tt2_sensX_off()
{
  bool result = ! M_Pio_Connections_I_pio5_4()  &&  tt2_sensX_off_isHigh; 
  
  tt2_sensX_off_isHigh = M_Pio_Connections_I_pio5_4();

  return result;
}

// Sensor for left turn position of TurnTable2 control.
bool
tt2_sensLeft_on()
{
  bool result = M_Pio_Connections_I_pio5_2()  &&  ! tt2_sensLeft_on_isHigh;

  tt2_sensLeft_on_isHigh = M_Pio_Connections_I_pio5_2();
  
  return result;
}

bool
tt2_sensLeft_off()
{
  bool result = ! M_Pio_Connections_I_pio5_2()  &&  tt2_sensLeft_off_isHigh;

  tt2_sensLeft_off_isHigh = M_Pio_Connections_I_pio5_2();

  return result;
}

// Sensor for right turn position of TurnTable2 control.
bool
tt2_sensRight_on()
{
  bool result = M_Pio_Connections_I_pio5_5()  &&  ! tt2_sensRight_on_isHigh;

  tt2_sensRight_on_isHigh = M_Pio_Connections_I_pio5_5();
  
  return result;
}

bool
tt2_sensRight_off()
{
  bool result = ! M_Pio_Connections_I_pio5_5()  &&  tt2_sensRight_off_isHigh;
  
  tt2_sensRight_off_isHigh = M_Pio_Connections_I_pio5_5();
  
  return result;
}

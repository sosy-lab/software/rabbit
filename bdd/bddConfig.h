//-*-Mode: C++;-*-

/*
 * File:        bddConfig.h
 * Purpose:     Set of configurations represented by a BDD
 * Author:      Andreas Noack
 * Created:     2000
 * Copyright:   (c) 2000, Brandenburg Technical University of Cottbus
 */

#ifndef _bddConfig_h_
#define _bddConfig_h_

#include "reprConfig.h"
#include "bddBdd.h"
#include "bddSymTab.h"
#include "bddExpression.h"

// Set of configurations represented by a BDD.

// It is possible that there exist bit vectors which are no valid encodings
//   of configurations (because the cardinalities of ranges 
//   of discrete variables and clocks do not need to be powers of 2).
//   For speed, these out-of-range bit vectors are not generally excluded from
//   representation. 
//   The bddVarInfo::maxValue of a variable or automaton is now
//   represented by all values maxValue...2**bitNr-1.
//   This invariant keeps the comparison operations 
//   (setEqual, setContains, isEmpty) correct, even without mkRange.
//   Methods for output (print) and config counts (getConfigNr) have to
//   restrict to the set of all range configurations before starting.
//   The operation mkRange returns the set of all range configurations.

// Comment for the operations mkEqual, mkExists, unTick, intersectAndUnTick:
//   The bdd variable ids (bddNode::var) of bits of unticked variables are
//   multiples of bddBdd::mUnTickMult, starting with mUnTickedMult (not 0!).
//   The bdd variable id of the corresponding ticked variable bit
//   is 1 higher (-> has rest 1 modulo bddBdd::mUntickMult).

class bddConfig : public reprConfig
{

private:
  // It should be not allowed to use the standard operators.
  // Uses the generated copy constructor. 
  // Uses the standard operator '='.
  void operator,(const bddConfig&);

private: // Private static methods.

  // Returns all configs satisfying pVI == pConst.
  static bddConfig
  mkEqualPure(const bddSymTab* pSymTab,
	      bddVarInfo pVI, 
	      reprNUMBER pConst);

  // Returns all configs satisfying pVI == pConst.
  //   If (pConst) is the max value of pVI (pVI.maxValue)
  //   then the completion for out-of-range-values is applied.
  //   This is to fulfill the invariant of bddConfig 
  //   (see comment on top of bddConfig.h).
  static bddConfig
  mkEqual(const bddSymTab* pSymTab,
	  bddVarInfo pVI, 
	  reprNUMBER pConst);
  
  // Return set of all range configuration encodings.
  //   Only for output (print) and config counts (getConfigNr).
  static bddConfig
  mkRange(const bddSymTab* pSymTab);

  // Return all configs satisfying pE1 < pE2. See mkEqual.
  static bddConfig
  mkLess(const bddSymTab* pSymTab,
	 const bddExpression* pE1, 
	 const bddExpression* pE2);

public: // Public static methods.

  // Returns all configs satisfying pE1 == pE2.
  // Warning: The maximum value of a variable range
  //   represents  a l l  values greater or equal to the maximum.
  //   Example: Variable x has range 0..2, y has range 0..3. Let x==2, y==3.
  //     Then x == y, x < y and y > x is true!
  //     This is needed (e.g.) for time steps: 
  //       x' == x + 1 has to be true for x == 2 and x' == 2.
  //   So be careful with computing complements of sets returned here.
  static bddConfig
  mkEqual(const bddSymTab* pSymTab,
	  const bddExpression* pE1, 
	  const bddExpression* pE2);
  
  // Returns all configs satisfying pE1 > pE2.
  static bddConfig
  mkGreater(const bddSymTab* pSymTab,
	    const bddExpression* pE1,
	    const bddExpression* pE2);

  // Return all configs where the (ticked or unticked, according to (pTicked))
  //   state of automaton (pAutoName) is (pStateName).
  static bddConfig
  mkState(const bddSymTab* pSymTab,
	  const string* pAutoName,
	  const string* pStateName,
	  bool pTicked);

  // Return the transition relation that changes nothing.
  //   (i.e. x == x' for all states and variables).
  static bddConfig
  mkInitiated(const bddSymTab* pSymTab);

private: // Attributes.

  // Symbol table (associated).
  const bddSymTab* mSymTab;
  // Set of configurations.
  bddBdd mBdd;


private:
  // Forbid implicite casts.
  bddConfig(void*);

public: // Constructors and destructor.
  
  bddConfig(const bddSymTab* pSymTab, bool full = false)
    : mSymTab(pSymTab),
      mBdd(full)
  {}

  bddConfig (const bddSymTab* pSymTab, const bddBdd& pBdd)
    : mSymTab(pSymTab),
      mBdd(pBdd)
  {}

public: // Accessors.

  ddmConfig*
  getDerivedObjDdm()
  {
    cerr << "Runtime error: Wrong derived object expected." << endl;
    return NULL;
  }

  const ddmConfig*
  getDerivedObjDdm() const
  {
    cerr << "Runtime error: Wrong derived object expected." << endl;
    return NULL;
  }

  bddConfig*
  getDerivedObjBdd()
  {
    return this;
  }

  const bddConfig*
  getDerivedObjBdd() const
  {
    return this;
  }

  // Return number of discrete configurations.
  double
  getConfigNr()
  {
    // First exclude out-of-range bit vectors 
    //   (see comment at top of the class).
    bddConfig lTmp = *this;
    lTmp.intersect(mkRange(mSymTab));
    return lTmp.mBdd.getStateNr
      (bddBdd::mUnTickMult*mSymTab->getUsedBitNr());
  }

  // Return node number of BDD (terminal nodes not counted).
  unsigned
  getBddSize()
  {
    return mBdd.getNodeNr();
  }

private: // Private convenience methods.

  // Existential quantification of (pVI).
  bddConfig
  mkExists(bddVarInfo pVI) const;

public: // Service methods.

  // Because we are not able to use the copy constructor
  //   in a polymorph way, we need a clone method.
  reprConfig*
  clone() const
  {
    return new bddConfig(*this);
  }

  bool
  setEqual(const reprConfig& p) const
  {
    return mBdd.setEqual( p.getDerivedObjBdd()->mBdd );
  }

  // Check if (*this) contains (p).
  bool
  setContains(const reprConfig& p) const
  {
    return mBdd.setContains( p.getDerivedObjBdd()->mBdd );
  }
  
  bool
  isEmpty() const
  {
    return mBdd.isEmpty();
  }
  
  reprConfig*
  mkComplement() const
  {
    bddConfig* lResult = new bddConfig(*this);
    lResult->mBdd.complement();
    return lResult;
  }

  void
  complement()
  {
    mBdd.complement();
  }

  void
  unite(const reprConfig& p) 
  {
    mBdd.unite(dynamic_cast<const bddConfig*>(&p)->mBdd);
  }

  void
  intersect(const reprConfig& p)
  {
    mBdd.intersect(dynamic_cast<const bddConfig*>(&p)->mBdd);
  }

  void
  setSubtract(const reprConfig& p)
  {
    mBdd.setSubtract(dynamic_cast<const bddConfig*>(&p)->mBdd);
  }

  // Existential quantification of (pIdentifier)
  //   which must be a variable.
  // We have to extend this method for automata!
  // (pAutomaton) is not needed, but the interface is given by (reprConfig). 
  void
  exists(const reprAutomaton* pAutomaton, const ctaString* pIdentifier)
  {
    // We should decide whether (pIdentifier) is a variable or an automaton,
    //  and call the right method, use (mSymTab) for this. 

    existsVar(pIdentifier, false);
  }

  // Existential quantification of (pTicked ? pVariable : pVariable').
  void
  existsVar(const ctaString* pVariable, bool pTicked)
  {
    bddVarInfo lVI = mSymTab->getVarInfo(*pVariable);
    lVI.ticked = pTicked;
    *this = mkExists(lVI);
  }

  // Existential quantification of (pTicked ? pAuto : pAuto').
  void
  existsAuto(const ctaString* pAuto, bool pTicked)
  {
    bddVarInfo lVI = mSymTab->getAutoInfo(*pAuto);
    lVI.ticked = pTicked;
    *this = mkExists(lVI);
  }

  // Existential quantification of all local variables and automata
  //   of the module (pModule) and its submodules.
  // Needed by simulation check.
  void
  existsModule(const ctaString& pModule, bool pTicked);

  // Existential quantification of unticked variables and renaming
  //   of ticked variables to unticked variables.
  void
  unTick() 
  {
    mBdd.unTick();
  }
  
  // Forward image computation:
  // Intersect with p and unTick() the result.
  void
  intersectAndUnTick(const reprConfig& p)
  {
    mBdd.intersectAndUnTick(dynamic_cast<const bddConfig*>(&p)->mBdd);
  }
  
  // Decrement variable ids of all bdd nodes.
  void
  decVars() 
  {
    mBdd.decVars();
  }
  
  // Increment variable ids of all bdd nodes.
  void
  incVars() 
  {
    mBdd.incVars();
  }
  
  // Compute initiated relation for the synchronisation of two
  //   transition relations with the initiated relations (*this) and (pConfig).
  void
  combineInitiated(const bddConfig& pConfig);

public: // IO.

  // Print set of configs.
  // If the set of configs depends on ticked variables or states
  //   (eg transition relation), (pPrintTicked) should be true. 
  //   Else printing of ticked states or vars can be avoided by 
  //   setting (pPrintTicked) to false.
  void 
  print(ostream& pS, bool pPrintTicked) const;

  // aheinig.
  void 
  printGraphicalBddVisualization(ostream& pS) const;

  static void
  printBddSizeInfo()
  {
    cerr << "Overall number of BDD nodes: " << bddBdd::getReachNodeNr() << endl
	 << "Number of external references: " << bddBdd::getExtRefNr() << endl;
  }
};

#endif // _bddConfig_h_

//-*-Mode: C++;-*-

/*
 * File:        bddAutomaton.h
 * Purpose:     Closed Timed Automaton with integer semantics
 * Author:      Andreas Noack
 * Created:     2000
 * Copyright:   (c) 2000, Brandenburg Technical University of Cottbus
 */

#ifndef _bddAutomaton_h_
#define _bddAutomaton_h_

#include <string>
#include <vector>
#include <queue>

#include "ctaIntSet.h"
#include "reprAutomaton.h"
#include "bddConfig.h"
#include "bddTransition.h"
#include "bddSymTab.h"

class bddAutomaton : public reprAutomaton
{
private:
  // It should be not allowed to use the standard operator ','.
  void operator,(const bddAutomaton&);

private: // Attributes.

  // Set of initial configurations.
  bddConfig mInitial;
  // Union of state invariants.
  bddConfig mInvariant;
  // Discrete Transitions.
  vector<bddTransition> mTransitions;
  // Time step transition (increments all clocks).
  bddConfig mTimeStep;
  // Set of synchronization labels.
  ctaIntSet mSyncSet;
  // Symbol table. Aggregated if (mProduct == true), else associated.
  const bddSymTab* mSymTab;
  bool mProduct;

  // This is added for refinement check (isSimulation).
  //   This map only has content at execution time of the
  //   method (isSimulation()).
  map<int, bddTransition*> mVisibleTrans;

public:
  // A global flag for communication between the signal 
  //  handler and the main loop.
  static bool mSimulationIsAborted;

public: // Constructor and destructor.  

  bddAutomaton(const bddConfig& pInvariant,
	       const vector<bddTransition>& pTransitions,
	       const ctaIntSet& pSyncSet,
	       const bddSymTab* pSymTab,
	       bool pProduct)
    : mInitial(pSymTab, false),
      mInvariant(pInvariant),
      mTransitions(pTransitions),
      mTimeStep(pSymTab, false),
      mSyncSet(pSyncSet),
      mSymTab(pSymTab),
      mProduct(pProduct)
  {}

  ~bddAutomaton()
  {
    if (mProduct)
    {
      delete mSymTab;
    } 
  }

public: // Accessors.
    
  ddmAutomaton*
  getDerivedObjDdm()
  {
    cerr << "Runtime error: Wrong derived object expected." << endl;
    return NULL;
  }

  const ddmAutomaton*
  getDerivedObjDdm() const
  {
    cerr << "Runtime error: Wrong derived object expected." << endl;
    return NULL;
  }

  bddAutomaton*
  getDerivedObjBdd()
  {
    return this;
  }

  const bddAutomaton*
  getDerivedObjBdd() const
  {
    return this;
  }

  void
  setInitial(const bddConfig& pInitial)
  {
    mInitial = pInitial;
  }

  const reprConfig&
  getInitial() const
  {
    return mInitial;
  }

  void 
  setTimeStep(const bddConfig& pTimeStep)
  {
    mTimeStep = pTimeStep;
  }

  // Needed in (anaRegLinConstraint) and (anaRegState) for compatibility
  //   of bdd and ddm-interfaces.
  const bddSymTab*
  getSymTab() const
  {
    return mSymTab;
  }

private: // Private convenience methods.
  
  // Compute the fixed point of the config set (pStates)
  //   regarding the transitions in (pTransitions).
  void
  computeFixPnt(bddConfig* const pStates,
		const vector<bddTransition>* const pTransitions) const;


  // The following three methods are needed in (isSimulation()).

  // Computes the transition relation for the visilbe transitions
  //   (synchronized by the labels from (pVisibleSyncs) and
  //   stores it in (mVisibleTrans).
  // (pVisibleSyncs) is the set of synchronization labels for
  //   which P and P||Q must synchronize (this automaton is one of them).
  // Invisible transitions are transitions labelled with
  //   synchronization labels not visible outside the module.
  void
  computeVisibleTransitions(const ctaIntSet& pVisibleSyncs);

  // To deallocate memory after simulation check.
  void
  deleteVisibleTransitions();

  // Computes the follower config using the 'visible' transition
  //   labelled with (pSync) and intersects it to (pConf).
  void
  bddAutomaton::visibleFollowers(bddConfig& pConf, int pSync);

  void
  productUniteOnlySyncs(bddAutomaton* pAuto);

  void
  productUniteAllTransitionsOfAnAutomaton(bddAutomaton* pAuto, 
				          bool firstProduct);
  
  void
  synchronizeSyncTransitionOfTwoAutomata(bddAutomaton* pAuto,
					 vector<bddTransition>* pTransition);

  // For controller simulation.

  // Initialize function map for output.
  //   (pOutputFuncMap) is in/out parameter.
  void 
  initOutputFuncMap(map<string, void (*)()>& pOutputFuncMap) const; 

  // Initialize function map for input.
  //   (pInputFuncMap) is in/out parameter.
  void 
  initInputFuncMap(map<string, bool (*)()>& pInputFuncMap) const;

  void
  processInput(map<string, bool (*)()>& pInputFuncMap,
	       queue<int>& pQueue) const;
  void
  processInput(int lSockfd, queue<int>& pQueue) const; 
  
  int
  initSockets() const;

public: // Service methods.
 
  // Construct the product automaton of (*this) and (pAuto).
  void
  product(bddAutomaton* pAuto, bool firstProduct);

  void
  mkUnionOfAllTransitions();

  void 
  mkTransitiveClosureOfEveryTransition();

  void
  printAllSyncForGraphicalVisualization();
  
  void
  randomDiskreteTransitionOrder();

  // Return all configurations reachable within (pSteps) time units.
  //   If a fixpoint is reached, a value >= 0 is returned in (pSteps), else -1.
  // Only implemented for (pTag) == mFORWARD.
  reprConfig*
  mkReachable(reprAutomaton_ReachDir pTag,
	      const reprConfig* const pConfig,
	      int& pSteps) const;

  // Returns true iff it is possible to reach (pError) from (pInitial).
  bool
  isReachable(const reprConfig* const pInitial, 
	      const reprConfig* const pError) const;

  // Returns true iff automaton (this) simulates automaton (lP).
  //   For given automata P and Q we want to check whether
  //   P is a refinement (or: implementation) of Q
  //   which is a more abstract version of P (has more traces).
  // We have to build the bddAutomaton (product) for P||Q and
  //   the bddAutomaton for P. Then we can call
  //   P||Q.isSimulation(P, syncSet).
  // (pVisibleSyncs) is the set of synchronization labels for
  //   which P and P||Q must synchronize.
  //   We use weak simulation after [Weise&Lenzkes1997].
  bool
  isSimulation(bddAutomaton* lP,
	       ctaIntSet& pVisibleSyncs);

  reprConfig*
  mkEmptyConfig() const
  {
    return new bddConfig(mSymTab, false);
  }

  reprAutomatonOstream
  mkAutomatonOstream(ostream& pS) const
  {
    return reprAutomatonOstream(&pS);
  }

  // Check if this automaton allows to sync on the foreign set (pForeign)
  //   if the own transition carries the label set (pOwn).
  bool
  allowsForeignSync(const int pForeign, const int pOwn) const;
  
  // Check if (*this) must sync on the given label.
  bool
  mustSync(const int pSync) const
  {
    return (mSyncSet.find(pSync) != mSyncSet.end());
  }
  
public: // IO.
  
  void 
  print(ostream& pS) const;

  // Print the set of configurations (pConfig).
  void 
  print(ostream& pS, const reprConfig* pConfig) const 
  {
    pConfig->getDerivedObjBdd()->print(pS, false);
  } 

  // Print the bdd sizes of the members.
  void 
  printBddSize(ostream& pS);

  // Simulates the CTA model at the real environment.
  void 
  simulateCtaModel(ctaMap<ctaComponent*>* pComp) const;

  // Simulates the CTA model at the real environment.
  void 
  simulateCtaModelWithSockets(ctaMap<ctaComponent*>* pComp) const;
};

#endif // _bddAutomaton_h_

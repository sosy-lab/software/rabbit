//-*-Mode: C++;-*-

/*
 * File:        bddBdd.h
 * Purpose:     Shared Binary Decision Diagram Package
 * Author:      Andreas Noack
 * Created:     2000
 * Copyright:   (c) 2000, Brandenburg Technical University of Cottbus
 */

#ifndef _bddBdd_h
#define _bddBdd_h

#include <vector>
#include <set>

#include "reprObject.h"
#include "bddSymTab.h"

// BDD node
struct bddNode 
{
  // To mark the node in garbage collection and node count
  unsigned mark:1; 
  // Variable id.
  unsigned var:31; 
  // Index of low son. Also used for linking unused nodes in the node array.
  unsigned low;    
  // Index of high son.
  unsigned high;
  // Next node in mUniqueHash-list (0 for end of list).
  unsigned next;
};

// Cache entry for binary operations
struct bddBinEntry 
{  
  // Identifier of the operation
  unsigned op;     
  // First argument of the operation
  unsigned root1;
  // Second argument of the operation
  unsigned root2;
  // Result of the operation
  unsigned result;
};

// Cache entry for counting states
struct bddStatEntry 
{
  // Argument of the operation
  unsigned root;
  // Result of the operation
  double result;  
};

// One Binary Decision Diagram
// and static data structures of the whole Shared BDD package
class bddBdd : private reprObject
{
public: // Constant.

  // The ids (bddNode::var) of unticked variables are multiples
  //   of mUnTickMult, starting with mUnTickedMult (not 0!).  
  // The identifier of the corresponding 
  //   ticked variable is 1 higher (-> has rest 1 modulo mUntickMult).
  // Because some additional space for multiplying transition relations
  //   should be reserved, mUnTickMult should be at least 4.
  // For efficiency of unTick() and intersectAndUnTick()
  //   it should be a power of 2.

  /* mvogel 2001-04-18: 'static const' added for linux-gnu compiler */
  static const unsigned mUnTickMult = 4;

private: // Constants.

  // Identifiers of the operations in the cache (mBinCache).
  enum { mComplement = 1,  mUnTick, mDecVars, mIncVars, mExists,
	 mUnite, mIntersect, mIntersectAndUnTick, mSubtract, mSetContains };

private: // Static attributes.

  // Node Array.
  // mNodes[0] is the 0-terminal, mNodes[1] is the 1-terminal.
  // Both are always marked (mark == 1) and have the variable id -1.
  static bddNode* mNodes;
  // Number of elements of mNodes.
  static unsigned mMaxNodeNr;
  // Index of the first unused node in mNodes. 
  // Unused nodes are linked using their low-element.
  static unsigned mFree;

  // Indices of externally (i.e. by the package user) reference nodes.
  // Updated by constructors and destructors. Does not contain terminals.
  // Used in garbage collections to recognise live nodes.
  static multiset<unsigned> mExtRefs;

  // Hash table of all used nodes.
  // Used by insert to ensure that mNodes contains no two equal nodes.
  // mUniqueHash[i] contains the index of first node of a list of all nodes 
  // with hash value i. Lists are linked by the next-element of the nodes.
  static unsigned* mUniqueHash;
  // Number of elements of mUniqueHash == 2^mUniqueHBitNr == 1<<mUniqueHBitNr.
  static unsigned mUniqueHBitNr;
  // Cache for the results of binary operations.
  static bddBinEntry* mBinCache;
  // Number of elements of mBinCache == 2^mBinCBitNr.
  static unsigned mBinCBitNr;
  // Cache for the results of getStateNr().
  static bddStatEntry* mStatCache;
  // Number of elements of mStatCache == 2^mStatCBitNr.
  static unsigned mStatCBitNr;

private: // Private static methods.

  // Hash/cache functions with 1 and 3 arguments.
  // Possible return values: 0..(1<<hashBitNr)-1.
  static inline unsigned 
  hash(unsigned p1, unsigned pHashBitNr);
  static inline unsigned 
  hash(unsigned p1, unsigned p2, unsigned p3, unsigned pHashBitNr);

  // Marks (mark=1) all nodes of the BDD with the root pRoot.
  static void 
  mark(unsigned pRoot);
  // Unmarks (mark=0) all nodes of the BDD with the root pRoot.
  // Terminal always remain marked.
  static void 
  unMark(unsigned pRoot);

  // Garbage collection: All dead nodes (i.e. all nodes which are not reachable
  // from a node in mExtRefs) are freed (i.e. inserted into unused-list mFree).
  // Terminal nodes are never freed.
  static void 
  gc();

  // Returns the mNodes-index of the node with the passed var-, low- and
  // high-values. If such node does not exists, it is inserted into
  // mNodes and mUniqueHash. If there are no free nodes left, throws exception.
  static unsigned 
  insert(unsigned pVar, unsigned pLow, unsigned pHigh);

  // Returns number of represented states of the BDD with root pRoot.
  // Only unticked variables (i.e. variables with ids divisible 
  //   by (mUnTickMult)) with ids >= mUnTickMult are considered.
  // The BDD must not contain nodes with other variable ids.
  // pMaxVar is the maximum id of an unticked variable.
  static double 
  getStateNr_(unsigned pRoot, unsigned pVar, unsigned pMaxVar);
  // Returns number of nodes of the BDD with root pRoot.
  // (Terminal nodes are not counted). Side effect: counted nodes are marked.
  static unsigned 
  getNodeNr_(unsigned pRoot);
  // Ugly debug print of BDD with root pRoot.
  static void 
  print_(unsigned pRoot);
  // aheinig
  // For the graphical output of bdd's. 
  static void
  printGraphicalBddVisualization_(unsigned pRoot, 
				  vector<unsigned>* pBddNodesPerLine);

  // Checks if pRoot2 represents a subset of pRoot1
  static bool 
  setContains_(unsigned pRoot1, unsigned pRoot2);
  // Like the equally named non-static functions, 
  // except that the static versions do not catch exceptions.
  static unsigned 
  complement_(unsigned pRoot);
  static unsigned 
  unite_(unsigned pRoot1, unsigned pRoot2);
  static unsigned 
  intersect_(unsigned pRoot1, unsigned pRoot2);
  static unsigned 
  exists_(unsigned pRoot, unsigned pVar);
  static unsigned 
  unTick_(unsigned pRoot);
  static unsigned 
  intersectAndUnTick_(unsigned pRoot1, unsigned pRoot2);
  static unsigned 
  decVars_(unsigned pRoot);
  static unsigned 
  incVars_(unsigned pRoot);

public: // Public static methods.

  // Initialisation of BDD package. Must be called before any other 
  // function of the package is used.
  // Parameters: Values for the m... variables.
  //   number of elements of mNodes == pNodes,
  //   number of elements of mUniqueHash == 2^pUniqueHBitNr,
  //   number of elements of mBinCache == 2^pBinCBitNr.
  //   number of elements of mStatCache == 2^pStatCBitNr.
  static void
  init(unsigned pMaxNodeNr, 
    unsigned pUniqueHBitNr, unsigned pBinCBitNr, unsigned pStatCBitNr);
  // Frees memory used by the static data structures.
  // To be called after use of the BDD package.
  static void done ();

  // Returns overall number of live nodes (Terminal nodes are not counted).
  static unsigned 
  getReachNodeNr();
  // Returns number of external (user) references in mExtrefs.
  static unsigned 
  getExtRefNr();

  // Prints list lengths in mUniqueHash.
  static void 
  analyseUniqueHash ();

private: // Attributes.

  // Index (in mNodes) of the Root node of the BDD.
  unsigned mRoot;

private: // Private methods.

  // Removes mRoot from mExtRefs (if mRoot is no terminal).
  inline void 
  decRef();
  // Inserts mRoot into mExtRefs (if mRoot is no terminal).
  inline void 
  incRef();

public: // Constructors and destructor.

  // Creates BDD with root pRoot.
  bddBdd(unsigned pRoot = 0);
  // Creates BDD as a copy of pBdd.
  bddBdd(const bddBdd& pBdd);
  // Creates BDD that assign the value pValue to the variable pVarId.
  bddBdd(unsigned pVarId, bool pValue);

  // Destructor.
  ~bddBdd();

  // Assignment operator.
  bddBdd& 
  operator=(const bddBdd& aBdd);

public: // Accessors.

  // Returns number of represented states.
  // Only unticked variables (i.e. variables with ids divisible 
  //   by (mUnTickMult)) with ids >= mUnTickMult are considered.
  // The BDD must not contain nodes with other variables.
  // pMaxVar is the maximum id of an unticked variable.
  double 
  getStateNr(unsigned pMaxVar) const;
  // Returns number of nodes (Terminal nodes are not counted).
  unsigned 
  getNodeNr() const;

  // Ugly debug print.
  void 
  print() const;
  
  // aheinig
  // For the graphical output of bdd's. 
  void
  printGraphicalBddVisualization(const bddSymTab* lSymTab, 
				 ostream& pS) const;

public: // Service methods.

  // Check if the sets represented by *this and pBdd are equal.
  bool 
  setEqual(const bddBdd& pBdd) const;
  // Check if pBdd represents a subset of *this.
  bool 
  setContains(const bddBdd& pBdd) const;
  // Check if the represented set is empty.
  bool 
  isEmpty() const;

  // All of the following operations catch exceptions thrown by insert(),
  //   call a garbage collection gc(), and call the operation again.
  // If the second try does not work too, the program is aborted.

  // Computes complement.
  void 
  complement();
  // Unites with pBdd.
  void 
  unite(const bddBdd& pBdd);
  // Intersects with pBdd.
  void 
  intersect(const bddBdd& pBdd);
  // Subtracts pBdd.
  void 
  setSubtract(const bddBdd& pBdd);
  // Existantial quantification of the variable pVar.
  void 
  exists(unsigned pVar);

  // Existential quantification of unticked variables and renaming
  //   of ticked variables to unticked variables.
  // IDs of ticked variables have rest 1 mod mUnTickMult, 
  //   ids of unticked variables rest 0 mod mUnTickMult.
  void 
  unTick();
  // Forward image computation:
  // Intersect with pBdd and unTick() the result.
  void 
  intersectAndUnTick(const bddBdd& pBdd);
  // Decrements variable ids of all nodes of the BDD.
  void 
  decVars();
  // Increments variable ids of all nodes of the BDD.
  void 
  incVars();
};

#endif

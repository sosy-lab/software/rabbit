//-*-Mode: C++;-*-

/*
 * File:        bddTransition.h
 * Purpose:     Transition of a bddAutomaton.
 * Author:      Andreas Noack
 * Created:     2000
 * Copyright:   (c) 2000, Brandenburg Technical University of Cottbus
 */

#ifndef _bddTransition_h_
#define _bddTransition_h_

#include "reprObject.h"
#include "bddConfig.h"
#include "bddSymTab.h"

// Transition of a bddAutomaton.
class bddTransition : private reprObject
{
private:
  // It should be not allowed to use the standard operators.
  void operator,(const bddTransition&);

private: // Attributes.

  // Guard and allowed changes and state changes of participating automata.
  bddConfig mAllowed;

  // Transition relation x == x'
  //   for all variables x not mentioned in the ALLOWED clause.
  //   and for all automata x which do not participate in the transition.
  bddConfig mInitiated;

  // Transition relation = intersection of mAllowed and mInitiated.
  bddConfig mAllowedAndInitiated;

  // The synchronization label. (-1) is the undefined sync label.
  int mSync;

public: // Constructor.

  bddTransition(const bddSymTab* pSymTab,
		const bddConfig& pAllowedAndInitiated,
		const int pSync = -1)
  : mAllowed(pSymTab, false),
    mInitiated(pSymTab, false),
    mAllowedAndInitiated(pAllowedAndInitiated),
    mSync(pSync)
  {
  }

  bddTransition(const bddConfig& pAllowed,
		const bddConfig& pInitiated,
		const int pSync = -1)
  : mAllowed(pAllowed),
    mInitiated(pInitiated),
    mAllowedAndInitiated(pAllowed),
    mSync(pSync)
  {
    mAllowedAndInitiated.intersect(mInitiated);
  }
  
public: // Accessors.

  bddConfig
  getAllowed() const
  {
    return mAllowed;
  }

  bddConfig
  getInitiated() const
  {
    return mInitiated;
  }

  bddConfig
  getAllowedAndInitiated() const
  {
    return mAllowedAndInitiated;
  }

  int
  getSync() const
  {
    return mSync;
  }

  void
  setSync(int pSync)
  {
    mSync = pSync;
  }

public: // Service methods.

  bool
  isEqualInitiated(const bddTransition* pTrans)
  {
    return mInitiated.setEqual(pTrans->mInitiated);
  }

  // Synchronize (this) with (pTrans).
  void
  synchronize(const bddTransition* pTrans)
  {
    mAllowed.intersect(pTrans->mAllowed);
    mInitiated.combineInitiated(pTrans->mInitiated);
    mAllowedAndInitiated = mAllowed;
    mAllowedAndInitiated.intersect(mInitiated);
  }

  // Unite (*this) with (pTrans).
  //   mInitiated is either equal in both transitions or becomes invalid.
  void
  unite(const bddTransition* pTrans)
  {
    mAllowed.unite(pTrans->mAllowed);
    mAllowedAndInitiated.unite(pTrans->getAllowedAndInitiated());
  }

  // Multiply (*this) with (*pTrans). The resulting transition
  //   corresponds to taking first (*this) and then (*pTrans).
  // (mInitiated) and (mAllowed) become invalid.
  void
  multiply(const bddTransition* pTrans)
  {
    mAllowedAndInitiated.decVars();
    mAllowedAndInitiated.intersectAndUnTick(pTrans->getAllowedAndInitiated());
    mAllowedAndInitiated.incVars();
  }

  // Compute the transitive closure of the mAllowedAndInitiated.
  // db 2001-02-05: (mInitiated) and (mAllowed) become invalid.
  void
  transitiveClosure()
  {
    bddConfig lOldAllowedAndInitiated = mAllowedAndInitiated;
    do
    {
      lOldAllowedAndInitiated = mAllowedAndInitiated;
      bddConfig lFactor = mAllowedAndInitiated;
      lFactor.decVars();
      lFactor.intersectAndUnTick(mAllowedAndInitiated);
      lFactor.incVars();
      mAllowedAndInitiated.unite(lFactor);
    }
    while (!mAllowedAndInitiated.setEqual(lOldAllowedAndInitiated));
  }
};

#endif // _bddTransition_h_

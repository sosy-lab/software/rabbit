//-*-Mode: C++;-*-

/*
 * File:        bddExpression.cpp
 * Purpose:     Expression of the form "variable + constant"
 * Author:      Andreas Noack
 * Created:     2000
 * Copyright:   (c) 2000, Brandenburg Technical University of Cottbus
 */

#include "bddExpression.h"

// Add (pAdd) to (*this). It is an error if both expression contain a variable.
void
bddExpression::add(const bddExpression& pAdd)
{
  if (mVar != "") 
  {
    if (pAdd.getVar() != "") 
    {
      cerr << "Runtime error: Two variables '" 
	   << mVar << "'" << endl << "and '" << pAdd.getVar()
	   << "' in one expression !" << endl;
    }
  }
  else
  {
    mVar = pAdd.getVar();
    mTicked = pAdd.getTicked();
  }
  mConst += pAdd.getConst();
}
  
// Add (p1) and (p2). It is an error if both expression contain a variable.
bddExpression
operator+ (const bddExpression& p1, const bddExpression& p2)
{
  if (p1.getVar() != "") 
  {
    if (p2.getVar() != "") 
    {
      cerr << "Runtime error: Two variables '" 
	   << p1.getVar() << "'" << endl << "and '" << p2.getVar()
	   << "' in one expression !" << endl;
    }
    return bddExpression 
      (p1.getVar(), p1.getTicked(), p1.getConst()+p2.getConst());
  }
  else
  {
    return bddExpression
      (p2.getVar(), p2.getTicked(), p1.getConst()+p2.getConst());
  }
}

//-*-Mode: C++;-*-

/*
 * File:        bddSymTab.h
 * Purpose:     Symbol table for the class bddAutomaton
 * Author:      Andreas Noack
 * Created:     2000
 * Copyright:   (c) 2000, Brandenburg Technical University of Cottbus
 */

#ifndef _bddSymTab_h_
#define _bddSymTab_h_

#include <string>
#include <map>
#include <vector>

#include "reprNUMBER.h"
#include "reprObject.h"

#include "ctaModule.h" 


// Infos about the bit position of a variable or automaton
//   in the binary encoded configuration.
struct bddVarInfo
{
  // Position of the first bit.
  unsigned position;
  // Number of bits needed to encode the variable ("bit width").
  unsigned bitNr;
  // Maximum value of a variable (or (number of states)-1 for automata).
  // bitNum is the smallest integer that is greater than log2(maxValue).
  reprNUMBER maxValue;
  // True iff the variable is ticked. 
  // Not used here (i.e. always false), but in the class bddConfig.
  bool ticked;
};

// Symbol table for the class bddAutomaton.

// Invariant for all non-const methods of this class:
//   After changing anything of an object of this class
//   a method has to invalidate the cached config for range
//   in (bddConfig).

class bddSymTab : private reprObject
{
private: // Attributes.

  // Name of the module.
  string mModulName;
  // Number of used bits = sum of bit widths of all variables and automata.
  unsigned mUsedBitNr;
  // bddVarInfo for every automaton.
  map<string, bddVarInfo> mAutos;
  // bddVarInfo for every variable.
  map<string, bddVarInfo> mVars;
  // Names of the states for every automaton.
  // The position of a state in the vector is its number for the encoding.
  map<string, vector<string> > mStateNames;
  // Names of sync labels.
  // The numerical name of a sync label is its position in the vector.
  vector<string> mSyncNames;
  // Includes the variable order with this symb tab is built.
  vector<pair<ctaModule::ParsingTag, ctaString*> >* mVariableOrder;

  // To get the position of a signal in 'mSyncNames' 
  //   with (log mSyncNames.size()) steps.
  map<string, unsigned> mSyncMap;

private:
  // It should not be allowed to use standard operators.
  void operator,(const bddSymTab&);
  void operator=(const bddSymTab&);
  bddSymTab(const bddSymTab&);

public: // Constructors.
  
  bddSymTab(vector<pair<ctaModule::ParsingTag, ctaString*> >* pVariableOrder)
    : mUsedBitNr(0)
  {
    mVariableOrder = pVariableOrder;
  }

  ~bddSymTab()
  {
    for(vector<pair<ctaModule::ParsingTag,ctaString*> >::iterator 
	it = mVariableOrder->begin();
	it != mVariableOrder->end();
	++it)
    {
      delete it->second;
    }
    delete mVariableOrder;
  }  
  

public: // Accessors.

  // This method changes the variable ordering.
  vector<pair<ctaModule::ParsingTag, ctaString*> >*
  getVariableOrder() const
  { 
    return mVariableOrder;
  }

  void
  setModulName(const string& pModulName)
  {
    mModulName = pModulName;
  }
  
  void
  setStateNames(const string& pAutoName, 
		const vector<string>& pStateNames)
  {
    mStateNames[pAutoName] = pStateNames;
  }
  
  void
  setSyncNames(const vector<string>& pSyncNames)
  {
    mSyncNames = pSyncNames;

    // Computing the reverse mapping: string -> position_in_vector
    vector<string>::const_iterator lFirstValueIt
    = mSyncNames.begin();
    for (vector<string>::const_iterator lValueIt
	 = mSyncNames.begin();
	 lValueIt != mSyncNames.end();
	 ++lValueIt)
    {
      mSyncMap[*lValueIt] = lValueIt - lFirstValueIt;
    }
  }
  
  // Only called by ctaModule::mkBddSymTab(...).
  const vector<string>&
  getSyncNames() const
  {
    return mSyncNames;
  }

  string
  getSyncName(int pSync) const
  {
    return mSyncNames[pSync];
  }

  const map<string, bddVarInfo>* const
  getAutoInfos() const
  {
    return &mAutos;
  }

  const map<string, bddVarInfo>* const
  getVarInfos() const
  {
    return &mVars;
  }

  bddVarInfo
  getAutoInfo(const string& pAutoName) const
  {
    return mAutos.find(pAutoName)->second;
  }

  bddVarInfo
  getVarInfo(const string& pVarName) const
  {
    return mVars.find(pVarName)->second;
  }

  // Return name of the automaton starting at bit pPosition
  //   or "" if no automaton starts at this position.
  string
  getAutoName(unsigned pPosition) const
  {
    map<string, bddVarInfo>::const_iterator lIt;
    for (lIt = mAutos.begin();
	 lIt != mAutos.end() 
	   && (lIt->second.position != pPosition || lIt->second.bitNr == 0);
	 ++lIt)
    {}

    if (lIt != mAutos.end())
    {
      return lIt->first;
    }
    else
    {
      return "";
    }
  }

  // Return name of the variable starting at bit pPosition
  //   or "" if no variable starts at this position.
  string
  getVarName(unsigned pPosition) const
  {
    map<string, bddVarInfo>::const_iterator lIt;
    for (lIt = mVars.begin();
	 lIt != mVars.end() 
	   && (lIt->second.position != pPosition || lIt->second.bitNr == 0);
	 ++lIt)
    {}

    if (lIt != mVars.end())
    {
      return lIt->first;
    }
    else
    {
      return "";
    }
  }

  // Return the number of state (pStateName), i.e. its index in the vector.
  unsigned
  getStateNum(const string& pAutoName, const string& pStateName) const
  {
    vector<string>::const_iterator lFirstState
      = mStateNames.find(pAutoName)->second.begin();
    vector<string>::const_iterator lState;
    for (lState = lFirstState;
	 *lState != pStateName;
	 ++lState)
    {}
 
    return lState-lFirstState;
  }

  string
  getStateName(const string& pAutoName, unsigned pNum) const
  {
    return mStateNames.find(pAutoName)->second[pNum];
  }

  // Return the number of sync label (pSyncName), i.e. its index
  // in the vector (mSyncNames). Or -1, if it is not contained in the vector.
  int
  getSyncNum(const string& pSyncName) const
  {
    map<string, unsigned>::const_iterator 
      lIt = mSyncMap.find(pSyncName);

    if (lIt == mSyncMap.end())
    {
      return -1;
    }
    else
    {
      return lIt->second;
    }
  }

  unsigned
  getUsedBitNr() const
  {
    return mUsedBitNr;
  }

public: // Service methods.
  
  // Add automaton (pAutoName) with (pStateNr) states to mAutos 
  // at position (pPosition).
  void
  addAutomaton(const string& pAutoName, 
	       reprNUMBER pStateNum, 
	       unsigned pPosition);

  // Add automaton (pAutoName) with (pStateNr) states to mAutos 
  // at the next free position (i.e. mUsedBitNr).
  void
  addAutomaton(const string& pAutoName, reprNUMBER pStateNr)
  {
    addAutomaton(pAutoName, pStateNr, mUsedBitNr);
  }

  // Add variable (pVarName) with max. value (pMaxValue) to mVars
  // at position (pPosition).
  void
  addVariable(const string& pVarName, 
	      reprNUMBER pMaxValue,
	      unsigned pPosition);

  // Add variable (pVarName) with max. value (pMaxValue) to mVars
  // at the next free position (i.e. mUsedBitNr).
  void
  addVariable(const string& pVarName, reprNUMBER pMaxValue)
  {
    addVariable(pVarName, pMaxValue, mUsedBitNr);
  }

public: // IO.

  void printModulName(ostream& pS) const;
  void printSyncNames(ostream& pS) const;
  void printSyncName(ostream& pS, int pSyncNum) const;
  void printVarNames(ostream& pS) const;
  void printStateNames(ostream& pS) const;
};

#endif // _bddSymTab_h_

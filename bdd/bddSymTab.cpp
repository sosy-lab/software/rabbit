//-*-Mode: C++;-*-

/*
 * File:        bddSymTab.cpp
 * Purpose:     Symbol table for the class bddAutomaton
 * Author:      Andreas Noack
 * Created:     2000
 * Copyright:   (c) 2000, Brandenburg Technical University of Cottbus
 */

#include "bddSymTab.h"

// Add automaton (pAutoName) with (pStateNr) states to mAutos 
// at position (pPosition).
void
bddSymTab::addAutomaton (const string& pAutoName, 
			 reprNUMBER pStateNr, 
			 unsigned pPosition)
{
  bddVarInfo lVI;
  
  pStateNr--;
  lVI.position = pPosition;
  lVI.maxValue = pStateNr;
  lVI.ticked = false;
  
  // Compute bit width (lVI.bitNr).
  lVI.bitNr = 0;
  while (pStateNr)
  {
    pStateNr /= 2;
    ++lVI.bitNr;
  }
  
  if (mUsedBitNr < pPosition + lVI.bitNr)
  {
    mUsedBitNr = pPosition + lVI.bitNr;
  }
  
  mAutos[pAutoName] = lVI;
}

// Add variable (pVarName) with max. value (pMaxValue) to mVars
// at position (pPosition).
void
bddSymTab::addVariable (const string& pVarName, 
			reprNUMBER pMaxValue,
			unsigned pPosition)
{
  bddVarInfo lVI;
  
  lVI.position = pPosition;
  lVI.maxValue = pMaxValue;
  lVI.ticked = false;
  
  // Compute bit width (lVI.bitNr).
  lVI.bitNr = 0;
  while (pMaxValue)
  {
    pMaxValue /= 2;
    ++lVI.bitNr;
  }

  if (mUsedBitNr < pPosition + lVI.bitNr)
  {
    mUsedBitNr = pPosition + lVI.bitNr;
  }
  
  mVars[pVarName] = lVI;
}

void
bddSymTab::printModulName(ostream& pS) const
{
  pS << "MODULE " << mModulName << endl;
}

void
bddSymTab::printSyncName(ostream& pS, int pSyncNum) const
{
  if (pSyncNum == -1)
  {
    pS << "SYNC <none>" << endl;
  }
  else
  {
    pS << "SYNC " << mSyncNames[pSyncNum] << endl;
  }
}

void
bddSymTab::printSyncNames(ostream& pS) const
{
  pS << "LOCAL ";

  for(vector<string>::const_iterator lIt = mSyncNames.begin();
      lIt != mSyncNames.end();
      lIt++)
  {
    pS << *lIt << ' ';
  }

  pS << ": SYNC;" << endl;
}

void
bddSymTab::printVarNames(ostream& pS) const
{
  pS << "LOCAL ";

  for(map<string, bddVarInfo>::const_iterator lIt = mVars.begin();
      lIt != mVars.end();
      lIt++)
  {
    pS << lIt->first << ' ';
  }

  pS << ": CLOCK / DISCRETE;" << endl;
}

void
bddSymTab::printStateNames(ostream& pS) const
{
  for (map<string, vector<string> >::const_iterator 
	 lAutoIt = mStateNames.begin();
       lAutoIt != mStateNames.end();
       ++lAutoIt)
  {
    pS << "AUTOMATON " << lAutoIt->first << endl;
    pS << "{" << endl;
    
    for (vector<string>::const_iterator lStateIt = lAutoIt->second.begin();
	 lStateIt != lAutoIt->second.end();
	 ++lStateIt)
    {
      pS << "STATE " << *lStateIt 
	 << " (" << lStateIt - lAutoIt->second.begin() << ");" << endl;
    }

    pS << "}" << endl;
  }
}

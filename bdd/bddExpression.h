//-*-Mode: C++;-*-

/*
 * File:        bddExpression.h
 * Purpose:     Expression of the form "variable + constant"
 * Author:      Andreas Noack
 * Created:     2000
 * Copyright:   (c) 2000, Brandenburg Technical University of Cottbus
 */

#ifndef _bddExpression_h_
#define _bddExpression_h_

#include <iostream>
#include <string>

#include "reprNUMBER.h"
#include "reprObject.h"

// Expression of the form "variable + constant".
class bddExpression : private reprObject
{
private:
  // It should not be allowed to use the standard operator ','.
  void operator,(const bddExpression&);

private: // Attributes.

  // Variable name, "" means no variable.
  string mVar;
  // True iff variable is ticked.
  bool mTicked;
  // Value of constant.
  reprNUMBER mConst;

public:

  bddExpression(const string& pVar, bool pTicked, reprNUMBER pConst)
    : mVar(pVar),
      mTicked(pTicked),
      mConst(pConst)
  {}

  bddExpression(const bddExpression& p)
    : mVar(p.mVar),
      mTicked(p.mTicked),
      mConst(p.mConst)
  {}

public: // Accessors.

  string
  getVar () const
  {
    return mVar;
  }

  bool
  getTicked () const
  {
    return mTicked;
  }

  reprNUMBER
  getConst () const
  {
    return mConst;
  }
  
public: // Service methods.

  // Add (pAdd) to (*this).
  // It is an error if both expression contain a variable.
  void
  add(const bddExpression& pAdd);
  
public: // Friends.

  // Add (p1) and (p2).
  // It is an error if both expression contain a variable.
  friend bddExpression
  operator + (const bddExpression& p1, const bddExpression& p2);

public: // IO.
  
  friend ostream&  
  operator << (ostream& s, const bddExpression& p)
  {
    s << p.getVar() << (p.mTicked ? "'" : "")
      << " + " << p.getConst() << ' ';
    return s;
  }
};

#endif // _bddExpression_h_

//-*-Mode: C++;-*-

/*
 * File:        bddCommunicationGraphNode.h
 * Purpose:     It contains a variable or a automaton with its bitWide 
 *              and its range.
 * Author:      Andy Heinig
 * Created:     2000
 * Copyright:   (c) 2000, University of Brandenburg, Cottbus
 */

#ifndef _bddCommunicationGraphNode_h
#define _bddCommunicationGraphNode_h

#include "ctaModule.h"

#include "reprNUMBER.h"

class bddCommunicationGraphNode : private ctaObject
{
private:
  // Associated, don't delete the pointer in the destructor.
  ctaString* mName;

  // Agregated.
  // bei automaten ist es die anzahl der zust"ande, bei variablen ist es die
  // range plus eins, weil dort die null einen wert repraesentiert, der mit
  // erfasst werden muss. in mRange enthaelt also der wert null keine 
  // bedeutung, sondern es wird richtig binaer codiert. (nicht 0..7) werte 
  // fuer 2hoch3 sondern 1..8 
  reprNUMBER* mRange;

  unsigned int mTwoHighRange;
  
  // Agregated. It's the log from mRange, and its the height of the bdd 
  //  for the bdd size appraisal.
  reprNUMBER* mBitWide;

  // Agregated. It's the wide for the box for the bdd size appraisal. 
  double mWideOfBdd;
  
  // Because we have a vector of pointer(mNodes) we can't get the index 
  //  of a entry, and so we must hold this information in this variable.
  unsigned int mPositionInMatrix;

  // It's the type of the node e.g. AUTOMATON or INTERFACE, this information 
  //  is for the mOrderedNodes vector.
  ctaModule::ParsingTag mType;

  double mSaveFirstWideOfBdd;

  double mBestWideOfBdd; // fuer den besten Knoten

  unsigned int mLastNode;

  // Includes the last node at begin of the permuteOneVariableWithAllOther 
  //  operation. We need this value for restore the last node after a cycle.
  unsigned int mSaveFirstLastNode;

  // Includes the calculated last node for the best variable order in a step 
  //  in the permuteOneVariableWithAllOther operation.
  unsigned int mBestLastNode; // fuer den besten Knoten

  // It should be not allowed to use the standard operators.
  bddCommunicationGraphNode(const bddCommunicationGraphNode&);
  void operator=(const bddCommunicationGraphNode&);
  void operator,(const bddCommunicationGraphNode&);

public:
  // constructor and destructor
  bddCommunicationGraphNode(ctaString* pName,
			    unsigned int lPositionInMatrix,
			    ctaModule::ParsingTag lType)
    : mName(pName),
      mPositionInMatrix(lPositionInMatrix),
      mType(lType)
  {
    mRange = new reprNUMBER(0);
    mBitWide = new reprNUMBER(0);
    mLastNode = 0;
  }

  ~bddCommunicationGraphNode()
  {
    delete mRange;
    delete mBitWide;
  }

  // accessor methods

  ctaString* 
  getName() const
  {
    return mName;
  }

  void 
  setLastNode(unsigned int lLastNode)
  {
    mLastNode = lLastNode;
  }

  double
  getWideOfBdd()
  {
    return mWideOfBdd;
  }

  unsigned int  
  getLastNode()
  {
    return mLastNode;
  }

  void
  setWideOfBdd(double pWideOfBdd)
  {
    mWideOfBdd = pWideOfBdd;
  }

  unsigned int
  getPositionInMatrix()
  {
    return mPositionInMatrix;
  }

  void
  setRangeAndBitWide(const reprNUMBER* const pRange) 
  {
    // Set the agregation to the range.
    *mRange = *pRange; 

    // Calculate the bitwide, it's the log from range.
    for(double l = *mRange; l > 1; l = l / 2)
    {
      (*mBitWide)++;
    }

    mTwoHighRange = 1;
    for(reprNUMBER l = 0; l < *mBitWide; l++)
    {
      mTwoHighRange = mTwoHighRange * 2;
    }
  }

  const reprNUMBER*
  getRange() const
  {
    return mRange;
  }
  
  ctaModule::ParsingTag
  giveType()
  {
    return mType;
  }

  void
  saveBestConfiguration()
  {
    mBestLastNode = mLastNode;
    mBestWideOfBdd = mWideOfBdd;
  }

  void 
  saveFirstConfiguration()
  {
    mSaveFirstWideOfBdd = mWideOfBdd;
    mSaveFirstLastNode = mLastNode;
  }
  
  void 
  //reloadFirstConfiguration()
  resetFirstConfiguration()
  {
    mLastNode = mSaveFirstLastNode;
    mWideOfBdd = mSaveFirstWideOfBdd;
  }

  void 
  reset()
  {
    // its for the best result 
    mLastNode = mBestLastNode;
    mWideOfBdd = mBestWideOfBdd;
  }
  
  reprNUMBER
  getBitWide()
  {
    return *mBitWide;
  }

  double
  getNodeSum()
  {
    return (mTwoHighRange - 1) * mWideOfBdd;
  }

};

#endif

/*
 * File:        bddBdd.cpp
 * Purpose:     Shared Binary Decision Diagram Package
 * Author:      Andreas Noack
 * Created:     2000
 * Copyright:   (c) 2000, Brandenburg Technical University of Cottbus
 */

#include <vector>
#include <iostream>
#include <set>

#include "bddBdd.h"
#include "ctaSet.h"
#include "bddSymTab.h"

bddNode* bddBdd::mNodes = 0;
unsigned bddBdd::mMaxNodeNr;
unsigned bddBdd::mFree;

multiset<unsigned> bddBdd::mExtRefs;

unsigned* bddBdd::mUniqueHash = 0;
unsigned bddBdd::mUniqueHBitNr;
bddBinEntry* bddBdd::mBinCache = 0;
unsigned bddBdd::mBinCBitNr;
bddStatEntry* bddBdd::mStatCache = 0;
unsigned bddBdd::mStatCBitNr;

// Hash functions.
inline unsigned bddBdd::hash (unsigned i, unsigned hashBitNr) 
{
  unsigned mask = (1u << hashBitNr) - 1;
  return i & mask;
}

inline unsigned bddBdd::hash
  (unsigned i, unsigned j, unsigned k, unsigned hashBitNr) 
{
  unsigned mask = (1u << hashBitNr) - 1;
  j ^= 0x55555555;   // XOR.
  return (i + 
    (j<<(hashBitNr>>1)) + (j>>(hashBitNr>>1)) +
    (k<<(hashBitNr>>2)) + (k>>(hashBitNr>>2))) & mask;
}

// Additional hash functions for comparison.
/*
inline unsigned bddBdd::hash (unsigned i, unsigned j, unsigned hashBitNr) {
  unsigned mask = (1u << hashBitNr) - 1;
  return (i + 
    ((j ^ 0x55555555)<<(hashBitNr>>1)) +
    (j<<(hashBitNr>>2)) + (j>>(hashBitNr>>2))) & mask;
}

inline unsigned bddBdd::hash (unsigned i, unsigned j, unsigned hashBitNr) {
  return ((i * 17765507 + j) * 9243337) >> (32-hashBitNr);
}

inline unsigned bddBdd::hash
  (unsigned i, unsigned j, unsigned k, unsigned hashBitNr) {
  return (((i * 14099753 + j) * 9243337 + k) * 3901787) >> (32-hashBitNr);
}
*/

// Marks (mark=1) all nodes of the BDD with the root pRoot.
void bddBdd::mark(unsigned pRoot) 
{
  if(!mNodes[pRoot].mark) 
  {
    mNodes[pRoot].mark = 1;
    mark(mNodes[pRoot].low);
    mark(mNodes[pRoot].high); 
  }
}

// Unmarks (mark=0) all nodes of the BDD with the root pRoot.
// Terminal always remain marked.
void bddBdd::unMark(unsigned pRoot) 
{
  if(mNodes[pRoot].mark && pRoot != 0 && pRoot != 1)
  {
    mNodes[pRoot].mark = 0;
    unMark(mNodes[pRoot].low);
    unMark(mNodes[pRoot].high);
  }
}

// Garbage collection: All dead nodes (i.e. all nodes which are not reachable
// from a node in mExtRefs) are freed (i.e. inserted into unused-list mFree).
// Terminal nodes are never freed.
void bddBdd::gc() 
{
  // Mark all live nodes.
  for(multiset<unsigned>::iterator lIt = mExtRefs.begin();
      lIt != mExtRefs.end();
      lIt++)
  {
    mark(*lIt);
  }
  
  // Clear hash and caches.
  memset(mUniqueHash, 0, (1u << mUniqueHBitNr) * sizeof(unsigned));
  memset(mBinCache, 0, (1u << mBinCBitNr) * sizeof(bddBinEntry));
  memset(mStatCache, 0, (1u << mStatCBitNr) * sizeof(bddStatEntry));

  mFree = 0;
  for(unsigned lCnt = mMaxNodeNr-1; lCnt >= 2; lCnt--) 
  {
    if(!mNodes[lCnt].mark) 
    { 
      // Free dead nodes.
      mNodes[lCnt].low = mFree;
      mFree = lCnt;
    } 
    else 
    {                
      // Insert live nodes into mUniqueHash.
      mNodes[lCnt].mark = 0;
      unsigned lHashIndex = hash(
	mNodes[lCnt].var, mNodes[lCnt].low, mNodes[lCnt].high, mUniqueHBitNr);
      mNodes[lCnt].next = mUniqueHash[lHashIndex];
      mUniqueHash[lHashIndex] = lCnt;
    }
  }
}


// Returns the mNodes-index of the node with the passed var-, low- and
// high-values. If such node does not exists, it is inserted into
// mNodes and mUniqueHash. If there are no free nodes left, throws exception.
unsigned bddBdd::insert (unsigned pVar, unsigned pLow, unsigned pHigh) 
{
  unsigned lHashIndex;
  unsigned lResult;

  // BDD-reduction of nodes with equal high- and low-child.
  if(pHigh == pLow) 
  {
    return pLow;
  }

  // Search mUniqueHash.
  lHashIndex = hash(pVar, pLow, pHigh, mUniqueHBitNr);
  lResult = mUniqueHash[lHashIndex];  
  while(lResult) 
  {
    if(mNodes[lResult].var == pVar 
       && mNodes[lResult].low == pLow
       && mNodes[lResult].high == pHigh) 
    {
      return lResult;
    }
    lResult = mNodes[lResult].next;
  }

  // Create new node.
  if(mFree == 0)
  {
    throw "Runtime error: BDD package out of memory\n";
  }

  lResult = mFree;
  mFree = mNodes[lResult].low;
  mNodes[lResult].var = pVar;
  mNodes[lResult].low = pLow;
  mNodes[lResult].high = pHigh;
  mNodes[lResult].next = mUniqueHash[lHashIndex];
  mUniqueHash[lHashIndex] = lResult;

  return lResult;
}

// Returns number of represented states of the BDD with root pRoot.
// Only unticked variables (i.e. variables with ids divisible 
//   by (mUnTickMult)) with ids >= mUnTickMult are considered.
// The BDD must not contain nodes with other variable ids.
// pMaxVar is the maximum id of an unticked variable.
double bddBdd::getStateNr_(unsigned pRoot, unsigned pVar, unsigned pMaxVar)
{
  unsigned lCacheIndex;

  // Terminal case.
  if(pVar > pMaxVar)
  {
    return (pRoot == 0 ? 0.0 : 1.0);
  }

  // Reduced node?
  if(pVar < mNodes[pRoot].var)
  {
    return 2 * getStateNr_(pRoot, pVar+mUnTickMult, pMaxVar);
  }

  // Now pVar == mNodes[pRoot].var holds.
  // Result in cache?
  lCacheIndex = hash(pRoot, mStatCBitNr);
  if(mStatCache[lCacheIndex].root == pRoot)
  {
    return mStatCache[lCacheIndex].result;
  }

  mStatCache[lCacheIndex].result 
    = getStateNr_(mNodes[pRoot].low, pVar+mUnTickMult, pMaxVar)
    + getStateNr_(mNodes[pRoot].high, pVar+mUnTickMult, pMaxVar);
  mStatCache[lCacheIndex].root = pRoot;

  return mStatCache[lCacheIndex].result;
}

// Returns number of nodes of the BDD with root pRoot.
// (Terminal nodes are not counted). Side effect: counted nodes are marked.
unsigned bddBdd::getNodeNr_(unsigned pRoot) 
{
  // Note: Terminal nodes are always marked.
  if(mNodes[pRoot].mark)
  {
    return 0;
  }
  mNodes[pRoot].mark = 1;

  return getNodeNr_(mNodes[pRoot].low) + getNodeNr_(mNodes[pRoot].high) + 1;
}

// Ugly debug print of the BDD with root pRoot.
void bddBdd::print_(unsigned pRoot)
{
  if(pRoot == 1) 
  {
    cout << 'T';
  }
  else 
  {
    if(pRoot == 0)
    {
      cout << 'F';
    }
    else 
    {
      cout << '(';
      print_(mNodes[pRoot].low);
      cout << ' ' << mNodes[pRoot].var << ' ';
      print_(mNodes[pRoot].high);
      cout << ')';
    }
  }
}

// Checks if pRoot2 represents a subset of pRoot1
bool bddBdd::setContains_(unsigned pRoot1, unsigned pRoot2) 
{
  unsigned lCacheIndex;
  unsigned lResult;

  // Terminal cases.
  if(pRoot1 == 1)
  {
    return true;
  }
  if(pRoot1 == 0)
  {
    return (pRoot2 == 0);
  }
  if(pRoot1 == pRoot2)
  {
    return true;
  }
  
  // Result in Cache?
  lCacheIndex = hash(mSetContains, pRoot1, pRoot2, mBinCBitNr);
  if(mBinCache[lCacheIndex].op == mSetContains
    && mBinCache[lCacheIndex].root1 == pRoot1
    && mBinCache[lCacheIndex].root2 == pRoot2) 
  {
    return mBinCache[lCacheIndex].result;
  }

  // Compute result.
  if(mNodes[pRoot1].var < mNodes[pRoot2].var)
  {
    lResult = setContains_(mNodes[pRoot1].low, pRoot2)
      && setContains_(mNodes[pRoot1].high, pRoot2);
  }
  else 
  {
    if(mNodes[pRoot1].var == mNodes[pRoot2].var)
    {
      lResult = setContains_(mNodes[pRoot1].low, mNodes[pRoot2].low)
	&& setContains_(mNodes[pRoot1].high, mNodes[pRoot2].high);
    }
    else 
    {
      lResult = setContains_(pRoot1, mNodes[pRoot2].low)
	&& setContains_(pRoot1, mNodes[pRoot2].high);
    }
  }

  // Write result into cache.
  mBinCache[lCacheIndex].result = lResult;
  mBinCache[lCacheIndex].op = mSetContains;
  mBinCache[lCacheIndex].root1 = pRoot1;
  mBinCache[lCacheIndex].root2 = pRoot2;

  return lResult;
}

unsigned bddBdd::complement_(unsigned pRoot) 
{
  unsigned lCacheIndex;
  unsigned lResult;

  if(pRoot == 0)
  {
    return 1;
  }
  if(pRoot == 1)
  {
    return 0;
  }

  lCacheIndex = hash(mComplement, pRoot, pRoot, mBinCBitNr);
  if(mBinCache[lCacheIndex].op == mComplement
    && mBinCache[lCacheIndex].root1 == pRoot) 
  {
    return mBinCache[lCacheIndex].result;
  }

  lResult = insert(mNodes[pRoot].var,
		   complement_(mNodes[pRoot].low), 
		   complement_(mNodes[pRoot].high));

  mBinCache[lCacheIndex].result = lResult;
  mBinCache[lCacheIndex].op = mComplement;
  mBinCache[lCacheIndex].root1 = pRoot;

  return lResult;
}

unsigned bddBdd::unite_(unsigned pRoot1, unsigned pRoot2) 
{
  unsigned lCacheIndex;
  unsigned lResult;

  if(pRoot1 == 0)
  {
    return pRoot2;
  }
  if(pRoot2 == 0)
  {
    return pRoot1;
  }
  if(pRoot1 == pRoot2)
  {
    return pRoot1;
  }

  lCacheIndex = hash(mUnite, pRoot1, pRoot2, mBinCBitNr);
  if (mBinCache[lCacheIndex].op == mUnite
    && mBinCache[lCacheIndex].root1 == pRoot1
    && mBinCache[lCacheIndex].root2 == pRoot2) 
  {
    return mBinCache[lCacheIndex].result;
  }

  if(mNodes[pRoot1].var < mNodes[pRoot2].var)
  {
    lResult = insert(mNodes[pRoot1].var,
		     unite_(mNodes[pRoot1].low, pRoot2), 
		     unite_(mNodes[pRoot1].high, pRoot2));
  }
  else 
  {
    if(mNodes[pRoot1].var == mNodes[pRoot2].var)
    {
      lResult = insert(mNodes[pRoot1].var,
		       unite_(mNodes[pRoot1].low, mNodes[pRoot2].low), 
		       unite_(mNodes[pRoot1].high, mNodes[pRoot2].high));
    }
    else 
    {
      lResult = insert(mNodes[pRoot2].var,
		       unite_(pRoot1, mNodes[pRoot2].low), 
		       unite_(pRoot1, mNodes[pRoot2].high));
    }
  }

  mBinCache[lCacheIndex].result = lResult;
  mBinCache[lCacheIndex].op = mUnite;
  mBinCache[lCacheIndex].root1 = pRoot1;
  mBinCache[lCacheIndex].root2 = pRoot2;

  return lResult;
}

unsigned bddBdd::intersect_(unsigned pRoot1, unsigned pRoot2) 
{
  unsigned lCacheIndex;
  unsigned lResult;

  if(pRoot1 == 1) 
  {
    return pRoot2;
  }
  if(pRoot2 == 1) 
  {
    return pRoot1;
  }
  if(pRoot1 == pRoot2)
  {
    return pRoot1;
  }

  lCacheIndex = hash(mIntersect, pRoot1, pRoot2, mBinCBitNr);
  if(mBinCache[lCacheIndex].op == mIntersect
    && mBinCache[lCacheIndex].root1 == pRoot1
    && mBinCache[lCacheIndex].root2 == pRoot2) 
  {
    return mBinCache[lCacheIndex].result;
  }

  if(mNodes[pRoot1].var < mNodes[pRoot2].var) 
  {
    lResult = insert(mNodes[pRoot1].var,
		     intersect_(mNodes[pRoot1].low, pRoot2), 
		     intersect_(mNodes[pRoot1].high, pRoot2));
  }
  else 
  {
    if(mNodes[pRoot1].var == mNodes[pRoot2].var)
    {
      lResult = insert(mNodes[pRoot1].var,
		       intersect_(mNodes[pRoot1].low, mNodes[pRoot2].low), 
		       intersect_(mNodes[pRoot1].high, mNodes[pRoot2].high));
    }
    else 
    {
      lResult = insert(mNodes[pRoot2].var,
		       intersect_(pRoot1, mNodes[pRoot2].low), 
		       intersect_(pRoot1, mNodes[pRoot2].high));
    }
  }

  mBinCache[lCacheIndex].result = lResult;
  mBinCache[lCacheIndex].op = mIntersect;
  mBinCache[lCacheIndex].root1 = pRoot1;
  mBinCache[lCacheIndex].root2 = pRoot2;

  return lResult;
}

unsigned bddBdd::exists_(unsigned pRoot, unsigned pVar)
{
  unsigned lCacheIndex;
  unsigned lResult;

  if(mNodes[pRoot].var < pVar)
  {
    lCacheIndex = hash(mExists, pRoot, pVar, mBinCBitNr);
    if(mBinCache[lCacheIndex].op == mExists
       && mBinCache[lCacheIndex].root1 == pRoot
       && mBinCache[lCacheIndex].root2 == pVar) 
    {
      return mBinCache[lCacheIndex].result;
    }

    lResult = insert(mNodes[pRoot].var,
		     exists_(mNodes[pRoot].low, pVar), 
		     exists_(mNodes[pRoot].high, pVar));

    mBinCache[lCacheIndex].result = lResult;
    mBinCache[lCacheIndex].op = mExists;
    mBinCache[lCacheIndex].root1 = pRoot;
    mBinCache[lCacheIndex].root2 = pVar;
  }
  else 
  { 
    if(mNodes[pRoot].var == pVar)
    {
      lResult = unite_(mNodes[pRoot].low, mNodes[pRoot].high);
    }
    else
    {
      lResult = pRoot;
    }
  }

  return lResult;
}

unsigned bddBdd::unTick_(unsigned pRoot) 
{
  unsigned lCacheIndex;
  unsigned lResult;

  if(pRoot == 0 || pRoot == 1)
  {
    return pRoot;
  }

  lCacheIndex = hash(mUnTick, pRoot, pRoot, mBinCBitNr);
  if(mBinCache[lCacheIndex].op == mUnTick
     && mBinCache[lCacheIndex].root1 == pRoot) 
  {
    return mBinCache[lCacheIndex].result;
  }

  if(mNodes[pRoot].var % mUnTickMult == 0) 
  {
    // Existential quantification of unticked variables.
    lResult = unite_(unTick_(mNodes[pRoot].low), unTick_(mNodes[pRoot].high));
  }
  else
  {
    if(mNodes[pRoot].var % mUnTickMult == 1) 
    {
      // Rename ticked variables to unticked variables
      lResult = insert(mNodes[pRoot].var-1,
		       unTick_(mNodes[pRoot].low),
		       unTick_(mNodes[pRoot].high));
    }
    else
    {
      lResult = insert(mNodes[pRoot].var,
		       unTick_(mNodes[pRoot].low),
		       unTick_(mNodes[pRoot].high));
    }
  }

  mBinCache[lCacheIndex].result = lResult;
  mBinCache[lCacheIndex].op = mUnTick;
  mBinCache[lCacheIndex].root1 = pRoot;

  return lResult;
}

unsigned bddBdd::intersectAndUnTick_(unsigned pRoot1, unsigned pRoot2) 
{
  unsigned lCacheIndex;
  unsigned lResult;
  // Low and high child of lResult.
  unsigned lResult1;
  unsigned lResult2;
  // The smaller of the variable ids of mNodes[pRoot1] and mNodes[pRoot1].
  unsigned lSmallVar;

  // Terminal cases.
  if(pRoot1 == 0 || pRoot2 == 0)
  {
    return 0;
  }
  if(pRoot1 == 1) 
  {
    return unTick_(pRoot2);
  }
  if(pRoot2 == 1 || pRoot1 == pRoot2)
  {
    return unTick_(pRoot1);
  }

  // Lookup cache.
  lCacheIndex = hash(mIntersectAndUnTick, pRoot1, pRoot2, mBinCBitNr);
  if(mBinCache[lCacheIndex].op == mIntersectAndUnTick
    && mBinCache[lCacheIndex].root1 == pRoot1
    && mBinCache[lCacheIndex].root2 == pRoot2)
  {
    return mBinCache[lCacheIndex].result;
  }

  // Compute lResult1, lResult2, lSmallVar
  if(mNodes[pRoot1].var < mNodes[pRoot2].var) 
  {
    lSmallVar = mNodes[pRoot1].var;
    lResult1 = intersectAndUnTick_(mNodes[pRoot1].low, pRoot2);
    lResult2 = intersectAndUnTick_(mNodes[pRoot1].high, pRoot2);
  }
  else 
  {
    if(mNodes[pRoot1].var == mNodes[pRoot2].var)
    {
      lSmallVar = mNodes[pRoot1].var;
      lResult1 = intersectAndUnTick_(mNodes[pRoot1].low, mNodes[pRoot2].low);
      lResult2 = intersectAndUnTick_(mNodes[pRoot1].high, mNodes[pRoot2].high);
    }
    else 
    {
      lSmallVar = mNodes[pRoot2].var;
      lResult1 = intersectAndUnTick_(pRoot1, mNodes[pRoot2].low);
      lResult2 = intersectAndUnTick_(pRoot1, mNodes[pRoot2].high);
    }
  }

  // Quantify or rename.
  if(lSmallVar % mUnTickMult == 0) 
  {
    // Existential quantification of unticked variables.
    lResult = unite_(lResult1, lResult2);
  }
  else
  {
    if(lSmallVar % mUnTickMult == 1) 
    {
      // Rename ticked variables to unticked variables.
      lResult = insert(lSmallVar-1, lResult1, lResult2);
    }
    else
    {
      lResult = insert(lSmallVar, lResult1, lResult2);
    }
  }

  // Write result into cache.
  mBinCache[lCacheIndex].result = lResult;
  mBinCache[lCacheIndex].op = mIntersectAndUnTick;
  mBinCache[lCacheIndex].root1 = pRoot1;
  mBinCache[lCacheIndex].root2 = pRoot2;

  return lResult;
}

unsigned bddBdd::decVars_ (unsigned pRoot) 
{
  unsigned lCacheIndex;
  unsigned lResult;

  if(pRoot == 0 || pRoot == 1)
  {
    return pRoot;
  }

  lCacheIndex = hash(mDecVars, pRoot, pRoot, mBinCBitNr);
  if(mBinCache[lCacheIndex].op == mDecVars
    && mBinCache[lCacheIndex].root1 == pRoot) 
  {
    return mBinCache[lCacheIndex].result;
  }

  lResult = insert(mNodes[pRoot].var-1,
		   decVars_(mNodes[pRoot].low),
		   decVars_(mNodes[pRoot].high));

  mBinCache[lCacheIndex].result = lResult;
  mBinCache[lCacheIndex].op = mDecVars;
  mBinCache[lCacheIndex].root1 = pRoot;

  return lResult;
}

unsigned bddBdd::incVars_ (unsigned pRoot) 
{
  unsigned lCacheIndex;
  unsigned lResult;

  if(pRoot == 0 || pRoot == 1)
  {
    return pRoot;
  }

  lCacheIndex = hash(mIncVars, pRoot, pRoot, mBinCBitNr);
  if(mBinCache[lCacheIndex].op == mIncVars
    && mBinCache[lCacheIndex].root1 == pRoot) 
  {
    return mBinCache[lCacheIndex].result;
  }

  lResult = insert(mNodes[pRoot].var+1,
		   incVars_(mNodes[pRoot].low),
		   incVars_(mNodes[pRoot].high));

  mBinCache[lCacheIndex].result = lResult;
  mBinCache[lCacheIndex].op = mIncVars;
  mBinCache[lCacheIndex].root1 = pRoot;

  return lResult;
}

// Initialisation of BDD package. Must be called before any other 
// function of the package is used.
// Parameters: Values for the m... variables.
//   number of elements of mNodes == pNodes,
//   number of elements of mUniqueHash == 2^pUniqueHBitNr,
//   number of elements of mBinCache == 2^pBinCBitNr.
//   number of elements of mStatCache == 2^pStatCBitNr.
void bddBdd::init (unsigned pMaxNodeNr, 
		   unsigned pUniqueHBitNr, 
		   unsigned pBinCBitNr, 
		   unsigned pStatCBitNr) {

  // Allocate memory.
  mMaxNodeNr = pMaxNodeNr+2;
  mNodes = new bddNode[mMaxNodeNr];

  mUniqueHBitNr = pUniqueHBitNr;
  mUniqueHash = new unsigned[1u << mUniqueHBitNr];

  mBinCBitNr = pBinCBitNr;
  mBinCache = new bddBinEntry[1u << mBinCBitNr];
  mStatCBitNr = pStatCBitNr;
  mStatCache = new bddStatEntry[1u << mStatCBitNr];

  if(!mNodes || !mUniqueHash || !mBinCache  || !mStatCache) 
  {
    cerr << "Runtime Error: "
	 << "Not enough memory for initialization of BDD package!" << endl;
    exit (1);
  }

  // Initialise arrays.
  memset(mNodes, 0, mMaxNodeNr * sizeof(bddNode));
  memset(mUniqueHash, 0, (1u << mUniqueHBitNr) * sizeof(unsigned));
  memset(mBinCache, 0, (1u << mBinCBitNr) * sizeof(bddBinEntry));
  memset(mStatCache, 0, (1u << mStatCBitNr) * sizeof(bddStatEntry));

  // Initialise terminal nodes.
  mNodes[0].var = (unsigned)-1;
  mNodes[0].mark = 1;
  mNodes[1].var = (unsigned)-1;
  mNodes[1].mark = 1;

  // Initialise mFree list of unused nodes.
  mFree = 2;
  for(unsigned lCnt = 2; lCnt < mMaxNodeNr-1; lCnt++)
  {
    mNodes[lCnt].low = lCnt+1;
  }
}

// Frees memory used by the static data structures.
void bddBdd::done () {
  delete mNodes;
  delete mUniqueHash;
  delete mBinCache;
  delete mStatCache;
}

// Returns overall number of live nodes (Terminal nodes are not counted).
unsigned bddBdd::getReachNodeNr()
{
  unsigned lResult;

  lResult = 0;
  for(multiset<unsigned>::iterator lIt = mExtRefs.begin();
      lIt != mExtRefs.end(); 
      ++lIt)
  {
    lResult += getNodeNr_(*lIt);
  }

  for(multiset<unsigned>::iterator lIt = mExtRefs.begin();
      lIt != mExtRefs.end(); 
      ++lIt)
  {
    unMark(*lIt);
  }

  return lResult;
}

// Returns number of external (user) references in mExtrefs.
unsigned bddBdd::getExtRefNr()
{
  return mExtRefs.size();
}

// Prints list lengths in mUniqueHash.
void bddBdd::analyseUniqueHash() 
{
  vector<unsigned> lListLenCnts;
  unsigned lListLen;
  unsigned lUniqueElem;

  for(unsigned lCnt = 0; lCnt < (1u << mUniqueHBitNr); lCnt++) 
  {
    lListLen = 0;
    lUniqueElem = mUniqueHash[lCnt];
    while(lUniqueElem) 
    {
      lUniqueElem = mNodes[lUniqueElem].next;
      ++lListLen;
    }
    ++lListLenCnts[lListLen];
  }

  cout << "Size of uniqueHash: " << (1u << mUniqueHBitNr) << '\n';
  cout << "n | number of lists of length n in uniqueHash:\n";
  for(unsigned lCnt = 0; lCnt < lListLenCnts.size(); lCnt++)
    cout << lCnt << ' ' << lListLenCnts[lCnt] << '\n';
}

// Removes mRoot from mExtRefs (if mRoot is no terminal).
inline void bddBdd::decRef() 
{
  if(mRoot != 0 && mRoot != 1)
  {
    multiset<unsigned>::iterator lDelete = mExtRefs.find(mRoot);

    if(lDelete == mExtRefs.end())
    {
      cerr << "Runtime error: Error in external BDD references!" << endl;
      exit(1);
    }
    
    mExtRefs.erase(lDelete);
  }
}

// Inserts mRoot into mExtRefs (if mRoot is no terminal).
inline void bddBdd::incRef() 
{
  if(mRoot != 0 && mRoot != 1)
  {
    mExtRefs.insert(mRoot);
  }
}

// Creates BDD with root pRoot.
bddBdd::bddBdd(unsigned pRoot = 0) 
{
  mRoot = pRoot;
  incRef();
}

// Creates BDD as a copy of pBdd.
bddBdd::bddBdd(const bddBdd& pBdd) 
{
  mRoot = pBdd.mRoot;
  incRef();
}

// Creates BDD that assign the value pValue to the variable pVarId.
bddBdd::bddBdd(unsigned pVarId, bool pValue) 
{
  if (pValue) 
  {
    mRoot = insert(pVarId, 0, 1);
  }
  else
  {
    mRoot = insert(pVarId, 1, 0);
  }
  incRef();
}

bddBdd::~bddBdd() 
{
  decRef();
}

bddBdd& bddBdd::operator=(const bddBdd& pBdd) 
{
  decRef();
  mRoot = pBdd.mRoot;
  incRef();
  return *this;
}

// Returns number of represented states.
// Only unticked variables (i.e. variables with ids divisible 
//   by (mUnTickMult)) with ids >= mUnTickMult are considered.
// The BDD must not contain nodes with other variables.
// pMaxVar is the maximum id of an unticked variable.
double bddBdd::getStateNr(unsigned pMaxVar) const
{
  return getStateNr_(mRoot, mUnTickMult, pMaxVar);
}

// Returns number of nodes (Terminal nodes are not counted).
unsigned bddBdd::getNodeNr() const
{
  unsigned lResult;

  lResult = getNodeNr_(mRoot);
  unMark(mRoot);  
  return lResult;
}

// Ugly debug print.
void bddBdd::print() const
{
  print_(mRoot);
  cout << endl;
}

// Check if the sets represented by *this and pBdd are equal.
bool bddBdd::setEqual(const bddBdd& pBdd) const
{
  return mRoot == pBdd.mRoot;
}

// Check if pBdd represents a subset of *this.
bool bddBdd::setContains(const bddBdd& pBdd) const
{
  return setContains_(mRoot, pBdd.mRoot);
}

// Check if the represented set is empty.
bool bddBdd::isEmpty() const
{
  return mRoot == 0;
}

// Computes complement.
void bddBdd::complement() 
{
  unsigned lResult;
  try 
  {
    lResult = bddBdd::complement_(mRoot);
  }
  catch(...) 
  {
    // Out of free nodes in nodes array. Collect Garbage and try again.
    bddBdd::gc();
    try
    {
      lResult = bddBdd::complement_(mRoot);
    }
    catch(...)
    {
      cout << "Runtime error: BDD package out of memory" << endl;
      exit(1);
    }
  }
  decRef();
  mRoot = lResult;
  incRef();
}

// Unites with pBdd.
void bddBdd::unite(const bddBdd& pBdd) 
{
  unsigned lResult;
  try 
  {
    lResult = bddBdd::unite_(mRoot, pBdd.mRoot);
  }
  catch(...) 
  {
    bddBdd::gc();
    try
    {
      lResult = bddBdd::unite_(mRoot, pBdd.mRoot);
    }
    catch(...)
    {
      cout << "Runtime error: BDD package out of memory" << endl;
      exit(1);
    }
  }
  decRef();
  mRoot = lResult;
  incRef();
}

// Intersects with pBdd.
void bddBdd::intersect(const bddBdd& pBdd) 
{
  unsigned lResult;
  try 
  {
    lResult = bddBdd::intersect_(mRoot, pBdd.mRoot);
  }
  catch(...) 
  {
    bddBdd::gc();
    try
    {
      lResult = bddBdd::intersect_(mRoot, pBdd.mRoot);
    }
    catch(...)
    {
      cout << "Runtime error: BDD package out of memory" << endl;
      exit(1);
    }
  }
  decRef();
  mRoot = lResult;
  incRef();
}

// Subtracts pBdd.
void bddBdd::setSubtract(const bddBdd& pBdd) 
{
  unsigned lResult;
  try 
  {
    lResult = bddBdd::intersect_(mRoot, bddBdd::complement_(pBdd.mRoot));
  }
  catch(...) 
  {
    bddBdd::gc();
    try
    {
      lResult = bddBdd::intersect_(mRoot, bddBdd::complement_(pBdd.mRoot));
    }
    catch(...)
    {
      cout << "Runtime error: BDD package out of memory" << endl;
      exit(1);
    }
  }
  decRef();
  mRoot = lResult;
  incRef();
}

// Existantial quantification of the variable pVar.
void bddBdd::exists(unsigned pVar) 
{
  unsigned lResult;
  try 
  {
    lResult = bddBdd::exists_(mRoot, pVar);
  }
  catch(...) 
  {
    bddBdd::gc();
    try
    {
      lResult = bddBdd::exists_(mRoot, pVar);
    }
    catch(...)
    {
      cout << "Runtime error: BDD package out of memory" << endl;
      exit(1);
    }
  }
  decRef();
  mRoot = lResult;
  incRef();
}

// Existantial quantification of unticked variables and renaming
// of ticked variables to unticked variables.
void bddBdd::unTick() 
{
  unsigned lResult;
  try 
  {
    lResult = bddBdd::unTick_(mRoot);
  }
  catch(...) 
  {
    bddBdd::gc();
    try
    {
      lResult = bddBdd::unTick_(mRoot);
    }
    catch(...)
    {
      cout << "Runtime error: BDD package out of memory" << endl;
      exit(1);
    }
  }
  decRef();
  mRoot = lResult;
  incRef();
}

// Forward image computation:
// Intersect with pBdd and unTick() the result.
void bddBdd::intersectAndUnTick(const bddBdd& pBdd)
{
  unsigned lResult;
  try 
  {
    lResult = bddBdd::intersectAndUnTick_(mRoot, pBdd.mRoot);
  }
  catch(...) 
  {
    bddBdd::gc();
    try
    {
      lResult = bddBdd::intersectAndUnTick_(mRoot, pBdd.mRoot);
    }
    catch(...)
    {
      cout << "Runtime error: BDD package out of memory" << endl;
      exit(1);
    }
  }
  decRef();
  mRoot = lResult;
  incRef();
}

// Decrements variable ids of all nodes of the BDD.
void bddBdd::decVars() 
{
  unsigned lResult;
  try 
  {
    lResult = bddBdd::decVars_(mRoot);
  }
  catch(...) 
  {
    bddBdd::gc();
    try
    {
      lResult = bddBdd::decVars_(mRoot);
    }
    catch(...)
    {
      cout << "Runtime error: BDD package out of memory" << endl;
      exit(1);
    }
  }
  decRef();
  mRoot = lResult;
  incRef();
}

// Increments variable ids of all nodes of the BDD.
void bddBdd::incVars() 
{
  unsigned lResult;
  try 
  {
    lResult = bddBdd::incVars_(mRoot);
  }
  catch(...) 
  {
    bddBdd::gc();
    try
    {
      lResult = bddBdd::incVars_(mRoot);
    }
    catch(...)
    {
      cout << "Runtime error: BDD package out of memory" << endl;
      exit(1);
    }
  }
  decRef();
  mRoot = lResult;
  incRef();
}

// aheinig
void
bddBdd::printGraphicalBddVisualization(const bddSymTab* lSymTab, 
				       ostream& pS) const
{
  cerr << "Output for graphical visualization" << endl;

  vector<unsigned>* bddNodesPerLine = 
    new vector<unsigned>(lSymTab->getUsedBitNr());

  printGraphicalBddVisualization_(mRoot, bddNodesPerLine);
  
  for(unsigned lIt = 0; lIt < bddNodesPerLine->size(); lIt++)
  {
    if( lSymTab->getVarName(lIt) != "" )
    {
      pS << lSymTab->getVarName(lIt) << endl;
    }
    else if( lSymTab->getAutoName(lIt) != "" )
    {
      pS << lSymTab->getAutoName(lIt) << endl;
    }

    pS << (*bddNodesPerLine)[lIt] << endl;
  }

  unMark(mRoot);  
  delete bddNodesPerLine;
}

// aheinig
void
bddBdd::printGraphicalBddVisualization_(unsigned pRoot, 
					vector<unsigned>* pBddNodesPerLine)
{
  // Note: Terminal nodes are always marked.
  if( !mNodes[pRoot].mark )
  {
    mNodes[pRoot].mark = 1;

    (*pBddNodesPerLine)[mNodes[pRoot].var / 4 - 1] = 
      (*pBddNodesPerLine)[mNodes[pRoot].var / 4 - 1] + 1;

    printGraphicalBddVisualization_(mNodes[pRoot].low, pBddNodesPerLine); 
    printGraphicalBddVisualization_(mNodes[pRoot].high, pBddNodesPerLine);
  }
}

//-*-Mode: C++;-*-

/*
 * File:        bddCommunicationGraph.h
 * Purpose:     Contains the communication graph with there operation
 * Author:      Andy Heinig
 * Created:     2000
 * Copyright:   (c) 2000, University of Brandenburg, Cottbus
 */

#ifndef _bddCommunicationGraph_h
#define _bddCommunicationGraph_h


#include "ctaVectorOfPointer.h"
#include "ctaModule.h"

#include "bddCommunicationGraphNode.h"

#include "reprNUMBER.h"

#include "utilTime.h"

#include <math.h>
#include <stdio.h>

class bddCommunicationGraph : private ctaObject
{
private:

  // Includes the nodes(automaton and variables).
  ctaVectorOfPointer<bddCommunicationGraphNode*>* mNodes;   
  
  // Includes the edges between the automaton and the non-const variables
  vector<vector <unsigned int> >* mInzidenzMatrix;
  
  // The calculated bdd size.
  double mSum;

  double mBestSum;

  // It should be not allowed to use the standard operators.
  bddCommunicationGraph(const bddCommunicationGraph&);
  void operator=(const bddCommunicationGraph&);
  void operator,(const bddCommunicationGraph&);

public:
  // constructor and destructor
  bddCommunicationGraph()
  {
    mNodes = new ctaVectorOfPointer<bddCommunicationGraphNode*>();
  }
 
  ~bddCommunicationGraph()
  {
    delete mInzidenzMatrix;
    delete mNodes;
  }

  //=======================================================================
  void 
  insertNode(ctaString* lName, ctaModule::ParsingTag pTag) const
  {
    bddCommunicationGraphNode* lNode = 
      new bddCommunicationGraphNode(lName, mNodes->size(), pTag);
    mNodes->push_back(lNode);
  }

  //=======================================================================
  void 
  insertMaxValueOfRangeAndBitWide(const ctaString* lName, 
				  const reprNUMBER* lMaxValueOfRange)
  {
    // Call the position of the ctaString in the mNode vector.
    ctaForAll_VectorOfPointer(mNodes, it, bddCommunicationGraphNode*)  
    {
      if( *(*it)->getName() == *lName ) 
      {
	(*it)->setRangeAndBitWide(lMaxValueOfRange);
      }
    }
  }

  //=======================================================================
  void 
  initInzidenzMatrix() 
  {
    //Initialize the matrix with number-mNode * number-mNode. 
    vector<unsigned int> vHelp(mNodes->size());
    mInzidenzMatrix = new vector<vector<unsigned int> >(mNodes->size(), vHelp);
    
    //Initialize all elements of the matrix with zero.
    {
      for(unsigned int It = 0; 
	  It < mNodes->size(); 
	  It++)
      {
	for(unsigned int It1 = 0; 
	  It1 < mNodes->size(); 
	  It1++)
	{
	  (*mInzidenzMatrix)[It][It1] = 0; 
	}
      }
    }
  }

  //=======================================================================  
  void 
  insertEdge(const ctaString* lName1, const ctaString* lName2)
  {    

    int i = 0, j = 0, counter = 0;
 
    // Search the position of the ctaString in the mNode vector.
    ctaForAll_VectorOfPointer(mNodes, it, bddCommunicationGraphNode*)  
    {
      if( *(*it)->getName() == *lName1 ) 
      {
	i = counter;
      }
      if( *(*it)->getName() == *lName2 ) 
      {
	j = counter;
      }
      ++counter;
    }
    // Insert an 1-element at the position which the ctaStrings has 
    //  in the vector.
    (*mInzidenzMatrix)[i][j] = 1;
    (*mInzidenzMatrix)[j][i] = 1;
  }

  //=======================================================================
  void 
  calculateVariableOrder()
  {
    if( pubCmdLineOpt->useRandomVariableOrder() )
    {
      this->shakeNodes();
    }

    double lControlSum;
    
    this->setLastNode(mNodes->size());
    this->calculateFirstSum(mNodes->size());
    
    cout << "Estimated size of BDD before calculate the variable ordering: " 
	 << mSum << endl;
 
    if( pubCmdLineOpt->useVariableOrderingAfterRandom() )
    {
       
      lControlSum = mSum;
      double lSum = mSum;
      do
      {
	lSum = mSum; 
	for(unsigned int k = 0; k < mNodes->size(); k++)
	{
	  {
	    this->permuteOneVariableWithAllOther(k, mNodes->size());
	  }
	}
	
	// Its for the numerical stabilisation, if we have a impovement of 
	//   more than 10e+10 bdd nodes, the iterative calculated results 
	//   can be false. And so we need a new calculation of the results. 
	if( log10(lControlSum / mSum) > 6 || mSum < 0)
	{
	  this->calculateFirstSum(mNodes->size());
	}
      }
      while( lSum >= (mSum + mSum * 0.000000005) );  
      //printList();
      
    }
    cout << "Estimated size of BDD after calculate the variable ordering: " 
	 << mSum << endl;
  }


  //=======================================================================
  void 
  printGraphicalBddEstim(ostream& pS)
  {
    this->setLastNode(mNodes->size());
    this->printGraphicalBddEstimLocal(pS);
  }

  //=======================================================================
  // Returns the vector with the ordered variables.
  void
  giveOrderedNodes(vector<pair<ctaModule::ParsingTag, 
		   ctaString*> >* pVariableOrder) const
  {
    unsigned int ItOrderedNodes = 0; 
    for(vector<pair<ctaModule::ParsingTag, ctaString*> >::iterator 
	  lVariableOrderIt = pVariableOrder->begin();
	lVariableOrderIt != pVariableOrder->end();
	++lVariableOrderIt) 
    {
      lVariableOrderIt->first = (*mNodes)[ItOrderedNodes]->giveType(); 
      lVariableOrderIt->second = (*mNodes)[ItOrderedNodes]->getName();
      ItOrderedNodes++;
    }
  }

  //privat:

  //=======================================================================
  // It's for testing only.
  void 
  printList()
  {    
    ctaForAll_VectorOfPointer(mNodes, it, bddCommunicationGraphNode*)  
    {
      cout << *((*it)->getName()) << endl;
    }
    for(unsigned lIt = 0; 
	lIt < mNodes->size();
	++lIt)
    {
      for(unsigned lIt1 = lIt; 
	  lIt1 < mNodes->size();
	  ++lIt1)
      {
	if( (*mInzidenzMatrix)
	    [(*mNodes)[lIt]->getPositionInMatrix()]
	    [(*mNodes)[lIt1]->getPositionInMatrix()] 
	    > 0 )
	{
	  cout << lIt << endl; 
	  cout << lIt1 << endl;
	}
      }
    }
    ctaForAll_VectorOfPointer(mNodes, it, bddCommunicationGraphNode*)  
    {
      cout << " " << ((*it)->getWideOfBdd()) << endl;
    }
  }

  //=======================================================================
  void
  shakeNodes()
  {  
    // It's a trick to get changing start values.
    utilTime* lTime = new utilTime();
    long lRand = lTime->getStartTime();
    delete lTime;

    srand(lRand);
    for (unsigned lIt = 0; 
	 lIt < (mNodes->size() / 4);
	 ++lIt)
    {
      unsigned int lRandomVar;
      
      lRandomVar = rand() % (mNodes->size()-lIt);

      bddCommunicationGraphNode* lSwap = (*mNodes)[lIt];
      (*mNodes)[lIt] = (*mNodes)[lIt+lRandomVar];
      (*mNodes)[lIt+lRandomVar] = lSwap;    
    }
  }

  //=======================================================================
  void
  setLastNode(unsigned int lTo)
  {  
    // For all elements we look for all edges which is the one who has 
    //  the node with the highest index.
    for(unsigned int ItNodes = 0; 
	ItNodes < lTo; 
	ItNodes++)
    {
      (*mNodes)[ItNodes]->setLastNode(0);
      for(unsigned int ItNodes1 = ItNodes; 
	  ItNodes1 < lTo; 
	  ItNodes1++)
      {
	if( (*mInzidenzMatrix)
	    [(*mNodes)[ItNodes]->getPositionInMatrix()]
	    [(*mNodes)[ItNodes1]->getPositionInMatrix()] 
	    > 0 )
	{
	  (*mNodes)[ItNodes]->setLastNode(ItNodes1);
	}
      }
    }
  }

  //=======================================================================
  void
  calculateFirstSum(unsigned int lTo)
  {
    double lEdgeProd = 1; 

    mSum = 0; 
    // alle knoten durchgehen 
    for(unsigned int ItNodes = 0; 
	ItNodes < lTo; 
	ItNodes++)
    { 
      lEdgeProd = 1; 
      // von allen davorstehenden
      for(unsigned int ItNodes1 = 0; 
	  ItNodes1 < ItNodes; 
	  ItNodes1++)
      {
	// nicht in der Inzidenzmatrix nach den Knoten nach ItOrderedNodes
	//  schauen, sondern auch nach denen die durchgehen.
	if( (*mNodes)[ItNodes1]->getLastNode() >= ItNodes )
	{
	  lEdgeProd = lEdgeProd * (*(*mNodes)[ItNodes1]->getRange());
	} 
      }
      (*mNodes)[ItNodes]->setWideOfBdd(lEdgeProd);

      mSum = mSum + (*mNodes)[ItNodes]->getNodeSum();
    }
    mBestSum = mSum;
  }

  //=======================================================================
  void 
  permuteOneVariableWithAllOther(unsigned int l1, unsigned int lTo)
  {
    bool isBetter = true;
    // Includes the position for the best calculated variable order.
    unsigned int bestOrder = l1;
    double firstSum = mSum;
    mBestSum = mSum;
  
    for(unsigned int k = 0; k < lTo; k++)
    {
      (*mNodes)[k]->saveFirstConfiguration();
      (*mNodes)[k]->saveBestConfiguration();
    }
    
    // von l1 nach hinten zyklisch tauschen, wenn wir auf dem letzten
    // element sind brauchen wir nicht nach hinten zu tauschen.
    if( l1 < lTo - 1 )
    {
      for(unsigned int j = l1; j < lTo - 1; j++)
      {
	isBetter = permuteTwoVariablesAndCalculateBddSize(j, j+1);
	if( isBetter )
	{
	  mBestSum = mSum;
	  bestOrder = j + 1;
	  for(unsigned int k = 0; k < lTo; k++)
	  {
	    (*mNodes)[k]->saveBestConfiguration();
	  }
	}
      }
    
      // Wiederherstellen bis l1
      bddCommunicationGraphNode* tmp2 = 
	           (*mNodes)[mNodes->size()-1];
      bddCommunicationGraphNode* tmp1 = (*mNodes)[l1];
      bddCommunicationGraphNode* tmp;
      for(unsigned int k = 0; k < lTo; k++)
      {
	(*mNodes)[k]->resetFirstConfiguration();
	if( k > l1 )
	{
	  tmp = tmp1;
	  tmp1 = (*mNodes)[k];
	  (*mNodes)[k] = tmp;
	}
      }
      (*mNodes)[l1] = tmp2;
      mSum = firstSum;
    }

    // vom l1 nach vone zyklisch tauschen, wenn wir auf dem ersten
    // element sind brauchen wir nicht nach vorne zu tauschen.
    if( l1 > 0 )
    {
      for(unsigned int j = l1; j > 0; j--)
      {
	isBetter = permuteTwoVariablesAndCalculateBddSize(j-1, j);
	
	if( isBetter )
	{  
	  mBestSum = mSum;
	  bestOrder = j - 1;
	  for(unsigned int k = 0; k < lTo; k++)
	  {
	    (*mNodes)[k]->saveBestConfiguration();
	  }
	}

      }
    }

    // beste ordnung herstellen
    for(unsigned int k = 0; k < bestOrder; k++)
    {
      bddCommunicationGraphNode* tmp = (*mNodes)[k];
      (*mNodes)[k] = (*mNodes)[k+1];
      (*mNodes)[k+1] = tmp;
      (*mNodes)[k]->reset();
    }

    // beste ordnung herstellen bis ende
    for(unsigned int k = bestOrder; k < lTo; k++)
    {
       (*mNodes)[k]->reset();
    }

    mSum = mBestSum;
  }

  //=======================================================================
  bool 
  permuteTwoVariablesAndCalculateBddSize(unsigned int l1, unsigned int l2)
  {

    mSum = mSum - (*mNodes)[l1]->getNodeSum() - 
                  (*mNodes)[l2]->getNodeSum(); 

    double lEdge1 = (*mNodes)[l1]->getWideOfBdd();
    double lEdge2 = (*mNodes)[l2]->getWideOfBdd();

    for(unsigned int ItNodes = 0; 
	ItNodes < l1; 
	ItNodes++)
    {
      // kante geht nach dem tauschen nicht mehr durch die grenze
      if( (*mNodes)[ItNodes]->getLastNode() == l2 )
      { 
	//wegnehmen
	lEdge2 = lEdge2 / (*(*mNodes)[ItNodes]->getRange());    
	(*mNodes)[ItNodes]->setLastNode(l1);
	//Kante geht zu l1 und l2
	if((*mInzidenzMatrix)
	           [(*mNodes)[ItNodes]->getPositionInMatrix()]
	           [(*mNodes)[l1]->getPositionInMatrix()] > 0 )
	{
	  lEdge2 = lEdge2 * (*(*mNodes)[ItNodes]->getRange());
	  (*mNodes)[ItNodes]->setLastNode(l2);
	}
      } 
      else
      {
	if( (*mNodes)[ItNodes]->getLastNode() == l1 )
	{ 
	  // kante geht nach dem tauschen ueber die grenze
	  //dazunehmen   
	  lEdge2 = lEdge2 * (*(*mNodes)[ItNodes]->getRange());
	  (*mNodes)[ItNodes]->setLastNode(l2);
	}
      }
    }

    // l2 hat kanten zu weiter hinten liegenden knoten, das heisst, 
    //   l2 dazunehmen
    if( (*mNodes)[l2]->getLastNode() > l2 )
    {
      lEdge2 = lEdge2 * (*(*mNodes)[l2]->getRange());
    }
    else
    {  
      // wenn es eine kante zwischen den beiden knoten gibt, oben dazunehmen
      if( (*mInzidenzMatrix)[(*mNodes)[l2]->getPositionInMatrix()]
	                    [(*mNodes)[l1]->getPositionInMatrix()] > 0 )
      {
	lEdge2 = lEdge2 * (*(*mNodes)[l2]->getRange()); 

	// das muss mal ueberlegt werden. (die if abfrage, nicht das 
	//  setLastNode an sich)
        if( (*mNodes)[l2]->getLastNode() <= l2 ) 
	{
	  (*mNodes)[l2]->setLastNode(l2);
	}
      }
    }

    // wenn es eine kante von l1 nach weiter hinten, bei l2 wegrechnen
    if( (*mNodes)[l1]->getLastNode() > l1 )
    {
      lEdge2 = lEdge2 / (*(*mNodes)[l1]->getRange());
      
      if( (*mNodes)[l1]->getLastNode() == l2 )
      {
	(*mNodes)[l1]->setLastNode(0);
      }
    }

    (*mNodes)[l2]->setWideOfBdd(lEdge1);
    (*mNodes)[l1]->setWideOfBdd(lEdge2);

    mSum += (*mNodes)[l1]->getNodeSum();
    mSum += (*mNodes)[l2]->getNodeSum();

    // Permute the two variables;
    bddCommunicationGraphNode* tmp = (*mNodes)[l1];
    (*mNodes)[l1] = (*mNodes)[l2];
    (*mNodes)[l2] = tmp;

    if( mSum < mBestSum )
    {
      return true;
    }
    else
    {
      return false;
    }
  }

  //=======================================================================
  void
  printGraphicalBddEstimLocal(ostream& pS)
  {
    double lEdgeSum = 1; 

    // alle knoten durchgehen 
    for(unsigned int ItOrderedNodes = 0; 
	ItOrderedNodes < mNodes->size(); 
	ItOrderedNodes++)
    {
      // von allen davorstehenden
      for(unsigned int ItOrderedNodes1 = 0; 
	  ItOrderedNodes1 < ItOrderedNodes; 
	  ItOrderedNodes1++)
      {
	// nicht nur in der Inzidenzmatrix nach den Knoten nach ItOrderedNodes
	//  schauen, sondern auch nach denen die durchgehen.
	if( (*mNodes)[ItOrderedNodes1]->getLastNode() >= 
	    ItOrderedNodes )
	{
	  lEdgeSum = lEdgeSum *
	    *(*mNodes)[ItOrderedNodes1]->getRange();
	} 
      }

      unsigned int twoHighL = 1;
      
      for(reprNUMBER l = 0; l < (*mNodes)[ItOrderedNodes]->getBitWide(); l++)
      {
	pS << lEdgeSum * twoHighL << endl;
	twoHighL = twoHighL * 2;
      }

      lEdgeSum = 1;  
    }
  }
};

#endif

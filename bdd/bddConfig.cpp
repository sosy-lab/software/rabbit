//-*-Mode: C++;-*-

/*
 * File:        bddConfig.cpp
 * Purpose:     Set of configurations represented by a BDD
 * Author:      Andreas Noack
 * Created:     2000
 * Copyright:   (c) 2000, Brandenburg Technical University of Cottbus
 */


#include "bddConfig.h"


// Returns all configs satisfying pVI == pConst.
bddConfig
bddConfig::mkEqualPure(const bddSymTab* pSymTab,
		       bddVarInfo pVI, 
		       reprNUMBER pConst)
{
  bddConfig result(pSymTab, bddBdd(true));

  // For all bits of the binary encoding of (pVI).
  for (unsigned lIt = 0; 
       lIt < pVI.bitNr;
       ++lIt)
  {
    // See comment on top of bddConfig.h
    unsigned lPosition = bddBdd::mUnTickMult * (pVI.position+pVI.bitNr-lIt);
    if (pVI.ticked)
    {
      lPosition++;
    }

    result.intersect(bddConfig(pSymTab, 
			       bddBdd(lPosition, (pConst & (1<<lIt)) > 0)));
  }

  return result;
}

// Returns all configs satisfying pVI == pConst.
//   If (pConst) is the max value (pVI.maxValue)
//   then the completion for out-of-range-values is applied.
//   This is to fulfill the invariant of bddConfig 
//   (see comment on top of bddConfig.h).
bddConfig
bddConfig::mkEqual(const bddSymTab* pSymTab,
		   bddVarInfo pVI, 
		   reprNUMBER pConst)
{
  bddConfig result(pSymTab, false);

  // Standard operation.
  result.unite( mkEqualPure(pSymTab, pVI, pConst) );

  // If (pConst) is the (maxValue) of (pVI)
  //   the (pVI) must equal all values greater than (maxValue),
  //   i.e. from the interval ( maxValue, 2^(bitNr)-1 ].
  if( pConst == pVI.maxValue )
  { 
    // From maximal value within range to maximal value represented by BDD.
    for (unsigned lVal = pVI.maxValue + 1; 
	 lVal <= (unsigned) ( 1 << pVI.bitNr) - 1;     // 2^n == (1<<n).
	 ++lVal)
    {
      result.unite( mkEqualPure(pSymTab, pVI, lVal) );
    }
  }

  return result;
}

// Return set of all valid configuration encodings.
bddConfig
bddConfig::mkRange(const bddSymTab* pSymTab)
{
  bddConfig result(pSymTab, true);
  
  // All possible values of all variables.
  for(map<string, bddVarInfo>::const_iterator 
      lVarIt = pSymTab->getVarInfos()->begin();
      lVarIt != pSymTab->getVarInfos()->end();
      ++lVarIt)
  {
    bddConfig lRange(pSymTab, false);
    
    for (reprNUMBER lValueIt = 0;
	 lValueIt <= lVarIt->second.maxValue;
	 ++lValueIt)
    {
      lRange.unite(mkEqualPure(pSymTab, lVarIt->second, lValueIt));
    }
    
    result.intersect(lRange);
  }
  
  // All possible states of all automata.
  for(map<string, bddVarInfo>::const_iterator 
      lAutoIt = pSymTab->getAutoInfos()->begin();
      lAutoIt != pSymTab->getAutoInfos()->end();
      ++lAutoIt)
  {
    bddConfig lRange(pSymTab, false);
    
    for (reprNUMBER lStateIt = 0;
	 lStateIt <= lAutoIt->second.maxValue;
	 ++lStateIt)
    {
      lRange.unite(mkEqualPure(pSymTab, lAutoIt->second, lStateIt));
    }
    
    result.intersect(lRange);
  }
  
  return result;
}

// Return all configs satisfying pE1 < pE2.
bddConfig
bddConfig::mkLess(const bddSymTab* pSymTab,
		  const bddExpression* pE1, 
		  const bddExpression* pE2)
{
  if (pE1->getConst() > pE2->getConst())
  {
    return mkGreater(pSymTab, pE2, pE1);
  }

  bddConfig result(pSymTab, false);
  reprNUMBER lConstDiff = pE2->getConst() - pE1->getConst();

  if (pE1->getVar() == "") 
  { // 0 < y + c
    if (pE2->getVar() == "")
    { // 0 < c
      return bddConfig(pSymTab, 0 < lConstDiff);
    }
    else
    { 
      if (lConstDiff == 0)
      { // 0 < y
	bddVarInfo lVI2 = pSymTab->getVarInfo(pE2->getVar());
	lVI2.ticked = pE2->getTicked();
  
	for (reprNUMBER lIt2 = 1;
	     lIt2 < lVI2.maxValue;
	     ++lIt2)
	{
	  result.unite(mkEqual(pSymTab, lVI2, lIt2));
	}
	result.unite(mkEqual(pSymTab, lVI2, lVI2.maxValue));
	return result;
      }
      else
      {
	return bddConfig(pSymTab, true);
      }
    }
  }

  bddVarInfo lVI1 = pSymTab->getVarInfo(pE1->getVar());
  lVI1.ticked = pE1->getTicked();

  if (pE2->getVar() == "")
  { // x < c
    for (reprNUMBER lIt1 = 0;
	 lIt1 < lConstDiff && lIt1 <= lVI1.maxValue;
	 ++lIt1)
    {
      result.unite(mkEqual(pSymTab, lVI1, lIt1));
    }
    return result;
  }

  bddVarInfo lVI2 = pSymTab->getVarInfo(pE2->getVar());
  lVI2.ticked = pE2->getTicked();
  
  // x < y + c
  for (reprNUMBER lIt2 = 0; 
       lIt2 < lVI2.maxValue;
       ++lIt2)
  {
    for (reprNUMBER lIt1 = 0;
	 lIt1 < lIt2+lConstDiff && lIt1 <= lVI1.maxValue;
	 ++lIt1)
      {
	bddConfig lValuePair = mkEqual(pSymTab, lVI1, lIt1);
	lValuePair.intersect(mkEqual(pSymTab, lVI2, lIt2));
	result.unite(lValuePair);
      }
  }
  for (reprNUMBER lIt1 = 0;
       lIt1 <= lVI1.maxValue;
       ++lIt1)
  {
    bddConfig lValuePair = mkEqual(pSymTab, lVI1, lIt1);
    lValuePair.intersect(mkEqual(pSymTab, lVI2, lVI2.maxValue));
    result.unite(lValuePair);
  }

  return result;
}

// Return all configs satisfying pE1 == pE2.
bddConfig
bddConfig::mkEqual(const bddSymTab* pSymTab,
		   const bddExpression* pE1, 
		   const bddExpression* pE2)
{
  if (pE1->getConst() > pE2->getConst())
  {
    return mkEqual(pSymTab, pE2, pE1);
  }

  bddConfig result(pSymTab, false);
  reprNUMBER lConstDiff = pE2->getConst() - pE1->getConst();

  if (pE1->getVar() == "")
  { // 0 = y + c
    if (pE2->getVar() == "") 
    { // 0 = c
      return bddConfig(pSymTab, lConstDiff == 0);
    }
    else
    { 
      bddVarInfo lVI2 = pSymTab->getVarInfo(pE2->getVar());
      lVI2.ticked = pE2->getTicked();

      if (lConstDiff == 0)
      { // 0 = y
	return mkEqual(pSymTab, lVI2, 0);
      }
      else
      {
	return bddConfig(pSymTab, false);
      }      
    }
  }
  
  bddVarInfo lVI1 = pSymTab->getVarInfo(pE1->getVar());
  lVI1.ticked = pE1->getTicked();

  if (pE2->getVar() == "") 
  { // x = c
    if (lConstDiff <= lVI1.maxValue)
    {
      return mkEqual(pSymTab, lVI1, lConstDiff);
    }
    else
    {
      return mkEqual(pSymTab, lVI1, lVI1.maxValue);
    }
  }

  bddVarInfo lVI2 = pSymTab->getVarInfo(pE2->getVar());
  lVI2.ticked = pE2->getTicked();
  
  // x = y + c
  for (reprNUMBER lIt = 0; 
       lIt+lConstDiff <= lVI1.maxValue || lIt <= lVI2.maxValue;
       ++lIt)
  {
    if (lIt+lConstDiff > lVI1.maxValue) 
    {
      bddConfig lValuePair = mkEqual(pSymTab, lVI1, lVI1.maxValue);
      lValuePair.intersect(mkEqual(pSymTab, lVI2, lIt));
      result.unite(lValuePair);
    }
    else 
    {
      if (lIt > lVI2.maxValue)
      {
	bddConfig lValuePair = mkEqual(pSymTab, lVI1, lIt+lConstDiff);
	lValuePair.intersect(mkEqual(pSymTab, lVI2, lVI2.maxValue));
	result.unite(lValuePair);
      }
      else
      {
	bddConfig lValuePair = mkEqual(pSymTab, lVI1, lIt+lConstDiff);
	lValuePair.intersect(mkEqual(pSymTab, lVI2, lIt));
	result.unite(lValuePair);
      }
    }
  }

  return result;
}

// Return all configs satisfying pE1 > pE2.
bddConfig
bddConfig::mkGreater(const bddSymTab* pSymTab,
		     const bddExpression* pE1, 
		     const bddExpression* pE2)
{
  if (pE1->getConst() > pE2->getConst())
  {
    return mkLess(pSymTab, pE2, pE1);
  }

  bddConfig result(pSymTab, false);
  reprNUMBER lConstDiff = pE2->getConst() - pE1->getConst();
  
  if (pE1->getVar() == "")
  { // 0 > y + c
    return bddConfig(pSymTab, false);
  }
  
  bddVarInfo lVI1 = pSymTab->getVarInfo(pE1->getVar());
  lVI1.ticked = pE1->getTicked();

  if (pE2->getVar() == "") 
  { // x > c
    for (reprNUMBER lIt1 = lConstDiff+1;
	 lIt1 < lVI1.maxValue;
	 ++lIt1)
    {
      result.unite(mkEqual(pSymTab, lVI1, lIt1));
    }
    result.unite(mkEqual(pSymTab, lVI1, lVI1.maxValue));
    return result;
  }

  bddVarInfo lVI2 = pSymTab->getVarInfo(pE2->getVar());
  lVI2.ticked = pE2->getTicked();
  
  // x > y + c
  for (reprNUMBER lIt1 = lConstDiff+1; 
       lIt1 < lVI1.maxValue;
       ++lIt1)
  {
    for (reprNUMBER lIt2 = 0;
	 lIt2+lConstDiff < lIt1 && lIt2 <= lVI2.maxValue;
	 ++lIt2)
      {
	bddConfig lValuePair = mkEqual(pSymTab, lVI1, lIt1);
	lValuePair.intersect(mkEqual(pSymTab, lVI2, lIt2));
	result.unite(lValuePair);
      }
  }
  for (reprNUMBER lIt2 = 0;
       lIt2 <= lVI2.maxValue;
       ++lIt2)
  {
    bddConfig lValuePair = mkEqual(pSymTab, lVI1, lVI1.maxValue);
    lValuePair.intersect(mkEqual(pSymTab, lVI2, lIt2));
    result.unite(lValuePair);
  }

  return result;
}

// Return all configs where the (ticked or unticked, according to (pTicked))
//   state of automaton (pAutoName) is (pStateName).
bddConfig
bddConfig::mkState(const bddSymTab* pSymTab,
		   const string* pAutoName, 
		   const string* pStateName,
		   bool pTicked)
{
  bddVarInfo lVarInfo = pSymTab->getAutoInfo(*pAutoName);
  lVarInfo.ticked = pTicked;

  bddConfig result =
    mkEqual(pSymTab, lVarInfo, pSymTab->getStateNum(*pAutoName, *pStateName));
  return result;
}

// Return the transition relation that changes nothing.
//   (i.e. x == x' for all states and variables).
bddConfig
bddConfig::mkInitiated(const bddSymTab* pSymTab)
{
  bddConfig result(pSymTab, true);

  // For all automata x set state(x) == state'(x).
  for (map<string, bddVarInfo>::const_iterator 
	 lAutoIt = pSymTab->getAutoInfos()->begin();
       lAutoIt != pSymTab->getAutoInfos()->end();
       ++lAutoIt)
  {
    bddConfig lStateUnchanged(pSymTab, false);
    bddVarInfo lVITicked = lAutoIt->second;
    lVITicked.ticked = true;

    for(reprNUMBER lStateIt = 0;
	lStateIt <= lAutoIt->second.maxValue;
	++lStateIt)
    {
      bddConfig lStatePair = mkEqual(pSymTab, lAutoIt->second, lStateIt);
      lStatePair.intersect(mkEqual(pSymTab, lVITicked, lStateIt));
      lStateUnchanged.unite(lStatePair);
    }

    result.intersect(lStateUnchanged);
  }

  // For all variables x set x == x'.
  for (map<string, bddVarInfo>::const_iterator 
	 lVarIt = pSymTab->getVarInfos()->begin();
       lVarIt != pSymTab->getVarInfos()->end();
       ++lVarIt)
  {
    bddConfig lStateUnchanged(pSymTab, false);
    bddVarInfo lVITicked = lVarIt->second;
    lVITicked.ticked = true;

    for(reprNUMBER lValueIt = 0;
	lValueIt <= lVarIt->second.maxValue;
	++lValueIt)
    {
      bddConfig lStatePair = mkEqual(pSymTab, lVarIt->second, lValueIt);
      lStatePair.intersect(mkEqual(pSymTab, lVITicked, lValueIt));
      lStateUnchanged.unite(lStatePair);
    }

    result.intersect(lStateUnchanged);
  }

  return result;
}

// Existential quantification of (pVI).
bddConfig
bddConfig::mkExists(const bddVarInfo pVI) const
{
  bddConfig result(*this);
 
  // For all bits of the encoding of (pVI).
  for (unsigned lIt = 0; 
       lIt < pVI.bitNr;
       ++lIt)
  {
    // See comment on top of bddConfig.h
    unsigned lPosition = bddBdd::mUnTickMult * (pVI.position+1+lIt);
    if (pVI.ticked)
    {
      lPosition++;
    }
    result.mBdd.exists(lPosition);
  }    

  return result;
}

// Existential quantification of all local variables and automata
//   of the module (pModule) and its submodules.
void
bddConfig::existsModule(const ctaString& pModule, bool pTicked)
{
  bddVarInfo lVI;
  ctaString lName = pModule;
  lName += ".";

  for(map<string, bddVarInfo>::const_iterator 
	lVarIt = mSymTab->getVarInfos()->begin();
      lVarIt != mSymTab->getVarInfos()->end();
      ++lVarIt)
  {
    if(lVarIt->first.find(lName) == 0)
    {
      lVI = lVarIt->second;
      lVI.ticked = pTicked;
      (*this) = mkExists(lVI);
    }
  }
  
  // All possible states of all automata.
  for(map<string, bddVarInfo>::const_iterator 
	lAutoIt = mSymTab->getAutoInfos()->begin();
      lAutoIt != mSymTab->getAutoInfos()->end();
      ++lAutoIt)
  {
    if(lAutoIt->first.find(lName) == 0)
    {
      lVI = lAutoIt->second;
      lVI.ticked = pTicked;
      (*this) = mkExists(lVI);
    }
  }    
}

// Compute initiated relation for the synchronisation of two
//   transition relations with the initiated relations (*this) and (pConfig).
void
bddConfig::combineInitiated(const bddConfig& pConfig)
{
  // (this) will be the result.
  // For each 'unmentioned' variable x the bddConfig for initiated contains
  //   the restriction x'=x.

  // For all automata.
  for (map<string, bddVarInfo>::const_iterator 
	 lAutoIt = mSymTab->getAutoInfos()->begin();
       lAutoIt != mSymTab->getAutoInfos()->end();
       ++lAutoIt)
  {
    // If the automaton is quantified out in pConfig, then quantify it out.
    if (pConfig.mkExists(lAutoIt->second).setEqual(pConfig))
    {
      bddVarInfo lVI = lAutoIt->second;
      *this = mkExists(lVI);
      lVI.ticked = true;
      *this = mkExists(lVI);
    }
  }

  // For all variables.
  for (map<string, bddVarInfo>::const_iterator 
	 lVarIt = mSymTab->getVarInfos()->begin();
       lVarIt != mSymTab->getVarInfos()->end();
       ++lVarIt)
  {
    // If the variable quantified out in pConfig, then quantify it out.
    if (pConfig.mkExists(lVarIt->second).setEqual(pConfig))
    {
      bddVarInfo lVI = lVarIt->second;
      *this = mkExists(lVI);
      lVI.ticked = true;
      *this = mkExists(lVI);
    }
  }
}

// Print set of configs.
// If the set of configs depends on ticked variables or states
//   (eg transition relation), (pPrintTicked) should be true. 
//   Else printing of ticked states or vars can be avoided by 
//   setting (pPrintTicked) to false.
void 
bddConfig::print(ostream& pS, bool pPrintTicked) const
{
  // Currently printed cofaktors.
  vector<bddConfig> lCurConfs;
  // Cofaktors to print next.
  vector<bddConfig> lNextConfs;
  // The cofactor with index 0 is always the empty set.
  lCurConfs.push_back(bddConfig(mSymTab, false));
  lNextConfs.push_back(bddConfig(mSymTab, false));
  // Start with the whole set of configs, but exclude invalid bit vectors.
  lCurConfs.push_back(*this);
  lCurConfs[1].intersect(mkRange(mSymTab));

  // Walk through the BDD from root to terminals.
  unsigned lPosIt = 0;
  while (lPosIt < mSymTab->getUsedBitNr())
  {

    // Get name and bddVarInfo of the current automaton / variable.
    bddVarInfo lVI;
    string lName = mSymTab->getAutoName(lPosIt);
    bool lIsAuto = lName != "";
    if (lIsAuto)
    {
      lVI = mSymTab->getAutoInfo(lName);
    }
    else
    {
      lName = mSymTab->getVarName(lPosIt);
      if (lName == "")
      { // Bit unused. Search the next used bit.
	lPosIt++;
	continue;
      }
      lVI = mSymTab->getVarInfo(lName);
    }

    // If (pPrintTicked), also print values of ticked variables.
    for (unsigned lTicked = 0;
	 lTicked <= (pPrintTicked ? 1u : 0u);
	 ++lTicked)
    {
      pS << lName << (lTicked ? "'" : "") << endl;

      lVI.ticked = lTicked;
	
      // For all cofactors in (lCurConfs) (except the empty set at index 0).
      for (vector<bddConfig>::const_iterator lConfIt = lCurConfs.begin()+1;
	   lConfIt != lCurConfs.end();
	   ++lConfIt)
      {
	pS << lConfIt - lCurConfs.begin() << ": ";
	
	// For all states of the automaton / values of the current variable.
	for (reprNUMBER lStateIt = 0;
	     lStateIt <= lVI.maxValue;
	     ++lStateIt)
	{	
	  // Print state or value.
	  if(lIsAuto)
	  {
	    pS << mSymTab->getStateName(lName, lStateIt) << ' ';
	  }
	  else
	  {
	    pS << lStateIt << ' ';
	  }

	  // Compute cofactor for current state/value in (lTmpConf).
	  bddConfig lTmpConf(*lConfIt);
	  lTmpConf.intersect(mkEqual(mSymTab, lVI, lStateIt));
	  lTmpConf = lTmpConf.mkExists(lVI);

	  // Test if (lTmpConf) is already contained in (lNextConfs).
	  vector<bddConfig>::const_iterator lFindIt;
	  for (lFindIt = lNextConfs.begin();
	       lFindIt != lNextConfs.end() && !lFindIt->setEqual(lTmpConf);
	       ++lFindIt)
	  {}
	  if(lFindIt == lNextConfs.end())
	  { // No -> Add Cofactor to (lNextConfs).
	    lNextConfs.push_back(lTmpConf);
	    // Print index of the cofactor.
	    pS << lNextConfs.size()-1 << '|';
	  }
	  else
	  {
	    // Print index of the cofactor.	    
	    pS << lFindIt-lNextConfs.begin() << '|';
	  }
	}
	pS << endl;
      }
      
      lCurConfs = lNextConfs;
      // Do not erase the empty set cofactor at index 0.
      lNextConfs.erase(lNextConfs.begin()+1, lNextConfs.end());
    }
    // Go to the position of the next automaton/variable.
    lPosIt += lVI.bitNr;
  }
}

// aheinig
// For the graphical output of bdd's. 
void 
bddConfig::printGraphicalBddVisualization(ostream& pS) const
{
  mBdd.printGraphicalBddVisualization(mSymTab, pS);
}

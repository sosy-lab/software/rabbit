//-*-Mode: C++;-*-

/*
 * File:        bddAutomaton.cpp
 * Purpose:     Closed Timed Automaton with integer semantics
 * Author:      Andreas Noack
 * Created:     2000
 * Copyright:   (c) 2000, Brandenburg Technical University of Cottbus
 */

#include "ctaComponent.h"

#include "bddAutomaton.h"

#include "utilCmdLineOptions.h"
#include "utilProgress.h"
#include "utilTime.h"

#ifdef win
#include "fertanl.h"

#include "timeb.h"

#include<WinSock2.h>

#define Win32_Winsock
#endif

// Flag for abort by user.
bool bddAutomaton::mSimulationIsAborted = false;

// Signal handler. (i) is the signal number to handle.
void 
signalHandler(int i)
{ 
  bddAutomaton::mSimulationIsAborted = true;
}

// Compute the fixed point of the config set (pStates)
//   regarding the transitions in (pTransitions).
void
bddAutomaton::computeFixPnt(bddConfig* const pConfig,
			const vector<bddTransition>* const pTransitions) const
{
  bddConfig lOldConfig = *pConfig;
  double maxNumbersOfBddNodes = 0;

  do 
  {
    bddConfig lTmp = *pConfig;
    lOldConfig = *pConfig;

    for(vector<bddTransition>::const_iterator 
	  lIt = pTransitions->begin();
	lIt != pTransitions->end();
	++lIt)
    {
      lTmp = *pConfig;
      lTmp.intersectAndUnTick(lIt->getAllowedAndInitiated());
      pConfig->unite(lTmp);

      // aheinig
      // This is for count the maximal numbers of bdd nodes in the 
      //  calculation of the fix point. It's a command line option -c1.  
      if( pubCmdLineOpt->getCountMaxNumbersOfBddNodes() )
      {
	if( pConfig->getBddSize() >= maxNumbersOfBddNodes )
	{
	  maxNumbersOfBddNodes = pConfig->getBddSize();
	}
      }
    }
  }
  while (!pConfig->setEqual(lOldConfig));

  // This is for command line option -c2.
  if( pubCmdLineOpt->getPrintBddSizeAfterComputeFixPnt() )
  {
    cout << endl
	 << "Counting option -c2: number of bdd nodes: " 
	 << pConfig->getBddSize() << endl;
  }

  // This is for command line option -c1.
  if( pubCmdLineOpt->getCountMaxNumbersOfBddNodes() )
  {
    cout << endl 
	 << "Counting option -c1: number of bdd nodes: " 
	 << maxNumbersOfBddNodes << endl;
  }
}

// Computes the transition relation for the visible transitions
//   (synchronized by the labels from (pVisibleSyncs) and
//   stores it in (mVisibleTrans).
// (pVisibleSyncs) is the set of synchronization labels for
//   which P and P||Q must synchronize (this automaton is one of them).
// Invisible transitions are transitions labelled with
//   synchronization labels not visible outside the module.
void
bddAutomaton::computeVisibleTransitions(const ctaIntSet& pVisibleSyncs)
{
  bddTransition lInvisibleTrans(mSymTab, bddConfig::mkInitiated(mSymTab));

  // Unite equivalent transitions.
  // db 2001-02-19: Unite transitions with same sync label.
  { 
    for(vector<bddTransition>::iterator lIt = mTransitions.begin();
	lIt != mTransitions.end();
	lIt++)
    {
      if (pVisibleSyncs.find(lIt->getSync()) != pVisibleSyncs.end())
      {
	// Visible.
	if (mVisibleTrans.find(lIt->getSync()) != mVisibleTrans.end())
	{
	  mVisibleTrans[lIt->getSync()]->unite(&(*lIt));
	}
	else
	{ 
	  // First transition with label (lIt->getSync()).
	  mVisibleTrans[lIt->getSync()] = new bddTransition(*lIt);
	}
      }
      else
      {
	// Invisible.
	lInvisibleTrans.unite(&(*lIt));
      }
    }
  }


  // To allow as many invisible transitions as wanted (star operation).
  lInvisibleTrans.transitiveClosure();

  // Set (mVisibleTrans[-1]) to (invariant-respecting) timestep.
  //   A time step is visible but has no "senseful" sync id.
  //   Thus, we use this ugly way to store it.
  {
    // Build the transition for a time step.
    bddConfig lTmp = mInvariant;
    lTmp.incVars();    // Now (lTmp) contains the invariant for ticked vars.
    lTmp.intersect(mInvariant);
    lTmp.intersect(mTimeStep);
    bddTransition lTimeTrans(mSymTab, lTmp);

    // We have to build a transition for: invisible* timestep invisible*
    // Add invisible transition.
    mVisibleTrans[-1] = new bddTransition(lInvisibleTrans);
    mVisibleTrans[-1]->multiply(&lTimeTrans);     // Time step.
    mVisibleTrans[-1]->multiply(&lInvisibleTrans);
  }

  // We have to build a transition for: invisible* syncLabel invisible*
  // Add invisible transitions to visible transitions.
  {
    for(ctaIntSet::iterator lIt = pVisibleSyncs.begin();
	lIt != pVisibleSyncs.end();
	lIt++)
    {
      if(mVisibleTrans.find(*lIt) != mVisibleTrans.end())
      {
	bddTransition lTmpTrans = lInvisibleTrans;
	lTmpTrans.multiply(mVisibleTrans[*lIt]);
	lTmpTrans.multiply(&lInvisibleTrans);
	delete mVisibleTrans[*lIt];
	mVisibleTrans[*lIt] = new bddTransition(lTmpTrans);
      }
      else
      {
	// For transition labels for which no transition exists:
	//   Empty transition.
	mVisibleTrans[*lIt] = new bddTransition(mSymTab, 
						bddConfig(mSymTab, false));
      }
    }
  }
}

void
bddAutomaton::deleteVisibleTransitions()
{
  for(map<int, bddTransition*>::iterator lIt = mVisibleTrans.begin();
      lIt != mVisibleTrans.end();
      lIt++)
  {  
    delete lIt->second;
  }
  mVisibleTrans.clear();
}

// Computes the follower config using the 'visible' transition
//   labelled with (pSync) and intersects it to (pConf).
void
bddAutomaton::visibleFollowers(bddConfig& pConf, int pSync)
{
  pConf.intersect(mVisibleTrans[pSync]->getAllowedAndInitiated());
}

// Construct the product automaton of (*this) and (pAuto).
void
bddAutomaton::product(bddAutomaton* pAuto,
		      bool firstProduct)
{
  // Construct state invariant.
  mInvariant.intersect(pAuto->mInvariant);

  switch ( pubCmdLineOpt->getTransitionProductType() ) 
  {
    // Here we choose a special method according to the parameters
    //   given by the user.
    case utilCmdLineOptions::mUniteOnlySyncs:
      // Partial transition relation (PTR).
      // This is the original one.
      productUniteOnlySyncs(pAuto);
      break;
    case utilCmdLineOptions::mUniteAllTransitionsOfAnAutomaton:
      // (PUTR).
      // Partial united, all unsynchronized transition of an automata
      //  are united.
      productUniteAllTransitionsOfAnAutomaton(pAuto, firstProduct);
      break;
    case utilCmdLineOptions::mUniteAllTransitions:
      // (UTR).
      // Parameter -b3 used.
      // We use the normal product operation
      //   but the union of all transitions is constructed within
      //   ctaModule::mkBddAutomaton(...) by calling
      //   bddAutomaton::mkUnionOfAllTransitions().
      productUniteOnlySyncs(pAuto);
      break;
  }

  // Construct set of sync labels.
  mSyncSet = ctaIntSet::mkUnion(mSyncSet, pAuto->mSyncSet);
    
  if(pubCmdLineOpt->Verbose())
  {
    cout << "Product automaton: Transitions "
	 << mTransitions.size() << "." << endl;
    
    cout << "We have " << pubCmdLineOpt->GetObjCount()
	 << " ctaObjects after this step." << endl;
  
    int size_ofTR_inBDDNodes = 0;
    for(vector<bddTransition>::const_iterator
	lIt = mTransitions.begin();
	lIt != mTransitions.end();
      ++lIt)
    {
      size_ofTR_inBDDNodes = size_ofTR_inBDDNodes +
	                     lIt->getAllowedAndInitiated().getBddSize();
    }

    cout << "Summarized size of the BDDs representing the transition" 
	 << " relation: " << size_ofTR_inBDDNodes << "." << endl;
  }
}


// Construct the product automaton of (*this) and (pAuto).
//   Order of transitions: transitions of each automaton together at first,
//                         then synchronized transitions together.
// The original one from Andreas.
void
bddAutomaton::productUniteOnlySyncs(bddAutomaton* pAuto)
{
  // Construct transitions.
  vector<bddTransition>* lTransitions = new vector<bddTransition>();

  // Construct transitions in which only (this) takes a transition.
  for(vector<bddTransition>::const_iterator
	lIt1 = mTransitions.begin();
      lIt1 != mTransitions.end();
      ++lIt1)
  {
    if(!pAuto->mustSync(lIt1->getSync()))
    {
      lTransitions->push_back(*lIt1);
    }
  }
  
  // Construct transitions in which only (pAuto) takes a transition.
  for(vector<bddTransition>::const_iterator
	lIt2 = pAuto->mTransitions.begin();
      lIt2 != pAuto->mTransitions.end();
      ++lIt2)
  {
    if(!mustSync(lIt2->getSync())) 
    {
      lTransitions->push_back(*lIt2);
    }
  }
  
  // Construct transitions in which both automata take a transition.
  synchronizeSyncTransitionOfTwoAutomata(pAuto, lTransitions);
  
  mTransitions = *lTransitions;
  delete lTransitions;
}

// Construct the product automaton of (*this) and (pAuto).
//   Partial united, all unsynchronized transition of an automata
//   are united.
void
bddAutomaton::productUniteAllTransitionsOfAnAutomaton(bddAutomaton* pAuto, 
						      bool firstProduct)
{
  // Construct transitions.
  vector<bddTransition>* lTransitions = new vector<bddTransition>();
  bddTransition* lTrans = NULL; 
  

  // Construct transitions in which only (this) takes a transition.
  for(vector<bddTransition>::iterator
	lIt1 = mTransitions.begin();
      lIt1 != mTransitions.end();
      ++lIt1)
  {
    if( !pAuto->mustSync(lIt1->getSync()) )
    {
      if( lIt1->getSync() == -1 )
      {
	if( firstProduct ) 
	{
	  if( lTrans == NULL ) 
	  {
	    lTrans = &(*lIt1);
	  }
	  else
	  {
	    lTrans->unite(&(*lIt1));
	  }
	}
	else
	{
	  lTransitions->push_back(*lIt1);
	}
      }
      else
      {
	lTransitions->push_back(*lIt1);
      }
    }
  }
  
  if( lTrans != NULL ) 
  {
    lTransitions->push_back(*lTrans);
  }

  // We reuse the variable.
  lTrans = NULL; 

  // Construct transitions in which only (pAuto) takes a transition.
  for(vector<bddTransition>::iterator
	lIt2 = pAuto->mTransitions.begin();
      lIt2 != pAuto->mTransitions.end();
      ++lIt2)
  {
    if( !mustSync(lIt2->getSync()) ) 
    {
      if( lIt2->getSync() == -1 )
      {
	if( lTrans == NULL ) 
	{
	  lTrans = &(*lIt2);
	}
	else
	{
	  lTrans->unite(&(*lIt2));
	}
      }
      else
      {
	lTransitions->push_back(*lIt2);
      }
    }
  }
  
  if( lTrans != NULL ) 
  {
    lTransitions->push_back(*lTrans);
  }

  // Construct transitions in which both automata take a transition.
  synchronizeSyncTransitionOfTwoAutomata(pAuto, lTransitions);

  mTransitions = *lTransitions;
  delete lTransitions;
}


void
bddAutomaton::synchronizeSyncTransitionOfTwoAutomata(bddAutomaton* pAuto,
				      vector<bddTransition>* pTransitions)
{			      
  // Construct transitions in which both automata take a transition.
  for(vector<bddTransition>::const_iterator
	lIt1 = mTransitions.begin();
      lIt1 != mTransitions.end();
      ++lIt1)
  {
    for(vector<bddTransition>::const_iterator
	  lIt2 = pAuto->mTransitions.begin();
	lIt2 != pAuto->mTransitions.end();
	++lIt2)
    {
      // Construct transition, iff both sync labels are identical
      if (lIt1->getSync() == lIt2->getSync() && lIt1->getSync() != -1)
      {
	bddTransition lSyncTrans = *lIt1;
	lSyncTrans.synchronize(&(*lIt2));
	if (!lSyncTrans.getAllowedAndInitiated().isEmpty())
	{
	  pTransitions->push_back(lSyncTrans);
	}
      }
    }
  }  
}


void
bddAutomaton::mkUnionOfAllTransitions()
{
  // Construct a new transition which is the union of all transitions
  //   of automaton (*this).
  bddTransition* lTrans = NULL; 

  for(vector<bddTransition>::iterator
	lIt1 = mTransitions.begin();
      lIt1 != mTransitions.end();
      ++lIt1)
  {
    if( lTrans == NULL ) 
    {
      lTrans = &(*lIt1);
    }
    else
    {
      lTrans->unite(&(*lIt1));
    }
  }

  mTransitions.clear();
  if( lTrans != NULL ) 
  {
    mTransitions.push_back(*lTrans);
  }

  if(pubCmdLineOpt->Verbose())
  {
    cout << "Summarized size of the BDDs representing the transition" 
	 << " relation: " 
	 << mTransitions[0].getAllowedAndInitiated().getBddSize() 
	 << "." << endl;
  }
}


void 
bddAutomaton::mkTransitiveClosureOfEveryTransition()
{  
  for(vector<bddTransition>::iterator
	lIt1 = mTransitions.begin();
      lIt1 != mTransitions.end();
      ++lIt1)
  {
    lIt1->transitiveClosure();
  }
}

// aheinig.
void
bddAutomaton::printAllSyncForGraphicalVisualization() 
{
  for(vector<bddTransition>::iterator
	lIt = mTransitions.begin();
      lIt != mTransitions.end();
      ++lIt)
  {
    if( lIt->getSync() != -1 )
    {
      lIt->getAllowedAndInitiated().printGraphicalBddVisualization(cout);
    }
  }
}

void 
bddAutomaton::randomDiskreteTransitionOrder()
{
  // It's a trick to get changing start values.
  utilTime* lTime = new utilTime();
  long lRand = lTime->getStartTime();
  delete lTime;

  srand(lRand);
  for (unsigned lIt = 0; 
       lIt < mTransitions.size();
       ++lIt)
  {
    unsigned lRandomVar;
    
    lRandomVar = rand() % (mTransitions.size()-lIt);
    bddTransition lSwap(mTransitions[lIt]);
    mTransitions[lIt] = mTransitions[lIt+lRandomVar];
    mTransitions[lIt+lRandomVar] = lSwap;
  }
}

// Return all configurations reachable within (pSteps) time units.
//   If a fixpoint is reached, a value >= 0 is returned in (pSteps), else -1.
// Only implemented for (pTag) == mFORWARD.
reprConfig*
bddAutomaton::mkReachable(reprAutomaton_ReachDir pTag,
			  const reprConfig* const pConfig,
			  int& pSteps) const
{
  utilTime* lTime = new utilTime();
 
  if (pTag != mFORWARD)
  {
    cerr << "Runtime error: bddAutomaton::mkReachable()" << endl
         << "  is not implemented for reverse time." << endl;
    return new bddConfig(mSymTab, false);
  }

  bddConfig lConfig = *pConfig->getDerivedObjBdd();
  bddConfig lOldConfig(mSymTab, false);
  // For progress output. Prints one '*' for every time step.
  utilProgress lProgress('*', 1);
  
  // Fixed point regarding the discrete transitions.
  computeFixPnt(&lConfig, &mTransitions);

  while (!lOldConfig.setEqual(lConfig) && pSteps > 0)
  {
    lOldConfig = lConfig;

    // Time step.
    lConfig.intersect(mInvariant);
    lConfig.intersectAndUnTick(mTimeStep);
    lConfig.intersect(mInvariant);

    lConfig.unite(lOldConfig);
   
    // Fixed point regarding the discrete transitions.
    computeFixPnt(&lConfig, &mTransitions);

    lProgress.GiveEvent();

    --pSteps;
  }

  if (!lOldConfig.setEqual(lConfig))
  {
    pSteps = -1;
  }

  if (pubCmdLineOpt->Verbose())
  {
    cout << endl << "Number of reachable discrete configurations: "
	 << lConfig.getConfigNr() << endl;
    cout << "BDD size of the set of reachable configurations: "
	 << lConfig.getBddSize() << endl;

    cout << "Verification time: ";
    lTime->printPastTime();
    cout << ".";
  }

  delete lTime;

  return new bddConfig(lConfig);
}

// Return true iff it is possible to reach (pError) from (pInitial).
//   On-the-fly.
bool
bddAutomaton::isReachable(const reprConfig* const pInitial, 
			  const reprConfig* const pError) const
{ 
  utilTime* lTime = new utilTime();

  bddConfig lNoError = *pError->getDerivedObjBdd();
  lNoError.complement();
  // Current configs and time.
  bddConfig lConfig = *pInitial->getDerivedObjBdd();
  int lStepCnt = 0;

  // Saved configs for fixed point recognition and their time.
  bddConfig lOldConfig(mSymTab, false);
  int lStepCntOld = 0;

  // For progress output. Prints one '*' for every time step.
  utilProgress lProgress('*', 1);
  
  // Fixed point regarding the discrete transitions.
  computeFixPnt(&lConfig, &mTransitions);

  while (!lOldConfig.setContains(lConfig) && lNoError.setContains(lConfig))
  {
    if (lStepCntOld <= lStepCnt/2 || lConfig.setContains(lOldConfig))
      {
      lStepCntOld = lStepCnt;
      lOldConfig = lConfig;
    }

    // Time step.
    lConfig.intersect(mInvariant);
    lConfig.intersectAndUnTick(mTimeStep);
    lConfig.intersect(mInvariant);

    // Fixed point regarding the discrete transitions.
    computeFixPnt(&lConfig, &mTransitions);

    lProgress.GiveEvent();

    ++lStepCnt;
  }
  cout << endl;

  if (pubCmdLineOpt->Verbose())
  {  
    cout << "Verification time: ";
    lTime->printPastTime();
    cout << "." << endl;
  }

  delete lTime;

  return !lNoError.setContains(lConfig);
}

// Returns a non-empty config (i.e. the simulation relation)
//   iff automaton (this) simulates automaton (lP).
//   For given automata P and Q we want to check whether
//   P is a refinement (or: implementation) of Q
//   which is a more abstract version of P (has more traces).
// We have built the bddAutomaton (product) for P||Q and
//   the bddAutomaton for P. Then we have called
//   P||Q.isSimulation(P, syncSet).
// (pVisibleSyncs) is the set of synchronization labels for
//   which P and P||Q must synchronize.
// We use weak simulation after [Weise&Lenzkes1997].
bool
bddAutomaton::isSimulation(bddAutomaton* lP,
			   ctaIntSet& pVisibleSyncs)
{
  computeVisibleTransitions(pVisibleSyncs);
  lP->computeVisibleTransitions(pVisibleSyncs);
  // Now visibleTrans[-1] is the time transition.
  pVisibleSyncs.insert(-1);

  // Compute reachable configurations of (*this).
  bddConfig lReach(mSymTab, false);
  {
    int lSteps = 1000;
    bddConfig* lTmp = 
      mkReachable(mFORWARD, &mInitial, lSteps)->getDerivedObjBdd();
    lReach = *lTmp;
    delete lTmp;
  }

  bddConfig lOldReach = lReach; // We have no constructor for empty BDD.
  do
  {
    // We need the former reach-set to determine whether the fixed point
    //   is reached.
    lOldReach = lReach;

    // We compute the reachable set of P from the reachable set of P||Q.
    bddConfig lPReach = lReach;
    lPReach.existsModule(ctaString("Q"), false);

    // If (lPReach) don't contain at least the whole initial config
    //   there cannot exist a simulation relation.
    if (!lPReach.setContains(lP->getInitial()))
    {
      deleteVisibleTransitions();
      lP->deleteVisibleTransitions();
      cerr << endl;

      return false;
    }

    // (pVisibleSyncs) contains also time transition (synclab -1).
    for(ctaIntSet::iterator lIt = pVisibleSyncs.begin();
	lIt != pVisibleSyncs.end();
	lIt++)
    {
      // (lReach) contains the configs fulfilling the
      //   simulation relation so far.
      //   It will be successively reduced by intersecting
      //   with the 'good' configs of this step.
      // In (lGoodConf) we store the configs fulfilling the 
      //   simulation condition in this step with a particular
      //   sync label.

      // Implementation of the formula for reduction of 
      //   the reachable set to the simulation relation.

      bddConfig lGoodConf = lReach;
      visibleFollowers(lGoodConf, *lIt);  // Intersection with transition.
      bddConfig lTmpConf = lReach;
      lTmpConf.incVars();                 // Use ticked vars.
      lGoodConf.intersect(lTmpConf);      // Eliminate bad configs.
      lGoodConf.existsModule(ctaString("Q"), true);  // Exists q'.

      lTmpConf = lPReach;
      lP->visibleFollowers(lTmpConf, *lIt);
      lTmpConf.complement();
      
      lGoodConf.unite(lTmpConf);

      // "forall p' <formula> holds" quantification as 
      //   "exists not p' that not <formula> holds".
      lGoodConf.complement();
      lGoodConf.existsModule(ctaString("P"), true);  // Exists p'.
      lGoodConf.complement();

      lReach.intersect(lGoodConf);
    }

    // Indicating progress.
    cerr << "." ;
  }
  while (!lReach.setEqual(lOldReach));

  deleteVisibleTransitions();
  lP->deleteVisibleTransitions();

  cerr << endl;

  // Um beim Modell RefineTest.cta diese komische BDD-Ausgabe zu erzeugen:
  //   Nonteminal-Knoten != 0 und != 1.
  //  lReach.print(cout, false);

  return true;
}

// Check if this automaton allows to sync on the foreign label (pForeign)
//   if the own transition carries the label (pOwn).
bool
bddAutomaton::allowsForeignSync(const int pForeign,
				const int pOwn) const
{
  bool result;
  if(mSyncSet.find(pForeign) != mSyncSet.end())
  {
    result = (pForeign == pOwn);
  }
  else
  {
    result = true;
  }
  return result;
}

void 
bddAutomaton::print(ostream& pS) const
{
  mSymTab->printModulName(pS);
  pS << endl;
  mSymTab->printSyncNames(pS);
  mSymTab->printVarNames(pS);
  pS << endl;
  mSymTab->printStateNames(pS);
  pS << endl;

  pS << "INITIAL" << endl;
  mInitial.print(pS, false);
  pS << endl;
  
  pS << "INV" << endl;
  mInvariant.print(pS, false);
  pS << endl;

  pS << "TIMESTEP" << endl;
  mTimeStep.print(pS, true);
  pS << endl;

  for (vector<bddTransition>::const_iterator 
	 lTransIt = mTransitions.begin();
       lTransIt != mTransitions.end();
       ++lTransIt)
  {
    mSymTab->printSyncName(pS, lTransIt->getSync());
    lTransIt->getAllowedAndInitiated().print(pS, true);
    pS << endl;
  }
}

// Print the bdd sizes of the members.
void 
bddAutomaton::printBddSize(ostream& pS)
{
  mSymTab->printModulName(pS);
  pS << endl;

  pS << "INITIAL " << mInitial.getBddSize() << endl;
  
  pS << "INV " << mInvariant.getBddSize() << endl;

  pS << "TIMESTEP " << mTimeStep.getBddSize() << endl;

  pS << "TRANS";

  unsigned lOvTransSize = 0;
  for (vector<bddTransition>::const_iterator lTransIt = mTransitions.begin();
       lTransIt != mTransitions.end();
       ++lTransIt)
  {
    lOvTransSize += lTransIt->getAllowedAndInitiated().getBddSize();
    pS << " " << lTransIt->getAllowedAndInitiated().getBddSize();
  }
  pS << " Overall: " << lOvTransSize << endl;

  bddConfig::printBddSizeInfo();
}

//aheinig
void 
bddAutomaton::simulateCtaModel(ctaMap<ctaComponent*>* pComp) const
{
#ifdef win   

  // Install the signal handler for keyboard interrupts.
  signal(SIGINT, &signalHandler);

  // Mapping signal names to functions for I/O.
  map<string, void (*)()> lOutputFuncMap;
  initOutputFuncMap(lOutputFuncMap);

  // Mapping signal names to functions for I/O.
  map<string, bool (*)()> lInputFuncMap;
  initInputFuncMap(lInputFuncMap);

  multimap<int, bddTransition> lTransitionWithInputSignal; 

  vector<bddTransition> lTransitionWithoutInputSignal;
 
  // Sort the transition into the two vectors (lTransitionWithInputSignal) 
  //   and (lTransitionWithoutInputSignal).
  for(vector<bddTransition>::const_iterator 
	lIt = mTransitions.begin();
      lIt != mTransitions.end();
      ++lIt)
  { 
    if( lIt->getSync() != -1 )
    { 
      ctaString lSyncName = 
	ctaString( mSymTab->getSyncName( lIt->getSync() ) ); 
  
      if(pComp->find(lSyncName)->second->GetRestrictionType() 
	                                        == ctaComponent::INPUT)
      { 
	// Insert transition with input signal.
	lTransitionWithInputSignal.insert(make_pair(lIt->getSync(), *lIt));
      }
      else
      {
	// Insert transition with output or local signal.
	lTransitionWithoutInputSignal.push_back(*lIt);
      }
      // There are no multrest signals per definition.
    }
    else
    {
      // Insert transition with no sync signal.
      lTransitionWithoutInputSignal.push_back(*lIt);
    }
  }

  // Prepare the environment (the machine).
  // Initialize the machine.
  init();

  bddConfig C = mInitial;

  // The Queue for the Input Signals.
  queue<int> lQueue; 

  long timeStepCounter = 0;

  _timeb lRealTime;
  _timeb lModelTime;

  _ftime( &lRealTime );

  cout << "Start time: " << lRealTime.time << "." 
       << lRealTime.millitm << endl;

  cout << "Preparation complete." << endl;

  lModelTime = lRealTime;

  do
  { 
    // Get the input signals from the environment.
    this->processInput(lInputFuncMap, lQueue); 

    // To check whether it was possible to take any transition
    //   according to the first input signal in queue.
    bool lTransitionSwitched = true;

    // A input signal is in the queue, we switch the transition with this
    //  input signal.
    while( !lQueue.empty() && lTransitionSwitched )
    {
      // To check whether it was possible to take any transition
      //   according to the first input signal in queue.
      lTransitionSwitched = false;

      //Sync-Signals has a numerical name in the bdd-representation.   
      // We get the numerical signalname from the queue.
      int lSync = lQueue.front();
      lQueue.pop();	
      
      bool lInpTransIsSwitched = false;
      bddConfig Ctmp = C;
      //We search for the transition with the sync label (lSync).
      typedef multimap<int, bddTransition>::const_iterator I;
      pair<I,I> lItPair = lTransitionWithInputSignal.equal_range(lSync);
      for(I lIt = lItPair.first; 
	   lIt != lItPair.second && !lInpTransIsSwitched;
	   ++lIt)
      {
	// A discrete step.
	C.intersectAndUnTick(lIt->second.getAllowedAndInitiated());
	
	if( !C.isEmpty() )
	{
	  lTransitionSwitched = true;
	  lInpTransIsSwitched = true;
	  if(pubCmdLineOpt->Verbose())
	  {
	    cout << "Transition with input-signal found. " << endl; 
	  }
	}
	else
	{
	  C = Ctmp;
	}
      }
    }

    _ftime( &lRealTime );
    int i1 = lRealTime.time; 
    int i2 = lRealTime.millitm;
    bool lTransIsSwitchedForTime = false;

    // Transition with output signal, local signal or without signal (-1).

    // If a transition in the for loop is switched, we must repeat this 
    //  for loop, so long as is possible to switch a transition in this 
    //  for loop. Otherwise a it is possible that another transition could 
    //  be switched but we dont repeat the loop and so we make a time step 
    //  in a state with invariant false, but this give a zero configuration. 
    bool lTransIsSwitched = false; 
    do
    { 
      lTransIsSwitched = false;
      for(vector<bddTransition>::const_iterator 
	    lIt = lTransitionWithoutInputSignal.begin();
	  lIt != lTransitionWithoutInputSignal.end();
	  ++lIt)
      {
	bddConfig Ctmp = C;

	// A discrete step.
	C.intersectAndUnTick(lIt->getAllowedAndInitiated());
	
	if( !C.isEmpty() ) 
	// We have a successor configuration. 
	{ 
	  cout << "Trans switched" << endl;
	  lTransIsSwitchedForTime = true;
	  // The condition to break up the loop.
	  lTransIsSwitched = true;
	  // If we have a outputsignal we call a method in the environment.
	  // (-1) means no signal, locals and outputs have signal != (-1).
	  if( lIt->getSync() != -1 )
	  {
	    ctaString lSyncName = 
	      ctaString( mSymTab->getSyncName(lIt->getSync()) ); 
	    
	    if(pComp->find(lSyncName)->second->GetRestrictionType() 
	       == ctaComponent::OUTPUT) 
	    {
	      if(pubCmdLineOpt->Verbose())
	      {
		cout << "method call with method name: " 
		     << lSyncName << endl; 
	      }
	      // Call output function.
	      (* lOutputFuncMap[lSyncName] )();
	    }
	  }
	}
	else
	{  
	  // We have no successor configuration for this transition and so
	  //  we must restore the old configuration. 
	  C = Ctmp;  
	}
      }
    }
    // We have switched a transition, we must repeat the for loop. All 
    //  possible transitions must switch, before a time step is realized. 
    while( lTransIsSwitched );
    
    _ftime( &lRealTime );
    if( lTransIsSwitchedForTime )
    {
      cout << "Time: " << i1 << "." << i2 << "    " << lRealTime.time << "." 
	   << lRealTime.millitm << endl;
    }

    if( (lRealTime.time == lModelTime.time && 
	 lRealTime.millitm >= lModelTime.millitm) ||

	(lRealTime.time > lModelTime.time) )
    {   
      // A time step (at the model).
      C.intersect(mInvariant);
      C.intersectAndUnTick(mTimeStep);
      C.intersect(mInvariant);

      // Discretisation with 100 milliseconds (a timestep is 100 
      //  milliseconds long) 
      lModelTime.millitm = lModelTime.millitm + 100;
      
      // This corrects after one second the seconds and milliseconds to 
      //  the correct value. 
      if( lModelTime.millitm > 1000 )
      {
	// Plus one second;
	lModelTime.time =  lModelTime.time + 1;
	// Set the millisecond to the correct value.
	lModelTime.millitm = lModelTime.millitm - 1000;
      }

      if(pubCmdLineOpt->Verbose())
      {
	//cout << "Time step. counter: " << timeStepCounter << endl;
      }
      ++timeStepCounter;
    }
  }
  while( !mSimulationIsAborted );

  // Reset all motors.

#endif
} 

//aheinig
void 
bddAutomaton::simulateCtaModelWithSockets(ctaMap<ctaComponent*>* pComp) const
{
#ifdef win   

  // Sockets initializing.
  int lSockfd = initSockets();
  string cancelSequence = "";

  // Install the signal handler for keyboard interrupts.
  signal(SIGINT, &signalHandler);

  // Mapping signal names to functions for I/O.
  //map<string, void (*)()> lOutputFuncMap;
  //initOutputFuncMap(lOutputFuncMap);

  // Mapping signal names to functions for I/O.
  //map<string, bool (*)()> lInputFuncMap;
  //initInputFuncMap(lInputFuncMap);

  multimap<int, bddTransition> lTransitionWithInputSignal; 

  vector<bddTransition> lTransitionWithoutInputSignal;
 
  // Sort the transition into the two vectors (lTransitionWithInputSignal) 
  //   and (lTransitionWithoutInputSignal).
  for(vector<bddTransition>::const_iterator 
	lIt = mTransitions.begin();
      lIt != mTransitions.end();
      ++lIt)
  { 
    if( lIt->getSync() != -1 )
    { 
      ctaString lSyncName = 
	ctaString( mSymTab->getSyncName( lIt->getSync() ) ); 
  
      if(pComp->find(lSyncName)->second->GetRestrictionType() 
	                                        == ctaComponent::INPUT)
      { 
	// Insert transition with input signal.
	lTransitionWithInputSignal.insert(make_pair(lIt->getSync(), *lIt));
      }
      else
      {
	// Insert transition with output or local signal.
	lTransitionWithoutInputSignal.push_back(*lIt);
      }
      // There are no multrest signals per definition.
    }
    else
    {
      // Insert transition with no sync signal.
      lTransitionWithoutInputSignal.push_back(*lIt);
    }
  }

  bddConfig C = mInitial;

  // The Queue for the Input Signals.
  queue<int> lQueue; 

  long timeStepCounter = 0;

  _timeb lRealTime;
  _timeb lModelTime;

  _ftime( &lRealTime );

  cout << "Start time: " << lRealTime.time << "." 
       << lRealTime.millitm << endl;

  cout << "Preperation complete." << endl;

  lModelTime = lRealTime;

  const char* buffer = cancelSequence.c_str();
  // Initialize the server.
  send(lSockfd, buffer, 255, 0);

  do
  { 
    // Get the input signals from the environment.
    processInput(lSockfd, lQueue);

    // A input signal is in the queue, we switch the transition with this
    //  input signal.
    while( !lQueue.empty() )  // A discrete step begins.
    {  
      //Sync-Signals has a numerical name in the bdd-representation.   
      // We get the numerical signalname from the queue.
      int lSync = lQueue.front();
      lQueue.pop();	
      
      bool lInpTransIsSwitched = false;
      bddConfig Ctmp = C;
      //We search for the transition with the sync label (lSync).
      typedef multimap<int, bddTransition>::const_iterator I;
      pair<I,I> lItPair = lTransitionWithInputSignal.equal_range(lSync);
      for(I lIt = lItPair.first; 
	   lIt != lItPair.second && !lInpTransIsSwitched;
	   ++lIt)
      {
	// A discrete step.
	C.intersectAndUnTick(lIt->second.getAllowedAndInitiated());
	
	if( !C.isEmpty() )
	{
	  lInpTransIsSwitched = true;
	  if(pubCmdLineOpt->Verbose())
	  {
	    cout << "Transition with input-signal found. " << endl; 
	  }
	}
	else
	{
	  C = Ctmp;
	}
      }
    }

    _ftime( &lRealTime );
    int i1 = lRealTime.time; 
    int i2 = lRealTime.millitm;
    bool lTransIsSwitchedForTime = false;

    // Transition with output signal, local signal or without signal (-1).

    // If a transition in the for loop is switched, we must repeat this 
    //  for loop, so long as is possible to switch a transition in this 
    //  for loop. Otherwise a it is possible that another transition could 
    //  be switched but we dont repeat the loop and so we make a time step 
    //  in a state with invariant false, but this give a zero configuration. 
    bool lTransIsSwitched = false; 
    do
    { 
      lTransIsSwitched = false;
      for(vector<bddTransition>::const_iterator 
	    lIt = lTransitionWithoutInputSignal.begin();
	  lIt != lTransitionWithoutInputSignal.end();
	  ++lIt)
      {
	bddConfig Ctmp = C;

	// A discrete step.
	C.intersectAndUnTick(lIt->getAllowedAndInitiated());
	
	if( !C.isEmpty() ) 
	// We have a successor configuration. 
	{ 
	  cout << "Trans switched" << endl;
	  lTransIsSwitchedForTime = true;
	  // The condition to break up the loop.
	  lTransIsSwitched = true;
	  // If we have a outputsignal we call a method in the environment.
	  // (-1) means no signal, locals and outputs have signal != (-1).
	  if( lIt->getSync() != -1 )
	  {
	    ctaString lSyncName = 
	      ctaString( mSymTab->getSyncName(lIt->getSync()) ); 
	    
	    if(pComp->find(lSyncName)->second->GetRestrictionType() 
	       == ctaComponent::OUTPUT) 
	    {
	      if(pubCmdLineOpt->Verbose())
	      {
		cout << "method call with method name: " 
		     << lSyncName << endl; 
	      }
	      // Call output function.
	      const char* lOutputBuffer = lSyncName.c_str();    
	      send(lSockfd, lOutputBuffer, 255, 0);
	    }
	  }
	}
	else
	{  
	  // We have no successor configuration for this transition and so
	  //  we must restore the old configuration. 
	  C = Ctmp;  
	}
      }
    }
    // We have switched a transition, we must repeat the for loop. All 
    //  possible transitions must switch, before a time step is realized. 
    while( lTransIsSwitched );
    
    // Make the server ready to read input from the environment. The 
    //  blocking of the server is canceled. 
    send(lSockfd, buffer, 255, 0);

    _ftime( &lRealTime );
    if( lTransIsSwitchedForTime )
    {
      cout << "Time: " << i1 << "." << i2 << "    " << lRealTime.time << "." 
	   << lRealTime.millitm << endl;
    }

    if( (lRealTime.time == lModelTime.time && 
	 lRealTime.millitm >= lModelTime.millitm) ||

	(lRealTime.time > lModelTime.time) )
    {   
      // A time step (at the model).
      C.intersect(mInvariant);
      C.intersectAndUnTick(mTimeStep);
      C.intersect(mInvariant);

      // Discretisation with 100 milliseconds (a timestep is 100 
      //  milliseconds long) 
      lModelTime.millitm = lModelTime.millitm + 100;
      
      // This corrects after one second the seconds and milliseconds to 
      //  the correct value. 
      if( lModelTime.millitm > 1000 )
      {
	// Plus one second;
	lModelTime.time =  lModelTime.time + 1;
	// Set the millisecond to the correct value.
	lModelTime.millitm = lModelTime.millitm - 1000;
      }

      if(pubCmdLineOpt->Verbose())
      {
	//cout << "Time step. counter: " << timeStepCounter << endl;
      }
      ++timeStepCounter;
    }
  }
  while( !mSimulationIsAborted );

#endif
} 

#ifdef win

int
bddAutomaton::initSockets() const
{  
  WSADATA wsaData;
  int lSockfd;
  struct sockaddr_in lMySockAdr;
  
  cout << "Sockets aufbauen." << endl;

  if( WSAStartup(MAKEWORD(2, 2), &wsaData ) != 0 )
  {
    cout << "Error in startup the sockets." << endl;
  }
  
  lSockfd = socket(AF_INET, SOCK_STREAM, 0);
  if (lSockfd == (-1)) 
  {
    cout << "Construction of socket incorrect." << endl;
  }    
  
  lMySockAdr.sin_family = AF_INET;
  lMySockAdr.sin_port=htons(4444);
  lMySockAdr.sin_addr.s_addr = inet_addr("141.43.23.134"); //erle
  memset(&(lMySockAdr.sin_zero),'\0', 8);
  
  if( connect(lSockfd, 
	      (struct sockaddr*) &lMySockAdr, 
	      sizeof(sockaddr)) < 0 ) 
  { 
    cout << "Method connect() generate error: " << WSAGetLastError() << endl;
  }

  return( lSockfd );
}

// aheinig
// ProcessInput for RUN SIMULATION with direct calls.
void
bddAutomaton::processInput(map<string, bool (*)()>& pInputFuncMap, 
			   queue<int>& pQueue ) const
{ 
  do_set_signals();  

  for(map<string, bool (*)()>::const_iterator 
	lIt = pInputFuncMap.begin();
      lIt != pInputFuncMap.end();
      ++lIt)
  {
    if( (*lIt->second)() )
    {
      if(pubCmdLineOpt->Verbose())
      { 
	cout << "Following signal pushed: " << lIt->first << endl;
      }
      pQueue.push(mSymTab->getSyncNum(lIt->first)); 
    }   
  }
}

// aheinig
// ProcessInput for RUN SIMULATION with sockets.
void
bddAutomaton::processInput(int lSockfd, 
			   queue<int>& pQueue) const 
{ 
  string inputBuffer = "1";
  int sendLenght;
  char buffer[255];

  while( inputBuffer != "" )
  {
    sendLenght = recv(lSockfd, buffer, 255, 0);      
    
    inputBuffer = buffer;
    if(pubCmdLineOpt->Verbose())
    { 
      cout << "Following signal pushed: " << inputBuffer << endl;
    }
    pQueue.push( mSymTab->getSyncNum(inputBuffer) ); 
  }
}

// Initialize function map for input.
//   (pInputFuncMap) is in/out parameter.
void 
bddAutomaton::initInputFuncMap(map<string, bool (*)()>& pInputFuncMap) const
{
  pInputFuncMap["b_sensX_on"]        = &b_sensX_on;
  pInputFuncMap["b_sensLdrX_on"]     = &b_sensLdrX_on;
  pInputFuncMap["b_sensLdrX_off"]    = &b_sensLdrX_off;
  pInputFuncMap["b1_sens1_on"]       = &b1_sens1_on;
  pInputFuncMap["b1_sens2_on"]       = &b1_sens2_on;
  pInputFuncMap["b2_sens_on"]        = &b2_sens_on;
  pInputFuncMap["b3_sens1_on"]       = &b3_sens1_on;
  pInputFuncMap["b3_sens2_on"]       = &b3_sens2_on;
  pInputFuncMap["b5_sens1_on"]       = &b5_sens1_on;
  pInputFuncMap["b5_sens2_on"]       = &b5_sens2_on;
  pInputFuncMap["b6_sens1_on"]       = &b6_sens1_on;
  pInputFuncMap["b6_sens2_on"]       = &b6_sens2_on;
  pInputFuncMap["b7_sens_on"]        = &b7_sens_on; 
  pInputFuncMap["b8_sens1_on"]       = &b8_sens1_on;
  pInputFuncMap["b8_sens2_on"]       = &b8_sens2_on;
  pInputFuncMap["b10_sens1_on"]      = &b10_sens1_on;
  pInputFuncMap["b10_sens2_on"]      = &b10_sens2_on;
  pInputFuncMap["m1_sensTop_on"]     = &m1_sensTop_on;
  pInputFuncMap["m1_sensBottom_on"]  = &m1_sensBottom_on; 
  pInputFuncMap["m1_sensBack_on"]    = &m1_sensBack_on;
  pInputFuncMap["m1_sensFront_on"]   = &m1_sensFront_on; 
  pInputFuncMap["m2_sensTop_on"]     = &m2_sensTop_on;
  pInputFuncMap["m2_sensBottom_on"]  = &m2_sensBottom_on; 
  pInputFuncMap["m2_sensBack_on"]    = &m2_sensBack_on;
  pInputFuncMap["m2_sensFront_on"]   = &m2_sensFront_on; 
  pInputFuncMap["m2_sensRevolv_on"]  = &m2_sensRevolv_on; 
  pInputFuncMap["m3_sensTop_on"]     = &m3_sensTop_on;
  pInputFuncMap["m3_sensBottom_on"]  = &m3_sensBottom_on; 
  pInputFuncMap["m4_sensTop_on"]     = &m4_sensTop_on;
  pInputFuncMap["m4_sensBottom_on"]  = &m4_sensBottom_on;
  pInputFuncMap["p1_sensBack_on"]    = &p1_sensBack_on;
  pInputFuncMap["p1_sensFront_on"]   = &p1_sensFront_on; 
  pInputFuncMap["p2_sensBack_on"]    = &p2_sensBack_on;
  pInputFuncMap["p2_sensFront_on"]   = &p2_sensFront_on; 
  pInputFuncMap["t_sensLeft_on"]     = &t_sensLeft_on;
  pInputFuncMap["t_sensRight_on"]    = &t_sensRight_on;
  pInputFuncMap["tt1_sensX_on"]      = &tt1_sensX_on;
  pInputFuncMap["tt1_sensLeft_on"]   = &tt1_sensLeft_on;
  pInputFuncMap["tt1_sensRight_on"]  = &tt1_sensRight_on;
  pInputFuncMap["tt2_sensX_on"]      = &tt2_sensX_on;
  pInputFuncMap["tt2_sensLeft_on"]   = &tt2_sensLeft_on;
  pInputFuncMap["tt2_sensRight_on"]  = &tt2_sensRight_on; 
}

// Initialize function map for output.
//   (pOutputFuncMap) is in/out parameter.
void 
bddAutomaton::initOutputFuncMap(map<string, void (*)()>& pOutputFuncMap) const
{
  pOutputFuncMap["b_forward"]        = &b_forward;
  pOutputFuncMap["b_stop"]           = &b_stop;
  pOutputFuncMap["b1_forward"]       = &b1_forward;
  pOutputFuncMap["b1_backward"]      = &b1_backward;
  pOutputFuncMap["b1_stop"]          = &b1_stop;
  pOutputFuncMap["b2_forward"]       = &b2_forward;
  pOutputFuncMap["b2_backward"]      = &b2_backward;
  pOutputFuncMap["b2_stop"]          = &b2_stop;
  pOutputFuncMap["b3_forward"]       = &b3_forward;
  pOutputFuncMap["b3_backward"]      = &b3_backward;
  pOutputFuncMap["b3_stop"]          = &b3_stop;
  pOutputFuncMap["b4_forward"]       = &b4_forward;
  pOutputFuncMap["b4_backward"]      = &b4_backward;
  pOutputFuncMap["b4_stop"]          = &b4_stop;  
  pOutputFuncMap["b5_forward"]       = &b5_forward;
  pOutputFuncMap["b5_backward"]      = &b5_backward;
  pOutputFuncMap["b5_stop"]          = &b5_stop;  
  pOutputFuncMap["b6_forward"]       = &b6_forward;
  pOutputFuncMap["b6_backward"]      = &b6_backward;
  pOutputFuncMap["b6_stop"]          = &b6_stop;  
  pOutputFuncMap["b7_forward"]       = &b7_forward;
  pOutputFuncMap["b7_backward"]      = &b7_backward;
  pOutputFuncMap["b7_stop"]          = &b7_stop;  
  pOutputFuncMap["b8_forward"]       = &b8_forward;
  pOutputFuncMap["b8_backward"]      = &b8_backward;
  pOutputFuncMap["b8_stop"]          = &b8_stop;  
  pOutputFuncMap["b9_forward"]       = &b9_forward;
  pOutputFuncMap["b9_backward"]      = &b9_backward;
  pOutputFuncMap["b9_stop"]          = &b9_stop;  
  pOutputFuncMap["b10_forward"]      = &b10_forward;
  pOutputFuncMap["b10_backward"]     = &b10_backward;
  pOutputFuncMap["b10_stop"]         = &b10_stop;
  pOutputFuncMap["m1_up"]            = &m1_up;
  pOutputFuncMap["m1_down"]          = &m1_down;
  pOutputFuncMap["m1_stop"]          = &m1_stop;
  pOutputFuncMap["m1_t_forward"]     = &m1_t_forward;
  pOutputFuncMap["m1_t_backward"]    = &m1_t_backward;
  pOutputFuncMap["m1_t_stop"]        = &m1_t_stop;
  pOutputFuncMap["m1_d_right"]       = &m1_d_right;
  pOutputFuncMap["m1_d_stop"]        = &m1_d_stop;
  pOutputFuncMap["m2_up"]            = &m2_up;
  pOutputFuncMap["m2_down"]          = &m2_down;
  pOutputFuncMap["m2_stop"]          = &m2_stop;
  pOutputFuncMap["m2_t_forward"]     = &m2_t_forward;
  pOutputFuncMap["m2_t_backward"]    = &m2_t_backward;
  pOutputFuncMap["m2_t_stop"]        = &m2_t_stop;
  pOutputFuncMap["m2_d_right"]       = &m2_d_right;
  pOutputFuncMap["m2_d_stop"]        = &m2_d_stop;
  pOutputFuncMap["m2_revolv_right"]  = &m2_revolv_right;
  pOutputFuncMap["m2_revolv_stop"]   = &m2_revolv_stop;
  pOutputFuncMap["m3_up"]            = &m3_up;
  pOutputFuncMap["m3_down"]          = &m3_down;
  pOutputFuncMap["m3_stop"]          = &m3_stop;
  pOutputFuncMap["m4_up"]            = &m4_up;
  pOutputFuncMap["m4_down"]          = &m4_down;
  pOutputFuncMap["m4_stop"]          = &m4_stop;
  pOutputFuncMap["m4_d_right"]       = &m4_d_right;
  pOutputFuncMap["m4_d_stop"]        = &m4_d_stop;
  pOutputFuncMap["p1_forward"]       = &p1_forward;
  pOutputFuncMap["p1_backward"]      = &p1_backward;
  pOutputFuncMap["p1_stop"]          = &p1_stop;
  pOutputFuncMap["p2_forward"]       = &p2_forward;
  pOutputFuncMap["p2_backward"]      = &p2_backward;
  pOutputFuncMap["p2_stop"]          = &p2_stop;
  pOutputFuncMap["t_left"]           = &t_left;
  pOutputFuncMap["t_right"]          = &t_right;
  pOutputFuncMap["t_stop"]           = &t_stop;
  pOutputFuncMap["tt1_b_forward"]    = &tt1_b_forward;
  pOutputFuncMap["tt1_b_stop"]       = &tt1_b_stop;
  pOutputFuncMap["tt1_left"]         = &tt1_left;
  pOutputFuncMap["tt1_right"]        = &tt1_right;
  pOutputFuncMap["tt1_stop"]         = &tt1_stop;
  pOutputFuncMap["tt2_b_forward"]    = &tt2_b_forward;
  pOutputFuncMap["tt2_b_stop"]       = &tt2_b_stop;
  pOutputFuncMap["tt2_left"]         = &tt2_left;
  pOutputFuncMap["tt2_right"]        = &tt2_right;
  pOutputFuncMap["tt2_stop"]         = &tt2_stop;
}

#endif

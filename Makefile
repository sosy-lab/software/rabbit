###########################################################################
# Dirk Beyer
#
# This makefile is for Unix and DOS.  Just type 'make' to make all programs,
# or give individual names
############################################################################
#
# Macro definitions
#
# SHELL=/bin/sh

# Reset suffix list from default to empty.  This 
#   removes default rules which seem to be veeeery 
#   time consuming on the Win95 file system.
.SUFFIXES=

CXX   = g++
CC    = gcc

# To make the compilation faster on walnuss (1.2GB mem.)
ifeq ($(HOSTNAME), walnuss)
	JOBS = 4
else
	JOBS = 2
endif

# To make the compilation faster on ep (SUN E450,4 CPU's 2GB mem.)
ifeq ($(HOST), ep) 
	JOBS = 8
endif

# To know the OS type.
ifeq ($(OSTYPE), solaris) 
	OPSYS = unix
endif
ifeq ($(OSTYPE), linux) 
	OPSYS = unix
endif
ifeq ($(OS), Windows_NT) 
	OPSYS = win
endif
ifeq ($(OS), Win95)
	OPSYS = win
endif

DEB  = #-g    # Store debugging information.
PROF = #-pg   # Store profiling information.
OPT  = -O3   # Optimized code generation.

CFLAGS= $(PROF) $(DEB) $(OPT) -ansi -Wall -I. -I../ddm -I../bdd -I../ana -I../cta -I../sim -D$(OPSYS)

# Link Windows socket library if using a Win32 system.
ifeq ($(OPSYS), win)
	LFLAGS = $(PROF) -L../ddm -L../bdd -lddm -lbdd -lwsock32
else
	LFLAGS = $(PROF) -L../ddm -L../bdd -lddm -lbdd
endif

export CC
export CXX
export CFLAGS
export LFLAGS
export OPSYS

#
# Default target
#
all:	
	cd ddm; make -j $(JOBS) -r libddm.a
	cd bdd; make -j $(JOBS) -r libbdd.a
	cd cta; make -j $(JOBS) -r rabbit

#
# Dependencies
#

dep:
	cd bdd; make -r dep
	cd cta; make -r dep
	cd ddm; make -r dep

#
# Clean
#
clean:
	-rm -f *~ *.o

cleanall:
	cd ddm; make -r clean
	cd cta; make -r clean
	cd bdd; make -r clean


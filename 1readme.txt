db 1999-12-16:
Die Version 'release-1-0' des gesamten Projektes spiegelt
den Stand der Entwicklung Ende Dezember wieder, nachdem
die erste Veroeffentlichung zum Tool eingereicht wurde.

db 2000-02-16:
Zum make'en dieses Projektes make in diesem Verzeichnis aufrufen.
Das executable 'rabbit' bzw. 'rabbit.exe' liegt in diesem Verzeichnis.

db 2000-03-31:
Die Souce-Files liegen unter cta, ana, ddm, bdd.

db 2002-08-28:
Relase 2.0 ('release-2-0'): BDD-basierte Version des Tools,
so wie in den Veröffentlichungen 2001 wiedergegeben.

db 2002-09-23:
Release 2.1 ('release-2-1'): Stand der Entwicklung Ende September 2002,
bzw. nach Abgabe der Dissertation von Dirk.

